//
//  ChooseCategories.m
//  TinyNews
//
//  Created by Nava Carmon on 7/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChooseCategories.h"

@implementation ChooseCategories

@synthesize tableView, currentRow, controller;

- (IBAction)back:(id)sender
{
    if ([controller respondsToSelector:@selector(saveSelectedCategory:)]) {
        NSString *type = nil;
        if (currentRow >= 0) {
            type = [[Context getInstance].categories objectAtIndex:currentRow];
        }             
        [controller performSelector:@selector(saveSelectedCategory:) withObject:type];
    }

    [self dismissModalViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    [tableView reloadData];
}

- (void) dealloc
{
    [controller release];
    [tableView release];
    [super dealloc];
}

#pragma mark UITableView delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [[Context getInstance].categories count];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CategoryCellIdentifier = @"CategoryCell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CategoryCellIdentifier];
    if (!cell) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CategoryCellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    NSString *categoryType = [[Context getInstance].categories objectAtIndex:indexPath.row];
    cell.textLabel.text = [[Context getInstance] getDisplayTextFromMessageType:categoryType];
    
    if (currentRow == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)atableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentRow = indexPath.row;
    [atableView reloadData];
}



@end
