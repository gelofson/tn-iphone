

//Bharat: 11/11/11: Setting to production server for release
//#define kServerUrl @"http://66.28.216.132"
//#define kServerUrl @"http://theshopbee.com"

#define kLockedInfoErrorCode 500

#define kCannotViewFriendInfoTitle @"Sorry!"

#define kCannotViewFriendInfoMessage @"Oops! sorry that information is for friend only."

#define kNetNotAvailableAlertTitle @"Internet Error!"

#define kNetNotAvailableAlertMsg @"Please check your internet connection and try again."

#define kNetworkDownErrorTitle @"Network Error!"

#define	kNetworkDownErrorMsg @"Server not responding. Please try again later."

#define kFBConnectionErrorTitle @"Facebook connection Error!"

#define	kFBConnectionErrorMsg @"Cannot connect to Facebook. Please try again later."


#define kFontName @"GillSans"

#define kSessionTimeOutTitle @"Session timeout!"

#define kSessionTimeOutMessage @"Session expired.Please login again to continue."

#define kHelveticaBoldFont @"Helvetica"
#define kHelveticaBoldBFont @"Helvetica-Bold"

#define kArialFont @"Arial"

#define kGillSansFont @"GillSans-Bold"
#define kMyriadProRegularFont @"MyriadPro-Regular"
#define kMyriadProBoldFont @"MyriadPro-BoldCond"

#define kArialBoldFont @"Arial-BoldMT"

#define kFuturaItalicFont @"Futura-MediumItalic"

#define kCursiveFontName @"Brush Script MT"

#define kFileName @"AuditData.plist"

#define kTotalAuditRecords 10

#define kRegistrationKey @"Registered"

#define kCustomerId @"CustomerId"

#define kTinyNewsUserUUID @"TinyNewsUserUUID"

#define IS_WIDESCREEN (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height==568)

#define kServiceNotAvailableInAreaMsg @"We do not see any sales within 50 miles of your zip code, do you still want to register?"

//@"Service not available in your area. We cannot find any retailers with in 50 miles at this point. We will notify you when the service is available in your area. Thank you for registering."
//Welcome to shopbee. No retailers found within 50 miles of your current location. 

//Categories slogan texts
//#define kShoesSlogan @"Shoes: What's hot..."
// Bharat: US221
#define kShoesSlogan @"Shoes for feet only... :)"

//#define kBagsSlogan @"Bags: What's hot..."
#define kBagsSlogan @"Bags for every occasion..."

//#define kApparelSlogan @"Apparel: What's hot..."
//#define kApparelSlogan @"Gorgeous clothes on sale"
// Bharat: US249 fixes
#define kApparelSlogan @"Favorite Fashions on Sale"

//#define kAccessoriesSlogan @"Accessories: What's hot..."
#define kAccessoriesSlogan @"Specialty Shops to Surprise..."

//#define kHealthBeautySlogan @"Health and Beauty: What's hot..."
#define kHealthBeautySlogan @"Health and Beauty: What's not to like?"

//#define kSportsSlogan @"Sports: What's hot..."
#define kSportsSlogan @"Sports: Because everyone needs a challenge"

//#define kElectronicsSlogan @"Electronics: What's hot..."
#define kElectronicsSlogan @"Gadgets on the go..."

//#define kHomeFashionsSlogan @"Home Fashions: What's hot..."
#define kHomeFashionsSlogan @"Home Fashions: What's hot..."

//#define kMoviesSlogan @"Movies: What's hot..."
#define kMoviesSlogan @"Entertainment for everyone..."

//#define kDiningSlogan @"Dining: What's around..."
#define kDiningSlogan @"Dining: Food for Food lovers"

//#define kKidsSlogan @"Kids: Great deals..."
#define kKidsSlogan @"Buy something while they're still young..."

//#define kMensSlogan @"Menswear: What's hot..."
#define kMensSlogan @"For the Men in your life"

//#define kCharitySlogan @"Giving: What you can do..."
#define kCharitySlogan @"Giving: What you can do..."

#define POPULAR 0
#define FRIENDS 1
#define FOLLOWING 2
#define MINE        3
//Categories names
#define kShoes @"Shoes"

#define kBags @"Bags"

#define kApparel @"Dresses"

#define kAccessories @"Jewellery"

#define kHealthBeauty @"Cosmetics"

#define kSports @"Sports"

#define kElectronics @"Electronics"

#define kHomeFashions @"Home"

#define kMovies @"Movies"

#define kDining @"Food"

#define kKids @"Kids"

#define kMen @"Men"

#define kCharity @"Giving"

#define kTellAFriendMailSubject @"Shopbee... in the iTunes store"

#define kTellAFriendMailBody @"Hi,\n\nI thought you might like this new shopping app. It's called Shopbee...it's for the iPhone:\n\nhttp://itunes.com/apps/shopbee\n\nThanks,"

#define kTellAFriendMailBody2 @"Hi, I thought you might like this new shopping app. It's called Shopbee...it's for the iPhone. Thanks,"

#define kTellAFriendMailBody3 @"Hi, I thought you might like this new shopping app. It's called Shopbee...it's for the iPhone: http://itunes.com/apps/shopbee Thanks,"

#define kAppStoreLink @"[{\"text\":\"Shopbee AppStore link..\",\"href\":\"http://itunes.com/apps/shopbee/\"}]"

#define kBuyItOrNot         @"BuyItOrNot"
#define kIBoughtIt          @"IBoughtIt"
#define kCheckThisOut       @"CheckThisOut"
#define kHowDoesThisLook    @"HowDoesThisLook"
#define kFoundASale         @"FoundASale"
#define kMessageBoard       @"Q"