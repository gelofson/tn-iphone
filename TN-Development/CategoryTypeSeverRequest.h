//
//  CategoryTypeSeverRequest.h
//  TinyNews
//
//  Created by jay kumar on 11/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSON.h"
@interface CategoryTypeSeverRequest : NSObject<NSURLConnectionDelegate>
{
    NSMutableData *responseData;
    NSURLConnection *m_connection;
    int m_responseCode;
}
-(void)getStoriesInRange:(SEL)tempSelector tempTarget:(id)target CustomId:(NSString*)CustID  page:(int)page number:(int)num type:(NSString*)categoryType;

-(void)searchStories:(SEL)tempSelector tempTarget:(id)target seratchText:(NSString*)searchText startIndex:(int)startIndex endIndex:(int)endIndex;

@end
