//
//  WishListViewController.h
//  QNavigator
//
//  Created by Soft Prodigy on 24/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "tblWishList.h"


@interface WishListViewController : UIViewController <UITextViewDelegate>
{

	IBOutlet UITableView *m_tblWishList;
	UIView *m_customEntryView;
	NSManagedObjectContext *_context; 
	IBOutlet UIButton *m_editButton;
	IBOutlet UILabel *m_myLabel;
}

@property (nonatomic,retain) UITableView *m_tblWishList;
@property (nonatomic,retain) NSManagedObjectContext *context;

-(void)requestFetching;
@end
