//
//  UploadImageAPI.m
//  QNavigator
//
//  Created by softprodigy on 23/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UploadImageAPI.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "JSON.h"
#import "NewsDataManager.h"
#import "LoadingIndicatorView.h"

@implementation UploadImageAPI

@synthesize m_theConnection;
@synthesize requestType;

QNavigatorAppDelegate *l_appDelegate;

BOOL isConnectionActive;


#pragma mark -
#pragma mark updateUserPic
-(void) updateUserPic:(NSDictionary *)updateDictionary
{
	
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//self.view.userInteractionEnabled=FALSE;
		m_mutResponseData=[[NSMutableData alloc] init];
		//callBackSelector=(SEL)[updateDictionary objectForKey:@"selector"];
		//tempSelector;
		
		//callBackTarget=[updateDictionary objectForKey:@"callingObject"];
		
		//[tempTarget retain];
		//NSLog(@"the dictionary is %@",updateDictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=updateUserInfo",kServerUrl];
		//	NSLog(@"value for rating is%i",userRating);//
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
			
		NSString *temp_strJson=[NSString stringWithFormat:@"userinfo=%@",[updateDictionary JSONRepresentation]];
		//NSLog(@"the strJson: %@",temp_strJson);
		
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
				
		if (isConnectionActive)
		{
			[m_theConnection cancel];
			
		}
		
		m_theConnection= [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
		
		[m_theConnection start];
		isConnectionActive=TRUE;
		
	}
	
			
}

#pragma mark -
#pragma mark sendBuzzRequest
-(NSString *)sendBuzzRequest:(NSDictionary*)m_dictionary
{
	
	[l_appDelegate CheckInternetConnection];
	//callBackSelector=tempSelector;
	//callBackTarget=tempTarget;
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		m_mutResponseData=[[NSMutableData alloc] init];
		
		//self.view.userInteractionEnabled=FALSE;
		//NSLog(@"The dictionary contains%@",m_dictionary);
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=sendBuzzRequest",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		NSString *temp_strJson=[NSString stringWithFormat:@"buzz=%@",[m_dictionary JSONRepresentation]];
		NSLog(@"%@",temp_strJson);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];

//		if (isConnectionActive)
//		{
//			[m_theConnection cancel];
//			
//		}
		
//		m_theConnection= [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
//		
//		[m_theConnection start];
//		isConnectionActive=TRUE;
		
		NSURLResponse *response;
        NSError *error = nil;
        
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&error];
        if (!error && data) {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            m_intResponseCode = [httpResponse statusCode];
            NSLog(@"%d",m_intResponseCode);
            if (m_intResponseCode==200 && requestType==0)//request from mypage user pic change
            {
                UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"Image uploaded!" message:@"User image uploaded successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [tempAlert show];
                [tempAlert release];
                tempAlert=nil;
            }
            else if(m_intResponseCode==200 && requestType==1)//rquest from buzz use case
            {
                requestType=0;
                UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [tempAlert show];
                [tempAlert release];
                tempAlert=nil;
            } else if (m_intResponseCode == 401)
                [[NewsDataManager sharedManager] showErrorByCode:m_intResponseCode fromSource:NSStringFromClass([self class])];
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            if (data && m_intResponseCode==200) {
                NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"Received String=%@",str);
                NSString *newString = [str substringWithRange:NSMakeRange(1, [str length] - 2)];
                [str release]; 
                return newString;
            } else {
                UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"Error!" message:@"Couldn't send the story. Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"Received String=%@",str);
                [tempAlert show];
                [tempAlert release];
                tempAlert=nil;
                return nil;
            }
        } else {
            [[LoadingIndicatorView SharedInstance]stopLoadingView];
            UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"Error!" message:@"Couldn't send the story. Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [tempAlert show];
            [tempAlert release];
            tempAlert=nil;
        }
		//[temp_strJson release];
	}
	return nil;
}


#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	m_intResponseCode = [httpResponse statusCode];
	NSLog(@"%d",m_intResponseCode);
	
	[m_mutResponseData setLength:0];	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	
	[m_mutResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	//[m_theConnection release];
//	m_theConnection=nil;
	
	isConnectionActive=FALSE;
	
	//NSString *str=[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
//	NSLog(@"Received String=%@",str);
//	[str release];
//	str=nil;
	
	if (m_intResponseCode==200 && requestType==0)//request from mypage user pic change
	{
		UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"Image uploaded!" message:@"User image uploaded successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tempAlert show];
		[tempAlert release];
		tempAlert=nil;
	}
	else if(m_intResponseCode==200 && requestType==1)//rquest from buzz use case
	{
		requestType=0;
		UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tempAlert show];
		[tempAlert release];
		tempAlert=nil;
	} else if (m_intResponseCode == 401)
        [[NewsDataManager sharedManager] showErrorByCode:m_intResponseCode fromSource:NSStringFromClass([self class])];

	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	//[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:m_intResponseCode] withObject:m_mutResponseData];
	
}


- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	//[m_theConnection release];
//	m_theConnection=nil;
	
	isConnectionActive=FALSE;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
    [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
	
	if (m_mutResponseData)
	{
		[m_mutResponseData release];
		 m_mutResponseData=nil;
	}
	
	//[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	
}
 

#pragma mark Singleton Instance funtion

+ (id) SharedInstance 
{
	static id sharedManager = nil;
	
    if (sharedManager == nil) {
        sharedManager = [[self alloc] init];
    }
	
    return sharedManager;
}

#pragma mark - memory management

- (void) dealloc {
    // I expect this was already released.
    //[m_theConnection release];
    
    [m_mutResponseData release];
    
    [super dealloc];
}

@end
