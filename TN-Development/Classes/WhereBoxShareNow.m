//
//  BuzzButton ShareNowFunctionality.m
//  QNavigator
//
//  Created by Bharat Biswal on 12/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "WhereBoxShareNow.h"
#import "BuzzButtonCameraFunctionality.h"
#import "Constants.h"
#import "BuzzButtonAddNewContact.h"
#import "BuzzButtonAddFromAddressBook.h"
#import "QNavigatorAppDelegate.h"
#import "LoadingIndicatorView.h"
#import "UploadImageAPI.h"

QNavigatorAppDelegate *l_appDelegate;

@interface WhereBoxShareNow (PRIVATE)
- (void)pushBuzzScreenWithText:(NSString*)text;
- (void)postToFacebookWall:(NSString *) photoUrl;
- (void)postToTwitterWall:(NSString *) photoUrl;
- (void) performDeleteOfContactsIfAny;
- (void) performPhotoShare;
@end

@implementation WhereBoxShareNow

#ifdef FB_OLD_SDK_USAGE
@synthesize localFBSession;
//@synthesize fbLoginDialog;
#endif

@synthesize twitterEngine;

@synthesize m_mutResponseData;
@synthesize m_intResponseCode;
@synthesize m_intRequestType;
@synthesize m_intInputType;
@synthesize m_photoUrl;

@synthesize m_catObjModel;

@synthesize m_emailTableView;
@synthesize m_facebookButton;
@synthesize m_twitterButton;
@synthesize m_selectAllButton;
@synthesize m_sectionHeaderView;
@synthesize m_photoView;
@synthesize photoImage;
@synthesize m_DictOfContactDicts;
@synthesize m_newlyAddedContactsDict;
@synthesize m_deletedContactsDict;
@synthesize addrBookVc;
//@synthesize twitterProcessingVc;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{

	[super viewDidLoad];
	addrBookVc = nil;
	m_photoUrl = nil;
	twitterEngine = nil;

	[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bgNewWhereBox.png"]]];
	
	NSMutableDictionary * dict = [[NSMutableDictionary alloc] initWithCapacity:3];
	self.m_DictOfContactDicts = dict;
	[dict release];
	dict = [[NSMutableDictionary alloc] initWithCapacity:3];
	self.m_newlyAddedContactsDict = dict;
	[dict release];
	dict = [[NSMutableDictionary alloc] initWithCapacity:3];
	self.m_deletedContactsDict = dict;
	[dict release];

	if (self.photoImage != nil) {
		[self.m_photoView setImage:self.photoImage];
        [self.m_photoView setContentMode:UIViewContentModeScaleAspectFill];
        self.m_photoView.clipsToBounds=YES;
	}
	
	self.m_facebookButton.selected = NO;
	self.m_twitterButton.selected = NO;
	self.m_selectAllButton.selected = YES;
    
    CALayer * layer = self.m_emailTableView.layer;
    layer.borderWidth = 3.0f;
    layer.borderColor = [UIColor colorWithRed:104.0f/255.0f green:176.0f/255.0f blue:223.0f/255.0f alpha:1.0].CGColor;
	
	[l_appDelegate CheckInternetConnection];
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		// Call FashionGram API to invoke share
		[self performSelectorInBackground:@selector(showLoadingView) withObject:nil];
		m_mutResponseData=[[NSMutableData alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
		NSString *temp_url;
		temp_url=[NSString stringWithFormat:@"%@/salebynow/json.htm?action=getUserContacts&custid=%@",kServerUrl, customerID];
		temp_url=[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];

		NSURLConnection * conn = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
		self.m_intRequestType = SHARENOW_REQUEST_TYPE_GETCONTACT;
		[conn start];
	}
    

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

#if TARGET_IPHONE_SIMULATOR == 1
#else 
	// Merge selected contacts from address book
	if (self.addrBookVc != nil) {
		
		if ([self.addrBookVc.m_addrBookEmailContacts count] > 0) {
			for (NSString * keyStr in [self.addrBookVc.m_addrBookEmailContacts allKeys]) {
				NSMutableDictionary * contactDict = [self.addrBookVc.m_addrBookEmailContacts objectForKey:keyStr];
				NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
				if ((selectionValue != nil) && ([selectionValue caseInsensitiveCompare:@"1"] == NSOrderedSame)) {
					NSMutableDictionary * existEntry = [self.m_DictOfContactDicts objectForKey:keyStr];
					if (existEntry != nil) {
						// entry exists, by name
						[existEntry setObject:@"1" forKey:@"selectionValue"];
						continue;
					}
					// check each email
					NSString * emailStr = [contactDict objectForKey:@"emailValue"];
					BOOL emailFound = NO;
					for (NSString * existKeyStr in [self.m_DictOfContactDicts allKeys]) {
						NSMutableDictionary * aDict = [self.m_DictOfContactDicts objectForKey:existKeyStr];
						NSString * aEmail = [aDict objectForKey:@"emailValue"];
						if ((aEmail != nil) && ([aEmail caseInsensitiveCompare:emailStr] == NSOrderedSame)) {
							[aDict setObject:@"1" forKey:@"selectionValue"];
							emailFound = YES;
							break;
						}
					}

					if (emailFound == NO) {
						[self.m_newlyAddedContactsDict setObject:contactDict forKey:keyStr];
						[self.m_DictOfContactDicts setObject:contactDict forKey:keyStr];
					}

				}
			}
		} 

		self.addrBookVc = nil;
	}
#endif

	BOOL selectedAll = YES;
	if ([self.m_DictOfContactDicts count] > 0) {
		for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
			NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
			NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
			if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
				selectedAll = NO;
				break;
			}
		}
	} else {
		selectedAll = NO;
	}

	self.m_selectAllButton.selected = selectedAll;

    [self.m_emailTableView reloadData];

}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
 
-(IBAction)goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
}

- (void)dealloc {
    
	//[l_appDelegate.m_session.delegates removeObject:self];

	self.m_photoUrl = nil;
	self.m_mutResponseData = nil;
	//self.twitterProcessingVc = nil;
	self.m_catObjModel = nil;
	self.addrBookVc = nil;
	self.m_emailTableView = nil;
	self.m_facebookButton = nil;
	self.m_twitterButton = nil;
	self.m_selectAllButton = nil;
	self.m_sectionHeaderView = nil;
	self.m_photoView = nil;
	self.photoImage = nil;
	self.m_DictOfContactDicts = nil;
	self.m_newlyAddedContactsDict = nil;
	self.m_deletedContactsDict = nil;

    [super dealloc];
}

-(IBAction) contactsSelectAllToggleAction:(id) sender {
	UIButton* button = (UIButton*)sender;
    button.selected = !button.selected;
	self.m_intRequestType = 0;
	
	if ([self.m_DictOfContactDicts count] > 0) {
		for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
			NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];

			if (button.selected == YES)
				[contactDict setObject:@"1" forKey:@"selectionValue"];
			else
				[contactDict setObject:@"0" forKey:@"selectionValue"];
		}
	} 

	[self.m_emailTableView reloadData];
}

#ifdef FB_OLD_SDK_USAGE
-(IBAction) facebookToggleAction:(id) sender {
	
	self.m_intRequestType = 0;
	if (self.m_facebookButton.selected == NO) {
		// User wants to enable FB share, check if FB login is available, else launch
		if (l_appDelegate.m_session == nil) {
			l_appDelegate.m_session = [FBSession sessionForApplication:_APP_KEY 
												  secret:_SECRET_KEY delegate:self];
		} else {
			if ([l_appDelegate.m_session.delegates indexOfObject:self] == NSNotFound)
				[l_appDelegate.m_session.delegates addObject:self];
		}
		
		localFBSession = [FBSession session];
		if (localFBSession.uid <= 0) {
            FBLoginDialog * fbLoginDialog = [[[FBLoginDialog alloc] initWithSession:localFBSession] autorelease];
			[fbLoginDialog show];
		} else {
			self.m_facebookButton.selected = YES;
		}
	} else {
		self.m_facebookButton.selected = NO;
	}
}
#endif

-(IBAction) twitterToggleAction:(id) sender {
	
	self.m_intRequestType = 0;
	if (self.m_twitterButton.selected == NO) {
		// User wants to enable TW share, check if TW login is available, else launch
		/*
		TwitterProcessing *temp_twitterPross=[[TwitterProcessing alloc]init];
		temp_twitterPross.m_addFrndMypageVC=self;
		self.twitterProcessingVc = temp_twitterPross;
		[temp_twitterPross release];
		
		[self.twitterProcessingVc btnTellFriendByTwitterAction:99];
		
		self.m_twitterButton.selected = YES;
		*/
		if(twitterEngine == nil)
		{  
			twitterEngine = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];  
			twitterEngine.consumerKey    = kOAuthConsumerKey;  
			twitterEngine.consumerSecret = kOAuthConsumerSecret;  
		}
		
		//	BOOL isEmpty = [ self validateIsEmpty:txtField.text];                                               
		if(![twitterEngine isAuthorized])
		{  
			UIViewController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:twitterEngine delegate:self];  
			if (controller)
				[self presentModalViewController: controller animated: YES];
		} else {
			self.m_twitterButton.selected = YES;
		}
	} else {
		self.m_twitterButton.selected = NO;
	}
}

-(IBAction) contactSelectToggleAction:(id) sender {
	UIButton* button = (UIButton*)sender;
    button.selected = !button.selected;
	
	if (button.selected == YES) {
	}
}

-(void)showLoadingView
{
	NSAutoreleasePool *tempPool=[[NSAutoreleasePool alloc]init];
	
	[[LoadingIndicatorView SharedInstance]startLoadingView:self];
	
	[tempPool release];
}



-(IBAction) doShareNow:(id) sender {
	// Call FashionGram API to invoke share
	[self performSelectorInBackground:@selector(showLoadingView) withObject:nil];

	// Send newly added email addresses
	if ([self.m_newlyAddedContactsDict count] > 0) {
		[l_appDelegate CheckInternetConnection];
		
		if(l_appDelegate.m_internetWorking==0)//0: internet working
		{
			// Call FashionGram API to invoke share
			[self performSelectorInBackground:@selector(showLoadingView) withObject:nil];
			m_mutResponseData=[[NSMutableData alloc] init];
			
			NSMutableArray * accArr = [[NSMutableArray alloc] initWithCapacity:3];
			if ((self.m_newlyAddedContactsDict) && ([self.m_newlyAddedContactsDict count] > 0)) {
				for (NSString * keyStr in [self.m_newlyAddedContactsDict allKeys]) {
					NSMutableDictionary * contactDict = [self.m_newlyAddedContactsDict objectForKey:keyStr];
					NSString * emailAddr = [contactDict objectForKey:@"emailValue"];
					NSString * nameAddr = [contactDict objectForKey:@"nameValue"];
					NSMutableDictionary * dict = [[NSMutableDictionary alloc] initWithCapacity:2];
					[dict setObject:emailAddr forKey:@"email"];
					[dict setObject:nameAddr forKey:@"name"];
					[accArr addObject:dict];
					[dict release];
				}
			}
			if ([accArr count] > 0)  {
			
				[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
				NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
				NSString *customerID= [prefs objectForKey:@"userName"];
				NSString *temp_url;
				temp_url=[NSString stringWithFormat:@"%@/salebynow/json.htm?action=addUserContacts&custid=%@",kServerUrl, customerID];
				temp_url=[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				
				NSLog(@"%@",temp_url);
				
				NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																		  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
				
				NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
			
				NSString *temp_strJson=[NSString stringWithFormat:@"contacts=%@",[accArr JSONRepresentation]];
				NSLog(@"temp_strJson=[%@]",temp_strJson);
				[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
				[theRequest setAllHTTPHeaderFields:headerFieldsDict];
				[theRequest setHTTPMethod:@"POST"];

				NSURLConnection * conn = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
				self.m_intRequestType = SHARENOW_REQUEST_TYPE_ADDCONTACT;
				[conn start];
			}
			[accArr release];
		}
	} else {
		[self performDeleteOfContactsIfAny];
	}
}

-(void) performDeleteOfContactsIfAny {
	
	// Send newly added email addresses
	if ([self.m_deletedContactsDict count] > 0) {
		[l_appDelegate CheckInternetConnection];
		
		if(l_appDelegate.m_internetWorking==0)//0: internet working
		{
			// Call FashionGram API to invoke share
			[self performSelectorInBackground:@selector(showLoadingView) withObject:nil];
			m_mutResponseData=[[NSMutableData alloc] init];
			
			NSMutableArray * accArr = [[NSMutableArray alloc] initWithCapacity:3];
			if ((self.m_deletedContactsDict) && ([self.m_deletedContactsDict count] > 0)) {
				for (NSString * keyStr in [self.m_deletedContactsDict allKeys]) {
					NSMutableDictionary * contactDict = [self.m_deletedContactsDict objectForKey:keyStr];
					NSString * emailAddr = [contactDict objectForKey:@"emailValue"];
					[accArr addObject:emailAddr];
				}
			}
			if ([accArr count] > 0)  {
			
				[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
				NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
				NSString *customerID= [prefs objectForKey:@"userName"];
				NSString *temp_url;
				temp_url=[NSString stringWithFormat:@"%@/salebynow/json.htm?action=deleteUserContacts&custid=%@",kServerUrl, customerID];
				temp_url=[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				
				NSLog(@"%@",temp_url);
				
				NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																		  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
				
				NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
			
				NSString *temp_strJson=[NSString stringWithFormat:@"listids=%@",[accArr JSONRepresentation]];
				NSLog(@"temp_strJson=[%@]",temp_strJson);
				[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
				[theRequest setAllHTTPHeaderFields:headerFieldsDict];
				[theRequest setHTTPMethod:@"POST"];

				NSURLConnection * conn = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
				self.m_intRequestType = SHARENOW_REQUEST_TYPE_DELCONTACT;
				[conn start];
			}
			[accArr release];
		}
	} else {
		[self performPhotoShare];
	}
}

-(void) performPhotoShare {

	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];

	NSString * productId = nil;
	if((self.m_catObjModel != nil) && (self.m_catObjModel.m_strId != nil))
		productId = self.m_catObjModel.m_strId;
	
	NSString * subject = nil;
	if((self.m_catObjModel != nil) && (self.m_catObjModel.m_strBrandName != nil))
		subject = self.m_catObjModel.m_strBrandName;
	else if((self.m_catObjModel != nil) && (self.m_catObjModel.m_strTitle != nil))
		subject = self.m_catObjModel.m_strTitle;
	else if((self.m_catObjModel != nil) && (self.m_catObjModel.m_strDiscount != nil))
		subject = self.m_catObjModel.m_strDiscount;
	else	
		subject = @"Share \"FashionGram\"";
	

	NSString *message = [NSString stringWithFormat:@"%@\n %@\n %@", self.m_catObjModel.m_strTitle, self.m_catObjModel.m_strDiscount, self.m_catObjModel.m_strAddress];

	NSMutableArray * emailArr = [[NSMutableArray alloc] initWithCapacity:3];
	if ((self.m_DictOfContactDicts) && ([self.m_DictOfContactDicts count] > 0)) {
		for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
			NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
			NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
			if ((selectionValue != nil) && ([selectionValue caseInsensitiveCompare:@"1"] == NSOrderedSame)) {
				NSString * emailAddr = [contactDict objectForKey:@"emailValue"];
				[emailArr addObject:emailAddr];			
			}
		}
	}
	if ([emailArr count] <= 0) {
		[emailArr addObject:customerID];
	}

	[l_appDelegate CheckInternetConnection];
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		// Call FashionGram API to invoke share
		[self performSelectorInBackground:@selector(showLoadingView) withObject:nil];

		m_mutResponseData=[[NSMutableData alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=shareSaleInfoViaEmail",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSString *temp_strJson=nil;
		
		if (self.m_intInputType == INPUT_TYPE_SHOPBEE_ADS) {
			temp_strJson=[NSString stringWithFormat:@"custid=%@&prdtid=%@&subject=%@&message=%@&listids=%@",
					 [customerID stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
					 productId,
					 [subject stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
					 [message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
					 [emailArr JSONRepresentation]];
			//NSLog(@"%@",temp_strJson);
		} else {
			temp_strJson=[NSString stringWithFormat:@"custid=%@&prdtid=0&subject=%@&message=%@&listids=%@",
					 [customerID stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
					 [subject stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
					 [message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
					 [emailArr JSONRepresentation]];
			//NSLog(@"%@",temp_strJson);
		}
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		NSLog(@"temp_strJson=[%@]",temp_strJson);
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];

		NSURLConnection * conn = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
		self.m_intRequestType = SHARENOW_REQUEST_TYPE_SHARENOW;
		[conn start];
	}
    
	[emailArr release];

}

-(IBAction) addNewEmailAction:(id) sender {
    
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"From my contact list",@"Enter new address",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
    [actionSheet release];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return self.m_sectionHeaderView.frame.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	return self.m_sectionHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [m_DictOfContactDicts count];
	//return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell * cell = nil;
	static NSString *cellIdentifier= @"ShareNow_CellIdentifier";
	
	cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if(cell==nil) {
		cell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier]autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		UIImageView *temp_imgView=[[UIImageView alloc]initWithFrame:CGRectMake(13,5,18,18)];
		//[temp_imgView setImage:l_objIndyShopModel.m_imgItemImage]; //@"clothes.png"]];
		[temp_imgView setImage:[UIImage imageNamed:@"sharenow_checkbox_disabled"]];
		temp_imgView.contentMode=UIViewContentModeScaleAspectFit;
		temp_imgView.tag = CONTACT_ENTRY_SELECT_TAG;
		[cell.contentView addSubview:temp_imgView];
		[temp_imgView release];

		UILabel *temp_lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(38,2,258,22)];
		temp_lblTitle.numberOfLines = 1;
		temp_lblTitle.font=[UIFont fontWithName:@"GillSans" size:16];
		temp_lblTitle.textColor=[UIColor colorWithRed:32.0f/255.0f green:80.0f/255.0f blue:112.0f/255.0f alpha:1.0];
		temp_lblTitle.backgroundColor=[UIColor clearColor];
		temp_lblTitle.text=@"Greg Elofson";
		temp_lblTitle.tag = CONTACT_ENTRY_NAME_TAG;
		[cell.contentView addSubview:temp_lblTitle];
		[temp_lblTitle release];
	
		temp_lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(38,24,258,22)];
		temp_lblTitle.numberOfLines = 1;
		temp_lblTitle.font=[UIFont fontWithName:@"GillSans" size:16];
		temp_lblTitle.textColor=[UIColor colorWithRed:112.0f/255.0f green:112.0f/255.0f blue:112.0f/255.0f alpha:1.0];
		temp_lblTitle.backgroundColor=[UIColor clearColor];
		temp_lblTitle.text=@"greg.elofson@gmail.com";
		temp_lblTitle.tag = CONTACT_ENTRY_EMAIL_TAG;
		[cell.contentView addSubview:temp_lblTitle];
		[temp_lblTitle release];
	
	}
		
	NSArray * sortedKeys = [[self.m_DictOfContactDicts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
        NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
        

        
        UIImageView * iv = (UIImageView *) [cell.contentView viewWithTag:CONTACT_ENTRY_SELECT_TAG];

		NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
		if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
			[iv setImage:[UIImage imageNamed:@"sharenow_checkbox_disabled"]];
		}
		else {
			[iv setImage:[UIImage imageNamed:@"sharenow_checkbox_enabled"]];
		}
        
        UILabel * lbl = (UILabel *) [cell.contentView viewWithTag:CONTACT_ENTRY_NAME_TAG];
        NSString * nameValue = [contactDict objectForKey:@"nameValue"];
        lbl.text = nameValue;
        
        lbl=nil;
        lbl = (UILabel *) [cell.contentView viewWithTag:CONTACT_ENTRY_EMAIL_TAG];
        NSString * emailValue = [contactDict objectForKey:@"emailValue"];
        lbl.text = emailValue;

    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 55.0f;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		NSArray * sortedKeys = [[self.m_DictOfContactDicts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
		if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        	NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
			[self.m_deletedContactsDict setObject:[self.m_DictOfContactDicts objectForKey:keyStr] forKey:keyStr];
			[self.m_DictOfContactDicts removeObjectForKey:keyStr];
			BOOL selectedAll = YES;
			if ([self.m_DictOfContactDicts count] > 0) {
				for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
					NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
					NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
					if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
						selectedAll = NO;
						break;
					}
				}
			} else {
				selectedAll = NO;
			}

			self.m_selectAllButton.selected = selectedAll;
		}
        [self.m_emailTableView reloadData];
	} else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray * sortedKeys = [[self.m_DictOfContactDicts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
        NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
        

        
        NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
        if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"1"] == NSOrderedSame)) {
            selectionValue = @"0";
        	[contactDict setObject:selectionValue forKey:@"selectionValue"];
			if (self.m_selectAllButton.selected == YES) {
				self.m_selectAllButton.selected = NO;
			}
		} else {
            selectionValue = @"1";
        	[contactDict setObject:selectionValue forKey:@"selectionValue"];
			BOOL selectedAll = YES;
			if ([self.m_DictOfContactDicts count] > 0) {
				for (NSString * keyStr in [self.m_DictOfContactDicts allKeys]) {
					NSMutableDictionary * contactDict = [self.m_DictOfContactDicts objectForKey:keyStr];
					NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
					if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
						selectedAll = NO;
						break;
					}
				}
			} else {
				selectedAll = NO;
			}

			self.m_selectAllButton.selected = selectedAll;
		}
        
        [contactDict setObject:selectionValue forKey:@"selectionValue"];
        
        [tableView reloadData];
    }



}

#pragma ActionheetDelegate method
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == actionSheet.cancelButtonIndex){
        [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }

    if (buttonIndex == 0) {
		// SHow contacts list
#if TARGET_IPHONE_SIMULATOR == 1
#else       
		if (self.addrBookVc == nil) {
			BuzzButtonAddFromAddressBook * vc = [[BuzzButtonAddFromAddressBook alloc] init];
			self.addrBookVc = vc;
			[self.navigationController pushViewController:vc animated:YES];
			[vc release];
		}
#endif

	} else if(buttonIndex == 1){
		// SHow add new contact page
		BuzzButtonAddNewContact * vc = [[BuzzButtonAddNewContact alloc] init];
		vc.m_DictOfContactDicts = self.m_DictOfContactDicts;
		vc.m_newlyAddedContactsDict = self.m_newlyAddedContactsDict;
		[self.navigationController pushViewController:vc animated:YES];
		[vc release];
    }
}

#ifdef FB_OLD_SDK_USAGE
- (void)getFacebookName {
	NSString* fql = [NSString stringWithFormat:
					 @"select uid,name from user where uid == %lld", localFBSession.uid];
	NSDictionary* params = [NSDictionary dictionaryWithObject:fql forKey:@"query"];
	[[FBRequest requestWithDelegate:self] call:@"facebook.fql.query" params:params];
//	self.post=YES;
}

- (void)session:(FBSession*)session didLogin:(FBUID)uid {
	localFBSession =session;
	[FBSession setSession:session];
    NSLog(@"User with id %lld logged in.", uid);

	[self performSelectorOnMainThread:@selector(handleFBAuthSuccess) withObject:nil waitUntilDone:YES];
	
	[self getFacebookName];
}

- (void)request:(FBRequest*)request didLoad:(id)result {
	if ([request.method isEqualToString:@"facebook.fql.query"]) {
		NSArray* users = result;
		NSDictionary* user = [users objectAtIndex:0];
		NSString* name = [user objectForKey:@"name"];
		NSLog(@"FB Loggedin username=[%@]",name);
		self.m_facebookButton.selected = YES;
	}
}
#endif


- (void)postToTwitterWall:(NSString *) photoUrl {
	if ((self.m_twitterButton.selected == NO) || (photoUrl == nil) || ([photoUrl length] <= 0)
			|| (twitterEngine == nil)) {
		[[LoadingIndicatorView SharedInstance]stopLoadingView];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		// Bharat: DE86: pop to root
		//[self.navigationController popViewControllerAnimated:YES];
		[self.navigationController popViewControllerAnimated:YES];
		return;
	}




	NSString * postName = self.m_catObjModel.m_strBrandName;
	NSMutableString *tweetBody;
	tweetBody=[NSMutableString stringWithFormat:@"%@",postName];
	[tweetBody appendFormat:@"\n"];
	[tweetBody appendFormat:@"%@",self.m_catObjModel.m_strTitle];
	[tweetBody appendFormat:@"\n"];
	[tweetBody appendFormat:@"%@",self.m_catObjModel.m_strDiscount];
	[tweetBody appendFormat:@"\n"];
	[tweetBody appendFormat:@"%@",self.m_catObjModel.m_strAddress];
	[twitterEngine sendUpdate:tweetBody];

}
	
#ifdef FB_OLD_SDK_USAGE
- (void)postToFacebookWall:(NSString *) photoUrl {
	
	NSLog(@"WhereBoxShareNow:postToFacebookWall:[%@]",photoUrl);

	if ((self.m_facebookButton.selected == NO) || (photoUrl == nil) || ([photoUrl length] <= 0)) {
		[self postToTwitterWall:photoUrl];
		return;
	}

	FBStreamDialog *dialog = [[[FBStreamDialog alloc] init] autorelease];
	dialog.userMessagePrompt = @"Enter additional comment";
	dialog.delegate = self;
	
	NSString * postName = self.m_catObjModel.m_strBrandName;
	
	NSString *customMessage = [NSString stringWithFormat:@"%@, %@, %@", self.m_catObjModel.m_strTitle, self.m_catObjModel.m_strDiscount, self.m_catObjModel.m_strAddress];
	
	NSString *serverLink = [NSString stringWithFormat:@"http://itunes.apple.com/app/fashiongram/id498116453?mt=8"];
	NSString *imageSrc = self.m_catObjModel.m_strImageUrl; // Put image URL here
	
	NSMutableDictionary *dictionary = [[[NSMutableDictionary alloc] init]autorelease];
	[dictionary setObject:postName forKey:@"name"];
	[dictionary setObject:serverLink forKey:@"href"];
	[dictionary setObject:customMessage forKey:@"description"];
	
	NSMutableDictionary *media = [[[NSMutableDictionary alloc] init]autorelease];
	[media setObject:@"image" forKey:@"type"];
	[media setObject:serverLink forKey:@"href"];
	[media setObject:imageSrc forKey:@"src"];               
	[dictionary setObject:[NSArray arrayWithObject:media] forKey:@"media"];  
	
	NSLog(@"WhereBoxShareNow:postToFacebookWall:attachment=[%@]",[dictionary JSONFragment]);
	dialog.attachment = [dictionary JSONFragment];


	[dialog show];
	
}
#endif


-(void)gotAllFriendsFromTwitter:(NSArray *)tempFriendsArray
{
	NSLog(@"WhereBoxShareNow:gotAllFriendsFromTwitter:%@",tempFriendsArray);
	self.m_twitterButton.selected = YES;
}

#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	m_intResponseCode = [httpResponse statusCode];
	NSLog(@"WhereBoxShareNow:didReceiveResponse:statusCode=%d",m_intResponseCode);
	
	[m_mutResponseData setLength:0];	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	
	NSLog(@"WhereBoxShareNow:didReceiveData");
	[m_mutResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"WhereBoxShareNow:connectionDidFinishLoading");
	
	if ((m_intResponseCode==200) && (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW))//request from mypage user pic change
	{

		NSLog(@"WhereBoxShareNow:shareSaleInfoViaEmail:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string=[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
		if ((temp_string != nil) && ([temp_string rangeOfString:@"true"].location != NSNotFound)) {
#if 0					
			self.m_photoUrl = [[NSString stringWithFormat:@"%@/%@",kServerUrl,str] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

			/* Testing short URL */
			NSString *photoShortenedUrl = nil;
			
			NSString * shortReq = [NSString stringWithFormat:@"https://api-ssl.bitly.com/v3/shorten?login=theshopbee&apiKey=R_9452d51e356692291115ec6bcb137e3c&format=json&longUrl=%@", self.m_photoUrl];
			//NSString * shortReq = @"https://api-ssl.bitly.com/v3/shorten?login=theshopbee&apiKey=R_9452d51e356692291115ec6bcb137e3c&format=json&longUrl=http%3A%2F%2Fbetaworks.com%2F";
									   
			NSLog(@"shortReq=[%@]",shortReq);
			NSURL *url = [NSURL URLWithString: shortReq];
			NSString * shortReqRes = [NSString stringWithContentsOfURL:url encoding:NSASCIIStringEncoding error:nil];
			NSLog(@"shortReqRes=[%@]",shortReqRes);
			if ( (shortReqRes != nil) && ([shortReqRes length] > 5) ) {
			}
#endif
			[self postToFacebookWall:self.m_catObjModel.m_strImageUrl];
			[temp_string release];
			return;
		} 
		
		[[LoadingIndicatorView SharedInstance]stopLoadingView];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[temp_string release];
		return;

	} else if ((m_intResponseCode==200) && (self.m_intRequestType == SHARENOW_REQUEST_TYPE_GETCONTACT))//request from mypage user pic change
	{

		NSLog(@"WhereBoxShareNow:getUserContacts:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string=[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
		SBJSON *temp_objSBJson = [[SBJSON alloc]init];
		NSArray *contArr = [temp_objSBJson objectWithString:temp_string];
        [temp_string release]; temp_string = nil;
        [temp_objSBJson release]; temp_objSBJson = nil;
		if ((contArr != nil) && ([contArr isKindOfClass:[NSArray class]] == YES) && ([contArr count] > 0)) {
			for (NSDictionary * dict in contArr) {
				NSString * email = [dict objectForKey:@"email"];
				NSString * name = [dict objectForKey:@"name"];

				NSMutableDictionary * d = [[NSMutableDictionary alloc] initWithCapacity:3];
				[d setObject:email forKey:@"emailValue"];
				[d setObject:name forKey:@"nameValue"];
				[d setObject:@"0" forKey:@"selectionValue"];

				[self.m_DictOfContactDicts setObject:d forKey:name];
                [d release]; d = nil;

			}
		}
		

		[temp_objSBJson release];
        [temp_string release];
		[self.m_emailTableView reloadData];
		
		[[LoadingIndicatorView SharedInstance]stopLoadingView];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

		return;
	} else if ((m_intResponseCode==200) && (self.m_intRequestType == SHARENOW_REQUEST_TYPE_ADDCONTACT))//request from mypage user pic change
	{

		NSLog(@"WhereBoxShareNow:addUserContacts:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		[self performDeleteOfContactsIfAny];

		return;
	} else if ((m_intResponseCode==200) && (self.m_intRequestType == SHARENOW_REQUEST_TYPE_DELCONTACT))//request from mypage user pic change
	{

		NSLog(@"WhereBoxShareNow:deleteUserContacts:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		[self performPhotoShare];


		return;
	}
	
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
}


- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	NSLog(@"WhereBoxShareNow:didFailWithError");
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	UIAlertView *tmpAlert=[[UIAlertView alloc]initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[tmpAlert show];
	[tmpAlert release];
	tmpAlert=nil;
	
	if (m_mutResponseData)
	{
		[m_mutResponseData release];
		 m_mutResponseData=nil;
	}
}

-(void) handleFBAuthCancelled {
	self.m_facebookButton.selected = NO;
}

-(void) handleFBAuthFailure {
	self.m_facebookButton.selected = NO;

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"login Unsuccesfull" message:@"login failed!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alert show];
	[alert release];
}

-(void) handleFBAuthSuccess {
	self.m_facebookButton.selected = YES;
}

-(void) handleTWAuthCancelled {
	self.m_twitterButton.selected = NO;
}

-(void) handleTWAuthFailure {
	self.m_twitterButton.selected = NO;

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"login Unsuccesfull" message:@"login failed!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alert show];
	[alert release];
}

-(void) handleTWAuthSuccess {
	self.m_twitterButton.selected = YES;
}

-(void) handleShareNowCompletionWithTWFailure {
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
 
-(void) handleShareNowCompletionWithTWSuccess {
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
	// Bharat: DE86: pop to root
	//[self.navigationController popViewControllerAnimated:YES];
	[self.navigationController popViewControllerAnimated:YES];
}
 
#pragma mark FBDialogDelegate
/**
 * Called when the dialog succeeds and is about to be dismissed.
 */
- (void)dialogDidSucceed:(FBDialog*)dialog {
	
	if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW) {
		[self performSelectorOnMainThread:@selector(postToTwitterWall:) withObject:self.m_photoUrl waitUntilDone:YES];
	}

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message sent" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alert show];
	[alert release];	
}

/**
 * Called when the dialog is cancelled and is about to be dismissed.
 */
- (void)dialogDidCancel:(FBDialog*)dialog {
	// do nothing
	if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW) {
		[self performSelectorOnMainThread:@selector(postToTwitterWall:) withObject:self.m_photoUrl waitUntilDone:YES];
	}

}

/** 
 * Called when an error prevents the request from completing successfully. 
 */ 
- (void)request:(FBRequest*)request didFailWithError:(NSError*)error{ 
	
	if (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW) {
		[self performSelectorOnMainThread:@selector(postToTwitterWall:) withObject:self.m_photoUrl waitUntilDone:YES];
	}

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alert show];
	[alert release];	
	
} 

/**
 * Asks if a link touched by a user should be opened in an external browser.
 *
 * If a user touches a link, the default behavior is to open the link in the Safari browser, 
 * which will cause your app to quit.  You may want to prevent this from happening, open the link
 * in your own internal browser, or perhaps warn the user that they are about to leave your app.
 * If so, implement this method on your delegate and return NO.  If you warn the user, you
 * should hold onto the URL and once you have received their acknowledgement open the URL yourself
 * using [[UIApplication sharedApplication] openURL:].
 */
- (BOOL)dialog:(FBDialog*)dialog shouldOpenURLInExternalBrowser:(NSURL*)url {
	return NO;
}



#pragma mark SA_OAuthTwitterController Delegate 

// following delegate methods are related with login process..
- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) user_name {
	NSLog(@"Authenticated with user %@", user_name);
	[self performSelectorOnMainThread:@selector(handleTWAuthSuccess) withObject:nil waitUntilDone:YES];

}

- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller {
	NSLog(@"Authentication Failure");
	[self performSelectorOnMainThread:@selector(handleTWAuthFailure) withObject:nil waitUntilDone:YES];
}

- (void) OAuthTwitterControllerCanceled: (SA_OAuthTwitterController *) controller {
	NSLog(@"Authentication Canceled");
	[self performSelectorOnMainThread:@selector(handleTWAuthCancelled) withObject:nil waitUntilDone:YES];
}

#pragma mark TwitterEngineDelegate

- (void) requestSucceeded: (NSString *) requestIdentifier {
	NSLog(@"Request %@ succeeded", requestIdentifier);
	[self performSelectorOnMainThread:@selector(handleShareNowCompletionWithTWSuccess) withObject:nil waitUntilDone:YES];
}

- (void) requestFailed: (NSString *) requestIdentifier withError: (NSError *) error {
	NSLog(@"Request %@ failed with error: %@", requestIdentifier, error);
	[self performSelectorOnMainThread:@selector(handleShareNowCompletionWithTWFailure) withObject:nil waitUntilDone:YES];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[alert release];
}


@end
