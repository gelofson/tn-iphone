//
//  ChangePictureViewController.h
//  QNavigator
//
//  Created by softprodigy on 01/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"MyPageViewController.h"


@interface ChangePictureViewController : UIViewController {
	UIImageView *m_thePictureView;
	
}
@property(nonatomic,retain) IBOutlet UIImageView *m_thePictureView;

-(IBAction) m_goToBackView;

@end
