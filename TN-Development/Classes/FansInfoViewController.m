//
//  FansInfoViewController.m
//  QNavigator
//
//  Created by softprodigy on 11/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FansInfoViewController.h"
#import"MyPageViewController.h"
#import"ShopbeeAPIs.h"
#import"QNavigatorAppDelegate.h"
#import"AsyncImageView.h"
#import"LoadingIndicatorView.h"
#import"ViewAFriendViewController.h"
#import "NewsDataManager.h"

@implementation FansInfoViewController

MyPageViewController *l_MyPageObj;
ShopbeeAPIs *l_requestObj;
QNavigatorAppDelegate *l_appDelegate;
LoadingIndicatorView *l_indicatorView;

@synthesize m_headerLabel;
@synthesize m_Label1;
@synthesize Follows;
@synthesize m_TableView;
@synthesize m_followArray;
@synthesize intUnfollowTrack;
@synthesize m_followerBtnClicked;
@synthesize m_FollowersTrack;

int UnfollowButtonTag;
-(IBAction)m_goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];
}
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/
-(void)viewDidAppear:(BOOL)animated
{
	
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[m_TableView setSeparatorColor:[UIColor clearColor]];
	if (Follows==1) {
        
        UIColor *color=[UIColor colorWithPatternImage:[UIImage imageNamed:@"followers_bar.png"]];
		m_Label1.text=@"    Bees are following you";
		//m_headerLabel.text=@"Followers";
         m_headerLabel.backgroundColor=color;
	}
	else if(Follows==2)
	{
        UIColor *color=[UIColor colorWithPatternImage:[UIImage imageNamed:@"following_bar.png"]];
		m_Label1.text=@"    Bees you are following";
		//m_headerLabel.text=@"Following";
        m_headerLabel.backgroundColor=color;
		
	}
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated
{
}


#pragma mark -
#pragma mark send request for unfollow
-(IBAction)UnFollowBtnAction:(id)sender
{
	NSString *mailId=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	UnfollowButtonTag=[sender tag];
	NSString *tmp_FollowingMail=[[m_followArray objectAtIndex:UnfollowButtonTag] valueForKey:@"email"];
	l_indicatorView=[LoadingIndicatorView SharedInstance];
	l_requestObj=[[ShopbeeAPIs alloc] init];
	[l_indicatorView startLoadingView:self];
	[l_requestObj unfollowAFriend:@selector(CallBackMethod:responseData:) tempTarget:self userid:mailId following:tmp_FollowingMail];
	
	[l_requestObj release];
	l_requestObj=nil;
	
	
}
#pragma mark -
#pragma mark call back methods
-(void)CallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_indicatorView stopLoadingView];
	
	if ([responseCode intValue]==200) 
	{
		NSLog(@"data downloaded");
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSLog(@"response string: %@",tempString);
		[tempString release];
		tempString=nil;
		[m_followArray removeObjectAtIndex:UnfollowButtonTag];
		[m_TableView reloadData];
		//[//self.navigationController popViewControllerAnimated:YES];	
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
		
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	
	
}
#pragma mark -
#pragma mark table view methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	static NSString *cellidentifier=@"cell";
	
	UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
	if (cell == nil) 
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier] autorelease];
	}
		if (m_FollowersTrack<2) 
		{
				
			//if (m_followerBtnClicked==YES)
//			{
				cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_without_arrow"]];
				
			//}
			//else 
//			{
//				cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_without_arrow.png"]];
//				
//			}
		}
		else 
		{
			cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_without_arrow"]];
		}

    UIImageView *lineImageView=[[UIImageView alloc]init];
    [lineImageView setFrame:CGRectMake(0, 58, 320, 1)];
    [lineImageView setImage:[UIImage imageNamed:@"grey_line.png"]];
    [cell.contentView addSubview:lineImageView];
    [lineImageView release];
    
    UIImageView *arrowImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"arrow.png"]];
    [arrowImageView setFrame:CGRectMake(275, 19, 20, 21)];
    [cell.contentView addSubview:arrowImageView];
    [arrowImageView release];

		
	cell.selectionStyle=UITableViewCellSelectionStyleNone;
	
	
	for(UIView *temp_view in cell.contentView.subviews)
	{
		if([temp_view isKindOfClass:[UILabel class]])
		{
			[temp_view removeFromSuperview];
		}
		
		else if([temp_view isKindOfClass:[UIButton class]])
		{
			[temp_view removeFromSuperview];
		}
	}
	AsyncImageView* asyncImage = [[[AsyncImageView alloc]
								   initWithFrame:CGRectMake(5,2,55,55)] autorelease];
	asyncImage.tag = 999;
	[cell.contentView addSubview:asyncImage];
		
	UILabel *temp_lblFriendName=[[UILabel alloc]init];
	temp_lblFriendName.frame=CGRectMake(70,17,165,25);// for setting the frame of the tableview cell
	temp_lblFriendName.font=[UIFont fontWithName:kFontName size:16];
    [temp_lblFriendName setTextColor:[UIColor blackColor]];
    //temp_lblFriendName.textColor=[UIColor colorWithRed:55/255.0 green:153/255.0 blue:200/255.0 alpha:1.0];
	//temp_lblFriendName.textColor=[UIColor colorWithRed:0.28f green:0.55f blue:0.60f alpha:1.0];
		if (![[[m_followArray objectAtIndex:indexPath.row] valueForKey:@"firstname"] isEqualToString:@""]) 
		{
			temp_lblFriendName.text=[[m_followArray objectAtIndex:indexPath.row] valueForKey:@"firstname"];
		}
		else {
			temp_lblFriendName.text=@"No Name";
		}

		[cell.contentView addSubview:temp_lblFriendName];
		[temp_lblFriendName release];
		temp_lblFriendName=nil; 
		
	if (Follows==2) 
	{
		if (intUnfollowTrack==0) 
		{
			UIButton *m_addButton=[UIButton buttonWithType:UIButtonTypeCustom];
			[m_addButton setImage:[UIImage imageNamed:@"unfollow_btn.png"] forState:UIControlStateNormal];
			m_addButton.tag =indexPath.row;
			[m_addButton addTarget:self action:@selector(UnFollowBtnAction:) forControlEvents:UIControlEventTouchUpInside];
			m_addButton.frame=CGRectMake(185,9,98,31);
			m_addButton.hidden = YES; // Hide unfollow, because unfollow can be done from the actual mypage friend page
			[cell.contentView addSubview:m_addButton];
		}
	}
	
	//NSDictionary *tempDict =[m_followArray objectAtIndex:indexPath.row];  
	NSString *Url=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_followArray objectAtIndex:indexPath.row] valueForKey:@"thumbImageUrl"]];
	NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
	AsyncImageView* asyncImage22 = (AsyncImageView*)[cell.contentView viewWithTag:999];
	[asyncImage22 loadImageFromURL:temp_loadingUrl];
	return cell;
}	

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
	return [m_followArray count];
		
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (m_FollowersTrack<2) 
	{
		//if (m_followerBtnClicked==YES) 
//		{
			ViewAFriendViewController *tmp_obj=[[ViewAFriendViewController alloc] init];//initWithNibName:@"ViewAFriendViewController" bundle:[NSBundle mainBundle]];
			tmp_obj.m_strLbl1 = [[m_followArray objectAtIndex:indexPath.row]valueForKey:@"firstname"];
			tmp_obj.m_Email=[[m_followArray objectAtIndex:indexPath.row]valueForKey:@"email"];
			tmp_obj.m_RequestTypeString=[[m_followArray objectAtIndex:indexPath.row] valueForKey:@"requestType"];
			tmp_obj.LevelTrackFriendsView=m_FollowersTrack;
			[tmp_obj.m_strLbl1 retain];
            tmp_obj.messageLayout = NO;
			[self.navigationController pushViewController:tmp_obj animated:YES];
			//m_FollowersTrack--;
			[tmp_obj release];
			tmp_obj=nil;
	//}
	}
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_headerLabel = nil;
    self.m_Label1 = nil;
    self.m_TableView = nil;
    self.m_followArray = nil;
}


- (void)dealloc {
    [m_headerLabel release];
	[m_Label1 release];
	[m_TableView release];
    [m_followArray release];

    [super dealloc];
}


@end
