//
//  BigSalesModelClass.m
//  QNavigator
//
//  Created by Soft Prodigy on 07/09/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BigSalesModelClass.h"

@implementation BigSalesModelClass

@synthesize m_strAdTitle;
@synthesize m_strDescription1;
@synthesize m_strDescription2;
@synthesize m_strLongitude;
@synthesize m_strLatitude;
@synthesize m_strAddress;
@synthesize m_strMallImageUrl;
@synthesize m_strAdUrl;
@synthesize m_imgAd;
@synthesize m_strId;

-(void)dealloc
{
    
	[m_strAdTitle release];
	[m_strDescription1 release];
	[m_strDescription2 release];
	[m_strLongitude release];
	[m_strLatitude release];
	[m_strMallImageUrl release];
	[m_strAddress release];
	[m_strAdUrl release];
	//[m_strId release];
	[m_imgAd release];
     
	[super dealloc];
}


@end
