//
//  TwitterFriendsViewController.h
//  QNavigator
//
//  Created by softprodigy on 29/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TwitterProcessing.h"
#import "SBJSON.h"
#import "JSON.h"

@interface TwitterFriendsViewController : UIViewController<UISearchBarDelegate,UITextViewDelegate,MFMailComposeViewControllerDelegate> {

	UITableView *table_AddFriends;
	NSArray *m_arrTwitterFriends;
	NSMutableArray *m_arrFrndsDic,*temp_arrFrndsDic;
	UISearchBar *searchB;
	NSMutableArray *arr_str;
	UIView *m_view;
	UIActivityIndicatorView *m_indicatorView;
	NSMutableString *m_strUids;
	NSMutableArray *m_shopBeeArray;
	NSMutableArray *m_nonshopBeeArray;
	int m_intCurrentTwitterId;
	TwitterProcessing *temp_twitterPross;
	NSArray *m_picUrls;
	UIView *m_dialog_view;
	NSArray *m_arrNonFrnds;
	NSMutableArray *m_arrFrnds;
	NSInteger tag_Val;
}

@property(nonatomic,readwrite)NSInteger tag_Val;
@property (nonatomic,retain)NSMutableArray *m_arrFrnds;
@property (nonatomic,retain) IBOutlet UIView *m_dialog_view;
@property (nonatomic,retain)NSArray *m_picUrls;
@property int m_intCurrentTwitterId;
@property (nonatomic,retain)NSMutableArray *m_shopBeeArray;
@property (nonatomic,retain)NSMutableArray *m_nonshopBeeArray;
@property(nonatomic,retain)IBOutlet UITableView *table_AddFriends;
@property(nonatomic,retain)	NSArray *m_arrTwitterFriends;
@property(nonatomic,retain)	NSMutableArray *m_arrFrndsDic;
@property(nonatomic,retain)IBOutlet UISearchBar *searchB;
@property(nonatomic,retain)NSMutableString *m_strUids;
@property(nonatomic,retain) TwitterProcessing *temp_twitterPross;

-(IBAction)btnBackAction:(id)sender;
-(IBAction) cancelButton:(id)sender;
-(void)dialogBoxForInvite:(id)sender;
-(void)sendAddFriendRequest;
-(void)hideProcessing;
-(void)load_tableViewDataSource;


@end
