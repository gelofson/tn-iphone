//
//  StreetMallMapViewController.h
//  QNavigator
//
//  Created by softprodigy on 04/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import <MapKit/MKReverseGeocoder.h>

@interface StreetMallMapViewController : UIViewController <MKReverseGeocoderDelegate>
{
	
	MKMapView *m_mapView;
	MKReverseGeocoder *m_geoCoder;
	MKPlacemark *m_placemark;
	
	UIImageView *m_imgViewMallMap;
	UIButton *m_btnStreetMap;
	UIButton *m_btnMallMap;
	
	
	UIActivityIndicatorView *m_activityIndicator;
	NSMutableData *m_mutResponseData;
	NSString *m_strPreviousUrl;
	BOOL m_isConnectionActive;
	
	BOOL m_isShowMallMapTab;
	
	
}

@property(nonatomic,retain) IBOutlet MKMapView *m_mapView;
@property(nonatomic,retain) MKReverseGeocoder *m_geoCoder;
@property(nonatomic,retain) MKPlacemark *m_placemark;
@property(nonatomic,retain) IBOutlet UIButton *m_btnStreetMap;
@property(nonatomic,retain) IBOutlet UIButton *m_btnMallMap;
@property(nonatomic,retain)IBOutlet UIImageView *m_imgViewMallMap;

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *m_activityIndicator;
@property (nonatomic, retain) NSMutableData *m_mutResponseData;
@property (nonatomic,retain) NSString *m_strPreviousUrl;
@property BOOL m_isConnectionActive;
@property BOOL m_isShowMallMapTab;

-(void)sendRequestToLoadImages:(NSString *)imageUrl;

-(IBAction)btnBackAction:(id)sender;
-(IBAction)btnStreetMapAction:(id)sender;
-(IBAction)btnMallMapAction:(id)sender;
-(void)sendRequestToCheckMallMap:(NSString *)imageUrl;



@end
