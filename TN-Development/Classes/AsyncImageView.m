//
//  AsyncImageView.m
//  Postcard
//
//  Created by markj on 2/18/09.
//  Copyright 2009 Mark Johnson. You have permission to copy parts of this code into your own projects for any use.
//  www.markj.net
//

#import "AsyncImageView.h"
#import "ImageCacheManager.h"


// This class demonstrates how the URL loading system can be used to make a UIView subclass
// that can download and display an image asynchronously so that the app doesn't block or freeze
// while the image is downloading. It works fine in a UITableView or other cases where there
// are multiple images being downloaded and displayed all at the same time. 

@implementation AsyncImageView

// GDL: We declared this property, so we need to synthesize it.
@synthesize imageView;
@synthesize delegate;
@synthesize urlString;
@synthesize connection;

- (void)dealloc {
	[connection cancel]; //in case the URL is still downloading
	[connection release];
	[imageView release];
    [delegate release];
	[data release]; 
    [urlString release];
    [super dealloc];
}

- (void)viewDidLoad
{
    self.urlString = nil;
}

- (void)loadImageFromURL:(NSURL*)url
{
    [self loadImageFromURL:url ignoreCaching:NO];
}

- (void)loadImageFromURL:(NSURL*)url  ignoreCaching:(BOOL)ignore 
{
    NSRange picActionRange = [[url absoluteString] rangeOfString:@"?action="];
    NSString *actionName = @"noaction";
    if (picActionRange.length != 0) {
        NSString *actionName = [[url absoluteString] substringFromIndex:picActionRange.location + picActionRange.length];
        NSRange picActionNameRange = [actionName rangeOfString:@"&"];
        actionName = [actionName substringToIndex:picActionNameRange.location];
    } 

    NSRange picid = [[url absoluteString] rangeOfString:@"picid="];
    NSString *prefix = @"";
    if (picid.length == 0) {
        picid = [[url absoluteString] rangeOfString:@"buzzimg="];
        if (picid.length == 0) {
            picid = [[url absoluteString] rangeOfString:@"advtid="];
            if (picid.length != 0)
                prefix = @"advtid";
        } else
            prefix = @"buzzimg";
    } else
        prefix = @"picid";

    UIImage *image = nil;
    if (picid.length > 0) {
        int endOfStr = picid.location + picid.length;
        NSString *urlStr = [url absoluteString];
        urlStr = [urlStr substringWithRange:NSMakeRange(endOfStr, [urlStr length] - endOfStr)];
        
        NSMutableCharacterSet *set = [[[NSMutableCharacterSet alloc] init] autorelease];
        
        [set formUnionWithCharacterSet:[NSCharacterSet decimalDigitCharacterSet]];
        [set invert];
        picid = [urlStr rangeOfCharacterFromSet:set];
        if (picid.length != 0) {
            self.urlString = [NSString stringWithFormat:@"%@-%@-%@.jpg", actionName, prefix, [urlStr substringToIndex:picid.location]];
        } else {
            self.urlString = [NSString stringWithFormat:@"%@-%@-%@.jpg", actionName, prefix, urlStr];
        }
        if (!ignore) {
            image = [[ImageCacheManager sharedManager] getImage:self.urlString];
        }
    } 
    
    
	if (data!=nil) { [data release]; data=nil; }
	
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	
	self.imageView = [[[UIImageView alloc] initWithImage:(image) ? image : [UIImage imageNamed:@"loading.png"]] autorelease];
	self.imageView.contentMode = UIViewContentModeScaleAspectFit;
	self.imageView.frame = self.bounds;
	[self.imageView setNeedsLayout];
	[self setNeedsLayout];
	[self addSubview:self.imageView];
	self.imageView.tag=101;
    if (!image) {
        NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        self.connection = [[[NSURLConnection alloc] initWithRequest:request delegate:self] autorelease]; //notice how delegate set to self object
    }
	//TODO error handling, what if connection is nil?
}

//the URL connection calls this repeatedly as data arrives
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	if (data==nil) { data = [[NSMutableData alloc] initWithCapacity:2048]; } 
	[data appendData:incrementalData];
}

//the URL connection calls this once all the data has downloaded
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
	//so self data now has the complete image 
	//if ([[self subviews] count]>0) {
    //		//then this must be another image, the old one is still in subviews
    //		[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
    //	}
	
	//make an image view for the image
	
	UIImage *tempImage=[UIImage imageWithData:data];
	if (tempImage==nil)
	{
		tempImage=[UIImage imageNamed:@"no-image"];
	} else {
        [[ImageCacheManager sharedManager] savePicture:data forName:self.urlString];
    }
	
	self.imageView.image=tempImage;
	
    if (self.delegate && [self.delegate respondsToSelector:@selector(setImage:)]) {
        [self.delegate performSelector:@selector(setImage:) withObject:tempImage];
    }
	self.connection = nil;
	[data release]; //don't need this any more, its in the UIImageView now
	data=nil;
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error
{
	[connection release];
	connection=nil;
	
	imageView.image=[UIImage imageNamed:@"no-image"];;
	
	
}


//just in case you want to get the image directly, here it is in subviews
- (UIImage*) image 
{
	UIImageView* iv = [[self subviews] objectAtIndex:0];
	return [iv image];
}

@end
