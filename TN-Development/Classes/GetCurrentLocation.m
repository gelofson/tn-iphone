//
//  GetCurrentLocation.m
//  QNavigator
//
//  Created by softprodigy on 14/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GetCurrentLocation.h"
#import "LoginViewController.h"
#import "QNavigatorAppDelegate.h"
#import "MyCLController.h"
#import "DBManager.h"
@implementation GetCurrentLocation

@synthesize m_locationManager;
@synthesize m_latitude;
@synthesize m_longitude;

QNavigatorAppDelegate *l_appDelegate;
BOOL l_showPrompt;
int l_intUpdateTimes;

- (void)initializeMembers
{
//l_appDelegate=(QNavigatorAppDelegate  *)[[UIApplication sharedApplication]delegate];
	
	if(!m_locationManager)
	{
		self.m_locationManager=[[CLLocationManager alloc]init];
		self.m_locationManager.distanceFilter = 5;
		self.m_locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
		self.m_locationManager.delegate = self; //
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
		[self.m_locationManager startUpdatingLocation];
		//l_showPrompt=TRUE;
		l_intUpdateTimes=1;
	}
	
}

#pragma mark -
#pragma mark Location Manager Methods

//Called when the location is updated
- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation
{
	
	//NSMutableString *temp_mutString=[[[NSMutableString alloc]init]autorelease];
	//[temp_mutString appendFormat:@"%f,%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude];
	
	self.m_latitude=newLocation.coordinate.latitude;
	self.m_longitude=newLocation.coordinate.longitude;
	
	
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	NSLog(@"GetCurrentLocation: fail to update location");
    if (error != nil) {
        NSString * descr = @""; 
        NSString * reason = @"";
        NSString * recovery = @"";
        if ([error localizedDescription] != nil)
            descr = [error localizedDescription];
        if ([error localizedFailureReason] != nil)
            reason = [error localizedFailureReason];
        if ([error localizedRecoverySuggestion] != nil)
            recovery = [error localizedRecoverySuggestion];

		
        if ([error domain] == kCLErrorDomain) {
			if ([error code] == kCLErrorDenied) {
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"\nAllow Location Services access to 'FashionGram' to receive deals in your area!\n\nTurn ON Location Services under Settings > General > FashionGram and re-launch application.\n\nWithout Location Services we will not be able to get you hot deals in your neighborhood." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				NSLog(@"GetCurrentLocation:didFailWithError:ERROR code=%d.Description=%@.Reason=%@.Recovery=%@",[error  code], descr, reason, recovery);
				[alert show]; [alert release];
			}
		}
		 
        NSLog(@"GetCurrentLocation:didFailWithError:ERROR code=%d.Description=%@.Reason=%@.Recovery=%@",[error  code], descr, reason, recovery);
    } else {
        NSLog(@"GetCurrentLocation:didFailWithError:ERROR");
    }
}


#pragma mark -
#pragma mark Login methods
-(void)showLoginScreenOnSessionExpire:(id)sender
{
		
//	LoginViewController *temp_loginScreen=[[LoginViewController alloc]init];
//		
//	temp_loginScreen.m_isHideCancelButton=YES;
//	
//	[l_appDelegate.m_customView setHidden:YES];
//	
//	
//	l_appDelegate.m_isDataReloadOnSessionExpire=YES;
//	
//	[sender presentModalViewController:temp_loginScreen animated:NO];
//	[temp_loginScreen release];
//	temp_loginScreen=nil;
    [tnApplication logout];
	
}

// GDL: Changed everything below.

#pragma mark - memory management
-(void)dealloc {
	[m_locationManager release];
    
	[super dealloc];
}

@end
