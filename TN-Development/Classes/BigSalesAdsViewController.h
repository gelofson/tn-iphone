//
//  BigSalesAdsViewController.h
//  QNavigator
//
//  Created by Soft Prodigy on 27/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNavigatorAppDelegate.h"
#import "CustomTopBarView.h"

@interface BigSalesAdsViewController : UIViewController <CategoryButtonsDelegate> //conforms to category button action delegate
{
	UIButton								*btnMoreInfo;
	UIImageView								*m_imgViewSaleAd;
	NSMutableData							*m_mutResponseData;
	UIActivityIndicatorView					*m_activityIndicator;
	int										m_intTypeOfResponse;
	UIImageView								*m_imgViewCurl;
	
	BOOL									m_isViewDidLoadCalled;
	
	BOOL									m_isConnectionFailed;
		
	BOOL									m_isViewTwiceLoaded;
	
	BOOL l_isConnectionActive;
	NSURLConnection  *l_theConnection;

	NSTimer *timerBSA;
	BOOL isReload;

    // GDL. For reloading the category ads every 10 minutes.
    NSTimer *reloadTimer;
}

@property BOOL l_isConnectionActive;
@property (nonatomic,retain) NSURLConnection *l_theConnection;

@property BOOL								m_isConnectionFailed;

@property BOOL								m_isViewDidLoadCalled;
@property BOOL								m_isViewTwiceLoaded;

@property(nonatomic,retain)	IBOutlet		UIButton *btnMoreInfo;
@property(nonatomic,retain)	IBOutlet		UIImageView *m_imgViewSaleAd;
@property(nonatomic,retain)	IBOutlet		UIActivityIndicatorView *m_activityIndicator;
@property(nonatomic,retain)	NSMutableData	*m_mutResponseData;
@property(nonatomic,retain)	IBOutlet		UIImageView *m_imgViewCurl;
@property int								m_intTypeOfResponse;

-(void)btnMoreInfoAction:(id)sender;
-(void)sendRequestForBigSalesAds;
-(void)restartImageLoadingOnNetworkFailure;
-(void)sendRequestToLoadImages:(NSString *)imageUrl;
-(void)switchLogosListingView;


@end
