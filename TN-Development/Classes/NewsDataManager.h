//
//  NewsDataManager.h
//  TinyNews
//
//  Created by Nava Carmon on 28/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CategoryData;

@interface NewsDataManager : NSObject <UIAlertViewDelegate>
{
    NSMutableArray *activeCategories;
    NSMutableArray *allActiveCategories;
    NSMutableArray *activeCategoriesWithData;
    NSMutableArray *allCategories;
    NSMutableArray *categoriesDataArray;
    NSOperationQueue   *getCategoriesQueue;
    NSInteger       requestedCategoriesCounter;
    NSMutableDictionary     *categoriesNewNames;
    NSString *alertMessage;
    NSInteger alertCode;
}

@property (nonatomic, retain) NSMutableDictionary  *categoriesNewNames;

+ (NewsDataManager *) sharedManager;
- (void) getActiveCategories:(id) delegateToUpdate;
- (void) addCategoryData:(NSDictionary *) categoryDict;
- (void) getAllCategoriesData:(id) delegateToUpdate filter:(int)filter number:(int)number numPage:(int)numPage  addToExisting:(BOOL)add;
- (NSString *) getCategoryIdAtIndex:(NSInteger) index;
- (NSInteger) countActiveCategories;
- (NSInteger) countAllCategories;
- (CategoryData *) getCategoryDataByIndex:(NSInteger)index;
- (void) getNotificationsCount:(id)delegateToUpdate;
- (CategoryData *) getCategoryDataByType:(NSString *)type;
-(void) getStoryByMessageId:(id)delegateToUpdate message:(int)messageId;
- (void) getOneCategoryData:(id) delegateToUpdate type:(NSString *)type filter:(int)filter number:(int)number numPage:(int)numPage addToExisting:(BOOL)add;
- (void) getAllCategories:(id) delegateToUpdate;
- (NSString *) getAllCategoryIdAtIndex:(NSInteger) index;
- (NSUInteger) indexOfCategoryByName:(NSString *)type;
- (BOOL)thereAreCategories;
- (void) cleanActiveCategories;
- (void) removeEmptyData;
- (void) removeProblematicCategory:(NSString *)categoryType delegate:(id)delegate;
- (NSUInteger) indexOfActiveCategoryByName:(NSString *)type;
- (void) checkCounter:(id)delegate;
- (NSString *) getCategoryNewName:(NSString *) oldName;

- (BOOL) showError:(NSError *)error;
- (void) showErrorByCode:(NSInteger)resultCode fromSource:(NSString *)source;

@end
