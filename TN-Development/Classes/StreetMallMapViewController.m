//
//  StreetMallMapViewController.m
//  QNavigator
//
//  Created by softprodigy on 04/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "StreetMallMapViewController.h"
#import "QNavigatorAppDelegate.h"
#import "AuditingClass.h"
#import "Constants.h"
#import "CategoryModelClass.h"
#import "BigSalesModelClass.h"
#import "ParkPlaceMark.h"


@implementation StreetMallMapViewController

@synthesize m_mapView;
@synthesize m_imgViewMallMap;
@synthesize m_geoCoder;
@synthesize m_placemark;
@synthesize m_btnMallMap;
@synthesize m_btnStreetMap;
@synthesize m_activityIndicator;
@synthesize m_mutResponseData;
@synthesize m_isConnectionActive;
@synthesize m_strPreviousUrl;
@synthesize m_isShowMallMapTab;

NSURLConnection *l_theConnection;

QNavigatorAppDelegate *l_appDelegate;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	m_mutResponseData=[[NSMutableData alloc]init];

	
	if (m_isShowMallMapTab==TRUE)
	{
		m_btnMallMap.hidden=NO;
		[m_btnStreetMap setUserInteractionEnabled:TRUE];
	
	}
	else 
	{
		[m_btnStreetMap setUserInteractionEnabled:FALSE];
		m_btnMallMap.hidden=YES;
		m_btnStreetMap.frame = CGRectMake(0, 45, 320, 32);
		[m_btnStreetMap setImage:[UIImage imageNamed:@"green_bar2.png"] forState:UIControlStateNormal];
		
		
		//m_btnStreetMap.backgroundColor = [UIColor clearColor];
//		m_btnStreetMap.titleLabel.text=@"Street";
	}

	m_btnStreetMap.selected=TRUE;
	[self performSelector:@selector(showStreetMap)];
	
}

-(void)viewWillAppear:(BOOL)animated
{
}

-(void)viewDidDisappear:(BOOL)animated
{
	NSLog(@"streetmall did disappear");
}

- (void)viewWillDisappear:(BOOL)animated
{
	if(m_isConnectionActive)
	{
		m_isConnectionActive=FALSE;
		[l_theConnection cancel];
		[m_activityIndicator stopAnimating];
		
	}
}

#pragma mark -
#pragma mark Custom Methods

-(IBAction)btnStreetMapAction:(id)sender
{
	if (m_btnStreetMap.selected==FALSE) {
		m_btnStreetMap.selected=TRUE;
		m_btnMallMap.selected=FALSE;
		
		m_mapView.hidden=NO;
		m_imgViewMallMap.hidden=YES;
		[m_activityIndicator stopAnimating];
		
		[self performSelector:@selector(showStreetMap)];
	}
	
		
	
}

-(IBAction)btnMallMapAction:(id)sender
{
	if (m_btnMallMap.selected==FALSE) {
	m_btnStreetMap.selected=FALSE;
	m_btnMallMap.selected=TRUE;
	
	m_mapView.hidden=YES;
	m_imgViewMallMap.hidden=NO;
	
	[self performSelector:@selector(showMallMap)];
	}
	
}

-(void)showStreetMap
{
	/*Region and Zoom*/
	MKCoordinateRegion temp_region;
	MKCoordinateSpan temp_span;
	temp_span.latitudeDelta=0.04;
	temp_span.longitudeDelta=0.04;
	
	CLLocationCoordinate2D temp_location=m_mapView.userLocation.coordinate;
	
	temp_location.latitude=0.0f;
	temp_location.longitude=0.0f;
	
		
	if(l_appDelegate.m_intCurrentView==1)//indy shop view
	{
		if(l_appDelegate.m_arrIndyShopData.count>0 && l_appDelegate.m_intIndyShopIndex!=-1)
		{
			CategoryModelClass *temp_objIndyShop=[l_appDelegate.m_arrIndyShopData objectAtIndex:l_appDelegate.m_intIndyShopIndex];
			
			
			//NSLog(@"temp_objIndyShop.m_strId %@",temp_objIndyShop.m_strId);

			//------------------------------------------------------------
			AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Navigate to street map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objIndyShop.m_strId,@"productId",nil];
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Navigate to street map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objIndyShop.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
				//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			//------------------------------------------------------------
			
			temp_location.latitude = [temp_objIndyShop.m_strLatitude floatValue]; //l_appDelegate.m_geoLatitude ;
			temp_location.longitude = [temp_objIndyShop.m_strLongitude floatValue]; //l_appDelegate.m_geoLongitude;
			ParkPlaceMark *placemark=[[ParkPlaceMark alloc] initWithCoordinate:temp_location];
			//placemark.titleStr=temp_objIndyShop.m_strAddress;
			NSArray * addressArray =[temp_objIndyShop.m_strAddress componentsSeparatedByString:@","];
			
			//NSLog(@"%@",addressArray);
			
			if ([addressArray count]>3) {
				
				placemark.titleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:0],[addressArray objectAtIndex:1]];
				placemark.subTitleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:2],[addressArray objectAtIndex:3]];
				
			}
			else if([addressArray count]>2){
				
				placemark.titleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:0],[addressArray objectAtIndex:1]];
				placemark.subTitleStr=[NSString stringWithFormat:@"%@",[addressArray objectAtIndex:2]];
								
			}
			else if([addressArray count]>1){
								
				placemark.titleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:0],[addressArray objectAtIndex:1]];
								
			}
			else if([addressArray count]>0){
				
				placemark.titleStr=[NSString stringWithFormat:@"%@",[addressArray objectAtIndex:0]];
				
			}
			[m_mapView addAnnotation:placemark];
			[placemark release];
			
			//NSLog(@"%@",temp_objIndyShop.m_strLatitude);
			//NSLog(@"%@",temp_objIndyShop.m_strLongitude);
		}
		else {
			
			//	UIAlertView *temp_indyAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please tap \"Look inside\" button to get the street map." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			//			[temp_indyAlert show];
			//			[temp_indyAlert release];
			//			temp_indyAlert=nil;
			
			temp_location.latitude = 0.0f; //l_appDelegate.m_geoLatitude ;
			temp_location.longitude = 0.0f;  //l_appDelegate.m_geoLongitude;
			
		}
	}
	else if(l_appDelegate.m_intCurrentView==2)//big sales view
	{
		if(l_appDelegate.m_arrBigSalesData.count>0 && l_appDelegate.m_intBigSalesIndex!=-1)
		{
			BigSalesModelClass *temp_objBigSales=[l_appDelegate.m_arrBigSalesData objectAtIndex:l_appDelegate.m_intBigSalesIndex];
			
			//------------------------------------------------------------
			AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Navigate to street map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objBigSales.m_strId,@"productId",nil];
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Navigate to street map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objBigSales.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
				//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			//------------------------------------------------------------
			
			temp_location.latitude = [temp_objBigSales.m_strLatitude floatValue]; //l_appDelegate.m_geoLatitude ;
			temp_location.longitude = [temp_objBigSales.m_strLongitude floatValue]; //l_appDelegate.m_geoLongitude;
			ParkPlaceMark *placemark=[[ParkPlaceMark alloc] initWithCoordinate:temp_location];
			
			NSArray * addressArray =[temp_objBigSales.m_strAddress componentsSeparatedByString:@","];
			
			//NSLog(@"%@",addressArray);
			
			if ([addressArray count]>3) {
				
				placemark.titleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:0],[addressArray objectAtIndex:1]];
				placemark.subTitleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:2],[addressArray objectAtIndex:3]];
				
			}
			else if([addressArray count]>2){
				
				placemark.titleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:0],[addressArray objectAtIndex:1]];
				placemark.subTitleStr=[NSString stringWithFormat:@"%@",[addressArray objectAtIndex:2]];
				
			}
			else if([addressArray count]>1){
				
				placemark.titleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:0],[addressArray objectAtIndex:1]];
		
			}
			else if([addressArray count]>0){
			
				placemark.titleStr=[NSString stringWithFormat:@"%@",[addressArray objectAtIndex:0]];
				
			}
			//placemark.titleStr=temp_objBigSales.m_strAddress;
		
			//placemark.customView.pinColor=MKPinAnnotationColorPurple;
			[m_mapView addAnnotation:placemark];
			
			[placemark release];
			//NSLog(@"%@",temp_objBigSales.m_strLatitude);
			//NSLog(@"%@",temp_objBigSales.m_strLongitude);
		}
		else {
			
			//UIAlertView *temp_indyAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please tap \"Look inside\" button to get the street map." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			//			[temp_indyAlert release];
			//			temp_indyAlert=nil;
			
			temp_location.latitude = 0.0f; //l_appDelegate.m_geoLatitude ;
			temp_location.longitude =0.0f;  //l_appDelegate.m_geoLongitude;
			
		}
	}
	else if(l_appDelegate.m_intCurrentView==3)//category sales views
	{
		//NSMutableArray *temp_arrCatData=[l_appDelegate.m_arrCategorySalesData objectAtIndex:l_appDelegate.m_intCategorySalesIndex];
		
		if(l_appDelegate.m_arrCategorySalesData.count>0 && l_appDelegate.m_intCategorySalesIndex!=-1)
		{
			CategoryModelClass *temp_objCatSales=[l_appDelegate.m_arrCategorySalesData objectAtIndex:l_appDelegate.m_intCategorySalesIndex];
	
			//------------------------------------------------------------
			AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Navigate to street map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objCatSales.m_strId,@"productId",nil];
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Navigate to street map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objCatSales.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
				//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			//------------------------------------------------------------
			
			temp_location.latitude = [temp_objCatSales.m_strLatitude floatValue]; //l_appDelegate.m_geoLatitude ;
			temp_location.longitude = [temp_objCatSales.m_strLongitude floatValue]; //l_appDelegate.m_geoLongitude;
			ParkPlaceMark *placemark=[[ParkPlaceMark alloc] initWithCoordinate:temp_location];
			
			NSArray * addressArray =[temp_objCatSales.m_strAddress componentsSeparatedByString:@","];
			
			//NSLog(@"%@",addressArray);
			
			if ([addressArray count]>3) {
				
				placemark.titleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:0],[addressArray objectAtIndex:1]];
				placemark.subTitleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:2],[addressArray objectAtIndex:3]];
				
			}
			else if([addressArray count]>2){
				
				placemark.titleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:0],[addressArray objectAtIndex:1]];
				placemark.subTitleStr=[NSString stringWithFormat:@"%@",[addressArray objectAtIndex:2]];
								
			}
			else if([addressArray count]>1){
				
				placemark.titleStr=[NSString stringWithFormat:@"%@,%@",[addressArray objectAtIndex:0],[addressArray objectAtIndex:1]];
								
			}
			else if([addressArray count]>0){
				
				placemark.titleStr=[NSString stringWithFormat:@"%@",[addressArray objectAtIndex:0]];
				
			}
			

			[m_mapView addAnnotation:placemark];
			
			[placemark release];
			
		}
		else {
			
		}
		
	}
	
	temp_region.span=temp_span;
	temp_region.center=temp_location;
	
	//[m_mapView setDelegate:self];
	[m_mapView setRegion:temp_region animated:TRUE];
	[m_mapView regionThatFits:temp_region];
	
}

-(void)checkForMallMapExistence
{
	if(m_isConnectionActive)
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		m_isConnectionActive=FALSE;
		[l_theConnection cancel];
	}
	
	//checks from which view user comes in mall map view
	if(l_appDelegate.m_intCurrentView==1)//1: represents indy shop view
	{
		if(l_appDelegate.m_arrIndyShopData.count>0 && l_appDelegate.m_intIndyShopIndex!=-1)
		{
			CategoryModelClass *temp_objIndyShop=[l_appDelegate.m_arrIndyShopData objectAtIndex:l_appDelegate.m_intIndyShopIndex];
			
			[self sendRequestToCheckMallMap:temp_objIndyShop.m_strMallImageUrl];
		}
			
	}
	else if(l_appDelegate.m_intCurrentView==2) //2: represents big sales ads view
	{
		if(l_appDelegate.m_arrBigSalesData.count>0 && l_appDelegate.m_intBigSalesIndex!=-1)
		{
			BigSalesModelClass *temp_objBigSales=[l_appDelegate.m_arrBigSalesData objectAtIndex:l_appDelegate.m_intBigSalesIndex];
				
			[self sendRequestToCheckMallMap:temp_objBigSales.m_strMallImageUrl];
			
		}
	}
	else if(l_appDelegate.m_intCurrentView==3) //represents other category sales view i.e. shoes, bags etc.
	{
		if(l_appDelegate.m_arrCategorySalesData.count > 0 && l_appDelegate.m_intCategorySalesIndex!=-1)
		{
			CategoryModelClass *temp_objCatSales=[l_appDelegate.m_arrCategorySalesData objectAtIndex:l_appDelegate.m_intCategorySalesIndex];
			
			[self sendRequestToCheckMallMap:temp_objCatSales.m_strMallImageUrl];			
		}
	}
}

-(void)sendRequestToCheckMallMap:(NSString *)imageUrl
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		
		//[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		//[m_activityIndicator startAnimating];
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/text; charset=utf-8", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		//if (m_isConnectionActive==TRUE)
//		{
//			[l_theConnection cancel];
//		}
//		
		//l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		NSURLResponse *temp_response;
		NSError *temp_error;
		NSData *temp_data= [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&temp_response error:&temp_error];
		
		NSString *text = [[NSString alloc] initWithData:temp_data encoding:NSUTF8StringEncoding];
		NSLog(@"mall map data: %@", text);
		
		if(temp_data != nil && temp_response != NULL )//&& [response statusCode] >= 200 && [response statusCode] < 300)
		{
			NSLog(@"inside mall map block");
			
		}
		[text release];
		//[l_theConnection start];
		///m_isConnectionActive=TRUE;
		
//		if(l_theConnection)
//		{
//			NSLog(@"Request sent to get data");
//		}	
	}
	else
	{
		m_isConnectionActive=FALSE;
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		
	}
}

-(void)showMallMap
{
	m_imgViewMallMap.image=[UIImage imageNamed:@""];
	
	if(m_isConnectionActive)
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		m_isConnectionActive=FALSE;
		[l_theConnection cancel];
	}
	
	//checks from which view user comes in mall map view
	if(l_appDelegate.m_intCurrentView==1)//1: represents indy shop view
	{
		if(l_appDelegate.m_arrIndyShopData.count>0 && l_appDelegate.m_intIndyShopIndex!=-1)
		{
			CategoryModelClass *temp_objIndyShop=[l_appDelegate.m_arrIndyShopData objectAtIndex:l_appDelegate.m_intIndyShopIndex];
			
			//------------------------------------------------------------
			AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Navigate to indoor map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objIndyShop.m_strId,@"productId",nil];
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Navigate to indoor map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objIndyShop.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
				//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			//------------------------------------------------------------
			
			[self sendRequestToLoadImages:temp_objIndyShop.m_strMallImageUrl];
		}
		else {
			UIAlertView *temp_indyAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please tap \"Look inside\" button to get the indoor map." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[temp_indyAlert show];
			[temp_indyAlert release];
			temp_indyAlert=nil;
		}
		
	}
	else if(l_appDelegate.m_intCurrentView==2) //2: represents big sales ads view
	{
		if(l_appDelegate.m_arrBigSalesData.count>0 && l_appDelegate.m_intBigSalesIndex!=-1)
		{
			BigSalesModelClass *temp_objBigSales=[l_appDelegate.m_arrBigSalesData objectAtIndex:l_appDelegate.m_intBigSalesIndex];
			
			//------------------------------------------------------------
			AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Navigate to indoor map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objBigSales.m_strId,@"productId",nil];
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Navigate to indoor map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objBigSales.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
				//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			//------------------------------------------------------------
			
			[self sendRequestToLoadImages:temp_objBigSales.m_strMallImageUrl];
	
		}
	}
	else if(l_appDelegate.m_intCurrentView==3) //represents other category sales view i.e. shoes, bags etc.
	{
		if(l_appDelegate.m_arrCategorySalesData.count > 0 && l_appDelegate.m_intCategorySalesIndex!=-1)
		{
			CategoryModelClass *temp_objCatSales=[l_appDelegate.m_arrCategorySalesData objectAtIndex:l_appDelegate.m_intCategorySalesIndex];
			
			//------------------------------------------------------------
			AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Navigate to indoor map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objCatSales.m_strId,@"productId",nil];
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Navigate to indoor map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objCatSales.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			//------------------------------------------------------------
			
			[self sendRequestToLoadImages:temp_objCatSales.m_strMallImageUrl];			
		}
	}
}

-(IBAction)btnBackAction:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
	
}


#pragma mark -
#pragma mark MKMapView delegate methods

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
	MKPinAnnotationView *annView=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
	annView.animatesDrop=TRUE;
	annView.canShowCallout=YES;
	return [annView autorelease];
	
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark
{
	NSLog(@"Reverse Geocoder completed");
	@try 
	{
		[m_mapView removeAnnotation:m_placemark];
		[m_mapView addAnnotation:placemark];
		m_placemark=placemark;
	}
	@catch (NSException * e) 
	{
		[m_mapView addAnnotation:placemark];
		m_placemark=placemark;
		NSLog(@"Exception occurred");
	}
		
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error
{
	//NSLog(@"%@",[error userInfo]);
	[m_geoCoder cancel];
}


-(void)sendRequestToLoadImages:(NSString *)imageUrl
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		[m_activityIndicator startAnimating];
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/text; charset=utf-8", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if (m_isConnectionActive==TRUE)
		{
			[l_theConnection cancel];
		}
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		[l_theConnection start];
		m_isConnectionActive=TRUE;
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		m_isConnectionActive=FALSE;
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		
	}
}

#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[m_mutResponseData setLength:0];	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[m_mutResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	m_isConnectionActive=FALSE;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	if(m_mutResponseData!=nil)
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		m_imgViewMallMap.image=[UIImage imageWithData:[NSData dataWithData:m_mutResponseData]];
	}
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	m_isConnectionActive=FALSE;
	
	UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[networkDownAlert show];
	[networkDownAlert release];
	networkDownAlert=nil;
	
	// inform the user
    //NSLog(@"Connection failed! Error - %@ %@",[error localizedDescription],[[error userInfo] objectForKey:NSErrorFailingURLStringKey]);
	//NSLog(@"Exit :didFailWithError");
	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets
    self.m_mapView = nil;
    self.m_btnStreetMap = nil;
    self.m_btnMallMap = nil;
    self.m_imgViewMallMap = nil;
    self.m_activityIndicator = nil;
}

- (void)dealloc {
    
    [m_mapView release];
    [m_geoCoder release];
    [m_placemark release];
    [m_imgViewMallMap release];
    [m_btnStreetMap release];
    [m_btnMallMap release];
    [m_activityIndicator release];
    [m_mutResponseData release];
    [m_strPreviousUrl release];
	
    [super dealloc];
}

@end
