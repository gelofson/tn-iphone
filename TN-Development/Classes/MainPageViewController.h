//
//  MainPageViewController.h
//  TinyNews
//
//  Created by Nava Carmon on 5/10/13.
//
//

#import <UIKit/UIKit.h>
#import "ContentViewController.h"

@class NewsMoreViewController;

@interface MainPageViewController : ContentViewController <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UITableView *categoriesTable;
    NSMutableArray *tableCells;
    NewsMoreViewController *moreController;
}

@property (nonatomic, retain) IBOutlet UITableView *categoriesTable;
@property (retain,nonatomic) NSMutableArray *tableCells;//**
@property(retain,nonatomic)NewsMoreViewController *moreController;//**

- (IBAction)makeStory:(id)sender;
- (IBAction)findFriends:(id)sender;
- (IBAction)openNotifications:(id)sender;
- (IBAction)openNewsMap:(id)sender;
- (IBAction)openMyPage:(id)sender;
- (IBAction)feedback:(id)sender;
- (IBAction)showSettingViewController;
- (void) updateCategories;
- (void) updateData;
@end
