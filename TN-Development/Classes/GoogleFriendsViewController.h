//
//  GoogleFriendsViewController.h
//  QNavigator
//
//  Created by softprodigy on 11/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GData.h"

#import "GDataFeedContact.h"
#import "JSON.h"



@interface GoogleFriendsViewController : UIViewController {
	
	IBOutlet UITextField *mUsernameField;
	IBOutlet UITextField *mPasswordField;
	
	IBOutlet UIButton *mShowDeletedCheckbox;
	IBOutlet UIButton *mMyContactsCheckbox;
	IBOutlet UIPickerView *mPropertyNameField;
	IBOutlet UIButton *mGetContactsButton;
	
	IBOutlet UISegmentedControl *mFeedSegmentedControl;
	IBOutlet UITableView *mFeedTable;
	IBOutlet UIProgressView *mFeedProgressIndicator;
	IBOutlet UITextView *mFeedResultTextField;
	IBOutlet UIButton *mFeedCancelButton;
	IBOutlet UIButton *mSortFeedCheckbox;
	
	IBOutlet UIImageView *mContactImageView;
	IBOutlet UIButton *mSetContactImageButton;
	IBOutlet UIButton *mDeleteContactImageButton;
	IBOutlet UIProgressView *mSetContactImageProgressIndicator;
	
	IBOutlet UIButton *mAddContactButton;
	IBOutlet UITextField *mAddTitleField;
	IBOutlet UITextField *mAddEmailField;
	IBOutlet UIButton *mDeleteContactButton;
	IBOutlet UIButton *mDeleteAllButton;
	
	IBOutlet UISegmentedControl *mEntrySegmentedControl;
	
	IBOutlet UITableView *mEntryTable;
	IBOutlet UITextView *mEntryResultTextField;
	
	IBOutlet UIButton *mAddEntryButton;
	IBOutlet UIButton *mDeleteEntryButton;
	IBOutlet UIButton *mEditEntryButton;
	IBOutlet UIButton *mMakePrimaryEntryButton;
	
	IBOutlet UITextField *mServiceURLField; 
	
	
	GDataServiceTicket *mContactFetchTicket;
	NSError *mContactFetchError;
	GDataFeedContact *mContactFeed;
	NSString *mContactImageETag;
	
	GDataFeedContactGroup *mGroupFeed;
	GDataServiceTicket *mGroupFetchTicket;
	NSError *mGroupFetchError;
	
	UIImageView *m_imgView;
	UIButton *m_rememberMeButton ;
	BOOL		isRememberMeActivated;
	
	NSMutableString *m_strEmailIds;
	UIView *m_view;
	UIActivityIndicatorView *m_indicatorView;
	NSMutableArray *temp_Array;
	NSMutableArray *m_nonshopBeeArray;
	NSMutableArray *m_shopBeeArray;
	IBOutlet UIImageView *m_logo;
}
@property (nonatomic,retain)NSMutableArray *m_shopBeeArray;
@property (nonatomic,retain)NSMutableArray *m_nonshopBeeArray;
@property (nonatomic,retain)NSMutableArray *temp_Array;
@property (nonatomic,retain)UIActivityIndicatorView *m_indicatorView;
@property (nonatomic,retain) UIView *m_view;
@property (nonatomic,retain) NSMutableString *m_strEmailIds;
@property (nonatomic,retain) IBOutlet UIImageView *m_imgView;

@property (nonatomic, retain) UIButton *m_rememberMeButton ;
@property(nonatomic,retain)IBOutlet UIImageView *m_logo;
-(IBAction) m_goToBackView;
- (IBAction)loginButtonAction:(id)sender;
-(GDataServiceGoogleContact *)contactService;
- (void)setContactFeed:(GDataFeedContact *)feed;
- (void)setContactFetchError:(NSError *)error;
- (void)setContactFetchTicket:(GDataServiceTicket *)ticket;
- (void)setContactFetchTicket:(GDataServiceTicket *)ticket;
- (void)fetchAllContacts;

// GDL: Not implemented any longer, so I'll comment it out.
//- (void)addProgressView;

- (void)fetchAllContacts;


@end
