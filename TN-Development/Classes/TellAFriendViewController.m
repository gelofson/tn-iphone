//
//  TellAFriendViewController.m
//  QNavigator
//
//  Created by softprodigy on 04/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TellAFriendViewController.h"
#import "QNavigatorAppDelegate.h"
#import "AuditingClass.h"
#import "Constants.h"

QNavigatorAppDelegate *l_appDelegate;

@implementation TellAFriendViewController
@synthesize m_btnTellAFriend;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
}


-(void)viewWillAppear:(BOOL)animated
{
	[self performSelector:@selector(showPicker)];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

-(IBAction)btnTellAFriendAction:(id)sender
{
	//[l_appDelegate.tabBarController setSelectedIndex:0];
	[self performSelector:@selector(showPicker)];
}


-(void)showPicker
{
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self performSelector:@selector(displayComposerSheet)];	
		}
		else
		{
			[self performSelector:@selector(launchMailAppOnDevice)];
		}
	}
	else
	{
		[self performSelector:@selector(launchMailAppOnDevice)];
	}
}
// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	//NSString *email;
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	NSMutableArray *toRecipients=[[NSMutableArray alloc]init];
	//toRecipients=[NSString stringWithFormat:@""];
	[picker setSubject:kTellAFriendMailSubject];
	
	
	NSString *emailBody;
	emailBody=kTellAFriendMailBody;
	//UIImage *logoPic = [UIImage imageNamed:@"logo.png"];
//	NSData *imageData = UIImageJPEGRepresentation(logoPic, 1);
//	[picker addAttachmentData:imageData mimeType:@"image/jpg" fileName:@"logo.png"];
//	
	[picker setToRecipients:toRecipients];
	[toRecipients release];
	toRecipients=nil;
	
	[picker setMessageBody:emailBody isHTML:NO];
	[self presentModalViewController:picker animated:YES];
	
    [picker release];
	picker=nil;
	
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	AuditingClass *temp_objAudit;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:

		//------------------------------------------------------------
			
			temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Navigate to indoor map",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objIndyShop.m_strId,@"productId",nil];
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Tell a Friend about FashionGram",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-2",@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
				//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			//------------------------------------------------------------
			
			
			
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	
	self.navigationItem.rightBarButtonItem.enabled=NO;
	//m_lblRecipient.text=@"";
	//m_lblRecipient.hidden=TRUE;
	[self dismissModalViewControllerAnimated:YES];
	
}

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=";
	NSString *body = @"&body=";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];

    // Release retained IBOutlets.
    self.m_btnTellAFriend = nil;
}

- (void)dealloc {
    [m_btnTellAFriend release];
    
    [super dealloc];
}


@end
