//
//  IMFollowingViewController.h
//  TinyNews
//
//  Created by jay kumar on 5/26/13.
//
//

#import <UIKit/UIKit.h>
#import "UIGridView.h"
#import "UIGridViewDelegate.h"
@interface IMFollowingViewController : UIViewController
{
    UIView *footerView;
    NSMutableArray *m_followArray;
    NSInteger m_FollowersTrack;
    
}
@property(nonatomic,retain)NSMutableArray *m_followArray;
@property(nonatomic,retain)UIView *footerView;
@property (nonatomic, retain) IBOutlet UIGridView *table;
@property NSInteger m_FollowersTrack;

-(IBAction)backAction:(id)sender;
-(IBAction)moreclicked:(id)sender;
@end
