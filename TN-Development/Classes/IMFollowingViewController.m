//
//  IMFollowingViewController.m
//  TinyNews
//
//  Created by jay kumar on 5/26/13.
//
//

#import "IMFollowingViewController.h"
#import "Cell.h"
#import"ShopbeeAPIs.h"
#import"QNavigatorAppDelegate.h"
#import"AsyncImageView.h"
#import"LoadingIndicatorView.h"
#import"ViewAFriendViewController.h"
#import "NewsDataManager.h"
#import "NewCell.h"
@interface IMFollowingViewController ()

@end

@implementation IMFollowingViewController
@synthesize footerView;
@synthesize table;
@synthesize m_followArray;
@synthesize m_FollowersTrack;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


#pragma mark- Custom Button
-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)moreclicked:(id)sender
{
    
}

#pragma UIGridViewDelegate

- (UIView *) gridFooterView
{
    if (!self.footerView) {
        self.footerView = [[[UIView alloc] initWithFrame:CGRectMake(0., 0., 320, 30)] autorelease];
        
    }
    return  self.footerView;
}



- (CGFloat) gridView:(UIGridView *)grid widthForColumnAt:(int)columnIndex
{
	return 80;
}

- (CGFloat) gridView:(UIGridView *)grid heightForRowAt:(int)rowIndex
{
	return 115;
}

- (NSInteger) numberOfColumnsOfGridView:(UIGridView *) grid
{
	return 4;
}


- (NSInteger) numberOfCellsOfGridView:(UIGridView *) grid
{
	return [m_followArray count];
}

- (UIGridViewCell *) gridView:(UIGridView *)grid cellForRowAt:(int)rowIndex AndColumnAt:(int)columnIndex
{
	NewCell *cell = (NewCell *)[grid dequeueReusableCell];
	
	if (cell == nil) {
		cell = [[[NewCell alloc] init] autorelease];
	}
	
    NSInteger indexInArray = rowIndex * 4 + columnIndex;
	
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_followArray   objectAtIndex:indexInArray]valueForKey:@"thumbImageUrl"]];
    cell.headline.text= [[m_followArray objectAtIndex:indexInArray]valueForKey:@"firstname"];
    cell.thumbnail.messageTag = indexInArray+1;
    cell.thumbnail.cropImage = YES;
    [cell.thumbnail loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked:) btnText:[[m_followArray objectAtIndex:indexInArray]valueForKey:@"email"]];
    
	return cell;
}

- (void) ProductBtnClicked:(id)sender
{
    int tag=((UIButton *)sender).tag-1;
    if (m_FollowersTrack<2)
    {
        ViewAFriendViewController *tmp_obj=[[ViewAFriendViewController alloc] init];//initWithNibName:@"ViewAFriendViewController" bundle:[NSBundle mainBundle]];
        tmp_obj.m_strLbl1 = [[m_followArray objectAtIndex:tag]valueForKey:@"firstname"];
        
        tmp_obj.m_Email=[[m_followArray objectAtIndex:tag]valueForKey:@"email"];
        tmp_obj.m_RequestTypeString=[[m_followArray objectAtIndex:tag] valueForKey:@"requestType"];
        [tmp_obj.m_strLbl1 retain];
        tmp_obj.LevelTrackFriendsView=m_FollowersTrack;
        tmp_obj.messageLayout = NO;
        [self.navigationController pushViewController:tmp_obj animated:YES];
        [tmp_obj release];
        tmp_obj=nil;
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [footerView release];
    [table release];
    [m_followArray release];
    [super dealloc];
}


@end
