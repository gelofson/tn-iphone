//
//  ShareSalesWithFriends.h
//  QNavigator
//
//  Created by softprodigy on 04/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<MessageUI/MessageUI.h>

@class CategoryModelClass;
@class BigSalesModelClass;
@interface ShareSalesWithFriends : UIViewController<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate>
{
	
	UISearchBar *m_searchBar;
	NSMutableArray *m_friendsArray;
	NSString *m_email;
	UITableView *m_table;
	//UIImage *m_image;
	//UIImage *share_image;
	//UIImageView *temp_imageView;
	UIButton *temp_btnShare;
	NSString *m_name;
	NSMutableString *m_personName;
	NSArray *m_arrData;
	NSMutableArray *m_backup;


	CategoryModelClass * m_catObjModel; 
	BigSalesModelClass * m_bigSalesModel; 

}
-(IBAction)m_goToBackView;
-(IBAction)FilterFriends;
@property(nonatomic,retain) IBOutlet UISearchBar *m_searchBar;
@property (nonatomic,retain) NSMutableArray *m_friendsArray;
@property (nonatomic,retain) IBOutlet UIView *m_shareView;
@property(nonatomic,retain) NSString *m_email;
@property(nonatomic,retain) IBOutlet  UITableView *m_table;
@property(nonatomic,retain) CategoryModelClass *m_catObjModel;
@property(nonatomic,retain) BigSalesModelClass *m_bigSalesModel;

@end


