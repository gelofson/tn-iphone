//
//  NewsDataManager.m
//  TinyNews
//
//  Created by Nava Carmon on 28/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NewsDataManager.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "JSON.h"
#import "CategoryRequest.h"
#import "CategoryData.h"

@interface NewsDataManager ()

- (BOOL) showError:(NSError *)error;
- (void) processRequest:(CategoryRequest *) request;

@end

@implementation NewsDataManager

@synthesize categoriesNewNames;

static NewsDataManager * manager = nil;

- (id) init 
{
	self = [super init];
    if (self) {
        allCategories = [[NSMutableArray alloc] init];
        activeCategories = [[NSMutableArray alloc] init];
        allActiveCategories = [[NSMutableArray alloc] init];
        activeCategoriesWithData = [[NSMutableArray alloc] init];
        getCategoriesQueue = [[NSOperationQueue alloc] init];
        categoriesDataArray = [[NSMutableArray alloc] init];
        categoriesNewNames = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                              @"Trending news", @"Breaking News",
                              @"Politics", @"Local Politics",
                              @"LOL", @"Funny",
                              @"The Green Earth", @"The Green Earth",
                              @"Sports", @"Sports",
                              @"International affairs", @"Politics and Insurrection",
                              @"Business", @"Trembling Economies",
                              @"Just Fun", @"What To Do",
                              @"Buzzworthy", @"Buzzworthy",
                              @"Fashion", @"Fashion",
                              @"Best Food n Drinks", @"Food n Drink",
                              @"Entertainment", @"Theatre-Arts",
                              @"Science n Tech", @"Tech",
                              @"Events", @"Events",
                              @"Nightlife", @"Movies",
                              @"Comics", @"BuyItOrNot",
                              @"Weather", @"IBoughtIt",
                              @"The Turnip", @"FoundASale",
                              @"Travel", @"How do I Look?",
                              nil];
        
        [self getAllCategories:nil];
    }
	return self;
}


+ (NewsDataManager *) sharedManager 
{
    @synchronized(self) 
	{
        if (manager == nil) 
		{
            manager = [[self alloc] init]; // assignment not done here
		}
    }
    return manager;
}

//Categories Global=====================================================================================

- (void) onGetActiveCategories:(NSURLResponse *)response data:(NSData *)responseData error:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ([self showError:error]) {
        return;
    }
	
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	int responseCode = [httpResponse statusCode];
    
	if (responseCode == 200) {
        
        NSString *tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];	
        
        if (activeCategories) {
            [activeCategories removeAllObjects];
            [activeCategoriesWithData removeAllObjects];
            [allActiveCategories removeAllObjects];
        }
        
        [activeCategories addObjectsFromArray:[tempString JSONValue]];
        NSUInteger qIndex = [activeCategories indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary *dict = (NSDictionary *)obj;
            
            return [[dict objectForKey:@"id"] isEqualToString:@"Q"];
        }];
        
        if (qIndex != NSNotFound) {
            [activeCategories removeObjectAtIndex:qIndex];
        }
        [activeCategoriesWithData addObjectsFromArray:activeCategories];
        [allActiveCategories addObjectsFromArray:activeCategories];
        NSMutableArray *categoriesToSave = [NSMutableArray array];
        NSLog(@"activeCategories - %@", activeCategoriesWithData);
        [categoriesNewNames enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSString *oldName = (NSString *)key;
            
            NSInteger index = [self indexOfActiveCategoryByName:oldName];
            if (index != NSNotFound) {
                [categoriesToSave addObject:[activeCategories objectAtIndex:index]];
            }
        }];
        [activeCategoriesWithData removeAllObjects];
        [allActiveCategories removeAllObjects];
                                            
        [activeCategoriesWithData addObjectsFromArray:categoriesToSave];
        [allActiveCategories addObjectsFromArray:categoriesToSave];
        [tempString release];
        [self getAllCategoriesData:self filter:POPULAR number:3 numPage:1 addToExisting:NO];
    } else if(responseCode==401) { // In case the session expires we have to make the user login again.
        [self showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }

}

- (void) getActiveCategories:(id) delegateToUpdate
{
    alertCode = 0;
	[tnApplication CheckInternetConnection];
	if(tnApplication.m_internetWorking==0)//0: internet working
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
        temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getAllMessageTypeActiveCategories",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
        [NSURLConnection sendAsynchronousRequest:theRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *jsonError) {
            [self onGetActiveCategories:response data:data error:jsonError];
            if ([activeCategories count] > 0 && [delegateToUpdate respondsToSelector:@selector(updateCategories)]) {
                [delegateToUpdate performSelector:@selector(updateCategories)];
            }
        }];
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}	
}

- (void) onGetAllCategories:(NSURLResponse *)response data:(NSData *)responseData error:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ([self showError:error]) {
        return;
    }
	
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	int responseCode = [httpResponse statusCode];
    
	if (responseCode == 200) {
        
        NSString *tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];	
        
        NSArray *array;
        if (allCategories) {
            [allCategories removeAllObjects];
        }
        array = [tempString JSONValue];
        
        NSIndexSet *indexSet = [array indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary *dict = (NSDictionary *)obj;
            
            return [[dict objectForKey:@"id"] isEqualToString:@"Q"] || [[dict objectForKey:@"global"] intValue] == 2;
        }];
        
        
        [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if (![indexSet containsIndex:idx]) {
                [allCategories addObject:obj];
            }
        }];
        
        [tempString release];
    } else if(responseCode==401) { // In case the session expires we have to make the user login again.
        [self showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }

}

- (void) getAllCategories:(id) delegateToUpdate
{
    if (!tnApplication.isLogged) {
        return;
    }
    
    alertCode = 0;
	[tnApplication CheckInternetConnection];
	if(tnApplication.m_internetWorking==0)//0: internet working
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
        temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getAllMessageTypeCategories",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
        NSURLResponse *response = nil;
        NSError *error = nil;
        
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&error];
        [self onGetAllCategories:response data:data error:error];
        if (delegateToUpdate && [allCategories count] > 0 && [delegateToUpdate respondsToSelector:@selector(updateAllCategories)]) {
            [delegateToUpdate performSelector:@selector(updateAllCategories)];
        }
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}	
}

- (void) removeProblematicCategory:(NSString *)categoryType delegate:(id)delegate
{
    NSInteger index = [self indexOfActiveCategoryByName:categoryType];
    if (index >= 0 && index != NSNotFound)
        [activeCategories removeObjectAtIndex:index];
}

- (void) cleanActiveCategories
{
    [activeCategories removeAllObjects];
}

- (BOOL) thereAreCategories
{
    return activeCategoriesWithData && [activeCategoriesWithData count];
}

- (BOOL) thereAreAllCategories
{
    return allCategories && [allCategories count];
}

- (NSInteger) countActiveCategories
{
    if ([self thereAreCategories]) {
        return [activeCategoriesWithData count];
    }
    return 0;
}

- (NSInteger) countAllCategories
{
    if ([self thereAreAllCategories]) {
        return [allCategories count];
    }
    return 0;
}

- (NSString *) getCategoryIdAtIndex:(NSInteger) index
{
    NSString *str =  ([self thereAreCategories] && index < [activeCategoriesWithData count]) ? [[activeCategoriesWithData objectAtIndex:index] objectForKey:@"id"] : @"";
    return str;
}

- (NSString *) getCategoryNewName:(NSString *) oldName
{
    if ([self.categoriesNewNames objectForKey:oldName]) {
        return [self.categoriesNewNames objectForKey:oldName];
    }
    
    return nil;
}

- (NSString *) getAllCategoryIdAtIndex:(NSInteger) index
{
    NSString *str =  ([self thereAreAllCategories] && index < [allCategories count]) ? [[allCategories objectAtIndex:index] objectForKey:@"id"] : @"";
    return str;
}

- (NSUInteger) indexOfCategoryByName:(NSString *)type
{
    NSUInteger newIndex = [allCategories indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *data = (NSDictionary *)obj;
        
        return [[data objectForKey:@"id"] isEqualToString:type];
        
    }];
    
    if (newIndex != NSNotFound) {
        return newIndex;
    }
    return NSNotFound;
}

- (NSUInteger) indexOfActiveCategoryByName:(NSString *)type
{
    NSUInteger newIndex = [activeCategories indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *data = (NSDictionary *)obj;
        
        return [[data objectForKey:@"id"] isEqualToString:type];
        
    }];
    
    if (newIndex != NSNotFound) {
        return newIndex;
    }
    return NSNotFound;
}


//Categories Specific=====================================================================================
- (void) getOneCategoryData:(id) delegateToUpdate type:(NSString *)type filter:(int)filter number:(int)number numPage:(int)numPage addToExisting:(BOOL)add
{
    alertCode = 0;
    NSDictionary *dataDict = [NSDictionary dictionaryWithObjectsAndKeys: type, @"type",
                              [NSNumber numberWithInt:number], @"number",
                              [NSNumber numberWithInt:numPage], @"numPage",
                              [NSNumber numberWithInt:filter], @"filter",
                              delegateToUpdate, @"delegate",
                              [NSNumber numberWithBool:YES], @"oneCategory",
                              [NSNumber numberWithBool:add], @"operation", nil];
    CategoryRequest *request = [[[CategoryRequest alloc] initWithData:dataDict] autorelease];
    [self processRequest:request];
}

- (void) getAllCategoriesData:(id) delegateToUpdate filter:(int)filter number:(int)number numPage:(int)numPage  addToExisting:(BOOL)add
{
    if (!add && [categoriesDataArray count] > 0) {
        [categoriesDataArray removeAllObjects];
    }
    
    if (activeCategories) {
        [activeCategories removeAllObjects];
        [activeCategories addObjectsFromArray:allActiveCategories];
    }
    
    requestedCategoriesCounter = 0;
    
    [activeCategories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *dict = (NSDictionary *) obj;
        NSDictionary *dataDict = [NSDictionary dictionaryWithObjectsAndKeys: [dict objectForKey:@"id"], @"type",
                                                                            [NSNumber numberWithInt:number], @"number",
                                                                            [NSNumber numberWithInt:numPage], @"numPage",
                                                                            [NSNumber numberWithInt:filter], @"filter",
                                                                            delegateToUpdate, @"delegate", 
                                                                            [NSNumber numberWithBool:add], @"operation",
                                                                        [NSNumber numberWithBool:NO], @"oneCategory", nil];
        CategoryRequest *request = [[[CategoryRequest alloc] initWithData:dataDict] autorelease];
        [self processRequest:request];
    }];
}

- (void) addCategoryData:(NSDictionary *) categoryDict
{
    @synchronized (self) {
        CategoryData *aData = [[CategoryData alloc] initWithData:[categoryDict objectForKey:@"array"]];
        BOOL add = [[categoryDict objectForKey:@"operation"] boolValue];
        
        CategoryData *oldData = [self getCategoryDataByType:aData.type];
        
        if (oldData) {
            if (!add) {
                [categoriesDataArray replaceObjectAtIndex:[categoriesDataArray indexOfObject:oldData] withObject:aData];
            } else {
                [oldData addDataFromArray:[categoryDict objectForKey:@"array"]];
            }
        } else {
            [categoriesDataArray addObject:aData];
        }
        [aData release];
        
        if ([[categoryDict objectForKey:@"oneCategory"] boolValue]) {
            id delegate = [categoryDict objectForKey:@"delegate"];
            if ([delegate respondsToSelector:@selector(updateData)])
                [delegate performSelector:@selector(updateData)];
        }
    }
}

- (void) checkCounter:(id)delegate
{
    requestedCategoriesCounter++;
            
    if (requestedCategoriesCounter == [allActiveCategories count]) {
        if ([categoriesDataArray count] > 0) {
            [self removeEmptyData];
            if (delegate && [delegate respondsToSelector:@selector(updateData)]) {
                [delegate performSelector:@selector(updateData)];
            }
        } else {
            if (delegate && [delegate respondsToSelector:@selector(noData)]) {
                [delegate performSelector:@selector(noData)];
            }
        }
    }
}

- (void) removeEmptyData
{
    NSIndexSet *indexesWithData = [categoriesDataArray  indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        CategoryData *data = (CategoryData *)obj;
        
        return !data.data || (data.data && data.totalRecords == 0);
    }];
    
    if ([indexesWithData count] <= 0) {
        if ([activeCategories count] != [activeCategoriesWithData count]) {
            [activeCategoriesWithData removeAllObjects];
            [activeCategoriesWithData addObjectsFromArray:activeCategories];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateData" object:nil];
        }
        return;
    }
    
    [activeCategoriesWithData removeAllObjects];
    
    NSArray *categDataArray = [NSArray arrayWithArray:categoriesDataArray];
    [categoriesDataArray removeAllObjects];
    
    [categDataArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![indexesWithData containsIndex:idx]) {
            [categoriesDataArray addObject:obj];
        }
    }];
    
    categDataArray = [NSArray arrayWithArray:categoriesDataArray];
    [categoriesDataArray removeAllObjects];
    
    [activeCategories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary *dict = (NSDictionary *)obj;
        NSString *type = [dict objectForKey:@"id"];
        
        NSInteger index = [categDataArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            CategoryData *data = (CategoryData *)obj;
            return [data.type isEqualToString:type];
        }];
        
        if (index != NSNotFound) {
            [categoriesDataArray addObject:[categDataArray objectAtIndex:index]];
            [activeCategoriesWithData addObject:dict];
        }
    }];
    
}

- (CategoryData *) getCategoryDataByType:(NSString *)type
{
    NSUInteger newIndex = [categoriesDataArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        CategoryData *data = (CategoryData *)obj;
        
        return [data.type isEqualToString:type];
        
    }];
    
    if (newIndex != NSNotFound) {
        return [categoriesDataArray objectAtIndex:newIndex];
    }
    return nil;
}

- (CategoryData *) getCategoryDataByIndex:(NSInteger)index
{
    if (index < [categoriesDataArray count]) {
        NSString *str = [self getCategoryIdAtIndex:index];
        NSUInteger newIndex = [categoriesDataArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            CategoryData *data = (CategoryData *)obj;
            
            return [data.type isEqualToString:str];
            
        }];
        
        if (newIndex != NSNotFound) {
            return [categoriesDataArray objectAtIndex:newIndex];
        }
        
        return nil;
    }
    return nil;
}

- (void) processRequest:(CategoryRequest *) request
{
    if (!getCategoriesQueue) {
        getCategoriesQueue = [NSOperationQueue new];
        [getCategoriesQueue setMaxConcurrentOperationCount:1];
    }
    [getCategoriesQueue addOperation:request];
}


#pragma mark -
#pragma mark Get Notifications Count
- (NSString *) onGetNotifications:(NSURLResponse *)response data:(NSData *)responseData error:(NSError *)error
{
    if ([self showError:error]) {
        return @"";
    }
	
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	int responseCode = [httpResponse statusCode];
	// NSLog(@"response string: %@",tempString);

	if (responseCode == 200) {
        NSString * tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSArray * tmpArray = [[tempString JSONValue]retain];
		NSLog(@"the array is %@",tmpArray);
        [tempString release];
		//tnApplication.userInfoDic = [tmpArray objectAtIndex:0];
        
		NSString * notificationsCount = [NSString stringWithFormat:@"%@", (tmpArray) ? [tmpArray objectAtIndex:0] : @""];
        NSLog(@"the array is %@",[tmpArray objectAtIndex:0]);
        
        return notificationsCount;
    } else if(responseCode==401) { // In case the session expires we have to make the user login again.
        [self showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }

    return @"";
}

-(void) getNotificationsCount:(id)delegateToUpdate
{
    alertCode = 0;
	[tnApplication CheckInternetConnection];
	if(tnApplication.m_internetWorking==0)//0: internet working
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
	
        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
        NSString *userid=[prefs valueForKey:@"userName"];
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getNotificationsCount&custid=%@",kServerUrl,userid];
		NSLog(@"value for name is%@",userid);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
        [NSURLConnection sendAsynchronousRequest:theRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *jsonError) {
            NSString *count = [self onGetNotifications:response data:data error:jsonError];
            if ([delegateToUpdate respondsToSelector:@selector(updateNotifications:)]) {
                [delegateToUpdate performSelector:@selector(updateNotifications:) withObject:count];
            }
        }];
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
	
}

//Messages==================================================================================================
- (NSArray *) onGetMessageData:(NSURLResponse *)response data:(NSData *)responseData error:(NSError *)error
{
    if ([self showError:error]) {
        return nil;
    }
	
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	int responseCode = [httpResponse statusCode];
	// NSLog(@"response string: %@",tempString);
    
	if (responseCode == 200) {
        NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSArray *tmp_array=[tempString JSONValue];
        return tmp_array;
    } else             
        [self showErrorByCode:responseCode fromSource:NSStringFromClass([self class])];
    return nil;
}

-(void) getStoryByMessageId:(id)delegateToUpdate message:(int)messageId
{
    alertCode = 0;
	[tnApplication CheckInternetConnection];
	if(tnApplication.m_internetWorking==0)//0: internet working
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
        
        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
        NSString *userid=[prefs valueForKey:@"userName"];

		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getFittingRoomRequest&custid=%@&msgid=%d",kServerUrl,userid,messageId];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
        [NSURLConnection sendAsynchronousRequest:theRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *jsonError) {
            NSArray *messageData = [self onGetMessageData:response data:data error:jsonError];
            if ([delegateToUpdate respondsToSelector:@selector(updateMessageData:)]) {
                [delegateToUpdate performSelector:@selector(updateMessageData:) withObject:messageData];
            }
        }];
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
	
}


//==========================================================================================================

- (BOOL) showError:(NSError *)error
{
    if (error) {
        [self showErrorByCode:5 fromSource:NSStringFromClass([self class])];
        return YES;
    }
    return NO;
}

- (void) exitFromApplication
{
    exit(0);
}

//- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated
//{
//    if (alertCode == 500 || alertCode == 401 || (alertCode <= 6 && alertCode > -1)) {
//        [self performSelector:@selector(exitFromApplication) withObject:nil afterDelay:1];
//    }
//    alertCode = 0;
//}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertCode == 500 || alertCode == 501 || alertCode == 503 || (alertCode <= 6 && alertCode > -1)) {
        [self performSelector:@selector(exitFromApplication) withObject:nil afterDelay:2];
    }
    alertCode = 0;
}


- (void) showErrorByCode:(NSInteger)resultCode fromSource:(NSString *)source
{
    if (alertCode != 0) {
        return;
    }
    
    NSLog(@"showErrorByCode = %i from source %@", resultCode, source);
    if(resultCode == 401 || resultCode == 6)  // In case the session expires we have to make the user login again.
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
        alertCode = resultCode;
    }
    else if(resultCode == 500 || alertCode == 501 || alertCode == 503 || (resultCode <= 5 && resultCode > -1))
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
        alert=nil;
        alertCode = resultCode;
    } /*else if(resultCode == 500) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Unknown error" message:[NSString stringWithFormat:@"Error code = %i from %@", resultCode, source] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
        alert=nil;
        alertCode = resultCode;
    }*/
    if (resultCode == 401 || resultCode <= 5 || resultCode == 6) {
        tnApplication.isUserLoggedInAfterLoggedOutState=FALSE;
        [tnApplication logout];
    }
}


- (void) dealloc
{
    [activeCategories release];
    [getCategoriesQueue release];
    [categoriesDataArray release];
    [allCategories release];
    [allActiveCategories release];
    [activeCategoriesWithData release];
    [categoriesNewNames release];
    [super dealloc];
}

@end
