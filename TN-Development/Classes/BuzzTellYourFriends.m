//
//  BuzzTellYourFriends.m
//  QNavigator
//
//  Created by softprodigy on 02/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BuzzTellYourFriends.h"
#include <QuartzCore/QuartzCore.h>
#import"QNavigatorAppDelegate.h"

@implementation BuzzTellYourFriends
@synthesize m_textView;
@synthesize m_view;
@synthesize m_lblTextCount;


int countText=140;
NSString *str_textView;
QNavigatorAppDelegate *l_appDelegate;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	m_lblTextCount.text=@"140";
	[m_textView.delegate self];
	//[m_view.layer setCornerRadius:6.0f];
	[m_textView.layer setCornerRadius:6.0f];
	[m_textView becomeFirstResponder];

}

-(IBAction)m_goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
	
	[l_appDelegate.m_customView setHidden:YES];
}

#pragma mark -
#pragma mark alertView delegate


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if(buttonIndex==0)
	{
		[m_textView resignFirstResponder];
		[self.navigationController popViewControllerAnimated:YES];
	}
}

-(IBAction)buzzButtonAction:(id)sender
{
	UIAlertView *BuzzAlertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Your message was sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[BuzzAlertView show];
	[BuzzAlertView release];
}

- (void)textViewDidChange:(UITextView *)textView
{
	
	int max_count;
	if (countText == 0) {
		NSLog(@"not editing\n");
		[m_textView setUserInteractionEnabled:FALSE];
	}
	str_textView = m_textView.text;
	[str_textView retain];
	max_count = countText-[str_textView length];
	m_lblTextCount.text=[NSString stringWithFormat:@"%d",max_count];
	NSLog(@"%d",countText);
	NSLog(@"%@",m_lblTextCount.text);
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{	// return NO to not change text
	
	if([text isEqualToString:@"\n"]) {
        [m_textView resignFirstResponder];
        return NO;
    }
	
	if(m_textView.text.length>=140 && range.length==0)
	{
		return NO;
	}
	else
	{
		return YES;
	}
}




/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

// GDL: Modified everything below

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];

    // Release all retained IBOutets.
    self.m_lblTextCount = nil;
    self.m_view = nil;
    self.m_textView = nil;
}

- (void)dealloc {
    [m_view release];
    [m_textView release];
    [m_lblTextCount release];
    
    [super dealloc];
}

@end
