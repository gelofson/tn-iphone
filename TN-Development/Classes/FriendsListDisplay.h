//
//  FriendsListDisplay.h
//  QNavigator
//
//  Created by softprodigy on 21/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"AddFriendsMypageViewController.h"



@interface FriendsListDisplay : UIViewController<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate> {
	
	UISearchBar *m_searchBar;
	
	AddFriendsMypageViewController *m_AddFriend;
	
	UIButton *m_addFriendButton;
	
	NSArray *m_friendsArray;
	NSMutableArray *m_backup;
		
	UIImage *m_image;
		
	NSString *m_name;
	
	NSMutableString *m_personName;
	
	UITableView *m_table;
	
	IBOutlet UIImageView *feedBackPage;//visible when no search records found;//**
}


@property(nonatomic,retain)IBOutlet UIButton *m_addFriendButton;
@property(nonatomic,retain) IBOutlet UISearchBar *m_searchBar;
@property (nonatomic,retain) NSArray *m_friendsArray;
@property(nonatomic,retain) IBOutlet UITableView *m_table;
@property(nonatomic, retain) UIImage *m_image;
@property(nonatomic, retain) NSMutableArray *m_backup;


-(IBAction)m_goToBackView;
-(IBAction)m_clickedOnAddFriendButton;
-(IBAction)AcceptFriendRequest:(id)sender;
-(IBAction)RejectFriendRequest:(id)sender;
-(IBAction)WithdrawFriendReuqest:(id)sender;
- (void) showHideTable:(NSArray *)array;


@end
