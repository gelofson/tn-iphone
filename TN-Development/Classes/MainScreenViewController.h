//
//  MainScreenViewController.h
//  QNavigator
//
//  Created by Soft Prodigy on 24/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTopBarView.h"
#import "QNavigatorAppDelegate.h"

@interface MainScreenViewController : UIViewController 
{
	UILabel *m_lblTitle;
}

@property(nonatomic,retain)IBOutlet UILabel *m_lblTitle;
@end
