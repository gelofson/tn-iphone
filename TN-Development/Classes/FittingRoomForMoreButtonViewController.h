//
//  FittingRoomForMoreButtonViewController.h
//  QNavigator
//
//  Created by softprodigy on 22/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface FittingRoomForMoreButtonViewController : UIViewController {

	UIScrollView *m_scrollView;
	UILabel *m_lableButItOrNot;
	NSString *stringBuyitOrNot;
	NSInteger m_Check;
	NSInteger m_MoreBtnTagValue;
	NSString *m_UserId;
	NSMutableArray *m_mainArray;
	int xCoordinate,yCoordinate,	tmp_width,tmp_height;
	NSMutableArray *m_messageIDsArray;
}

@property  int xCoordinate,yCoordinate,	tmp_width,tmp_height;
@property(nonatomic,retain) IBOutlet UIScrollView *m_scrollView;
@property(nonatomic,retain)IBOutlet UILabel *m_lableButItOrNot;
@property(nonatomic,retain)	NSString *stringBuyitOrNot;
@property(nonatomic,readwrite) NSInteger m_Check; 
@property(nonatomic,readwrite) NSInteger m_MoreBtnTagValue;


-(IBAction)btnBackAction:(id)sender;

-(IBAction) addiconsOnTheScreen:(NSInteger) number;

-(IBAction)IconClickedAction:(id)sender;

-(IBAction)getMoreRecords:(id)sender;

@end
