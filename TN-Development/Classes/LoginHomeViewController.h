//
//  LoginHomeViewController.h
//  QNavigator
//
//  Created by Soft Prodigy on 30/09/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@interface LoginHomeViewController : UIViewController <FBSessionDelegate>{
	BOOL		isUserLoggedOut;

	IBOutlet UIImageView * backgroundImageView;
	IBOutlet UIButton * loginButton;
	IBOutlet UIButton * signupButon;
    IBOutlet UIActivityIndicatorView *spinner;
    int intResponseStatusCode;
    NSString *firstName;
    NSString *lastName;
    NSString *fbID;
    NSString *twID;
}

@property (retain, nonatomic) ACAccountStore *accountStore;
@property (retain, nonatomic) ACAccount *twitterAccount;
@property BOOL isUserLoggedOut;
@property (nonatomic, retain) IBOutlet UIImageView * backgroundImageView;
@property (nonatomic, retain) IBOutlet UIButton * loginButton;
@property (nonatomic, retain) IBOutlet UIButton * signupButon;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic,copy) NSString *firstName;
@property (nonatomic,copy) NSString *lastName;
@property (nonatomic,copy) NSString *fbID;
@property (nonatomic,copy) NSString *twID;

-(IBAction)btnLoginAction:(id)sender;
-(IBAction)btnSignupAction:(id)sender;
-(IBAction)registerWithFacebook:(id)sender;
-(IBAction)registerWithTwitter:(id)sender;

-(void) sendRequestForLogin;
-(void) sendRequestForLogin:(BOOL) fromFB;
-(void) showAlertView:(NSString *)title alertMessage:(NSString *)message;

@end
