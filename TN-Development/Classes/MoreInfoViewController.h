//
//  MoreInfoViewController.h
//  QNavigator
//
//  Created by Soft Prodigy on 27/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BigSalesModelClass.h"

#import"SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"
#import "FBConnect.h"
#import "MGTwitterEngine.h"
#import "ShareSalesWithFriends.h"
#import "JSON.h"
#import "PhotosLoadingView.h"

#ifdef OLD_FB_SDK
#import "FBConnect/FBConnect.h"
#import "FBConnect/FBSession.h"
#endif

@class TwitterFriendsViewController;
@interface MoreInfoViewController : UIViewController <UITextFieldDelegate, SA_OAuthTwitterControllerDelegate,MFMailComposeViewControllerDelegate
#ifdef OLD_FB_SDK
, FBSessionDelegate,FBRequestDelegate, FBDialogDelegate
#endif
> {


	IBOutlet UITextView		*m_txtDesc;
	IBOutlet UITextView		*m_txtDetail;
	IBOutlet UIImageView	*m_imgViewAdThumb;
	IBOutlet UILabel		*m_lblTagLine;
	BigSalesModelClass		*m_objBigSales;
	BOOL					m_isConnectionActive;
	
	UIView	*m_FittingRoomView;
	IBOutlet UIView * popUpforShareInfo;
	IBOutlet UIView * m_shareView;
	IBOutlet UIButton *m_btnCancelWhereBox;
	IBOutlet UIImageView *m_imgShareView;
	IBOutlet UILabel *m_sharing_lable;

	IBOutlet UIButton *m_emailFrnds;
	IBOutlet UIButton *m_facebookFrnds;
	IBOutlet UIButton *m_twitterFrnds;
	IBOutlet UIButton *m_shopBeeFrnds;

	IBOutlet UIButton *m_btnSendBuyOrNot;
	IBOutlet UIButton *m_btnSendIBoughtIt;

	IBOutlet UIButton *m_publicWishlist;
	IBOutlet UIButton *m_privateWishlist;
	
	NSString *m_BoughtItOrNot;
	NSString *m_privateOrPublic;
	//UIImage *m_productImage;
	
	MGTwitterEngine *twitterEngine; 
	TwitterFriendsViewController *tw;
#ifdef OLD_FB_SDK
	FBSession *localFBSession;
	FBLoginDialog* fbLoginDialog;
#endif
}

@property(nonatomic,retain) IBOutlet UIView *m_FittingRoomView;
@property (nonatomic,retain) NSString *m_privateOrPublic;
@property (nonatomic,retain) NSString *m_BoughtItOrNot;
//@property (nonatomic,retain) UIImage *m_productImage;
@property (nonatomic,retain) MGTwitterEngine *twitterEngine; 
@property(nonatomic,retain) TwitterFriendsViewController *tw;
@property(nonatomic,retain)UITextView *m_txtDesc;
@property(nonatomic,retain)UITextView *m_txtDetail;
@property(nonatomic,retain)UIImageView *m_imgViewAdThumb;
@property(nonatomic,retain)BigSalesModelClass *m_objBigSales;
@property(nonatomic,retain)UILabel *m_lblTagLine;
@property BOOL				m_isConnectionActive;
@property (nonatomic,retain) IBOutlet UIView *popUpforShareInfo; 
@property (nonatomic,retain) IBOutlet UIView *m_shareView;
@property (nonatomic,retain) IBOutlet UIButton *m_btnCancelWhereBox;
@property (nonatomic,retain) IBOutlet UIImageView *m_imgShareView;
@property (nonatomic,retain)IBOutlet UILabel *m_sharing_lable;

@property (nonatomic,retain)IBOutlet UIButton *m_emailFrnds;
@property (nonatomic,retain)IBOutlet UIButton *m_facebookFrnds;
@property (nonatomic,retain)IBOutlet UIButton *m_twitterFrnds;
@property (nonatomic,retain)IBOutlet UIButton *m_shopBeeFrnds;

@property (nonatomic,retain)IBOutlet UIButton *m_btnSendBuyOrNot;
@property (nonatomic,retain)IBOutlet UIButton *m_btnSendIBoughtIt;

@property (nonatomic,retain)IBOutlet UIButton *m_publicWishlist;
@property (nonatomic,retain)IBOutlet UIButton *m_privateWishlist;


-(IBAction)backBtnAction;

-(IBAction)btnMapItAction:(id)sender;
-(void)sendRequestToCheckMallMap:(NSString *)imageUrl;

-(IBAction)shareBtnAction;
-(IBAction)btnTellFriendByEmailAction:(id)sender;
-(IBAction)btnCloseOnThreeButtonViewAction;
//-(IBAction)btnTellFriendByTwitterAction:(id)sender;
//-(void)whereBtnPressed:(int)tag;
-(IBAction)btnTellAFriendAction;
-(IBAction)ShopbeeFriends;
-(IBAction)clickButtonTellFriendsByFacebook:(id)sender;
-(IBAction) btnFindTwitterFrnd;
-(IBAction)btnFittingRoomViewAction;
-(IBAction)btnSendIBoughtItAction;
-(IBAction)btnSendBuyOrNotAction;
-(IBAction)btnWishListViewAction:(id)sender;
-(IBAction)btnTellShopBeeFriendsAction:(id)sender;
-(IBAction)btnPublicAction;
-(IBAction)btnPrivateAction;
-(IBAction)btnShareInfoAction:(id)sender;
// facebook
-(void)getFacebookName;
-(void)postToFacebookWall;

@end
