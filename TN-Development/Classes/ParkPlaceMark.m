#import "ParkPlaceMark.h"

@implementation ParkPlaceMark
@synthesize coordinate;
@synthesize titleStr;
@synthesize customView;
@synthesize subTitleStr;

- (NSString *)subtitle{
	NSString *temp=[NSString stringWithFormat:@"%@",subTitleStr];
	if (![temp isEqual:[NSNull null]] && temp!=nil) {
		return temp;
	}
	return @"";
}
- (NSString *)title{
	
	NSString *temp=[NSString stringWithFormat:@"%@",titleStr];
	
	return temp;
	
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c{
	coordinate=c;
	NSLog(@"%f,%f",c.latitude,c.longitude);
	return self;
}

// GDL: Changed everything below.

- (void) dealloc {
    
    [titleStr release];
    [subTitleStr release];
    [customView release];
    
    [super dealloc];
}

@end
