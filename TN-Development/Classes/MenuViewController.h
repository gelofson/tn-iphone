//
//  MenuViewController.h
//  SlideOutNavigationSample
//
//  Created by Nick Harris on 2/3/12.
//  Copyright (c) 2012 Sepia Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSON.h"
#import "GAITrackedViewController.h"

@interface MenuViewController : GAITrackedViewController <UIGestureRecognizerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *screenShotImageView;
@property (strong, nonatomic) UIImage *screenShotImage;
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (strong, nonatomic) UIPanGestureRecognizer *panGesture;
@property(strong,nonatomic)UIActivityIndicatorView		*m_activityIndicator;
@property(strong,nonatomic)NSMutableData 				*m_mutResponseData;
- (void)slideThenHide;
- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer ;
-(void)sendRequestForLogout;
@property (retain, nonatomic) IBOutlet UIButton *btn_crash;

@end
