//
//  PhotosLoadingView.m
//  QNavigator
//
//  Created by softprodigy on 24/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PhotosLoadingView.h"


@implementation PhotosLoadingView

@synthesize imageView;

- (void)dealloc {
	[connection cancel]; //in case the URL is still downloading
	[connection release];
	[imageView release];
	[data release]; 
    [super dealloc];
}

- (void)loadImageFromURL:(NSURL*)url tempImage:(id)tempImage
{
	if (connection!=nil) 
	{
		[connection cancel];
		[connection release]; 
		connection=nil;
		
	} //in case we are downloading a 2nd image
	if (data!=nil) { [data release]; data=nil; }
	
	//if ([[self subviews] count]>0) {
	//	//		//then this must be another image, the old one is still in subviews
	//			[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
	//	}	
	
	for (UIView *tempView in [self subviews])
	{
		[tempView removeFromSuperview];
	}

	if ((self.frame.size.width/self.frame.size.height) > 1.25) {
		// rectangular shape
		imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"groupon_loading_icon"]];
	} else {
		imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading.png"]];
	}
	imageView.tag=101;
	//make sizing choices based on your needs, experiment with these. maybe not all the calls below are needed.
	imageView.contentMode = UIViewContentModeScaleAspectFit;
	//imageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth || UIViewAutoresizingFlexibleHeight );
	self.backgroundColor = [UIColor whiteColor];
	[self addSubview:imageView];
	imageView.frame = self.bounds;
	[imageView setNeedsLayout];
	[self setNeedsLayout];
	NSLog(@"checking image in photos");
	
	if (tempImage==nil)
	{
		NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
		connection = [[NSURLConnection alloc] initWithRequest:request delegate:self]; //notice how delegate set to self object
	
	}
	else {
		NSLog(@"assigning stored image in photos");
		imageView.image=(UIImage *)tempImage;
	}

		//TODO error handling, what if connection is nil?
}


//the URL connection calls this repeatedly as data arrives
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	if (data==nil) { data = [[NSMutableData alloc] initWithCapacity:2048]; } 
	[data appendData:incrementalData];
}

//the URL connection calls this once all the data has downloaded
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
	//so self data now has the complete image 
	[connection release];
	connection=nil;
	//if ([[self subviews] count]>0) {
	//		//then this must be another image, the old one is still in subviews
	//		[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
	//	}
	
	//make an image view for the image
	
	self.backgroundColor = [UIColor clearColor];
	UIImage *tempImage=[UIImage imageWithData:data];
	if (tempImage==nil)
	{
		tempImage=[UIImage imageNamed:@"no-image"];
	}
	
	imageView.image=tempImage;
	
	//UIImageView* imageView = [[[UIImageView alloc] initWithImage:[UIImage imageWithData:data]] autorelease];
	
	//imageView.tag=101;
	//	//make sizing choices based on your needs, experiment with these. maybe not all the calls below are needed.
	//	imageView.contentMode = UIViewContentModeScaleAspectFit;
	//	imageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth || UIViewAutoresizingFlexibleHeight );
	//	[self addSubview:imageView];
	//	imageView.frame = self.bounds;
	//	[imageView setNeedsLayout];
	//	[self setNeedsLayout];
	
	[data release]; //don't need this any more, its in the UIImageView now
	data=nil;
}

//just in case you want to get the image directly, here it is in subviews
- (UIImage*) image 
{
	UIImageView* iv = [[self subviews] objectAtIndex:0];
	return [iv image];
}

@end
