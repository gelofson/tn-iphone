//
//  BuyItCommentViewController.m
//  QNavigator
//
//  Created by softprodigy on 28/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CommentViewController.h"
#import<QuartzCore/QuartzCore.h>
#import"Constants.h"
#import"FittingRoomUseCasesViewController.h"
#import"LoadingIndicatorView.h"
#import"ShopbeeAPIs.h"
#import"FittingRoomMoreViewController.h"
#import"AsyncImageView.h"
#import"QNavigatorAppDelegate.h"

#import "NewsDataManager.h"

@implementation CommentViewController

FittingRoomUseCasesViewController *l_Obj;

ShopbeeAPIs *l_requestObj;

LoadingIndicatorView *l_buyItCommentIndicatorView;

QNavigatorAppDelegate *l_appDelegate;

@synthesize m_CallBackViewController;

@synthesize m_textView;

@synthesize m_data;

@synthesize m_PersonName;

@synthesize m_Subject;

@synthesize m_HeaderLabel;

@synthesize m_Dictionary;

@synthesize m_FavourString;
@synthesize m_personImage;
@synthesize senderImage;
//
//@synthesize m_Imageview;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewDidAppear:(BOOL)animated
{
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[m_textView.layer  setCornerRadius:4.0f];
	[m_textView.layer setMasksToBounds:YES];
	m_textView.font=[UIFont fontWithName:kFontName size:14];
        
	NSDictionary *tmp_dict = [m_data valueForKey:@"wsMessage"];
    
    // GDL: m_Type is not retained. I was releasing it.
	m_Type=[tmp_dict valueForKey:@"messageType"];
    
	m_messageId=[[tmp_dict valueForKey:@"id"] intValue];
	
	m_Subject.text=[tmp_dict valueForKey:@"bodyMessage"];
    m_Subject.font = [UIFont fontWithName:kMyriadProRegularFont size:13];
	m_PersonName.text=[m_data valueForKey:@"originatorName"];
    m_PersonName.font = [UIFont fontWithName:kMyriadProRegularFont size:13];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	m_UserID=[prefs valueForKey:@"userName"];
    
    // GDL: Was getting crash here. Sending isEqualToString message to deallocated instance.
    // GDL: That's because I was releasing m_Type in dealloc without retaining it here.
    m_HeaderLabel.font = [UIFont fontWithName:kMyriadProBoldFont size:19];
/*	if ([m_Type isEqualToString:kBuyItOrNot])
		m_HeaderLabel.text=[NSString stringWithFormat:@"%@: Comment", [[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"]];
	else if ([m_Type isEqualToString:kIBoughtIt])
		m_HeaderLabel.text=[NSString stringWithFormat:@"%@: Comment", [[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"]];
    else if ([m_Type isEqualToString:kCheckThisOut])
		m_HeaderLabel.text=[NSString stringWithFormat:@"%@: Comment", [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."]];
    else if ([m_Type isEqualToString:kHowDoesThisLook])
		m_HeaderLabel.text=[NSString stringWithFormat:@"%@: Comment", [[Context getInstance] getDisplayTextFromMessageType:@"How do I look?"]];
    else if ([m_Type isEqualToString:kFoundASale])
		m_HeaderLabel.text=[NSString stringWithFormat:@"%@: Comment", [[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"]];
    else if ([m_Type isEqualToString:kMessageBoard])
		m_HeaderLabel.text=[NSString stringWithFormat:@"%@: Comment", [[Context getInstance] getDisplayTextFromMessageType:@"Q"]];
    else {*/
        m_HeaderLabel.text = m_Type;
//    }
    
	l_buyItCommentIndicatorView=[LoadingIndicatorView SharedInstance];
	m_Dictionary=[[NSMutableDictionary alloc] init];

    NSString *Url=[NSString stringWithFormat:@"%@%@",kServerUrl,[self.m_data valueForKey:@"originatorThumbImageUrl"]];	
	NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
	//((UIImageView *)[asyncImage viewWithTag:101]).image=[UIImage imageNamed:@"loading.png"];
	[self.senderImage loadImageFromURL:temp_loadingUrl ignoreCaching:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appBecameActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    //[senderImage setImage:self.m_personImage];
}

- (void) appBecameActive:(NSNotification *)notification
{
    [m_textView performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.2];
}

- (void) appWillResignActive:(NSNotification *)notification
{
    [m_textView performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.2];
}


#pragma mark -
#pragma mark Custom methods
-(void)doneClick{
	[(UIButton*)[self.view viewWithTag:1] setUserInteractionEnabled:YES];
	[(UIButton*)[self.view viewWithTag:2] setUserInteractionEnabled:YES];
	[m_textView resignFirstResponder];	
	[self animateViewDownward];
	
}
-(IBAction) goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];	
}

-(IBAction) BtnSendAction
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	//l_ViewRequest=[[FittingRoomMoreViewController alloc] init];
	NSString *ResponseWords=[m_textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if ([ResponseWords isEqualToString:@""]) 
	{
		[self showAlertView:@"" alertMessage:@"Please enter your comments." tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
	}
	else 
	{
		[m_Dictionary setValue:ResponseWords forKey:@"responseWords"];
		[m_Dictionary setObject:[NSNumber numberWithInt:m_messageId] forKey:@"messageId"];
		//[m_Dictionary setObject:m_Type forKey:@"type"];
		[m_Dictionary setObject:m_FavourString forKey:@"isFavour"];
		[l_buyItCommentIndicatorView startLoadingView:self];
		[l_requestObj addResponseForFittingRoomRequest:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserID msgresponse:m_Dictionary];
		
	}
		
	//UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Your message sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//	[alert  show];
//	[alert setTag:1];
//	[alert release];
}
-(void)addTopBarOnKeyboard
{	
	[(UIButton*)[self.view viewWithTag:1] setUserInteractionEnabled:NO];
	[(UIButton*)[self.view viewWithTag:2] setUserInteractionEnabled:NO];
	if(doneBackground)
	{
		[doneBackground removeFromSuperview];
		[doneBackground release];
		doneBackground=nil;
	}
	if(doneButton)
	{
		[doneButton removeFromSuperview];
		[doneButton release];
		doneButton=nil;
	}
	
	doneBackground=[[UIImageView alloc]initWithFrame:CGRectMake(0,285,320,40)];
	//[doneBackground setTag:1001];
	doneBackground.image=[UIImage imageNamed:@"bar-for-wheel.png"];
	[self.view addSubview:doneBackground];
	
	doneButton = [[UIButton alloc]initWithFrame:CGRectMake(255,290, 54, 28)];
	//[doneButton retain];
//	[doneButton setTag:1002];
	[doneButton setBackgroundImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
	[doneButton addTarget:self action:@selector(doneClick)
		 forControlEvents:UIControlEventTouchUpInside];	
	[self.view addSubview:doneButton];
	
}
-(IBAction)BtnCancelAction{
	[self.navigationController popToViewController:m_CallBackViewController animated:YES];
    //[self showAlertView:nil alertMessage:@"Do you want to send your vote anyway?" tag:2 cancelButtonTitle:@"NO" otherButtonTitles:@"OK"];
}

-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage tag:(NSInteger)Tagvalue cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles
{
	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles ,nil];
	tempAlert.tag=Tagvalue;
	[tempAlert show];
	[tempAlert release];
	tempAlert=nil;
}

#pragma mark Animation methods

-(void)animateViewUpward
{
	[UIView beginAnimations: @"upView" context: nil];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDuration: 0.4];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	self.view.frame = CGRectMake(0,-80, 320,460);
	[UIView commitAnimations];
}

-(void)animateViewDownward
{
	[UIView beginAnimations: @"downView" context: nil];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDuration: 0.4];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	self.view.frame = CGRectMake(0,0, 320,460);
	[UIView commitAnimations];
	doneButton.hidden=YES;
	doneBackground.hidden=YES;
}
#pragma mark -
#pragma mark call back methods
-(void)CallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_buyItCommentIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) {
		[self showAlertView:nil alertMessage:@"Your message was sent successfully." tag:1 cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [self.m_textView resignFirstResponder];
		
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	[tempString release];
	tempString=nil;
}


#pragma mark -
#pragma mark textView delegate
//-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
//	//[self addTopBarOnKeyboard];
//	//[self animateViewUpward];
//}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
	[self addTopBarOnKeyboard];	
	[self animateViewUpward];
	return YES;
	
}
-(BOOL) textViewShouldEndEditing:(UITextView *)textView
{
	[self animateViewDownward];
	[m_textView resignFirstResponder];
	return YES;
}

// Override to allow orientations other than the default portrait orientation.
/*- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
*/
 
#pragma mark -
#pragma mark alert view delegates
- (void) goBack
{
    [self.navigationController popToViewController:m_CallBackViewController animated:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (alertView.tag==1) {
						
		if (buttonIndex==0) 
		{
			[self performSelector:@selector(goBack) withObject:nil afterDelay:1];
		}
	}
	if (alertView.tag==2) {
		if (buttonIndex==0) {
			[self.navigationController popViewControllerAnimated:YES];
		}
		else if(buttonIndex==1)
		{
			[m_Dictionary setValue:@"" forKey:@"responseWords"];
			[m_Dictionary setValue:m_Type forKey:@"type"];
			[m_Dictionary setObject:m_FavourString forKey:@"isFavour"];
			[m_Dictionary setValue:[NSNumber numberWithInt:m_messageId]  forKey:@"messageId"];
			[l_buyItCommentIndicatorView startLoadingView:self];
			[l_requestObj addResponseForFittingRoomRequest:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserID msgresponse:m_Dictionary];
		}
	}
	if (alertView.tag==3) {
		if (buttonIndex==0) {
			[self.navigationController popToRootViewControllerAnimated:YES];
		}
	}
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // Release retained IBOutlets.
    self.m_textView = nil;
    self.m_PersonName = nil;
    self.m_Subject = nil;
    self.m_data = nil;
    self.m_HeaderLabel = nil;
}


- (void)dealloc {
    [m_textView release];
    [doneBackground release];
    [doneButton release];
    [m_data release];
    [m_PersonName release];
    [m_Subject release];
    //[m_UserID release];
    [m_Dictionary release];
    [m_personImage release];
    // GDL: m_Type is not retained. It is retained by m_MainArray
    //[m_Type release];
    [senderImage release];
    [m_FavourString release];
    [m_CallBackViewController release];
    
    [super dealloc];
}


@end
