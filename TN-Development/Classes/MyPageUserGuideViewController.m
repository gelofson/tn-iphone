//
//  MyPageUserGuideViewController.m
//  QNavigator
//
//  Created by softprodigy on 01/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MyPageUserGuideViewController.h"
#import "Constants.h"
#import"QNavigatorAppDelegate.h"

# define SectionHeaderHeight 30

@implementation MyPageUserGuideViewController

@synthesize m_tableUserGuide;

QNavigatorAppDelegate *l_appDelegate;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
#pragma mark custom methods
-(IBAction) m_goToBackView{
	[self.navigationController popViewControllerAnimated:YES];
}
#pragma mark table view methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 3;
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section==0) 
	{
		return 122;
			
	}
	else if (indexPath.section==1) 
	{
		return 137;
		
	}
	else if (indexPath.section==2) 
	{
		return 102;
				
	}
    
    // GDL: We should not get here without returning something.
    return tableView.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	NSString *SimpleTableIdentifier = [NSString stringWithFormat:@"Cell %@",indexPath];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"test"];
	
	if (cell == nil) { 
		
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleTableIdentifier] autorelease];
	}
	
	if(indexPath.section==0)
	{
		UITextView *tmp_textView1=[[UITextView alloc] initWithFrame:CGRectMake(3,3,295
																			   ,115)];
		tmp_textView1.text=@"FashionGram shows you the retail sales and promotions that are near you. It uses GPS technology to know where you are, and compares your location to the sales that are going on nearby, and tells you, so you can find them right away.";
		tmp_textView1.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
		tmp_textView1.font=[UIFont fontWithName:kFontName size:12];
		tmp_textView1.backgroundColor=[UIColor clearColor];
		[cell.contentView addSubview:tmp_textView1];
		//tmp_textView1.textAlignment=UITextAlignmentLeft;
		tmp_textView1.userInteractionEnabled=NO;
		[tmp_textView1 release];
		tmp_textView1=nil;
		//cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"whatTheShoppbeeDo.png"]];	
	}
	else if(indexPath.section==1)
	{
		UITextView *tmp_textView2=[[UITextView alloc] initWithFrame:CGRectMake(3,3,295,130)];
		tmp_textView2.text=@"FashionGram can help find sales on shoes, bags, apparel, accessories, sporting goods, electronics, kids clothes, menswear, movies and restraunts. FashionGram can also find sales on boutiques (the indy shop icon in the row of icons at the top of FashionGram main screen), and there you will find endless variety.";
		tmp_textView2.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
		tmp_textView2.font=[UIFont fontWithName:kFontName size:12];
		///tmp_textView2.backgroundColor=[UIColor redColor];
		[cell.contentView addSubview:tmp_textView2];
		tmp_textView2.userInteractionEnabled=NO;
		[tmp_textView2 release];
		tmp_textView2=nil;
		//cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"WhatKindOfSale.png"]];	
	}
	else if(indexPath.section==2)
	{
		UITextView *tmp_textView3=[[UITextView alloc] initWithFrame:CGRectMake(3,3,295,95)];
		tmp_textView3.text=@"Look at the top of FashionGram screen. You will see the row of icons there. If there is a shoe icon along that row, just press on it and shoe sales will popup. But, if there are no shoes on sale nearby, you will not see the shoe icon at the top.";
		tmp_textView3.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
		//tmp_textView3.backgroundColor=[UIColor redColor];
		tmp_textView3.font=[UIFont fontWithName:kFontName size:12];
		[cell.contentView addSubview:tmp_textView3];
		tmp_textView3.userInteractionEnabled=NO;
		[tmp_textView3 release];
		tmp_textView3=nil;
		
		//cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"HowDoILookFor.png"]];	
	}
	
	
	cell.selectionStyle=UITableViewCellSelectionStyleNone;
	return cell;
	
}

//- (void) tableView: (UITableView *) tableView willDisplayCell: (UITableViewCell *) cell forRowAtIndexPath: (NSIndexPath *) indexPath {
//   
//	if(indexPath.section==0)
//	{
//		UITextView *tmp_textView1=[[UITextView alloc] initWithFrame:CGRectMake(3,3,295,115)];
//		tmp_textView1.text=@"FashionGram shows you the retail sales and promotions that are near you.It uses GPS technology to know where you are, and compares your location to the sales that are going on nearby, and tells them so you can find them right away.";
//		tmp_textView1.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
//		tmp_textView1.font=[UIFont fontWithName:kFontName size:12];
//		tmp_textView1.backgroundColor=[UIColor redColor];
//		[cell.contentView addSubview:tmp_textView1];
//		//tmp_textView1.textAlignment=UITextAlignmentLeft;
//		tmp_textView1.userInteractionEnabled=NO;
//		[tmp_textView1 release];
//		tmp_textView1=nil;
//		//cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"whatTheShoppbeeDo.png"]];	
//	}
//	else if(indexPath.section==1)
//	{
//		UITextView *tmp_textView2=[[UITextView alloc] initWithFrame:CGRectMake(3,3,295,130)];
//		tmp_textView2.text=@"FashionGram can help find sales on shoes, bags, apparel, accessories, sporting goods, electronics, kids clothes, menswear, movies and restraunts. FashionGram can also find sales on beautiques(the indy shop icon in the row of icons at the top of FashionGram main screen),and there you will find endless variety.";
//		tmp_textView2.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
//		tmp_textView2.font=[UIFont fontWithName:kFontName size:12];
//		tmp_textView2.backgroundColor=[UIColor redColor];
//		[cell.contentView addSubview:tmp_textView2];
//		tmp_textView2.userInteractionEnabled=NO;
//		[tmp_textView2 release];
//		tmp_textView2=nil;
//		//cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"WhatKindOfSale.png"]];	
//	}
//	else if(indexPath.section==2)
//	{
//		UITextView *tmp_textView3=[[UITextView alloc] initWithFrame:CGRectMake(3,3,295,95)];
//		tmp_textView3.text=@"Look at the top of FashionGram screen.You will see the row of icons,there.If there is a shoe icon along that row,just press on it and shoe sales will popup.But,if there are no shoes on sale nearby,you will not see the shoe icon at the top. ";
//		tmp_textView3.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
//		tmp_textView3.backgroundColor=[UIColor redColor];
//		tmp_textView3.font=[UIFont fontWithName:kFontName size:12];
//		[cell.contentView addSubview:tmp_textView3];
//		tmp_textView3.userInteractionEnabled=NO;
//		[tmp_textView3 release];
//		tmp_textView3=nil;
//		
//		//cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"HowDoILookFor.png"]];	
//	}
//	
//}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	
	if (section==0)
	{
		
		UIView *tmp_view;
		tmp_view = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, SectionHeaderHeight)];
		tmp_view.backgroundColor=[UIColor clearColor]; 
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,3, 320, 20)] ;
		tmp_headerLabel.font=[UIFont fontWithName:kFontName size:13];
		tmp_headerLabel.text=@"   What does FashionGram do?";
		tmp_headerLabel.textColor=[UIColor whiteColor];
		tmp_headerLabel.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		
		[tmp_view addSubview:tmp_headerLabel];
		[tmp_headerLabel release];
		tmp_headerLabel=nil;
		return [tmp_view autorelease];
	}
	else if(section==1) {
		
		
		UIView *tmp_headerForSecondSection;
		tmp_headerForSecondSection = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, SectionHeaderHeight)];
		tmp_headerForSecondSection.backgroundColor=[UIColor clearColor];
		[tmp_headerForSecondSection autorelease];
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,3, 320, 20)] ;
		tmp_headerLabel.font=[UIFont  fontWithName:kFontName size:13 ];
		tmp_headerLabel.text=@"   What kind of sales can FashionGram Help me find?";
		tmp_headerLabel.textColor=[UIColor whiteColor];
		tmp_headerLabel.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		
		[tmp_headerForSecondSection addSubview:tmp_headerLabel];
		
		[tmp_headerLabel release];
		tmp_headerLabel=nil;
		
		return tmp_headerForSecondSection;
		[tmp_headerForSecondSection release];
		tmp_headerForSecondSection=nil;
		
		
	}
	else if(section == 2)
	{
		UIView *tmp_view;
		tmp_view = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, SectionHeaderHeight)];
		tmp_view.backgroundColor=[UIColor clearColor]; //colorWithRed:.73f green:.74f blue:.74f alpha:1.0];
		
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,3, 320, 20)]  ;
		tmp_headerLabel.font=[UIFont fontWithName:kFontName size:13];
		tmp_headerLabel.text=@"   How do I look for something like shoes on sales?";
		tmp_headerLabel.textColor=[UIColor whiteColor];
		tmp_headerLabel.backgroundColor=[UIColor colorWithRed:0.73f green:0.74f blue:0.74f alpha:1.0];
		[tmp_view addSubview:tmp_headerLabel];
		
		[tmp_headerLabel release];
		tmp_headerLabel=nil;
	
	
		return [tmp_view autorelease];
	}	
			else
	{
		return nil;
	}
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section 
{ 
	return 30.0;
} 

#pragma mark -------------

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
- (void)viewDidLoad {
	
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated{
}

// GDL: changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_tableUserGuide = nil;
}

- (void)dealloc {
	[m_tableUserGuide release];

    [super dealloc];
}

@end
