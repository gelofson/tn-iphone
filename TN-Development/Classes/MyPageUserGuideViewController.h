//
//  MyPageUserGuideViewController.h
//  QNavigator
//
//  Created by softprodigy on 01/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyPageUserGuideViewController : UIViewController {

	UITableView *m_tableUserGuide;
}
@property(nonatomic,retain)IBOutlet UITableView *m_tableUserGuide;

-(IBAction) m_goToBackView;
@end
