//
//  TwitterFriendsViewController.m
//  QNavigator
//
//  Created by softprodigy on 29/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TwitterFriendsViewController.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "WhereBox.h"
#import "ATIconDownloader.h"
#import "AddFriendsMypageViewController.h"
#import "ShopbeeAPIs.h"
#import "SBJSON.h"
#import "TwitterProcessing.h"
#import "AsyncImageView.h"
#import <QuartzCore/QuartzCore.h>
#import "LoadingIndicatorView.h"
#import "NewsDataManager.h"
#define SectionHeaderHeight 25

@implementation TwitterFriendsViewController

@synthesize table_AddFriends;
@synthesize m_arrTwitterFriends;
@synthesize m_arrFrndsDic;
@synthesize searchB;

@synthesize m_strUids;
@synthesize m_nonshopBeeArray;
@synthesize m_shopBeeArray;
@synthesize m_intCurrentTwitterId;
@synthesize temp_twitterPross;
@synthesize m_picUrls;
@synthesize m_dialog_view;
@synthesize m_arrFrnds;
@synthesize tag_Val;

ShopbeeAPIs *l_request;
QNavigatorAppDelegate *l_appDelegate;
NSMutableDictionary *l_Dictionary;
UITextView *l_textView;
int countFilteredNameList2=0;



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

//...............................................................................................................................................//
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	m_dialog_view = [[UIView alloc] initWithFrame:CGRectMake(30, 100,260, 200)];

	temp_twitterPross=[[TwitterProcessing alloc]init];
	m_shopBeeArray =[[NSMutableArray alloc]init];
	m_nonshopBeeArray=[[NSMutableArray alloc]init];	
	
	m_arrFrndsDic=[[NSMutableArray alloc]init];
	searchB.delegate=self;
		
	
	/* search bar background setting */
	[searchB setFrame:CGRectMake(0, 78, 320, 32)];
	UITextField *searchField;
	NSUInteger numViews = [searchB.subviews count];
	for(int i = 0; i < numViews; i++) 
	{
		
		[[searchB.subviews objectAtIndex:0] setHidden:YES];
		
		if([[searchB.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
		{
			searchField = [searchB.subviews objectAtIndex:i];
		}
	}
	
	if(!(searchField == nil)) 
	{
		[searchField setBackground: [UIImage imageNamed:@"gray_searchbox@2x.png"] ];
		[searchField setBorderStyle:UITextBorderStyleNone];
	}
	/***********************************/	
	
	NSLog(@"arrrrrrrrrrrr>>%@",self.m_arrTwitterFriends);
		
	
	for(int i=0;i<self.m_arrTwitterFriends.count;i++)
	{
		NSMutableDictionary *dict=[m_arrTwitterFriends objectAtIndex:i];
		NSString *str=[dict objectForKey:@"name"];
		NSString *image_url =[dict objectForKey:@"profile_image_url"];
		NSString *string_id=[dict objectForKey:@"id"];
		l_Dictionary=[[NSMutableDictionary alloc]initWithObjectsAndKeys:str ,@"name",image_url,@"profile_image_url",string_id,@"id",nil];
		
		[m_arrFrndsDic addObject:l_Dictionary];
		NSLog(@"urls>>>>>>>%@",image_url);

	}
	m_arrTwitterFriends=[m_arrFrndsDic retain];

		
	
	NSMutableArray	*tempArray=[[NSMutableArray alloc]init];
	
	for(int i=0;i<[m_arrFrndsDic count];i++)
	{
		[tempArray addObject:[NSString stringWithFormat:@"%@",[[m_arrFrndsDic objectAtIndex:i]valueForKey:@"id"]]];
	}
	
	self.m_strUids=[NSString stringWithFormat:@"listids=%@",[tempArray JSONFragment]];
	NSLog(@"m_strUids: %@",m_strUids);
	[tempArray release];
	tempArray=nil;

	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	NSUserDefaults *userDefaultU_IdTwitter=[NSUserDefaults standardUserDefaults];
	NSString *TwitterUid=[userDefaultU_IdTwitter objectForKey:@"twitterUid"];
	
	
	//m_view.hidden=NO;
//	m_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
//	m_view.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
//	
//	m_indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(140,200 , 30, 30)];
//	
	
	[self performSelector:@selector(dialogBoxForInvite:)];
	
//	[m_view addSubview:m_indicatorView];
//	[self.view addSubview:m_view];
//	[m_indicatorView startAnimating];
	l_request=[[ShopbeeAPIs alloc] init];
	[[LoadingIndicatorView SharedInstance] startLoadingView:self];
	[l_request mapTwitterID:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:customerID twitterid:TwitterUid];
	[l_request release];
	l_request=nil;
}

-(void)viewWillAppear:(BOOL)animated
{
	[m_dialog_view.layer setCornerRadius:8.0f];
	[m_dialog_view.layer setMasksToBounds:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
}

#pragma mark -
#pragma mark table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
	return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
		
	
	if (section==0) {
		
		UIView *tmp_view;
		tmp_view = [[UIView alloc] initWithFrame:CGRectMake(0,0, 350, SectionHeaderHeight)];
		tmp_view.backgroundColor=[UIColor colorWithRed:0.73f green:0.74f blue:0.74f alpha:1.0];
		[tmp_view autorelease];
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,2, 350,25)]  ;
		tmp_headerLabel.font=[UIFont fontWithName:kFontName size:16];
		tmp_headerLabel.text=[NSString stringWithFormat:@"%d friends are on FashionGram",m_arrFrnds.count] ;
		tmp_headerLabel.backgroundColor=[UIColor colorWithRed:0.73f green:0.74f blue:0.74f alpha:1.0];
		tmp_headerLabel.textColor=[UIColor whiteColor];
		[tmp_view addSubview:tmp_headerLabel];
		
		UIButton *temp_addAll=[[UIButton alloc] initWithFrame:tmp_view.frame];
		[temp_addAll setImage:[UIImage imageNamed:@"add_all.png"] forState:UIControlStateNormal];
		[temp_addAll addTarget: self action:@selector(addAllBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
		temp_addAll.frame=CGRectMake(240,4,42,23);
		[tmp_view addSubview:temp_addAll];
		
		[tmp_headerLabel release];
		tmp_headerLabel=nil;
		
		return tmp_view;
	}
	else 
	{
		
		UIView *tmp_headerForSecondSection;
		tmp_headerForSecondSection = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 350, SectionHeaderHeight)];
		tmp_headerForSecondSection.backgroundColor=[UIColor colorWithRed:0.73f green:0.74f blue:0.74f alpha:1.0];
		[tmp_headerForSecondSection autorelease];

		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,2, 350, 25)] ;
		[tmp_headerLabel autorelease];
		tmp_headerLabel.font=[UIFont  fontWithName:kFontName size:16 ];
		tmp_headerLabel.text=[NSString stringWithFormat:@"%d friends not on FashionGram",m_arrNonFrnds.count];
		tmp_headerLabel.textColor=[UIColor whiteColor];
		tmp_headerLabel.backgroundColor=[UIColor colorWithRed:0.73f green:0.74f blue:0.74f alpha:1.0];
		[tmp_headerForSecondSection addSubview:tmp_headerLabel];
		
		
		UIButton *temp_addAll=[[UIButton alloc] initWithFrame:tmp_headerForSecondSection.frame];
		[temp_addAll setImage:[UIImage imageNamed:@"btn_inviteAll"] forState:UIControlStateNormal];
		[temp_addAll addTarget: self action:@selector(btnInviteAllAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_addAll.frame=CGRectMake(260,5,43,20);
		[tmp_headerForSecondSection addSubview:temp_addAll];
						
		return tmp_headerForSecondSection;
	}
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	if(section==0)
	
	{
		return [m_arrFrnds count];
	}
	else
	{
		return [m_arrNonFrnds count];
	}
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	
	NSString *SimpleTableIdentifier = [NSString stringWithFormat:@"Cell %@",indexPath];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: SimpleTableIdentifier];
	
	
	if (cell == nil) 
	{ 
		
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleTableIdentifier] autorelease];
		
		cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_without_arrow_in_add_friend"]];	
		cell.selectionStyle=UITableViewCellSelectionStyleNone;
		
		CGRect frame;
		frame.size.width=30; frame.size.height=30;
		frame.origin.x=5; frame.origin.y=10;
		
		AsyncImageView* asyncImage = [[[AsyncImageView alloc]
									   initWithFrame:frame] autorelease];
		asyncImage.tag = 999;
		[cell.contentView addSubview:asyncImage];
		
		UILabel *temp_lblFriendName=[[UILabel alloc]init];
		temp_lblFriendName.frame=CGRectMake(45,6,185,25);
		temp_lblFriendName.font=[UIFont fontWithName:kFontName size:16];
		temp_lblFriendName.tag=102;
		temp_lblFriendName.backgroundColor=[UIColor clearColor];
		temp_lblFriendName.textColor=[UIColor colorWithRed:0.28f green:0.55f blue:0.60f alpha:1.0];
		[cell.contentView addSubview:temp_lblFriendName];
		[temp_lblFriendName release];
		
	}
	else 
	{
		AsyncImageView* asyncImage = (AsyncImageView*)[cell.contentView viewWithTag:999];
		UIImageView *tempImageview=(UIImageView *)[asyncImage viewWithTag:101];
		[tempImageview setImage:nil];
		
		[(UILabel *)[cell.contentView viewWithTag:102] setText:@""];
	}
	
	if(indexPath.section==0)
	{		
		
		[(UILabel *)[cell.contentView viewWithTag:102] setText:
		 [NSString stringWithFormat:@"%@",[[m_arrFrnds objectAtIndex:indexPath.row] objectForKey:@"name"]]];
			
		UIButton *temp_addButton=[UIButton buttonWithType:UIButtonTypeCustom];
		[temp_addButton setImage:[UIImage imageNamed:@"add_in_add_friend_button.png"] forState:UIControlStateNormal];
		temp_addButton.tag=indexPath.row;
		[temp_addButton addTarget: self action:@selector(btn_AddAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_addButton.frame=CGRectMake(230,13,42,23);
		
		[cell.contentView addSubview:temp_addButton];
		
		NSDictionary *tempDict =[m_arrFrnds objectAtIndex:indexPath.row];  
		NSURL *loadingUrl = [NSURL URLWithString:[tempDict valueForKey:@"profile_image_url"]];
		
		AsyncImageView* asyncImage = (AsyncImageView*)[cell.contentView viewWithTag:999];
		[asyncImage loadImageFromURL:loadingUrl];
	}
	else
	{
		
		[(UILabel *)[cell.contentView viewWithTag:102] setText:
		 [NSString stringWithFormat:@"%@",[[m_arrNonFrnds objectAtIndex:indexPath.row] objectForKey:@"name"]]];
		
		UIButton *temp_inviteButton=[UIButton buttonWithType:UIButtonTypeCustom];
		[temp_inviteButton setImage:[UIImage imageNamed:@"btn_invite"] forState:UIControlStateNormal];
		temp_inviteButton.tag=indexPath.row;
		//NSLog(@"indexPath.rowwwwwww : %d",indexPath.row);
		
		[temp_inviteButton addTarget: self action:@selector(btnInviteAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_inviteButton.frame=CGRectMake(250,13,43,24);
		[cell.contentView addSubview:temp_inviteButton];
		
		NSDictionary *tempDict =[m_arrNonFrnds objectAtIndex:indexPath.row];  
		NSURL *loadingUrl = [NSURL URLWithString:[tempDict valueForKey:@"profile_image_url"]];
		
		AsyncImageView* asyncImage = (AsyncImageView*)[cell.contentView viewWithTag:999];
		[asyncImage loadImageFromURL:loadingUrl];
		
		
	}
	return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section 
{ 
	if (section==0 && m_arrFrnds.count>0)
		return 30;
	else if(section==0 && m_arrFrnds.count==0)
		return 0;
	if (section==1 && m_arrNonFrnds.count>0)
		return 30;
	else if(section==1 && m_arrNonFrnds.count==0)
		return 0;
	
    // GDL: We shouldn't get here without returning something.
    return 0;
} 

#pragma mark -
#pragma mark textView delegate Methods 


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	if ([text isEqualToString:@"\n"]) 
	{
		[l_textView resignFirstResponder];
	}
	return YES;
}



#pragma mark -
#pragma mark custom Methods

-(IBAction)btnCloseOnThreeButtonViewAction
{
	[l_textView resignFirstResponder];
	[m_dialog_view setHidden:YES];
	table_AddFriends.userInteractionEnabled=YES;
	searchB.userInteractionEnabled=YES;
}

-(void)btnInviteAction:(id)sender
{
	NSLog(@"tag_value : %d",[sender tag]);
	tag_Val=[sender tag];
	[m_dialog_view setHidden:NO];
	table_AddFriends.userInteractionEnabled=NO;
	searchB.userInteractionEnabled=NO;
	
}


-(void)dialogBoxForInvite:(id)sender
{
	[m_dialog_view setHidden:YES];
	[m_dialog_view setBackgroundColor:[UIColor colorWithRed:0.32 green:0.76 blue:0.80 alpha:1]];
	
	UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 27, 220, 20)];
	[tempLabel setText:@"Send invitation to friend."];
	[tempLabel setBackgroundColor:[UIColor clearColor]];
	[tempLabel setTextColor:[UIColor whiteColor]];
	[tempLabel setFont:[UIFont fontWithName:kFontName size:16]];
	[m_dialog_view addSubview:tempLabel];
	[tempLabel release];
	
	
	l_textView = [[UITextView alloc] initWithFrame:CGRectMake(20, 50, 220,80)];
	l_textView.delegate=self;
	l_textView.text=kTellAFriendMailBody3;
	[l_textView.layer setCornerRadius:6];
	l_textView.backgroundColor=[UIColor whiteColor];
	
	[m_dialog_view addSubview:l_textView];
	
	UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(235,3,20,20); // position in the parent view and set the size of the button
    [closeBtn setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
 	
    [closeBtn addTarget:self action:@selector(btnCloseOnThreeButtonViewAction) forControlEvents:UIControlEventTouchUpInside];
    [m_dialog_view addSubview:closeBtn];
	
	UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(20,150,100,28); // position in the parent view and set the size of the button
    [cancelBtn setBackgroundImage:[UIImage imageNamed:@"blue_cancelButton.png"] forState:UIControlStateNormal];
 
    [cancelBtn addTarget:self action:@selector(btnCloseOnThreeButtonViewAction) forControlEvents:UIControlEventTouchUpInside];
 
    [m_dialog_view addSubview:cancelBtn];
	
	UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBtn.frame = CGRectMake(140, 150, 100, 28); // position in the parent view and set the size of the button
    [sendBtn setBackgroundImage:[UIImage imageNamed:@"send_green_btn.png"] forState:UIControlStateNormal];    // add targets and actions
    [sendBtn addTarget:self action:@selector(btnSendAction:) forControlEvents:UIControlEventTouchUpInside];
    [m_dialog_view addSubview:sendBtn];
	
	[self.view addSubview:m_dialog_view];
	
}


-(void)btn_AddAction:(id)sender
{
	NSLog(@"Pressing add button");
	m_intCurrentTwitterId=[sender tag];
	[self sendAddFriendRequest];
	
}

-(void)addAllBtnPressed:(id)sender
{
	NSLog(@"Pressing add All button");
	m_intCurrentTwitterId=-1;
	if (m_shopBeeArray.count>0) {
		[self sendAddFriendRequest];
	}
	
}

-(void)btnSendAction:(id)sender//dialogBoxForInvite
{
	[searchB resignFirstResponder];
		
	//m_indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(100,100 , 30, 30)];
//	
//	[m_dialog_view addSubview:m_indicatorView];
//	[m_indicatorView startAnimating];
	[[LoadingIndicatorView SharedInstance] startLoadingView:self];
	
	NSString *temp_str;
	temp_twitterPross.m_TwitterFrndsVC=self;
	temp_str = [NSString stringWithFormat:@"%@",[[m_nonshopBeeArray objectAtIndex:tag_Val] objectForKey:@"id"]];
	temp_twitterPross.m_TextViewData=l_textView.text;
	
	[m_dialog_view setHidden:YES];
	
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator startLoadingView:self];
	
	[temp_twitterPross btnInviteAction: temp_str];	
	table_AddFriends.userInteractionEnabled=YES;
	searchB.userInteractionEnabled=YES;

}

-(void)hideProcessing
{
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	//[m_indicatorView stopAnimating];
	[m_dialog_view setHidden:YES];
}

-(void)btnInviteAllAction:(id)sender
{
	//TwitterProcessing *temp_twitterPross=[[TwitterProcessing alloc]init];
//	//temp_twitterPross.tw=self;
//	[temp_twitterPross btnTellFriendByTwitterAction:6];	//2: for getting friends list, 5: sharing sale from wherebox, 6: inviting friends from mypage module
}

-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage
{
	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[tempAlert show];
	[tempAlert release];
	tempAlert=nil;
}

-(void)sendAddFriendRequest
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	//m_view.hidden=NO;
//	m_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
//	m_view.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
//	
//	m_indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(140,200 , 30, 30)];
//	
//	[m_view addSubview:m_indicatorView];
//	[self.view addSubview:m_view];
//	[m_indicatorView startAnimating];
	[[LoadingIndicatorView SharedInstance] startLoadingView:self];	
	l_request=[[ShopbeeAPIs alloc] init];
	
	NSMutableString *tempString;
	
	if (m_intCurrentTwitterId>=0)//incase of individual users add request
	{
		tempString= [NSMutableString stringWithFormat:@"[\"%@\"]",[[m_arrFrnds objectAtIndex:m_intCurrentTwitterId] valueForKey:@"emailId"]];
		
		NSLog(@"%@",tempString);
	}
	else if(m_intCurrentTwitterId==-1)//incase of add all 
	{
		NSMutableArray *tempArray=[[NSMutableArray alloc]init];
		for (int i=0; i<m_arrFrnds.count; i++)
		{
			[tempArray addObject:[[m_arrFrnds objectAtIndex:i]valueForKey:@"emailId"]];
		}
		tempString=[NSString stringWithString:[tempArray JSONRepresentation]];
		[tempArray release];
		NSLog(@"tempString: %@",tempString);			
		
	}
	
	[l_request sendRequestToAddFriend:@selector(requestCallBackMethodForAddFriend:responseData:) tempTarget:self custId:customerID tmpUids:tempString];//[m_shopBeeArray objectAtIndex:[sender tag]]
	
	[l_request release];
	l_request=nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
-(IBAction) cancelButton:(id)sender
{
	[searchB resignFirstResponder];
	table_AddFriends.userInteractionEnabled=YES;
	searchB.userInteractionEnabled=YES;
}


-(IBAction)btnBackAction:(id)sender
{
	
	[self.navigationController popViewControllerAnimated:YES];
	
}
-(void)load_tableViewDataSource{
	
	m_arrNonFrnds=[[NSArray alloc]initWithArray:m_nonshopBeeArray];
	m_arrFrnds=[[NSMutableArray alloc] initWithArray:m_shopBeeArray];
	[self.table_AddFriends reloadData];
	
}
#pragma mark -
#pragma mark call back methods

-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	//NSLog(@"Friend added. response code: %d, response string:%@",[responseCode intValue],[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
	
	int  temp_responseCode=[responseCode intValue];
	
	if (temp_responseCode==500 || temp_responseCode==200)
	{
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
			
		l_request=[[ShopbeeAPIs alloc] init];
		
		[l_request getRegisteredTwitterUsers:@selector(requestCallBackMethodForListIds:responseData:) tempTarget:self customerid:customerID listids:m_strUids];
		
		[l_request release];
		l_request=nil;
					
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

	
	//[m_indicatorView stopAnimating];
	//[m_view removeFromSuperview];
		
}

-(void)requestCallBackMethodForListIds:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	
	int  temp_responseCode=[responseCode intValue];
	//[m_view removeFromSuperview];
//	[m_indicatorView stopAnimating];
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	 if(temp_responseCode==200)
	{
		//NSLog(@"responseCode is 200");
		
		NSString *str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding]autorelease];
	//	NSLog(@"%@",str);
		id response=[str JSONValue]; 
	//	NSLog(@"%@",response);
		NSMutableArray	*tempArray_Uids=[[NSMutableArray alloc]init];
					
		for(int i=0;i<[response count];i++)
		{
			[tempArray_Uids addObject:[[response objectAtIndex:i]valueForKey:@"shopbee"]];
		}
		
		//NSLog(@"twitter FashionGram >>%@",tempArray_Uids);
		NSArray *arr = [tempArray_Uids objectAtIndex:0];
	//	NSLog(@"%@",m_arrFrndsDic);
		BOOL tempStatus;
		tempStatus=FALSE;
		NSString *tempString;
		
		for(int j=0;j<[m_arrFrndsDic count];j++)
		{
			for(int i=0;i<[arr	count];i++)
			{	
				if([[[m_arrFrndsDic objectAtIndex:j] objectForKey:@"id"] isEqualToString:[[arr objectAtIndex:i] objectForKey:@"twitter"]])
				{
					tempStatus=TRUE;
					tempString=[NSString stringWithString:[[arr objectAtIndex:i] valueForKey:@"email"]];
					break;
				
				}
				else
				{
					tempStatus=FALSE;
				}
			}
				if (tempStatus==TRUE)
				{
					
					NSMutableDictionary *tempDict=[NSMutableDictionary dictionaryWithDictionary:[m_arrFrndsDic objectAtIndex:j]];
					[tempDict setValue:tempString forKey:@"emailId"];
					[m_shopBeeArray addObject:tempDict];
					tempStatus=FALSE;
				}
				else 
				{
					[m_nonshopBeeArray addObject:[m_arrFrndsDic objectAtIndex:j]];
				}
		}
		
	//	NSLog(@"FashionGram count: %d, nonshopbee count: %d",m_shopBeeArray.count, m_nonshopBeeArray.count);
	//	NSLog(@"FashionGram %@ ....... nonshopbee %@",m_shopBeeArray,m_nonshopBeeArray);
		
		[table_AddFriends reloadData];
		//[self performSelector:@selector(loadImagesForOnscreenRows) withObject:nil afterDelay:0.5];
		
	}
	 else             
         [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	
	[self load_tableViewDataSource];

	
}

-(void)requestCallBackMethodForAddFriend:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	int  temp_responseCode=[responseCode intValue];
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	//NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	//
//	[m_indicatorView stopAnimating];
//	[m_view removeFromSuperview];
//	[m_view release];
//	m_view=nil;
//	[m_indicatorView release];
//	m_indicatorView=nil;
//	
	if (temp_responseCode==500)
	{
		NSLog(@"responseCode is 500");
		
		[self showAlertView:@"Error!" alertMessage:@"Unable to send request.This user may be already in your friends list."];
		
		if (m_intCurrentTwitterId>=0 && m_shopBeeArray.count>0) {
			[m_arrFrnds removeObjectAtIndex:m_intCurrentTwitterId];
			[self.table_AddFriends reloadData];
		}
		
		
	}
	else if(temp_responseCode==200)//tempArray_Uids
	{
		NSLog(@"responseCode is 200");
		
		if (m_intCurrentTwitterId>=0 && m_shopBeeArray.count>0)
		{
			[self showAlertView:@"Request sent" alertMessage:@"Friend request has beent sent."];
			[m_arrFrnds removeObjectAtIndex:m_intCurrentTwitterId];
			[self.table_AddFriends reloadData];
		}
		else if(m_intCurrentTwitterId==-1)
		{
			[self showAlertView:@"Request sent" alertMessage:@"Friend request has beent sent to all twitter friends."];
			[m_arrFrnds removeAllObjects];
			[self.table_AddFriends reloadData];
		}
		
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	
	
}

#pragma mark -
#pragma mark search Bar delegate methods

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
	[searchB resignFirstResponder];
	[searchB setShowsCancelButton:NO animated:YES];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)lclSearchBar
{
	m_arrNonFrnds=[m_nonshopBeeArray retain];
	m_arrFrnds=[m_shopBeeArray retain];
	NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@",searchB.text];
	NSArray *filteredNameList = [m_arrNonFrnds filteredArrayUsingPredicate:bPredicate];
	NSArray *filterednameList1=[m_arrFrnds filteredArrayUsingPredicate:bPredicate];
	NSLog(@"ARRAY=%@",filteredNameList);
	m_arrNonFrnds=[filteredNameList retain];
	m_arrFrnds=[filterednameList1 retain];
	[self.table_AddFriends reloadData];
	[searchB resignFirstResponder];
	[searchB setShowsCancelButton:NO animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	if(searchB.text.length==0)
	{
		[self performSelector:@selector(load_tableViewDataSource)];
		
		[self.table_AddFriends reloadData];
		
		[searchB resignFirstResponder];
	}
	[searchB setShowsCancelButton:YES animated:YES];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
	[searchB setShowsCancelButton:YES animated:YES];
	
	return YES;
	
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_dialog_view = nil;
    self.table_AddFriends = nil;
    self.searchB = nil;
}


- (void)dealloc {
    
    [table_AddFriends release];
    [m_arrTwitterFriends release];
    [m_arrFrndsDic release];
    [temp_arrFrndsDic release];
    [searchB release];
    [arr_str release];
    [m_view release];
    [m_indicatorView release];
    [m_strUids release];
    [m_shopBeeArray release];
    [m_nonshopBeeArray release];
    [temp_twitterPross release];
    [m_picUrls release];
    [m_dialog_view release];
    [m_arrNonFrnds release];
    [m_arrFrnds release];
    
    [super dealloc];
}

@end
