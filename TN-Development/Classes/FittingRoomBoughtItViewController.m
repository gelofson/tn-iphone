//
//  FittingRoomBoughtItViewController.m
//  QNavigator
//
//  Created by softprodigy on 28/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FittingRoomBoughtItViewController.h"
#import<QuartzCore/QuartzCore.h>
#import"Constants.h"
#import"AsyncButtonView.h"
#import"ShopbeeAPIs.h"
#import"LoadingIndicatorView.h"
#import"BuyItCommentViewController.h"
#import"CategoryModelClass.h"
#import"AsyncImageView.h"
#import"QNavigatorAppDelegate.h"
#import "ViewAFriendViewController.h"
#import"MyPageViewController.h"
#import "UIView+Origin.h"

#define SUBJECT_MIN_HEIGHT 15

#define PICTURE_SIZE    240


@implementation FittingRoomBoughtItViewController
int i,k=4,textView_Y=4,imgView_Y=200,m=0,messageID1;

@synthesize m_lableButItOrNot, m_Namelbl;
@synthesize m_CommentButton;
@synthesize m_LikeBtn;
@synthesize m_WishListBtn;
@synthesize m_IboughtItBtn;
@synthesize m_ChangeInMindBtn;
@synthesize m_mainArray;
@synthesize m_PictureView;
@synthesize m_BigPicView;
@synthesize MineTrack;
@synthesize m_StrMessageId;
@synthesize UserName;
@synthesize m_CallbackViewController;
@synthesize m_backgroundImageView;
@synthesize m_FollowLabel;
@synthesize m_FollowBtn;
@synthesize m_messageIDsBoughtItArray;
@synthesize m_currentMessageID;
@synthesize m_MineCommentButton;
@synthesize m_likeLabel;
@synthesize m_scrollViewMoreInfo;
@synthesize headerLabel;
@synthesize whoWhatTextView;
@synthesize whoWhattitleTextView;
@synthesize m_leftArrow, m_rightArrow;

LoadingIndicatorView *l_boughtItIndicatorView; 

ShopbeeAPIs *l_requestObj;

CategoryModelClass *l_objCatModel;

AsyncButtonView *asyncProductButton,*asyncButton;

QNavigatorAppDelegate *l_appDelegate;


#pragma mark -
#pragma mark Custom
-(IBAction) goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(NSString *)formatWhoWhattitle:(NSDictionary*)datatitle
{
//    if ([[datatitle objectForKey:@"oped"] length] > 0)
//        return [datatitle objectForKey:@"oped"];
    
    
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    NSString *data = [datatitle objectForKey:@"who"];
    if ([data length] > 0) {
        
        [string appendFormat:@"•%@:\n",@"Who"];
    }
    data = [datatitle objectForKey:@"what"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"What"];
        
    }
    data = [datatitle objectForKey:@"when"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"When"];
    }
    data = [datatitle objectForKey:@"where"];
    if ([data length] > 0) {
        if ([data length]<=24)
        {
            [string appendFormat:@"•%@:\n",@"Where"];
        }
        else
        {
        [string appendFormat:@"•%@:\n\n",@"Where"];
        }
        
    }
    data = [datatitle objectForKey:@"how"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"How"];
        
    }
    data = [datatitle objectForKey:@"why"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"Why"];
    }
    return string;
    
}


- (NSString *) formatWhoWhatText:(NSDictionary *)dataDict
{
    if ([[dataDict objectForKey:@"oped"] length] > 0)
        return [dataDict objectForKey:@"oped"];
    
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    NSString *data = [dataDict objectForKey:@"who"];
    if ([data length] > 0) {
        [string appendFormat:@"%@\n", data];
    }
    data = [dataDict objectForKey:@"what"];
    if ([data length] > 0) {
        [string appendFormat:@"%@\n", data];
    }
    data = [dataDict objectForKey:@"when"];
    if ([data length] > 0) {
        [string appendFormat:@" %@\n",[self dateReFormated:data]];
    }
    data = [dataDict objectForKey:@"where"];
    if ([data length] > 0) {
        [string appendFormat:@"%@\n", data];
    }
    data = [dataDict objectForKey:@"how"];
    if ([data length] > 0) {
        [string appendFormat:@"%@\n", data];
    }
    data = [dataDict objectForKey:@"where"];
    if ([data length] > 0) {
        [string appendFormat:@"%@\n", data];
    }
    return string;
}
-(NSString*)dateReFormated:(NSString*)date
{
 NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    if ([date intValue])
    {
        NSArray *mainarray=[date componentsSeparatedByString:@" "];
        NSArray *timearray=[[mainarray objectAtIndex:1]componentsSeparatedByString:@":"];
        NSString *timeformate=[NSString stringWithFormat:@"%@ %@:%@",[mainarray objectAtIndex:0],[timearray objectAtIndex:0],[timearray objectAtIndex:1]];
        NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
        [dateFormate setDateFormat:@"YYYY:mm:dd HH:mm"];
        NSDate *dateValue=[dateFormate dateFromString:timeformate];
        [dateFormate setDateFormat:@"MMM dd, HH:mm a"];
        [string appendFormat:@"%@",[dateFormate stringFromDate:dateValue]];
        [dateFormate release]; 
        //return string;
    }
    else
    {
        [string appendFormat:@"%@",date];  
    }
    return string;
}
-(IBAction)btnMoreAction:(id)sender
{
	NSDictionary *tmp_dictionary=[m_mainArray objectAtIndex:0];
	NSArray *messageResponseArray=[tmp_dictionary valueForKey:@"messageResponseList"];
	int Count=[messageResponseArray count];
	
	m_scrollViewMoreInfo.tag=1001;
	m_scrollViewMoreInfo.contentOffset = CGPointMake(0,0);
	m_scrollViewMoreInfo.backgroundColor = [UIColor whiteColor];
	
	UISwipeGestureRecognizer *recognizer; // To detect the swipe on the scroll view.
	
	
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	[m_scrollViewMoreInfo addGestureRecognizer:recognizer];
	recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:UISwipeGestureRecognizerDirectionRight];
	//recognizer.delaysTouchesBegan = TRUE;
	
	[m_scrollViewMoreInfo addGestureRecognizer:recognizer];	
	
//=================== Adding product picture
    float imageYPos = 24;
	asyncProductButton = [[AsyncButtonView alloc] initWithFrame:CGRectMake(32, imageYPos, PICTURE_SIZE, PICTURE_SIZE)];
	asyncProductButton.tag=1000;
    asyncProductButton.layer.shadowColor = [UIColor grayColor].CGColor;
    asyncProductButton.layer.shadowOffset = CGSizeMake(1, -1);
    asyncProductButton.layer.shadowOpacity = 0.5;
	//asyncProductButton.isCatView=YES;
	
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[tmp_dictionary valueForKey:@"productImageUrl"]];
	//Bharat: 11/23/11: US44 - Check if product info exists, if yes, show the image2
	id wsProductDictionary = [tmp_dictionary valueForKey:@"wsProduct"];
	NSString * prodDiscountInfoStr = nil;
	NSString * prodTagLineStr = nil;
	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
		id imageUrlsArr = [((NSDictionary *)wsProductDictionary) valueForKey:@"imageUrls"];
		if ((imageUrlsArr != nil) && ([imageUrlsArr isKindOfClass:[NSArray class]] == YES) && 
            ([((NSArray *)imageUrlsArr) count] > 1) && ([[imageUrlsArr objectAtIndex:1] length] > 1)) {
			productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[((NSArray *)imageUrlsArr) objectAtIndex:1]];
		}	
		id discInf = [((NSDictionary *)wsProductDictionary) valueForKey:@"discountInfo"];
		if ((discInf != nil) && ([discInf isKindOfClass:[NSString class]] == YES)) {
			prodDiscountInfoStr=[NSString stringWithFormat:@"%@",discInf];
		}	
		id tagLine = [((NSDictionary *)wsProductDictionary) valueForKey:@"productTagLine"];
		if ((tagLine != nil) && ([tagLine isKindOfClass:[NSString class]] == YES)) {
			prodTagLineStr=[NSString stringWithFormat:@"%@",tagLine];
		}	
	}
    
    
	[asyncProductButton loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked:) btnText:@""];
	[m_scrollViewMoreInfo addSubview:asyncProductButton];
	
//================  Adding story texts - headline & who, what...
    NSDictionary *temp_text_dict=[tmp_dictionary valueForKey:@"wsBuzz"];
	headerLabel.text = [temp_text_dict objectForKey:@"headline"];
    whoWhatTextView.text = [self formatWhoWhatText:temp_text_dict];
    whoWhattitleTextView.text=[self formatWhoWhattitle:temp_text_dict];
	CGFloat yHeight = imageYPos + PICTURE_SIZE + 10;
    if ([whoWhatTextView.text length] > 0) {
        [whoWhatTextView sizeToFit];
		whoWhatTextView.frame = CGRectMake(whoWhatTextView.frame.origin.x, whoWhatTextView.frame.origin.y, whoWhatTextView.frame.size.width, whoWhatTextView.contentSize.height);
        
        [whoWhattitleTextView sizeToFit];
        whoWhattitleTextView.frame=CGRectMake(whoWhattitleTextView.frame.origin.x, whoWhattitleTextView.frame.origin.y, whoWhattitleTextView.frame.size.width, whoWhattitleTextView.contentSize.height);
        yHeight = whoWhatTextView.frame.origin.y + whoWhatTextView.frame.size.height + 10;
    }
    
//=================  Adding originator name
	UserName=[tmp_dictionary valueForKey:@"originatorName"];	
	m_Namelbl.text=[NSString stringWithFormat:@"%@:",UserName];
	[m_Namelbl setNumberOfLines:1];
	[m_Namelbl setBackgroundColor:[UIColor clearColor ]];
	self.m_Namelbl.font = [UIFont fontWithName:kMyriadProRegularFont size:13];
	[m_Namelbl sizeToFit];
    [self.m_Namelbl changeViewYOriginTo:yHeight];
    
//=================  Adding originator picture
	NSString *Url=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_mainArray objectAtIndex:0]valueForKey:@"originatorThumbImageUrl"]];
	
	NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
	asyncButton = [[[AsyncButtonView alloc]
                    initWithFrame:CGRectMake(32,yHeight,48,48)] autorelease];
	[asyncButton setTag:10001];
	asyncButton.isCatView=YES;
    asyncButton.layer.shadowColor = [UIColor grayColor].CGColor;
    asyncButton.layer.shadowOffset = CGSizeMake(1, -1);
    asyncButton.layer.shadowOpacity = 0.5;
	[asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(ProductBtnClicked:) btnText:@""];
	//asyncButton.backgroundColor=[UIColor whiteColor];
	[m_scrollViewMoreInfo addSubview:asyncButton];
	
//=================  Adding likes text
	if ([[tmp_dictionary valueForKey:@"favourCount"] intValue]>1) 
	{
		m_likeLabel.text=[NSString stringWithFormat:@"%d Likes",[[tmp_dictionary valueForKey:@"favourCount"] intValue] ];
	}
	else {
		m_likeLabel.text=[NSString stringWithFormat:@"%d Like",[[tmp_dictionary valueForKey:@"favourCount"] intValue] ];
		
	}
	
	m_likeLabel.textColor=[UIColor darkGrayColor];
	m_likeLabel.backgroundColor=[UIColor clearColor];
    [self.m_likeLabel changeViewYOriginTo:yHeight];
    
//=================  Adding text subject
	NSDictionary *temp_dictionary=[tmp_dictionary valueForKey:@"wsMessage"];
	m_messageId=[[temp_dictionary valueForKey:@"id"] intValue];
	m_StrMessageId=[temp_dictionary valueForKey:@"id"];
	m_messageType=[temp_dictionary valueForKey:@"messageType"];
	m_textViewSubject.text=[temp_dictionary valueForKey:@"bodyMessage"];
	//m_textViewSubject.textColor=[UIColor blackColor];
	m_textViewSubject.textColor=[UIColor darkGrayColor];
	m_textViewSubject.editable = NO;
	m_textViewSubject.scrollEnabled = YES;
	m_textViewSubject.bounces = NO;
	//m_textViewSubject.font=[UIFont fontWithName:kFontName size:12];
	[m_textViewSubject setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_SIZE_KEY]]];
	m_textViewSubject.backgroundColor=[UIColor clearColor];
	m_textViewSubject.autoresizingMask = UIViewAutoresizingFlexibleHeight;
	//[m_scrollViewMoreInfo addSubview:m_textViewSubject];
	[m_textViewSubject sizeToFit];
    [m_textViewSubject changeViewYOriginTo:m_Namelbl.frame.origin.y +
     m_Namelbl.frame.size.height + 2];
	float extra_height = 0.0f;
	if (m_textViewSubject.contentSize.height > SUBJECT_MIN_HEIGHT) {
		extra_height = m_textViewSubject.contentSize.height - SUBJECT_MIN_HEIGHT;
		m_textViewSubject.frame = CGRectMake(m_textViewSubject.frame.origin.x, m_textViewSubject.frame.origin.y, m_textViewSubject.frame.size.width, m_textViewSubject.contentSize.height);
	}
	//[tmp_SubjectLabel  release];
    //	tmp_SubjectLabel=nil;
	
#if ADD_ELAPSED_TIME
	//Bharat: 11/17/11: Elapsed time label fixed now
	//m_elapsedTimelabel=[[UILabel alloc] initWithFrame:CGRectMake(73,22+SUBJECT_MIN_HEIGHT-10+extra_height, 90, 20)];
	m_elapsedTimelabel=[[UILabel alloc] initWithFrame:CGRectMake(m_Namelbl.frame.origin.x+m_Namelbl.frame.size.width+10,5, 120, 20)];
	m_elapsedTimelabel.text = nil;
#if 0
Bharat: DE116: Commented out for now (so that user does not see the timeline nad reject app for old updates)
	m_elapsedTimelabel.text=[temp_dictionary valueForKey:@"elapsedTime"];
#endif
	m_elapsedTimelabel.textColor=[UIColor grayColor];
	m_elapsedTimelabel.textAlignment=UITextAlignmentLeft;
	m_elapsedTimelabel.font=[UIFont fontWithName:kHelveticaBoldFont size:12];
	m_elapsedTimelabel.backgroundColor=[UIColor clearColor];
	[m_scrollViewMoreInfo addSubview:m_elapsedTimelabel];
#endif
	
#ifdef ADD_DISCOUNT_INFO
	
	yHeight = imageYPos + PICTURE_SIZE + 10;
	//Bharat: 11/24/11: Adding discount info 
	if ((prodDiscountInfoStr != nil) && ([prodDiscountInfoStr length] > 0)) {
		UILabel *temp_lblDiscount=[[UILabel alloc]initWithFrame:CGRectMake(50,yHeight,215,20)]; //190,5,85,20)];
		temp_lblDiscount.numberOfLines = 1;
		[temp_lblDiscount setText:prodDiscountInfoStr]; //@"10% discount"];
		[temp_lblDiscount setTextAlignment:UITextAlignmentCenter];
		[temp_lblDiscount setTextColor:[UIColor colorWithRed:.50f green:.70f blue:.69f alpha:1.0f]];
		[temp_lblDiscount setBackgroundColor:[UIColor clearColor]];
		[temp_lblDiscount setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_DISCOUNTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_DISCOUNTLABEL_FONT_SIZE_KEY]]];
		//[m_scrollViewMoreInfo addSubview:temp_lblDiscount];
		[temp_lblDiscount release];
		yHeight += (20+5);
	}
	
	//Bharat: 11/24/11: Adding tagline info 
	if ((prodTagLineStr != nil) && ([prodTagLineStr length] > 0)) {
		UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(50,yHeight,215,20)]; //190,5,85,20)];
		temp_lblTitle.numberOfLines = 1;
		[temp_lblTitle setText:prodTagLineStr]; //@"10% discount"];
		[temp_lblTitle setTextAlignment:UITextAlignmentCenter];
		[temp_lblTitle setTextColor:[UIColor colorWithRed:.50f green:.70f blue:.69f alpha:1.0f]];
		[temp_lblTitle setBackgroundColor:[UIColor clearColor]];
		[temp_lblTitle setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];
		//[m_scrollViewMoreInfo addSubview:temp_lblTitle];
		[temp_lblTitle release];
		yHeight += (5+20);
	}
#endif	
    
	//Bharat: 11/24/11: Adjust height automatically
	//imgView_Y=350 + extra_height;
    yHeight = m_textViewSubject.frame.origin.y + m_textViewSubject.frame.size.height + 20;
	imgView_Y=yHeight;

	int counter;
	counter=0;
	for(int a=0;a<Count;a++)
	{
		
		
		NSString	*temp_string=[[messageResponseArray objectAtIndex:a] valueForKey:@"responseMessageBody"];
		if (![temp_string isEqualToString:@""]) 
		{
			UIImageView *temp_imgView2=[[UIImageView alloc]initWithFrame:CGRectMake(10, imgView_Y, 274, 59)];
			temp_imgView2.tag=32;
			UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(40, 0, 190, 40)];
			textView.tag=33;
			textView.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
			textView.font = [UIFont fontWithName:kFontName size:13];
			textView.backgroundColor = [UIColor clearColor];
			textView.delegate = self;
			textView.text=temp_string;
			textView.returnKeyType = UIReturnKeyDefault;
			textView.keyboardType = UIKeyboardTypeDefault; 
			textView.scrollEnabled = YES;
			textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
			textView.editable=YES; 
			textView.userInteractionEnabled=YES;
			
			
			if (counter%2==0) 
			{
				temp_imgView2.frame=CGRectMake(10, imgView_Y, 274, 25+textView.frame.size.height);
				temp_imgView2.image=[[UIImage imageNamed:@"white_message_box.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
			}
			else 
			{
				imgView_Y=imgView_Y-25;
				temp_imgView2.frame=CGRectMake(10, imgView_Y, 274, 25+textView.frame.size.height);
				temp_imgView2.image=[[UIImage imageNamed:@"white_message_box_rotated.png"] stretchableImageWithLeftCapWidth:50 topCapHeight:30];
			}
			
			
			[temp_imgView2 addSubview:textView];
			
			[m_scrollViewMoreInfo addSubview:temp_imgView2];
			
			temp_imgView2.frame=CGRectMake(10, imgView_Y, 274, 25+textView.contentSize.height);
			
			
			textView.frame =CGRectMake(40, 0, 190,textView.contentSize.height);
			textView.frame =CGRectMake(40, 0, 190,textView.contentSize.height);
			
			if (counter%2!=0) 
			{
				textView.frame =CGRectMake(40, 12, 190,textView.contentSize.height);
				textView.frame =CGRectMake(40, 12, 190,textView.contentSize.height);
			}
			
			
			UILabel *tmp_elapsedTime=[[UILabel alloc] initWithFrame:CGRectMake(47, textView.contentSize.height -5, 190, 12)];
			
			if (counter%2!=0) 
			{
				tmp_elapsedTime.frame=CGRectMake(47, textView.contentSize.height +8, 190, 12);
				
			}
			
			//Bharat: DE116: Commented out for now (so that user does not see the timeline nad reject app for old updates)
			//tmp_elapsedTime.text=[NSString stringWithFormat:@"Added %@ by %@ ",[[messageResponseArray objectAtIndex:a]valueForKey:@"elapsedTime"],[[messageResponseArray objectAtIndex:a]valueForKey:@"responseCustomerName"]];
			tmp_elapsedTime.text=[NSString stringWithFormat:@"Added by %@ ",[[messageResponseArray objectAtIndex:a]valueForKey:@"responseCustomerName"]];
			tmp_elapsedTime.backgroundColor=[UIColor clearColor];	
			tmp_elapsedTime.textColor=[UIColor lightGrayColor];
			tmp_elapsedTime.font=[UIFont fontWithName:kFontName size:10];
			
			[temp_imgView2 addSubview:tmp_elapsedTime];
			//[tmp_elapsedTime setFrame:CGRectMake(30, textView.contentSize.height -5, 190, 12)];
			
			[tmp_elapsedTime release];
			tmp_elapsedTime=nil;
			
			imgView_Y=imgView_Y+textView.contentSize.height+40;
			
			AsyncImageView* SenderImage = [[[AsyncImageView alloc]
											initWithFrame:CGRectMake(32,300,48,48)] autorelease];
			
			if (counter%2!=0) 
			{
				SenderImage.frame=CGRectMake(5, 20, 35, 35);
			}
			
			[temp_imgView2 addSubview:SenderImage];
			NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[messageResponseArray objectAtIndex:a] valueForKey:@"thumbCustomerUrl"]];
			NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,60,32]];
			//((UIImageView *)[asyncImage viewWithTag:101]).image=[UIImage imageNamed:@"loading.png"];
			[SenderImage loadImageFromURL:tempLoadingUrl ];
			
			
			NSLog(@"imgViewY>>>>>>>>>>>>>>>>%d",imgView_Y);
			//textViewY=textViewY+4;
			NSLog(@"iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii>>>>>>>>%d",i);
			[temp_imgView2 release];
			temp_imgView2=nil;
			[textView release];
			textView=nil;
			
			counter++;
		}
	}
	//Bharat: 11/26/11: Set absolute value, do not set increments
	m_scrollViewMoreInfo.contentSize = CGSizeMake(m_scrollViewMoreInfo.contentSize.width,imgView_Y+10);
	//[self.view addSubview:m_scrollViewMoreInfo];
	
	/*// To add the arrow image on the scroll view 
     UIImageView *tmp_arrowImageView=[[UIImageView alloc] initWithFrame:CGRectMake(287,325, 17,17)];
     tmp_arrowImageView.image=[UIImage imageNamed:@"Arrow.png"];
     //tmp_arrowImageView.tag=5000;
     [self.view addSubview:tmp_arrowImageView];
     [tmp_arrowImageView release];
     tmp_arrowImageView = nil;
     */
    
    /*UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLeft setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"left_arrow" ofType:@"png"]] forState:UIControlStateNormal];
    [btnLeft setFrame:CGRectMake(20.0,325.0, 15.0,15.0)];
    [btnLeft addTarget:self action:@selector(leftClick) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:btnLeft];*/
    
    /*UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"right_arrow" ofType:@"png"]] forState:UIControlStateNormal];
    [btnRight setFrame:CGRectMake(285.0,325.0, 15.0,15.0)];
    [btnRight addTarget:self action:@selector(rightClick) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:btnRight];*/
    
    //	*text = @"hello sir,how are you...?? wanna buy this???";
	//	CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(240.0f, 480.0f) lineBreakMode:UILineBreakModeWordWrap];
	//	UIImage *balloon;
	//	temp_imgView2.frame = CGRectMake(320.0f - (size.width + 28.0f), 2.0f, size.width + 28.0f, size.height + 15.0f);
	//	balloon = [[UIImage imageNamed:@"white_message_box.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
	//	label.frame = CGRectMake(307.0f - (size.width + 5.0f), 8.0f, size.width + 5.0f, size.height);
	
	
	
	
	//[m_moreDetailView addSubview:m_scrollView];
	
	//[m_scrollViewMoreInfo release];
    //	m_scrollViewMoreInfo=nil;
	
	//Bharat: 11/17/11: Shifting by 10 picels to right
	//m_likeImageView=[[UIImageView alloc ]initWithFrame:CGRectMake(223, 10, 78, 22)];
	//m_likeImageView=[[UIImageView alloc ]initWithFrame:CGRectMake(239, 46, 78, 22)];
	
	//UIImageView *tmp_buyItimageView=[[UIImageView alloc]initWithFrame:CGRectMake(6, 8, 13, 11)];
	m_likeImageView.image=[UIImage imageNamed:@"star_like_rate_blue_bg.png"];
	m_likeImageView.backgroundColor=[UIColor clearColor];
	//[self.view addSubview:m_likeImageView];
	//[m_scrollViewMoreInfo addSubview:m_likeImageView];
	//m_scrollViewMoreInfo.clipsToBounds = NO;
	//[tmp_likeImageView addSubview:tmp_buyItimageView];
	//[tmp_buyItimageView release];
    //	tmp_buyItimageView=nil;
	
	
	
	
	//m_likeLabel=[[UILabel alloc] initWithFrame:CGRectMake(19,2, 50, 12)];
	if ([[tmp_dictionary valueForKey:@"favourCount"] intValue]>1) 
	{
		m_likeLabel.text=[NSString stringWithFormat:@"%d Likes",[[tmp_dictionary valueForKey:@"favourCount"] intValue] ];
	}
	else {
		m_likeLabel.text=[NSString stringWithFormat:@"%d Like",[[tmp_dictionary valueForKey:@"favourCount"] intValue] ];
		
	}
    
	
	m_likeLabel.textColor=[UIColor darkGrayColor];
	m_likeLabel.backgroundColor=[UIColor clearColor];
	//[m_likeImageView addSubview:m_likeLabel];

	//Bharat: 11/23/11: US44 - Adding sunburst and discount info to pop-over view
    wsProductDictionary = nil;
	wsProductDictionary = [tmp_dictionary valueForKey:@"wsProduct"];
	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
		id discountInfo = [((NSDictionary *)wsProductDictionary) valueForKey:@"discountInfo"];
		if ((discountInfo != nil) && ([discountInfo isKindOfClass:[NSString class]] == YES) && ([discountInfo length] > 0)) {
			UILabel *temp_lblDiscount=[[UILabel alloc]initWithFrame:CGRectMake(111,25,85,40)]; //190,5,85,20)];
			temp_lblDiscount.numberOfLines = 2;
			[temp_lblDiscount setText:discountInfo]; //@"10% discount"];
			[temp_lblDiscount setTextAlignment:UITextAlignmentCenter];
			[temp_lblDiscount setTextColor:[UIColor darkGrayColor]];
			[temp_lblDiscount setBackgroundColor:[UIColor clearColor]];
			[temp_lblDiscount setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_DISCOUNTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_DISCOUNTLABEL_FONT_SIZE_KEY]]];
			
			UIImageView *temp_yellowImgView=[[UIImageView alloc]init] ;
			temp_yellowImgView.contentMode=UIViewContentModeScaleAspectFit;
			if([discountInfo length]>7)
			{
				temp_yellowImgView.frame=CGRectMake(105,3,99,80);
				[temp_yellowImgView setImage:[UIImage imageNamed:@"yellow_tag.png"]];
				
			}
			else
			{
				[temp_yellowImgView setImage:[UIImage imageNamed:@"yellow_tag_small.png"]];
				temp_yellowImgView.frame=CGRectMake(113,5,83,79);
				temp_lblDiscount.frame=CGRectMake(118, 25, 70,40);
			}
			[m_BigPicView addSubview:temp_yellowImgView];
			[m_BigPicView addSubview:temp_lblDiscount];
			[temp_yellowImgView release];
			[temp_lblDiscount release];
		}
		id productTagLine = [((NSDictionary *)wsProductDictionary) valueForKey:@"productTagLine"];
		if ((productTagLine != nil) && ([productTagLine isKindOfClass:[NSString class]] == YES) && ([productTagLine length] > 0)) {
			UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10,390,290,60)];
			temp_lblTitle.lineBreakMode = UILineBreakModeWordWrap;
			temp_lblTitle.numberOfLines = 0;
			[temp_lblTitle setText:productTagLine];
			temp_lblTitle.textAlignment = UITextAlignmentCenter;
			[temp_lblTitle setTextColor:[UIColor whiteColor]];
			[temp_lblTitle setBackgroundColor:[UIColor clearColor]];
			[temp_lblTitle setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];

			[m_BigPicView addSubview:temp_lblTitle];
			[temp_lblTitle release];
		}
	}

	
}

-(IBAction) BtnLikeAction
{
	l_requestObj = [[ShopbeeAPIs alloc]init];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_UserID=[prefs valueForKey:@"userName"];
	[m_MainDictionary setValue:@"Y" forKey:@"isFavour"];
	[m_MainDictionary setValue:@"" forKey:@"responseWords"];
	[m_MainDictionary setObject:[NSNumber numberWithInt:m_messageId] forKey:@"messageId"];
	[m_MainDictionary setValue:m_messageType forKey:@"type"];
	[l_boughtItIndicatorView startLoadingView:self];
	[l_requestObj addResponseForFittingRoomRequest:@selector(CallBackMethod:responseData:) tempTarget:self userid:tmp_UserID msgresponse:m_MainDictionary];
	
	l_requestObj = nil;
	[l_requestObj release];
    
	//
    //	[m_MainDictionary setValue:@"Y" forKey:@"isFavour"];
    //	[m_MainDictionary setObject:[NSNumber numberWithInt:m_messageId] forKey:@"messageId"];
    //	[self showAlertView:nil alertMessage:@"Thanks! Would you like to leave a review for your friend?" tag:1 cancelButtonTitle:@"NO" otherButtonTitles:@"OK"];
    //	UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Thanks!Would you like to leave a review for your friend?" 
    //											 delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"OK",nil];
    //	[alert show];
    //	[alert setTag:1];
    //	[alert release];
}
-(IBAction) btnFlagItAction{
	UIActionSheet *alertSheet=[[UIActionSheet alloc] initWithTitle:@"Please flag with care" delegate:self cancelButtonTitle:@"Cancel" 
											destructiveButtonTitle:nil otherButtonTitles:@"Misorganised",@"Prohibited",@"Spam/Overpost",nil];
	[alertSheet setTag:1];
	[alertSheet showFromTabBar:self.tabBarController.tabBar];
	[alertSheet release];
	
	
	
}
-(IBAction) btnFollowAction:(id)sender
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_UserName=[prefs valueForKey:@"userName"];
	NSDictionary *tmp_dictionary=[[m_mainArray objectAtIndex:0] valueForKey:@"wsMessage"];
	NSString *tmp_originatorName=[tmp_dictionary valueForKey:@"sentFromCustomer"];	
	NSArray *tmp_array=[[NSArray alloc] initWithObjects:tmp_originatorName,nil];
	if ([tmp_UserName isEqualToString:tmp_originatorName])
	{
		UIAlertView *tmp_AlertView=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"You cannot Follow yourself." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_AlertView show];
		[tmp_AlertView release];
		tmp_AlertView=nil;
	}
	else 
	{
		[l_boughtItIndicatorView startLoadingView:self];
		[l_requestObj addFollowing:@selector(CallBackMethodforfollow:responseData:) tempTarget:self customerid:tmp_UserName followers:tmp_array];
	}
	l_requestObj = nil;
	[l_requestObj release];
    
	[tmp_array release];
	tmp_array=nil;
    follow = YES;
    [m_FollowBtn setImage:[UIImage imageNamed:@"unfollow_withtext"] forState:UIControlStateNormal];
    
}
-(IBAction)btnUnfollowAction:(id)sender
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_UserName=[prefs valueForKey:@"userName"];
	NSDictionary *tmp_dictionary=[[m_mainArray objectAtIndex:0] valueForKey:@"wsMessage"];
	NSString *tmp_originatorName=[tmp_dictionary valueForKey:@"sentFromCustomer"];	
	[l_boughtItIndicatorView startLoadingView:self];
	[l_requestObj unfollowAFriend:@selector(CallBackMethodforUnfollow:responseData:) tempTarget:self userid:tmp_UserName following:tmp_originatorName];
	l_requestObj = nil;
	[l_requestObj release];
    follow = NO;
    [m_FollowBtn setImage:[UIImage imageNamed:@"follow_icon"] forState:UIControlStateNormal];
    
}
-(IBAction)btnWishListAction
{
	UIActionSheet *alert=[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil
                                            otherButtonTitles:@"Leave it Public",@"Keep it Private",nil];
	[alert setTag:2];
	[alert showFromTabBar:self.tabBarController.tabBar];
	[alert release];
}

-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage tag:(NSInteger)Tagvalue cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles
{
	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles ,nil];
	tempAlert.tag=Tagvalue;
	[tempAlert show];
	[tempAlert release];
	tempAlert=nil;
}
-(void)addToWishListPrivate:(id)sender{
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_userName=[prefs valueForKey:@"userName"];
	NSString *tmp_visibility=@"private";
	NSDictionary *tmp_ResponseDict=[[m_mainArray objectAtIndex:0] valueForKey:@"wsMessage"];
	NSString *body_Message=[tmp_ResponseDict valueForKey:@"bodyMessage"];
	//NSString *tmp_subject=[tmp_ResponseDict valueForKey:@"subject"];
	//NSString *tmp_comments=[NSString stringWithFormat:@"%@%@",tmp_subject,body_Message];
	
	NSDictionary *tmp_dict=[NSDictionary dictionaryWithObjectsAndKeys:tmp_userName,@"custid",body_Message,@"comments",m_StrMessageId,@"msgid",tmp_visibility,@"visibility",nil];
    
	//..NSDictionary *tmp_dict=[NSDictionary dictionaryWithObjectsAndKeys:tmp_userName,@"custid",body_Message,@"comments",tmp_visibility,@"visibility",nil];
	NSLog(@"%@",tmp_dict);
	NSLog(@"%@",l_objCatModel.m_strId);
	[l_boughtItIndicatorView startLoadingView:self];
	l_requestObj = [[ShopbeeAPIs alloc]init];
	[l_requestObj addToWishListWithFittingRoomData:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	//..[l_requestObj addWishListWithoutProduct:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	l_requestObj = nil;
	[l_requestObj release];
    
	[l_objCatModel release];
	l_objCatModel=nil;
	
}

-(void)addToWishListPublic:(id)sender
{
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_userName=[prefs valueForKey:@"userName"];
	NSString *tmp_visibility=@"public";
	NSDictionary *tmp_ResponseDict=[[m_mainArray objectAtIndex:0] valueForKey:@"wsMessage"];
	NSString *body_Message=[tmp_ResponseDict valueForKey:@"bodyMessage"];
	//NSString *tmp_subject=[tmp_ResponseDict valueForKey:@"subject"];
	//NSString *tmp_comments=[NSString stringWithFormat:@"%@%@",tmp_subject,body_Message];
	NSLog(@"%@",m_StrMessageId);
	NSDictionary *tmp_dict=[NSDictionary dictionaryWithObjectsAndKeys:tmp_userName,@"custid",body_Message,@"comments",m_StrMessageId,@"msgid",tmp_visibility,@"visibility",nil];
	
	
	NSLog(@"%@",tmp_dict);
    //	NSLog(@"%@",l_objCatModel.m_strId);
	[l_boughtItIndicatorView startLoadingView:self];
	
	l_requestObj = [[ShopbeeAPIs alloc]init];
	
	// http://66.28.216.132/salebynow/json.htm?action=addToWishListWithFittingRoomData&wishlist={"custid":"test21@gmail.com","comments":"This Product is very good .. ","msgid":"22","visibility":"private"}
	
	[l_requestObj addToWishListWithFittingRoomData:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	//[l_requestObj addWishListWithoutProduct:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	l_requestObj = nil;
	[l_requestObj release];
    
	[l_objCatModel release];
	l_objCatModel=nil;
}


-(void)ProductBtnClicked:(id)sender
{
	
	if ([sender tag]==1000)
	{
		[m_BigPicView setFrame:CGRectMake(5, 25, 310, 452)];
		[l_appDelegate.window addSubview:m_BigPicView];
		UIImage *tempImage=[asyncProductButton.m_btnIcon backgroundImageForState:UIControlStateNormal];
		if (!tempImage) 
		{
			tempImage=[UIImage imageNamed:@"no-image"];
		}
		
		m_PictureView.image= tempImage;
		[self initialDelayEnded];
	}
	else if([sender tag]==10001)
	{
		//if (MineTrack==YES) 
        //		{
        //			MyPageViewController *temp_mypage=[[MyPageViewController alloc] init];
        //			temp_mypage.BackButtonTrack=YES;
        //			[self.navigationController pushViewController:temp_mypage animated:YES];
        //			[temp_mypage release];
        //			temp_mypage=nil;
        //		}
        //		else 
        //		{
		
        
        
		NSLog(@"go to mypage");
		ViewAFriendViewController *tempViewAFriend=[[ViewAFriendViewController alloc]init];
		NSDictionary *tmp_dictionary=[m_mainArray objectAtIndex:0];
		tempViewAFriend.m_Email=[[tmp_dictionary valueForKey:@"wsMessage"] valueForKey:@"sentFromCustomer"];
		tempViewAFriend.m_strLbl1=[tmp_dictionary valueForKey:@"originatorName"];
		tempViewAFriend.messageLayout = NO;
		[self.navigationController pushViewController:tempViewAFriend animated:YES];
		[tempViewAFriend release];
		tempViewAFriend=nil;
		//}
		
		
	}	 
	
	
}
-(void)initialDelayEnded
{
    m_BigPicView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    //m_BigPicView.alpha = 1.0;
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
	m_BigPicView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    [UIView commitAnimations];
}

- (void)bounce1AnimationStopped
{
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
	m_BigPicView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
    [UIView commitAnimations];
}

- (void)bounce2AnimationStopped
{
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	m_BigPicView.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
}
-(IBAction)CloseBtnAction
{
	[m_BigPicView removeFromSuperview];	
}
-(IBAction)ChangeinMindAction
{
	l_requestObj = [[ShopbeeAPIs alloc]init];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_string=[prefs valueForKey:@"userName"];
	[l_boughtItIndicatorView startLoadingView:self];
	[l_requestObj announceAChangeInMind:@selector(CallBackMethod:responseData:) tempTarget:self userid:tmp_string messageId:m_messageId];
	ChangeinMindBtnClicked=YES;
	l_requestObj = nil;
	[l_requestObj release];
    
}
-(IBAction)CommentBtnAction
{
	BuyItCommentViewController *tmp_obj=[[BuyItCommentViewController alloc]init];
	tmp_obj.m_MainArray=m_mainArray;
	tmp_obj.m_FavourString=@"";
	tmp_obj.m_CallBackViewController=self;//**
	[self.navigationController pushViewController:tmp_obj animated:YES];
	[tmp_obj release];
	tmp_obj=nil;
    
}
-(IBAction)FollowOrUnfollowAction // If user is not following a particular person show follow, else show unfollow.
{
	if (!follow) {
		[self btnFollowAction:nil];
	}
	else {
		[self btnUnfollowAction:nil];
	}
	
}

#pragma mark Handle swipe

- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer
{
	if(recognizer.direction==UISwipeGestureRecognizerDirectionRight)
	{
		[self RightMovement:nil];
	}
	else
	{
		[self LeftMovement:nil];
	}
	
}
-(IBAction)FindtheCurrentIndexInTheArray
{
	m_currentMessageID=[m_messageIDsBoughtItArray indexOfObject:[NSNumber numberWithInt:m_messageId] ];
	//m_currentMessageID=[ MessageID integerValue];
}	
#pragma mark Right swipe

-(void)RightMovement:(id) sender
{
	if (m_currentMessageID < m_messageIDsBoughtItArray.count-1) 
        m_currentMessageID++;
    
    
    // Show-Hide arrow button
	[self showHideArrowButton];
    

	if (m_currentMessageID<=m_messageIDsBoughtItArray.count-1)  
	{
		messageID1=[[m_messageIDsBoughtItArray objectAtIndex:m_currentMessageID] intValue];
		m_messageId=messageID1;
		NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
		NSString *tmp_String=[prefs2 stringForKey:@"userName"];
		l_requestObj=[[ShopbeeAPIs alloc] init];
		//[l_boughtItIndicatorView startLoadingView:self];
		[l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:messageID1];
		[l_requestObj release];
		l_requestObj=nil;
		
	}
}


#pragma mark Left swipe

-(void)LeftMovement:(id) sender
{
    if (m_currentMessageID >= 0) 
		m_currentMessageID--;
    
    // Hide/Show Arrow
	[self showHideArrowButton];
    
	
	if (m_currentMessageID>=0)  
	{
		messageID1=[[m_messageIDsBoughtItArray objectAtIndex:m_currentMessageID] intValue];
		m_messageId=messageID1;
		NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
		NSString *tmp_String=[prefs2 stringForKey:@"userName"];
		l_requestObj=[[ShopbeeAPIs alloc] init];
		//[l_boughtItIndicatorView startLoadingView:self];
		[l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:messageID1];
		[l_requestObj release];
		l_requestObj=nil;
	}
}

-(void) showHideArrowButton 
{ 
    // Left Arrow Button
	if (m_currentMessageID == 0){
        m_leftArrow.hidden = YES;
    }
    else {
        m_leftArrow.hidden = NO;
    }
    
    // Right Arrow Button
    if (m_currentMessageID == m_messageIDsBoughtItArray.count-1){
        m_rightArrow.hidden = YES;
    }
    else{
        m_rightArrow.hidden = NO;
    }
    
}


-(IBAction)UpdateScrollViewContent
{
	for (UIView *temp in m_scrollViewMoreInfo.subviews)
	{
		//if ([temp isKindOfClass:[UIImageView class]]) {
		if ([[m_scrollViewMoreInfo viewWithTag:32] superview]) {
			[[m_scrollViewMoreInfo viewWithTag:32] removeFromSuperview];	
		}	
		
		if ([[m_scrollViewMoreInfo viewWithTag:33] superview]) {
			[[m_scrollViewMoreInfo viewWithTag:33] removeFromSuperview];	
		}	
		if ([[m_scrollViewMoreInfo viewWithTag:44] superview]) {
			[[m_scrollViewMoreInfo viewWithTag:44] removeFromSuperview];	
		}	
		
		if ([[m_scrollViewMoreInfo viewWithTag:45] superview]) {
			[[m_scrollViewMoreInfo viewWithTag:45] removeFromSuperview];	
		}	
		if ([[m_scrollViewMoreInfo viewWithTag:5000] superview]) {
			[[m_scrollViewMoreInfo viewWithTag:5000] removeFromSuperview];	
		}	
	}
	
	
	
	NSDictionary *tmp_dictionary1=[m_mainArray objectAtIndex:0];
	NSArray *messageResponseArray=[tmp_dictionary1 valueForKey:@"messageResponseList"];
	int Count=[messageResponseArray count];
	NSDictionary *tmp_dictionary=[[m_mainArray objectAtIndex:0] valueForKey:@"wsMessage"];
    CGRect newRect;
	if (MineTrack==YES) 
	{
		if ([[tmp_dictionary valueForKey:@"isMessageAppendable"] isEqualToString:@"OPEN"]) 
		{
			//[self performSelector:@selector(btnMoreAction:)];
			m_MineCommentButton.hidden = NO;
			m_ChangeInMindBtn.hidden=NO;
			m_WishListBtn.hidden=NO;
            newRect = CGRectMake(121, 368, m_WishListBtn.frame.size.width, m_WishListBtn.frame.size.height);
            [m_WishListBtn setFrame:newRect];
			//m_LabelWishlist.hidden = YES;
			m_LikeBtn.hidden=YES;
			m_CommentButton.hidden=YES;
			m_backgroundImageView.hidden=NO;
			//m_CommentButton.frame=CGRectMake(100, 10, 52, 45);
			m_FollowBtn.hidden=YES;
			//m_FollowLabel.hidden=YES;
		}
		else 
		{
			m_backgroundImageView.hidden=YES;
			m_MineCommentButton.hidden =YES;
			m_ChangeInMindBtn.hidden=YES;
			m_WishListBtn.hidden=YES;
			//m_LabelWishlist.hidden = YES;
			m_LikeBtn.hidden=YES;
			//m_backgroundImageView.hidden=NO;
			//m_backgroundImageView.hidden=YES;
			m_CommentButton.hidden=YES;
			//m_FollowLabel.hidden=YES;
			m_FollowBtn.hidden=YES;
		}
		
	}
	else 
	{
		m_MineCommentButton.hidden=YES;
		m_ChangeInMindBtn.hidden=YES;
        CGRect newRect = CGRectMake(163, 368, m_WishListBtn.frame.size.width, m_WishListBtn.frame.size.height);
        [m_WishListBtn setFrame:newRect];
		m_WishListBtn.hidden=NO;
		//m_LabelWishlist.hidden = YES;
		m_LikeBtn.hidden=NO;
	}

//=================== Adding product picture
    float imageYPos = 24;
	NSString *productImageUrl1=[NSString stringWithFormat:@"%@%@",kServerUrl,[tmp_dictionary1 valueForKey:@"productImageUrl"]];
	//Bharat: 11/23/11: US44 - Check if product info exists, if yes, show the image2
	id wsProductDictionary = [tmp_dictionary1 valueForKey:@"wsProduct"];
	NSString * prodDiscountInfoStr = nil;
	NSString * prodTagLineStr = nil;
	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
		id imageUrlsArr = [((NSDictionary *)wsProductDictionary) valueForKey:@"imageUrls"];
		if ((imageUrlsArr != nil) && ([imageUrlsArr isKindOfClass:[NSArray class]] == YES) && 
            ([((NSArray *)imageUrlsArr) count] > 1) && ([[imageUrlsArr objectAtIndex:1] length] > 1)) {
			productImageUrl1=[NSString stringWithFormat:@"%@%@",kServerUrl,[((NSArray *)imageUrlsArr) objectAtIndex:1]];
		}	
		id discInf = [((NSDictionary *)wsProductDictionary) valueForKey:@"discountInfo"];
		if ((discInf != nil) && ([discInf isKindOfClass:[NSString class]] == YES)) {
			prodDiscountInfoStr=[NSString stringWithFormat:@"%@",discInf];
		}	
		id tagLine = [((NSDictionary *)wsProductDictionary) valueForKey:@"productTagLine"];
		if ((tagLine != nil) && ([tagLine isKindOfClass:[NSString class]] == YES)) {
			prodTagLineStr=[NSString stringWithFormat:@"%@",tagLine];
		}	
	}
    
    
	[asyncProductButton loadImageFromURL:[NSURL URLWithString:productImageUrl1] target:self action:@selector(ProductBtnClicked:) btnText:@""];
	asyncProductButton.frame = CGRectMake(asyncProductButton.frame.origin.x, imageYPos, asyncProductButton.frame.size.width, asyncProductButton.frame.size.height);
	
//================  Adding story texts - headline & who, what...
    NSDictionary *temp_text_dict=[tmp_dictionary1 valueForKey:@"wsBuzz"];
	headerLabel.text = [temp_text_dict objectForKey:@"headline"];
    whoWhatTextView.text = [self formatWhoWhatText:temp_text_dict];
    whoWhattitleTextView.text=[self formatWhoWhattitle:temp_text_dict];

	CGFloat yHeight = imageYPos + PICTURE_SIZE + 10;
    if ([whoWhatTextView.text length] > 0) {
        [whoWhatTextView sizeToFit];
		whoWhatTextView.frame = CGRectMake(whoWhatTextView.frame.origin.x, whoWhatTextView.frame.origin.y, whoWhatTextView.frame.size.width, whoWhatTextView.contentSize.height);
        [whoWhattitleTextView sizeToFit];
        whoWhattitleTextView.frame=CGRectMake(whoWhattitleTextView.frame.origin.x, whoWhattitleTextView.frame.origin.y, whoWhattitleTextView.frame.size.width, whoWhattitleTextView.contentSize.height);
        
        yHeight = whoWhatTextView.frame.origin.y + whoWhatTextView.frame.size.height + 10;
    }
    
//=================  Adding originator name
    NSDictionary *temp_dictionary1=[tmp_dictionary1 valueForKey:@"wsMessage"];
    //	m_Namelbl.text=[NSString stringWithFormat:@"%@ said:",UserName];
	m_Namelbl.text=[NSString stringWithFormat:@"%@:",[tmp_dictionary1 valueForKey:@"originatorName"]];
    [self.m_Namelbl changeViewYOriginTo:yHeight];
//=================  Adding originator picture
	NSString *Url=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_mainArray objectAtIndex:0]valueForKey:@"originatorThumbImageUrl"]];
	
	NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
	
	[asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(ProductBtnClicked:) btnText:@""];
    [asyncButton changeViewYOriginTo:yHeight];
//=================  Adding likes text
	m_likeLabel.text=[NSString stringWithFormat:@"%d Likes",[[tmp_dictionary1 valueForKey:@"favourCount"] intValue] ];
    [m_likeLabel changeViewYOriginTo:yHeight];
    
//=================  Adding text subject
	m_textViewSubject.text=[temp_dictionary1 valueForKey:@"bodyMessage"];
	[m_textViewSubject sizeToFit];
	float extra_height = 0.0f;
	if (m_textViewSubject.contentSize.height > SUBJECT_MIN_HEIGHT) {
		extra_height = m_textViewSubject.contentSize.height - SUBJECT_MIN_HEIGHT;
	}
	m_textViewSubject.frame = CGRectMake(m_textViewSubject.frame.origin.x, m_textViewSubject.frame.origin.y, m_textViewSubject.frame.size.width, m_textViewSubject.contentSize.height);

    [m_textViewSubject changeViewYOriginTo:m_Namelbl.frame.origin.y +
     m_Namelbl.frame.size.height + 2];
    
#if ADD_ELAPSED_TIME
#if 0
Bharat: DE116: Commented out for now (so that user does not see the timeline nad reject app for old updates)
	m_elapsedTimelabel.text=[temp_dictionary1 valueForKey:@"elapsedTime"];
#endif
	//Bharat: 11/17/2011: elapsed time size fixed
	//m_elapsedTimelabel.frame = CGRectMake(m_elapsedTimelabel.frame.origin.x, 22+SUBJECT_MIN_HEIGHT-10+extra_height, m_elapsedTimelabel.frame.size.width, m_elapsedTimelabel.frame.size.height);
#endif
	
	
#ifdef ADD_DISCOUNT_INFO
	
	yHeight = imageYPos + PICTURE_SIZE + 10;
	//Bharat: 11/24/11: Adding discount info 
	if ((prodDiscountInfoStr != nil) && ([prodDiscountInfoStr length] > 0)) {
		UILabel *temp_lblDiscount=[[UILabel alloc]initWithFrame:CGRectMake(50,yHeight,215,20)]; //190,5,85,20)];
		temp_lblDiscount.numberOfLines = 1;
		[temp_lblDiscount setText:prodDiscountInfoStr]; //@"10% discount"];
		[temp_lblDiscount setTextAlignment:UITextAlignmentCenter];
		[temp_lblDiscount setTextColor:[UIColor whiteColor]];
		[temp_lblDiscount setBackgroundColor:[UIColor clearColor]];
		[temp_lblDiscount setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_DISCOUNTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_DISCOUNTLABEL_FONT_SIZE_KEY]]];
		//[m_scrollViewMoreInfo addSubview:temp_lblDiscount];
		[temp_lblDiscount release];
		yHeight += (20+5);
	}
	
	//Bharat: 11/24/11: Adding tagline info 
	if ((prodTagLineStr != nil) && ([prodTagLineStr length] > 0)) {
		UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(50,yHeight,215,20)]; //190,5,85,20)];
		temp_lblTitle.numberOfLines = 1;
		[temp_lblTitle setText:prodTagLineStr]; //@"10% discount"];
		[temp_lblTitle setTextAlignment:UITextAlignmentCenter];
		[temp_lblTitle setTextColor:[UIColor whiteColor]];
		[temp_lblTitle setBackgroundColor:[UIColor clearColor]];
		[temp_lblTitle setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];
		//[m_scrollViewMoreInfo addSubview:temp_lblTitle];
		[temp_lblTitle release];
		yHeight += (5+20);
	}
#endif	
	//Bharat: 11/24/11: Adjust height automatically
	//imgView_Y=350 + extra_height;
    yHeight = m_textViewSubject.frame.origin.y + m_textViewSubject.frame.size.height + 20;
	imgView_Y=yHeight;

	
	int counter;
    counter=0;
	
	for(int abc=0;abc<Count;abc++)
	{
		
		UIImageView *	temp_imgView2 = [[UIImageView alloc] init];
		UITextView	*textView = [[UITextView alloc] init];
		
		if ([temp_imgView2 superview]) {
			[temp_imgView2 removeFromSuperview];
		}
		
		if ([textView superview]) {
			[textView removeFromSuperview];
		}
		
		NSString	*temp_string=[[messageResponseArray objectAtIndex:abc] valueForKey:@"responseMessageBody"];
		if (![temp_string isEqualToString:@""]) 
		{
			
			temp_imgView2.frame= CGRectMake(10, imgView_Y, 274, 59);
			temp_imgView2.tag=44;
			textView.tag=45;
			textView.frame = CGRectMake(40, 0, 190, 40);
			textView.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
			//textView.font = [UIFont fontWithName:kFontName size:13];
			[textView setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_SIZE_KEY]]];
			textView.backgroundColor = [UIColor clearColor];
			textView.delegate = self;
			textView.text=temp_string;
			textView.returnKeyType = UIReturnKeyDefault;
			textView.keyboardType = UIKeyboardTypeDefault; 
			textView.scrollEnabled = YES;
			textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
			textView.editable=YES; 
			textView.userInteractionEnabled=YES;
            
			
			if (counter%2==0) 
			{
				temp_imgView2.frame=CGRectMake(10, imgView_Y, 274, 25+textView.frame.size.height);
				temp_imgView2.image=[[UIImage imageNamed:@"white_message_box.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
			}
			else 
			{
				imgView_Y=imgView_Y-25;
				temp_imgView2.frame=CGRectMake(10, imgView_Y, 274, 25+textView.frame.size.height);
				temp_imgView2.image=[[UIImage imageNamed:@"white_message_box_rotated.png"] stretchableImageWithLeftCapWidth:50 topCapHeight:30];
			}
			
			
			[temp_imgView2 addSubview:textView];
			
			[m_scrollViewMoreInfo addSubview:temp_imgView2];
			
			temp_imgView2.frame=CGRectMake(10, imgView_Y, 274, 25+textView.contentSize.height);
			
			
			textView.frame =CGRectMake(40, 0, 190,textView.contentSize.height);
			textView.frame =CGRectMake(40, 0, 190,textView.contentSize.height);
			
			if (counter%2!=0) 
			{
				textView.frame =CGRectMake(40, 12, 190,textView.contentSize.height);
				textView.frame =CGRectMake(40, 12, 190,textView.contentSize.height);
			}
			
			
			UILabel *tmp_elapsedTime=[[UILabel alloc] initWithFrame:CGRectMake(47, textView.contentSize.height -5, 190, 12)];
			
			if (counter%2!=0) 
			{
				tmp_elapsedTime.frame=CGRectMake(47, textView.contentSize.height +8, 190, 12);
				
			}
			
			//Bharat: DE116: Commented out for now (so that user does not see the timeline nad reject app for old updates)
			//tmp_elapsedTime.text=[NSString stringWithFormat:@"Added %@ by %@ ",[[messageResponseArray objectAtIndex:abc]valueForKey:@"elapsedTime"],[[messageResponseArray objectAtIndex:abc]valueForKey:@"responseCustomerName"]];
			tmp_elapsedTime.text=[NSString stringWithFormat:@"Added by %@ ",[[messageResponseArray objectAtIndex:abc]valueForKey:@"responseCustomerName"]];
			tmp_elapsedTime.backgroundColor=[UIColor clearColor];	
			tmp_elapsedTime.textColor=[UIColor lightGrayColor];
			tmp_elapsedTime.font=[UIFont fontWithName:kFontName size:10];
			
			[temp_imgView2 addSubview:tmp_elapsedTime];
			//[tmp_elapsedTime setFrame:CGRectMake(30, textView.contentSize.height -5, 190, 12)];
			
			[tmp_elapsedTime release];
			tmp_elapsedTime=nil;
			
			imgView_Y=imgView_Y+textView.contentSize.height+40;
			
			AsyncImageView* SenderImage = [[[AsyncImageView alloc]
											initWithFrame:CGRectMake(5,5,35,35)] autorelease];
			
			if (counter%2!=0) 
			{
				SenderImage.frame=CGRectMake(5, 20, 35, 35);
			}
			
			[temp_imgView2 addSubview:SenderImage];
			NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[messageResponseArray objectAtIndex:abc] valueForKey:@"thumbCustomerUrl"]];
			NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,60,32]];
			//((UIImageView *)[asyncImage viewWithTag:101]).image=[UIImage imageNamed:@"loading.png"];
			[SenderImage loadImageFromURL:tempLoadingUrl ];
			
			
			NSLog(@"imgViewY>>>>>>>>>>>>>>>>%d",imgView_Y);
			//textViewY=textViewY+4;
			NSLog(@"iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii>>>>>>>>%d",i);
			
			
			counter++;
            
		}
		// To add the arrow image on the scroll view 
        //[temp_imgView2 release];
        //		temp_imgView2=nil;
        //		[textView release];
        //		textView=nil;
        
	}
    
	//Bharat: 11/26/11: Set absolute value, do not set increments
    m_scrollViewMoreInfo.contentSize = CGSizeMake(m_scrollViewMoreInfo.contentSize.width,imgView_Y+10);
	
	//[self.view addSubview:m_scrollViewMoreInfo];
	//[m_moreDetailView addSubview:m_scrollView];
	//[m_scrollViewMoreInfo release];
    //	m_scrollViewMoreInfo=nil;
	
}

#pragma mark -
#pragma mark CallBackMethod
-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData
{		
	NSLog(@"data downloaded");
	[l_boughtItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) 
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		m_mainArray=[[tempString JSONValue] retain];
		[self UpdateScrollViewContent];
		
		NSString *Following=[[m_mainArray objectAtIndex:0]valueForKey:@"isFollowing"] ;
		
		if ([Following isEqualToString:@"N"]) 
		{
            follow = NO;
            [m_FollowBtn setImage:[UIImage imageNamed:@"follow_icon"] forState:UIControlStateNormal];
		}
		else if([Following isEqualToString:@"Y"])
		{
            follow = YES;
            [m_FollowBtn setImage:[UIImage imageNamed:@"unfollow_withtext"] forState:UIControlStateNormal];
		}
		if (tempString)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	
	
}

-(void)CallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_boughtItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) 
	{
		if (ChangeinMindBtnClicked==YES) 
		{
			m_ChangeInMindBtn.hidden=NO;
			ChangeinMindBtnClicked=NO;
			m_backgroundImageView.hidden=YES;
			m_MineCommentButton.hidden =YES;
			m_ChangeInMindBtn.hidden=YES;
			//[l_boughtItIndicatorView stopLoadingView];
		}
		else 
		{
			NSLog(@"data downloaded");
			NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
			NSLog(@"response string: %@",tempString);
			[l_boughtItIndicatorView stopLoadingView];
			[self showAlertView:nil alertMessage:@"Your message was sent successfully." tag:11 cancelButtonTitle:@"OK" otherButtonTitles:nil];
			//-10 value passed as an dummy value
			[tempString release];
			tempString=nil;
			ChangeinMindBtnClicked=NO;
		}
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{           
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	
}

-(void)requestCallBackMethodforWishList:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_boughtItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) {
		
		[self showAlertView:nil alertMessage:@"Your item has been added to Wishlist successfully." tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
	tempString=nil;
    
}
-(void)CallBackMethodforfollow:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_boughtItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) {
		//[self showAlertView:nil alertMessage:@"Thank you for message" tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
        follow = YES;
        [m_FollowBtn setImage:[UIImage imageNamed:@"unfollow_withtext"] forState:UIControlStateNormal];
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
	tempString=nil;
	
}
-(void)CallBackMethodforUnfollow:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_boughtItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) {
		//[self showAlertView:nil alertMessage:@"Thank you for message" tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
        follow = NO;
        [m_FollowBtn setImage:[UIImage imageNamed:@"follow_icon"] forState:UIControlStateNormal];
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
	tempString=nil;
	
}
-(void)CallBackMethodforFlagIt:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_boughtItIndicatorView stopLoadingView];
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_boughtItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200)
	{
		[self showAlertView:nil alertMessage:@"Your message was sent successfully." tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
	tempString=nil;
	
}

#pragma mark -
#pragma mark action sheet methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSString *tmp_flagTypeString;
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_UserName=[prefs valueForKey:@"userName"];
	l_requestObj = [[ShopbeeAPIs alloc] init];
	
	if (actionSheet.tag==1) 
	{
		if (buttonIndex==0) 
		{
			tmp_flagTypeString=@"misorganized";
			[l_boughtItIndicatorView startLoadingView:self];
			[l_requestObj FlagAMessage:@selector(CallBackMethodforFlagIt:responseData:) tempTarget:self userid:tmp_UserName messageId:m_messageId flagType:tmp_flagTypeString];
			
		}
		else if (buttonIndex==1) 
		{
			tmp_flagTypeString=@"prohibited";
			[l_boughtItIndicatorView startLoadingView:self];
			[l_requestObj FlagAMessage:@selector(CallBackMethodforFlagIt:responseData:) tempTarget:self userid:tmp_UserName messageId:m_messageId flagType:tmp_flagTypeString];
			//[l_boughtItIndicatorView startLoadingView:self];
		}
		else if (buttonIndex==2) 
		{
			tmp_flagTypeString=@"spamOrOverPost";
			[l_boughtItIndicatorView startLoadingView:self];
			[l_requestObj FlagAMessage:@selector(CallBackMethodforFlagIt:responseData:) tempTarget:self userid:tmp_UserName messageId:m_messageId flagType:tmp_flagTypeString];
            //	[l_boughtItIndicatorView startLoadingView:self];
		}
	}
    
	else if (actionSheet.tag==2) 
	{
		if (buttonIndex==0) 
		{
			[self addToWishListPublic:nil];
		}
		else if(buttonIndex==1)
		{
			[self addToWishListPrivate:nil];	
		}
		//UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Your item has been added to wishlist successfully" 
        //												 delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
        //		[alert setTag:3];
        //		[alert show];
        //		[alert release];
    }
	l_requestObj = nil;
	[l_requestObj release];
}
#pragma mark -
#pragma mark Custom
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag==1) 
	{
		if (buttonIndex==0) 
		{
			NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
			NSString *tmp_UserID=[prefs valueForKey:@"userName"];
			l_requestObj = [[ShopbeeAPIs alloc] init];
			
			[m_MainDictionary setValue:@"" forKey:@"responseWords"];
			[m_MainDictionary setValue:m_messageType forKey:@"type"];
            [l_boughtItIndicatorView startLoadingView:self];
			[l_requestObj addResponseForFittingRoomRequest:@selector(CallBackMethod:responseData:) tempTarget:self userid:tmp_UserID msgresponse:m_MainDictionary];
			
			l_requestObj = nil;
			[l_requestObj release];
            
		}
		else if (buttonIndex==1) 
		{
            BuyItCommentViewController *tmp_obj=[[BuyItCommentViewController alloc]init];
            tmp_obj.m_MainArray=m_mainArray;
            tmp_obj.m_FavourString=@"Y";
			tmp_obj.m_CallBackViewController=m_CallbackViewController;
            [self.navigationController pushViewController:tmp_obj animated:YES];
            [tmp_obj release];
            tmp_obj=nil;
		}
	}
	if (alertView.tag==3) 
	{
        if (buttonIndex==0) 
        {
            //[self showAlertView:nil alertMessage:@"Your message s"tag: cancelButtonTitle:<#(NSString *)cancelButtonTitle#> otherButtonTitles:<#(NSString *)otherButtonTitles#>]
            //[self.navigationController popViewControllerAnimated:YES];
        }
	}
    
}
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

#pragma mark -
-(void)viewDidAppear:(BOOL)animated
{
	[l_appDelegate.m_customView setHidden:YES];
    if (m_currentMessageID==-1) {
		m_currentMessageID=0;
	}
    messageID1=[[m_messageIDsBoughtItArray objectAtIndex:m_currentMessageID] intValue];
    m_messageId=messageID1;
    
    [self showHideArrowButton];
    
    // Commented to solve double refresh of image Bug //
    
//    NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
//    NSString *tmp_String=[prefs2 stringForKey:@"userName"];
//    l_requestObj=[[ShopbeeAPIs alloc] init];
//    [l_boughtItIndicatorView startLoadingView:self];
//    [l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:messageID1];
//    [l_requestObj release];
//    l_requestObj=nil;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{	
    m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
	m_ChangeInMindBtn.hidden=YES;
	[self performSelector:@selector(btnMoreAction:)];
	//[self.view setBackgroundColor:[UIColor colorWithRed:.63f green:.87f blue:.90f alpha:1.0f]];
	//[self.view setBackgroundColor:[UIColor colorWithRed:86.0f/255.0f green:167.0f/255.0f blue:187.0f/255.0f alpha:1.0f]];
	m_MainDictionary=[[NSMutableDictionary alloc] init];
	l_boughtItIndicatorView=[LoadingIndicatorView SharedInstance];
	//l_requestObj=[[ShopbeeAPIs alloc] init];
	[super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
	[self FindtheCurrentIndexInTheArray];
	[l_appDelegate.m_customView setHidden:YES];
	if ([[[m_mainArray objectAtIndex:0] valueForKey:@"isFollowing"] isEqualToString:@"Y"]) 
	{
        follow = YES;
        [m_FollowBtn setImage:[UIImage imageNamed:@"unfollow_withtext"] forState:UIControlStateNormal];
	}
	else
	{
        follow = NO;
        [m_FollowBtn setImage:[UIImage imageNamed:@"follow_icon"] forState:UIControlStateNormal];
	}
	NSDictionary *tmp_dictionary=[[m_mainArray objectAtIndex:0] valueForKey:@"wsMessage"];
    CGRect newRect;
	if (MineTrack==YES) 
	{
		if ([[tmp_dictionary valueForKey:@"isMessageAppendable"] isEqualToString:@"OPEN"]) 
        {
            //[self performSelector:@selector(btnMoreAction:)];
            m_MineCommentButton.hidden = NO;
            newRect = CGRectMake(204, 368, m_WishListBtn.frame.size.width, m_WishListBtn.frame.size.height);
            [m_WishListBtn setFrame:newRect];
            m_ChangeInMindBtn.hidden=NO;
            m_WishListBtn.hidden=NO;
            m_LikeBtn.hidden=YES;
            m_CommentButton.hidden=YES;
            m_FollowBtn.hidden=YES;
            greyLine3.hidden = NO;
            greyLine1.hidden = YES;
            greyLine2.hidden = YES;
       }
		else 
		{
			m_backgroundImageView.hidden=YES;
			m_MineCommentButton.hidden =YES;
			m_ChangeInMindBtn.hidden=YES;
			m_WishListBtn.hidden=YES;
			m_LabelWishlist.hidden = YES;
			m_LikeBtn.hidden=YES;
			//m_backgroundImageView.hidden=NO;
			//m_backgroundImageView.hidden=YES;
			m_CommentButton.hidden=YES;
			//m_FollowLabel.hidden=YES;
			m_FollowBtn.hidden=YES;
            greyLine3.hidden = YES;
            greyLine1.hidden = YES;
            greyLine2.hidden = YES;

		}
        
	}
	else 
	{
		m_MineCommentButton.hidden=YES;
		m_ChangeInMindBtn.hidden=YES;
        newRect = CGRectMake(163, 368, m_WishListBtn.frame.size.width, m_WishListBtn.frame.size.height);
        [m_WishListBtn setFrame:newRect];
		m_WishListBtn.hidden=NO;
		m_LabelWishlist.hidden = NO;
		m_LikeBtn.hidden=NO;
        greyLine1.hidden = NO;
        greyLine2.hidden = NO;
        greyLine3.hidden = NO;
	}
    
	
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

// GDL: Changed everyting below

#pragma mark - memory management


- (void)viewDidUnload {
    [m_LabelWishlist release];
    m_LabelWishlist = nil;
    [super viewDidUnload];
    
    // Release retained IBOutets.
    self.m_LikeBtn = nil;
    self.m_WishListBtn = nil;
    self.m_IboughtItBtn = nil;
    self.m_ChangeInMindBtn = nil;
    self.m_mainArray = nil;
    self.m_BigPicView = nil;
    self.m_PictureView = nil;
	self.m_CommentButton=nil;
	self.m_FollowLabel=nil;
    self.m_leftArrow = nil;
    self.m_rightArrow = nil;
    self.whoWhattitleTextView=nil;
}

- (void)dealloc {
	[m_leftArrow release];
    [m_rightArrow release];
    [m_lableButItOrNot release];
	[m_CommentButton release];
	[m_scrollViewMoreInfo release];
    [m_LikeBtn release];
    [m_WishListBtn release];
    [m_IboughtItBtn release];
    [m_ChangeInMindBtn release];
	//if (m_mainArray) 
    //	{
    //		[m_mainArray release];	
    //	}
    
    [m_MainDictionary release];
    // [m_messageType release];
    [m_BigPicView release];
    [m_PictureView release];
    //[m_StrMessageId release];
    //[UserName release];
    // [m_CallbackViewController release];
    [m_FollowLabel release];
    [m_LabelWishlist release];
    [whoWhattitleTextView release];
    [super dealloc];
}

@end
