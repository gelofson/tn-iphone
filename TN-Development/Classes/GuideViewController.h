//
//  GuideViewController.h
//  QNavigator
//
//  Created by softprodigy on 04/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GuideViewController : UIViewController 
{
	UIWebView					*m_webView;
	
	UIActivityIndicatorView		*m_activityIndicator;
}

@property(nonatomic,retain) IBOutlet UIWebView						*m_webView;
@property(nonatomic,retain) IBOutlet UIActivityIndicatorView		*m_activityIndicator;

@end
