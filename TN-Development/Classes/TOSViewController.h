//
//  TOSViewController.h
//  QNavigator
//
//  Created by Soft Prodigy on 30/09/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TOSViewController : UIViewController <UIWebViewDelegate>
{

	UIButton					*m_btnDone;
	UILabel						*m_lblTitle;
	UIWebView					*m_webView;
	NSString					*m_strTypeOfData;
	UIActivityIndicatorView		*m_activityIndicator;
	
}

@property (nonatomic, retain) IBOutlet UIButton			*m_btnDone;
@property (nonatomic, retain) IBOutlet UILabel			*m_lblTitle;
@property (nonatomic, retain) IBOutlet UIWebView		*m_webView;
@property (nonatomic, retain) NSString					*m_strTypeOfData; 
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView	*m_activityIndicator;

-(IBAction)btnDoneAction:(id)sender;


@end
