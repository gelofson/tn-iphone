//
//  MyPageViewController.m
//  QNavigator
//
//  Created by softprodigy on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MyPageViewController.h"
#import "QNavigatorAppDelegate.h"
#import"DBManager.h"
#import "WishListViewController.h"
//#import"ShopbeeAPIs.h"
#import"ProfileSettingViewController.h"
#import"FansInfoViewController.h"
#import"Constants.h"
#import"JSON.h"
#import "BuzzButtonUseCase.h"
#import "AsyncButtonView.h"
#import"LoadingIndicatorView.h"
#include <QuartzCore/QuartzCore.h>
#import "UploadImageAPI.h"
#import "PhotoUseCaseViewController.h"
#import "MessageBoardManager.h"
// GDL: We use it, so why not import it?
#import "GLImage.h"
#import "BuyItCommentViewController.h"
#import "NSArray+Reverse.h"
#import "NewsDataManager.h"
#import "MyFriendViewController.h"
#import "FollowingMeViewController.h"
#import "IMFollowingViewController.h"
#import "CategoryData.h"
#import "DetailPageMessageController.h"
#import "UIImage+Scale.h"
#import "NSMutableArray+Reverse.h"
#import "ViewAFriendViewController.h"
#import "MoreButton.h"

#define REFRESH_HEADER_HEIGHT 52.0f
@implementation MyPageViewController
@synthesize m_addPhotoButton; 
@synthesize m_changePic;
@synthesize m_changePhotoView;
@synthesize m_theMyPageTable;
@synthesize m_changePicView;
@synthesize m_nameLabel;
@synthesize m_wishlistLabel;
@synthesize m_photoLabel;
@synthesize m_friendsLabel;
@synthesize m_scrollView;
@synthesize m_followingBtn;
@synthesize m_followerBtn;
@synthesize m_requestsCount;
@synthesize m_RequestImageView; 
@synthesize m_backButton;
@synthesize BackButtonTrack;
@synthesize m_commentsView;
@synthesize m_profileInfoBtn;
@synthesize m_commentsButton;
@synthesize m_commentsArray;
@synthesize m_dataArray;
@synthesize m_personImage;
@synthesize userID;
@synthesize m_SettingButton;
@synthesize m_addFriedButton;
@synthesize addCommentButton;
@synthesize refreshArrow;
@synthesize refreshSpinner;
@synthesize refreshHeaderView;
@synthesize refreshLabel;
@synthesize m_meBtn;
@synthesize m_messageBtn;
@synthesize m_StoriesBtn;
@synthesize m_FavoritesBtn;
@synthesize m_cityLBL;
@synthesize m_meView;
@synthesize m_aboutme;
@synthesize m_favoriteLBL;
@synthesize  m_chatView;
@synthesize m_storyView;
@synthesize m_storyHeaderLBL;
@synthesize m_storyMore;
@synthesize m_FavoriteHeaderLBL;
@synthesize m_FavoritesView;
@synthesize m_FavoritesyMore;
@synthesize m_mystoryheader;
@synthesize  m_myfavoriteheader;
@synthesize favBrandLabel;
@synthesize interestsLabel;

BOOL shouldForcefullySessionExpire;


QNavigatorAppDelegate *l_appDelegate;

ProfileSettingViewController *l_profileSetting;

FansInfoViewController *l_FansView;

LoadingIndicatorView *l_indicatorView;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */
#pragma mark custom methods

- (void) adjustBounds:(UIImage *)img forButton:(UIButton *)btn
{
    CGSize imageSize = img.size;
    CGSize newSize = imageSize;
    CGRect iconBounds = btn.bounds;
    CGRect cropRect = iconBounds;
    
    CGFloat IWF = imageSize.width/imageSize.height;
    CGFloat WWF = iconBounds.size.width/iconBounds.size.height;
    
    if (IWF>WWF) {
        newSize.height = iconBounds.size.height;
        newSize.width = newSize.height/imageSize.height * imageSize.width;
        cropRect.origin.x = (newSize.width - iconBounds.size.width)/2;
    } else if (WWF>IWF) {
        newSize.width = iconBounds.size.width;
        newSize.height = newSize.width/imageSize.width * imageSize.height;
        cropRect.origin.y = (newSize.height - iconBounds.size.height)/2;
    } else {
        imageSize = iconBounds.size;
        newSize = imageSize;
    }
    
    img = [img scaleToSize:newSize];
    img = [img crop:cropRect];
    
	[btn setImage:img  forState:UIControlStateNormal];
}

-(IBAction)getparticularBtuuonDetails:(id)sender
{
    int tag;
    if (sender==nil)
    {
        tag=1;
    }
    else
    {
        tag=[sender tag];
    }
    
    switch (tag) {
        case 1:
            m_meBtn.selected=YES;
            m_messageBtn.selected=NO;
            m_StoriesBtn.selected=NO;
            m_FavoritesBtn.selected=NO;
            m_meView.hidden=NO;
            [self.m_chatView setHidden:YES];
            [self.m_commentsView setHidden:YES];
            [self.m_storyView setHidden:YES];
             [self.m_FavoritesView setHidden:YES];
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                            withAction:@"showProfile"
                                                             withLabel:nil
                                                             withValue:nil];
            break;
            
        case 2:
            m_meBtn.selected=NO;
            m_messageBtn.selected=YES;
            m_StoriesBtn.selected=NO;
            m_FavoritesBtn.selected=NO;
            m_meView.hidden=YES;
            [self.m_storyView setHidden:YES];
           [self.m_chatView setHidden:NO];
            [self.m_commentsView setHidden:NO];
            
             [self.m_FavoritesView setHidden:YES];
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                            withAction:@"showMessages"
                                                             withLabel:nil
                                                             withValue:nil];
            break;
        case 3:
            m_meBtn.selected=NO;
            m_messageBtn.selected=NO;
            m_StoriesBtn.selected=YES;
            m_FavoritesBtn.selected=NO;
            m_meView.hidden=YES;
            selectedIndex = -1;
            
            CGRect frame=[self.m_storyView frame];
            frame.origin.y=self.m_meView.frame.origin.y;
            self.m_storyView.frame=frame;
            
            if (![self.m_scrollView.subviews containsObject:self.m_storyView]) {
                [self.m_scrollView addSubview:self.m_storyView];
            }
            
            [self.m_scrollView addSubview:self.m_storyView];
            
            [self.m_storyView setHidden:NO];
            [self.m_chatView setHidden:YES];
            [self.m_commentsView setHidden:YES];
            [self.m_FavoritesView setHidden:YES];
            if (m_storyArray !=nil)
            {
                [m_storyArray release];
                m_storyArray=nil;
            }
            m_storyArray=[[NSMutableArray alloc]init];
            if ([[l_appDelegate.userInfoDic valueForKey:@"photosCount"]intValue]>0)
            {
                if ([[l_appDelegate.userInfoDic valueForKey:@"photosCount"]intValue]<6)
                {
                    [self.m_storyMore setHidden:YES];
                }
                else
                {
                 [self.m_storyMore setHidden:NO];
                }
                [l_indicatorView startLoadingView:self];
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *customerID= [prefs objectForKey:@"userName"];
                
                [l_request getMyPagePopularPhotos:@selector(requestCallBackMethodforPopularPhoto:responseData:) tempTarget:self customerid:customerID num:10 loginid:customerID];
              
            }
            
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                            withAction:@"clickedStories"
                                                             withLabel:nil
                                                             withValue:nil];

            break;
        case 4:
            m_meBtn.selected=NO;
            m_messageBtn.selected=NO;
            m_StoriesBtn.selected=NO;
            m_FavoritesBtn.selected=YES;
            m_meView.hidden=YES;
            selectedFavIndex = -1;
            
            CGRect frame1=[self.m_FavoritesView frame];
            frame1.origin.y=self.m_meView.frame.origin.y;
            self.m_FavoritesView.frame=frame1;
            if (![self.m_scrollView.subviews containsObject:self.m_FavoritesView]) {
                [self.m_scrollView addSubview:self.m_FavoritesView];
            }
                        
            [self.m_storyView setHidden:YES];
            [self.m_chatView setHidden:YES];
            [self.m_commentsView setHidden:YES];
            
            [self.m_FavoritesView setHidden:NO];
            
            if (m_favoriteArray!=nil)
            {
                [m_favoriteArray release];
                m_favoriteArray=nil;
            }
            m_favoriteArray=[[NSMutableArray alloc]init];
            [l_indicatorView startLoadingView:self];

            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *customerID= [prefs objectForKey:@"userName"];
            [l_request getItemsOnWishList:@selector(requestCallBackMethodforGetItems:responseData:) tempTarget:self customerid:customerID type:@"mine"];
            
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                            withAction:@"clickedFavorites"
                                                             withLabel:nil
                                                             withValue:nil];
            break;
        default:
            break;
    }


}


-(IBAction) btn_wishList
{
	WishListViewController *temp_obj_wishList=[[WishListViewController alloc]initWithNibName:@"WishListViewController" bundle:[NSBundle mainBundle]];
	temp_obj_wishList.isFromFriendsView=NO;
	[self.navigationController pushViewController:temp_obj_wishList animated:YES ];
	[temp_obj_wishList release];
	temp_obj_wishList=nil;
    
}

-(IBAction) clickonChangeBtn
{	
	PhotoUseCaseViewController *PhotoUseCaseObj=[[PhotoUseCaseViewController alloc] initWithNibName:@"PhotoUseCaseViewController" bundle:nil];
	[PhotoUseCaseObj setHidesBottomBarWhenPushed:YES];
	PhotoUseCaseObj.m_MypageViewController=self;
	[self.navigationController pushViewController:PhotoUseCaseObj animated:YES];
	[PhotoUseCaseObj release];
	PhotoUseCaseObj=nil;
	
}
-(IBAction) goToMyPageAction
{
	[m_changePicView removeFromSuperview];
	
}

-(IBAction) clickOnCameraButton:(id)sender       // for displaying the action sheet after user clicks on the camera button
{
	
	if ([m_addPhotoButton imageForState:UIControlStateNormal]==[UIImage imageNamed:@"addphoto_icon.png"])
	{
		if(count==0)
		{
	        PhotoUseCaseViewController *PhotoUseCaseObj=[[PhotoUseCaseViewController alloc] initWithNibName:@"PhotoUseCaseViewController" bundle:nil];
			[PhotoUseCaseObj setHidesBottomBarWhenPushed:YES];
			PhotoUseCaseObj.m_MypageViewController=self;
			[self.navigationController pushViewController:PhotoUseCaseObj animated:YES];
            [PhotoUseCaseObj release];
			PhotoUseCaseObj=nil;
			
		}
		
		else 
		{
			[(UIImageView *) [m_changePicView viewWithTag:100] setImage:[m_addPhotoButton imageForState:UIControlStateNormal]];
			[self.view addSubview:m_changePicView];
		} 
	}
	else 
	{
		[(UIImageView *) [m_changePicView viewWithTag:100] setImage:[m_addPhotoButton imageForState:UIControlStateNormal]];
		[self.view addSubview:m_changePicView];
	}
}

-(IBAction) clickedOnFriendsButton
{	
    
    MyFriendViewController *Vc=[[MyFriendViewController alloc]initWithNibName:@"MyFriendViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:Vc animated:YES];
	[Vc release];
	
	
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                    withAction:@"clickedOnFriendsButton"
                                                     withLabel:nil
                                                     withValue:nil];
	
}

-(IBAction) showProfileSettingsAction
{
	MyPageSettingViewController *tmp_obj1=[[MyPageSettingViewController alloc]initWithNibName:@"MyPageSettingViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:tmp_obj1 animated:YES];
	[tmp_obj1 release];
	tmp_obj1=nil;
	
}

-(IBAction) viewPhotosAction
{
	if ([[l_appDelegate.userInfoDic valueForKey:@"photosCount"]intValue]>0)
	{
        if ([userID length] <= 0)
        {
            m_photocontroller=[[MyPagePhotoViewController alloc] initWithNibName:@"MyPagePhotoViewController" bundle:[NSBundle mainBundle]];
            m_photocontroller.isFromViewFriends=NO;
            m_photocontroller.storyCount=[[l_appDelegate.userInfoDic valueForKey:@"photosCount"]intValue];
            [self.navigationController pushViewController:m_photocontroller animated:YES];	
            [m_photocontroller release];
            m_photocontroller=nil;            
        }
        else
        {
            m_photocontroller=[[MyPagePhotoViewController alloc] initWithNibName:@"MyPagePhotoViewController" bundle:[NSBundle mainBundle]];
            m_photocontroller.isFromViewFriends=YES;
            m_photocontroller.m_FriendMailString=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
            m_photocontroller.m_FriendNameString=m_nameLabel.text;
            m_photocontroller.storyCount=[[l_appDelegate.userInfoDic valueForKey:@"photosCount"]intValue];
            [self.navigationController pushViewController:m_photocontroller animated:YES];	
            [m_photocontroller release];
            m_photocontroller=nil;
            
        }
    }
    }

-(IBAction) FollowersBtnAction// for displaying the follower properties on the fans info view controller 
{
	if ([[l_appDelegate.userInfoDic valueForKey:@"followersCount"]intValue]>0) 
	{
		NSString *custid=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
		[l_indicatorView startLoadingView:self];
		[l_request getFollowerList:@selector(FollowCallBackMethod:responseData:) tempTarget:self customerid:custid loginid:custid];
		m_checkFollowWebService=1;
        [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                        withAction:@"FollowersBtnAction"
                                                         withLabel:nil
                                                         withValue:nil];
	}
}

-(IBAction) FollowingBtnAction
{
	if ([[l_appDelegate.userInfoDic valueForKey:@"followingCount"] intValue] > 0)
	{
		NSString *custid=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
		[l_indicatorView startLoadingView:self];
		[l_request getFollowingList:@selector(FollowCallBackMethod:responseData:) tempTarget:self customerid:custid loginid:custid];
		
		m_checkFollowWebService=-1;
        [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                        withAction:@"FollowingBtnAction"
                                                         withLabel:nil
                                                         withValue:nil];
	}
}

-(IBAction)sendPictureToWebservice
{
	UploadImageAPI *tempUploadObj=[UploadImageAPI SharedInstance];
	
	NSMutableDictionary *temp_dictRegData;
	NSUInteger len = [m_personData length];
	const char *raw=[m_personData bytes];
	
	NSMutableArray *tempByteArray=[[NSMutableArray alloc]init];
	for (long int i = 0; i < len; i++)
	{
		[tempByteArray addObject:[NSNumber numberWithInt:raw[i]]];
	} 
	
	NSString *tmp_usrId=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	
	
	
	temp_dictRegData=[NSMutableDictionary dictionaryWithObjectsAndKeys:[tempByteArray JSONFragment],@"photo",tmp_usrId,@"userId",nil];
	NSDictionary *tmp_dictionary=(NSMutableDictionary *)temp_dictRegData;
	
	[tempUploadObj updateUserPic:tmp_dictionary];
	
	[tempByteArray release];
	tempByteArray=nil;
	
	
}	
-(IBAction)BackBtnAction
{
	[self.navigationController popViewControllerAnimated:YES];	
}

-(IBAction) addAFriendAction
{
    [l_indicatorView startLoadingView:self];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_CustomerId= [prefs valueForKey:@"userName"];
	NSMutableArray *tmp_array=[[NSMutableArray alloc] init];
	[tmp_array addObject:[l_appDelegate.userInfoDic valueForKey:@"emailid"]];
	NSString *tmp_String=[tmp_array JSONRepresentation];
	[tmp_array release];
	tmp_array=nil;
	
	[l_request sendRequestToAddFriend:@selector(AddFriendCallBackMethod:responseData:) tempTarget:self custId:tmp_CustomerId tmpUids:tmp_String ];	
}

-(IBAction)gotoStoryDetailsPage:(id)sender
{
    int tag=[sender tag];
    
    NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
    NSString *tmp_String=[prefs2 stringForKey:@"userName"];
    
    selectedIndex = tag - 1;
    [l_indicatorView startLoadingView:self];
    [l_request getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_storyArray objectAtIndex:selectedIndex]valueForKey:@"id"]intValue]];
    
}

-(IBAction)gotoFavoriteDetailsPage:(id)sender
{
    int tag=[sender tag]-50;
    int min = MIN([m_favoriteArray count], 6);
    BOOL inRange = selectedFavIndex < min;
    if (inRange) {
        NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
        NSString *tmp_String=[prefs2 stringForKey:@"userName"];
    
        selectedFavIndex=tag;
        [l_indicatorView startLoadingView:self];
        [l_request getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_favoriteArray objectAtIndex:selectedFavIndex]valueForKey:@"messageid"]intValue]];
    }

}

#pragma mark -
#pragma mark Callback methods

-(void)requestCallBackMethodforGetItems:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSString *str;
	int  temp_responseCode=[responseCode intValue];
	[l_indicatorView stopLoadingView];
	
	str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
	NSLog(@"str :%@",str);
	
	
	if(temp_responseCode==200)
	{
		NSArray *favrotAry=[str JSONValue];
         NSLog(@"%@",favrotAry);
        for (int i=0; i<[favrotAry count]; i++)
        {
            NSDictionary *dict=[favrotAry objectAtIndex:i];
            
            if (![[dict objectForKey:@"buzzImageThumbUrl"] isEqualToString:@""])
            {
                [m_favoriteArray addObject:dict];
                NSLog(@"%@",m_favoriteArray);
            }
           
          
        }
        if (m_favoriteArray.count>0)
        {
            
            [m_favoriteArray reverse];
            if (m_favoriteArray.count>6)
            {
                self.m_FavoritesyMore.hidden=NO;
            }
            else
            {
                self.m_FavoritesyMore.hidden=NO;
            }
            NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
            NSString *tmp_String=[prefs2 stringForKey:@"userName"];
            [l_indicatorView startLoadingView:self];
            
            [l_request getDataForParticularRequest:@selector(CallBackMethodForGetHeader:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_favoriteArray objectAtIndex:0] valueForKey:@"messageid"]intValue]];
            
            [self loadFavorites];
        }
        
		
		
		
	}
	else {
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
        
        
	}
	
	
}

-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSLog(@"data downloaded");
	if ([responseCode intValue]==200)
	{
        if (m_StoriesBtn.selected)
        {
           	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            NSArray *tmp_array=[tempString JSONValue];
            NSDictionary *tmp_dict = [[tmp_array objectAtIndex:0] valueForKey:@"wsMessage"];
            
            CategoryData *catData = [CategoryData convertMessageData:[tmp_array objectAtIndex:0]];
            DetailPageMessageController *detailController = [[[DetailPageMessageController alloc] init] autorelease];
            detailController.m_data = catData;
            [detailController setMessagesIdsFromPopularPhotos:m_storyArray];
            detailController.m_Type = [[m_storyArray objectAtIndex:selectedIndex] objectForKey:@"messageType"];
            detailController.m_messageId = [[tmp_dict objectForKey:@"id"] intValue];
            detailController.filter = POPULAR;
            
            NSDictionary *aData = [catData messageData:0];
            NSDictionary *tmp_Dict=[aData valueForKey:@"wsMessage"];
            NSString *custID=[tmp_Dict valueForKey:@"sentFromCustomer"];
            
            NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
            if ([[custID lowercaseString] isEqualToString:[[prefs valueForKey:@"userName"] lowercaseString] ])
                detailController.mineTrack = YES;
            else
                detailController.mineTrack = NO;
            detailController.curPage = 1;
            detailController.m_CallBackViewController = self;
            [self.navigationController pushViewController:detailController animated:YES];
            
            
            if (tempString!=nil)
            {
                [tempString release];
                tempString=nil;
            }
        }
	else if(m_FavoritesBtn.selected)
    {
    	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
        NSDictionary *tmp_dict = [[tmp_array objectAtIndex:0] valueForKey:@"wsMessage"];
		
        CategoryData *catData = [CategoryData convertMessageData:[tmp_array objectAtIndex:0]];
        
        DetailPageMessageController *detailController = [[[DetailPageMessageController alloc] init] autorelease];
        detailController.m_data = catData;
//        [detailController setMessagesIdsFromWishListData:[m_favoriteArray objectAtIndex:selectedFavIndex]];
        [detailController setMessagesIdsFromWishListPhotos:m_favoriteArray];
        detailController.m_Type = [[m_favoriteArray objectAtIndex:selectedFavIndex] objectForKey:@"buzzType"];
        detailController.m_messageId = [[tmp_dict objectForKey:@"id"] intValue];
        detailController.filter = POPULAR;
        detailController.mineTrack = NO;
        detailController.curPage = 1;
        detailController.m_CallBackViewController = self;
        [self.navigationController pushViewController:detailController animated:YES];
		
		
        if (tempString!=nil)
		{
			[tempString release];
			tempString=nil;
		}
    
    
    }
		
	}
	else
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
    
    [l_indicatorView stopLoadingView];;
}


- (void)requestCallBackMethodforPopularPhoto:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSString *str;
	str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
	NSArray *tempArray=[[str JSONValue] retain];
	NSLog(@"%@",tempArray);
	
	
	if ([responseCode intValue]==200) {
		NSDictionary *dict;
		for(int i=0;i<[tempArray count] || [m_storyArray count] == 6;i++)
		{
            NSString *msgType = [[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"] valueForKey:@"messageType"];
            
            if (![msgType isEqualToString:@"Q"]) {
                dict=[NSDictionary dictionaryWithObjectsAndKeys:[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"bodyMessage"],@"bodyMessage",[[tempArray objectAtIndex:i]valueForKey:@"buzzId"],@"buzzId",[[tempArray objectAtIndex:i]valueForKey:@"discountInfo"],@"discountInfo",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"elapsedTime"],@"elapsedTime",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"id"],@"id",[[tempArray objectAtIndex:i]valueForKey:@"tagLine"],@"tagLine",[[tempArray objectAtIndex:i]valueForKey:@"productName"],@"productName",[[tempArray objectAtIndex:i]valueForKey:@"photoUrl"],@"photoUrl",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"] valueForKey:@"messageType"],@"messageType",[[tempArray objectAtIndex:i]valueForKey:@"photoThumbUrl"],@"photoThumbUrl",nil];
                NSLog(@"%@",dict);
               [m_storyArray addObject:dict];
            }
        }
        if (m_storyArray.count>0)
        {
            NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
            NSString *tmp_String=[prefs2 stringForKey:@"userName"];
            [l_request getDataForParticularRequest:@selector(CallBackMethodForGetHeader:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_storyArray objectAtIndex:0] valueForKey:@"id"] intValue]];
            
            [self loadStoryView:m_storyArray];
            
        }
    }  else if([responseCode intValue]==kLockedInfoErrorCode) {
        //[l_loadingView stopLoadingView];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kCannotViewFriendInfoTitle message:kCannotViewFriendInfoMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
        alert=nil;
    } else
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
        [l_indicatorView stopLoadingView];
}

-(void)CallBackMethodForGetHeader:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSLog(@"data downloaded");
	if ([responseCode intValue]==200)
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
        NSDictionary *tmp_dict = [[tmp_array objectAtIndex:0] valueForKey:@"wsBuzz"];
        
        if (![[tmp_dict objectForKey:@"headline"] isEqualToString:@""])
        {
            if (self.m_StoriesBtn.selected)
            {
                self.m_storyHeaderLBL.text=[tmp_dict objectForKey:@"headline"];
            }
            else if(self.m_FavoritesBtn.selected)
            {
                self.m_FavoriteHeaderLBL.text=[tmp_dict objectForKey:@"headline"];
            }
        }
		
        if (tempString!=nil)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	[l_indicatorView stopLoadingView];
}

-(void)loadFavorites
{
    int imageCount= MIN([m_favoriteArray count], 6);

    for (int i=0; i < imageCount; i++)
    {
        AsyncButtonView *btn=(AsyncButtonView*)[self.m_FavoritesView viewWithTag:i+50];
        [btn resetButton];
        NSString *temp_strUrl=[NSMutableString stringWithFormat:@"%@%@",kServerUrl,[[m_favoriteArray objectAtIndex:i]valueForKey:@"buzzImageUrl"]];
        btn.cropImage = YES;
        btn.maskImage = YES;
        btn.messageTag = i+50;
        [btn loadImageFromURL:[NSURL URLWithString:temp_strUrl] target:self action:@selector(gotoFavoriteDetailsPage:) btnText:nil];
        
    }
}

-(void)loadStoryView:(NSArray*)array
{
    int imageCount= MIN([array count], 6);
    
    for (int i=0; i<imageCount; i++)
    {
        AsyncButtonView *btn=(AsyncButtonView*)[self.m_storyView viewWithTag:i+1];
        NSString *temp_strUrl=[NSMutableString stringWithFormat:@"%@%@",kServerUrl,[[array objectAtIndex:i] valueForKey:@"photoUrl"]];
        btn.cropImage = YES;
        btn.maskImage = YES;
        btn.messageTag = i + 1;
        [btn loadImageFromURL:[NSURL URLWithString:temp_strUrl] target:self action:@selector(gotoStoryDetailsPage:) btnText:nil];
    }
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)AddFriendCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
    [l_indicatorView stopLoadingView];
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	if ([responseCode intValue]==200) 
	{
		UIAlertView *tmp_alertview=[[UIAlertView alloc] initWithTitle:@"Friend request sent" message:@"Friend request was sent successfully." 
															 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_alertview show];
		[tmp_alertview release];
		
		m_addFriedButton.hidden=YES;
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
}

-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSLog(@"response code is %d",[responseCode intValue]);
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_indicatorView stopLoadingView];
	
	if([responseCode intValue]==200)
	{
		m_scrollView.hidden=NO;
        NSArray *tmpArray=[[tempString JSONValue]retain];
		NSLog(@"the array is %@",tmpArray);
		l_appDelegate.userInfoDic=[tmpArray objectAtIndex:0]; 
		m_nameLabel.text=[NSString stringWithFormat:@"%@",[l_appDelegate.userInfoDic valueForKey:@"firstName"] ];
      
        m_cityLBL.text=[NSString stringWithFormat:@"%@",[l_appDelegate.userInfoDic valueForKey:@"city"] ];
        
        m_aboutme.text=[NSString stringWithFormat:@"%@",[l_appDelegate.userInfoDic valueForKey:@"aboutMe"]];
        
        m_favoriteLBL.text=[NSString stringWithFormat:@"%@",[l_appDelegate.userInfoDic valueForKey:@"favQuotes"]];

        favBrandLabel.text=[NSString stringWithFormat:@"%@",[l_appDelegate.userInfoDic valueForKey:@"favBrand"]];
        interestsLabel.text=[NSString stringWithFormat:@"%@",[l_appDelegate.userInfoDic valueForKey:@"interest"]];
		m_profileSettings=[[NSArray alloc] initWithObjects: [l_appDelegate.userInfoDic valueForKey:@"aboutMe"],
						   [l_appDelegate.userInfoDic valueForKey:@"interest"],
						   [l_appDelegate.userInfoDic valueForKey:@"favBrand"],
						   [l_appDelegate.userInfoDic valueForKey:@"favQuotes"],nil];
		
            if ([[l_appDelegate.userInfoDic objectForKey:@"friendsRequestCount"] intValue]>0) 
		{
			m_RequestImageView.hidden=NO;
			m_requestsCount.hidden=NO;
			m_requestsCount.text=[NSString stringWithFormat:@"%@",[l_appDelegate.userInfoDic valueForKey:@"friendsRequestCount"]];
		}
		else 
		{
			m_RequestImageView.hidden=YES;
			m_requestsCount.hidden=YES;
			
		}
		
		[tmpArray release];
		tmpArray=nil;
		[m_theMyPageTable reloadData];
		
		
		[tempString release];
		tempString=nil;
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

    [l_indicatorView stopLoadingView];
    
	
	m_friendsbuttonclicked=NO;
		
	NSFileManager *manager = [[NSFileManager alloc] init];
    if ([userID length] <= 0)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *fileName=[NSString stringWithFormat:@"%@.jpg",[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"]];
        
        NSMutableString *pathDoc=[NSString stringWithFormat:@"%@/%@",documentsDirectory,fileName];
        NSLog(@"Doc Directory______Path_____%@",pathDoc);

        if([manager fileExistsAtPath:pathDoc]) 
        {
            self.m_personImage = [UIImage imageWithContentsOfFile:pathDoc];
            [self adjustBounds:self.m_personImage forButton:m_addPhotoButton];
//            [m_addPhotoButton setImage:self.m_personImage forState:UIControlStateNormal];
            
        }
        else if(((NSString *)[l_appDelegate.userInfoDic valueForKey:@"profilePicUrl"]).length > 0)
        {
            NSURL *temp_loadingUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kServerUrl,[l_appDelegate.userInfoDic valueForKey:@"profilePicUrl"]]];
            NSData *tempImageData=[NSData dataWithContentsOfURL:temp_loadingUrl];
            self.m_personImage = [UIImage imageWithData:tempImageData];
            [self adjustBounds:self.m_personImage forButton:m_addPhotoButton];
//            [m_addPhotoButton setImage:[UIImage imageWithData:tempImageData] forState:UIControlStateNormal];
            
            [tempImageData writeToFile:pathDoc atomically:YES];		
            
        }
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *fileName=[NSString stringWithFormat:@"%@.jpg",userID];
        
        NSMutableString *pathDoc=[NSString stringWithFormat:@"%@/%@",documentsDirectory,fileName];
        NSLog(@"Doc Directory______Path_____%@",pathDoc);

        if(((NSString *)[l_appDelegate.userInfoDic valueForKey:@"profilePicUrl"]).length > 0)
        {
            NSURL *temp_loadingUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kServerUrl,[l_appDelegate.userInfoDic valueForKey:@"profilePicUrl"]]];
            NSData *tempImageData=[NSData dataWithContentsOfURL:temp_loadingUrl];
            self.m_personImage = [UIImage imageWithData:tempImageData];
            [self adjustBounds:self.m_personImage forButton:m_addPhotoButton];
//            [m_addPhotoButton setImage:self.m_personImage forState:UIControlStateNormal];
            
            [tempImageData writeToFile:pathDoc atomically:YES];		
            
        }
        else
        {
            self.m_personImage = [UIImage imageNamed:@"no-image"];
            [m_addPhotoButton setImage:self.m_personImage forState:UIControlStateNormal];            
        }
    }
	
	if ([m_addPhotoButton imageForState:UIControlStateNormal]==nil)
	{
		[m_addPhotoButton setImage:[UIImage imageNamed:@"addphoto_icon.png"]forState:UIControlStateNormal];
	}
	
	[manager release];
    [self  stopLoading];
	
}

-(void)FollowCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_indicatorView stopLoadingView];
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	NSMutableArray *tmp_array=[tempString JSONValue];
	
	
	if ([responseCode intValue]==200)
	{
		if (m_checkFollowWebService==1&&([tmp_array count]>0))
		{
            
           
            FollowingMeViewController *Vc=[[FollowingMeViewController alloc]initWithNibName:@"FollowingMeViewController" bundle:[NSBundle mainBundle]];
            Vc.m_followArray=[[tempString JSONValue]retain];
            Vc.m_FollowersTrack=0;
            [self.navigationController pushViewController:Vc animated:YES];
            [Vc release];
            
			m_checkFollowWebService=0;
            
		}
		else if(m_checkFollowWebService==-1&&([tmp_array count]>0))
		{																			// for             
            IMFollowingViewController *Vc=[[IMFollowingViewController alloc]initWithNibName:@"IMFollowingViewController" bundle:[NSBundle mainBundle]];
            Vc.m_followArray=[[tempString JSONValue]retain];
            Vc.m_FollowersTrack=0;
            [self.navigationController pushViewController:Vc animated:YES];
            [Vc release];
            
			m_checkFollowWebService=0;
		}
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	[tempString release];
	tempString=nil;
}

-(void)requestCallBackMethodForUploadPicture:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[tempString release];
	tempString=nil;
	[l_indicatorView stopLoadingView];
}		



#pragma mark -
#pragma mark actionsheet methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	
	if (buttonIndex==0) 
	{
		isPictureSelected=YES;
		if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
		{
			UIImagePickerController *tmp_picker=[[UIImagePickerController alloc]init];
			tmp_picker.sourceType=UIImagePickerControllerSourceTypeCamera ;
			tmp_picker.delegate=self;
			tmp_picker.allowsEditing=NO;
			[self presentModalViewController:tmp_picker animated:YES];
			[tmp_picker release];
			NSLog(@"button index is %i",buttonIndex);
		}
		else 
		{
			UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No camera device found." message:nil delegate:self 
											   cancelButtonTitle:@"OK" otherButtonTitles:nil ];
			[alert show];
			[alert release];
			
		}
	}
	
	else if(buttonIndex==1) {
		isPictureSelected=YES;
		if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) 
		{
			NSLog(@"button index is %i",buttonIndex);
			UIImagePickerController *picker=[[UIImagePickerController  alloc] init];
			picker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
			picker.allowsEditing=NO;
			picker.delegate=self;
			[self presentModalViewController:picker animated:YES];
			[picker release];
			
		}
	}
	else if(buttonIndex==2)
	{
		
		isPictureSelected=NO;
		[m_changePicView removeFromSuperview];
	}
	
	
}
#pragma mark -
#pragma mark imagepicker Methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
	UIImage *tempImage=[image scaledDownImageWithMaximumSize:CGSizeMake(320,320)];
	
	if (m_personData) 
	{
		[m_personData release];
		m_personData=nil;
	}
	
	m_personData=UIImageJPEGRepresentation(tempImage,0.0);
	
	tempImage=[UIImage imageWithData:m_personData];	
	self.m_personImage = tempImage;
    [self adjustBounds:self.m_personImage forButton:m_addPhotoButton];
//	[m_addPhotoButton setImage:tempImage forState:UIControlStateNormal];
	m_changePic.image=tempImage;
	
	[picker dismissModalViewControllerAnimated:YES];
	m_changePic.image=tempImage;
	
	tmp_count++;
	
	//m_personImage=[[[UIImage alloc] init]autorelease];
	
	m_personImage=tempImage;
    
	//NSString *base64String=[m_personData base64EncodedString];
	
	[m_personData retain];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *pathDoc=[NSString stringWithFormat:@"%@",documentsDirectory];
	NSLog(@"Doc Directory______Path_____%@",pathDoc);
	
	
	NSMutableString *tempEmail=[NSMutableString stringWithFormat:@"%@/%@.jpg",pathDoc,[l_appDelegate.userInfoDic valueForKey:@"emailid"]];
	NSLog(@"tempEmail %@",tempEmail);
	
	//[l_indicatorView startLoadingView:self];
	
	[m_personData writeToFile:tempEmail atomically:YES];
	//[self sendPictureToWebservice];
	
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
	
	[picker dismissModalViewControllerAnimated:YES];
	count=0;
    
	[m_changePicView removeFromSuperview];
	
}
#if 0
- (void) presentComments
{
    [self.m_commentsView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];   
    
	self.m_commentsView.scrollsToTop = NO;
	self.m_commentsView.contentOffset = CGPointMake(0,0);
	CGFloat imgViewY=0;
	
	int counter;
	
	NSInteger Count = [self.m_commentsArray count];
    
   // NSLog(@"comment array %@",self.m_commentsArray);

    
	counter=0;
	for(int a=0;a<Count;a++)
	{
		
		
		NSString	*temp_string=[[self.m_commentsArray objectAtIndex:a] valueForKey:@"responseMessageBody"];
		if (![temp_string isEqualToString:@""]) 
		{
			UIView *temp_imgView2=[[UIView alloc]initWithFrame:CGRectMake(10, imgViewY, 274, 59)];
			temp_imgView2.tag = 32;	
			
			UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(45, 0, 210, 40)];
			textView.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
			//textView.font = [UIFont fontWithName:kFontName size:13];
			[textView setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_SIZE_KEY]]];
			textView.backgroundColor = [UIColor clearColor];
			textView.delegate = nil;
			textView.tag = 33;
			textView.text=temp_string;
			textView.returnKeyType = UIReturnKeyDefault;
			textView.keyboardType = UIKeyboardTypeDefault; 
			textView.scrollEnabled = YES;
			textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
			textView.editable=YES; 
			textView.userInteractionEnabled=NO;
			
			
			[temp_imgView2 addSubview:textView];
			
			[self.m_commentsView addSubview:temp_imgView2];
			
			temp_imgView2.frame=CGRectMake(0, imgViewY, 320, 25+textView.contentSize.height);
			
			
			textView.frame =CGRectMake(40, 0, 210,textView.contentSize.height);
			
			if (counter%2!=0) 
			{
				textView.frame =CGRectMake(40, 12, 210,textView.contentSize.height);
			}
			
			
			UILabel *tmp_elapsedTime=[[UILabel alloc] initWithFrame:CGRectMake(45, textView.contentSize.height -5, 190, 12)];
			
			if (counter%2!=0) 
			{
				tmp_elapsedTime.frame=CGRectMake(45, textView.contentSize.height +8, 190, 12);
				
			}
			
			//Bharat: DE116: Commented out for now (so that user does not see the timeline nad reject app for old updates)
			//tmp_elapsedTime.text=[NSString stringWithFormat:@"Added %@ by %@ ",[[messageResponseArray objectAtIndex:a]valueForKey:@"elapsedTime"],[[messageResponseArray objectAtIndex:a]valueForKey:@"responseCustomerName"]];
			tmp_elapsedTime.text=[NSString stringWithFormat:@"%@ %@ ",[[self.m_commentsArray objectAtIndex:a]valueForKey:@"responseCustomerName"],[[self.m_commentsArray objectAtIndex:a]valueForKey:@"elapsedTime"]];
			tmp_elapsedTime.backgroundColor=[UIColor clearColor];	
			tmp_elapsedTime.textColor=[UIColor lightGrayColor];
			tmp_elapsedTime.font=[UIFont fontWithName:kFontName size:10];
			
			[temp_imgView2 addSubview:tmp_elapsedTime];
			//[tmp_elapsedTime setFrame:CGRectMake(30, textView.contentSize.height -5, 190, 12)];
			
						
			
			imgViewY=imgViewY+textView.contentSize.height+40;
			
            AsyncButtonView* SenderImage = [[[AsyncButtonView alloc]
                                            initWithFrame:CGRectMake(5,5,35,35)] autorelease];
			
			if (counter%2!=0) 
			{
				SenderImage.frame=CGRectMake(5, 20, 35, 35);
			}
			
			[temp_imgView2 addSubview:SenderImage];
			NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[self.m_commentsArray objectAtIndex:a] valueForKey:@"thumbCustomerUrl"]];
			NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,60,32]];
			//((UIImageView *)[asyncImage viewWithTag:101]).image=[UIImage imageNamed:@"loading.png"];
            SenderImage.cropImage = YES;
            SenderImage.maskImage = YES;
			[SenderImage loadImageFromURL:tempLoadingUrl target:nil action:nil btnText:@"" ignoreCaching:NO];
			
			
            UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(0, textView.contentSize.height+tmp_elapsedTime.frame.size.height+2, 320 , 1)];
			
			if (counter%2!=0)
			{
				lbl.frame=CGRectMake(0, textView.contentSize.height+tmp_elapsedTime.frame.size.height+8, 320, 1);
				
			}
            [lbl setBackgroundColor:[UIColor lightGrayColor]];
            [temp_imgView2 addSubview:lbl];
            [lbl release];
            
			NSLog(@"imgViewY>>>>>>>>>>>>>>>>%f",imgViewY);
			//textViewY=textViewY+4;
			[temp_imgView2 release];
			temp_imgView2=nil;
			[textView release];
			textView=nil;
			[tmp_elapsedTime release];
			tmp_elapsedTime=nil;

			
			

            
			counter++;
		}
	}
	//Bharat: 11/26/11: Set absolute value, do not set increments
	self.m_commentsView.contentSize = CGSizeMake(m_commentsView.contentSize.width,imgViewY+10);
}
#else
- (void) presentComments
{
    [self.m_commentsView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
	self.m_commentsView.scrollsToTop = NO;
	self.m_commentsView.contentOffset = CGPointMake(0,0);
	CGFloat imgViewY=0;
	
	int counter;
	
	NSInteger Count = [self.m_commentsArray count];
    
    // NSLog(@"comment array %@",self.m_commentsArray);
    
    
	counter=0;
	for(int a=0;a<Count;a++)
	{
		
		
		NSString	*temp_string=[[self.m_commentsArray objectAtIndex:a] valueForKey:@"responseMessageBody"];
		if (![temp_string isEqualToString:@""])
		{
			UIView *temp_imgView2=[[UIView alloc]initWithFrame:CGRectMake(0, imgViewY, 310, 59)];
			temp_imgView2.tag = 32;
			
            // ================= text view with comment
			UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(40, 5, 210, 40)];
			textView.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
			//textView.font = [UIFont fontWithName:kFontName size:13];
			[textView setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_SIZE_KEY]]];
			textView.backgroundColor = [UIColor clearColor];
			textView.delegate = nil;
			textView.tag = 33;
			textView.text=temp_string;
			textView.returnKeyType = UIReturnKeyDefault;
			textView.keyboardType = UIKeyboardTypeDefault;
			textView.scrollEnabled = YES;
			textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
			textView.editable=YES;
			textView.userInteractionEnabled=NO;
			
			[temp_imgView2 addSubview:textView];
			
			
			temp_imgView2.frame=CGRectMake(0, imgViewY, 310, 25+textView.contentSize.height);
			
			
			textView.frame =CGRectMake(40, 5, 210,textView.contentSize.height);
			[temp_imgView2 addSubview:textView];
			
			UILabel *tmp_elapsedTime=[[UILabel alloc] initWithFrame:CGRectMake(47, textView.contentSize.height, 190, 12)];
			
            // ================= elapsed time
			
			tmp_elapsedTime.text=[NSString stringWithFormat:@"%@ %@ ",[[self.m_commentsArray objectAtIndex:a]valueForKey:@"responseCustomerName"],[[self.m_commentsArray objectAtIndex:a]valueForKey:@"elapsedTime"]];
			tmp_elapsedTime.backgroundColor=[UIColor clearColor];
			tmp_elapsedTime.textColor=[UIColor lightGrayColor];
			tmp_elapsedTime.font=[UIFont fontWithName:kFontName size:10];
			
			[temp_imgView2 addSubview:tmp_elapsedTime];
            
            CGRect newframe=[temp_imgView2  frame];
            
            AsyncButtonView* SenderImage = [[[AsyncButtonView alloc]
                                             initWithFrame:CGRectMake(5,(newframe.size.height-35)/2,35,35)] autorelease];
			
			[temp_imgView2 addSubview:SenderImage];

			NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[self.m_commentsArray objectAtIndex:a] valueForKey:@"thumbCustomerUrl"]];
			NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,60,32]];
            SenderImage.cropImage = YES;
            SenderImage.maskImage = YES;
            SenderImage.messageTag = a;
			[SenderImage loadImageFromURL:tempLoadingUrl target:self action:@selector(commenterImagePressed:) btnText:@""];
			
			
            UIView *lbl=[[UIView alloc] initWithFrame:CGRectMake(0, temp_imgView2.frame.size.height, 320 , 1)];
			
            
            [lbl setBackgroundColor:[UIColor lightGrayColor]];
            [temp_imgView2 addSubview:lbl];
            imgViewY = temp_imgView2.frame.origin.y + temp_imgView2.frame.size.height;
            [lbl release];
			[self.m_commentsView addSubview:temp_imgView2];
			
            
			[self.m_commentsView addSubview:temp_imgView2];
            
            imgViewY = temp_imgView2.frame.origin.y + temp_imgView2.frame.size.height;
			//textViewY=textViewY+4;
			[temp_imgView2 release];
			temp_imgView2=nil;
			[textView release];
			textView=nil;
			[tmp_elapsedTime release];
			tmp_elapsedTime=nil;
			counter++;
		}
	}
	//Bharat: 11/26/11: Set absolute value, do not set increments
	self.m_commentsView.contentSize = CGSizeMake(m_commentsView.contentSize.width,imgViewY+10);
}
#endif

- (void)commenterImagePressed:(id)sender
{
    ViewAFriendViewController *tempViewAFriend=[[ViewAFriendViewController alloc]init];
    NSDictionary *responseDict = [self.m_commentsArray objectAtIndex:((MoreButton *)sender).tag];
    
    tempViewAFriend.m_Email=[responseDict valueForKey:@"responseCustomerId"];
    
    // If it's my post - don't let click on it
    if ([tempViewAFriend.m_Email isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"]]) {
        return;
    }
    
    tempViewAFriend.m_strLbl1=[responseDict valueForKey:@"responseCustomerName"];
    tempViewAFriend.messageLayout = NO;
    [self.navigationController pushViewController:tempViewAFriend animated:YES];
    [tempViewAFriend release];
    tempViewAFriend=nil;
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                    withAction:@"commenterImagePressed"
                                                     withLabel:nil
                                                     withValue:nil];
}
#pragma mark -
-(void)viewDidAppear:(BOOL)animated
{
	//NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    //	NSArray *languages=[defs objectForKey:@"AppleLanguages"];
    //	
    //	for (int i=0; i<[languages count]; i++) 
    //	{
    //		NSLog(@"the count supported by the xcode is %d",[languages count]);
    //		NSLog(@"the objects inthe languages are %@",[languages objectAtIndex:i]);
    //		
    //	} 
	
//    [self.m_imageHeader setFrame:CGRectMake(0, 0, 320, 52)];
//    [m_scrollView setFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
	m_scrollView.contentSize = CGSizeMake(m_scrollView.frame.size.width,[UIScreen mainScreen].bounds.size.height );
	
}
- (void)viewDidLoad 
{
    [super viewDidLoad];
    self.trackedViewName = @"MyPage";
    [self getparticularBtuuonDetails:nil];
	[m_nameLabel setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:MYPAGEVIEWCONTROLLER_USERNAMELABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:MYPAGEVIEWCONTROLLER_USERNAMELABEL_FONT_SIZE_KEY]]];
	l_appDelegate.m_checkForRegAndMypage=@"2";
	m_theProfileArray=[[NSArray alloc] initWithObjects:@"About me:",@"Interests:",@"Favorite brand:",@"Favorite quotation:",nil];
	tmp_count=0;
	count=0;
	m_theMyPageTable.separatorColor=[UIColor clearColor];
	m_theMyPageTable.rowHeight=44;
//    [m_scrollView setFrame:CGRectMake(0, 74, 320, [UIScreen mainScreen].bounds.size.height - 74)];
	//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //	NSString *documentsDirectory = [paths objectAtIndex:0];
    //	NSString *fileName=[NSString stringWithFormat:@"%@.jpg",[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"]];
    //	
    //	NSMutableString *pathDoc=[NSString stringWithFormat:@"%@/%@",documentsDirectory,fileName];
    //	NSLog(@"Doc Directory______Path_____%@",pathDoc);
    //	
    //	NSFileManager *manager = [[NSFileManager alloc] init];
    //	
    //	[m_addPhotoButton.layer setBorderWidth:2.0];
    //	[m_addPhotoButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    //	
    //	if([manager fileExistsAtPath:pathDoc]) 
    //	{
    //		[m_addPhotoButton setImage:[UIImage imageWithContentsOfFile:pathDoc] forState:UIControlStateNormal];
    //			
    //	}
    //	else
    //	{
    //		[m_addPhotoButton setImage:[UIImage imageNamed:@"white&green_addphoto_button_Bing.png"]forState:UIControlStateNormal];
    //	}
    //	
    //	[manager release];
	
	l_indicatorView=[LoadingIndicatorView SharedInstance];
	
	l_request=[[ShopbeeAPIs alloc] init];
	m_RequestImageView.hidden=YES;          
	m_requestsCount.hidden=YES;
    [self.m_meView setContentOffset:CGPointMake(0, 0)];
    self.m_meView.contentSize = CGSizeMake(320, 420);
    self.m_meView.clipsToBounds = YES;
     [self addPullTorefreshView];
}

-(void)viewWillAppear:(BOOL)animated
{
	selectedIndex=-1;
    selectedFavIndex=-1;
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
	m_scrollView.contentSize = CGSizeMake(m_scrollView.frame.size.width,[UIScreen mainScreen].bounds.size.height );
    [self getResponse];
   
}

-(void)addPullTorefreshView
{
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    [refreshHeaderView setBackgroundColor:[UIColor clearColor]];
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = [UIFont boldSystemFontOfSize:12.0];
    refreshLabel.textAlignment = UITextAlignmentCenter;
    
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PullArrow.png"]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 27) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 20) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [self.m_scrollView addSubview:refreshHeaderView];
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (isLoading) return;
    isDragging = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            self.m_scrollView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            self.m_scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = @"Release to refresh...";;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = @"Pull down to refresh...";
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
}
- (void)startLoading {
    isLoading = YES;
    
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        self.m_scrollView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = @"Loading...";;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    
    // Refresh action!
    [self refresh];
}
-(void)refresh
{
    [self getResponse];
//    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:2.0];
}
-(void)stopLoading
{
    isLoading = NO;
    
    
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        self.m_scrollView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
    
    
}
- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = @"Pull down to refresh...";
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}


-(void)getResponse
{

    if (l_appDelegate.isMyPageShow==YES) //means reloading after log out (on re-login)
	{
		l_appDelegate.m_checkForRegAndMypage=@"2";
		m_scrollView.hidden=YES;
		l_appDelegate.isMyPageShow=NO;
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *fileName=[NSString stringWithFormat:@"%@.jpg",[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"]];
		
		NSMutableString *pathDoc=[NSString stringWithFormat:@"%@/%@",documentsDirectory,fileName];
		NSLog(@"Doc Directory______Path_____%@",pathDoc);
		
		NSFileManager *manager = [[NSFileManager alloc] init];
		
		if([manager fileExistsAtPath:pathDoc])
		{
            self.m_personImage = [UIImage imageWithContentsOfFile:pathDoc];
            [self adjustBounds:self.m_personImage forButton:m_addPhotoButton];
//			[m_addPhotoButton setImage:self.m_personImage forState:UIControlStateNormal];
			
		}
		else
		{
            self.m_personImage = [UIImage imageNamed:@"no-image"];
			[m_addPhotoButton setImage:[UIImage imageNamed:@"addphoto_icon.png"]forState:UIControlStateNormal];
		}
		
		[manager release];
		count=0;
        
	}
	
	if (isPictureSelected==NO)
	{
		[m_theMyPageTable reloadData];
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *custID= [prefs objectForKey:@"userName"];
		[l_indicatorView startLoadingView:self];
        
        if ([userID length] <= 0)
        {
            [m_SettingButton setHidden:NO];
            [m_backButton setHidden:YES];
            [m_addFriedButton setHidden:YES];
            [l_request getUserInfo:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:custID loginId:custID];
        }
        else
        {
            [m_SettingButton setHidden:YES];
            [m_backButton setHidden:NO];
            [m_addFriedButton setHidden:NO];
            [l_request getUserInfo:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:custID loginId:userID];
        }
	}
	else
	{
		isPictureSelected=NO;
	}
    
	if (BackButtonTrack==YES)
	{
		m_backButton.hidden=NO;
	}
	else
	{
        
	}
    
    self.m_profileInfoBtn.selected = self.m_commentsView.isHidden;
    self.m_commentsButton.selected = !self.m_commentsView.isHidden;
    [[MessageBoardManager sharedManager] getMessageBoardId:self];
    // get messages:

}

- (void) setMessageId:(NSString *)messageID
{
    NSString *myMessageID = [[NSUserDefaults standardUserDefaults] objectForKey:@"MessageBoardID"];
    if (!myMessageID || (myMessageID && ![myMessageID isEqualToString:messageID])) {
        [[NSUserDefaults standardUserDefaults] setValue:messageID forKey:@"MessageBoardID"];
    }
    [[MessageBoardManager sharedManager] getComments:self];
}


- (void) setCommentsArray:(NSArray *)anArray
{
    if (!anArray) {
        addCommentButton.enabled = NO;
        return;
    }
    addCommentButton.enabled = YES;
    self.m_dataArray = anArray;
    self.m_commentsArray = [[[self.m_dataArray objectAtIndex:0] valueForKey:@"messageResponseList"] reversedArray];
    [self presentComments];
}

- (IBAction)showMessages:(id)sender
{
    self.m_commentsButton.selected = YES;
    self.m_profileInfoBtn.selected = NO;
    
    [self.m_theMyPageTable setHidden:YES];
    [self.m_commentsView setHidden:NO];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                    withAction:@"showMessages"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (IBAction)showProfile:(id)sender
{
    self.m_commentsButton.selected = NO;
    self.m_profileInfoBtn.selected = YES;

    [self.m_commentsView setHidden:YES];
    [self.m_theMyPageTable setHidden:NO];
    [self.m_theMyPageTable reloadData];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                    withAction:@"showProfile"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (IBAction)addComment:(id)sender
{
	BuyItCommentViewController *commentController = [[BuyItCommentViewController alloc] init];
    
    // GDL: I was releasing one of the components of m_MainArray.
	commentController.m_MainArray = self.m_dataArray;
	commentController.m_FavourString = @"";
	commentController.m_CallBackViewController = self;//**
    commentController.m_personImage = self.m_personImage;
	[self.navigationController pushViewController:commentController animated:YES];
	[commentController release];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                    withAction:@"postMessage"
                                                     withLabel:nil
                                                     withValue:nil];
}


// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [self setM_imageHeader:nil];
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_addPhotoButton = nil;
    self.m_changePic = nil;
    self.m_changePhotoView = nil;
    self.m_theMyPageTable = nil;
    self.m_changePicView = nil;
    self.m_nameLabel = nil;
    self.m_wishlistLabel = nil;
    self.m_friendsLabel = nil;
    self.m_photoLabel = nil;
    self.m_scrollView = nil;
    self.m_followingBtn = nil;
    self.m_followerBtn = nil;
    self.m_requestsCount = nil;
    self.m_RequestImageView = nil;
    self.m_backButton = nil;
    self.refreshLabel=nil;
    self.refreshSpinner=nil;
    self.refreshHeaderView=nil;
    self.refreshArrow=nil;
    
    self.m_meBtn=nil;
    self.m_messageBtn=nil;
    self.m_StoriesBtn=nil;
    self.m_FavoritesBtn=nil;
    self.m_cityLBL=nil;
    self.m_meView=nil;
    self.m_aboutme=nil;
    self.m_favoriteLBL=nil;
    self.m_chatView=nil;
    self.m_storyView=nil;
    self.m_storyHeaderLBL=nil;
    self.m_storyMore=nil;
    self.m_FavoritesView=nil;
    self.m_FavoriteHeaderLBL=nil;
    self.m_FavoritesyMore=nil;
    self.m_mystoryheader=nil;
    self.m_myfavoriteheader=nil;
    self.favBrandLabel = nil;
    self.interestsLabel = nil;
}

- (void)dealloc {
    
    [m_changePhotoView release];
    [m_addPhotoButton release];
    [m_changePic release];
    [m_changePicView release];
    [m_theProfileArray release];
    [m_theMyPageTable release];
    [m_profileSettings release];
    [m_photocontroller release];
    [m_nameLabel release];
    [m_photoLabel release];
    [m_wishlistLabel release];
    [m_friendsLabel release];
    [m_scrollView release];
    [m_followingBtn release];
    [m_followerBtn release];
    [m_personImage release];
    [m_personData release];
    [m_requestsCount release];
    [m_RequestImageView release];
    [l_request release];
    [m_backButton release];
    [m_commentsView release];
    [m_commentsArray release];
    [m_profileInfoBtn release];
    [m_commentsButton release];
    [addCommentButton release];
    //[_m_imageHeader release];
    
    [refreshLabel release];
    [refreshSpinner release];
    [refreshHeaderView release];
    [refreshArrow release];
    
    [m_meBtn release];
    [m_messageBtn release];
    [m_StoriesBtn release];
    [m_FavoritesBtn release];
    [m_cityLBL release];
    [m_meView release];
    [m_aboutme release];
    [m_favoriteLBL release];
    [m_chatView release];
    [m_storyView release];
    [m_storyHeaderLBL release];
    [m_storyMore release];
    [m_FavoritesView release];
    [m_FavoriteHeaderLBL release];
    [m_FavoritesyMore release];
    [m_mystoryheader release];
    [m_myfavoriteheader release];
    [favBrandLabel release];
    [interestsLabel release];
    [super dealloc];	
}

@end
