//
//  PrivacySettingsViewController.m
//  QNavigator
//
//  Created by softprodigy on 14/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PrivacySettingsViewController.h"
#import"Constants.h"
#import"ShopbeeAPIs.h"
#import"LoadingIndicatorView.h"
#import"QNavigatorAppDelegate.h"
#import "NewsDataManager.h"

@implementation PrivacySettingsViewController


@synthesize m_scrollView;
@synthesize m_label;
@synthesize m_HeaderLable;
LoadingIndicatorView *l_PrivacyIndicatorView;

QNavigatorAppDelegate *l_appDelegate;
#pragma mark Custom methods-----
-(IBAction) m_goToBackView{      // to navigate back to the previous views
	[self.navigationController popViewControllerAnimated:YES ];
	
}
-(IBAction)m_feedBtnAction:(id)sender
{
	UIActionSheet *alert=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" 
									   destructiveButtonTitle:nil otherButtonTitles:@"All",@"Friends only",nil];
	[alert showInView:self.view];
	[alert setTag:[sender tag]];
	NSLog(@"%i",[sender tag]);
	[alert release];
	
}

-(IBAction)SaveBtnAction
{
	ShopbeeAPIs *l_request=[[ShopbeeAPIs alloc] init];
	[l_PrivacyIndicatorView startLoadingView:self];
	[l_request setPrivacySettings:@selector(requestCallBackMethod:responseData:) tempTarget:self settingDict:(NSMutableDictionary *)m_SettingsDictionary];
	
	[m_SettingsDictionary release];
	m_SettingsDictionary=nil;
	[l_request release];
	l_request=nil;
}
-(IBAction)InitializeScrollView
{
	m_scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0,116,320,310)];
	
	m_scrollView.pagingEnabled = NO;
	m_scrollView.userInteractionEnabled = YES;
	m_scrollView.showsVerticalScrollIndicator = YES;
	m_scrollView.showsHorizontalScrollIndicator = NO;
	
	m_scrollView.scrollsToTop = NO;
	m_scrollView.contentOffset = CGPointMake(0,0);
	m_scrollView.backgroundColor = [UIColor clearColor];
	
	
	UIButton *temp_feeds = [UIButton buttonWithType:UIButtonTypeCustom];
	[temp_feeds setTag:1];
	UILabel *tmp_personalInfo=[[UILabel alloc] initWithFrame:CGRectMake(10, 15, 150, 20)];
	tmp_personalInfo.text=@"My Personal Info";
	tmp_personalInfo.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	tmp_personalInfo.font=[UIFont fontWithName:kFontName size:14];
	tmp_personalInfo.backgroundColor=[UIColor clearColor];
	m_label=[[UILabel alloc] initWithFrame:CGRectMake(185, 13, 90,25)];
	m_label.backgroundColor=[UIColor clearColor];
	m_label.textColor=[UIColor lightGrayColor];
	m_label.font=[UIFont fontWithName:kFontName size:16];
	m_label.textAlignment=UITextAlignmentRight;
	m_label.text=@"All";
	UIImageView *m_personalInfoImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 66, 320, 1)];
    [m_personalInfoImage    setImage:[UIImage imageNamed:@"grey_line.png"]];
    
    temp_feeds.frame = CGRectMake(13, 20, 292, 45); // position in the parent view and set the size of the button
	[temp_feeds setImage:[UIImage imageNamed:@"Settings_bar.png"] forState:UIControlStateNormal];
	// add targets and actions
    [temp_feeds addTarget:self action:@selector(m_feedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
	[temp_feeds addSubview:m_label];
	[temp_feeds addSubview:tmp_personalInfo];
    [m_scrollView addSubview:m_personalInfoImage];
    [m_scrollView addSubview:temp_feeds];
	[tmp_personalInfo release];
	tmp_personalInfo=nil;
    [m_personalInfoImage release];
    m_personalInfoImage=nil;
	
	UIButton *temp_aboutMe= [UIButton buttonWithType:UIButtonTypeCustom];
	[temp_aboutMe setTag:2];
	UILabel *tmp_followingInfo=[[UILabel alloc] initWithFrame:CGRectMake(10, 15, 150, 20)];
	tmp_followingInfo.text=@"People I am Following";
	tmp_followingInfo.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	tmp_followingInfo.font=[UIFont fontWithName:kFontName size:14];
	tmp_followingInfo.backgroundColor=[UIColor clearColor];
	[temp_aboutMe addSubview:tmp_followingInfo];
	m_aboutmelabel=[[UILabel alloc] initWithFrame:CGRectMake(185, 13,90, 25)];
	m_aboutmelabel.backgroundColor=[UIColor clearColor];
	m_aboutmelabel.textColor=[UIColor lightGrayColor];
	m_aboutmelabel.font=[UIFont fontWithName:kFontName size:16];
	m_aboutmelabel.text=@"All";
	m_aboutmelabel.textAlignment=UITextAlignmentRight;
    
    UIImageView *m_aboutmelabelImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 116, 320, 1)];
    [m_aboutmelabelImage    setImage:[UIImage imageNamed:@"grey_line.png"]];
	temp_aboutMe.frame = CGRectMake(13, 70, 292, 45); // position in the parent view and set the size of the button
    [temp_aboutMe setImage:[UIImage imageNamed:@"Settings_bar.png"] forState:UIControlStateNormal];
	[temp_aboutMe addSubview:m_aboutmelabel];
	[temp_aboutMe addTarget:self action:@selector(m_feedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
	[tmp_followingInfo release];
	tmp_followingInfo=nil;
	// add to a view
    [m_scrollView addSubview:temp_aboutMe];
    [m_scrollView addSubview:m_aboutmelabelImage];
    [m_aboutmelabelImage release];
    m_aboutmelabelImage=nil;
	
	
	UIButton *temp_interested = [UIButton buttonWithType:UIButtonTypeCustom];
	[temp_interested setTag:3];
    UILabel *tmp_photosInfo=[[UILabel alloc] initWithFrame:CGRectMake(10, 15, 150, 20)];
	tmp_photosInfo.text=@"My Photos";
	tmp_photosInfo.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	tmp_photosInfo.font=[UIFont fontWithName:kFontName size:14];
	tmp_photosInfo.backgroundColor=[UIColor clearColor];
	[temp_interested addSubview:tmp_photosInfo];

	temp_interested.frame = CGRectMake(13, 120,292, 45);
	m_interestedlabel=[[UILabel alloc] initWithFrame:CGRectMake(185, 13,90, 25)];
	m_interestedlabel.backgroundColor=[UIColor clearColor];
	m_interestedlabel.textColor=[UIColor lightGrayColor];
	m_interestedlabel.font=[UIFont fontWithName:kFontName size:16];
	m_interestedlabel.text=@"All";
	m_interestedlabel.textAlignment=UITextAlignmentRight;// position in the parent view and set the size of the button
    [temp_interested setImage:[UIImage imageNamed:@"Settings_bar.png"] forState:UIControlStateNormal];
	[temp_interested addTarget:self action:@selector(m_feedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
	[temp_interested addSubview:m_interestedlabel];
    [m_scrollView addSubview:temp_interested];
    UIImageView *m_interestedlImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 166, 320, 1)];
    [m_interestedlImage    setImage:[UIImage imageNamed:@"grey_line.png"]];
	[m_scrollView addSubview:m_interestedlImage];
    [m_interestedlImage release];
    m_interestedlImage=nil;
	
	UIButton *temp_fvbrand= [UIButton buttonWithType:UIButtonTypeCustom];
	[temp_fvbrand setTag:4];
	UILabel *tmp_friendsInfo=[[UILabel alloc] initWithFrame:CGRectMake(10, 15, 150, 20)];
	tmp_friendsInfo.text=@"My Friends";
	tmp_friendsInfo.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	tmp_friendsInfo.font=[UIFont fontWithName:kFontName size:14];
	tmp_friendsInfo.backgroundColor=[UIColor clearColor];
	[temp_fvbrand addSubview:tmp_friendsInfo];

    temp_fvbrand.frame = CGRectMake(13, 170, 292, 45);
	m_fvbrandlabel=[[UILabel alloc] initWithFrame:CGRectMake(185, 13,90, 25)];
	m_fvbrandlabel.backgroundColor=[UIColor clearColor];
	m_fvbrandlabel.textColor=[UIColor lightGrayColor];
	m_fvbrandlabel.font=[UIFont fontWithName:kFontName size:16];
	m_fvbrandlabel.text=@"All";
	m_fvbrandlabel.textAlignment=UITextAlignmentRight;// position in the parent view and set the size of the button
	[temp_fvbrand addTarget:self action:@selector(m_feedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [temp_fvbrand setImage:[UIImage imageNamed:@"Settings_bar.png"] forState:UIControlStateNormal];
	[temp_fvbrand addSubview:m_fvbrandlabel];
	[m_scrollView addSubview:temp_fvbrand];
    
	UIImageView *m_fvbrandImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 216, 320, 1)];
    [m_fvbrandImage    setImage:[UIImage imageNamed:@"grey_line.png"]];
	[m_scrollView addSubview:m_fvbrandImage];
    [m_fvbrandImage release];
    m_fvbrandImage=nil;
	
	UIButton *temp_fvquotation = [UIButton buttonWithType:UIButtonTypeCustom];
	[temp_fvquotation setTag:5];
	UILabel *tmp_wishlistInfo=[[UILabel alloc] initWithFrame:CGRectMake(10, 15, 150, 20)];
	tmp_wishlistInfo.text=@"My Wishlist";
	tmp_wishlistInfo.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	tmp_wishlistInfo.font=[UIFont fontWithName:kFontName size:14];
	tmp_wishlistInfo.backgroundColor=[UIColor clearColor];
	[temp_fvquotation addSubview:tmp_wishlistInfo];
	
    temp_fvquotation.frame = CGRectMake(13, 220, 292, 45); // position in the parent view and set the size of the button
    [temp_fvquotation setImage:[UIImage imageNamed:@"Settings_bar.png"] forState:UIControlStateNormal];
	[temp_fvquotation addTarget:self action:@selector(m_feedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
	m_fvquotationlabel=[[UILabel alloc] initWithFrame:CGRectMake(185, 13,90, 25)];
	m_fvquotationlabel.backgroundColor=[UIColor clearColor];
	m_fvquotationlabel.textColor=[UIColor lightGrayColor];
	m_fvquotationlabel.font=[UIFont fontWithName:kFontName size:16];
	m_fvquotationlabel.text=@"All";
	m_fvquotationlabel.textAlignment=UITextAlignmentRight;
	[temp_fvquotation addSubview:m_fvquotationlabel];
	[m_scrollView addSubview:temp_fvquotation];
    
	m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width, 500);//-50);
	
	[self.view addSubview:m_scrollView];
	
	[m_scrollView release];
	m_scrollView=nil;
}

#pragma mark action sheet methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_username=[prefs valueForKey:@"userName"];
	[m_SettingsDictionary setValue:tmp_username forKey:@"userId"];
	if (actionSheet.tag==1) 
	{
		if (buttonIndex==0) 
		{
			m_label.text=@"All";
			[m_SettingsDictionary setValue:@"All" forKey:@"myPersonalInfoSetting"];
		}
		else if(buttonIndex==1)
		{
			m_label.text=@"Friends only";
			[m_SettingsDictionary setValue:@"Friend only" forKey:@"myPersonalInfoSetting"];
		}
	}
	if (actionSheet.tag==2) {
		if (buttonIndex==0) {
			m_aboutmelabel.text=@"All";
			[m_SettingsDictionary setValue:@"All" forKey:@"peopleIFollowingSetting"];
		}
		else if(buttonIndex==1)
		{
			m_aboutmelabel.text=@"Friends only";
			[m_SettingsDictionary setValue:@"Friend only" forKey:@"peopleIFollowingSetting"];
		}
	}
	if (actionSheet.tag==3) 
	{
		if (buttonIndex==0) 
		{
			m_interestedlabel.text=@"All";
			[m_SettingsDictionary setValue:@"All" forKey:@"myPhotosSetting"];
		}
		else if(buttonIndex==1)
		{
			m_interestedlabel.text=@"Friends only";
			[m_SettingsDictionary setValue:@"Friend only" forKey:@"myPhotosSetting"];
		}
	}
	if (actionSheet.tag==4) 
	{
		if (buttonIndex==0) 
		{
			m_fvbrandlabel.text=@"All";
			[m_SettingsDictionary setValue:@"All" forKey:@"myFriendsSetting"];
		}
		else if(buttonIndex==1)
		{
			m_fvbrandlabel.text=@"Friends only";
			[m_SettingsDictionary setValue:@"Friend only" forKey:@"myFriendsSetting"];
		}
	}
	if (actionSheet.tag==5) 
	{
		if (buttonIndex==0) 
		{
			m_fvquotationlabel.text=@"All";
			[m_SettingsDictionary setValue:@"All" forKey:@"myWishlistSetting"];
		}
		else if(buttonIndex==1)
		{
			m_fvquotationlabel.text=@"Friends only";
			[m_SettingsDictionary setValue:@"Friend only" forKey:@"myWishlistSetting"];

		}
	}
}	
#pragma mark -
#pragma mark call back methods
-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_PrivacyIndicatorView stopLoadingView];
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	if ([responseCode intValue]==200) 
	{
		//[m_backup removeAllObjects];
		
		NSLog(@"response string: %@",tempString);
		[self.navigationController popViewControllerAnimated:YES];		
	}
	else {
		NSLog(@"Privacy settings response result: %d",[responseCode intValue]);
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }
	
	[tempString release];
	tempString=nil;
}
-(void)requestCallBackMethodForGetPrivacySettings:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_PrivacyIndicatorView stopLoadingView];
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSArray *tmp_array=[tempString JSONValue];
	NSDictionary *tmp_SettingDict=[tmp_array objectAtIndex:0];
	m_SettingsDictionary=[[NSMutableDictionary dictionaryWithDictionary:[tmp_array objectAtIndex:0]]retain];
	if ([responseCode intValue]==200) 
	{
		NSLog(@"response string: %@",tempString);
		if ([[tmp_SettingDict valueForKey:@"myPersonalInfoSetting"] isEqualToString:@"All"]) 
		{
			m_label.text=@"All";
			//	[m_SettingsDictionary setValue:@"" forKey:<#(NSString *)key#>]
		}
		else 
		{
			m_label.text=@"Friends only";
			
		}
		
		if ([[tmp_SettingDict valueForKey:@"peopleIFollowingSetting"] isEqualToString:@"All"]) 
		{
			m_aboutmelabel.text=@"All";
		}
		else 
		{
			m_aboutmelabel.text=@"Friends only";
			
		}
		
		if ([[tmp_SettingDict valueForKey:@"myPhotosSetting"] isEqualToString:@"All"]) 
		{
			m_interestedlabel.text=@"All";
		}
		else 
		{
			m_interestedlabel.text=@"Friends only";
			
		}
		
		if ([[tmp_SettingDict valueForKey:@"myFriendsSetting"] isEqualToString:@"All"]) 
		{
			m_fvbrandlabel.text=@"All";
		}
		else 
		{
			m_fvbrandlabel.text=@"Friends only";
			
		}
		
		if ([[tmp_SettingDict valueForKey:@"myWishlistSetting"] isEqualToString:@"All"]) 
		{
			m_fvquotationlabel.text=@"All";
		}
		else 
		{
			m_fvquotationlabel.text=@"Friends only";
			
		}
	}
	else {
            NSLog(@"Privacy settings response result: %d",[responseCode intValue]);
            [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }
	
	[tempString release];
	tempString=nil;
	
	
}
#pragma mark-
	

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewDidAppear:(BOOL)animated
{
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	[self InitializeScrollView];
	//m_SettingsDictionary=[[NSMutableDictionary alloc] init];
	l_PrivacyIndicatorView=[LoadingIndicatorView SharedInstance];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_username=[prefs valueForKey:@"userName"];
	[l_PrivacyIndicatorView startLoadingView:self];
	ShopbeeAPIs *l_requestObj=[[ShopbeeAPIs alloc] init];
	[l_requestObj getPrivacySettings:@selector(requestCallBackMethodForGetPrivacySettings:responseData:) tempTarget:self custid:tmp_username];
	[l_requestObj release];
	l_requestObj=nil;
    UIColor *m_hederColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"lightgrey_bar.png"]];
    m_HeaderLable.backgroundColor=m_hederColor;
    
}

-(void)viewWillAppear:(BOOL)animated
{
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_scrollView = nil;
    self.m_label = nil;
    
}

- (void)dealloc {
	[m_scrollView release];
	[m_label release];
    [m_aboutmelabel release];
    [m_interestedlabel release];
    [m_fvbrandlabel release];
    [m_fvquotationlabel release];
    [m_SettingsDictionary release];
    

    [super dealloc];
}

@end
