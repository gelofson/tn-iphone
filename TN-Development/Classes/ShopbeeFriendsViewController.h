//
//  ShopbeeFriendsViewController.h
//  QNavigator
//
//  Created by softprodigy on 14/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SA_OAuthTwitterEngine.h" 
#import"SA_OAuthTwitterController.h"
#ifdef OLD_FB_SDK
#import "FBConnect/FBConnect.h"
#endif
#import "QNavigatorAppDelegate.h"
#import "FBConnect.h"
#import "JSON.h"
#import "FbGraph.h"


@interface ShopbeeFriendsViewController : UIViewController<UISearchBarDelegate,UITableViewDelegate
,UITableViewDataSource, FBSessionDelegate, FBRequestDelegate, FBDialogDelegate> {
#ifdef OLD_FB_SDK
	FbGraph *fbGraph;
	FBSession				*m_session;
	FBLoginDialog			*m_loginDialog;
#endif

	UITableView *m_tableView;
	NSString				*facebookName;
	BOOL			    		m_posting;
	NSDictionary            *m_userDetails;
	NSArray					*myList;
	NSMutableArray			*m_friendsDetails;
	UIImageView				*m_imgLogo;
	UISearchBar *m_searchBar;
	NSArray *m_arrNonFrnds;
	NSMutableArray *m_arrFrnds;
	UIActivityIndicatorView *m_activityInidicator;
	NSMutableString *m_strUids;
	UIView *m_view;
	NSMutableArray *m_shopBeeArray;
	NSMutableArray *m_nonshopBeeArray;
	int m_intCurrentUserIndex;
	NSMutableArray *m_picUrlNonShopBee;
	NSMutableArray *m_picUrlShopBee;
	UIButton *btnFbclose;
    int currentAPICall;
}
@property (nonatomic, retain)UIButton *btnFbclose;
@property (nonatomic, retain) FbGraph *fbGraph;
@property (nonatomic,retain) NSArray *m_arrNonFrnds;
@property (nonatomic,retain) NSMutableArray *m_arrFrnds;
@property (nonatomic,retain)IBOutlet UISearchBar *m_searchBar;
@property (nonatomic,retain)NSMutableArray *m_picUrlNonShopBee;
@property (nonatomic,retain)NSMutableArray *m_picUrlShopBee;
@property int m_intCurrentUserIndex;
@property (nonatomic,retain)NSMutableArray *m_shopBeeArray;
@property (nonatomic,retain)NSMutableArray *m_nonshopBeeArray;
@property (nonatomic,retain)NSMutableString *m_strUids;
@property (nonatomic,retain) UIView *m_view;
@property (nonatomic,retain)UIActivityIndicatorView *m_indicatorView;

@property (nonatomic,retain) IBOutlet UIImageView *m_imgLogo;

@property(nonatomic,retain)IBOutlet UITableView *m_tableView;


#ifdef OLD_FB_SDK
@property(nonatomic,retain) FBLoginDialog			*m_loginDialog;
#endif

@property(nonatomic,retain) NSString				*facebookName;

@property(nonatomic,retain) NSDictionary            *m_userDetails;

#ifdef OLD_FB_SDK
@property(nonatomic,retain) FBSession				*m_session;
#endif

@property(nonatomic,retain)	NSMutableArray *m_friendsDetails;

@property(nonatomic,retain)IBOutlet UITableView *table_AddFriends;


@property(nonatomic,retain) NSMutableArray *arr_str;
@property(nonatomic,retain)NSMutableArray *backup_m_arrFrndsDic;


-(IBAction)btnbackAction:(id)sender;
-(void)btn_AddAction:(id)sender;
-(void)sendAddFriendRequest;
- (void)getFacebookName;
-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage;
-(IBAction)btnLogoutAction:(id)sender;
- (IBAction)setSelectedButton1:(id)sender;
-(void)closeButtonClicked;

@end
