//
//  BigSalesModelClass.h
//  QNavigator
//
//  Created by Soft Prodigy on 07/09/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BigSalesModelClass : NSObject 
{
	NSString *m_strId;
	NSString *m_strAdTitle;
	NSString *m_strDescription1;
	NSString *m_strDescription2;
	NSString *m_strLatitude;
	NSString *m_strLongitude;
	NSString *m_strMallImageUrl;
	NSString *m_strAdUrl;
	NSString * m_strAddress;
	UIImage *m_imgAd;
}

@property(nonatomic,retain)NSString *m_strAdTitle;
@property(nonatomic,retain)NSString *m_strDescription1;
@property(nonatomic,retain)NSString *m_strDescription2;
@property(nonatomic,retain)NSString *m_strLatitude;
@property(nonatomic,retain)NSString *m_strLongitude;
@property(nonatomic,retain)NSString *m_strMallImageUrl;
@property(nonatomic,retain) NSString * m_strAddress;
@property(nonatomic,retain)NSString *m_strAdUrl;
@property(nonatomic,retain)UIImage *m_imgAd;
@property(nonatomic,retain)NSString *m_strId;

@end