//
//  Notification.h
//  QNavigator
//
//  Created by Nicolas Jakubowski on 10/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

enum NotificationType {
    NotificationMessage = 1,
    NotificationAcceptReject = 2
    };

typedef enum {
    NotificationTypeMessage,
    NotificationTypeAcceptReject
} NotificationType;

@interface Notification : NSObject{
    NSString*           m_elapsedTime;
    NSString*           m_email;
    NSString*           m_firstName;
    NSString*           m_imageUrl;
    NSString*           m_lastname;
    NSString*           m_locationDetails;
    NSString*           m_messageId;
    NSString*           m_messageType;
    NSString*           m_notificationId;
    NSString*           m_notificationMessage;
    NSString*           m_thumbImageUrl;
    NotificationType    m_notificationType;
}

@property (nonatomic,retain) NSString*          elapsedTime;
@property (nonatomic,retain) NSString*          email;
@property (nonatomic,retain) NSString*          firstName;
@property (nonatomic,retain) NSString*          imageUrl;
@property (nonatomic,retain) NSString*          lastname;
@property (nonatomic,retain) NSString*          locationDetails;
@property (nonatomic,retain) NSString*          messageId;
@property (nonatomic,retain) NSString*          messageType;
@property (nonatomic,retain) NSString*          notificationId;
@property (nonatomic,retain) NSString*          notificationMessage;
@property (nonatomic,retain) NSString*          thumbImageUrl;
@property (nonatomic,assign) NotificationType   notificationType;

- (void)parseFromDictionary:(NSDictionary*)d;

@end
