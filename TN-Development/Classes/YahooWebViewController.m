//
//  YahooWebViewController.m
//  QNavigator
//
//  Created by sukhwinder on 28/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "YahooWebViewController.h"
#import "YahooFriendsViewController.h"
#import "NewsDataManager.h"

@implementation YahooWebViewController
@synthesize m_webview;
@synthesize strUrlToLoad;
@synthesize m_activityIndicator;
@synthesize m_yahooFrndsview;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	[m_webview loadRequest:[NSURLRequest requestWithURL:strUrlToLoad]];
	
	
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	[m_activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	[m_activityIndicator stopAnimating];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	[m_activityIndicator stopAnimating];

    if ([error code] == 401 || [error code] == 500) {
        NSLog(@"YahooWebViewController %d", [error code]);
        [[NewsDataManager sharedManager] showErrorByCode:[error code] fromSource:NSStringFromClass([self class])];
    }

}


-(IBAction)btnDoneAction:(id)sender
{
	[m_webview stopLoading];
	
	[(YahooFriendsViewController *)self.m_yahooFrndsview setShouldShowVerificationView:YES];
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[m_activityIndicator release];
	[m_webview release];
    [super dealloc];
}


@end
