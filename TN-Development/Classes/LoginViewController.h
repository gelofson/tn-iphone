//
//  LoginViewController.h
//  QNavigator
//
//  Created by Soft Prodigy on 30/09/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginHomeViewController.h"

@class ForgetPassword;
@interface LoginViewController : UIViewController {
	UITextField					*m_txtEmail;
	UITextField					*m_txtPassword;
	UIActivityIndicatorView		*m_activityIndicator;
	NSMutableData				*m_mutResponseData;
	UIButton					*m_btnCancel;
	
	UIButton						*m_rememberMeButton ;
	
	
	NSString	*m_strUsername;
	NSString	*m_strPassword;
	BOOL		m_fromRegistration;
	BOOL		m_isLoggedOut;
	
	BOOL		isDontRememberMeState;
	LoginHomeViewController *m_objLoginHome;
	//ForgetPassword *m_objforgetpassword;
	UIImageView *doneBackground;
	
	UIButton *doneButton;

	int m_intResponseStatusCode;	
}

@property (nonatomic,retain) LoginHomeViewController *m_objLoginHome;
//@property (nonatomic,retain) ForgetPassword *m_objforgetpassword;


@property BOOL m_isLoggedOut;

@property (nonatomic, retain) IBOutlet UIButton						*m_rememberMeButton ;
@property (nonatomic,retain) IBOutlet UITextField					*m_txtEmail;
@property (nonatomic,retain) IBOutlet UITextField					*m_txtPassword;
@property (nonatomic,retain) IBOutlet UIActivityIndicatorView		*m_activityIndicator;
@property (nonatomic,retain) IBOutlet UIButton						*m_btnCancel;
@property (nonatomic,retain) NSMutableData							*m_mutResponseData;
@property					BOOL									m_fromRegistration;
//@property(nonatomic,retain)ForgetPassword *m_objforgetpassword;

@property	 int m_intResponseStatusCode;

-(IBAction)btnCancelAction:(id)sender;
-(IBAction)btnGoAction:(id)sender;
-(void)sendRequestForLogin;
-(IBAction)btnforgotPassword:(id)sender;
-(void)addTopBarOnKeyboard;
-(void)dismissLoginView;


@end
