//
//  NewRegistration.h
//  QNavigator
//
//  Created by softprodigy on 15/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewRegistration : UIViewController<UIActionSheetDelegate,UINavigationControllerDelegate
,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UITextFieldDelegate> {
	
	UITextField				*m_txtFirstName;
	UITextField				*m_txtLastName;
	UITextField				*m_txtEmail;
	UITextField				*m_txtPassword;
	UIButton                *m_MrButton;
	UIButton                *m_MsButton;
	UIActivityIndicatorView *m_activityIndicator;
	UIPickerView            *m_agePicker;
	NSMutableData			*m_mutResponseData;
	int						m_intResponseCode;
	NSArray                 *m_agesArray;
	NSString                *m_gender;
	IBOutlet UIPickerView	*m_pickerView;
	IBOutlet UIToolbar		*m_toolbarWithDoneButton;
	UIButton                *m_ageGroupBtn;
	UIImage					*m_personImage;
    NSData					*m_personData;
	NSString				*m_ageGroup;
	UIButton				*m_TakePictureBtn;
	UIView					*m_view;
    UIView                  *m_nextView;
	UIActivityIndicatorView *m_indicatorView;
    
    // GDL: should be id, not id *
	id						m_homeViewObj;

}

// GDL: should be id, not id *
@property (nonatomic,retain) id m_homeViewObj;
@property(nonatomic,retain)IBOutlet  UIView                  *m_nextView;
@property (nonatomic,retain) IBOutlet UITextField		*m_txtFirstName;

@property (nonatomic,retain) IBOutlet UITextField		*m_txtLastName;

@property (nonatomic,retain) IBOutlet UITextField		*m_txtEmail;

@property (nonatomic,retain) IBOutlet UITextField		*m_txtPassword;

@property (nonatomic,retain) IBOutlet UIButton            *m_MsButton;

@property(nonatomic,retain) IBOutlet UIButton             *m_MrButton;

@property (nonatomic, retain) UIPickerView *m_pickerView;

@property(nonatomic,retain) UIToolbar *m_toolbarWithDoneButton;

@property(nonatomic,retain) IBOutlet UIButton *m_ageGroupBtn;

@property (nonatomic,retain) NSData *m_personData;

@property(nonatomic,retain) IBOutlet UIButton			*m_TakePictureBtn;

@property int m_intResponseCode;

@property (retain, nonatomic) IBOutlet UIImageView *m_imageHeader;

-(IBAction) btnCancelAction:(id)sender;

-(IBAction) btnSaveAction:(id)sender;

-(IBAction) btnTakePictureAction;

-(IBAction) MrButtonSelected;

-(IBAction) MsButtonSelected;

-(IBAction) btnPrivacyStatementAction:(id)sender;

-(IBAction) btnTOSAction:(id)sender;

-(IBAction) btnAgeGroupAction;

-(IBAction) btnDoneAction;

-(void)animateViewUpward;

-(void)animateViewDownward;

-(void)sendRequestForRegistration:(int )ignoreSaleData;

-(void)resignAllResponders;

-(IBAction) m_ShowActivityIndicator;
-(IBAction)btnGoAction:(id)sender;
-(IBAction)btnNewCancleAction:(id)sender;

@end
