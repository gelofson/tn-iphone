//
//  PictureRequest.h
//  TinyNews
//
//  Created by Nava Carmon on 01/11/12.
//
//

#import <Foundation/Foundation.h>

@interface PictureRequest : NSOperation
{
    NSURL *picUrl;
    NSString *saveName;
    id       delegate;
}

- (id) initWithPictureURL:(NSURL *) url nameToSave:(NSString *) name ignoreCache:(BOOL)ignore;
- (id) initWithPictureURL:(NSURL *) url nameToSave:(NSString *) name;
- (void) performMain;

@property (nonatomic, retain) NSURL *picUrl;
@property (nonatomic, retain) NSString *saveName;
@property (nonatomic, retain) id       delegate;


@end
