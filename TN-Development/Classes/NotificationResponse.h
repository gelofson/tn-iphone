//
//  NotificationResponse.h
//  QNavigator
//
//  Created by Nicolas Jakubowski on 10/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationResponse : NSObject{
    NSString*               m_serverResponse;
    NSMutableArray*         m_notifications;
    int                     m_totalRecords;
    int                     m_numberOfPages;
}

@property (nonatomic, retain) NSString*             serverResponse;
@property (nonatomic, readonly) NSMutableArray*     notifications;
@property (nonatomic, readonly) int                 totalRecords;
@property (nonatomic, readonly) int                 numberOfPages;

- (void)parse;

@end
