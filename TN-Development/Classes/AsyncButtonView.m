//
//  AsyncButtonView.m
//  QNavigator
//
//  Created by softprodigy on 27/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AsyncButtonView.h"
#import<QuartzCore/QuartzCore.h>
#import "CategoriesViewController.h"
#import "Constants.h"
#import "UIImage+Scale.h"
#import "ImageCacheManager.h"
#import "MoreButton.h"
#import "PictureRequest.h"
#import "ImageLoaderManager.h"

@implementation AsyncButtonView
@synthesize m_target;
@synthesize m_btnIcon;
@synthesize isCatView;
@synthesize data;
@synthesize messageTag;
@synthesize urlString;
@synthesize connection;
@synthesize typeString;
@synthesize m_btnIconSelect;
@synthesize fetchingTimer, indicator, imagePresent, cropImage, delegate, maskImage;
SEL l_selector;

- (void)dealloc  {
	[connection cancel]; //in case the URL is still downloading
	//if(data)
    //	{
    //		data = nil;
    //		[data release];
    //    }
    // GDL: added these.
    // This was already released.
    //[m_target release];
    [m_btnIconSelect release];
    [m_btnIcon release];
    self.urlString = nil;
    [typeString release];
    self.data = nil;
    self.connection = nil;
    [indicator release];
    if (self.delegate) {
        [delegate release];
    }
    /*[self.fetchingTimer invalidate];
    self.fetchingTimer = nil;*/
    [super dealloc];
}

- (void)viewDidLoad
{
    self.urlString = nil;
    self.maskImage = YES;
}

- (void) setButtonData:(NSDictionary *)adata
{
    self.m_btnIcon.data = adata;
    if (self.cropImage) {
        self.m_btnIconSelect.data = adata;
    }
}

- (void)resetButton
{
    self.urlString = nil;
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.maskImage = YES;
}

- (void)loadImageFromURL:(NSURL*)url target:(id)target action:(SEL)selector btnText:(NSString *)btnText
{
    [self loadImageFromURL:url target:target action:selector btnText:btnText ignoreCaching:NO];
}

- (void)loadImageFromURL:(NSURL*)url target:(id)target action:(SEL)selector btnText:(NSString *)btnText ignoreCaching:(BOOL)ignore
{
    self.imagePresent = NO;
    
    NSRange picActionRange = [[url absoluteString] rangeOfString:@"?action="];
    NSString *actionName = @"noaction";
    if (picActionRange.length != 0) {
        NSString *actionName = [[url absoluteString] substringFromIndex:picActionRange.location + picActionRange.length];
        NSRange picActionNameRange = [actionName rangeOfString:@"&"];
        actionName = [actionName substringToIndex:picActionNameRange.location];
    }
    
    NSRange picid = [[url absoluteString] rangeOfString:@"picid="];
    NSString *prefix = @"";
    if (picid.length == 0) {
        picid = [[url absoluteString] rangeOfString:@"buzzimg="];
        if (picid.length == 0) {
            picid = [[url absoluteString] rangeOfString:@"advtid="];
            if (picid.length != 0)
                prefix = @"advtid";
        } else
            prefix = @"buzzimg";
    } else 
        prefix = @"picid";
    
    UIImage *image = nil;
    if (picid.length > 0) {
        int endOfStr = picid.location + picid.length;
        NSString *urlStr = [url absoluteString];
        urlStr = [urlStr substringWithRange:NSMakeRange(endOfStr, [urlStr length] - endOfStr)];
        
        NSMutableCharacterSet *set = [[[NSMutableCharacterSet alloc] init] autorelease];
        
        [set formUnionWithCharacterSet:[NSCharacterSet decimalDigitCharacterSet]];
        [set invert];
        picid = [urlStr rangeOfCharacterFromSet:set];
        if (picid.length != 0) {
            self.urlString = [NSString stringWithFormat:@"%@-%@-%@.jpg", actionName, prefix, [urlStr substringToIndex:picid.location]];
        } else {
            self.urlString = [NSString stringWithFormat:@"%@-%@-%@.jpg", actionName, prefix, urlStr];
        }
        
        if(!ignore)
            image = [[ImageCacheManager sharedManager] getImage:self.urlString];
    }
    
	m_target=target;
	l_selector=selector;
	   
    self.m_btnIcon = [[[MoreButton alloc] initWithFrame:self.bounds] autorelease];
    //	self.m_btnIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    //	[m_btnIcon setFrame:self.bounds];
    
    self.clipsToBounds = YES;
    self.m_btnIcon.clipsToBounds = YES;
    [m_btnIcon addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
	[self.m_btnIcon setTag:self.messageTag];
	[self addSubview:self.m_btnIcon];
    
    if (self.cropImage) {
        self.m_btnIconSelect = [[[MoreButton alloc] initWithFrame:self.bounds] autorelease];
        [m_btnIconSelect addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
        [self.m_btnIconSelect setTag:self.messageTag];
        [self.m_btnIconSelect setBackgroundImage:[UIImage imageNamed:@"image-selected-state"] forState:UIControlStateHighlighted];
        [self addSubview:self.m_btnIconSelect];
    }
    
    
    if (!image && [prefix length] > 0) {
//        [self setBackgroundColor:[UIColor colorWithWhite:0.80 alpha:0.25]];
//        self.layer.cornerRadius = 5.;
        
        self.indicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
        [self.indicator startAnimating];
        self.indicator.frame = CGRectMake((self.bounds.size.width - self.indicator.bounds.size.width)/2,
                                     (self.bounds.size.height - self.indicator.bounds.size.height)/2,
                                     self.indicator.bounds.size.width, self.indicator.bounds.size.height);
        [self addSubview:self.indicator];
        //add a label to display temporary text
        UILabel *tmp_label=[[UILabel alloc]initWithFrame:CGRectMake(2, self.frame.size.height-25, self.frame.size.width-2 , 20)]; //66, 20)];
        [tmp_label setTag:101010];
        
        if (self.frame.size.width <=50)
        {
            tmp_label.font=[UIFont fontWithName:kFontName size:12];
        }
        else 
        {
            tmp_label.font=[UIFont fontWithName:kFontName size:14];
        }
        
        tmp_label.textAlignment=UITextAlignmentCenter;
        tmp_label.backgroundColor=[UIColor clearColor];
        tmp_label.textColor=[UIColor darkGrayColor];
        tmp_label.text=btnText;	
        
        //[self.m_btnIcon addSubview:tmp_label];
        [tmp_label release];
        tmp_label=nil;	
    } else {
        [self setNewImage:image];
        return;
    }
	
	
#if 1	
    PictureRequest *request = [[[PictureRequest alloc] initWithPictureURL:url nameToSave:self.urlString ignoreCache:ignore]  autorelease];
    request.delegate = self;
    [[ImageLoaderManager sharedManager] addLoaderRequest:request];
    
#else
    self.fetchingTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(getImage:) userInfo:nil repeats:YES];
#endif
}

- (void) getImage:(NSTimer *)timer
{
    UIImage *image = [[ImageCacheManager sharedManager] getImage:self.urlString];
    
    if (image) {
        [self setNewImage:image];
        [self setBackgroundColor:[UIColor colorWithWhite:1. alpha:1.]];
        self.layer.cornerRadius = 0.;
        [indicator removeFromSuperview];
        [self.fetchingTimer invalidate];
    }
    
}

//Crash here

//the URL connection calls this repeatedly as data arrives
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	
	
	//if (data != nil) {
    //	[data release],data = nil;
	//			NSLog(@"DATA IS NOT NULL");
	//}
	if (self.data==nil) 
	{ 
		self.data = [[[NSMutableData alloc] init] autorelease];
	} 
	
	[self.data appendData:incrementalData];
}

- (void)cancelRequest {
    
    [self setBackgroundColor:[UIColor colorWithWhite:1. alpha:1.]];
    self.layer.cornerRadius = 0.;
    [self.indicator removeFromSuperview];
    self.data = nil;
    [connection cancel];
    
}

//the URL connection calls this once all the data has downloaded
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection 
{ 
    [self setBackgroundColor:[UIColor colorWithWhite:1. alpha:1.]];
    self.layer.cornerRadius = 0.;
    [self.indicator removeFromSuperview];
    
	UIImage *tempImage=[UIImage imageWithData:data];
	if (tempImage==nil)
	{
		tempImage=[UIImage imageNamed:@"no-image"];
	} else {
        [[ImageCacheManager sharedManager] savePicture:data forName:self.urlString];
    }
    
    [self setNewImage:tempImage];

	if (!isCatView)//[m_target isKindOfClass:[CategoriesViewController class]])
	{
		[m_btnIcon.layer setBorderWidth:2.0f];
		[m_btnIcon.layer setBorderColor:[[UIColor whiteColor]CGColor]];
	}
	//[[m_btnIcon viewWithTag:101010] removeFromSuperview];
    
	self.connection=nil;
	
	self.data=nil;
}

- (void) adjustBounds:(UIImage *)img
{
    CGSize imageSize = img.size;
    CGSize newSize = imageSize;
    CGRect iconBounds = self.bounds;
    CGRect cropRect = iconBounds;

    CGFloat IWF = imageSize.width/imageSize.height;
    CGFloat WWF = iconBounds.size.width/iconBounds.size.height;

    if (IWF>WWF) {
        newSize.height = iconBounds.size.height;
        newSize.width = round(newSize.height/imageSize.height * imageSize.width);
        cropRect.origin.x = round((newSize.width - iconBounds.size.width)/2);
    } else if (WWF>IWF) {
        newSize.width = iconBounds.size.width;
        newSize.height = round(newSize.width/imageSize.width * imageSize.height);
        cropRect.origin.y = round((newSize.height - iconBounds.size.height)/2);
        
    } else {
        imageSize = iconBounds.size;
        newSize = imageSize;
    }
   
    img = [img scaleToSize:newSize];
    img = [img crop:cropRect];
    
	[self.m_btnIcon setImage:img  forState:UIControlStateNormal];
}

- (void) setNewImage:(UIImage *)img
{
    self.imagePresent = YES;
	if (img==nil)
	{
		img=[UIImage imageNamed:@"no-image"];
	}
    
    if (self.maskImage) {
        [self adjustBounds:img];
        return;
    }
    
    CGSize imageSize = img.size;
    CGRect iconBounds = self.bounds;
    
    
    CGFloat ratio = imageSize.width/imageSize.height;
    
    if (ratio < 1.) {
        // height > width
        imageSize.width = iconBounds.size.width;
        imageSize.height = round(imageSize.width/ratio);
        img = [img scaleToSize:imageSize];
        
        if (self.cropImage && imageSize.height > iconBounds.size.height) {
            img = [img crop:iconBounds];
            [self.m_btnIcon setContentMode:UIViewContentModeTopLeft];
        } else
            [self.m_btnIcon setContentMode:UIViewContentModeScaleAspectFit];
    } else if (ratio > 1.) {
        // height < width
        if (self.cropImage) {
            imageSize.height = iconBounds.size.height;
            imageSize.width = round(imageSize.height*ratio);
        } else {
            imageSize.width = iconBounds.size.width;
            imageSize.height = round(imageSize.width/ratio);
        }
        img = [img scaleToSize:imageSize];
        
        if (self.cropImage && imageSize.width > iconBounds.size.width) {
            img = [img crop:iconBounds];
            [self.m_btnIcon setContentMode:UIViewContentModeTopLeft];
        } else
            [self.m_btnIcon setContentMode:UIViewContentModeScaleAspectFit];

    } else {
        imageSize.width = iconBounds.size.width;
        imageSize.height = round(imageSize.width/ratio);
        img = [img scaleToSize:imageSize];
        [self.m_btnIcon setContentMode:UIViewContentModeScaleAspectFit];
    }
    
    
	
	[self.m_btnIcon setImage:img  forState:UIControlStateNormal];
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error
{
    [self setBackgroundColor:[UIColor colorWithWhite:1. alpha:1.]];
    self.layer.cornerRadius = 0.;
    [self.indicator removeFromSuperview];
	[connection release];
	connection=nil;
    self.data = nil;
	
	[self.m_btnIcon setBackgroundImage:[UIImage imageNamed:@"no-image"]  forState:UIControlStateNormal];
    if (l_selector) {
        [self.m_btnIcon addTarget:m_target action:l_selector forControlEvents:UIControlEventTouchUpInside];
    }
    
	//because we dont want border in logos view
	if (!isCatView)//[m_target isKindOfClass:[CategoriesViewController class]]) 
	{
		[self.m_btnIcon.layer setBorderWidth:2.0f];
		[self.m_btnIcon.layer setBorderColor:[[UIColor whiteColor]CGColor]];
	}
	//[[self.m_btnIcon viewWithTag:101010] removeFromSuperview];
}

- (void) setNewImage:(UIImage *)img forName:(NSString *)name
{
    [self setBackgroundColor:[UIColor colorWithWhite:1. alpha:1.]];
    self.layer.cornerRadius = 0.;
    [self.indicator removeFromSuperview];
    if ([self.urlString isEqualToString:name]) {
        if (img==nil) {
            img=[UIImage imageNamed:@"no-image"];
        } else {
            [self setNewImage:img];
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(configureView)]) {
            [self.delegate performSelector:@selector(configureView)];
        }
    }
}

- (CGSize) getImageSize
{
    UIImage *img = [self.m_btnIcon imageForState:UIControlStateNormal];
    return img.size;
}

- (void) changeViewRect:(CGRect)newRect
{
    self.frame = newRect;
    
    CGRect buttonFrame = self.m_btnIcon.frame;
    buttonFrame.size = newRect.size;
    self.m_btnIcon.frame = buttonFrame;
}

/*
- (UIImage*) image
{
	for (UIView *tempView in [self subviews])
	{
		if ([tempView isKindOfClass:[UIButton class]])
		{
			return [(UIButton *)tempView imageForState:UIControlStateNormal];
		}
	}
	
	return nil;
}
*/
@end
