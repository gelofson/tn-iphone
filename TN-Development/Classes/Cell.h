//
//  Cell.h
//  naivegrid
//
//  Created by Apirom Na Nakorn on 3/6/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIGridViewCell.h"
#import "AsyncButtonView.h"

@interface Cell : UIGridViewCell {

}

@property (nonatomic, retain) IBOutlet AsyncButtonView *thumbnail;
@property (nonatomic, retain) IBOutlet UILabel *headline;

@end
