//
//  QNavigatorAppDelegate.h
//  QNavigator
//
//  Created by Soft Prodigy on 24/08/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <CoreLocation/CoreLocation.h>
#import "CustomTopBarView.h"
#import "Reachability.h"
#import "GetCurrentLocation.h"
#import "GetLocationAfterDistance.h"
#import "LoginHomeViewController.h"
#import "FBConnect.h"
#import "LoginViewController.h"
#import "PushNotificationViewController.h"

// JMC & Hockey App
#import "JMC.h"
#import "CNSHockeyManager.h"

// GDL: added
#import "MyCLController.h"

// Google Analytics
#import "GAI.h"

// Twitter Details
//#define kOAuthConsumerKey @"xMaR9o6lbbYAgaUReAeDmA"         // Account: QNavigator
//#define kOAuthConsumerSecret @"YjFDdfNC7xHajUBD8iXXTS9DH4mVLx6uL6NCeSx3OqY"     //REPLACE With Twitter App OAuth Secret YjFDdfNC7xHajUBD8iXXTS9DH4mVLx6uL6NCeSx3OqY
#define kOAuthConsumerKey @"z6KDsKQefUWFqGZVZAQihQ"         // Account: theshopbee
#define kOAuthConsumerSecret @"AbR8FS00QPtn71xifrg6K9d2YlIJXY9A7zLiL7w5tI"     

// Facebook Details
//#define _APP_ID @"207285035948301"
#define _APP_ID @"453204974697950"
#define _APP_KEY @"52850ab435dfe5464a2164e662e9618d"
#define _SECRET_KEY @"b97546c72f7826ace43b820647c5eb72"


// SFO, California
#define LATITUDE_IN_SIMULATOR (37.783947f)
#define LONGITUDE_IN_SIMULATOR (-122.407158f)

// Chandler, AZ 85225, USA 
//#define LATITUDE_IN_SIMULATOR (33.3)
//#define LONGITUDE_IN_SIMULATOR (-111.82)

// Phoenix, Arizona
//#define LATITUDE_IN_SIMULATOR (33.43)
//#define LONGITUDE_IN_SIMULATOR (-112.02)

// Bhubaneswar, India
//#define LATITUDE_IN_SIMULATOR (20.244364f)
//#define LONGITUDE_IN_SIMULATOR (85.817781f)
/*
#define SALES_TAB_INDEX 0
#define RUNWAY_TAB_INDEX 1
#define SHARE_TAB_INDEX 2
#define FIND_FRIENDS_TAB_INDEX 3
#define MY_PAGE_TAB_INDEX 4
*/
#define SALES_TAB_INDEX -1

enum {
    RUNWAY_TAB_INDEX,
    GoogleMap_TAB_INDEX,
    SHARE_TAB_INDEX,
    FIND_FRIENDS_TAB_INDEX,
    MY_PAGE_TAB_INDEX,
    SETTINGS_TAB_INDEX,
    NOTIFICATIONS_TAB_INDEX
};

@class  AddFriendsMypageViewController;
@class  ContentViewController, MainPageViewController;
// GDL: added MyCLControllerDelegate
@interface QNavigatorAppDelegate : NSObject <UIApplicationDelegate/*, UITabBarControllerDelegate*/,CLLocationManagerDelegate,MyCLControllerDelegate, FBSessionDelegate, CNSHockeyManagerDelegate> 
{
    UIWindow *window;
    //UITabBarController *tabBarController;
	int intSelectedCategory;
	int intTypeOfView;
    //Bharat: 11/14/2011: state for background check
	BOOL appBeingPushedToBackground;
    BOOL isLogged;
    NSMutableArray *navControllersArray;
	// new var to store tag for category icon pressed
	NSInteger m_tagIdentifier;
	NSMutableArray *m_arrTableData;
	
	CLLocationManager *m_locationManager;
	
	float m_geoLatitude;
	float m_geoLongitude;
	
	NSString *m_strDeviceId;
	
	Reachability *m_internetReach;
	int m_internetWorking;
	
	NSMutableArray *m_arrCategoryIcons;
	
	
	//-----Big sales Ads members-----
	NSMutableArray *m_arrBigSalesData;
	int m_intBigSalesIndex;
	//-------------------------------
	
	//-----Category Sales view members--
	NSMutableArray *m_arrCategorySalesData;
	int m_intCategorySalesIndex;
	NSString *m_strCategoryName;
	//----------------------------------
	
	//-----Indy shop view members------
	NSMutableArray *m_arrIndyShopData;
	int m_intIndyShopIndex;
	//---------------------------------
	
	//tracks the type of view displayed i.e 1: indy shop, 2: big sales ads, 3 onwards : category sales ads
	int m_intCurrentView;
	
	//tracks the index number for category sales views
	int m_intCategoryIndex;
	
	int m_intTotalIndyPages;
	int m_intTotalIndyRecords;
	int m_intIndyPageNumber;
	
	//NSString *m_strTempImagesPath;
	GetCurrentLocation *m_objGetCurrentLocation;
	
	GetLocationAfterDistance *m_objGetLocationDistance;
	
	BOOL isReloadingData;
	
	UIAlertView *m_reloadAlert;
	
	BOOL m_isDataReloadOnSessionExpire;
	//Facebook *facebook;
		
	//for facebook fbconnect..here we creating the object for the session
	Facebook	*m_session;
	NSString *str_forTwitter; 
	NSString *m_strDeviceToken;
	
	NSDictionary *userInfoDic;	
	
	NSInteger taggValue;               
	BOOL isUserLoggedInAfterLoggedOutState;
	LoginHomeViewController *m_loginHomeViewCtrl;
    PushNotificationViewController *notificationTray;
	
	LoginViewController *m_loginViewCtrl;
	
	NSString *m_checkForRegAndMypage;
	
	NSMutableArray		*m_arrBackupIndyShop;
	BOOL isMyPageShow; 
	BOOL isFittingRoomShown;
	BOOL isAddFriendLoadFromTab;
	
	id m_SelectedMainPageClassObj;
    
    AddFriendsMypageViewController *objAddFriendsMypageViewController;
    UINavigationController*         push_notifications;
    BOOL isLoginFacebook;
   
    UIImage *imageforchosecategory;
    NSTimer *heartBeatTimer;
    
}
@property(nonatomic,retain)UIImage *imageforchosecategory;
@property(retain) UINavigationController* push_notifications;
@property(nonatomic, retain) id<GAITracker> tracker;

@property(nonatomic,retain) NSString *m_strDeviceToken;
@property (nonatomic,retain) NSMutableArray		*m_arrBackupIndyShop;
@property (nonatomic,retain) LoginViewController *m_loginViewCtrl;
@property (nonatomic,retain) LoginHomeViewController *m_loginHomeViewCtrl;
@property(nonatomic,retain) id m_SelectedMainPageClassObj;
@property (strong, nonatomic) ContentViewController *contentViewController;
//@property (strong, nonatomic) MenuViewController *menuViewController;
@property (strong, nonatomic) UINavigationController *menuViewController;

@property (readonly, getter = serverUrl) NSString *serverUrl;
@property BOOL isUserLoggedInAfterLoggedOutState;
@property BOOL isAddFriendLoadFromTab;
@property (nonatomic, retain) NSMutableArray *navControllersArray;
//@property(nonatomic, retain) Facebook *facebook;

@property BOOL m_isDataReloadOnSessionExpire;
@property (nonatomic,retain) UIAlertView *m_reloadAlert;
@property BOOL isReloadingData;
@property int intSelectedCategory;
@property int intTypeOfView;
@property float m_geoLatitude;
@property float m_geoLongitude;
@property BOOL isMyPageShow;
@property 	BOOL isFittingRoomShown;
@property 	BOOL isLoginFacebook;
@property (nonatomic,retain) Reachability *m_internetReach;
@property(nonatomic,retain) NSTimer *heartBeatTimer;

@property int m_internetWorking;

@property (nonatomic,retain) NSString *m_strDeviceId;
@property (nonatomic,retain) 	NSArray *arr;

@property (nonatomic,retain) NSMutableArray *m_arrTableData;
@property (nonatomic,retain) CLLocationManager *m_locationManager;

@property (nonatomic, retain) IBOutlet UIWindow *window;
//@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;

@property (nonatomic,readwrite)	NSInteger m_tagIdentifier;
 @property (nonatomic,readwrite) NSInteger taggValue;
@property(nonatomic,retain)NSMutableArray *m_arrBigSalesData;
@property int m_intBigSalesIndex;

@property (nonatomic,retain) NSMutableArray *m_arrCategorySalesData;
@property (nonatomic,retain) NSString *m_strCategoryName;

@property int m_intCategorySalesIndex;

@property (nonatomic,retain) NSMutableArray *m_arrIndyShopData;
@property (nonatomic,retain) NSMutableArray *m_arrCategoryIcons;
@property (nonatomic,retain)NSString *str_forTwitter; 

//@property (nonatomic,retain) NSString *m_strTempImagesPath;

@property int m_intIndyShopIndex;
@property int m_intCurrentView;
@property int m_intCategoryIndex;
@property int m_intTotalIndyPages;
@property int m_intTotalIndyRecords;
@property int m_intIndyPageNumber;

@property (nonatomic,retain) GetCurrentLocation *m_objGetCurrentLocation;
@property (nonatomic,retain) GetLocationAfterDistance *m_objGetLocationDistance;

@property (nonatomic,retain) Facebook	*m_session;

@property(nonatomic,retain) NSDictionary *userInfoDic;	
@property(nonatomic,retain) NSString *m_checkForRegAndMypage;
@property(nonatomic,retain)AddFriendsMypageViewController *objAddFriendsMypageViewController;
@property (nonatomic, assign) BOOL appBeingPushedToBackground;
@property BOOL isLogged;

//-(void)tableArray;
-(void)CheckInternetConnection;
-(void)writeData:(NSArray *)arrAuditData;
-(void)failToUpdateLocation;
-(void)networkErrorInLocationUpdate;
-(void)unknownLocationInLocationUpdate;
-(void) dismissNotifications;
- (void)loginWithFB;
- (void)storeOAuthData;
-(void) removeOAuthData;
- (NSString *)applicationDocumentsDirectory; 
- (NSString *)applicationImagesDirectory; 
- (BOOL) runPingService;

-(NSArray *)readData;
-(void)showSideMenu;
-(void)hideSideMenu;

// Bharat: 11/10/2011: Create tab bar manually. So that we can recreate tab bar when logout happens, 
// thus the stacked views will be all fresh for the next logged in user. Why Apple guys did not think abt this!!
- (void) createTabBarController;
- (void) goToNewsPage;
- (void) goToPage:(NSInteger)index;
- (void) showNotifications;
- (void) logout;
- (BOOL) menuMode;

@end

#define tnApplication ((QNavigatorAppDelegate *)[UIApplication sharedApplication].delegate)

#define kServerUrl tnApplication.serverUrl
