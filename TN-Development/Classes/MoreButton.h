//
//  MoreButton.h
//  TinyNews
//
//  Created by Nava Carmon on 31/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreButton : UIButton
{
    NSDictionary *data;
}

@property (nonatomic, retain) NSDictionary *data;

@end
