//
//  BuzzButtonPhotoStoryFunctionality.h
//  TinyNews
//
//  Created by jay kumar on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuzzButtonPhotoStoryFunctionality : UIViewController<UIActionSheetDelegate>
{
    
    UITextField *m_whotext;
    UITextField *m_whattext;
    UITextField *m_whentext;
    UITextField *m_wheretext;
    UITextField *m_howtext;
    UITextField *m_whytext;
    UIImage *m_photo;
    NSString *m_headerString;
    NSString *m_strType;
    NSString *m_strMessage;
    UIScrollView *m_scrollView;
}

@property(nonatomic,retain)IBOutlet UITextField *m_whotext;
@property(nonatomic,retain)IBOutlet UITextField *m_whattext;
@property(nonatomic,retain)IBOutlet UITextField *m_whentext;
@property(nonatomic,retain)IBOutlet UITextField *m_wheretext;
@property(nonatomic,retain)IBOutlet UITextField *m_howtext;
@property(nonatomic,retain)IBOutlet UITextField *m_whytext;
@property(nonatomic,retain)UIImage *m_photo;
@property(nonatomic,retain)NSString *m_headerString;
@property(nonatomic,retain)NSString *m_strType;
@property(nonatomic,retain) NSString *m_strMessage;
@property(nonatomic,retain)IBOutlet UIScrollView *m_scrollView;

-(IBAction)clickToNext:(id)sender;
-(IBAction)clickToCancel:(id)sender;
-(IBAction)openPriceClick;
-(IBAction)skipBTNClick;
-(void) finallyPerformRealShare;

@end
