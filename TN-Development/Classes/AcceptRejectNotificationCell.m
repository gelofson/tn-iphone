//
//  AcceptRejectNotificationCell.m
//  QNavigator
//
//  Created by Nicolas Jakubowski on 10/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AcceptRejectNotificationCell.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"

@implementation AcceptRejectNotificationCell

@synthesize notification = m_notification;
@synthesize delegate = m_delegate;
@synthesize m_clockView;
@synthesize m_clockLabel;

- (void)setNotification:(Notification *)notification{
    if (!notification) {
        return;
    }
    
    @synchronized(self){
        
        [m_notification release];
        
        m_notification = [notification retain];
        
        [self setNeedsLayout];
        
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    if (!m_notification) {
        return;
    }
    
    if (!m_imageView) {
        AsyncButtonView* asyncButton = [[[AsyncButtonView alloc]
                                         initWithFrame:CGRectMake(3.5f,3.5f,48.0f,48.0f)] autorelease];
        asyncButton.maskImage = YES;
        
        [asyncButton loadImageFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kServerUrl,m_notification.thumbImageUrl]]
                               target:self 
                               action:@selector(actionProfileButton:) 
                              btnText:m_notification.firstName];
        
        [self.contentView  addSubview:asyncButton];
        
        m_imageView = asyncButton;
        
    }else{
        [m_imageView resetButton];
        m_imageView.maskImage = YES;
        [m_imageView cancelRequest];
        [m_imageView loadImageFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kServerUrl,m_notification.thumbImageUrl]] 
                                     target:self 
                                     action:@selector(actionProfileButton:) 
                                    btnText:m_notification.firstName];
    }
    
	if (!m_clockView) {
        m_clockView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"clock_icon.png"]];
        m_clockView.frame = CGRectMake(320.0f-50.0f,4,17,17);
		[self.contentView addSubview:m_clockView];
    }
    if (!m_clockLabel) {
        m_clockLabel = [[UILabel alloc] initWithFrame:CGRectMake(m_clockView.frame.origin.x+m_clockView.frame.size.width+2,m_clockView.frame.origin.y,300,12)];
		m_clockLabel.backgroundColor = [UIColor clearColor];
		m_clockLabel.font = [UIFont boldSystemFontOfSize:10.0];
    	UIColor* timeTextColor = [UIColor colorWithRed:126.0f/255.0f green:126.0f/255.0f blue:126.0f/255.0f alpha:1.0f];
		m_clockLabel.textColor = timeTextColor;
		m_clockLabel.textAlignment = UITextAlignmentLeft;
		[self.contentView addSubview:m_clockLabel];
    }
	NSString * str = [NSString stringWithString:m_notification.elapsedTime];
	if ([[str componentsSeparatedByString:@"our"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"our"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else if ([[str componentsSeparatedByString:@"in"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"in"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else if ([[str componentsSeparatedByString:@"ay"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"ay"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else if ([[str componentsSeparatedByString:@"moments"] count] > 1) {
		m_clockLabel.text = @"1m";
	} else if ([[str componentsSeparatedByString:@"eek"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"eek"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else if ([[str componentsSeparatedByString:@"onth"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"onth"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else if ([[str componentsSeparatedByString:@"ear"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"ear"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else {
		m_clockLabel.text = str;
	}
    
    
    if (!m_nameLabel) {
        m_nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(55.0f, 0.0f, 120.0f, 18.0f)];
        m_nameLabel.font = [UIFont boldSystemFontOfSize:17.0f];
        //Bharat: 11/18/11: Set text color for name on notification page
		//m_nameLabel.textColor = [UIColor colorWithRed:0.3843f green:0.5608f blue:0.6000 alpha:1.0f];
        m_nameLabel.textColor = [UIColor colorWithRed:0.28f green:0.28f blue:0.28f alpha:1.0];
        m_nameLabel.backgroundColor = [UIColor clearColor];
        m_nameLabel.lineBreakMode = UILineBreakModeTailTruncation;
        [self.contentView addSubview:m_nameLabel];
    }
    m_nameLabel.text = [m_notification.firstName capitalizedString];
	[m_nameLabel sizeToFit];
    
    if (!m_wouldLikeToAddYouAsFriend) {
        //m_wouldLikeToAddYouAsFriend = [[UILabel alloc] initWithFrame:CGRectMake(55.0f, 27.0f, 120.0f, 22.0f)];
        m_wouldLikeToAddYouAsFriend = [[UILabel alloc] initWithFrame:CGRectMake(55,m_nameLabel.frame.origin.y+m_nameLabel.frame.size.height,120,30)];
        m_wouldLikeToAddYouAsFriend.backgroundColor = [UIColor clearColor];
        m_wouldLikeToAddYouAsFriend.textColor = [UIColor colorWithRed:0.3647f green:0.3647f blue:0.3647f alpha:1.0f];
        //m_wouldLikeToAddYouAsFriend.font = [UIFont boldSystemFontOfSize:8.0f];
        m_wouldLikeToAddYouAsFriend.font = [UIFont systemFontOfSize:12.0f];
        m_wouldLikeToAddYouAsFriend.lineBreakMode = UILineBreakModeWordWrap;
        [self.contentView addSubview:m_wouldLikeToAddYouAsFriend];
		m_wouldLikeToAddYouAsFriend.numberOfLines = 2;
        m_wouldLikeToAddYouAsFriend.text = @"would like to add you as a friend.";
    }
    
    if (!m_acceptButton) {
        m_acceptButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [m_acceptButton setImage:[UIImage imageNamed:@"accept_btn.png"] forState:UIControlStateNormal];
        [m_acceptButton addTarget:self action:@selector(actionAccept) forControlEvents:UIControlEventTouchUpInside];
        [m_acceptButton setFrame:CGRectMake(255.0f, 25.0f, 62.0f, 22.0f)];
        [self.contentView addSubview:m_acceptButton];
        
    }
    
    if (!m_rejectButton) {
        m_rejectButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        [m_rejectButton setImage:[UIImage imageNamed:@"reject_btn.png"] forState:UIControlStateNormal];
        [m_rejectButton addTarget:self action:@selector(actionDeny) forControlEvents:UIControlEventTouchUpInside];
        [m_rejectButton setFrame:CGRectMake(200.0f, 25.0f, 62.0f, 22.0f)];
        [self.contentView addSubview:m_rejectButton];
    }
    
}

- (void)actionAccept{
    if (m_delegate) {
        [m_delegate acceptInvitationFromNotification:m_notification];
    }
}

- (void)actionDeny{
    if (m_delegate) {
        [m_delegate denyInvitationFromNotification:m_notification];
    }
}

- (void)actionProfileButton:(id)sender{
    if (m_delegate) {
        [m_delegate selectedProfileWithEmail:m_notification.email withName:m_notification.firstName];
    }
}

- (void)dealloc{
    
    if (m_notification) {
        [m_notification release];
        m_notification = nil;
    }
    
    if (m_nameLabel) {
        [m_nameLabel release];
        m_nameLabel = nil;
    }
    
    if (m_imageView) {
        [m_imageView release];
        m_imageView = nil;
    }
    
    if (m_acceptButton) {
        [m_acceptButton release];
        m_acceptButton = nil;
    }
    
    if (m_rejectButton) {
        [m_rejectButton release];
        m_rejectButton = nil;
    }
    
    if (m_wouldLikeToAddYouAsFriend) {
        [m_wouldLikeToAddYouAsFriend release];
        m_wouldLikeToAddYouAsFriend = nil;
    }

    if (m_clockLabel) {
        [m_clockLabel release];
        m_clockLabel = nil;
    }
    
    if (m_clockView) {
        [m_clockView release];
        m_clockView = nil;
    }
    
    
    if (m_delegate) {
        [m_delegate release];
        m_delegate = nil;
    }
    
    [super dealloc];
}




- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
