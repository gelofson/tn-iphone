//
//  FriendsListDisplay.m
//  QNavigator
//
//  Created by softprodigy on 21/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FriendsListDisplay.h"
#import <QuartzCore/QuartzCore.h>
#import"Constants.h"
#import"QNavigatorAppDelegate.h"
#import"ViewAFriendViewController.h"
#import"ShopbeeAPIs.h"
#import"JSON.h"
#import"AsyncImageView.h"
#import"LoadingIndicatorView.h"
#import "NewsDataManager.h"

@implementation FriendsListDisplay

@synthesize m_addFriendButton;
@synthesize m_searchBar;
@synthesize m_friendsArray;
@synthesize m_table;
@synthesize m_image;
@synthesize m_backup;

QNavigatorAppDelegate *l_appDelegate;

ShopbeeAPIs *l_request;

LoadingIndicatorView *l_indicatorView;

NSInteger LevelTrack;
#pragma mark custom methods

-(IBAction)m_goToBackView; // for navigating to the back view
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)m_clickedOnAddFriendButton
{
	l_appDelegate.isAddFriendLoadFromTab = NO;
	m_AddFriend=[[AddFriendsMypageViewController alloc] initWithNibName:@"AddFriendsMypageViewController" bundle:[NSBundle mainBundle]];
	[self.navigationController pushViewController:m_AddFriend animated:YES];
	
}	
-(void)load_tableViewDataSource{
    self.m_backup = [[[NSMutableArray  alloc] initWithArray:m_friendsArray] autorelease];
	
    NSLog(@"backup =%@",m_backup);
}
-(IBAction)AcceptFriendRequest:(id)sender
{
	l_request=[[ShopbeeAPIs alloc] init];
	int AcceptButtonTag=[sender tag];
	NSString *acceptString=@"Y";
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_UserId=[prefs valueForKey:@"userName"];
	NSString *tmp_FriendId=[[self.m_backup objectAtIndex:AcceptButtonTag] valueForKey:@"email"];
	[l_indicatorView startLoadingView:self];
	[l_request answerFriendRequest:@selector(requestCallBackMethodForAcceptOrReject:responseData:) tempTarget:self userid:tmp_UserId friendid:tmp_FriendId acceptString:acceptString];
	
	[l_request release];
	l_request=nil;
}
-(IBAction)RejectFriendRequest:(id)sender
{
	l_request=[[ShopbeeAPIs alloc] init];
	int RejectButtonTag=[sender tag];
	NSString *acceptString=@"N";
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_UserId=[prefs valueForKey:@"userName"];
	NSString *tmp_FriendId=[[self.m_backup objectAtIndex:RejectButtonTag] valueForKey:@"email"];
	[l_indicatorView startLoadingView:self];
	[l_request answerFriendRequest:@selector(requestCallBackMethodForAcceptOrReject:responseData:) tempTarget:self userid:tmp_UserId friendid:tmp_FriendId acceptString:acceptString];
	
	[l_request release];
	l_request=nil;
}
-(IBAction)WithdrawFriendReuqest:(id)sender
{
	l_request=[[ShopbeeAPIs alloc] init];
	int PendingButtonTag=[sender tag];
	NSString *acceptString=@"N";
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_UserId=[prefs valueForKey:@"userName"];
	NSString *tmp_FriendId=[[self.m_backup objectAtIndex:PendingButtonTag] valueForKey:@"email"];
	[l_indicatorView startLoadingView:self];
	[l_request answerFriendRequest:@selector(requestCallBackMethodForAcceptOrReject:responseData:) tempTarget:self userid:tmp_FriendId friendid:tmp_UserId acceptString:acceptString];
	
	[l_request release];
	l_request=nil;
}
#pragma mark -
#pragma mark Searchbar Delegates 


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar 
{
	self.m_backup= [[[NSMutableArray alloc] initWithArray:m_friendsArray] autorelease];
 	[m_searchBar resignFirstResponder];
	NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"firstname CONTAINS[cd] %@",m_searchBar.text];
	NSArray *filteredNameList = [self.m_backup filteredArrayUsingPredicate:bPredicate];
	self.m_backup= [[[NSMutableArray alloc] initWithArray:filteredNameList] autorelease];
	[self.m_table reloadData];
	// If no result,, show 'Not Found' image
    [self showHideTable:self.m_backup];
	[m_searchBar setShowsCancelButton:NO animated:YES];

}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
	[m_searchBar resignFirstResponder];
	[m_searchBar setShowsCancelButton:NO animated:YES];
	m_searchBar.text = @"";
    [self load_tableViewDataSource];
	[self.m_table reloadData];
    [self showHideTable:m_friendsArray];	
}

- (void) showHideTable:(NSArray *)array
{
	if ([array count] == 0) {
		self.m_table.hidden = YES;
		[feedBackPage setImage:[UIImage imageNamed:@"exception_alone"]];
		feedBackPage.hidden = NO;
        m_addFriendButton.hidden = NO;
	} else {
		self.m_table.hidden = NO;
		feedBackPage.hidden = YES;
        m_addFriendButton.hidden = YES;
	}
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	if(m_searchBar.text.length==0)
	{
		[self performSelector:@selector(load_tableViewDataSource)];
		
		[self.m_table reloadData];
		
		[m_searchBar resignFirstResponder];
		
        [self showHideTable:m_friendsArray];	
	}
	[m_searchBar setShowsCancelButton:YES animated:YES];
	
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
	[m_searchBar setShowsCancelButton:YES animated:YES];
	
	return YES;
	
}

#pragma mark tableview delegates
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.m_backup count];
	
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellidentifier=@"cell";
	UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
	
	
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellidentifier] autorelease];
		
		NSLog(@"index is---%@",[m_friendsArray objectAtIndex:indexPath.row]);
		//cell.backgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_with_arrow_in_add_friend.png"]];	
		cell.selectionStyle=UITableViewCellSelectionStyleNone;
       	UILabel *temp_lblFriendName=[[UILabel alloc]init];
		temp_lblFriendName.tag=10;
		temp_lblFriendName.frame=CGRectMake(60,12,165,25); 
		temp_lblFriendName.font=[UIFont fontWithName:kMyriadProBoldFont size:18];
		//temp_lblFriendName.text=  [NSString stringWithFormat:@"%@ %@",
//								  [[m_backup objectAtIndex:indexPath.row] valueForKey:@"firstname"],
//								  [[m_backup objectAtIndex:indexPath.row] valueForKey:@"lastname"]];
		temp_lblFriendName.textColor=[UIColor blackColor];
		temp_lblFriendName.lineBreakMode = UILineBreakModeTailTruncation;
		AsyncImageView* asyncImage = [[[AsyncImageView alloc]
								   initWithFrame:CGRectMake(0,2,55,55)] autorelease];
		asyncImage.tag = 999;
		[cell.contentView addSubview:asyncImage];
		[cell.contentView addSubview:temp_lblFriendName];
		[temp_lblFriendName release];
		
		temp_lblFriendName=nil;
        
//        UIImageView *arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_friends"]];
//        arrowView.frame = CGRectMake(cell.contentView.bounds.size.width - 23, (cell.contentView.bounds.size.height - 21)/2, 20, 21);
//        
//		[cell.contentView addSubview:arrowView];
//        [arrowView release];
        cell.backgroundColor = [UIColor whiteColor];
	}
    if (!cell.accessoryView)
        cell.accessoryView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_friends"]] autorelease];
	
	if (![[[self.m_backup objectAtIndex:indexPath.row] valueForKey:@"firstname"] isEqualToString:@""])
	{
		
	((UILabel *)[cell.contentView viewWithTag:10]).text=[NSString stringWithFormat:@"%@ %@",
														   [[self.m_backup objectAtIndex:indexPath.row] valueForKey:@"firstname"],
														   [[self.m_backup objectAtIndex:indexPath.row] valueForKey:@"lastname"]];
	}
	else 
	{
	((UILabel *)[cell.contentView viewWithTag:10]).text=@"No Name";	
	}

	for(UIView *temp in cell.contentView.subviews)
	{
		if([temp isKindOfClass:[UIButton class]])
			[temp removeFromSuperview];
			
	}
	for(UIView *temp in cell.contentView.subviews)
	{
		if(temp.tag==10000 )
			[temp removeFromSuperview];
		
	}
	for(UIView *temp in cell.contentView.subviews)
	{
		if(temp.tag==10001 )
			[temp removeFromSuperview];
		
	}
	
	if ([[[self.m_backup objectAtIndex:indexPath.row] valueForKey:@"requestType"] isEqualToString:@"acceptORreject"])
	{
		
		UIButton *tmp_acceptButton=[[UIButton alloc] initWithFrame:CGRectMake(197, 15, 62, 22)];
		[tmp_acceptButton setImage:[UIImage imageNamed:@"btn_accept.png"] forState:UIControlStateNormal];
		[cell.contentView addSubview:tmp_acceptButton];
		[tmp_acceptButton addTarget:self action:@selector(AcceptFriendRequest:) forControlEvents:UIControlEventTouchUpInside];
		tmp_acceptButton.tag=indexPath.row;
		[tmp_acceptButton release];
		tmp_acceptButton=nil;
		
		UIButton *tmp_RejectButton=[[UIButton alloc] initWithFrame:CGRectMake(252, 15, 61, 22)];
		[tmp_RejectButton setImage:[UIImage imageNamed:@"btn_reject.png"] forState:UIControlStateNormal];
		[cell.contentView addSubview:tmp_RejectButton];
		[tmp_RejectButton addTarget:self action:@selector(RejectFriendRequest:) forControlEvents:UIControlEventTouchUpInside];
		tmp_RejectButton.tag=indexPath.row;
		[tmp_RejectButton release];
		tmp_RejectButton=nil;
		
		UILabel *tmp_addFrndlabel=[[UILabel alloc] initWithFrame:CGRectMake(60, 33,120, 10)];
		tmp_addFrndlabel.text=@"would like to add you ";
		tmp_addFrndlabel.numberOfLines=2;
		tmp_addFrndlabel.textColor=[UIColor darkGrayColor];
		tmp_addFrndlabel.backgroundColor=[UIColor clearColor];
		tmp_addFrndlabel.tag=10001;
		tmp_addFrndlabel.font=[UIFont fontWithName:kFontName size:10];
		[cell.contentView addSubview:tmp_addFrndlabel];
		[tmp_addFrndlabel release];
		tmp_addFrndlabel=nil;
		
		UILabel *tmp_addFrndlabeladd=[[UILabel alloc] initWithFrame:CGRectMake(60, 43,100 , 10)];
		tmp_addFrndlabeladd.text=@"as a friend.";
		tmp_addFrndlabeladd.numberOfLines=2;
		tmp_addFrndlabeladd.textColor=[UIColor darkGrayColor];
		tmp_addFrndlabeladd.backgroundColor=[UIColor clearColor];
		tmp_addFrndlabeladd.tag=10000;
		tmp_addFrndlabeladd.font=[UIFont fontWithName:kFontName size:10];
		[cell.contentView addSubview:tmp_addFrndlabeladd];
		[tmp_addFrndlabeladd release];
		tmp_addFrndlabeladd=nil;
        
        cell.accessoryView = nil;
	}
	if ([[[self.m_backup objectAtIndex:indexPath.row] valueForKey:@"requestType"] isEqualToString:@"pending"])
		{			
			UIButton *tmp_PendingButton=[[UIButton alloc] initWithFrame:CGRectMake(320 - 93 - 10, 9, 93, 36)];
			[tmp_PendingButton setImage:[UIImage imageNamed:@"requestpending_btn.png"] forState:UIControlStateNormal];
			tmp_PendingButton.userInteractionEnabled=YES;
			[cell.contentView addSubview:tmp_PendingButton];
			[tmp_PendingButton addTarget:self action:@selector(WithdrawFriendReuqest:) forControlEvents:UIControlEventTouchUpInside];
			tmp_PendingButton.tag=indexPath.row;
			[tmp_PendingButton release];
			tmp_PendingButton=nil;
            cell.accessoryView = nil;
		}
		NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[self.m_backup objectAtIndex:indexPath.row] valueForKey:@"thumbImageUrl"]];
		NSURL *tempLoadingUrl = [NSURL URLWithString:temp_strUrl];
		AsyncImageView* asyncImage = (AsyncImageView*)[cell.contentView viewWithTag:999];
		//((UIImageView *)[asyncImage viewWithTag:101]).image=[UIImage imageNamed:@"loading.png"];
		[asyncImage loadImageFromURL:tempLoadingUrl];
		return cell;	
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	ViewAFriendViewController *tmp_obj=[[ViewAFriendViewController alloc] init];//initWithNibName:@"ViewAFriendViewController" bundle:[NSBundle mainBundle]];
	tmp_obj.m_strLbl1 = [[self.m_backup objectAtIndex:indexPath.row]valueForKey:@"firstname"];
	tmp_obj.m_Email=[[self.m_backup objectAtIndex:indexPath.row]valueForKey:@"email"];
	tmp_obj.m_RequestTypeString=[[self.m_backup objectAtIndex:indexPath.row] valueForKey:@"requestType"];
	[tmp_obj.m_strLbl1 retain];
	tmp_obj.m_personImage=m_image;
	tmp_obj.LevelTrackFriendsView=LevelTrack;
    tmp_obj.messageLayout = NO;
	[self.navigationController pushViewController:tmp_obj animated:YES];
	[tmp_obj release];
	tmp_obj=nil;
}
#pragma mark -
#pragma mark callback methods
#pragma mark -
#pragma mark callback methods
-(void)requestCallBackMethodForAcceptOrReject:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	if ([responseCode intValue]==200)
	{
		l_request=[[ShopbeeAPIs alloc] init];
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
		[l_request getFriendsList:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:customerID loginId:customerID];
		NSLog(@"data downloaded");
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSLog(@"response string: %@",tempString);
		[tempString release];
		tempString=nil;
		[l_request release];
		l_request=nil;
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
		
	}
	
}		
-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_indicatorView stopLoadingView];
	
	if ([responseCode intValue]==200) 
	{
		//[m_backup removeAllObjects];
		NSLog(@"data downloaded");
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSLog(@"response string: %@",tempString);
        
        NSArray *friendsArray = [[tempString JSONValue] retain];
        NSSortDescriptor *sortDescriptor1 = [[[NSSortDescriptor alloc] initWithKey:@"requestType"
                                                                         ascending:NO] autorelease];
        NSSortDescriptor *sortDescriptor2 = [[[NSSortDescriptor alloc] initWithKey:@"firstname"
                                                                         ascending:YES selector:@selector(caseInsensitiveCompare:)] autorelease];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1, sortDescriptor2, nil];
        
        self.m_backup = [[NSMutableArray alloc] initWithArray:[friendsArray sortedArrayUsingDescriptors:sortDescriptors]];
        
        
		//m_backup=[[tempString JSONValue] retain];
        NSLog(@"after backup =%@",self.m_backup);
		[m_table reloadData];
		[l_indicatorView stopLoadingView];
		[tempString release];
        [friendsArray release];
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
}

-(void)requestCallBackMethodForFriendsList:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_indicatorView stopLoadingView];
	
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	
	if ([responseCode intValue]==200) 
	{	
        NSArray *friendsArray = [tempString JSONValue];
        NSSortDescriptor *sortDescriptor1 = [[[NSSortDescriptor alloc] initWithKey:@"requestType"
                                                      ascending:NO] autorelease];
        NSSortDescriptor *sortDescriptor2 = [[[NSSortDescriptor alloc] initWithKey:@"firstname"
                                                                        ascending:YES selector:@selector(caseInsensitiveCompare:)] autorelease];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1, sortDescriptor2, nil];
        
        self.m_friendsArray = [friendsArray sortedArrayUsingDescriptors:sortDescriptors];
		[self load_tableViewDataSource];
        
		[l_indicatorView stopLoadingView];
		[m_table reloadData];
		CGRect r;
		if ([m_friendsArray count] == 0) {
			self.m_table.hidden = YES;
			self.m_searchBar.hidden = YES;
			[feedBackPage setImage:[UIImage imageNamed:@"exception_alone"]];
			feedBackPage.hidden = NO;
            m_addFriendButton.hidden = NO;

		} else {
			self.m_table.hidden = NO;
			self.m_searchBar.hidden = NO;
			feedBackPage.hidden = YES;
            m_addFriendButton.hidden = YES;
		}
		
		
	}
	
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	}
	else if([responseCode intValue]!=-1)
	{
		[l_indicatorView stopLoadingView];
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
}

#pragma mark -

// Implement viewDidLoad to do additional setup after loa ding the view, typically from a nib.
-(void)viewWillAppear:(BOOL)animated{
	
	[l_indicatorView startLoadingView:self];
	l_request=[[ShopbeeAPIs alloc] init];
	//m_friendsbuttonclicked=YES;
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	[l_request getFriendsList:@selector(requestCallBackMethodForFriendsList:responseData:) tempTarget:self customerid:customerID loginId:customerID];
	[l_request release];
	l_request=nil;

	
}
-(void)viewDidAppear:(BOOL)animated
{
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
//	[[m_addFriendButton layer] setCornerRadius:8.0f];
//	[[m_addFriendButton layer] setMasksToBounds:YES];
	l_indicatorView=[LoadingIndicatorView SharedInstance];
		
	LevelTrack=0;
	

	/* search bar background setting */
	//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
	UITextField *searchField;
	NSUInteger numViews = [m_searchBar.subviews count];
	for(int i = 0; i < numViews; i++) 
	{
		
		[[m_searchBar.subviews objectAtIndex:0] setHidden:YES];
		
		if([[m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
		{
			searchField = [m_searchBar.subviews objectAtIndex:i];
		}
	}
	
	if(!(searchField == nil)) 
	{
		[searchField setBackground: [UIImage imageNamed:@"gray_searchbox@2x.png"] ];
		[searchField setBorderStyle:UITextBorderStyleNone];
	}
	/***********************************/
	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


// GDL: Modified everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release retained IBOutlets.
    self.m_addFriendButton = nil;
    self.m_searchBar = nil;
    self.m_table = nil;
	[feedBackPage release];
    
    // Is this created in viewDidLoad?
	[m_AddFriend release];
	m_AddFriend = nil;
}


- (void)dealloc {
    [m_searchBar release];
    [m_AddFriend release];
	[m_addFriendButton release];
    [m_friendsArray release];
    [m_backup release];
    [m_image release];
    [m_name release];
    [m_personName release];
    [m_table release];
	[feedBackPage release];
    
    [super dealloc];
}


@end
