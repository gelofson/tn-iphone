//
//  BigSalesAdsViewController.m
//  QNavigator
//
//  Created by Soft Prodigy on 27/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BigSalesAdsViewController.h"
#import "MoreInfoViewController.h"
#include <QuartzCore/QuartzCore.h>
#import "CustomTopBarView.h"
#import "CategoriesViewController.h"
#import "IndyShopViewController.h"
#import "Constants.h"
#import "SBJSON.h"
#import "BigSalesModelClass.h"
#import "ATIconDownloader.h"
#import "AuditingClass.h"
#import "LoadingIndicatorView.h"
#import "noSalePageViewController.h"

// GDL
#import "UISearchBar+ContentInset.h"

@implementation BigSalesAdsViewController

@synthesize btnMoreInfo;
@synthesize m_imgViewSaleAd;
@synthesize m_mutResponseData;
@synthesize m_activityIndicator;
@synthesize m_intTypeOfResponse;
@synthesize m_imgViewCurl;
@synthesize m_isConnectionFailed;
@synthesize m_isViewDidLoadCalled;

@synthesize m_isViewTwiceLoaded;

@synthesize l_isConnectionActive;
@synthesize l_theConnection;

CGPoint l_touchLocation;
QNavigatorAppDelegate *l_appDelegate;


NSString *temp_strCity;
NSString *temp_strState;
NSString *temp_strZip;
NSString *temp_strAdd2;



int l_bigSalesId;

// GDL: For reloading the category ads every 10 minutes.
- (void)reloadTimerFired:(NSTimer*)theTimer {
    static NSDate *lastReloadDate = nil;
    
    if ( lastReloadDate == nil || [lastReloadDate timeIntervalSinceNow] < - 9.5 * 60 ) {
        [self  sendRequestForBigSalesAds];
        [lastReloadDate release];
        lastReloadDate = [[NSDate alloc] init];
    }
}
 


 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {

        
		m_isViewDidLoadCalled=FALSE;
		// Custom initialization
    }
    return self;
}


#pragma mark -
#pragma mark UIView Methods

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad  {
    [super viewDidLoad];
	
	isReload = NO;
	
	if (m_isViewDidLoadCalled == NO) {
	
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
		l_bigSalesId=-1;
	
		m_mutResponseData = [[NSMutableData alloc] init];
		
		m_isViewDidLoadCalled = TRUE;
		
	}
	else {
		NSLog(@"VIEW TWICE CALLED.");
		m_isViewTwiceLoaded=TRUE;
	}
    
	// GDL: Set up a timer that fires every minute to reload the category data.
    [reloadTimer invalidate];
    [reloadTimer release];
	reloadTimer = [[NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(reloadTimerFired:) userInfo:nil repeats:YES] retain];
    
//#ifdef TEST_FLIGHT_ENGAGED
//	[TestFlight passCheckpoint:[NSString stringWithFormat:@"%@:BigSalesAdsViewController:viewDidLoad: completed", 
//		[[Context getInstance] getAppVersion]]];
//#endif

}

-(void)viewWillAppear:(BOOL)animated
{
	l_appDelegate.m_SelectedMainPageClassObj=self;
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	if(l_appDelegate.m_arrBigSalesData.count==0)
	{
		[self sendRequestForBigSalesAds];
			
	}
	else if(m_isViewTwiceLoaded==TRUE) 
	{
		if(l_appDelegate.m_arrBigSalesData.count >= l_appDelegate.m_intBigSalesIndex && l_appDelegate.m_intBigSalesIndex != -1)
		{
			BigSalesModelClass  *temp_objBigSales=[l_appDelegate.m_arrBigSalesData objectAtIndex:l_appDelegate.m_intBigSalesIndex];
			[m_imgViewSaleAd setImage:temp_objBigSales.m_imgAd];
			btnMoreInfo.hidden=NO;
			m_imgViewCurl.hidden=NO;
			
		}
	}
	
	l_appDelegate.m_intCurrentView=2; //for big sales ads view. used on street map view
	
//	if(![l_appDelegate.m_customView superview])
//	{
//		l_appDelegate.m_customView.categoriesDelegate=self;
//		[l_appDelegate addCategoryButtons];
//	}
//	
//	l_appDelegate.m_customView.hidden=NO;
//	l_appDelegate.m_customView.userInteractionEnabled=YES;
//	[l_appDelegate.m_customView selectCategory:2];
//	
//	m_isViewTwiceLoaded=FALSE;
//    if ([l_appDelegate.m_customView.m_arrCategories count]<=1) {
//        noSalePageViewController *nosale=[[noSalePageViewController alloc] init];
//        [self.navigationController pushViewController:nosale animated:YES];
//        [nosale release];
//    }
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark -
#pragma mark Custom Methods

-(void)btnMoreInfoAction:(id)sender
{
	if (l_appDelegate.m_intBigSalesIndex!=-1)
	{
		
		if(l_appDelegate.m_arrBigSalesData.count > 0 && l_appDelegate.m_intBigSalesIndex >= 0)
		{
			BigSalesModelClass  *temp_objBigSales=[l_appDelegate.m_arrBigSalesData objectAtIndex:l_appDelegate.m_intBigSalesIndex];
		
			//-----------------------------------------------------------------------------------
			AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"More info button pressed",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objBigSales.m_strId,@"productId",nil];
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"More info button pressed",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objBigSales.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
				//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
			}
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			
			//-----------------------------------------------------------------------------------
		}
		
		MoreInfoViewController *temp_moreInfoViewCtrl=[[MoreInfoViewController alloc]init];
		[self.navigationController pushViewController:temp_moreInfoViewCtrl animated:YES];
		
		[CATransaction begin];
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionFromRight;
		animation.duration = 0.3;
		//animation.delegate=self;
		[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
//		[[l_appDelegate.m_customView layer] addAnimation:animation forKey:@"Slide"];
//		[l_appDelegate.m_customView setHidden:YES];
		[CATransaction commit];
		
		[temp_moreInfoViewCtrl release];
		temp_moreInfoViewCtrl=nil;
	}

}

/*method to load required class in the memory and display on screen
 i.e. Indy shops, Big sales view, or category sales view*/

-(void)loadRequiredViewController:(int)typeOfViewController catType:(int)catType
{
	BOOL isViewCtrlExists;
	
	if (typeOfViewController==1) //1: Indy shop view, 2: Big Sales view, 3: other Categories view
	{
		isViewCtrlExists=FALSE;
		
		self.tabBarController.selectedIndex=0;
		
		//check if Indy shop view controller already exists
		for(UIViewController *temp_viewCtrl in self.navigationController.viewControllers)
		{
			if ([temp_viewCtrl isKindOfClass:[IndyShopViewController class]])
			{
				isViewCtrlExists=TRUE;
				
				[((IndyShopViewController *)temp_viewCtrl).m_tableView setHidden:NO];
				[((IndyShopViewController *)temp_viewCtrl).m_exceptionPage setHidden:YES];
				((IndyShopViewController *)temp_viewCtrl).isCurrentIndyShopList=YES;
				[((IndyShopViewController *)temp_viewCtrl).m_tblSearchResults setHidden:YES];
				// Bharat: 11/8/11: Showing search bar icon within 100 pixels at the extreme right of the frame (width) 
				//[((IndyShopViewController *)temp_viewCtrl).m_searchBar setContentInset:UIEdgeInsetsMake(5,220,5,5)];
				[((IndyShopViewController *)temp_viewCtrl).m_searchBar setContentInset:
					UIEdgeInsetsMake(5,((IndyShopViewController *)temp_viewCtrl).m_searchBar.frame.size.width-100,5,5)];
				
				if (l_appDelegate.m_arrBackupIndyShop)
				{
					l_appDelegate.m_arrIndyShopData=[NSMutableArray arrayWithArray:l_appDelegate.m_arrBackupIndyShop];
				}
				
				/* search bar background setting */
				//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
				UITextField *searchField=nil;
				NSUInteger numViews = [((IndyShopViewController *)temp_viewCtrl).m_searchBar.subviews count];
				for(int i = 0; i < numViews; i++) 
				{
					if([[((IndyShopViewController *)temp_viewCtrl).m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
					{
						searchField = [((IndyShopViewController *)temp_viewCtrl).m_searchBar.subviews objectAtIndex:i];
					}
				}
				
				if(!(searchField == nil)) 
				{
					[searchField setBackground: [UIImage imageNamed:@"search_bar_small.png"] ];
					[searchField setBorderStyle:UITextBorderStyleNone];
				}
				/***********************************/
				
				[((IndyShopViewController *)temp_viewCtrl).m_searchBar setText:@""];
				
				[((IndyShopViewController *)temp_viewCtrl).m_lblCaption setHidden:NO];
				[((IndyShopViewController *)temp_viewCtrl).m_searchBar resignFirstResponder];
				[((IndyShopViewController *)temp_viewCtrl).m_searchBar setShowsCancelButton:NO];
				[((IndyShopViewController *) temp_viewCtrl).m_tableView reloadData];
				
				
				
				if(l_appDelegate.m_arrIndyShopData.count==0)
				{
					ATIconDownloader *iconDownloader = [[[ATIconDownloader alloc] init] autorelease];
					BOOL temp_response=[iconDownloader closeAllActiveConnections];
					NSLog(@"cancel image download req 1: %d",temp_response);
					
					[(IndyShopViewController *)temp_viewCtrl sendRequestForIndyShopData:1];
				}
				
				[self.navigationController popToViewController:temp_viewCtrl animated:NO];
				
				break;
			}
		}
		
		if (isViewCtrlExists==FALSE)
		{
			IndyShopViewController *temp_indyShopViewCtrl=[[IndyShopViewController alloc]init];
			
			[temp_indyShopViewCtrl.m_tableView setHidden:NO];
			[temp_indyShopViewCtrl.m_exceptionPage setHidden:YES];
			temp_indyShopViewCtrl.isCurrentIndyShopList=YES;
			[temp_indyShopViewCtrl.m_tblSearchResults setHidden:YES];
			
			if (l_appDelegate.m_arrBackupIndyShop)
			{
				l_appDelegate.m_arrIndyShopData=[NSMutableArray arrayWithArray:l_appDelegate.m_arrBackupIndyShop];
			}
			
			// Bharat: 11/8/11: Showing search bar icon within 100 pixels at the extreme right of the frame (width) 
			//[temp_indyShopViewCtrl.m_searchBar setContentInset:UIEdgeInsetsMake(5,220,5,5)];
			[temp_indyShopViewCtrl.m_searchBar setContentInset:UIEdgeInsetsMake(5,temp_indyShopViewCtrl.m_searchBar.frame.size.width-100,5,5)];
			
			/* search bar background setting */
			//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
			UITextField *searchField=nil;
			
			NSUInteger numViews = [temp_indyShopViewCtrl.m_searchBar.subviews count];
			for(int i = 0; i < numViews; i++) 
			{
				if([[temp_indyShopViewCtrl.m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
				{
					searchField = [temp_indyShopViewCtrl.m_searchBar.subviews objectAtIndex:i];
				}
			}
			
			if(!(searchField == nil)) 
			{
				[searchField setBackground: [UIImage imageNamed:@"search_bar_small.png"] ];
				[searchField setBorderStyle:UITextBorderStyleNone];
			}
			/***********************************/
			
			[temp_indyShopViewCtrl.m_searchBar setText:@""];
			
			[temp_indyShopViewCtrl.m_lblCaption setHidden:NO];
			[temp_indyShopViewCtrl.m_searchBar resignFirstResponder];
			[temp_indyShopViewCtrl.m_searchBar setShowsCancelButton:NO];
			[temp_indyShopViewCtrl.m_tableView reloadData];
			
			
			
			if(l_appDelegate.m_arrIndyShopData.count==0)
			{
				ATIconDownloader *iconDownloader = [[[ATIconDownloader alloc] init] autorelease];
				BOOL temp_response=[iconDownloader closeAllActiveConnections];
				NSLog(@"cancel image download req 2: %d",temp_response);
				
				[temp_indyShopViewCtrl sendRequestForIndyShopData:1];
			}
			
			[self.navigationController pushViewController:temp_indyShopViewCtrl animated:NO];
			[temp_indyShopViewCtrl release];
			temp_indyShopViewCtrl=nil;
		}
				
	}
	else if(typeOfViewController==2)//Big sales view
	{
		self.tabBarController.selectedIndex=0;
		//isViewCtrlExists=FALSE;
		//check if Big Sales Ads view controller already exists		
		if (isReload)
		{
			l_appDelegate.m_intBigSalesIndex = -1;
			[l_appDelegate.m_arrBigSalesData removeAllObjects];
			//[self sendRequestForBigSalesAds];
		}
		

		[self.navigationController popToRootViewControllerAnimated:NO];
			
		
	}
	else if(typeOfViewController==3)//category sales view
	{
		isViewCtrlExists=FALSE;
		//[l_appDelegate tableArray];
		self.tabBarController.selectedIndex=0;
//		[l_appDelegate.m_customView selectCategory:catType];
		//check if Categories view controller already exists
		
		[[LoadingIndicatorView SharedInstance] stopLoadingView];
		
		for(UIViewController *temp_viewCtrl in self.navigationController.viewControllers)
		{
			if ([temp_viewCtrl isKindOfClass:[CategoriesViewController class]])
			{
				isViewCtrlExists=TRUE;
								
				//[((CategoriesViewController *)temp_viewCtrl).m_tableView reloadData];
				
				if(catType==3)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kShoesSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kShoesSlogan];
				}
				else if(catType==4)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kBagsSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kBagsSlogan];
				}
				else if(catType==5)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kApparelSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kApparelSlogan];
				}
				else if(catType==6)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kAccessoriesSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kAccessoriesSlogan];
				}
				else if(catType==7)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kHealthBeautySlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kHealthBeautySlogan];
				}
				else if(catType==8)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kSportsSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kSportsSlogan];
				}
				else if(catType==9)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kElectronicsSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kElectronicsSlogan];
				}
				else if(catType==10)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kHomeFashionsSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kHomeFashionsSlogan];
				}
				else if(catType==11)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kMoviesSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kMoviesSlogan];
					
				}
				else if(catType==12)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kDiningSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kDiningSlogan];
					
				}
				else if(catType==13)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kKidsSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kKidsSlogan];
					
				}
				else if(catType==14)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kMensSlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kMensSlogan];
					
				}
				else if(catType==15)
				{
					[(CategoriesViewController *)temp_viewCtrl m_lblCaption].text=kCharitySlogan;
					[(CategoriesViewController *)temp_viewCtrl setM_strCaption:kCharitySlogan];
					
				}
				
				[((CategoriesViewController *)temp_viewCtrl).m_tableView setHidden:YES];
				[((CategoriesViewController *)temp_viewCtrl).m_exceptionPage setHidden:YES];
				 ((CategoriesViewController *)temp_viewCtrl).isCurrentListingView=NO;
				// Bharat : 12/17/2011: hide logos
				//[((CategoriesViewController *)temp_viewCtrl).m_logosScrollView setHidden:NO];
				[((CategoriesViewController *)temp_viewCtrl).m_logosScrollView setHidden:YES];
				// Bharat: 11/8/11: Showing search bar icon within 100 pixels at the extreme right of the frame (width) 
				//[((CategoriesViewController *)temp_viewCtrl).m_searchBar setContentInset:UIEdgeInsetsMake(5,220,5,5)];
				[((CategoriesViewController *)temp_viewCtrl).m_searchBar setContentInset:UIEdgeInsetsMake(5,((CategoriesViewController *)temp_viewCtrl).m_searchBar.frame.size.width-100,5,5)];
				
				/* search bar background setting */
				//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
				UITextField *searchField=nil;
				NSUInteger numViews = [((CategoriesViewController *)temp_viewCtrl).m_searchBar.subviews count];
				for(int i = 0; i < numViews; i++) 
				{
					if([[((CategoriesViewController *)temp_viewCtrl).m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
					{
						searchField = [((CategoriesViewController *)temp_viewCtrl).m_searchBar.subviews objectAtIndex:i];
					}
				}
				
				if(!(searchField == nil)) 
				{
					[searchField setBackground: [UIImage imageNamed:@"search_bar_small.png"] ];
					[searchField setBorderStyle:UITextBorderStyleNone];
				}
				/***********************************/
				
				[((CategoriesViewController *)temp_viewCtrl).m_searchBar setText:@""];
				
				[((CategoriesViewController *)temp_viewCtrl).m_lblCaption setHidden:NO];
				[((CategoriesViewController *)temp_viewCtrl).m_searchBar resignFirstResponder];
				[((CategoriesViewController *)temp_viewCtrl).m_searchBar setShowsCancelButton:NO];
				
				ATIconDownloader *iconDownloader = [[[ATIconDownloader alloc] init] autorelease];
				BOOL temp_response=[iconDownloader closeAllActiveConnections];
				NSLog(@"cancel image download req 3: %d",temp_response);
				
				[(CategoriesViewController *)temp_viewCtrl checkIfDataAvailable];
				[self.navigationController popToViewController:temp_viewCtrl animated:NO];
				break;
			}
		}
		if (isViewCtrlExists==FALSE)
		{
			CategoriesViewController *temp_categoriesViewCtrl=[[CategoriesViewController alloc]initWithNibName:@"CategoriesViewController" bundle:[NSBundle mainBundle]];
						
			if(catType==3)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kShoesSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kShoesSlogan];
			}
			else if(catType==4)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kBagsSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kBagsSlogan];
			}
			else if(catType==5)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kApparelSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kApparelSlogan];
			}
			else if(catType==6)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kAccessoriesSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kAccessoriesSlogan];
			}
			else if(catType==7)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kHealthBeautySlogan;
				[temp_categoriesViewCtrl setM_strCaption:kHealthBeautySlogan];
			}
			else if(catType==8)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kSportsSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kSportsSlogan];
			}
			else if(catType==9)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kElectronicsSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kElectronicsSlogan];
			}
			else if(catType==10)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kHomeFashionsSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kHomeFashionsSlogan];
			}
			else if(catType==11)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kMoviesSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kMoviesSlogan];
				
			}
			else if(catType==12)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kDiningSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kDiningSlogan];
				
			}
			else if(catType==13)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kKidsSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kKidsSlogan];
				
			}
			else if(catType==14)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kMensSlogan;
				[temp_categoriesViewCtrl setM_strCaption:kMensSlogan];
				
			}
			else if(catType==15)
			{
				[temp_categoriesViewCtrl m_lblCaption].text=kCharitySlogan;
				[temp_categoriesViewCtrl setM_strCaption:kCharitySlogan];
				
			}
			
			[temp_categoriesViewCtrl.m_tableView setHidden:YES];
			[temp_categoriesViewCtrl.m_exceptionPage setHidden:YES];
			temp_categoriesViewCtrl.isCurrentListingView=NO;
			[temp_categoriesViewCtrl.m_logosScrollView setHidden:NO];
			
			// Bharat: 11/8/11: Showing search bar icon within 100 pixels at the extreme right of the frame (width) 
			//[temp_categoriesViewCtrl.m_searchBar setContentInset:UIEdgeInsetsMake(5,220,5,5)];
			[temp_categoriesViewCtrl.m_searchBar setContentInset:UIEdgeInsetsMake(5,temp_categoriesViewCtrl.m_searchBar.frame.size.width-100,5,5)];
			
			/* search bar background setting */
			//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
			UITextField *searchField=nil;
			
			NSUInteger numViews = [temp_categoriesViewCtrl.m_searchBar.subviews count];
			for(int i = 0; i < numViews; i++) 
			{
				if([[temp_categoriesViewCtrl.m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
				{
					searchField = [temp_categoriesViewCtrl.m_searchBar.subviews objectAtIndex:i];
				}
			}
			
			if(!(searchField == nil)) 
			{
				[searchField setBackground: [UIImage imageNamed:@"search_bar_small.png"] ];
				[searchField setBorderStyle:UITextBorderStyleNone];
			}
			/***********************************/
			
			[temp_categoriesViewCtrl.m_searchBar setText:@""];
			
			[temp_categoriesViewCtrl.m_lblCaption setHidden:NO];
			[temp_categoriesViewCtrl.m_searchBar resignFirstResponder];
			[temp_categoriesViewCtrl.m_searchBar setShowsCancelButton:NO];
			
			
						
			ATIconDownloader *iconDownloader = [[[ATIconDownloader alloc] init] autorelease];
			BOOL temp_response=[iconDownloader closeAllActiveConnections];
			NSLog(@"%d",temp_response);
			
			[temp_categoriesViewCtrl checkIfDataAvailable];
			[self.navigationController pushViewController:temp_categoriesViewCtrl animated:NO];
			
			[temp_categoriesViewCtrl release];
			temp_categoriesViewCtrl=nil;
		}
			
	}
}

/* Method to switch between logos view (for category sales) and 
	listing view (when search feature is used)*/
-(void)switchLogosListingView
{
	for(UIViewController *temp_viewCtrl in self.navigationController.viewControllers)
	{
		if ([temp_viewCtrl isKindOfClass:[CategoriesViewController class]])
		{
			
			[(CategoriesViewController *)temp_viewCtrl checkIfDataAvailable];
			
			[((CategoriesViewController *)temp_viewCtrl).m_tableView setHidden:YES];
			[((CategoriesViewController *)temp_viewCtrl).m_exceptionPage setHidden:YES];
			((CategoriesViewController *)temp_viewCtrl).isCurrentListingView=NO;
			[((CategoriesViewController *)temp_viewCtrl).m_logosScrollView setHidden:NO];
			
			// Bharat: 11/8/11: Showing search bar icon within 100 pixels at the extreme right of the frame (width) 
			//[((CategoriesViewController *)temp_viewCtrl).m_searchBar setContentInset:UIEdgeInsetsMake(5,220,5,5)];
			[((CategoriesViewController *)temp_viewCtrl).m_searchBar setContentInset:
					UIEdgeInsetsMake(5,((CategoriesViewController *)temp_viewCtrl).m_searchBar.frame.size.width-100,5,5)];
			
			/* search bar background setting */
			//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
			UITextField *searchField=nil;
			NSUInteger numViews = [((CategoriesViewController *)temp_viewCtrl).m_searchBar.subviews count];
			for(int i = 0; i < numViews; i++) 
			{
				if([[((CategoriesViewController *)temp_viewCtrl).m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
				{
					searchField = [((CategoriesViewController *)temp_viewCtrl).m_searchBar.subviews objectAtIndex:i];
				}
			}
			
			if(!(searchField == nil)) 
			{
				[searchField setBackground: [UIImage imageNamed:@"search_bar_small.png"] ];
				[searchField setBorderStyle:UITextBorderStyleNone];
			}
			/***********************************/
			
			[((CategoriesViewController *)temp_viewCtrl).m_searchBar setText:@""];
			
			[((CategoriesViewController *)temp_viewCtrl).m_lblCaption setHidden:NO];
			[((CategoriesViewController *)temp_viewCtrl).m_searchBar resignFirstResponder];
			[((CategoriesViewController *)temp_viewCtrl).m_searchBar setShowsCancelButton:NO];
			
			
		}
	}
}

/* Method to switch between indy shop view  and 
 search listing */
-(void)switchIndyShopListingView
{
	for(UIViewController *temp_viewCtrl in self.navigationController.viewControllers)
	{
		if ([temp_viewCtrl isKindOfClass:[IndyShopViewController	class]])
		{
			
			//[(CategoriesViewController *)temp_viewCtrl checkIfDataAvailable];
			
			[((IndyShopViewController *)temp_viewCtrl).m_tableView setHidden:NO];
			[((IndyShopViewController *)temp_viewCtrl).m_exceptionPage setHidden:YES];
			((IndyShopViewController *)temp_viewCtrl).isCurrentIndyShopList =YES;
			[((IndyShopViewController *)temp_viewCtrl).m_tblSearchResults setHidden:YES];
			
			if (l_appDelegate.m_arrBackupIndyShop)
			{
				l_appDelegate.m_arrIndyShopData=[NSMutableArray arrayWithArray:l_appDelegate.m_arrBackupIndyShop];
			}
			
			// Bharat: 11/8/11: Showing search bar icon within 100 pixels at the extreme right of the frame (width) 
			//[((IndyShopViewController *)temp_viewCtrl).m_searchBar setContentInset:UIEdgeInsetsMake(5,220,5,5)];
			[((IndyShopViewController *)temp_viewCtrl).m_searchBar setContentInset:
				UIEdgeInsetsMake(5,((IndyShopViewController *)temp_viewCtrl).m_searchBar.frame.size.width-100,5,5)];
			
			/* search bar background setting */
			//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
			UITextField *searchField=nil;
			NSUInteger numViews = [((IndyShopViewController *)temp_viewCtrl).m_searchBar.subviews count];
			for(int i = 0; i < numViews; i++) 
			{
				if([[((IndyShopViewController *)temp_viewCtrl).m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
				{
					searchField = [((IndyShopViewController *)temp_viewCtrl).m_searchBar.subviews objectAtIndex:i];
				}
			}
			
			if(!(searchField == nil)) 
			{
				[searchField setBackground: [UIImage imageNamed:@"search_bar_small.png"] ];
				[searchField setBorderStyle:UITextBorderStyleNone];
			}
			/***********************************/
			
			[((IndyShopViewController *)temp_viewCtrl).m_searchBar setText:@""];
			
			[((IndyShopViewController *)temp_viewCtrl).m_lblCaption setHidden:NO];
			[((IndyShopViewController *)temp_viewCtrl).m_searchBar resignFirstResponder];
			[((IndyShopViewController *)temp_viewCtrl).m_searchBar setShowsCancelButton:NO];
			[((IndyShopViewController *)temp_viewCtrl).m_tableView reloadData];
			
			
		}
	}
}


//Method called from CustomTopBarView.h class on tap of category button icons
-(void)categoriesButtonAction:(id)sender
{
	NSLog(@"cat button pressed");
	
	if([sender tag]==1)
	{
		l_appDelegate.m_intCategoryIndex=[sender tag];
		[self loadRequiredViewController:1 catType:[sender tag]];//load indy shop view
		
	}
	else if([sender tag]==2)
	{
		l_appDelegate.m_intCategoryIndex=[sender tag];
		[self loadRequiredViewController:2 catType:[sender tag]];// load big sales view
	}
	else if([sender tag] > 2)
	{
		l_appDelegate.m_intCategoryIndex=[sender tag];
		[self loadRequiredViewController:3 catType:[sender tag]];//load categories view
	}
}

#pragma mark -
#pragma mark Memory Management Methods

// GDL: Changed the following two methods

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // GDL: Cancel the reloadData timer.
    [reloadTimer invalidate];
    [reloadTimer release];
    reloadTimer = nil;
    
    // Release retained IBOutlets.
    self.btnMoreInfo = nil;
    self.m_imgViewSaleAd = nil;
    self.m_activityIndicator = nil;
    self.m_imgViewCurl = nil;
}

- (void)dealloc {
    // GDL: Cancel the reloadData timer.
    [reloadTimer invalidate];
    [reloadTimer release];
    reloadTimer = nil;
    
    [btnMoreInfo release];
    [m_imgViewSaleAd release];
    [m_mutResponseData release];
    [m_activityIndicator release];
    [m_imgViewCurl release];
    
    // I expect this was already released.
    //[l_theConnection release];

    [super dealloc];
}

#pragma mark -
#pragma mark UITouch Methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event];
	UITouch *touch = [[event allTouches] anyObject];
	l_touchLocation = [touch previousLocationInView:m_imgViewSaleAd];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [[event allTouches] anyObject];
	CGPoint touchPoint=[touch locationInView:m_imgViewSaleAd];
	
	//if user slides the finger from left to right (top left corner), previous ad will be displayed
	if(CGRectContainsPoint(CGRectMake(0,0,70,100),l_touchLocation))//250,260 ,70,50), l_touchLocation))
	{
		if((touchPoint.x - l_touchLocation.x) > 40)
		{
			//NSLog(@"%d",l_appDelegate.m_intBigSalesIndex);
			
			if(l_appDelegate.m_arrBigSalesData.count > 0)
			{
				//Bharat: 11/22/11: Check if abt to reach beginning, then reset to end
				if (l_appDelegate.m_intBigSalesIndex > 0)
					l_appDelegate.m_intBigSalesIndex=l_appDelegate.m_intBigSalesIndex -1;
				else
					l_appDelegate.m_intBigSalesIndex=l_appDelegate.m_arrBigSalesData.count -1;
				
				BigSalesModelClass  *temp_objBigSales=[l_appDelegate.m_arrBigSalesData objectAtIndex:l_appDelegate.m_intBigSalesIndex];
				
				//-------------------------- Manages auditing data ----------------------------------
				AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
				[temp_objAudit initializeMembers];
				NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
				[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
				
				if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
				{
					//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Flip back for previous Ad",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objBigSales.m_strId,@"productId",nil];
					NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
											 @"Flip back for previous Ad",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objBigSales.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
					
					[temp_objAudit.m_arrAuditData addObject:temp_dict];
					//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
				}
				if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
				{
					[temp_objAudit sendRequestToSubmitAuditData];
				}
				
				//-----------------------------------------------------------------------------------
				
				[UIView beginAnimations:@"previousAd" context:nil];
				[UIView setAnimationDuration:0.4f];
				//[UIView setAnimationDelegate:self];
				//[UIView setAnimationCurve:UIViewAnimationOptionCurveEaseInOut];
				[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
				m_imgViewSaleAd.image= temp_objBigSales.m_imgAd;//[UIImage imageNamed:[NSString stringWithFormat:@"Ad%d.png",l_appDelegate.m_intBigSalesIndex]];
				[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:m_imgViewSaleAd cache:YES];
				[UIView commitAnimations];
				
				m_imgViewCurl.hidden=NO;
				
				if(m_isConnectionFailed==TRUE)
				{
					//m_isConnectionFailed=FALSE;
					[self restartImageLoadingOnNetworkFailure];
				}
				

			}
		}
	}
	//if user slides the finger from right to left, next ad will be displayed
	else if(CGRectContainsPoint(CGRectMake(250,150 ,70,160), l_touchLocation))
	{
		if((l_touchLocation.x-touchPoint.x) > 40)
		{
			//NSLog(@"%d",l_appDelegate.m_intBigSalesIndex);
			
			if(l_appDelegate.m_arrBigSalesData.count > 0) 
			{
				//Bharat: 11/22/11: Check if abt to reach end, then reset to 0
				if (l_appDelegate.m_intBigSalesIndex >= (l_appDelegate.m_arrBigSalesData.count -1))
					l_appDelegate.m_intBigSalesIndex = 0;
				else
					l_appDelegate.m_intBigSalesIndex=l_appDelegate.m_intBigSalesIndex+1;
						
				BigSalesModelClass *temp_objBigSales=[l_appDelegate.m_arrBigSalesData objectAtIndex:l_appDelegate.m_intBigSalesIndex];
				
				if (temp_objBigSales.m_imgAd!=nil) 
				{
					
					//------------------------------------------------------------
					AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
					[temp_objAudit initializeMembers];
					NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
					[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
										
					if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
					{
						//NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:@"Flip for next Ad",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objBigSales.m_strId,@"productId",nil];
						NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
												 @"Flip for next Ad",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",temp_objBigSales.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
						
						[temp_objAudit.m_arrAuditData addObject:temp_dict];
						//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
					}
					
					if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
					{
						[temp_objAudit sendRequestToSubmitAuditData];
					}
					//------------------------------------------------------------
					
					[UIView beginAnimations:@"nextAd" context:nil];
					[UIView setAnimationDuration:0.4f];
					
					//[UIView setAnimationDelegate:self];
					//[UIView setAnimationCurve:UIViewAnimationOptionCurveEaseInOut];
				
					m_imgViewSaleAd.image=temp_objBigSales.m_imgAd; //[UIImage imageNamed:[NSString stringWithFormat:@"Ad%d.png",l_appDelegate.m_intBigSalesIndex]];
				
					[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:m_imgViewSaleAd cache:YES];
					[UIView commitAnimations];
					

					//Bharat: 11/22/11: Do not hide the curl button any more
					/*
					if(l_appDelegate.m_intBigSalesIndex == l_appDelegate.m_arrBigSalesData.count -1)
						m_imgViewCurl.hidden=YES;
					*/
				}
				else {
					
					if(m_isConnectionFailed==TRUE)
					{
						//m_isConnectionFailed=FALSE;
						[self restartImageLoadingOnNetworkFailure];
					}
					
					l_appDelegate.m_intBigSalesIndex=l_appDelegate.m_intBigSalesIndex -1;
					UIAlertView *temp_alert=[[UIAlertView alloc]initWithTitle:@"Please wait.." message:@"Image downloading is in progress." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[temp_alert show];
					[temp_alert release];
					temp_alert=nil;
				}
	
			}
		}
		
	}	
	
}

//called in case network fails to load data
// i.e. to reload data on network failure
-(void)restartImageLoadingOnNetworkFailure
{
	for (int i=0;i<l_appDelegate.m_arrBigSalesData.count;i++)
	{
		BigSalesModelClass *temp_objBigSales;
		temp_objBigSales=[l_appDelegate.m_arrBigSalesData objectAtIndex:i];
		
		if(temp_objBigSales.m_imgAd ==nil)
		{
			l_bigSalesId=[temp_objBigSales.m_strId intValue];
			
			[self sendRequestToLoadImages:temp_objBigSales.m_strAdUrl];	
			break;
		}
	}
}

#pragma mark -
#pragma mark Web Service methods

//send the request to webservice to get big sales ads
-(void)sendRequestForBigSalesAds
{
	NSLog(@"BigSalesAdsViewController:sendRequestForBigSalesAds:");
	[l_appDelegate CheckInternetConnection];
	btnMoreInfo.hidden=YES;
	m_imgViewCurl.hidden=YES;
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		m_intTypeOfResponse=0; 
        
        // Bharat: 11/27/11: DE22 fixes
		//self.m_imgViewSaleAd.image=[UIImage imageNamed:@"loadingImage.jpg"];
        self.m_imgViewSaleAd.image=[UIImage imageNamed:@"exception_searching for sales"];

			
		
		self.view.userInteractionEnabled=FALSE;
		[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getBigSaleInfo&did=%@&lng=%f&lat=%f&pagenum=%d&num=%d&dist=%d",kServerUrl,l_appDelegate.m_strDeviceId,l_appDelegate.m_geoLongitude,l_appDelegate.m_geoLatitude,1,20,50];
		
					
		[temp_url replaceOccurrencesOfString:@"(null)" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [temp_url length])];
		
		NSLog(@"BigSalesAdsViewController:sendRequestForBigSalesAds:url=[%@]", temp_url);
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
	//	NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"text/xml; charset=utf-8", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_url dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if(l_isConnectionActive)
			[l_theConnection cancel];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		l_isConnectionActive=TRUE;
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		self.view.userInteractionEnabled=TRUE;
		l_isConnectionActive=FALSE;
		
		[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
}

//downloads the big sales ads 
-(void)sendRequestToLoadImages:(NSString *)imageUrl
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		m_intTypeOfResponse=1; 
		m_isConnectionFailed=FALSE;
		//self.view.userInteractionEnabled=FALSE;
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:25.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"text/xml; charset=utf-8", @"Content-Type", nil];
		
		
		//[theRequest setHTTPBody:[imageUrl dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if(l_isConnectionActive)
			[l_theConnection cancel];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		l_isConnectionActive=TRUE;
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		l_isConnectionActive=FALSE;
		m_isConnectionFailed=TRUE;
		m_intTypeOfResponse=-1;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
}


#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSLog(@"BigSalesAdsViewController:didReceiveResponse:");
	[m_mutResponseData setLength:0];
	
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	int temp_responseCode= [httpResponse statusCode];
	NSLog(@"%d",temp_responseCode);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	//NSLog(@"BigSalesAdsViewController:didReceiveData:");
	[m_mutResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	l_isConnectionActive = FALSE;
	
	NSLog(@"BigSalesAdsViewController:connectionDidFinishLoading:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
	
	if ( m_mutResponseData != nil && m_intTypeOfResponse == 0 ) {
        // typeofresponse=0: response is in json, to get data
		//NSLog(@"type of response=0 JSON=[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string = [[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
		//NSLog(@"BigSalesAdsViewController:connectionDidFinishLoading:m_intTypeOfResponse=0: [%@]",temp_string);
				
		SBJSON *temp_objSBJson = [[SBJSON alloc]init];
		NSArray *temp_arrResponse = [[NSArray alloc]initWithArray:[temp_objSBJson objectWithString:temp_string]];
		
		
		NSLog(@"BigSalesAdsViewController:connectionDidFinishLoading:EntriesCount=%d",[temp_arrResponse count]);
		
		BigSalesModelClass *temp_objBigSales;
		
		for ( int i = 0; i < [temp_arrResponse count]; i++ ) {
			NSDictionary *temp_dictAd=[temp_arrResponse objectAtIndex:i];
			
			//NSLog(@"Object[%d]=[%@]",i,temp_dictAd);
			
			//NSLog(@"location=[%@]",[[temp_dictAd objectForKey:@"location"] objectForKey:@"latitude"]);
			
			temp_objBigSales = [[BigSalesModelClass alloc]init];
			temp_objBigSales.m_strLatitude = [[temp_dictAd objectForKey:@"location"] objectForKey:@"latitude"];
			temp_objBigSales.m_strLongitude = [[temp_dictAd objectForKey:@"location"] objectForKey:@"longitude"];
			temp_objBigSales.m_strMallImageUrl = [NSString stringWithFormat:@"%@%@",kServerUrl,[[temp_dictAd objectForKey:@"location"] objectForKey:@"mallMapImageUrl"]];
			
			//NSLog(@"%@",[NSString stringWithFormat:@"%@%@",kServerUrl,[[temp_dictAd objectForKey:@"location"] objectForKey:@"mallMapImageUrl"]]);
			
			//[[temp_dictAd objectForKey:@"location"] objectForKey:@"mallMapImageUrl"];
			
			temp_strAdd2 = [[temp_dictAd objectForKey:@"location"] objectForKey:@"address2"];
			
			if ( temp_strAdd2.length > 0 )
				temp_objBigSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",[[temp_dictAd objectForKey:@"location"] objectForKey:@"address1"],temp_strAdd2];
			else
				temp_objBigSales.m_strAddress=[NSString stringWithFormat:@"%@",[[temp_dictAd objectForKey:@"location"] objectForKey:@"address1"]];
			
			temp_strCity = [[temp_dictAd objectForKey:@"location"] objectForKey:@"city"];
			temp_strState = [[temp_dictAd objectForKey:@"location"] objectForKey:@"state"];
			temp_strZip = [[temp_dictAd objectForKey:@"location"] objectForKey:@"zip"];
			
			if ( temp_strCity.length > 0 )
				temp_objBigSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objBigSales.m_strAddress,temp_strCity];
			
			if ( temp_strState.length > 0 )
				temp_objBigSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objBigSales.m_strAddress,temp_strState];
			
			if ( temp_strZip.length > 0 )
				temp_objBigSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objBigSales.m_strAddress,temp_strZip];
			
			
			temp_objBigSales.m_strDescription1 = [[temp_dictAd objectForKey:@"product"] objectForKey:@"description"];
			temp_objBigSales.m_strAdTitle = [[temp_dictAd objectForKey:@"product"] objectForKey:@"productTagLine"];
			temp_objBigSales.m_strDescription2 = [[temp_dictAd objectForKey:@"product"] objectForKey:@"termsAndConditions"];
			//NSLog(@"ClassName=[%@]",NSStringFromClass([[[temp_dictAd objectForKey:@"product"] objectForKey:@"id"] class]));
            if ([NSStringFromClass([[[temp_dictAd objectForKey:@"product"] objectForKey:@"id"] class]) caseInsensitiveCompare:@"NSDecimalNumber"] == NSOrderedSame) {
                temp_objBigSales.m_strId = [[[temp_dictAd objectForKey:@"product"] objectForKey:@"id"] stringValue];
            } else {
                temp_objBigSales.m_strId = [[temp_dictAd objectForKey:@"product"] objectForKey:@"id"];
            }
			
			NSArray *temp_arrImgAdUrls=[[temp_dictAd objectForKey:@"product"] objectForKey:@"imageUrls"];
			
			NSString *temp_str = @"";
			
				if ( temp_arrImgAdUrls.count > 0 ) {
					temp_str = [NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:0]];
					temp_str = [temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
					//NSLog(@"%@",temp_str);
					
				}
				temp_objBigSales.m_strAdUrl=temp_str;
				
				//Bharat: 11/22/11: Check if ad already exists, if yes, then update data (image, details might have changed)
				int alreadyExistingIndex=-1;
				for (int kk=0; kk < [l_appDelegate.m_arrBigSalesData count]; kk++) {
					BigSalesModelClass * aObj = [l_appDelegate.m_arrBigSalesData objectAtIndex:kk];
					if (    (aObj != nil) && 
							(aObj.m_strId != nil) && 
							([aObj.m_strId caseInsensitiveCompare:temp_objBigSales.m_strId] == NSOrderedSame)) {
						alreadyExistingIndex = kk;
						break;
					}
				}

				if (alreadyExistingIndex >= 0) {
					[l_appDelegate.m_arrBigSalesData replaceObjectAtIndex:alreadyExistingIndex withObject:temp_objBigSales];
				} else {
					[l_appDelegate.m_arrBigSalesData addObject:temp_objBigSales];
				}
		//	[temp_objBigSales release];
//			temp_objBigSales=nil;
		}
		
		[temp_objSBJson release];
		temp_objSBJson=nil;
		
		[temp_string release];
		temp_string=nil;
		
		[temp_arrResponse release];
		temp_arrResponse=nil;
			
		if ( l_appDelegate.m_arrBigSalesData.count > 0 ) {
			btnMoreInfo.hidden=NO;
			
			//dont show the curl image if single ad is there
			if ( l_appDelegate.m_arrBigSalesData.count == 1 )
				m_imgViewCurl.hidden=YES;
			else
				m_imgViewCurl.hidden=NO;
			
			[self performSelector:@selector(downloadBigSalesImages)];
			
		}
		else if ( l_appDelegate.m_arrCategoryIcons.count > 1 ) {
			btnMoreInfo.hidden=YES;
			m_imgViewCurl.hidden=YES;
			[m_imgViewSaleAd setImage:[UIImage imageNamed:@"defaultImage.jpg"]];
		}
		else {
			btnMoreInfo.hidden=YES;
			m_imgViewCurl.hidden=YES;
			[m_imgViewSaleAd setImage:[UIImage imageNamed:@"no_sale.png"]];
			
		}
	
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		[self.view setUserInteractionEnabled:TRUE];
		
		if ( l_appDelegate.m_arrBigSalesData.count>1 ) {
            //first image is already downloaded
			BigSalesModelClass *temp_objBigSales;
			temp_objBigSales = [l_appDelegate.m_arrBigSalesData objectAtIndex:1];
			l_bigSalesId = [temp_objBigSales.m_strId intValue];
			[self sendRequestToLoadImages:temp_objBigSales.m_strAdUrl];
		}
		
	}
	//logic to send requests for image downloading
	else if ( m_mutResponseData != nil && m_intTypeOfResponse == 1 ) {
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		m_intTypeOfResponse = -1;//reset the response variable
		
		if ( l_appDelegate.m_arrBigSalesData.count >= 2 ) {
            //first image is already downloaded
			for ( int i = 0; i < l_appDelegate.m_arrBigSalesData.count; i++ ) {
				BigSalesModelClass *temp_objBigSales;
				temp_objBigSales = [l_appDelegate.m_arrBigSalesData objectAtIndex:i];
				
				if ( [temp_objBigSales.m_strId intValue]==l_bigSalesId ) {
					temp_objBigSales.m_imgAd=[UIImage imageWithData:[NSData dataWithData:m_mutResponseData]];
					[l_appDelegate.m_arrBigSalesData replaceObjectAtIndex:i withObject:temp_objBigSales];
				}
				
				if ( temp_objBigSales.m_imgAd == nil ) {
					l_bigSalesId=[temp_objBigSales.m_strId intValue];
					
					[self sendRequestToLoadImages:temp_objBigSales.m_strAdUrl];	
					break;
				}
				
				if ( i == l_appDelegate.m_arrBigSalesData.count-1 ) {
                    //Start timer to check 10 mins to reload the Big Sales Ads 
					isReload = NO;
                    // GDL: Set time interval to 600 seconds = 10 minutes. It was set to 30 seconds for testing.
					timerBSA = [NSTimer scheduledTimerWithTimeInterval:600 target:self selector:@selector(timerTick) userInfo:nil repeats:NO];
				}				
			}
			
		}
	}
}

//downloads first Ad image only
-(void)downloadBigSalesImages {
	BigSalesModelClass *temp_objBigSales;
	
	for( int i = 0; i < [l_appDelegate.m_arrBigSalesData count]; i++ ) {
		temp_objBigSales = [l_appDelegate.m_arrBigSalesData objectAtIndex:i];
		
		//NSLog(@"%@ , %@",temp_objBigSales.m_strAdUrl,temp_objBigSales.m_strLatitude);
			
		temp_objBigSales.m_imgAd = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[temp_objBigSales.m_strAdUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
		
		//Bharat: 11/23/11: DE10 fix
		//if (l_appDelegate.m_intBigSalesIndex == -1 ) {
			m_imgViewSaleAd.image=temp_objBigSales.m_imgAd;
			l_appDelegate.m_intBigSalesIndex=0;
		//}
		
		[l_appDelegate.m_arrBigSalesData replaceObjectAtIndex:i withObject:temp_objBigSales];
		if ( i == 0 )
            break;
	}
}
		
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"BigSalesAdsViewController:didFailWithError:");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	l_isConnectionActive=FALSE;
	m_isConnectionFailed=TRUE;
	self.view.userInteractionEnabled=TRUE;
	
	UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[networkDownAlert show];
	[networkDownAlert release];
	networkDownAlert=nil;
	
	if(l_appDelegate.m_arrBigSalesData.count>0)
	{
		btnMoreInfo.hidden=NO;
		
		//dont show the curl image if single ad is there
		if(l_appDelegate.m_arrBigSalesData.count==1)
			m_imgViewCurl.hidden=YES;
		else
		{
			m_imgViewCurl.hidden=NO;
		}
		
		//[self performSelector:@selector(downloadBigSalesImages)];
		
	}
	else if(l_appDelegate.m_arrCategoryIcons.count>1) {
		btnMoreInfo.hidden=YES;
		m_imgViewCurl.hidden=YES;
		[m_imgViewSaleAd setImage:[UIImage imageNamed:@"defaultImage.jpg"]];
	}
	else {
		btnMoreInfo.hidden=YES;
		m_imgViewCurl.hidden=YES;
		[m_imgViewSaleAd setImage:[UIImage imageNamed:@"no_sale.png"]];
		
	}

	// inform the user
    //NSLog(@"Connection failed! Error - %@ %@",[error localizedDescription],[[error userInfo] objectForKey:NSErrorFailingURLStringKey]);
	//NSLog(@"Exit :didFailWithError");	
}

#pragma mark -
#pragma mark NSTimer Tick

// Timer called every 10 minutes to reload data.
- (void)timerTick {
	[timerBSA invalidate];
	isReload = YES;
}

@end
