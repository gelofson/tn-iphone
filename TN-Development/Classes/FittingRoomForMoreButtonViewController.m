//
//  FittingRoomForMoreButtonViewController.m
//  QNavigator
//
//  Created by softprodigy on 22/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FittingRoomForMoreButtonViewController.h"
#import "QNavigatorAppDelegate.h"
#import"FittingRoomUseCasesViewController.h"
#import<QuartzCore/QuartzCore.h>
#import"ShopbeeAPIs.h"
#import"AsyncButtonView.h"
#import"Constants.h"
#import"FittingRoomMoreViewController.h"
#import"FittingRoomBoughtItViewController.h"
#import"LoadingIndicatorView.h"
#import"Constants.h"

@implementation FittingRoomForMoreButtonViewController
@synthesize m_scrollView;
@synthesize m_lableButItOrNot;
@synthesize stringBuyitOrNot;
@synthesize  m_Check;
@synthesize m_MoreBtnTagValue;
@synthesize xCoordinate, yCoordinate, tmp_width,tmp_height;

QNavigatorAppDelegate *l_appDelegate;
FittingRoomUseCasesViewController *l_FittingRoomUseCase;
ShopbeeAPIs *l_requestObj;
FittingRoomMoreViewController *l_ViewRequestObj;
FittingRoomBoughtItViewController *l_boughtItRequest;

LoadingIndicatorView *l_moreBtnIndicatorView;

int x,y,height,width;
int PageNo;
int DisplayedRecords;
int TotalRecords;
int Recordsleft;
int MinesTrack;
int messageId;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/
#pragma mark -
#pragma mark custom methods
-(IBAction)IconClickedAction:(id)sender
{
	 messageId=[sender tag];
	if (l_ViewRequestObj) 
	{
		[l_ViewRequestObj release];
		l_ViewRequestObj=nil;
	}
	if (l_boughtItRequest) 
	{
		[l_boughtItRequest release];
		l_boughtItRequest=nil;
	}
	
	l_ViewRequestObj=[[FittingRoomMoreViewController alloc] init];
	l_boughtItRequest=[[FittingRoomBoughtItViewController alloc] init];
	l_ViewRequestObj.m_messageId=messageId;
	//NSLog(@"Message Id is%@",[sender tag]);
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_String=[prefs valueForKey:@"userName"];
	[[LoadingIndicatorView SharedInstance] startLoadingView:self];
	
	l_requestObj=[[ShopbeeAPIs alloc] init];
	[l_moreBtnIndicatorView startLoadingView:self];
	[l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:messageId];
	
	if (l_requestObj) {
		[l_requestObj release],l_requestObj=nil;
	}
}

-(IBAction)btnBackAction:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
	
}

-(IBAction)getMoreRecords:(id)sender
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	PageNo=PageNo+1;
	switch (m_Check) 
	{
		case 1:
			if (m_MoreBtnTagValue==102) 
			{
				NSLog(@"buyit");
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
			}
			else if(m_MoreBtnTagValue==202) 
			{
				NSLog(@"BoughtIt");
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
			}
            else if(m_MoreBtnTagValue == 302) 
			{
				NSLog(kCheckThisOut);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:PageNo];
				
				m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
			}
            else if(m_MoreBtnTagValue == 402) 
			{
				NSLog(kHowDoesThisLook);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:PageNo];
				
				m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
			}
            else if(m_MoreBtnTagValue == 502) 
			{
				NSLog(kFoundASale);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
			}
			break;
		case 2:
			if (m_MoreBtnTagValue==102) {
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForFriends:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
			}
			else if(m_MoreBtnTagValue==202) {
				NSLog(@"BoughtIt");
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForFriends:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
			}
            else if(m_MoreBtnTagValue == 302) 
			{
				NSLog(kCheckThisOut);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForFriends:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:PageNo];
				
				m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
			}
            else if(m_MoreBtnTagValue == 402) 
			{
				NSLog(kHowDoesThisLook);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForFriends:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:PageNo];
				
				m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
			}
            else if(m_MoreBtnTagValue == 502) 
			{
				NSLog(kFoundASale);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForFriends:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
			}
			break;
		case 3:
			if (m_MoreBtnTagValue==102) {
				NSLog(@"buyit");
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForFollowing:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
			}
			else if(m_MoreBtnTagValue==202) {
				NSLog(@"BoughtIt");
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForFollowing:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
			}
            else if(m_MoreBtnTagValue == 302) 
			{
				NSLog(kCheckThisOut);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForFollowing:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:PageNo];
				
				m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
			}
            else if(m_MoreBtnTagValue == 402) 
			{
				NSLog(kHowDoesThisLook);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForFollowing:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:PageNo];
				
				m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
			}
            else if(m_MoreBtnTagValue == 502) 
			{
				NSLog(kFoundASale);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForFollowing:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
			}
			break;
		case 4:
			if (m_MoreBtnTagValue==102) {
				NSLog(@"buyit");
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForNearby:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
			}
			else if(m_MoreBtnTagValue==202) {
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForNearby:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
				NSLog(@"BoughtIt");
			}
            else if(m_MoreBtnTagValue == 302) 
			{
				NSLog(kCheckThisOut);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForNearby:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:PageNo];
				
				m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
			}
            else if(m_MoreBtnTagValue == 402) 
			{
				NSLog(kHowDoesThisLook);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForNearby:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:PageNo];
				
				m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
			}
            else if(m_MoreBtnTagValue == 502) 
			{
				NSLog(kFoundASale);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForNearby:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
			}
			break;
		case 5:
			if (m_MoreBtnTagValue==102) {
				NSLog(@"buyit");
				[[LoadingIndicatorView SharedInstance] startLoadingView:self];
				
				[l_requestObj getUnreadRequestsForPopular:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:25 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
			}
			else if(m_MoreBtnTagValue==202) {
				NSLog(@"BoughtIt");
				[[LoadingIndicatorView SharedInstance] startLoadingView:self];
				[l_requestObj getUnreadRequestsForPopular:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:25 pageno:PageNo];
			
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
			}
            else if(m_MoreBtnTagValue == 302) 
			{
				NSLog(kCheckThisOut);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForPopular:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:PageNo];
				
				m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
			}
            else if(m_MoreBtnTagValue == 402) 
			{
				NSLog(kHowDoesThisLook);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForPopular:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:PageNo];
				
				m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
			}
            else if(m_MoreBtnTagValue == 502) 
			{
				NSLog(kFoundASale);
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForPopular:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
			}
			break;
			
		default:
			break;
	}
	[l_requestObj release];
	l_requestObj=nil;
}

-(IBAction)addiconsOnTheScreen:(NSInteger)number
{
	//NSDictionary *tmp_dict=[[m_mainArray objectAtIndex:0] valueForKey:@"wsMessage"];
    int newHeight;	
	switch (m_Check)
	{
		case 1:
			
			MinesTrack=1;
			if (m_mainArray.count<=24)
			{
				xCoordinate=10, yCoordinate=10;	
				m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,80);
				//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,120);
			}
			else
			{
				xCoordinate=10;//, yCoordinate=yCoordinate+tmp_height+10;
				//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+80);
				//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+60);
			}	
			tmp_width=70, tmp_height=70;
			for (int i=m_mainArray.count-number; i<m_mainArray.count;i++)//(int i=0; i<number; i++) 
			{
				
				if (i%4==0 && i!=0) 
				{
					xCoordinate=10;
					yCoordinate=yCoordinate+tmp_height+10;
					m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+80);
					//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+60);
					
				}
				else if(i!=0)
				{
					xCoordinate=xCoordinate+tmp_width+6;
				}
				NSString *Url=[NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[[m_mainArray objectAtIndex:i] valueForKey:@"senderThumbImageUrl"],70,70];
				
				NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
				
				AsyncButtonView* asyncButton = [[[AsyncButtonView alloc]
												 initWithFrame:CGRectMake(xCoordinate,yCoordinate,tmp_width,tmp_height)] autorelease];
				asyncButton.tag=[[[m_mainArray objectAtIndex:i] valueForKey:@"messageId"] intValue];
				
				[asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[[m_mainArray objectAtIndex:i] valueForKey:@"senderName"]];
				[m_scrollView  addSubview:asyncButton];
				
			}
            
            if (m_mainArray.count%4!=0 && m_mainArray.count!=0) {
                m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
            }
			
            newHeight = (yCoordinate+tmp_height+10 + 40) - m_scrollView.contentSize.height;
            m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+newHeight);
            
			if (m_mainArray.count<TotalRecords && [self.view viewWithTag:2020]==nil)
			{
				UIButton *MoreRecords=[[UIButton alloc] initWithFrame:CGRectMake(170,yCoordinate+tmp_height+10 , 150, 30)];
				MoreRecords.tag=2020;
				[MoreRecords setTitle:@"More Records" forState:UIControlStateNormal];
				[MoreRecords.titleLabel setFont:[UIFont fontWithName:kFontName size:14]];
                [MoreRecords    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
				//[MoreRecords setImage:[UIImage imageNamed:@"no-image"] forState:UIControlStateNormal];
				[MoreRecords addTarget:self action:@selector(getMoreRecords:) forControlEvents:UIControlEventTouchUpInside];
				//[MoreRecords ]
				[m_scrollView addSubview:MoreRecords];
                [MoreRecords release];
				
			}
			else if(m_mainArray.count<TotalRecords)
			{
				[[self.view viewWithTag:2020] setFrame:CGRectMake(170, yCoordinate+tmp_height+10,150,30)];
			}
			else {
				[[self.view viewWithTag:2020] setHidden:YES];
			}
			
			break;
		case 2:
			
			MinesTrack=0;
			
			if (m_mainArray.count<=24)
			{
				xCoordinate=10, yCoordinate=10;	
				m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,80);
			}
			else
			{
				xCoordinate=10;//, yCoordinate=yCoordinate+tmp_height+10;
				//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+80);
			}	
			tmp_width=70, tmp_height=70;
			for (int i=m_mainArray.count-number; i<m_mainArray.count;i++) 
			{
				
				if (i%4==0 && i!=0) 
				{
					xCoordinate=10;
					yCoordinate=yCoordinate+tmp_height+10;
					m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+80);
					//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
				}
				else if(i!=0)
				{
					xCoordinate=xCoordinate+tmp_width+6;
				}
				NSString *Url=[NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[[m_mainArray objectAtIndex:i] valueForKey:@"senderThumbImageUrl"],70,70];
				
				NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
				
				AsyncButtonView* asyncButton = [[[AsyncButtonView alloc]
												 initWithFrame:CGRectMake(xCoordinate,yCoordinate,tmp_width,tmp_height)] autorelease];
				asyncButton.tag=[[[m_mainArray objectAtIndex:i] valueForKey:@"messageId"] intValue];
				[asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[[m_mainArray objectAtIndex:i] valueForKey:@"senderName"]];
				[m_scrollView  addSubview:asyncButton];
				
				
			}
            
            if (m_mainArray.count%4!=0 && m_mainArray.count!=0) {
                m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
            }

			newHeight = (yCoordinate+tmp_height+10 + 40) - m_scrollView.contentSize.height;
            m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+newHeight);
			if (m_mainArray.count<TotalRecords && [self.view viewWithTag:2030]==nil)
			{
				
				UIButton *MoreRecords=[[UIButton alloc] initWithFrame:CGRectMake(170,yCoordinate+tmp_height+10 , 150, 30)];
				MoreRecords.tag=2030;
				[MoreRecords setTitle:@"More Records" forState:UIControlStateNormal];
				[MoreRecords.titleLabel setFont:[UIFont fontWithName:kFontName size:14]];
                [MoreRecords    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
				//[MoreRecords setImage:[UIImage imageNamed:@"no-image"] forState:UIControlStateNormal];
                [MoreRecords    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
				[MoreRecords addTarget:self action:@selector(getMoreRecords:) forControlEvents:UIControlEventTouchUpInside];
				//[MoreRecords ]
				[m_scrollView addSubview:MoreRecords];
				[MoreRecords release];
				MoreRecords=nil; 
			}
			else if(m_mainArray.count<TotalRecords)
			{
				[[self.view viewWithTag:2030] setFrame:CGRectMake(170, yCoordinate+tmp_height+10,150,30)];
			}
			else {
				[[self.view viewWithTag:2030] setHidden:YES];
			}
			
			break;
		case 3:
			
			MinesTrack=0;
			
			if (m_mainArray.count<=24)
			{
				xCoordinate=10, yCoordinate=10;	
				m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,80);
			}
			else
			{
				xCoordinate=10;//, yCoordinate=yCoordinate+tmp_height+10;
				//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
			}	
			tmp_width=70, tmp_height=70;
			
			for (int i=m_mainArray.count-number; i<m_mainArray.count;i++) 
			{
				
				if (i%4==0 && i!=0) 
				{
					xCoordinate=10;
					yCoordinate=yCoordinate+tmp_height+10;
					m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
					//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+60);
				}
				else if(i!=0)
				{
					xCoordinate=xCoordinate+tmp_width+6;
				}
				NSString *Url=[NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[[m_mainArray objectAtIndex:i] valueForKey:@"senderThumbImageUrl"],70,70];
				
				NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
				
				AsyncButtonView* asyncButton = [[[AsyncButtonView alloc]
												 initWithFrame:CGRectMake(xCoordinate,yCoordinate,tmp_width,tmp_height)] autorelease];
				asyncButton.tag=[[[m_mainArray objectAtIndex:i] valueForKey:@"messageId"] intValue];
				[asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[[m_mainArray objectAtIndex:i] valueForKey:@"senderName"]];
				[m_scrollView  addSubview:asyncButton];
				
			}
            
            
            if (m_mainArray.count%4!=0 && m_mainArray.count!=0) {
                m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
            }

			newHeight = (yCoordinate+tmp_height+10 + 40) - m_scrollView.contentSize.height;
            m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+newHeight);
			if (m_mainArray.count<TotalRecords && [self.view viewWithTag:2040]==nil)
			{
				UIButton *MoreRecords=[[UIButton alloc] initWithFrame:CGRectMake(170,yCoordinate+tmp_height+10, 150, 30)];
				MoreRecords.tag=2040;
				[MoreRecords setTitle:@"More Records" forState:UIControlStateNormal];
				[MoreRecords.titleLabel setFont:[UIFont fontWithName:kFontName size:14]];
                [MoreRecords    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
				//[MoreRecords setImage:[UIImage imageNamed:@"no-image"] forState:UIControlStateNormal];
				[MoreRecords addTarget:self action:@selector(getMoreRecords:) forControlEvents:UIControlEventTouchUpInside];
				//[MoreRecords ]
				[m_scrollView addSubview:MoreRecords];
                [MoreRecords release];
			}
			else if(m_mainArray.count<TotalRecords)
			{
				[[self.view viewWithTag:2040] setFrame:CGRectMake(170, yCoordinate+tmp_height+10,150,30)];
			}
			else {
				[[self.view viewWithTag:2040] setHidden:YES];
			}
			
			break;
		case 4:
			//l_ViewRequestObj.MineTrack=NO;
			//			l_boughtItRequest.MineTrack=NO;
			MinesTrack=0;
			
			if (m_mainArray.count<=24)
			{
				xCoordinate=10, yCoordinate=10;	
				m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,70);
			}
			else
			{
				xCoordinate=10;//, yCoordinate=yCoordinate+tmp_height+10;
				//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
			}	
			
			
			tmp_width=70, tmp_height=70;
			for (int i=m_mainArray.count-number; i<m_mainArray.count;i++) 
			{
				
				if (i%4==0 && i!=0) 
				{
					xCoordinate=10;
					yCoordinate=yCoordinate+tmp_height+10;
					m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+80);
					//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
				}
				else if(i!=0)
				{
					xCoordinate=xCoordinate+tmp_width+6;
				}
				NSString *Url=[NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[[m_mainArray objectAtIndex:i] valueForKey:@"senderThumbImageUrl"],70,70];
				
				NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
				
				AsyncButtonView* asyncButton = [[[AsyncButtonView alloc]
												 initWithFrame:CGRectMake(xCoordinate,yCoordinate,tmp_width,tmp_height)] autorelease];
				asyncButton.tag=[[[m_mainArray objectAtIndex:i] valueForKey:@"messageId"] intValue];
				[asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[[m_mainArray objectAtIndex:i] valueForKey:@"senderName"]];
				[m_scrollView  addSubview:asyncButton];
				
			}
            
            if (m_mainArray.count%4!=0 && m_mainArray.count!=0) {
                m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
            }

			newHeight = (yCoordinate+tmp_height+10 + 40) - m_scrollView.contentSize.height;
            m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+newHeight);
			if (m_mainArray.count<TotalRecords && [self.view viewWithTag:2050]==nil)
			{
				UIButton *MoreRecords=[[UIButton alloc] initWithFrame:CGRectMake(170,yCoordinate+tmp_height+10 , 150, 30)];
				MoreRecords.tag=2050;
				[MoreRecords setTitle:@"More Records" forState:UIControlStateNormal];
				[MoreRecords.titleLabel setFont:[UIFont fontWithName:kFontName size:14]];
                [MoreRecords    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
				//[MoreRecords setImage:[UIImage imageNamed:@"no-image"] forState:UIControlStateNormal];
				[MoreRecords addTarget:self action:@selector(getMoreRecords:) forControlEvents:UIControlEventTouchUpInside];
				//[MoreRecords ]
				[m_scrollView addSubview:MoreRecords];
                [MoreRecords release];
			}
			else if(m_mainArray.count<TotalRecords)
			{
				[[self.view viewWithTag:2050] setFrame:CGRectMake(170, yCoordinate+tmp_height+10,150,30)];
			}
			else {
				[[self.view viewWithTag:2050] setHidden:YES];
			}
			
			[self.view addSubview:m_scrollView];
			break;
		case 5:
			
			MinesTrack=0;
			if (m_mainArray.count<=24)
			{
				xCoordinate=10, yCoordinate=10;	
				m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,80);//because starting from 10
			}
			else
			{
				xCoordinate=10;//, yCoordinate=yCoordinate+tmp_height+10;
                //yCoordinate = m_scrollView.contentSize.height - 150;
				//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+60);
				
			}	
			
			tmp_width=70, tmp_height=70;
			for (int i=m_mainArray.count-number; i<m_mainArray.count;i++)
			{
				
				if (i%4==0 && i!=0) 
				{
					xCoordinate=10;
					yCoordinate=yCoordinate+tmp_height+10;
					
					NSLog(@"popular scrollview content height: %f",m_scrollView.contentSize.height);
					
					m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
					//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+65);
					
				}
				else if(i!=0)
				{
					xCoordinate=xCoordinate+tmp_width+6;
				}
				
				
				NSString *Url=[NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[[m_mainArray objectAtIndex:i] valueForKey:@"senderThumbImageUrl"],70,70];
				
				NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
                
				AsyncButtonView* asyncButton = [[[AsyncButtonView alloc]
												 initWithFrame:CGRectMake(xCoordinate,yCoordinate,tmp_width,tmp_height)] autorelease];
				asyncButton.tag=[[[m_mainArray objectAtIndex:i] valueForKey:@"messageId"] intValue];
				[asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[[m_mainArray objectAtIndex:i] valueForKey:@"senderName"]];
				[m_scrollView  addSubview:asyncButton];
				
				
			}
            
            if (m_mainArray.count%4!=0 && m_mainArray.count!=0) {
                m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+70);
            }
			newHeight = (yCoordinate+tmp_height+10 + 40) - m_scrollView.contentSize.height;
            m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.contentSize.height+newHeight);
			if (m_mainArray.count<TotalRecords && [self.view viewWithTag:2060]==nil)
			{
				UIButton *MoreRecords=[[UIButton alloc] initWithFrame:CGRectMake(170,yCoordinate+tmp_height+10 , 150, 30)];
				MoreRecords.tag=2060;
				[MoreRecords setTitle:@"More Records" forState:UIControlStateNormal];
				[MoreRecords.titleLabel setFont:[UIFont fontWithName:kFontName size:14]];
                [MoreRecords    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
				//[MoreRecords setImage:[UIImage imageNamed:@"no-image"] forState:UIControlStateNormal];
				[MoreRecords addTarget:self action:@selector(getMoreRecords:) forControlEvents:UIControlEventTouchUpInside];
				//[MoreRecords ]
				[m_scrollView addSubview:MoreRecords];
                [MoreRecords release];
			}
			else if(m_mainArray.count<TotalRecords)
			{
				[[self.view viewWithTag:2060] setFrame:CGRectMake(170, yCoordinate+tmp_height+10,150,30)];
			}
			else {
				[[self.view viewWithTag:2060] setHidden:YES];
			}
			
			break;
			
		default:
			break;
	}
	
}
#pragma mark -
#pragma mark callback methods
-(void)CallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	
	
	if ([responseCode intValue]==200) 
	{
		NSArray *temp_mainArray =[tempString JSONValue];
		TotalRecords=[[[temp_mainArray objectAtIndex:0] valueForKey:@"totalRecords"]intValue];
		temp_mainArray=[[temp_mainArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"];
		
		for (int i=0; i<[temp_mainArray count];i++)
		{
			[m_mainArray addObject:[temp_mainArray objectAtIndex:i]];
			
			NSInteger MessageID=[[[temp_mainArray objectAtIndex:i] valueForKey:@"messageId"] intValue];
			[m_messageIDsArray addObject:[NSNumber numberWithInt:MessageID]];

		}
			
		
		
		[self addiconsOnTheScreen:temp_mainArray.count];
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
	tempString=nil;
	
}
-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData
{		
	NSLog(@"data downloaded");
	[l_moreBtnIndicatorView stopLoadingView];
	
	if ([responseCode intValue]==200)
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
		
		if (MinesTrack==1) 
		{
			l_ViewRequestObj.MineTrack=YES;
			l_boughtItRequest.MineTrack=YES;
		}
		else 
		{
			l_ViewRequestObj.MineTrack=NO;
			l_boughtItRequest.MineTrack=NO;	
		}
		
		NSDictionary *tmp_dict=[[tmp_array objectAtIndex:0] valueForKey:@"wsMessage"] ;
		NSString *tmp_type=[tmp_dict valueForKey:@"messageType"];
		l_ViewRequestObj.m_Type=tmp_type;
		NSLog(@"response string: %@",tempString);
		if ([tmp_type caseInsensitiveCompare:kBuyItOrNot] == NSOrderedSame) 
		{
			l_ViewRequestObj.m_CallBackViewController = self;
			l_ViewRequestObj.m_dataArray=tmp_array;
			//l_ViewRequestObj.MineTrack=YES;
			l_ViewRequestObj.m_messageIDsArray=m_messageIDsArray;
			
			[self.navigationController pushViewController:l_ViewRequestObj animated:YES];
			[l_ViewRequestObj release];
			l_ViewRequestObj=nil;
		}
		else if([tmp_type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) 
		{
			l_boughtItRequest.m_CallbackViewController = self;
			l_boughtItRequest.m_mainArray=tmp_array;
			l_boughtItRequest.m_messageIDsBoughtItArray=m_messageIDsArray;
			//l_boughtItRequest.m_currentMessageID=messageId;
			//l_boughtItRequest.MineTrack=YES;
			[self.navigationController pushViewController:l_boughtItRequest animated:YES];
			[l_boughtItRequest release];
			l_boughtItRequest=nil;
		}
		else if([tmp_type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
            l_ViewRequestObj.m_CallBackViewController = self;
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArray;
			
            [self.navigationController pushViewController:l_ViewRequestObj animated:YES];
			[l_ViewRequestObj release];
			l_ViewRequestObj = nil;
			
		}
        else if([tmp_type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
            l_ViewRequestObj.m_CallBackViewController = self;
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArray;
			
            [self.navigationController pushViewController:l_ViewRequestObj animated:YES];
			[l_ViewRequestObj release];
			l_ViewRequestObj = nil;
			
		}
        else if([tmp_type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
            l_ViewRequestObj.m_CallBackViewController = self;
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArray;
			
            [self.navigationController pushViewController:l_ViewRequestObj animated:YES];
			[l_ViewRequestObj release];
			l_ViewRequestObj = nil;
			
		}
		
		[tempString release];
		tempString=nil;
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
#pragma mark -
-(void)viewDidAppear:(BOOL)animated
{
	[l_appDelegate.m_customView setHidden:YES];
	
}
- (void)viewDidLoad {
    [super viewDidLoad];
	l_requestObj=[[ShopbeeAPIs alloc] init];
	m_messageIDsArray=[[NSMutableArray alloc] init];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	m_UserId=[prefs valueForKey:@"userName"];
	l_FittingRoomUseCase=[[FittingRoomUseCasesViewController alloc] init];
	l_moreBtnIndicatorView=[LoadingIndicatorView SharedInstance];
	m_lableButItOrNot.text=stringBuyitOrNot;
		
	m_mainArray=[[NSMutableArray alloc]init];
	
	PageNo=1;
switch (m_Check)
{
case 1:
		self.view.backgroundColor=[UIColor colorWithRed:.39f green:.69f blue:.75f alpha:1.0];
		if (m_MoreBtnTagValue==102) {
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			NSLog(kBuyItOrNot);
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:PageNo];
			
			m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
		}
		else if(m_MoreBtnTagValue==202) {
			NSLog(kIBoughtIt);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:PageNo];
			
			m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
		}
        else if(m_MoreBtnTagValue == 302) {
			NSLog(kCheckThisOut);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
		}
        else if(m_MoreBtnTagValue == 402) {
			NSLog(kHowDoesThisLook);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
		}
        else if(m_MoreBtnTagValue == 502) {
			NSLog(kFoundASale);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
		}
		break;
case 2:
		self.view.backgroundColor=[UIColor colorWithRed:.40f green:.71f blue:.78f alpha:1.0];
		if (m_MoreBtnTagValue==102) {
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForFriends:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:PageNo];
			
			m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
		}
		else if(m_MoreBtnTagValue==202) {
			NSLog(kIBoughtIt);
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForFriends:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:PageNo];
			
			m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
		}
        else if(m_MoreBtnTagValue == 302) {
			NSLog(kCheckThisOut);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
		}
        else if(m_MoreBtnTagValue == 402) {
			NSLog(kHowDoesThisLook);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
		}
        else if(m_MoreBtnTagValue == 502) {
			NSLog(kFoundASale);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForMine:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
		}
        break;
case 3:
		self.view.backgroundColor=[UIColor colorWithRed:.43f green:.76f blue:.83f alpha:1.0f];
		if (m_MoreBtnTagValue==102) {
			NSLog(@"buyit");
				[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForFollowing:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:PageNo];
		
			m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
		}
		else if(m_MoreBtnTagValue==202) {
			NSLog(@"BoughtIt");
			[l_moreBtnIndicatorView startLoadingView:self];
		[l_requestObj getUnreadRequestsForFollowing:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:PageNo];
		
			m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
		}
        else if(m_MoreBtnTagValue == 302) {
			NSLog(kCheckThisOut);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForFollowing:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
		}
        else if(m_MoreBtnTagValue == 402) {
			NSLog(kHowDoesThisLook);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForFollowing:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
		}
        else if(m_MoreBtnTagValue == 502) {
			NSLog(kFoundASale);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForFollowing:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
		}
        break;
case 4:
		self.view.backgroundColor=[UIColor colorWithRed:.46f green:.82f blue:.89f alpha:1.0f];
			if (m_MoreBtnTagValue==102) {
				NSLog(@"buyit");
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForNearby:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
			}
			else if(m_MoreBtnTagValue==202) {
				NSLog(@"BoughtIt");
				[l_moreBtnIndicatorView startLoadingView:self];
				[l_requestObj getUnreadRequestsForNearby:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:PageNo];
				
				m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
			}
            else if(m_MoreBtnTagValue == 302) {
                NSLog(kCheckThisOut);
                [[LoadingIndicatorView SharedInstance] startLoadingView:self];
                [l_moreBtnIndicatorView startLoadingView:self];
                [l_requestObj getUnreadRequestsForNearby:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:PageNo];
                
                m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
            }
            else if(m_MoreBtnTagValue == 402) {
                NSLog(kHowDoesThisLook);
                [[LoadingIndicatorView SharedInstance] startLoadingView:self];
                [l_moreBtnIndicatorView startLoadingView:self];
                [l_requestObj getUnreadRequestsForNearby:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:PageNo];
                
                m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
            }
            else if(m_MoreBtnTagValue == 502) {
                NSLog(kFoundASale);
                [[LoadingIndicatorView SharedInstance] startLoadingView:self];
                [l_moreBtnIndicatorView startLoadingView:self];
                [l_requestObj getUnreadRequestsForNearby:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:PageNo];
                
                m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
            }
            break;
case 5:
		self.view.backgroundColor=[UIColor colorWithRed:.58f green:.84f blue:.90f alpha:1.0];
		if (m_MoreBtnTagValue==102) {
			NSLog(@"buyit");
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForPopular:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:25 pageno:PageNo];
			
			m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];
		}
		else if(m_MoreBtnTagValue==202) {
			NSLog(@"BoughtIt");
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForPopular:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:25 pageno:PageNo];
			
			m_lableButItOrNot.text=[[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"];
		}
        else if(m_MoreBtnTagValue == 302) {
			NSLog(kCheckThisOut);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForPopular:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
		}
        else if(m_MoreBtnTagValue == 402) {
			NSLog(kHowDoesThisLook);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForPopular:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
		}
        else if(m_MoreBtnTagValue == 502) {
			NSLog(kFoundASale);
			[[LoadingIndicatorView SharedInstance] startLoadingView:self];
			[l_moreBtnIndicatorView startLoadingView:self];
			[l_requestObj getUnreadRequestsForPopular:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:PageNo];
			
			m_lableButItOrNot.text = [[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
		}
	break;

		default:
			break;
	}
			
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	[l_requestObj release];
	l_requestObj=nil;

}

-(void)viewWillAppear:(BOOL)animated	
{
	
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication]setStatusBarHidden:NO];
	[l_appDelegate.m_customView setHidden:YES];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_scrollView = nil;
    self.m_lableButItOrNot = nil;
}

- (void)dealloc {
    [m_scrollView release];
    [m_lableButItOrNot release];
    [stringBuyitOrNot release];
   // [m_UserId release];
    [m_mainArray release];
    
    [super dealloc];
}

@end
