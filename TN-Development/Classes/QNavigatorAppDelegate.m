//
//  QNavigatorAppDelegate.m
//  QNavigator
//
//  Created by Soft Prodigy on 24/08/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "QNavigatorAppDelegate.h"
#import "CategoryModelClass.h"
#import "Constants.h"
#import "SBJSON.h"
#import "AuditingClass.h"
#import "DBManager.h"
//#import "RegistrationViewController.h"
#import "LoginHomeViewController.h"
#import "MyCLController.h"
#import "BigSalesAdsViewController.h"
#import "WhereBox.h"
#include"BuzzButtonCameraFunctionality.h"
#include "FittingRoomUseCasesViewController.h"
#include "MyPageViewController.h"
#import "AddFriendsMypageViewController.h"
//#import "GooglemapViewController.h"
#import "NewsPageViewController.h"
//#import "MenuViewController.h"
#import "MainPageViewController.h"
#import "ContentViewController.h"
//#define _APP_ID @"207285035948301"
//@@@ TEMPORARY
#define _APP_ID @"453204974697950"
#import "UIImage+ViewImage.h"
#import "NewsDataManager.h"
#import "JMC.h"
#import "NewMapViewController.h"
#import "Context.h"
#import "Reachability.h"
#import "MyPageSettingViewController.h"
static NSString *const kTrackingId = @"UA-38919000-1";


@implementation QNavigatorAppDelegate
@synthesize m_strDeviceToken;
@synthesize window;
//@synthesize tabBarController;
@synthesize intSelectedCategory;
@synthesize intTypeOfView;
@synthesize m_tagIdentifier;
@synthesize m_arrTableData;
@synthesize m_locationManager;
@synthesize m_geoLatitude;
@synthesize m_geoLongitude;
@synthesize m_strDeviceId;
@synthesize m_internetWorking;
@synthesize m_internetReach;
@synthesize m_arrBigSalesData;
@synthesize m_intBigSalesIndex;
@synthesize taggValue;
@synthesize navControllersArray;
@synthesize m_arrCategorySalesData;
@synthesize m_intCategorySalesIndex;
@synthesize m_strCategoryName;

@synthesize m_arrIndyShopData;
@synthesize m_intIndyShopIndex;

@synthesize m_arrCategoryIcons;
@synthesize m_intCurrentView;
@synthesize m_intCategoryIndex;

@synthesize m_intTotalIndyPages;
@synthesize m_intTotalIndyRecords;
@synthesize m_intIndyPageNumber;
@synthesize m_isDataReloadOnSessionExpire;
@synthesize appBeingPushedToBackground;

//@synthesize m_strTempImagesPath;

@synthesize m_objGetCurrentLocation;
@synthesize	m_objGetLocationDistance;

@synthesize isReloadingData;

@synthesize m_reloadAlert;
@synthesize m_session;
@synthesize str_forTwitter;
@synthesize arr;
@synthesize isUserLoggedInAfterLoggedOutState;
@synthesize userInfoDic;
@synthesize m_loginHomeViewCtrl;

@synthesize m_loginViewCtrl;
@synthesize m_checkForRegAndMypage;
@synthesize m_arrBackupIndyShop;
@synthesize isMyPageShow;
@synthesize isFittingRoomShown;
@synthesize m_SelectedMainPageClassObj;
@synthesize isLoginFacebook;
@synthesize isAddFriendLoadFromTab;
@synthesize contentViewController;
@synthesize menuViewController;
@synthesize objAddFriendsMypageViewController;
@synthesize heartBeatTimer;
@synthesize imageforchosecategory;
NSMutableData *l_mutCategoryResponseData;
BOOL l_isConnectionActive;
NSURLConnection  *l_theConnection;

int l_responseCode;

@synthesize isLogged;
@synthesize push_notifications;

@synthesize tracker = tracker_;


#pragma mark -
#pragma mark Application lifecycle

// Bharat: 11/14/2011: Create tab bar manually. So that we can recreate tab bar when logout happens, 
// thus the stacked views will be all fresh for the next logged in user. Why Apple guys did not think abt this!!
- (void) createTabBarController {
	
//	if (tabBarController)
//		[tabBarController release];
//	tabBarController = nil;
	
	self.navControllersArray = [[[NSMutableArray alloc] initWithCapacity:5] autorelease];

	if (SALES_TAB_INDEX > -1) {
		BigSalesAdsViewController * salesVc = [[BigSalesAdsViewController alloc] initWithNibName:@"BigSalesAdsViewController" bundle:nil];
		UINavigationController * salesNavc = [[UINavigationController alloc] initWithRootViewController:salesVc];
		salesNavc.tabBarItem = [[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Sales", nil) image:[UIImage imageNamed:@"dollar.png"] tag:1] autorelease];
		salesNavc.navigationBarHidden = YES;
		[navControllersArray addObject:salesNavc];
		[salesNavc release];
		[salesVc release];
	}

	if (RUNWAY_TAB_INDEX > -1) {
		NewsPageViewController * runwayVc = [[NewsPageViewController alloc] initWithNibName:@"NewsPageViewController" bundle:nil];
		UINavigationController * runwayNavc = [[UINavigationController alloc] initWithRootViewController:runwayVc];
		runwayNavc.tabBarItem = [[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"News", nil) image:nil tag:2] autorelease];
        [runwayNavc.tabBarItem   setFinishedSelectedImage:[UIImage imageNamed:@"news_selected_icon.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"news_white_icon.png"]];
        [runwayNavc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor darkGrayColor], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateNormal];
        [runwayNavc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor colorWithRed:61./255. green:67./255. blue:72./255. alpha:1.], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateSelected];
        [runwayNavc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0., -3.)];
		runwayNavc.navigationBarHidden = YES;
		[navControllersArray addObject:runwayNavc];
		[runwayNavc release];
		[runwayVc release];
	}

    if (GoogleMap_TAB_INDEX>-1)
    {
        NewMapViewController *googleVC=[[ NewMapViewController alloc]initWithNibName:@"NewMapViewController" bundle:nil];
     
        // When NewMapViewController stops crashing we can uncomment following line
//        NewMapViewController *googleVC=[[ NewMapViewController alloc]initWithNibName:@"NewMapViewController" bundle:nil];
        
        UINavigationController *googleNav=[[UINavigationController alloc]initWithRootViewController:googleVC];
        googleNav.tabBarItem=[[[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"News Map", nil) image:[UIImage imageNamed:@"news-map-white.png"] tag:3]autorelease];
        [googleNav.tabBarItem   setFinishedSelectedImage:[UIImage imageNamed:@"news-map.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"news-map-white.png"]];
        [googleNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      [UIColor darkGrayColor], UITextAttributeTextColor,
                                                      nil] forState:UIControlStateNormal];
        [googleNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      [UIColor colorWithRed:61./255. green:67./255. blue:72./255. alpha:1.], UITextAttributeTextColor,
                                                      nil] forState:UIControlStateSelected];
        [googleNav.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0., -3.)];
        googleNav.navigationBarHidden = YES;
        [navControllersArray addObject:googleNav];
        [googleNav release];
        [googleVC release];
    }

	if (SHARE_TAB_INDEX > -1) {
		BuzzButtonCameraFunctionality * shareVc = [[BuzzButtonCameraFunctionality alloc] initWithNibName:@"BuzzButtonCameraFunctionality" bundle:nil];
		[shareVc setHidesBottomBarWhenPushed:YES];
		UINavigationController * shareNavc = [[UINavigationController alloc] initWithRootViewController:shareVc];
		shareNavc.tabBarItem = [[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Report News", nil) image:[UIImage imageNamed:@"newreport_white_icon.png"] tag:4] autorelease];
        [shareNavc.tabBarItem   setFinishedSelectedImage:[UIImage imageNamed:@"newreport_selected_icon.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"newreport_white_icon.png"]];

        [shareNavc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      [UIColor darkGrayColor], UITextAttributeTextColor,
                                                      nil] forState:UIControlStateNormal];
        [shareNavc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor colorWithRed:61./255. green:67./255. blue:72./255. alpha:1.], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateSelected];
        [shareNavc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0., -3.)];
		shareNavc.navigationBarHidden = YES;
		[navControllersArray addObject:shareNavc];
		[shareNavc release];
		[shareVc release];
	}

	if (FIND_FRIENDS_TAB_INDEX > -1) {
		AddFriendsMypageViewController * addFriendsVc = [[AddFriendsMypageViewController alloc] initWithNibName:@"AddFriendsMypageViewController" bundle:nil];
		UINavigationController * addFriendsNavc = [[UINavigationController alloc] initWithRootViewController:addFriendsVc];
		addFriendsNavc.tabBarItem = [[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Find Friends", nil) image:[UIImage imageNamed:@"addfriends_white_icon.png"] tag:5] autorelease];
        [addFriendsNavc.tabBarItem   setFinishedSelectedImage:[UIImage imageNamed:@"addfriends_selected_icon.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"addfriends_white_icon.png"]];
        [addFriendsNavc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      [UIColor darkGrayColor], UITextAttributeTextColor,
                                                      nil] forState:UIControlStateNormal];
        [addFriendsNavc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      [UIColor colorWithRed:61./255. green:67./255. blue:72./255. alpha:1.], UITextAttributeTextColor,
                                                      nil] forState:UIControlStateSelected];
        [addFriendsNavc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0., -3.)];
		addFriendsNavc.navigationBarHidden = YES;
		[navControllersArray addObject:addFriendsNavc];
		[addFriendsNavc release];
		[addFriendsVc release];
	}


	if (MY_PAGE_TAB_INDEX > -1) {
		MyPageViewController * myPageVc = [[MyPageViewController alloc] initWithNibName:@"MyPageViewController" bundle:nil];
		UINavigationController * myPageNavc = [[UINavigationController alloc] initWithRootViewController:myPageVc];
		myPageNavc.tabBarItem = [[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"My page", nil) image:[UIImage imageNamed:@"mynews_white_icon.png"] tag:6] autorelease];
        [myPageNavc.tabBarItem   setFinishedSelectedImage:[UIImage imageNamed:@"mynews_selected_icon.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"mynews_white_icon.png"]];
        [myPageNavc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor darkGrayColor], UITextAttributeTextColor,
                                                           nil] forState:UIControlStateNormal];
        [myPageNavc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:61./255. green:67./255. blue:72./255. alpha:1.], UITextAttributeTextColor,
                                                           nil] forState:UIControlStateSelected];
        [myPageNavc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0., -3.)];
		myPageNavc.navigationBarHidden = YES;
		[navControllersArray addObject:myPageNavc];
		[myPageNavc release];
		[myPageVc release];
	}
    if (SETTINGS_TAB_INDEX>-1)
    {
        MyPageSettingViewController *tmp_obj1=[[MyPageSettingViewController alloc]init];
        UINavigationController * mysettingNav = [[UINavigationController alloc] initWithRootViewController:tmp_obj1];
		mysettingNav.tabBarItem = [[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Settings", nil) image:[UIImage imageNamed:@"settings.png"] tag:7] autorelease];
        [mysettingNav.tabBarItem   setFinishedSelectedImage:[UIImage imageNamed:@"mynews_selected_icon.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"mynews_white_icon.png"]];
        [mysettingNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor darkGrayColor], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateNormal];
        [mysettingNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor colorWithRed:61./255. green:67./255. blue:72./255. alpha:1.], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateSelected];
        [mysettingNav.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0., -3.)];
		mysettingNav.navigationBarHidden = YES;
		[navControllersArray addObject:mysettingNav];
		[mysettingNav release];
		[tmp_obj1 release];

    }
    
    if (NOTIFICATIONS_TAB_INDEX > -1) {
        
        PushNotificationViewController *notificationController=[[PushNotificationViewController alloc] init];
        
		UINavigationController * myPageNavc = [[UINavigationController alloc] initWithRootViewController:notificationController];
		myPageNavc.tabBarItem = [[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"My page", nil) image:[UIImage imageNamed:@"mynews_white_icon.png"] tag:8] autorelease];
        [myPageNavc.tabBarItem   setFinishedSelectedImage:[UIImage imageNamed:@"mynews_selected_icon.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"mynews_white_icon.png"]];
        [myPageNavc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor darkGrayColor], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateNormal];
        [myPageNavc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor colorWithRed:61./255. green:67./255. blue:72./255. alpha:1.], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateSelected];
        [myPageNavc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0., -3.)];
		myPageNavc.navigationBarHidden = YES;
		[navControllersArray addObject:myPageNavc];
		[myPageNavc release];
		[notificationController release];
    }


}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    

    
       
    
	[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];

	//So that creates config backup file
	[Context getInstance];
//#ifdef TEST_FLIGHT_ENGAGED
//	NSLog(@"TEST FLIGHT IS ENGAGED ==========>>>>>>>>>>>> ");
//	//Bharat: 11/16/2011: Integrating with TestFlight
//	[TestFlight takeOff:@"30077c20b3de6e43a6fe546d346fcee3_NjMzNzAyMDEyLTAyLTE2IDIzOjE1OjQyLjI3MDkxNA"];
//	[TestFlight passCheckpoint:[NSString stringWithFormat:@"%@:QNavigatorAppDelegate:didFinishLaunchingWithOptions: started", 
//		[[Context getInstance] getAppVersion]]];
//#endif

    // Initialize Google Analytics with a 120-second dispatch interval. There is a
    // tradeoff between battery usage and timely dispatch.
    [GAI sharedInstance].debug = YES;
    [GAI sharedInstance].dispatchInterval = 120;
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    [[GAI sharedInstance].defaultTracker setSessionTimeout:1800];

    self.tracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingId];
    
	appBeingPushedToBackground = NO;
	isLogged = NO;
	isUserLoggedInAfterLoggedOutState = NO;

#if TARGET_IPHONE_SIMULATOR == 1
	// Bharat: 11/14/2011: Bootstrap location to SFO
    m_geoLatitude=LATITUDE_IN_SIMULATOR;       //san francisco's
    m_geoLongitude=LONGITUDE_IN_SIMULATOR; 
#endif

	// Bharat: 11/14/2011: Create tab bar manually. So that we can recreate tab bar when logout happens, 
	// thus the stacked views will be all fresh for the next logged in user. Why Apple guys did not think abt this!!
	[self createTabBarController];
    
	// [self LogFontName];
	//[self LogFontName];
    NSLog(@"didFinishlLaunching Dictionary %@", [launchOptions JSONRepresentation]);
    
    //Bharat: 11/14/2011: Variable not used. Hence commenting out.
    //BOOL runningFromPushNotification = YES; //or not
    
    isUserLoggedInAfterLoggedOutState = YES;
	intSelectedCategory=1;
	intTypeOfView=0;
	
	//register for push notifications
	[[UIApplication sharedApplication]
	 registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
										 UIRemoteNotificationTypeSound |
										 UIRemoteNotificationTypeAlert)];
	
	AuditingClass *temp_objAuditing=[AuditingClass SharedInstance];
	//[temp_objAuditing initializeMembers];
	temp_objAuditing.m_arrAuditData = [[NSMutableArray alloc]initWithArray:[self readData]];
	//NSLog(@"%d",temp_objAuditing.m_arrAuditData.count);
	
	//m_strTempImagesPath=NSTemporaryDirectory();
	
//	m_objGetCurrentLocation=[[GetCurrentLocation alloc]init];
//	[m_objGetCurrentLocation initializeMembers];
	
	m_objGetLocationDistance=[[GetLocationAfterDistance alloc]init];
	
	//call to method to create/copy the database to proper location
	DBManager *db=[DBManager SharedInstance];
	[db createEditableCopyOfDatabaseIfNeeded];
	[db InitializeDB];
	
	m_arrCategoryIcons=[[NSMutableArray	 alloc]init];
	l_mutCategoryResponseData=[[NSMutableData alloc]init];
	
	//------------------------------------------------
	m_arrBigSalesData=[[NSMutableArray alloc]init];
	m_intBigSalesIndex=-1;//initially set to -1, 
	//------------------------------------------------
	
	//------------------------------------------------
	self.m_arrCategorySalesData=[[NSMutableArray alloc]init];
	m_intCategorySalesIndex=-1;
	m_strCategoryName=@"";
	m_intCategoryIndex=-1;
	//------------------------------------------------
	
	//------------------------------------------------
	m_arrIndyShopData=[[NSMutableArray alloc]init];
	m_intIndyShopIndex=-1;
	//------------------------------------------------
	
    CFStringRef uuidString;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kTinyNewsUserUUID] == nil) {
        CFUUIDRef ref = CFUUIDCreate(kCFAllocatorDefault); 
        uuidString = CFUUIDCreateString(kCFAllocatorDefault, ref);
        char buffer [100];
        CFStringGetCString(uuidString, buffer, 100, kCFStringEncodingASCII);
        [[NSUserDefaults standardUserDefaults] setObject:(NSString *)uuidString forKey:kTinyNewsUserUUID];
    }
	self.m_strDeviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kTinyNewsUserUUID];
    
	//NSString *temp_strCheckRegStatus=[[NSUserDefaults standardUserDefaults] valueForKey:kRegistrationKey];
	[MyCLController sharedInstance].delegate = self;
	
    LoginHomeViewController *aController = [[[LoginHomeViewController alloc]init]autorelease];
	self.m_loginHomeViewCtrl=aController;
    self.m_loginHomeViewCtrl.isUserLoggedOut = TRUE;
	//[self.window addSubview:self.m_loginHomeViewCtrl.view];
    self.window.rootViewController = self.m_loginHomeViewCtrl;
	[window makeKeyAndVisible];
	
	isAddFriendLoadFromTab = YES;
		
//	if ([CLLocationManager locationServicesEnabled] == NO)
//	{
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"\nEnable Location Services to receive deals in your area!\n\nTurn ON Location Services under Settings > General and re-launch application.\n\nWithout Location Services we will not be able to get you hot deals in your neighborhood." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//		[alert show]; [alert release];
//	}
    self.heartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:180 target:self selector:@selector(checkSTO:) userInfo:nil repeats:YES];
/*#ifdef DEBUG
    if (![[NSUserDefaults standardUserDefaults] objectForKey:SERVER_SETTING]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"http://192.95.18.122" forKey:SERVER_SETTING];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
#else*/
    if (![[NSUserDefaults standardUserDefaults] objectForKey:SERVER_SETTING]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"http://tinynews.me" forKey:SERVER_SETTING];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
//#endif
    
    
//==================== Initialize Facebook interface ======================//
    m_session = [[Facebook alloc] initWithAppId:_APP_ID andDelegate:self];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] &&
        [defaults objectForKey:@"FBExpirationDateKey"]) {
        m_session.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        m_session.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
    }
    
   // self.menuViewController = [[[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil] autorelease];
    MainPageViewController *mainController = [[[MainPageViewController alloc] initWithNibName:@"MainPageViewController" bundle:nil] autorelease];
    UINavigationController *nc = [[[UINavigationController alloc] initWithRootViewController:mainController] autorelease];
    nc.navigationBarHidden = YES;
    [self.navControllersArray addObject:nc];
    self.contentViewController = mainController;
    // JMC & Hockey App
//    [[CNSHockeyManager sharedHockeyManager] configureWithIdentifier:@"245fa86e6426840cd3f3687dcd988fc2" delegate:self];
	[[JMC sharedInstance]
        configureJiraConnect:@"https://tinynews1.atlassian.net/"
        projectKey:@"TNIDEV"
        apiKey:@"407356dc-5896-4166-aca1-6db8de3adfcb"];
    
    return YES;
}


// JMC & Hockey App
- (NSString *)customDeviceIdentifier
{
//#ifndef APPSTORE
//    if ([[UIDevice currentDevice] respondsToSelector:@selector(uniqueIdentifier)])
//        return [[UIDevice currentDevice] performSelector:@selector(uniqueIdentifier)];
//#endif    
return nil;
}


- (void)loginWithFB
{
    if (![m_session isSessionValid]) {
        [m_session authorize:nil];
    }
}

- (void)fbDidNotLogin:(BOOL)cancelled
{
    
}

- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt
{
    
}

- (void)fbDidLogout
{
    
}

- (void)fbSessionInvalidated
{
    
}


- (void)storeOAuthData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:m_session.accessToken forKey:@"FBAccessTokenKey"];
    [defaults setObject:m_session.expirationDate forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
}


-(void) removeOAuthData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"FBAccessTokenKey"];
    [defaults removeObjectForKey:@"FBExpirationDateKey"];
    [defaults synchronize];
}

-(void) logoutFromFacebook
{
    if(isLoginFacebook){
        [self removeOAuthData];
        [m_session logout];
    }
}



- (BOOL) application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [m_session handleOpenURL:url];
}

- (BOOL) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [m_session handleOpenURL:url];
}

- (void) fbDidLogin 
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[m_session accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[m_session expirationDate] forKey:@"FBExpirationDateKey"];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {

	//Bharat: 11/14/2011: set state
	self.appBeingPushedToBackground = YES;
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
 
#if 0
#if TARGET_IPHONE_SIMULATOR == 1
  
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    NSDate *item = [NSDate dateWithTimeIntervalSinceNow:5]; 
    
    localNotif.fireDate = item;
	// Notification details
    localNotif.alertBody = @"Hi!! this is mi local notification. This will appear only in Simulator for testing Notification Tray. [Bharat]";
	// Set the action button
    localNotif.alertAction = @"View";
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
	// Specify custom data for the notification
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"someValue" forKey:@"someKey"];
    localNotif.userInfo = infoDict;
    
	// Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    [localNotif release];

#endif
#endif

}

- (NSString *) serverUrl
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:SERVER_SETTING];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	/*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    if (self.appBeingPushedToBackground && self.isLogged) {
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"UpdatePopular" object:nil];
        if (![tnApplication runPingService]) {
            [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
        }
    }
    self.appBeingPushedToBackground = NO;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
	
	//save the auditing data
	AuditingClass *temp_objAuditing=[AuditingClass SharedInstance];
	[self writeData:temp_objAuditing.m_arrAuditData];
	
	DBManager *temp_objDB=[DBManager SharedInstance];
	[temp_objDB flushDB];
	[temp_objDB CloseDatabase];
	
	NSString *tempPath = NSTemporaryDirectory();
	
	//delete the category sales images saved in temproray directory on applcation termination
	//NSError **tempErr;
	NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:tempPath error:NULL];
	//directoryContentsAtPath:tempPath];
	NSArray *onlyJPGs = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.jpg'"]];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	if (onlyJPGs) 
	{	
		for (int i = 0; i < [onlyJPGs count]; i++) {
			//NSLog(@"Directory Count: %i", [onlyJPGs count]);
			NSString *contentsOnly = [NSString stringWithFormat:@"%@%@", tempPath, [onlyJPGs objectAtIndex:i]];
			[fileManager removeItemAtPath:contentsOnly error:nil];
		}
	}
	
}
#pragma mark -
#pragma mark Push Notification Methods
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)_deviceToken {
	
	//Get a hex string from the device token with no spaces or < >
	
	NSString *temp_strDeviceToken;// = [[NSString alloc]init];
	temp_strDeviceToken = [[[[_deviceToken description]
							 stringByReplacingOccurrencesOfString:@"<" withString:@""] 
							stringByReplacingOccurrencesOfString:@">" withString:@""] 
						   stringByReplacingOccurrencesOfString:@" " withString:@""];
	
	NSLog(@"Device Token :--- %@", temp_strDeviceToken);
	
	self.m_strDeviceToken=[temp_strDeviceToken retain];

	if ([application enabledRemoteNotificationTypes] == 0) 
	{
		NSLog(@"Notifications are disabled for this application. Not registering with APNS");
		return;
	}
	
}


-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *) error {
	
#if TEST_FLIGHT_ENGAGED 
	self.m_strDeviceToken=@"5cfd979547225c65cca51a5f2b65d747f8431391dec4d5346b09f106a92946a5";
#else
	self.m_strDeviceToken=@"";
#endif
	NSLog(@"Failed to register with error : %@", error);	
	
}


-(void) showNotificationsTray {
		PushNotificationViewController * vc = [[PushNotificationViewController alloc] init];
        vc.view.frame = CGRectMake(0, 20, vc.view.frame.size.width, vc.view.frame.size.height);
        UINavigationController * navC = self.contentViewController.navigationController;
		if ((navC != nil) && ([navC isKindOfClass:[UINavigationController class]] == YES)) {
			[navC pushViewController:vc animated:YES];
        }
		[vc release];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{

    if (!isLogged || !isUserLoggedInAfterLoggedOutState) {

		//Bharat: 11/14/2011: This is not needed.
		/*
        // Not working right now //
        UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:m_loginHomeViewCtrl];
        [nav.navigationBar setHidden:YES];
        [self.window addSubview:nav.view];
		*/
        
    }
    // If the user is logged in and the session is active. Show Notification Tray.//
    else if (self.appBeingPushedToBackground == YES) {
		/*UINavigationController * navC = [self.navControllersArray objectAtIndex:RUNWAY_TAB_INDEX];
		if ((navC != nil) && (navC.viewControllers != nil)) {
            for (int i =0; i < [navC.viewControllers count]; i++) {
                UIViewController * vc = [navC.viewControllers objectAtIndex:i];
                if ((vc!=nil) && ([vc isKindOfClass:[PushNotificationViewController class]] == YES)) {
                    [navC popToRootViewControllerAnimated:YES];
                    break;
                }
            }
        }*/
        
        
        [self goToNewsPage];

//		[self performSelector:@selector(showNotificationsTray) withObject:nil afterDelay:0.3];
    } else {
        UIAlertView *tmp_AlertView=[[UIAlertView alloc] initWithTitle:@"Test Alert" message:notification.alertBody delegate:nil cancelButtonTitle:@"close" otherButtonTitles:nil];
		[tmp_AlertView show];
		[tmp_AlertView release];
	}

//    if (!self.isUserLoggedInAfterLoggedOutState)
//    {
//        notificationTray = [[PushNotificationViewController alloc] 
//                            initWithDoneButton:YES];
//        notificationTray.view.frame = CGRectMake(0, 20, notificationTray.view.frame.size.width, notificationTray.view.frame.size.height);
//        [self.window addSubview:notificationTray.view];
//    }
    
}

-(void) dismissNotifications
{
    [self.window sendSubviewToBack:push_notifications.view];
    [notificationTray release];
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
	
    if (!isLogged || !isUserLoggedInAfterLoggedOutState) {
		//Bharat: 11/14/2011: This is not needed.
		/*
        // Not working right now //
        UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:m_loginHomeViewCtrl];
        [nav.navigationBar setHidden:YES];
        [self.window addSubview:nav.view];
        */
    }
    else if (self.appBeingPushedToBackground == YES) {
        /*
         Bharat: 11/11/11: Commenting this part out as it is unstable.
        
            // If the user is logged in and the session is active. Show Notification Tray.//
        notificationTray = [[PushNotificationViewController alloc] initWithDoneButton:YES];
        notificationTray.view.frame = CGRectMake(0, 20, notificationTray.view.frame.size.width, notificationTray.view.frame.size.height);
        QNavigatorAppDelegate *del = (QNavigatorAppDelegate *)[UIApplication sharedApplication].delegate;
        // Bharat: 11/11/11: Do not create new navigation control.
        // Find out the current navigation control and push the tray in to that.
        // Bharat: 11/11/11: Needs to be revamped when functionality is activated.
        del.push_notifications = [[UINavigationController alloc] initWithRootViewController:notificationTray];
        [del.push_notifications.navigationBar setHidden:YES];
        //        [del.push_notifications presentModalViewController:notificationTray animated:YES];
        
        [self.window addSubview:del.push_notifications.view];
        */

		/*UINavigationController * navC = [self.navControllersArray objectAtIndex:RUNWAY_TAB_INDEX];
		if ((navC != nil) && (navC.viewControllers != nil)) {
            for (int i =0; i < [navC.viewControllers count]; i++) {
                UIViewController * vc = [navC.viewControllers objectAtIndex:i];
                if ((vc!=nil) && ([vc isKindOfClass:[PushNotificationViewController class]] == YES)) {
                    [navC popToRootViewControllerAnimated:YES];
                    break;
                }
            }
        }
        self.contentViewController = [self.navControllersArray objectAtIndex:RUNWAY_TAB_INDEX];;
        
        self.window.rootViewController = self.contentViewController;*/

		[self performSelector:@selector(showNotificationsTray) withObject:nil afterDelay:0.3];

    } else {
		NSString *alertMsg=nil;
		NSString *title=nil;
		if( [userInfo objectForKey:@"title"] != nil) {
			title = [userInfo objectForKey:@"title"]; 
		}
		if( [[userInfo objectForKey:@"aps"] objectForKey:@"alert"] != nil)
		{
			alertMsg = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
		}
		else
		{    alertMsg = @"{no alert message in dictionary}";
		}
		UIAlertView *tmp_AlertView=[[UIAlertView alloc] initWithTitle:title message:alertMsg delegate:nil cancelButtonTitle:@"close" otherButtonTitles:nil];
		[tmp_AlertView show];
		[tmp_AlertView release];
    }

	//NSLog(@"remote Notification:--%@",userInfo);   
	//The description of the response of notification....
    
//    if (!self.isUserLoggedInAfterLoggedOutState)
//    {
//        notificationTray = [[PushNotificationViewController alloc] 
//                            initWithDoneButton:YES];
//        notificationTray.view.frame = CGRectMake(0, 20, notificationTray.view.frame.size.width, notificationTray.view.frame.size.height);
//        [self.window addSubview:notificationTray.view];
//    }
//    
//
//	UIApplicationState state = [application applicationState];
//    if (state == UIApplicationStateActive) {
//		//the app is in the foreground, so here you do your stuff since the OS does not do it for you
//		//navigate the "aps" dictionary looking for "loc-args" and "loc-key", for example, or your personal payload)
//		UIAlertView *alert = [[UIAlertView alloc] 
//							  initWithTitle:@"FashionGram!" 
//							  message:[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] 
//							  delegate:nil 
//							  cancelButtonTitle:@"OK" 
//							  otherButtonTitles:nil];
//		
//		[alert show];
//		[alert release];
//	}
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc
{
    //[tabBarController release];
	//[navigationController release];
    [tracker_ release];
    [navControllersArray release];
    [window release];
    [m_session release];
	[m_locationManager release];
	[m_arrBigSalesData release];
	//[userInfoDic release];
    [imageforchosecategory release];
    [self.heartBeatTimer invalidate];
    self.heartBeatTimer = nil;
	[super dealloc];
}


#pragma mark -
#pragma mark Category icons webservice methods
-(void)sendRequestForCategoryIcons
{
	[self CheckInternetConnection];
	if(self.m_internetWorking==0)//0: internet working
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		NSMutableString *temp_url; 
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getCategoriesByHandsetLongitudeAndLatitude&did=%@&lng=%f&lat=%f&dist=%d",kServerUrl,self.m_strDeviceId,self.m_geoLongitude,self.m_geoLatitude,50];
		
		
		[temp_url replaceOccurrencesOfString:@"(null)" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [temp_url length])];
		
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];	
		
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory =[paths objectAtIndex:0];
		NSString *writableStatePath=[documentsDirectory stringByAppendingPathComponent:@"cookieFile.plist"];
		
		NSDictionary *tempdict=[NSDictionary dictionaryWithContentsOfFile:writableStatePath];
		NSLog(@"%@",tempdict);
		
		if (tempdict)
		{
			NSHTTPCookie *tempCookie=[NSHTTPCookie  cookieWithProperties:tempdict];
			[[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:tempCookie];
			
			NSArray * availableCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
			NSDictionary * headers;
			headers = [NSHTTPCookie requestHeaderFieldsWithCookies:availableCookies];
			[theRequest setAllHTTPHeaderFields:headers];
		}
		
		
		[theRequest setHTTPShouldHandleCookies:YES];
		[theRequest setHTTPMethod:@"GET"];
		
		if(l_isConnectionActive)
			[l_theConnection cancel];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		[l_theConnection start];
		l_isConnectionActive=TRUE;
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get category icons data");
		}	
	}
	else
	{
		l_isConnectionActive=FALSE;
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		
	}
}

#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	//checks the request response code
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	l_responseCode= [httpResponse statusCode];
	NSLog(@"%d",l_responseCode);
	
	[l_mutCategoryResponseData setLength:0];	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[l_mutCategoryResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	l_isConnectionActive=FALSE;
	
	//[m_loginViewCtrl dismissLoginView];//dismiss the login view
    //[self createTabBarController];
	
	if( l_responseCode==401)
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
	}
	else if(l_mutCategoryResponseData!=nil && l_responseCode==200)
	{
		[m_objGetLocationDistance initializeMembers];
		
		NSLog(@"%@",[[[NSString alloc]initWithData:l_mutCategoryResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string=[[NSString alloc]initWithData:l_mutCategoryResponseData encoding:NSUTF8StringEncoding];
		
		SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		NSArray *temp_arrResponse =[[NSArray alloc]initWithArray:[temp_objSBJson objectWithString:temp_string]];
		
		[temp_objSBJson release];
		temp_objSBJson=nil;
		
		NSDictionary *temp_dict;
		
		BOOL temp_isBigSaleExist;
		temp_isBigSaleExist=FALSE;
		
		if ([temp_arrResponse count]>0)
		{
			if ([[temp_arrResponse objectAtIndex:0] isEqual:[NSNull null]])
			{
				NSLog(@"No sale exists");
                
			}
			else {
				
				if (m_arrCategoryIcons.count>0)
					[m_arrCategoryIcons removeAllObjects];
				
				//
				for (int i=0;i<[temp_arrResponse count]; i++)
				{
					temp_dict=[NSDictionary dictionaryWithDictionary:[temp_arrResponse objectAtIndex:i]];
					[m_arrCategoryIcons addObject:[NSNumber numberWithInt:[[temp_dict objectForKey:@"id"]intValue]]]; //[NSString stringWithFormat:@"%d",[[temp_dict objectForKey:@"id"]intValue]]];
					if ([[temp_dict objectForKey:@"id"]intValue]==2)
					{
						temp_isBigSaleExist=TRUE;
					}
				}
			}
		}
		
		[temp_arrResponse release];
		temp_arrResponse=nil;
		
		//if big sale icon not included, then add it manually
		
		[temp_string release];
		temp_string=nil;
		
		if(!isUserLoggedInAfterLoggedOutState)
		{
            self.contentViewController = [self.navControllersArray objectAtIndex:RUNWAY_TAB_INDEX];
            // set the rootViewController to the contentViewController
            self.window.rootViewController = self.contentViewController;

		} else  {
			
//			self.contentViewController=[self.navControllersArray objectAtIndex:RUNWAY_TAB_INDEX];
//			
//            self.window.rootViewController = self.contentViewController;
            [tnApplication goToNewsPage];
            [self.window makeKeyAndVisible];
			
            if ([self.contentViewController isKindOfClass:[UINavigationController class]]) {
                [(UINavigationController *)self.contentViewController popToRootViewControllerAnimated:NO];
            }
			
			NSString *tempPath = NSTemporaryDirectory();
			
			//NSError **tempErr;
			NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:tempPath error:NULL];
			//directoryContentsAtPath:tempPath];
			NSArray *onlyJPGs = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.jpg'"]];
			
			NSFileManager *fileManager = [NSFileManager defaultManager];
			
			//remove the saved jpg images from temporary directory
			if (onlyJPGs) 
			{	
				for (int i = 0; i < [onlyJPGs count]; i++) {
					//NSLog(@"Directory Count: %i", [onlyJPGs count]);
					NSString *contentsOnly = [NSString stringWithFormat:@"%@%@", tempPath, [onlyJPGs objectAtIndex:i]];
					[fileManager removeItemAtPath:contentsOnly error:nil];
				}
			}
			
		}
		
	}	
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	l_isConnectionActive=FALSE;
	
    NSLog(@"Error in connection %@", [error localizedDescription]);
    [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
	
	if (m_arrCategoryIcons.count > 0)
		[m_arrCategoryIcons removeAllObjects];
	
	[m_arrCategoryIcons addObject:[NSNumber numberWithInt:2]];
    self.window.rootViewController = self.contentViewController;
	//[self.window addSubview:tabBarController.view];
	[self.window makeKeyAndVisible];
	
}

//Generate user proxy
-(void)newLocationUpdate:(NSString *)text 
{	
	NSMutableArray *cordinateData =(NSMutableArray *) [text componentsSeparatedByString:@","];
	if([cordinateData count]==2)
	{
		NSString *strLat = (NSString *)[cordinateData objectAtIndex :0];
		NSString *strLng = (NSString *)[cordinateData objectAtIndex :1];
		
		[[MyCLController sharedInstance].locationManager stopUpdatingLocation];	
		
		self.m_geoLatitude = [strLat floatValue];
		self.m_geoLongitude = [strLng floatValue];
		
		
#if TARGET_IPHONE_SIMULATOR == 1
		//Bharat: 11/11/11: Use these lines if you intend to test application on device outside USA.
		m_geoLatitude=LATITUDE_IN_SIMULATOR;       //san francisco's
		m_geoLongitude=LONGITUDE_IN_SIMULATOR; 
#endif	

		//[self sendRequestForCategoryIcons];
		
	}
}

-(void)failToUpdateLocation;
{
	[[MyCLController sharedInstance].locationManager stopUpdatingLocation];
	
	UIAlertView *temp_alert=[[[UIAlertView alloc]initWithTitle:@"Access denied!" message:@"Unable to get user location. Please enable location services for \"Tiny News\" in settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]autorelease];
	[temp_alert show];
	
	//temporarily set the lat long values
	
	m_geoLatitude=LATITUDE_IN_SIMULATOR;          //san francisco's
	m_geoLongitude=LONGITUDE_IN_SIMULATOR;
	
	
	//[self sendRequestForCategoryIcons];
	
}

//-(void)LogFontName
//{
//	// List all fonts on iPhone
//	//NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
////	NSArray *fontNames;
////	NSInteger indFamily, indFont;
//	//for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
////	{
////		NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
////		fontNames = [[NSArray alloc] initWithArray:
////					 [UIFont fontNamesForFamilyName:
////					  [familyNames objectAtIndex:indFamily]]];
////		for (indFont=0; indFont<[fontNames count]; ++indFont)
////		{
////			NSLog(@"    Font name: %@", [fontNames objectAtIndex:indFont]);
////		}
////		[fontNames release];
////	}
//	
//	NSArray *fontNames = [[NSArray alloc] initWithArray:[UIFont fontNamesForFamilyName:@"Futura"]];
//		for (int i=0; i<[fontNames count]; ++i)
//		{
//			NSLog(@"Font name: %@", [fontNames objectAtIndex:i]);
//		}
//				 
//	
//	//[familyNames release];
//}


-(void)networkErrorInLocationUpdate
{
	
	m_geoLatitude=LATITUDE_IN_SIMULATOR;
	m_geoLongitude=LONGITUDE_IN_SIMULATOR;
	[[MyCLController sharedInstance].locationManager stopUpdatingLocation];
	
	UIAlertView *temp_alert=[[[UIAlertView alloc]initWithTitle:@"Network Error!" message:@"Unable to get user location. Please check your network connection and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]autorelease];
	[temp_alert show];
	
	//[self sendRequestForCategoryIcons];
}

-(void)unknownLocationInLocationUpdate
{
	
	m_geoLatitude=LATITUDE_IN_SIMULATOR;
	m_geoLongitude=LONGITUDE_IN_SIMULATOR;
	[[MyCLController sharedInstance].locationManager stopUpdatingLocation];
	
	UIAlertView *temp_alert=[[[UIAlertView alloc]initWithTitle:@"Unknown Location!" message:@"Application unable to recognize user location. Please check your location services in \"Settings\"." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]autorelease];
	[temp_alert show];
	
	//[self sendRequestForCategoryIcons];
	
}

#pragma mark -
#pragma mark alertView Methods

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	//exit(0);
}


#pragma mark -
#pragma mark Internet Reachability Methods

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
	if([curReach isEqual:self.m_internetReach])
	{
		//NSLog(@"Internet");
		NetworkStatus netStatus = [curReach currentReachabilityStatus];
		switch (netStatus)
		{
			case NotReachable:
			{
				self.m_internetWorking = -1;
				//NSLog(@"Internet NOT WORKING");
				break;
			}
			case ReachableViaWiFi:
			{
				self.m_internetWorking = 0;
				break;
			}
			case ReachableViaWWAN:
			{
				self.m_internetWorking = 0;
				break;
				
			}
		}
	} 
}

//method to check if internet connection is available or not
-(void)CheckInternetConnection
{
	self.m_internetReach = [[Reachability reachabilityForInternetConnection] retain];
	[self.m_internetReach startNotifier];
	[self updateInterfaceWithReachability: self.m_internetReach];
}


- (void) checkSTO:(NSTimer *)aTimer
{
    
    if (self.isLogged) {
        NSLog(@"================= Checking heartbeat =====================");
        [self runPingService];
    }
}

- (BOOL) runPingService
{
    @synchronized (self) {
        NSString *temp_url=[NSString stringWithFormat:@"%@/salebynow/ping",kServerUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:temp_url]];
        
        NSURLResponse *response;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        if (!error && data) {
            NSString *tmpStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

            if ([tmpStr rangeOfString:@"true"].length > 0) {
                NSLog(@"================= We're still logged in =====================");
                return YES;
            }
        }
        
        return NO;
    }
}

#pragma mark -
#pragma mark Plist Methods

//Response from register Device.
-(void)writeData:(NSArray *)arrAuditData
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory =[paths objectAtIndex:0];
	NSString *writableStatePath=[documentsDirectory stringByAppendingPathComponent:kFileName];
	//NSLog(@"%@  %d",writableStatePath,[userArray count]);
	NSFileManager *fileManager = [NSFileManager defaultManager];
	BOOL success =[fileManager fileExistsAtPath:writableStatePath];
	if (success)
	{
		NSLog(@"file already exists");
	}	
	
	NSLog(@"writableStatePath :- %@", writableStatePath);
	
	[arrAuditData writeToFile:writableStatePath atomically:YES];
}

// Returns Details of User when application get Resume 
-(NSArray *)readData
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableStatePath=[documentsDirectory stringByAppendingPathComponent:kFileName];
	NSLog(@"writableStatePath :- %@", writableStatePath);
	BOOL success =[fileManager fileExistsAtPath:writableStatePath];
	if (success)
	{
		NSArray *temp_arrAuditData=[[NSArray alloc]initWithContentsOfFile:writableStatePath];
		return [temp_arrAuditData autorelease];
	}
	
	return nil;
}

#pragma mark -
#pragma mark Application's documents directory

/**
 Returns the path to the application's documents directory.
 */
- (NSString *)applicationDocumentsDirectory 
{
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

- (NSString *)applicationImagesDirectory 
{
    NSString *basePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"Images"];
    BOOL isDir=YES;
    BOOL exist = [[NSFileManager defaultManager] fileExistsAtPath:basePath isDirectory:&isDir];
    
    if (!exist || !isDir) {
        NSError *anError;
        [[NSFileManager defaultManager] removeItemAtPath:basePath error:&anError];
        if(![[NSFileManager defaultManager] createDirectoryAtPath:basePath withIntermediateDirectories:NO attributes:nil error:&anError])
            basePath = [self applicationDocumentsDirectory];
    }
    return basePath;
}

-(void)showSideMenu
{    
    // before swaping the views, we'll take a "screenshot" of the current view
    // by rendering its CALayer into the an ImageContext then saving that off to a UIImage
//    UIImage *image = [UIImage imageFromView:((UINavigationController *)self.contentViewController).visibleViewController.view];
    
    // pass this image off to the MenuViewController then swap it in as the rootViewController
//    self.menuViewController.screenShotImage = image;
    self.contentViewController = [self.navControllersArray lastObject];
    
    //    [self.window.rootViewController viewWillDisappear:YES];
    
    if ([self.contentViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navHandle = (UINavigationController *)self.contentViewController;
        [navHandle popToRootViewControllerAnimated:NO];
        [navHandle.topViewController viewWillDisappear:NO];
        
    }
/*    UIViewController *topViewController = self.window.rootViewController.presentedViewController;
    if (topViewController != nil && [topViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *presenting = (UINavigationController *)topViewController.presentingViewController;
        UIViewController *topController = presenting.topViewController;
        [topController dismissModalViewControllerAnimated:NO];
        if ([topController respondsToSelector:@selector(removePicker)]) {
            [topController performSelector:@selector(removePicker)];
        }
    }*/
    self.window.rootViewController = self.contentViewController;//self.contentViewController;
    //self.window.rootViewController = self.menuViewController;
}

- (BOOL) menuMode
{
    return self.window.rootViewController == self.menuViewController;
}

-(void)hideSideMenu
{
    // all animation takes place elsewhere. When this gets called just swap the contentViewController in
    UINavigationController *navController = (UINavigationController *)self.contentViewController;
    BOOL buzz = [navController.topViewController isKindOfClass:[BuzzButtonCameraFunctionality class]];
    if (buzz) {
        BuzzButtonCameraFunctionality *buzzController = (BuzzButtonCameraFunctionality *)navController.topViewController;
        // don't show picker
        buzzController.wasCameraPickerActiveThenSkipCamLaunch = YES;
    }
    
    self.window.rootViewController = self.contentViewController;
    if (buzz) {
        BuzzButtonCameraFunctionality *buzzController = (BuzzButtonCameraFunctionality *)navController.topViewController;
        [buzzController btnRetakeAction:nil];
    }
}

- (void) goToNewsPage
{
//    [self goToPage:RUNWAY_TAB_INDEX];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [self showSideMenu];
}

- (void) goToPage:(NSInteger)index
{
    self.contentViewController = [self.navControllersArray objectAtIndex:index];
    
    if ([self.contentViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navHandle = (UINavigationController *)self.contentViewController;
        NSArray *poppedControllers = [navHandle popToRootViewControllerAnimated:NO];
        NSLog(@"popped controllers  = %@", poppedControllers);
        [navHandle.topViewController viewWillDisappear:NO];
        NSLog(@"navHandle.topViewController  = %@", navHandle.topViewController);
    }
    
//    [self.window.rootViewController viewWillDisappear:YES];
    self.window.rootViewController = self.contentViewController;//self.contentViewController;

//    if (index == SHARE_TAB_INDEX) {
//        UINavigationController *navController = (UINavigationController *)self.contentViewController;
//        BuzzButtonCameraFunctionality *buzzController = (BuzzButtonCameraFunctionality *)navController.topViewController;
//        [buzzController btnRetakeAction:nil];
//    }
}

- (void) showNotifications
{
    PushNotificationViewController *notificationController=[[PushNotificationViewController alloc] init];
    if ([self.contentViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navControl = (UINavigationController *) self.contentViewController;
        [navControl  pushViewController:notificationController animated:YES];
    }
    [notificationController release];
}

- (void) logout
{
    // Hide the network activity
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    self.isLogged = NO;
    self.m_loginHomeViewCtrl.isUserLoggedOut = TRUE;
    
//    [self.m_loginHomeViewCtrl release];
//    self.m_loginHomeViewCtrl = nil;
//    
//    LoginHomeViewController *aController = [[[LoginHomeViewController alloc]init]autorelease];
//	self.m_loginHomeViewCtrl=aController;

    self.window.rootViewController = self.m_loginHomeViewCtrl;

    self.userInfoDic = nil;
    self.isMyPageShow = YES;
    self.isFittingRoomShown = YES;
    
    NSHTTPCookieStorage * sharedCookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray * cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:
                         [NSURL URLWithString:[NSString stringWithFormat:@"%@/salebynow/", kServerUrl]]];
    for (NSHTTPCookie * cookie in cookies){
        NSLog(@"deleting");
        [sharedCookieStorage deleteCookie:cookie];
    }
    
    
}

@end

