//
//  CategoryModelClass.m
//  QNavigator
//
//  Created by Soft Prodigy on 27/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CategoryModelClass.h"


@implementation CategoryModelClass

@synthesize m_strTitle;
@synthesize m_strDiscount;
@synthesize m_strAddress;
@synthesize m_strStoreWebsite;
@synthesize m_strEndRedemptionAt;
@synthesize m_strStartRedemptionAt;
@synthesize m_strDealUrl;
@synthesize m_strImageUrl;
@synthesize m_strOverview;
@synthesize m_imgItemImage;
@synthesize m_strLatitude;
@synthesize m_strLongitude;
@synthesize m_strMallImageUrl;
@synthesize m_imgItemImage2;
@synthesize m_strImageUrl2;
@synthesize m_strImageUrl3;
@synthesize m_imgItemImage3;
@synthesize m_strDistance;
@synthesize m_strId;
@synthesize m_strBrandName;
@synthesize m_strProductName;
@synthesize m_strStoreHours;
@synthesize m_strPhoneNo;
@synthesize m_strDescription;

- (id)init
{
	if (self = [super init]) {
		
		m_strDealUrl = [[NSString alloc] initWithString:@" "];
		m_strStoreWebsite = [[NSString alloc] initWithString:@" "];
		m_strStartRedemptionAt = [[NSString alloc] initWithString:@" "];
		m_strEndRedemptionAt = [[NSString alloc] initWithString:@" "];
	}
	return self;
}



-(void)dealloc {
	[m_strTitle release];
	[m_strDiscount release];
	[m_strAddress release];
	
	if (m_strEndRedemptionAt != nil)
		[m_strEndRedemptionAt release];
	m_strEndRedemptionAt=nil;
	
	if (m_strStartRedemptionAt != nil)
		[m_strStartRedemptionAt release];
	m_strStartRedemptionAt=nil;
	
	if (m_strStoreWebsite != nil)
		[m_strStoreWebsite release];
	
	if (m_strDealUrl != nil)
		[m_strDealUrl release];
	
	[m_strImageUrl release];
	[m_strOverview release];
	[m_imgItemImage release];
	[m_strLatitude release];
	[m_strLongitude release];
	[m_strMallImageUrl release];
	[m_imgItemImage2 release];
	[m_strImageUrl2 release];
	[m_strDistance release];
	[m_strId release];
	[m_strImageUrl3 release];
	[m_imgItemImage3 release];
	[m_strStoreHours release];
	[m_strBrandName release];
	[m_strProductName release];
	[m_strPhoneNo release];
	[m_strDescription release];
    
	[super dealloc];
}

@end
