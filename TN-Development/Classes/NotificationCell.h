//
//  NotificationCell.h
//  QNavigator
//
//  Created by Nicolas Jakubowski on 10/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "Notification.h"
#import "AsyncButtonView.h"

@protocol NotificationCellDelegate;

@interface NotificationCell : UITableViewCell{
    Notification*                   m_notification;
    AsyncButtonView*                m_asyncButtonView;
    UIImageView*                    m_clockView;
    UILabel*                                m_nameLabel;
    UILabel*                    	m_clockLabel;
    UILabel*                    	m_messageLabel;
    UIImageView*                    m_arrowView;
    CATextLayer*                    m_messageLayer;
    id<NotificationCellDelegate>    m_delegate;
}

@property (nonatomic,retain) Notification*                  notification;
@property (nonatomic,retain) id<NotificationCellDelegate>   delegate;
@property (nonatomic,retain) UIImageView*                  m_arrowView;
@property (nonatomic,retain) UIImageView*                  m_clockView;
@property (nonatomic,retain) UILabel*                  m_clockLabel;
@property (nonatomic,retain) UILabel*                  m_nameLabel;
@property (nonatomic,retain) UILabel*                  m_messageLabel;


+ (CGFloat)heightForNotification:(Notification*)n;

@end

@protocol NotificationCellDelegate <NSObject>

- (void)selectedProfileWithEmail:(NSString*)email withName:(NSString*)name;

@end