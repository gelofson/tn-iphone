//
//  ViewAFriendViewController.h
//  QNavigator
//
//  Created by softprodigy on 06/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsyncButtonView;

@interface ViewAFriendViewController : UIViewController <UIActionSheetDelegate,UIAlertViewDelegate>{
	UILabel *m_label1;
	
	UILabel *m_label2;
	
	UILabel *m_label3;
	
	UIImageView *m_PicView;
	
	UIButton *m_blockBtn;
	
	UIButton *m_unblockBtn;
	
	NSString *m_strLbl1;
	
	UIImage *m_personImage;
	
	UITableView *m_TableView;
	UIScrollView *m_commentsView;
	
	NSArray *m_theProfileArray;
	
	NSArray *m_profileSettings;
	
	UIScrollView *m_ScrollView;
	
	UIImageView *m_imageView;
	
	NSString *m_Email;
	
	UILabel *m_photosLabel;
	
	UILabel *m_friendsLabel;
	
	UILabel *m_wishListLabel;
	
	BOOL isFriendsButtonClicked;
	
	UIButton *m_followBtn;
	
	UIButton *m_unfollowBtn;
	
	int m_checkFollowWebService;// to differentiate between follower and following callback methods

	UIButton *m_followingBtn;
	
	UIButton *m_followerBtn;
	
	UIButton *m_addFriendButton;
	
	
	NSString *m_friendNameString;
	
	BOOL UnblockBtnclicked,FollowBtnClicked;
	
	NSString *m_PingStatus;
	
	NSInteger LevelTrackFriendsView;
	
	BOOL isFriendRequestPending;
	
	UILabel *m_lblFollowStatusLine;
	
	UIButton *m_FriendRequestPendingBtn;
	
	NSString *m_RequestTypeString;
	
	UIImageView *m_followBtnImageView;
	
	NSString *m_YourFriendNameString;
	NSString *m_friendMessageID;
    NSArray *m_commentsArray;
    NSArray *m_dataArray;
	UIButton *m_profileInfoBtn;
	UIButton *m_commentsButton;
    IBOutlet    UIButton *m_addCommentButton;
    BOOL    messageLayout;
    
    UILabel *m_cityLBL;
    UILabel *m_aboutme;
    UILabel *m_favoriteLBL;
    UIView *m_chatView;
    UIScrollView *m_meView;
    
    UIView *m_storyView;
    
    NSMutableArray *m_storyArray;
    UILabel *m_storyHeaderLBL;
    UIImageView *m_mystoryheader;
    int selectedIndex;
    UIButton *m_storyMore;
    
    int selectedFavIndex;
    UIView *m_FavoritesView;
    UIButton *m_FavoritesyMore;
    NSMutableArray *m_favoriteArray;
    UILabel *m_FavoriteHeaderLBL;
    IBOutlet AsyncButtonView* userPortrait;
    UIImageView *m_myfavoriteheader;
    UILabel *favBrandLabel;
    UILabel *interestsLabel;
}
@property(nonatomic,retain) IBOutlet    UILabel *favBrandLabel;
@property(nonatomic,retain) IBOutlet    UILabel *interestsLabel;
@property(nonatomic,retain)IBOutlet UIImageView *m_myfavoriteheader;
@property(nonatomic,retain)IBOutlet UIImageView *m_mystoryheader;
@property(nonatomic,retain)IBOutlet UIView *m_FavoritesView;
@property(nonatomic,retain)IBOutlet UIButton *m_FavoritesyMore;
@property(nonatomic,retain)IBOutlet UILabel *m_FavoriteHeaderLBL;


@property(nonatomic,retain)IBOutlet UIButton *m_storyMore;
@property(nonatomic,retain)IBOutlet UILabel *m_storyHeaderLBL;
@property(nonatomic,retain)IBOutlet UIView *m_storyView;
@property(nonatomic,retain)IBOutlet UIScrollView *m_meView;
@property(nonatomic,retain)IBOutlet UIView *m_chatView;
@property(nonatomic,retain)IBOutlet UILabel *m_favoriteLBL;
@property(nonatomic,retain)IBOutlet UILabel *m_aboutme;
@property(nonatomic,retain)IBOutlet UIButton *m_meBtn;
@property(nonatomic,retain)IBOutlet UIButton *m_messageBtn;
@property(nonatomic,retain)IBOutlet UIButton *m_StoriesBtn;
@property(nonatomic,retain)IBOutlet UIButton *m_FavoritesBtn;

@property(nonatomic,retain)IBOutlet UILabel *m_cityLBL;

@property(nonatomic,retain)IBOutlet UILabel *m_label1;

@property(nonatomic,retain)IBOutlet UILabel *m_label2;

@property(nonatomic,retain) IBOutlet UILabel *m_label3;

@property(nonatomic,retain) IBOutlet UIImageView *m_PicView;

@property(nonatomic,retain) IBOutlet UIButton *m_blockBtn;

@property(nonatomic,retain) IBOutlet UIButton *m_unblockBtn;

@property (nonatomic, retain) NSString *m_strLbl1;

@property(nonatomic,retain) UIImage *m_personImage; 

@property(nonatomic,retain) IBOutlet UITableView *m_TableView;

@property(nonatomic,retain) IBOutlet UIScrollView *m_ScrollView;

@property(nonatomic,retain) IBOutlet UIImageView *m_imageView;

@property(nonatomic,retain) IBOutlet NSString *m_Email;

@property(nonatomic,retain) IBOutlet UILabel *m_photosLabel;

@property(nonatomic,retain) IBOutlet UILabel *m_friendsLabel;

@property(nonatomic,retain) IBOutlet UILabel *m_wishListLabel;

@property(nonatomic,retain) IBOutlet UIButton *m_followBtn;

@property(nonatomic,retain) IBOutlet UIButton *m_unfollowBtn;

@property(nonatomic,retain) IBOutlet UIButton *m_followingBtn;

@property(nonatomic,retain) IBOutlet UIButton *m_followerBtn;

@property(nonatomic,retain) IBOutlet UIButton *m_addFriendButton; 

@property(nonatomic,readwrite) NSInteger LevelTrackFriendsView;

@property (nonatomic,retain) IBOutlet UILabel *m_lblFollowStatusLine;

@property(nonatomic,retain) IBOutlet UIButton *m_FriendRequestPendingBtn;

@property  BOOL isFriendRequestPending;

@property(nonatomic,retain) IBOutlet NSString *m_RequestTypeString;

@property(nonatomic,retain) IBOutlet UIImageView *m_followBtnImageView;

@property(nonatomic,retain)  NSString *m_YourFriendNameString;
@property(nonatomic,copy)  NSString *m_friendMessageID;
@property(nonatomic,retain) IBOutlet	UIScrollView *m_commentsView;
@property(nonatomic,retain) IBOutlet    UIButton *m_profileInfoBtn;
@property(nonatomic,retain) IBOutlet    UIButton *m_commentsButton;
@property(nonatomic, copy) NSArray *m_commentsArray;
@property(nonatomic, copy) NSArray *m_dataArray;
@property (getter = messageLayout, setter = setMessageLayout:) BOOL messageLayout;
@property (nonatomic, retain) IBOutlet    UIButton *m_addCommentButton;
@property (nonatomic, retain) IBOutlet AsyncButtonView* userPortrait;

-(IBAction)m_goToBackView;

-(IBAction) m_BlockAction;

-(IBAction) m_unblockAction;

-(IBAction) m_clickedOnFriendsButton;

-(IBAction) m_viewPhotosAction;

-(IBAction) btn_wishList;

-(IBAction) m_FollowersAction;

-(IBAction) m_FollowingAction;

-(IBAction) btnFollowAction:(id)sender;

-(IBAction)btnUnfollowAction:(id)sender;

-(IBAction) deleteAFriendAction;

-(IBAction) addAFriendAction;
- (void) setImage:(UIImage *) anImage;

-(IBAction)CancelAFriendRequest;
//- (IBAction)showMessages:(id)sender;
//- (IBAction)showProfile:(id)sender;
- (IBAction)addComment:(id)sender;
- (void) setCommentsArray:(NSArray *)anArray;
- (void) presentComments;
-(IBAction)getparticularBtuuonDetails:(id)sender;
-(IBAction)gotoStoryDetailsPage:(id)sender;
-(IBAction)gotoFavoriteDetailsPage:(id)sender;
- (void)commenterImagePressed:(id)sender;

@end
