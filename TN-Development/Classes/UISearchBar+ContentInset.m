//
//  UISearchBar+ContentInset.m
//  QNavigator
//
//  Created by Greg Landweber on 7/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

// GDL: We're using the undocumented setContentInset method on UISearchBar, so I define the property here.
// GDL: This may get the app rejected from the App Store, or it may not.

#import "UISearchBar+ContentInset.h"

@implementation UISearchBar (ContentInset)
@dynamic contentInset;
@end