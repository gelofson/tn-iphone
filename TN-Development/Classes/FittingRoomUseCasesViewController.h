//
//  FittingRoomUseCasesViewController.h
//  QNavigator
//
//  Created by softprodigy on 21/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"ShopbeeAPIs.h"
#import "JMC.h"

#define DEFAULT_NUMBEROF_ITEMS_IN_ROW 3
#define DEFAULT_INTER_ITEM_GAP 4.0f
#define DEFAULT_MARGIN_WIDTH_FROM_SIDES 6.0f
#define DEFAULT_MARGIN_WIDTH_FROM_TOP 4.0f

@interface FittingRoomUseCasesViewController : UIViewController 
{
	UIScrollView *m_scrollView;
	UIScrollView *m_scrollView2;
	//UILabel *m_lableUseCase;
	UIScrollView *m_mineView;
	UIView *m_View;
	UIActivityIndicatorView *m_indicatorView;
	NSArray *m_buyItArray;
    NSArray * m_CheckThisOutArray;
    NSArray * m_HowDoesThisLookArray;
	NSArray * m_FoundASaleArray;
    NSMutableDictionary *m_mainDictionary; 
	NSString *m_UserId;
	ShopbeeAPIs *m_requestObj;
	UIButton *m_MineBtn;
	UIButton *m_FriendsBtn;
	UIButton *m_FollowingBtn;
	UIButton *m_NearbyBtn;
	UIButton *m_PopularBtn;
	NSInteger tagValue;
	NSString *m_type;
	NSMutableArray *m_messageIDsArrayBuyIt;
	NSMutableArray *m_messageIDsArrayBoughtIt;
    NSMutableArray * m_messageIDsArrayCheckThisOut;
    NSMutableArray * m_messageIDsArrayHowDoesThisLook;
    NSMutableArray * m_messageIDsArrayFoundASale;
    UIButton *notificationButton;//**
    UIImageView *m_exceptionPage;//**
	IBOutlet UIButton *m_exceptionButton;
    int sectionNumberToDraw;
    int y;
    
   // UIImageView *notificationBadge;
    int totalRecordCountForActiveTag;
}
@property(retain,nonatomic)IBOutlet UIButton *notificationButton;//**
@property(retain,nonatomic)IBOutlet UIImageView *m_exceptionPage;//**
@property(retain,nonatomic)IBOutlet UIButton *m_exceptionButton;//**
@property (nonatomic,retain) IBOutlet UIScrollView *m_scrollView;
//@property (nonatomic,retain)IBOutlet UILabel *m_lableUseCase;
@property (nonatomic,retain) IBOutlet UIScrollView *m_mineView;
@property(nonatomic,retain) IBOutlet UIButton *m_MineBtn;
@property(nonatomic,retain) IBOutlet UIButton *m_FriendsBtn;
@property(nonatomic,retain) IBOutlet UIButton *m_FollowingBtn;
@property(nonatomic,retain) IBOutlet UIButton *m_NearbyBtn;
@property(nonatomic,retain) IBOutlet UIButton *m_PopularBtn;
@property(nonatomic) NSInteger tagValue;
@property(assign) int sectionNumberToDraw;
@property(assign) int y;
@property(nonatomic, assign) int totalRecordCountForActiveTag;


-(IBAction)IconClickedAction:(id)sender;
-(IBAction)BtnMoreClicked:(id)sender;
-(IBAction)btnPicturesAction:(id)sender;
-(IBAction)sendRequestforAParticularBtn:(id)sender;
-(IBAction)addButtonsOnTheScreen:(UIView*)View NumberOfButtons:(NSInteger)NumberOfButtons type:(NSString*)type;
-(IBAction)viewNotification;
-(IBAction)exceptionButtonAction;

//-(IBAction)btnBoughtItPicturesAction:(id)sender;
- (IBAction)triggerFeedback;

@end
