//
//  BuzzTellYourFriends.h
//  QNavigator
//
//  Created by softprodigy on 02/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BuzzTellYourFriends : UIViewController {

	UIView *m_view;
	UITextView *m_textView;
	UILabel *m_lblTextCount;
}
@property(nonatomic,retain)IBOutlet UILabel *m_lblTextCount;
@property(nonatomic,retain)IBOutlet UIView *m_view;
@property(nonatomic,retain)IBOutlet UITextView *m_textView;

-(IBAction)buzzButtonAction:(id)sender;
-(IBAction)m_goToBackView;

@end
