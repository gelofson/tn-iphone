//
//  TwitterProcessing.h
//  QNavigator
//
//  Created by softprodigy on 29/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import"SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"
#import "FBConnect.h"
#import "MGTwitterEngine.h"
#import "AddFriendsMypageViewController.h"
#import "WhereBox.h"
#import "JSON.h"

@class TwitterFriendsViewController;
NSMutableString *str1;

@interface TwitterProcessing : NSObject <UITextFieldDelegate, SA_OAuthTwitterControllerDelegate,MGTwitterEngineDelegate>{
	
		
		UIView	*m_moreDetailView,*m_shareView;
		UITableView	*m_tableView;
		UIScrollView *m_scrollView;
		NSURLConnection *m_mallMapConnection;
		BOOL m_isConnectionActive;
		BOOL m_isMallMapConnectionActive;
		UIActivityIndicatorView	*m_activityIndicator;
		NSMutableData *m_mutCatResponseData;
		int m_intResponseCode;
		SA_OAuthTwitterEngine *_engine;  
		IBOutlet UIButton *m_cancel;
		IBOutlet UIView *popupView;
		MGTwitterEngine *twitterEngine; 
		TwitterFriendsViewController *m_TwitterFrndsVC;
    
        // GDL: Should be id, not id *
		id m_addFrndMypageVC;
		NSString *strUserId;
		UIView *m_view;
		UIActivityIndicatorView *m_indicatorView;
		NSString *m_TextViewData;
		
	}
	@property (nonatomic,retain)NSString *m_TextViewData;
	@property (nonatomic,retain)NSString *strUserId;;
	@property (nonatomic,retain) UIScrollView *m_scrollView;
	@property (nonatomic,retain) IBOutlet UITableView *m_tableView;
	@property (nonatomic,retain) UIView	*m_moreDetailView;
	@property (nonatomic,retain) IBOutlet UIView *m_shareView;
	@property (nonatomic,retain) NSURLConnection *m_theConnection;
	@property (nonatomic,retain) NSURLConnection *m_mallMapConnection;
	@property BOOL m_isConnectionActive;
	@property BOOL m_isMallMapConnectionActive;
	@property (nonatomic,retain) IBOutlet UIActivityIndicatorView *m_activityIndicator;
	@property int m_intResponseCode;
	@property (nonatomic,retain) NSMutableData	*m_mutCatResponseData;
	@property (nonatomic,retain) MGTwitterEngine *twitterEngine; 
	@property(nonatomic,retain) TwitterFriendsViewController *m_TwitterFrndsVC;

    // GDL: Should be id, not id *
	@property(nonatomic,retain)id m_addFrndMypageVC;

	-(IBAction)btnTellFriendByTwitterAction:(NSInteger)sender;
	-(void)btnInviteAction:(NSString *)str;
    - (void) requestSucceeded: (NSString *) requestIdentifier ;
	- (NSString *) cachedTwitterOAuthDataForUsername: (NSString *) username; 
	
	@end
	

