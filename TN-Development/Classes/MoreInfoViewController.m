//
//  MoreInfoViewController.m
//  QNavigator
//
//  Created by Soft Prodigy on 27/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#include <QuartzCore/QuartzCore.h>

#import "MoreInfoViewController.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "StreetMallMapViewController.h"
#import "CategoryModelClass.h"

#import "MGTwitterStatusesParser.h"
#import "MGTwitterXMLParser.h"
#import "QNavigatorAppDelegate.h"
#import "CustomTopBarView.h"
#import "CategoriesViewController.h"
#import "CategoryModelClass.h"
#import "AuditingClass.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"

#import "SBJSON.h"
#import "ATIconDownloader.h"
#import "DBManager.h"
#import "StreetMallMapViewController.h"
#import "TellAFriendViewController.h"
#import "SA_OAuthTwitterEngine.h" 
#import"SA_OAuthTwitterController.h"
#import "ShopbeeFriendsViewController.h"
#import "MGTwitterEngine.h"
#import "TwitterFriendsViewController.h"
#import "AddFriendsMypageViewController.h"
#import "TwitterProcessing.h"
#import "FittingRoomViewController.h"
#import "ShopbeeAPIs.h"
#import"LoadingIndicatorView.h"



#define POP_UP_OFFSET_X 1
#define POP_UP_OFFSET_Y 45


@implementation MoreInfoViewController

@synthesize popUpforShareInfo;
@synthesize m_txtDesc;
@synthesize m_txtDetail;
@synthesize m_imgViewAdThumb;
@synthesize m_objBigSales;
@synthesize m_lblTagLine;
@synthesize m_isConnectionActive;

@synthesize m_shareView;
//@synthesize m_scrollView;
//@synthesize m_mutCatResponseData;
@synthesize twitterEngine, tw; 
@synthesize m_FittingRoomView;
@synthesize m_emailFrnds,m_facebookFrnds,m_twitterFrnds,m_shopBeeFrnds;
@synthesize m_btnSendBuyOrNot;
@synthesize m_btnSendIBoughtIt;
@synthesize m_sharing_lable;
@synthesize m_imgShareView;
@synthesize m_publicWishlist;
@synthesize m_privateWishlist;
@synthesize m_btnCancelWhereBox;
@synthesize m_privateOrPublic;
@synthesize m_BoughtItOrNot;
//@synthesize m_productImage;
//@synthesize m_DarkGreenBoxView;
//@synthesize isFromIndyShopView;




NSURLConnection *l_theConnection;

QNavigatorAppDelegate *l_appDelegate;

//CategoryModelClass *l_objCatModel;
CLLocation *l_cllocation;
TwitterFriendsViewController *Twitter_obj;
FittingRoomViewController *tempFittingRoom;
LoadingIndicatorView *l_whereboxIndicatorView;
ShopbeeAPIs *l_request;


-(IBAction)backBtnAction
{
	[CATransaction begin];
	CATransition *animation = [CATransition animation];
	animation.type = kCATransitionFromLeft;
	animation.duration = 0.3;
	[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
	[CATransaction commit];
	
	[self.navigationController popViewControllerAnimated:YES];
	
}

-(IBAction)btnMapItAction:(id)sender
{
	[self performSelector:@selector(checkForMallMapExistence)];
	//StreetMallMapViewController *temp_streetMapViewCtrl=[[StreetMallMapViewController alloc]init];
//	
//	
//	//[self.navigationController pushViewController:temp_streetMapViewCtrl animated:YES];
//	[temp_streetMapViewCtrl release];
//	temp_streetMapViewCtrl=nil;
}

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	l_whereboxIndicatorView=[LoadingIndicatorView SharedInstance];
	tempFittingRoom=[[FittingRoomViewController alloc]init];
	[m_emailFrnds setHidden:YES];
	[m_facebookFrnds setHidden:YES];
	[m_twitterFrnds setHidden:YES];
	[m_shopBeeFrnds setHidden:YES];
	[m_btnSendBuyOrNot setHidden:YES];
	[m_btnSendIBoughtIt setHidden:YES];
	[m_sharing_lable setHidden:YES];
	
	
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	m_txtDesc.editable=NO;
	m_txtDetail.editable=NO;
	m_txtDesc.scrollEnabled=FALSE;
	
	m_txtDesc.font=[UIFont fontWithName:kArialFont size:15];
	m_txtDetail.font=[UIFont fontWithName:kArialFont size:15];
	
	if(l_appDelegate.m_arrBigSalesData.count>0)
	{
		m_objBigSales=[l_appDelegate.m_arrBigSalesData objectAtIndex:l_appDelegate.m_intBigSalesIndex];
		m_txtDesc.text=m_objBigSales.m_strDescription1;
		m_txtDetail.text=m_objBigSales.m_strDescription2;
		m_imgViewAdThumb.image=m_objBigSales.m_imgAd;
		m_lblTagLine.text=m_objBigSales.m_strAdTitle;
	}
}

-(void)viewWillAppear:(BOOL)animated
{
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

// GDL: Changed the following two methods.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    [m_txtDesc release]; m_txtDesc = nil;
    [m_txtDetail release]; m_txtDetail = nil;
    [m_imgViewAdThumb release]; m_imgViewAdThumb = nil;
    [m_lblTagLine release]; m_lblTagLine = nil;
}

- (void)dealloc {
    [m_txtDesc release];
    [m_txtDetail release];
    [m_imgViewAdThumb release];
    [m_lblTagLine release];
    //[m_objBigSales release];
	
    [super dealloc];
}

#pragma mark -
#pragma mark Check for mall map existence

-(void)checkForMallMapExistence
{
	if(m_isConnectionActive)
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		m_isConnectionActive=FALSE;
		[l_theConnection cancel];
	}
	
	if(l_appDelegate.m_intCurrentView==2) //2: represents big sales ads view
	{
		if(l_appDelegate.m_arrBigSalesData.count>0 && l_appDelegate.m_intBigSalesIndex!=-1)
		{
			BigSalesModelClass *temp_objBigSales=[l_appDelegate.m_arrBigSalesData objectAtIndex:l_appDelegate.m_intBigSalesIndex];
			
			[self sendRequestToCheckMallMap:temp_objBigSales.m_strMallImageUrl];
			
		}
	}
	
	
	
}

-(void)sendRequestToCheckMallMap:(NSString *)imageUrl
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		//[m_activityIndicator startAnimating];
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/text; charset=utf-8", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if (m_isConnectionActive==TRUE)
		{
			[l_theConnection cancel];
		}
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		//NSURLResponse *temp_response;
//		NSError *temp_error;
//		NSData *temp_data= [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&temp_response error:&temp_error];
//		
//		NSString *text = [[NSString alloc] initWithData:temp_data encoding:NSUTF8StringEncoding];
//		NSLog(@"mall map data: %@", text);
//		
//		if(temp_data != nil && temp_response != NULL )//&& [response statusCode] >= 200 && [response statusCode] < 300)
//		{
//			NSLog(@"inside mall map block");
//			
//		}
		
		[l_theConnection start];
		m_isConnectionActive=TRUE;

		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		m_isConnectionActive=FALSE;
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
			
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		
	}
}

#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	//[m_mutResponseData setLength:0];	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	long long temp_length=[response expectedContentLength];
	//**
	[l_theConnection cancel];
	m_isConnectionActive=FALSE;
	
	StreetMallMapViewController *temp_streetMapViewCtrl=[[StreetMallMapViewController alloc]init];
	
	if (temp_length ==0)
	{
		temp_streetMapViewCtrl.m_isShowMallMapTab=FALSE;	
	}
	else {
		temp_streetMapViewCtrl.m_isShowMallMapTab=TRUE;	
	}

	[self.navigationController pushViewController:temp_streetMapViewCtrl animated:YES];
	[temp_streetMapViewCtrl release];
	temp_streetMapViewCtrl=nil;
	
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	//[m_mutResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	m_isConnectionActive=FALSE;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	//[m_activityIndicator stopAnimating];
//	if(m_mutResponseData!=nil)
//	{
//		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//		m_imgViewMallMap.image=[UIImage imageWithData:[NSData dataWithData:m_mutResponseData]];
//	}
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	m_isConnectionActive=FALSE;
	
	UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[networkDownAlert show];
	[networkDownAlert release];
	networkDownAlert=nil;
	
	
}

-(IBAction)shareBtnAction
{
	[m_emailFrnds setHidden:NO];
	[m_facebookFrnds setHidden:NO];
	[m_twitterFrnds setHidden:NO];
	[m_shopBeeFrnds setHidden:NO];
	
	[m_btnSendBuyOrNot setHidden:YES];
	[m_btnSendIBoughtIt setHidden:YES];
	
	[m_publicWishlist setHidden:YES];
	[m_privateWishlist setHidden:YES];
	[m_btnCancelWhereBox setHidden:YES];
	
	
	[m_imgShareView setImage:[UIImage imageNamed:@"pop-up_background.png"]];
	
	if(![m_shareView superview])
	{
		[self.view	addSubview:m_shareView];
		[m_shareView setTransform:CGAffineTransformMakeTranslation(POP_UP_OFFSET_X, POP_UP_OFFSET_Y)];
		[m_shareView setHidden:NO];
	}
	else {
		[m_shareView setHidden:NO];
		[self.view bringSubviewToFront:m_shareView];
	}
	
}

-(IBAction)btnTellFriendByEmailAction:(id)sender
{
	[self btnTellAFriendAction];
}

-(IBAction)btnCloseOnThreeButtonViewAction
{
	[m_shareView setHidden:YES];
}

-(IBAction)btnTellAFriendAction
{
	[self performSelector:@selector(showPicker)];
}

-(IBAction)ShopbeeFriends
{

	ShopbeeFriendsViewController *shopbee_obj=[[ShopbeeFriendsViewController alloc]init];
	[self.navigationController pushViewController:shopbee_obj animated:YES];
	[shopbee_obj release];
	shopbee_obj=nil;
}

- (IBAction)clickButtonTellFriendsByFacebook:(id)sender {
#ifdef OLD_FB_SDK
	if (l_appDelegate.m_session == nil) {
		l_appDelegate.m_session = [FBSession sessionForApplication:_APP_KEY 
											  secret:_SECRET_KEY delegate:self];
	}
	
	localFBSession = [FBSession session];
	
	if (fbLoginDialog == nil) {
		fbLoginDialog = [[[FBLoginDialog alloc] initWithSession:localFBSession] autorelease];
	}
    [fbLoginDialog show];
#endif
}

-(IBAction) btnFindTwitterFrnd
{
	TwitterProcessing *temp_twitterPross=[[TwitterProcessing alloc]init];
	temp_twitterPross.m_addFrndMypageVC=self;
	[temp_twitterPross btnTellFriendByTwitterAction:5];	
}	

-(IBAction)btnFittingRoomViewAction
{
	
	[m_emailFrnds setHidden:YES];
	[m_facebookFrnds setHidden:YES];
	[m_twitterFrnds setHidden:YES];
	[m_shopBeeFrnds setHidden:YES];
	
	[m_btnSendBuyOrNot setHidden:NO];
	[m_btnSendIBoughtIt setHidden:NO];
	
	[m_publicWishlist setHidden:YES];
	[m_privateWishlist setHidden:YES];
	[m_btnCancelWhereBox setHidden:NO];
	
	
	[m_imgShareView setImage:[UIImage imageNamed:@"pop-up_fittingroom-backgound.png"]];
	
	if(![m_shareView superview])
	{
		[self.view	addSubview:m_shareView];
		[m_shareView setTransform:CGAffineTransformMakeTranslation(POP_UP_OFFSET_X, POP_UP_OFFSET_Y)];
		[m_shareView setHidden:NO];
	}
	else 
	{
		[m_shareView setHidden:NO];
		[self.view bringSubviewToFront:m_shareView];
	}
	
}

-(IBAction)btnWishListViewAction:(id)sender
{
	[m_emailFrnds setHidden:YES];
	[m_facebookFrnds setHidden:YES];
	[m_twitterFrnds setHidden:YES];
	[m_shopBeeFrnds setHidden:YES];
	
	[m_btnSendBuyOrNot setHidden:YES];
	[m_btnSendIBoughtIt setHidden:YES];
	[m_publicWishlist setHidden:NO];
	[m_privateWishlist setHidden:NO];
	[m_btnCancelWhereBox setHidden:NO];
	
	
	[m_imgShareView setImage:[UIImage imageNamed:@"pop-up_wishlist-background.png"]];
	
	
	if(![m_shareView superview])
	{
		[self.view	addSubview:m_shareView];
		[m_shareView setTransform:CGAffineTransformMakeTranslation(POP_UP_OFFSET_X, POP_UP_OFFSET_Y)];
		[m_shareView setHidden:NO];
	}
	else {
		[m_shareView setHidden:NO];
		[self.view bringSubviewToFront:m_shareView];
	}
}

-(IBAction)btnSendIBoughtItAction
{
	m_BoughtItOrNot=@"   I bought it";
	//if(tempFittingRoom)
//	{
//		[tempFittingRoom release];
//		tempFittingRoom = nil;
//	}
	tempFittingRoom=[[FittingRoomViewController alloc]init];
	
	tempFittingRoom.m_sendBuy=@"   I bought it";
	tempFittingRoom.m_typeLabel=@"I bought it";
	
	
	UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Would you like to leave a comment?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil]autorelease];
	[dataReloadAlert show];
	
}
-(IBAction)btnSendBuyOrNotAction
{
	m_BoughtItOrNot=@"   Buy or not";
	//if(tempFittingRoom)
//	{
//		[tempFittingRoom release];
//		tempFittingRoom = nil;
//	}
	tempFittingRoom=[[FittingRoomViewController alloc]init];
	tempFittingRoom.m_sendBuy=@"   Buy or not";
	tempFittingRoom.m_typeLabel=@"Buy or not";
	
	UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Would you like to leave a comment?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil]autorelease];
	[dataReloadAlert show];
}



-(IBAction)btnPublicAction
{
	m_privateOrPublic=@"public";
	[self performSelector:@selector(addToWishList:)];
	
}	
-(IBAction)btnPrivateAction
{
	m_privateOrPublic=@"private";
	[self performSelector:@selector(addToWishList:)];
	
}


-(IBAction)addToWishList:(id)sender
{
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	l_request=[[ShopbeeAPIs alloc] init];
	NSDictionary *dic= 	[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",m_objBigSales.m_strId,@"advtlocid",m_objBigSales.m_strAdTitle,@"comments",m_privateOrPublic,@"visibility",nil];
	NSLog(@"%@",dic);
	[l_whereboxIndicatorView startLoadingView:self];
	[l_request addToWishList:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:dic];
	l_request=nil;
}

-(IBAction)btnTellShopBeeFriendsAction:(id)sender
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	l_request=[[ShopbeeAPIs alloc] init];
	[l_whereboxIndicatorView startLoadingView:self];
	[l_request getFriendsList:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:customerID loginId:customerID];
	[l_request release];
	l_request=nil;
	
}


-(IBAction)btnShareInfoAction:(id)sender
{
	if(![popUpforShareInfo superview])
	{
		[self.view	addSubview:popUpforShareInfo];
		[popUpforShareInfo setTransform:CGAffineTransformMakeTranslation(30,70)];
	}
	else
	{
		[popUpforShareInfo setHidden:NO];
		[self.view bringSubviewToFront:popUpforShareInfo];
	}
}

#pragma mark Facebookmethods
#ifdef OLD_FB_SDK

- (void)session:(FBSession*)session didLogin:(FBUID)uid {
	localFBSession =session;
    NSLog(@"User with id %lld logged in.", uid);
	[self getFacebookName];
}

- (void)getFacebookName {
	NSString* fql = [NSString stringWithFormat:
					 @"select uid,name from user where uid == %lld", localFBSession.uid];
	NSDictionary* params = [NSDictionary dictionaryWithObject:fql forKey:@"query"];
	[[FBRequest requestWithDelegate:self] call:@"facebook.fql.query" params:params];
//	self.post=YES;
}

- (void)request:(FBRequest*)request didLoad:(id)result {
	if ([request.method isEqualToString:@"facebook.fql.query"]) {
//		NSArray* users = result;
//		NSDictionary* user = [users objectAtIndex:0];
//		NSString* name = [user objectForKey:@"name"];
//		self.username = name;		
		
//		if (self.post) {
			[self postToFacebookWall];
//			self.post = NO;
//		}
	}
}

- (void)postToFacebookWall {
	
	NSLog(@"%@, %@, %@, %@, %@, %@", m_objBigSales.m_strAdTitle, m_objBigSales.m_strAdTitle, m_objBigSales.m_strDescription1, m_objBigSales.m_strAddress, m_objBigSales.m_strAdUrl, m_objBigSales.m_strMallImageUrl);
	NSString *message = [NSString stringWithFormat:@"%@, %@, %@", m_objBigSales.m_strAdTitle, m_objBigSales.m_strDescription1, m_objBigSales.m_strAddress];
	//NSString *message2 = [NSString stringWithFormat:@"%@\r%@\r%@", m_objBigSales.m_strAdTitle, m_objBigSales.m_strDescription1, m_objBigSales.m_strAddress];

	FBStreamDialog *dialog = [[[FBStreamDialog alloc] init] autorelease];
	dialog.userMessagePrompt = @"Enter additional comment";
	dialog.delegate = self;
	
	// build attachment with JSONFragment
	NSString *customMessage = message; 
	NSString *postName = m_objBigSales.m_strAdTitle; 
	NSString *serverLink = [NSString stringWithFormat:@"http://itunes.apple.com/app/fashiongram/id498116453?mt=8"];
	NSString *imageSrc = m_objBigSales.m_strAdUrl;
	
	NSMutableDictionary *dictionary = [[[NSMutableDictionary alloc] init]autorelease];
	[dictionary setObject:postName forKey:@"name"];
	[dictionary setObject:serverLink forKey:@"href"];
	[dictionary setObject:customMessage forKey:@"description"];
	
	NSMutableDictionary *media = [[[NSMutableDictionary alloc] init]autorelease];
	[media setObject:@"image" forKey:@"type"];
	[media setObject:serverLink forKey:@"href"];
	[media setObject:imageSrc forKey:@"src"];               
	[dictionary setObject:[NSArray arrayWithObject:media] forKey:@"media"];  
	
	dialog.attachment = [dictionary JSONFragment];
	[dialog show];
	
}

/**
 * Called when the dialog succeeds and is about to be dismissed.
 */
- (void)dialogDidSucceed:(FBDialog*)dialog {
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message sent" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alert show];
	[alert release];	
}

/**
 * Called when the dialog is cancelled and is about to be dismissed.
 */
- (void)dialogDidCancel:(FBDialog*)dialog {
	// do nothing
}

- (void)fbDidLogin
{	
	 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Successful" message:@"login Successfull" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	 [alert show];
	 [alert release];	
	 NSLog(@"logged in.....................");
	
}
#pragma mark 
#pragma mark FBRequestDelegate 
/** 
 * Called when an error prevents the request from completing successfully. 
 */ 
- (void)request:(FBRequest*)request didFailWithError:(NSError*)error{ 
	
} 

/** 
 * Called when a request returns and its response has been parsed into an object. 
 * The resulting object may be a dictionary, an array, a string, or a number, depending 
 * on thee format of the API response. 
 */ 
//- (void)request:(FBRequest*)request didLoad:(id)result { 
//   
//	NSLog(@"resultresult:-%@",result);
//			
//}

- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"received response");
}

#endif
#pragma mark -
#pragma mark alertView  

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if(alertView.tag!=2)
	{
		
		if(buttonIndex==0)
		{
			if([m_BoughtItOrNot isEqualToString:@"   I bought it"])
			{	
				NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
				NSString *customerID= [prefs objectForKey:@"userName"];
				l_request=[[ShopbeeAPIs alloc] init];
				NSDictionary *dic= 	[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",m_objBigSales.m_strId,@"advtlocid",@"",@"comments",nil];
				NSLog(@"%@",dic);
				[l_whereboxIndicatorView startLoadingView:self];
				[l_request sendIBoughtItRequest:@selector(requestCallBackMethodForSendBuyItOrNot:responseData:) tempTarget:self tmpDict:dic];
				[l_request release];
				l_request=nil;
			}
			else 
			{
				NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
				NSString *customerID= [prefs objectForKey:@"userName"];
				l_request=[[ShopbeeAPIs alloc] init];
				NSDictionary *dic= 	[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",m_objBigSales.m_strId,@"advtlocid",@"",@"comments",nil];
				NSLog(@"%@",dic);
				[l_whereboxIndicatorView startLoadingView:self];
				[l_request sendBuyItOrNotRequest:@selector(requestCallBackMethodForSendBuyItOrNot:responseData:) tempTarget:self tmpDict:dic];
				
				[l_request release];
				l_request=nil;
				
				
			}
			[m_shareView setHidden:YES];
		}
		else
		{
			tempFittingRoom.m_Image=self.m_objBigSales.m_imgAd;
			NSLog(@"this is one");//yes
			tempFittingRoom.m_catObjModel = nil;
			tempFittingRoom.m_bigSalesModel = self.m_objBigSales;
			[self.navigationController pushViewController:tempFittingRoom animated:YES];
			
			
		}
		
	}
	
}

#pragma mark -
#pragma mark mail composer methods


-(void)showPicker
{
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self performSelector:@selector(displayComposerSheet)];	
		}
		else
		{
			[self performSelector:@selector(launchMailAppOnDevice)];
		}
	}
	else
	{
		[self performSelector:@selector(launchMailAppOnDevice)];
	}
}



// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	[picker setSubject:@"Share \"FashionGram\""];
	NSMutableArray *toRecipients=[[NSMutableArray alloc]init];

	
	NSMutableString *emailBody;//=[[NSMutableString alloc] init];
	emailBody=[NSMutableString stringWithFormat:@"%@",m_objBigSales.m_strAdTitle];
	[emailBody appendFormat:@"\n"];
	[emailBody appendFormat:@"%@",m_objBigSales.m_strDescription1];
	[emailBody appendFormat:@"\n"];
	[emailBody appendFormat:@"%@",m_objBigSales.m_strDescription2];
	[emailBody appendFormat:@"\n"];
	[emailBody appendFormat:@"%@",m_objBigSales.m_strAddress];
	
	
	//	working
	//
	UIImage *logoPic = [UIImage imageNamed:@"logo_for_share.png"];
	NSData *imageD = UIImageJPEGRepresentation(logoPic, 1);
	[picker addAttachmentData:imageD mimeType:@"image/jpg" fileName:@"logo_for_share.png"];
	
	[picker setToRecipients:toRecipients];
	[toRecipients release];
	toRecipients=nil;
	
	[picker setMessageBody:emailBody isHTML:NO];
	
	[self presentModalViewController:picker animated:NO];
	
    [picker release];
	picker=nil;
	
}	


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	AuditingClass *temp_objAudit;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			[m_shareView setHidden:YES];
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			
			//------------------------------------------------------------
			
			temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Tell a Friend about FashionGram",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-2",@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
				
			}
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"your message sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert setTag:2];
			
			[alert show];
			[alert release];
			
			[m_shareView setHidden:YES];

			//------------------------------------------------------------
					
			
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	
	self.navigationItem.rightBarButtonItem.enabled=NO;
	[self dismissModalViewControllerAnimated:YES];
	
}

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=";
	NSString *body = @"&body=";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

#pragma mark -
#pragma mark Callback methods
-(void)requestCallBackMethodforWishList:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	//NSLog(@"Friend added. response code: %d, response string:%@",[responseCode intValue],[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
	
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	[l_whereboxIndicatorView stopLoadingView];
	
	
	if([responseCode intValue]==200)
	{
		
		UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Your message sent successfully." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]autorelease];
		[m_shareView setHidden:YES];
		[dataReloadAlert show];
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]==500)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Advertisement Expired!" message:@"Advertisement doesn't exists." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else 
	{
		[l_whereboxIndicatorView stopLoadingView];
	}
	
	
}


-(void)requestCallBackMethodForSendBuyItOrNot:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	[l_whereboxIndicatorView stopLoadingView];
	
	
	if([responseCode intValue]==200)
	{
		
		UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Your message sent successfully." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]autorelease];
		[dataReloadAlert show];
		[m_shareView setHidden:YES];
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else 
	{
		[l_whereboxIndicatorView stopLoadingView];
	}
	
	
}



-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	[l_whereboxIndicatorView stopLoadingView];
	
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	
	NSArray *tempArray=[[tempString JSONValue] retain];
	
	if ([responseCode intValue]==200) 
	{
		if ([tempArray count]>0) 
		{
			ShareSalesWithFriends *tmp_obj=[[ShareSalesWithFriends alloc]init];
			tmp_obj.m_catObjModel=nil;
			tmp_obj.m_bigSalesModel=self.m_objBigSales;
			[tmp_obj.m_friendsArray addObjectsFromArray:tempArray];
			[self.navigationController pushViewController:tmp_obj animated:YES];	
			[tmp_obj release];
			tmp_obj=nil;
		}
		else 
		{
			UIAlertView *Tmp_alert=[[ UIAlertView alloc] initWithTitle:@"Oops!" message:@"No FashionGram friends found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[Tmp_alert show];
			[Tmp_alert release];
			Tmp_alert=nil;			
		}

	}
	
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else 
	{
		[l_whereboxIndicatorView stopLoadingView];
	}
	
	[tempString release];
	tempString=nil;
	
}


@end
