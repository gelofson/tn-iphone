//
//  UIView+Badge.m
//  QNavigator
//
//  Created by Mac on 9/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UIView+Badge.h"
#import "BadgeView.h"

@implementation UIView (badge)
-(void)setBadge:(NSString *)badgeString
{
    for (UIView *view in self.subviews) {
        if ([view class]==[BadgeView class]) {
            [view removeFromSuperview];
        }
    }
    BadgeView *badge=(BadgeView *)[[[NSBundle mainBundle] loadNibNamed:@"BadgeView" owner:self options:nil] objectAtIndex:0];
    
    [badge.badgeLabel setText:badgeString];
    [badge setFrame:CGRectMake(self.frame.size.width-10, -5, 20, 20)];
    [self addSubview:badge];
    


}

-(void) removeBadge {
    
    for (UIView * view in [self subviews]) {
        if ([view class] == [BadgeView class]) {
            [view removeFromSuperview];
        }
    }
    
}
@end
