//
//  ProfileSettingViewController.h
//  QNavigator
//
//  Created by softprodigy on 31/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProfileSettingViewController : UIViewController {
	UILabel *m_mailLabel;
	UILabel *m_passwordLabel;
	UILabel *m_firstNameLabel;
	UILabel *m_lastNameLabel;
	UIButton *m_MrButton;
	UIButton *m_MsButton;
	UILabel *m_ageGroupLabel;
}

-(IBAction) m_goToBackView;


@property(nonatomic,retain) IBOutlet UILabel *m_mailLabel;

@property(nonatomic,retain) IBOutlet UILabel *m_passwordLabel;

@property(nonatomic,retain) IBOutlet UILabel *m_firstNameLabel;

@property(nonatomic,retain) IBOutlet UILabel *m_lastNameLabel;

@property(nonatomic,retain) IBOutlet UIButton *m_MrButton;

@property(nonatomic,retain) IBOutlet UIButton *m_MsButton;

@property(nonatomic,retain) IBOutlet UILabel *m_ageGroupLabel;

-(IBAction)resetButtonClick:(id)sender;
@end
