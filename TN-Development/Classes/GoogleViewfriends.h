//
//  GoogleViewfriends.h
//  QNavigator
//
//  Created by softprodigy on 13/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface GoogleViewfriends : UIViewController<MFMailComposeViewControllerDelegate,UINavigationControllerDelegate> {

	NSMutableArray *m_nonshopBeeFriends;
	NSMutableArray *m_googleFriendsEmail;
	NSMutableArray *m_googleAllFriendsEmail;
	NSMutableArray *m_ShopBeegoogleFriends;
	
	NSMutableArray *m_arrAllFriends;
	
	NSString *m_string_email;
	UIView *m_view;
	UIActivityIndicatorView *m_indicatorView;
	UITableView *table;
	
	int currentIndex;
	
	NSMutableData *m_mutResponseData;
	int m_intResponseCode;
	int m_intRequestType;
}
@property int currentIndex;
@property (nonatomic,retain) NSMutableArray *m_arrAllFriends;
@property (nonatomic,retain)IBOutlet UITableView *table;

@property (nonatomic,retain) UIView *m_view;
@property (nonatomic,retain)UIActivityIndicatorView *m_indicatorView;

@property(nonatomic,retain)NSString *m_string_email;
@property(nonatomic,retain)NSMutableArray *m_ShopBeegoogleFriends;
@property(nonatomic,retain)NSMutableArray *m_nonshopBeeFriends;
@property(nonatomic,retain)NSMutableArray *m_googleFriendsEmail;
@property(nonatomic,retain)NSMutableArray *m_googleAllFriendsEmail;
@property (nonatomic,assign) int m_intRequestType;
@property (nonatomic,assign) int m_intResponseCode;
@property (nonatomic,retain) NSMutableData *m_mutResponseData;

-(void)showPicker:(id)sender;
-(void)btnInviteAllAction:(id)sender;
-(IBAction)btnBackAction:(id)sender;
-(IBAction)btnLogoutAction:(id)sender;
-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage;
-(UIImage *)checkForImageAvailability:(NSString *)email;
-(void) performInviteFriends:(NSInteger)tag ;
-(void)showLoadingView;


@end
