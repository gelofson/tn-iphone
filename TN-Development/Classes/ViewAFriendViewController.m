//
//  ViewAFriendViewController.m
//  QNavigator
//
//  Created by softprodigy on 06/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ViewAFriendViewController.h"
#import"QNavigatorAppDelegate.h"
#import"ShopbeeAPIs.h"
#import"SelectedFriendsListDisplay.h"
#import"FansInfoViewController.h"
#import"Constants.h"
#import"JSON.h"
#import"AsyncImageView.h"
#import"MyPagePhotoViewController.h"
#import"WishListViewController.h"
#import"LoadingIndicatorView.h"
#include <QuartzCore/QuartzCore.h>
#import "MessageBoardManager.h"
#import "BuyItCommentViewController.h"
#import "NSArray+Reverse.h"
#import "NewsDataManager.h"
#import "MyFriendViewController.h"
#import "FollowingMeViewController.h"
#import "IMFollowingViewController.h"
#import "CategoryData.h"
#import "DetailPageMessageController.h"
#import "UIImage+Scale.h"
#import "NSMutableArray+Reverse.h"
#import "MoreButton.h"

@implementation ViewAFriendViewController

@synthesize m_label1;

@synthesize m_label2;

@synthesize m_PicView;

@synthesize m_unblockBtn;

@synthesize m_blockBtn;

@synthesize m_label3;

@synthesize m_strLbl1;

@synthesize m_personImage;

@synthesize m_TableView;

@synthesize m_ScrollView;

@synthesize m_imageView;

@synthesize m_Email;

@synthesize m_photosLabel;

@synthesize m_friendsLabel;

@synthesize m_wishListLabel;

@synthesize m_followBtn;

@synthesize m_unfollowBtn;

@synthesize m_followingBtn;

@synthesize m_followerBtn;

@synthesize m_addFriendButton;

@synthesize LevelTrackFriendsView;

@synthesize isFriendRequestPending;

@synthesize m_lblFollowStatusLine;

@synthesize m_FriendRequestPendingBtn;

@synthesize m_RequestTypeString;

@synthesize m_followBtnImageView;

@synthesize m_YourFriendNameString;

@synthesize m_friendMessageID;
@synthesize m_commentsView;
@synthesize m_profileInfoBtn;
@synthesize m_commentsButton;
@synthesize m_commentsArray;
@synthesize m_dataArray;
@synthesize m_addCommentButton;
@synthesize m_cityLBL;
@synthesize m_meBtn;
@synthesize m_messageBtn;
@synthesize m_StoriesBtn;
@synthesize m_FavoritesBtn;
@synthesize m_aboutme;
@synthesize m_favoriteLBL;
@synthesize  m_chatView;
@synthesize m_meView;
@synthesize m_storyView;
@synthesize m_storyHeaderLBL;
@synthesize m_storyMore;

@synthesize m_FavoriteHeaderLBL;
@synthesize m_FavoritesView;
@synthesize m_FavoritesyMore;
@synthesize m_mystoryheader;
@synthesize  m_myfavoriteheader;
@synthesize userPortrait;
@synthesize favBrandLabel;
@synthesize interestsLabel;


QNavigatorAppDelegate *l_appDelegate;

ShopbeeAPIs *l_requestObj;

SelectedFriendsListDisplay *l_SelectedFriendObj;

FansInfoViewController *l_FansView;

LoadingIndicatorView *l_FriendsIndicatorView;
int m_Photos,m_following,m_followers,m_WishListCount,m_friendCount;
#pragma mark custom methods

-(IBAction)getparticularBtuuonDetails:(id)sender
{
    int tag;
    if (sender==nil)
    {
        tag=1;
    }
    else
    {
        tag=[sender tag];
    }
    
    switch (tag) {
        case 1:
            m_meBtn.selected=YES;
            m_messageBtn.selected=NO;
            m_StoriesBtn.selected=NO;
            m_FavoritesBtn.selected=NO;
            self.m_meView.hidden=NO;
            self.m_chatView.hidden=YES;
            self.m_commentsView.hidden=YES;
             [self.m_storyView setHidden:YES];
           
            [self.m_FavoritesView setHidden:YES];
            
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                            withAction:@"showProfile"
                                                             withLabel:nil
                                                             withValue:nil];
            break;
            
        case 2:
            m_meBtn.selected=NO;
            m_messageBtn.selected=YES;
            m_StoriesBtn.selected=NO;
            m_FavoritesBtn.selected=NO;
           self.m_meView.hidden=YES;
             [self.m_storyView setHidden:YES];
            [self.m_FavoritesView setHidden:YES];

            if (messageLayout)
            {
             self.m_chatView.hidden=NO;   
            }
            else
            {
            self.m_chatView.hidden=YES;
            }
            self.m_commentsView.hidden=NO;
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                            withAction:@"showMessages"
                                                             withLabel:nil
                                                             withValue:nil];
            break;
        case 3:
            m_meBtn.selected=NO;
            m_messageBtn.selected=NO;
            m_StoriesBtn.selected=YES;
            m_FavoritesBtn.selected=NO;
             m_meView.hidden=YES;
            selectedIndex = -1;
            CGRect frame=[self.m_storyView frame];
            frame.origin.y=self.m_meView.frame.origin.y-5;
            self.m_storyView.frame=frame;
            
            if (![self.m_ScrollView.subviews containsObject:self.m_storyView]) {
                [self.m_ScrollView addSubview:self.m_storyView];
            }
            
            [self.m_storyView setHidden:NO];
            [self.m_chatView setHidden:YES];
            [self.m_commentsView setHidden:YES];
            [self.m_FavoritesView setHidden:YES];
            
            if (m_storyArray !=nil)
            {
                [m_storyArray release];
                m_storyArray=nil;
            }
            m_storyArray=[[NSMutableArray alloc]init];
            if (m_Photos>0)
            {
                if (m_Photos<6)
                {
                    [self.m_storyMore setHidden:YES];
                }
                else
                {
                 [self.m_storyMore setHidden:NO];
                }
                [l_FriendsIndicatorView startLoadingView:self];
                
                self.m_storyHeaderLBL.hidden=NO;
                self.m_mystoryheader.hidden=NO;
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString *customerID= [prefs objectForKey:@"userName"];
                
               // [l_requestObj getMyPageRecentPhotos:@selector(requestCallBackMethodforresentStory:responseData:) tempTarget:self customerid:m_friendNameString loginid:customerID];
                
                 [l_requestObj getMyPagePopularPhotos:@selector(requestCallBackMethodforPopularPhoto:responseData:) tempTarget:self customerid:m_friendNameString num:10 loginid:customerID];
                
                
            }
            else{
                
                UILabel *lastLBL=(UILabel *)[self.m_storyView viewWithTag:1122];
                if (lastLBL!=nil)
                {
                    [lastLBL removeFromSuperview];
                }
                self.m_storyHeaderLBL.hidden=YES;
                self.m_storyMore.hidden=YES;
                self.m_mystoryheader.hidden=YES;
                UILabel *lab=[[UILabel alloc]initWithFrame:CGRectMake(30, 110, 260, 50)];
                [lab setTag:1122];
                [lab setTextAlignment:UITextAlignmentCenter];
                [lab setBackgroundColor:[UIColor clearColor]];
                [lab setText:@"No Story Available"];
                [lab setNumberOfLines:0];
                [self.m_storyView addSubview:lab];
                [lab release];
            }

            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                            withAction:@"clickedStories"
                                                             withLabel:nil
                                                             withValue:nil];

            break;
        case 4:
            m_meBtn.selected=NO;
            m_messageBtn.selected=NO;
            m_StoriesBtn.selected=NO;
            m_FavoritesBtn.selected=YES;
            selectedFavIndex = -1;
            
            
            CGRect frame1=[self.m_FavoritesView frame];
            frame1.origin.y=self.m_meView.frame.origin.y-5;
            self.m_FavoritesView.frame=frame1;
            if (![self.m_ScrollView.subviews containsObject:self.m_FavoritesView]) {
                [self.m_ScrollView addSubview:self.m_FavoritesView];
            }
            
            [self.m_storyView setHidden:YES];
            [self.m_chatView setHidden:YES];
            [self.m_commentsView setHidden:YES];
            
            [self.m_FavoritesView setHidden:NO];
            
            if (m_favoriteArray!=nil)
            {
                [m_favoriteArray release];
                m_favoriteArray=nil;
            }
            m_favoriteArray=[[NSMutableArray alloc]init];
            [l_FriendsIndicatorView startLoadingView:self];
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *customerID= [prefs objectForKey:@"userName"];
            [l_requestObj getItemsOnFriendWishList:@selector(requestCallBackMethodforGetItemsOfFriends:responseData:) tempTarget:self customerid:customerID friendId:m_friendNameString];
            
            
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                            withAction:@"clickedFavorites"
                                                             withLabel:nil
                                                             withValue:nil];
            break;
        default:
            break;
    }
    
    
}

-(void)loadFavorites
{
    int imageCount= MIN([m_favoriteArray count], 6);
    
    for (int i=0; i < imageCount; i++)
    {
        AsyncButtonView *btn=(AsyncButtonView*)[self.m_FavoritesView viewWithTag:i+50];
        [btn resetButton];
        NSString *temp_strUrl=[NSMutableString stringWithFormat:@"%@%@",kServerUrl,[[m_favoriteArray objectAtIndex:i]valueForKey:@"buzzImageUrl"]];
        btn.cropImage = YES;
        btn.maskImage = YES;
        btn.messageTag = i+50;
        [btn loadImageFromURL:[NSURL URLWithString:temp_strUrl] target:self action:@selector(gotoFavoriteDetailsPage:) btnText:nil];
        
    }
}


-(void)loadStoryView
{
    int imageCount= MIN([m_storyArray count], 6);
    
    for (int i=0; i<imageCount; i++)
    {
        AsyncButtonView *btn=(AsyncButtonView*)[self.m_storyView viewWithTag:i+1];
        NSString *temp_strUrl=[NSMutableString stringWithFormat:@"%@%@",kServerUrl,[[m_storyArray objectAtIndex:i] valueForKey:@"photoUrl"]];
        btn.cropImage = YES;
        btn.maskImage = YES;
        btn.messageTag = i+1;
        [btn loadImageFromURL:[NSURL URLWithString:temp_strUrl] target:self action:@selector(gotoStoryDetailsPage:) btnText:nil];
    }
}

- (void) adjustBounds:(UIImage *)img forButton:(UIButton *)btn
{
    CGSize imageSize = img.size;
    CGSize newSize = imageSize;
    CGRect iconBounds = btn.bounds;
    CGRect cropRect = iconBounds;
    
    CGFloat IWF = imageSize.width/imageSize.height;
    CGFloat WWF = iconBounds.size.width/iconBounds.size.height;
    
    if (IWF>WWF) {
        newSize.height = iconBounds.size.height;
        newSize.width = newSize.height/imageSize.height * imageSize.width;
        cropRect.origin.x = (newSize.width - iconBounds.size.width)/2;
    } else if (WWF>IWF) {
        newSize.width = iconBounds.size.width;
        newSize.height = newSize.width/imageSize.width * imageSize.height;
        cropRect.origin.y = (newSize.height - iconBounds.size.height)/2;
    } else {
        imageSize = iconBounds.size;
        newSize = imageSize;
    }
    
    img = [img scaleToSize:newSize];
    img = [img crop:cropRect];
    
	[btn setImage:img  forState:UIControlStateNormal];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}



-(IBAction)gotoStoryDetailsPage:(id)sender
{
    int tag=[sender tag];
    
    NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
    NSString *tmp_String=[prefs2 stringForKey:@"userName"];
    
    selectedIndex = tag-1;
    [l_FriendsIndicatorView startLoadingView:self];
    [l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_storyArray objectAtIndex:selectedIndex]valueForKey:@"id"]intValue]];
}

-(IBAction)gotoFavoriteDetailsPage:(id)sender
{
    int tag=[sender tag]-50;
    
    NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
    NSString *tmp_String=[prefs2 stringForKey:@"userName"];
    int min = MIN([m_favoriteArray count], 6);
    BOOL inRange = selectedFavIndex < min;
    if (inRange) {
        selectedFavIndex=tag;
        [l_FriendsIndicatorView startLoadingView:self];
        [l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_favoriteArray objectAtIndex:selectedFavIndex]valueForKey:@"messageid"]intValue]];
    }
    
}


-(IBAction)m_goToBackView;    // for navigating to the back view
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction) m_BlockAction			
{
	
	UIActionSheet *tmp_alert=[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" 
										   destructiveButtonTitle:nil otherButtonTitles:@"Turn pings off",@"Unfriend",nil];
	tmp_alert.tag=10;
	[tmp_alert showInView:self.view];
	[tmp_alert release];
	
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"ViewAFriendViewController"
                                                    withAction:@"m_BlockAction"
                                                     withLabel:nil
                                                     withValue:nil];
}

-(IBAction) m_unblockAction
{
	
	UIActionSheet *tmp_alert=[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" 
										   destructiveButtonTitle:nil otherButtonTitles:@"Turn Pings On",@"Unfriend",nil];
	tmp_alert.tag=20;
	[tmp_alert showInView:self.view];
	[tmp_alert release];
	
}
-(IBAction) m_clickedOnFriendsButton
{	
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *m_CustomerId=[prefs valueForKey:@"userName"];
	if (m_friendCount>0) 
	{
		[l_FriendsIndicatorView startLoadingView:self];
		[l_requestObj getFriendsList:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:m_Email loginId:m_CustomerId];
		isFriendsButtonClicked=YES;
	}
	else if (m_friendCount==-1) 
	{
		UIAlertView *tmp_ErrorAlert=[[UIAlertView alloc] initWithTitle:nil message:kCannotViewFriendInfoMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_ErrorAlert show];
		[tmp_ErrorAlert release];
		tmp_ErrorAlert=nil;
		
	}
	
}
-(IBAction) m_viewPhotosAction
{
	if (m_Photos>0)
	{
		MyPagePhotoViewController *m_photocontroller=[[MyPagePhotoViewController alloc] initWithNibName:@"MyPagePhotoViewController" bundle:[NSBundle mainBundle]];
		m_photocontroller.isFromViewFriends=YES;
		m_photocontroller.m_FriendMailString=m_Email;
		m_photocontroller.m_FriendNameString=m_YourFriendNameString;
        m_photocontroller.storyCount=m_Photos;
		[self.navigationController pushViewController:m_photocontroller animated:YES];	
		[m_photocontroller release];
		m_photocontroller=nil;
	}
	else if (m_Photos==-1) 
	{
		UIAlertView *tmp_ErrorAlert=[[UIAlertView alloc] initWithTitle:nil message:kCannotViewFriendInfoMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_ErrorAlert show];
		[tmp_ErrorAlert release];
		tmp_ErrorAlert=nil;
		
	}
	
}
-(IBAction) btn_wishList
{
	if (m_WishListCount>0) 
	{
		
		WishListViewController *temp_obj_wishList=[[WishListViewController alloc]initWithNibName:@"WishListViewController" bundle:[NSBundle mainBundle]];
		temp_obj_wishList.isFromFriendsView=YES;
		//l_appDelegate.m_friendIdForWishList=m_Email;
		temp_obj_wishList.m_friendsMailString=m_Email;
		[self.navigationController pushViewController:temp_obj_wishList animated:YES ];
		[temp_obj_wishList release];
		temp_obj_wishList=nil;
	}
	else if (m_WishListCount==-1) 
	{
		UIAlertView *tmp_ErrorAlert=[[UIAlertView alloc] initWithTitle:nil message:kCannotViewFriendInfoMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_ErrorAlert show];
		[tmp_ErrorAlert release];
		tmp_ErrorAlert=nil;
		
	}
	
}
-(IBAction) m_FollowersAction
{
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *m_loginId=[prefs valueForKey:@"userName"];
	if (m_followers>0) 
	{
		[l_FriendsIndicatorView startLoadingView:self];
		[l_requestObj getFollowerList:@selector(FollowsCallBackMethod:responseData:) tempTarget:self customerid:m_Email loginid:m_loginId];
	
		m_checkFollowWebService=1;
	}
	
}
-(IBAction) m_FollowingAction
{
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *m_loginId=[prefs valueForKey:@"userName"];
	if (m_following>0) 
	{

		[l_FriendsIndicatorView startLoadingView:self];
		[l_requestObj getFollowingList:@selector(FollowsCallBackMethod:responseData:) tempTarget:self customerid:m_Email loginid:m_loginId];
		
		m_checkFollowWebService=-1;
	}
	else if (m_following==-1) 
	{
		UIAlertView *tmp_ErrorAlert=[[UIAlertView alloc] initWithTitle:nil message:kCannotViewFriendInfoMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_ErrorAlert show];
		[tmp_ErrorAlert release];
		tmp_ErrorAlert=nil;
		
	}

}
-(IBAction) btnFollowAction:(id)sender{
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *m_loginId=[prefs valueForKey:@"userName"];
	NSMutableArray *tmp_array=[[NSMutableArray alloc] init];
	[tmp_array addObject:m_Email];
	FollowBtnClicked=YES;
	[l_FriendsIndicatorView startLoadingView:self];
	[l_requestObj addFollowing:@selector(FollowAndUnfollowCallBackMethod:responseData:) tempTarget:self customerid:m_loginId followers:tmp_array];
	[tmp_array release];
	tmp_array=nil;
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"ViewAFriendViewController"
                                                    withAction:@"btnFollowAction"
                                                     withLabel:nil
                                                     withValue:nil];
	
}
-(IBAction)btnUnfollowAction:(id)sender
{
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *m_loginId=[prefs valueForKey:@"userName"];
	//NSString *custid=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	[l_FriendsIndicatorView startLoadingView:self];
	[l_requestObj unfollowAFriend:@selector(FollowAndUnfollowCallBackMethod:responseData:) tempTarget:self userid:m_loginId following:m_Email];
	FollowBtnClicked=NO;
}
-(IBAction) deleteAFriendAction
{
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_CustomerId= [prefs valueForKey:@"userName"];
	//NSString *customerID=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	[l_FriendsIndicatorView startLoadingView:self];
	[l_requestObj deletefriend:@selector(DeleteFriendCallBackMethod:responseData:) tempTarget:self custId:tmp_CustomerId friendid:m_Email];
	
	
}
-(IBAction) addAFriendAction
{
	//NSString *customerID=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_CustomerId= [prefs valueForKey:@"userName"];
	NSMutableArray *tmp_array=[[NSMutableArray alloc] init];
	[tmp_array addObject:m_Email];
	NSString *tmp_String=[tmp_array JSONRepresentation];
	[tmp_array release];
	tmp_array=nil;
	[l_FriendsIndicatorView startLoadingView:self];
	[l_requestObj sendRequestToAddFriend:@selector(AddFriendCallBackMethod:responseData:) tempTarget:self custId:tmp_CustomerId tmpUids:tmp_String ];
	
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"ViewAFriendViewController"
                                                    withAction:@"addAFriendAction"
                                                     withLabel:nil
                                                     withValue:nil];
}
-(IBAction)CancelAFriendRequest
{
	NSString *acceptString=@"N";
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_UserId=[prefs valueForKey:@"userName"];
	[l_FriendsIndicatorView startLoadingView:self];
	[l_requestObj answerFriendRequest:@selector(requestCallBackMethodForAcceptOrReject:responseData:) tempTarget:self userid:m_Email friendid:tmp_UserId acceptString:acceptString];
	
}
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

- (void) setImage:(UIImage *) anImage
{
    self.m_personImage = anImage;
}

#pragma mark action sheet methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (actionSheet.tag==10) 
	{
		if (buttonIndex==0) 
			{
				NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
				NSString *tmp_string=[prefs valueForKey:@"userName"];
				[l_FriendsIndicatorView startLoadingView:self];
				[l_requestObj TurnPingsOnOrOff:@selector(blockORunblockPingsCallBackMethod:responseData:) tempTarget:self userid:tmp_string  friendid:m_Email pingStatus:@"Off"];
					
			}	
		else if (buttonIndex==1) 
		{
			UIAlertView *tmp_alertview=[[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete this friend?" message:nil
															 delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil ];
			[tmp_alertview show];
			[tmp_alertview setTag:11];
			[tmp_alertview release];
		}
	}
	else if(actionSheet.tag==20)
	{
		if (buttonIndex==0) 
		{
			//if (m_blockBtn.hidden==YES) 
//			{	
			UnblockBtnclicked=YES;
				NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
				NSString *tmp_string=[prefs valueForKey:@"userName"];
				[l_FriendsIndicatorView startLoadingView:self];
				[l_requestObj TurnPingsOnOrOff:@selector(blockORunblockPingsCallBackMethod:responseData:) tempTarget:self userid:tmp_string friendid:m_Email pingStatus:@"On"];
			
			//}
			
		}	
	else if (buttonIndex==1)
	{
		UIAlertView *tmp_alertview=[[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete this friend?" message:nil
															 delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil ];
		[tmp_alertview show];
		[tmp_alertview setTag:11];
		[tmp_alertview release];
	}
}

	
}
#pragma mark -
#pragma mark Callback methods

#pragma mark -
#pragma mark Callback methods

-(void)requestCallBackMethodforGetItemsOfFriends:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSString *str;
	int  temp_responseCode=[responseCode intValue];
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
	NSLog(@"str :%@",str);
	
	
	if(temp_responseCode==200)
	{
		NSArray *favrotAry=[str JSONValue];
        NSLog(@"%@",favrotAry);
        for (int i=0; i<[favrotAry count]; i++)
        {
            NSDictionary *dict=[favrotAry objectAtIndex:i];
            
            if (![[dict objectForKey:@"buzzImageThumbUrl"] isEqualToString:@""])
            {
                [m_favoriteArray addObject:dict];
                NSLog(@"%@",m_favoriteArray);
            }
            
            
        }
        if (m_favoriteArray.count>0)
        {
            [m_favoriteArray reverse];
            if (m_favoriteArray.count>=6)
            {
                self.m_FavoritesyMore.hidden=NO;
            }
            else
            {
             self.m_FavoritesyMore.hidden=YES;
            }
            m_FavoriteHeaderLBL.hidden=NO;
            self.m_myfavoriteheader.hidden=NO;
            
            NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
            NSString *tmp_String=[prefs2 stringForKey:@"userName"];
            [l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetHeader:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_favoriteArray objectAtIndex:0]valueForKey:@"messageid"]intValue]];
            [self loadFavorites];
        }
        else{
            
            UILabel *lastLBL=(UILabel *)[self.m_FavoritesView viewWithTag:2211];
            if (lastLBL!=nil)
            {
                [lastLBL removeFromSuperview];
            }
            
            self.m_FavoritesyMore.hidden=YES;
            m_FavoriteHeaderLBL.hidden=YES;
            self.m_myfavoriteheader.hidden=YES;
        UILabel *lab=[[UILabel alloc]initWithFrame:CGRectMake(30, 110, 260, 50)];
        [lab setBackgroundColor:[UIColor clearColor]];
        [lab setText:@"There are no favorites right now"];
            [lab setTag:2211];
        [lab setNumberOfLines:0];
        [self.m_FavoritesView addSubview:lab];
        [lab release];
        }
		
	}
	else if([responseCode intValue]==401 || [responseCode intValue]!=-1)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	}
	else if ([responseCode intValue]==kLockedInfoErrorCode)
	{
		UIAlertView *tmp_AlertView=[[UIAlertView alloc] initWithTitle:kCannotViewFriendInfoTitle message:kCannotViewFriendInfoMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_AlertView show];
		[tmp_AlertView release];
		tmp_AlertView=nil;
	}
	
	
}




-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSLog(@"data downloaded");
	if ([responseCode intValue]==200)
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
        NSDictionary *tmp_dict = [[tmp_array objectAtIndex:0] valueForKey:@"wsMessage"];
		
        CategoryData *catData = [CategoryData convertMessageData:[tmp_array objectAtIndex:0]];
        DetailPageMessageController *detailController = [[[DetailPageMessageController alloc] init] autorelease];
        detailController.m_data = catData;
        
        if (!self.m_storyView.isHidden) {
            [detailController setMessagesIdsFromPopularPhotos:m_storyArray];
            detailController.m_Type = [[m_storyArray objectAtIndex:selectedIndex] objectForKey:@"messageType"];
        } else {
            [detailController setMessagesIdsFromWishListPhotos:m_favoriteArray];
            detailController.m_Type = [[m_favoriteArray objectAtIndex:selectedFavIndex] objectForKey:@"buzzType"];
        }
        
        detailController.m_messageId = [[tmp_dict objectForKey:@"id"] intValue];
        detailController.filter = POPULAR;
        
        NSDictionary *aData = [catData messageData:0];
        NSDictionary *tmp_Dict=[aData valueForKey:@"wsMessage"];
        NSString *custID=[tmp_Dict valueForKey:@"sentFromCustomer"];
        
        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
        if ([[custID lowercaseString] isEqualToString:[[prefs valueForKey:@"userName"] lowercaseString] ])
            detailController.mineTrack = YES;
        else
            detailController.mineTrack = NO;
        detailController.curPage = 1;
        detailController.m_CallBackViewController = self;
        [self.navigationController pushViewController:detailController animated:YES];
		
		
        if (tempString!=nil)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
    
    [l_FriendsIndicatorView stopLoadingView];;
}





- (void)requestCallBackMethodforPopularPhoto:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSString *str;
	str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
	NSArray *tempArray=[[str JSONValue] retain];
	NSLog(@"%@",tempArray);
	
	
	if ([responseCode intValue]==200)
	{
		NSDictionary *dict;
		for(int i=0;i<[tempArray count] || [m_storyArray count] == 6;i++)
		{
            NSString *msgType = [[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"] valueForKey:@"messageType"];
            
            if (![msgType isEqualToString:@"Q"]) {
                dict=[NSDictionary dictionaryWithObjectsAndKeys:[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"bodyMessage"],@"bodyMessage",[[tempArray objectAtIndex:i]valueForKey:@"buzzId"],@"buzzId",[[tempArray objectAtIndex:i]valueForKey:@"discountInfo"],@"discountInfo",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"elapsedTime"],@"elapsedTime",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"]valueForKey:@"id"],@"id",[[tempArray objectAtIndex:i]valueForKey:@"tagLine"],@"tagLine",[[tempArray objectAtIndex:i]valueForKey:@"productName"],@"productName",[[tempArray objectAtIndex:i]valueForKey:@"photoUrl"],@"photoUrl",[[[tempArray objectAtIndex:i]valueForKey:@"wsMessage"] valueForKey:@"messageType"],@"messageType",[[tempArray objectAtIndex:i]valueForKey:@"photoThumbUrl"],@"photoThumbUrl",nil];
                NSLog(@"%@",dict);
                [m_storyArray addObject:dict];
            }
        }
        if (m_storyArray.count>0)
        {
            NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
            NSString *tmp_String=[prefs2 stringForKey:@"userName"];
            [l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetHeader:responseData:) tempTarget:self userid:tmp_String messageId:[[[m_storyArray objectAtIndex:0]valueForKey:@"id"]intValue]];
            
            
            [self loadStoryView];
        }
    }
    
    else if([responseCode intValue]==kLockedInfoErrorCode)
	{
		//[l_loadingView stopLoadingView];
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kCannotViewFriendInfoTitle message:kCannotViewFriendInfoMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	
	else
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
    [l_FriendsIndicatorView stopLoadingView];
}


-(void)CallBackMethodForGetHeader:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSLog(@"data downloaded");
	if ([responseCode intValue]==200)
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
        NSDictionary *tmp_dict = [[tmp_array objectAtIndex:0] valueForKey:@"wsBuzz"];
        
        if (![[tmp_dict objectForKey:@"headline"] isEqualToString:@""])
        {
            if (m_StoriesBtn.selected)
            {
                self.m_storyHeaderLBL.text=[tmp_dict objectForKey:@"headline"];
            }
            else if(m_FavoritesBtn.selected)
            {
                self.m_FavoriteHeaderLBL.text=[tmp_dict objectForKey:@"headline"];
            }
        }
		
        if (tempString!=nil)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	[l_FriendsIndicatorView stopLoadingView];
}


-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_FriendsIndicatorView stopLoadingView];
	NSLog(@"---------- data downloaded --------------");
	
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(@"response string: %@",tempString);
	if ([responseCode intValue]==200) 
	{
		//NSLog(@"Response code: %d, response string:%@",[responseCode intValue],[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        

		if (isFriendsButtonClicked==YES) 
		{
			LevelTrackFriendsView++;
			NSArray *tempArray=[[tempString JSONValue] retain];
			l_SelectedFriendObj=[[SelectedFriendsListDisplay alloc] init];
			l_SelectedFriendObj.m_FriendsListArray=tempArray;
			l_SelectedFriendObj.LevelTrackSelectedFriend=LevelTrackFriendsView;
			[self.navigationController pushViewController:l_SelectedFriendObj animated:YES];
			LevelTrackFriendsView--;
			[l_SelectedFriendObj release];
			l_SelectedFriendObj=nil;
			isFriendsButtonClicked=NO;
			[tempArray release];
			tempArray=nil;
		}
		else
		{
            NSLog(@"-- Did receive data to load profile --");
			NSArray *tempArray=[[tempString JSONValue] retain];
			NSDictionary *tmp_dict=[tempArray objectAtIndex:0];
			self.m_YourFriendNameString=[tmp_dict valueForKey:@"firstName"];
			NSString *tmp_isFriendString=[tmp_dict valueForKey:@"isFriend"];
			NSString *tmp_FollowingString=[tmp_dict valueForKey:@"isFollowing"];
			NSString *tmp_FriendRequestStatus=[tmp_dict valueForKey:@"friendRequestType"];
			m_PingStatus=[tmp_dict valueForKey:@"pingStatus"];
			m_friendNameString=[[tmp_dict valueForKey:@"emailid"] retain];
		
            m_cityLBL.text=[NSString stringWithFormat:@"%@",[tmp_dict valueForKey:@"city"] ];
            
            
            m_aboutme.text=[NSString stringWithFormat:@"%@",[tmp_dict valueForKey:@"aboutMe"]];
            
            m_favoriteLBL.text=[NSString stringWithFormat:@"%@",[tmp_dict valueForKey:@"favQuotes"]];
            
            favBrandLabel.text=[NSString stringWithFormat:@"%@",[tmp_dict valueForKey:@"favBrand"]];
            interestsLabel.text=[NSString stringWithFormat:@"%@",[tmp_dict valueForKey:@"interest"]];
            
			// in case of photos been locked by the user show the photos locked
            
            
            
			if ([[tmp_dict valueForKey:@"photosStatus"] isEqualToString:@"Locked"]) 
			{
				m_photosLabel.text=@"N/A";
				m_Photos=-1;
			}
			else 
			{
                
                
				m_photosLabel.text=[NSString stringWithFormat:@"%@ Stories",[tmp_dict valueForKey:@"photosCount"]];
				m_Photos=[[tmp_dict valueForKey:@"photosCount"] intValue];
			}
			// in case the friends have been locked by the user ,show locked there
			if ([[tmp_dict valueForKey:@"friendsStatus"] isEqualToString:@"Locked"]) 
			{
				m_friendsLabel.text=@"N/A";
				m_friendCount=-1;
			}
			else
			{
				m_friendsLabel.text=[NSString stringWithFormat:@"%@ Friends",[tmp_dict valueForKey:@"friendsCount"]];
				m_friendCount=[[tmp_dict valueForKey:@"friendsCount"] intValue];
			}
			// in case the wishlist has been locked by the user show locked
			if ([[tmp_dict valueForKey:@"wishlistStatus"] isEqualToString:@"Locked"]) 
			{
				m_wishListLabel.text=[NSString stringWithFormat:@"N/A"];
				m_WishListCount=-1;
			}
			else 
			{
				m_WishListCount=[[tmp_dict valueForKey:@"wishListCount"] intValue];
				m_wishListLabel.text=[NSString stringWithFormat:@"%@ Favorites",[tmp_dict valueForKey:@"wishListCount"]];
			}
				// in case the following has been locked by the user,show following as locked
			if ([[tmp_dict valueForKey:@"followingStatus"]isEqualToString:@"Locked"]) 
			{
				[m_followingBtn setTitle:@"Following: N/A" forState:UIControlStateNormal];
				m_following=-1;
                m_followingBtn.titleLabel.textColor=[UIColor colorWithRed:55/255.0 green:153/255.0 blue:200/255.0 alpha:1.0];
			}
			else 
			{
				[m_followingBtn setTitle:[NSString stringWithFormat:@"Following: %@",[tmp_dict valueForKey:@"followingCount"]] forState:UIControlStateNormal];
                m_followingBtn.titleLabel.textColor=[UIColor colorWithRed:55/255.0 green:153/255.0 blue:200/255.0 alpha:1.0];
				m_following=[[tmp_dict valueForKey:@"followingCount"] intValue];
			}
			if ([[tmp_dict valueForKey:@"aboutMe"]isEqualToString:@"Locked"])
			{
				m_TableView.hidden=YES;
				m_label3.hidden=YES;
                m_ScrollView.contentSize = CGSizeMake(m_ScrollView.frame.size.width,[UIScreen mainScreen].bounds.size.height );
			}

			m_profileSettings=[[NSArray alloc] initWithObjects: [tmp_dict valueForKey:@"aboutMe"],
							   [tmp_dict valueForKey:@"interest"],
							   [tmp_dict valueForKey:@"favBrand"],
							   [tmp_dict valueForKey:@"favQuotes"],nil];
			
			[m_followerBtn setTitle:[NSString stringWithFormat:@"Followers: %@",[tmp_dict valueForKey:@"followersCount"]] forState:UIControlStateNormal];
            m_followerBtn.titleLabel.textColor=[UIColor colorWithRed:55/255.0 green:153/255.0 blue:200/255.0 alpha:1.0];;

			m_followers=[[tmp_dict valueForKey:@"followersCount"] intValue];
			
			NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,
								   [tmp_dict valueForKey:@"profilePicUrl"]];
			NSURL *tempLoadingUrl = [NSURL URLWithString:temp_strUrl];

            [userPortrait resetButton];
            userPortrait.maskImage = YES;
            userPortrait.userInteractionEnabled = NO;
			[userPortrait loadImageFromURL:tempLoadingUrl target:nil action:nil btnText:@"" ignoreCaching:NO];

			[self.m_TableView reloadData];
			[tempArray release];
			tempArray=nil;
			
			NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
			NSString *loginUserName=[prefs valueForKey:@"userName"];
			
			
			if (LevelTrackFriendsView<2 && ![loginUserName isEqualToString:m_Email]) 
			{
				if ([tmp_isFriendString isEqualToString:@"N"] && [tmp_FriendRequestStatus isEqualToString:@"pending"]) 
				{
					m_addFriendButton.hidden=YES;
					m_FriendRequestPendingBtn.hidden=NO;
					
				}
				else if ([tmp_isFriendString isEqualToString:@"N"] && ![tmp_FriendRequestStatus isEqualToString:@"pending"]&&![tmp_FriendRequestStatus isEqualToString:@"acceptOReject"]) 
				{
					m_addFriendButton.hidden=NO;
					m_FriendRequestPendingBtn.hidden=YES;
					m_blockBtn.hidden=YES;
					m_unblockBtn.hidden=YES;
				}
				else if ([tmp_FriendRequestStatus isEqualToString:@"acceptOReject"]) 
				{
					m_addFriendButton.hidden=YES;
					m_blockBtn.hidden=YES;
					m_unblockBtn.hidden=YES;
					m_FriendRequestPendingBtn.hidden=YES;
					
				}
				else 
				{
					m_addFriendButton.hidden=YES;
					
					if ([m_PingStatus isEqualToString:@"On"])
					{
						m_unblockBtn.hidden=YES;
						m_blockBtn.hidden=NO;
					}
					else 
					{
						m_unblockBtn.hidden=NO;
						m_blockBtn.hidden=YES;
					}
                    self.messageLayout = YES;

				}
				
				if ([tmp_FollowingString isEqualToString:@"N"]) 
				{
					m_lblFollowStatusLine.text=@"You are not following this user";
					m_followBtn.hidden=NO;
					m_unfollowBtn.hidden=YES;
				}
				else
				{
					m_lblFollowStatusLine.text=@"You are following this user";
					m_followBtn.hidden=YES;
					m_unfollowBtn.hidden=NO;
				}
			}
			else if ([loginUserName isEqualToString:m_Email]) 
			{
				m_lblFollowStatusLine.hidden=YES;
				[m_followBtnImageView setFrame:CGRectMake(11, 233, 302, 73)];
				[m_label3 setFrame:CGRectMake(3, 313, 320, 23)];
				[m_TableView setFrame:CGRectMake(1, 340, 320, 204)];
				[m_followingBtn setFrame:CGRectMake(31, 240, 240, 27)];
				[m_followerBtn setFrame:CGRectMake(31, 275, 240, 27)];
				m_followBtn.hidden=YES;
				m_unfollowBtn.hidden=YES;
				m_blockBtn.hidden=YES;
				m_unblockBtn.hidden=YES;
				
			}
			else 
			{
				m_followBtn.hidden=YES;
				m_unfollowBtn.hidden=YES;
				m_blockBtn.hidden=YES;
				m_unblockBtn.hidden=YES;
			}
			
			
		}
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
		isFriendsButtonClicked=NO;
	}
	else if ([responseCode intValue]==kLockedInfoErrorCode) // in case th info has been locked by the other user,an alert is shown here.
	{
		UIAlertView *tmp_ErrorAlert=[[UIAlertView alloc] initWithTitle:nil message:kCannotViewFriendInfoMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_ErrorAlert show];
		[tmp_ErrorAlert release];
		tmp_ErrorAlert=nil;
		isFriendsButtonClicked=NO;
	}
 else if([responseCode intValue]!=-1)
	{
		/*UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;*/
        [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
		isFriendsButtonClicked=NO;
	}
	[tempString  release];
	tempString=nil;
	
}	
-(void)FollowsCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_FriendsIndicatorView stopLoadingView];
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	if ([responseCode intValue]==200) 
	{
		if (m_checkFollowWebService==1) 
		{	// for followers webservice
            
            FollowingMeViewController *Vc=[[FollowingMeViewController alloc]initWithNibName:@"FollowingMeViewController" bundle:[NSBundle mainBundle]];
            LevelTrackFriendsView++;
            Vc.m_followArray=[[tempString JSONValue]retain];
            Vc.m_FollowersTrack=LevelTrackFriendsView;
            [self.navigationController pushViewController:Vc animated:YES];
            LevelTrackFriendsView--;
            [Vc release];
            
			m_checkFollowWebService=0;
            
            
            
//			l_FansView=[[FansInfoViewController alloc] init];
//			l_FansView.Follows=1;
//			LevelTrackFriendsView++;
//			l_FansView.m_followArray=[[tempString JSONValue] retain];
//			l_FansView.m_FollowersTrack=LevelTrackFriendsView;
//			l_FansView.m_followerBtnClicked=YES;
//			[self.navigationController pushViewController:l_FansView animated:YES];
//			LevelTrackFriendsView--;
//			[l_FansView release];
//			l_FansView=nil;
//			m_checkFollowWebService=0;
			//NSLog(@"Response code: %d, response string:%@",[responseCode intValue],[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
		}
		else if(m_checkFollowWebService==-1)
		{	// for following webservice
            
            IMFollowingViewController *Vc=[[IMFollowingViewController alloc]initWithNibName:@"IMFollowingViewController" bundle:[NSBundle mainBundle]];
            LevelTrackFriendsView++;
            Vc.m_followArray=[[tempString JSONValue]retain];
            Vc.m_FollowersTrack=LevelTrackFriendsView;
            [self.navigationController pushViewController:Vc animated:YES];
            LevelTrackFriendsView--;
            [Vc release];
            
			m_checkFollowWebService=0;
            
            
            
//			l_FansView=[[FansInfoViewController alloc] init];
//			l_FansView.Follows=2;
//			LevelTrackFriendsView++;
//			l_FansView.m_FollowersTrack=LevelTrackFriendsView;
//			l_FansView.intUnfollowTrack=1;//statically passed
//			l_FansView.m_followArray=[[tempString JSONValue] retain];
//			[self.navigationController pushViewController:l_FansView animated:YES];
//			LevelTrackFriendsView--;
//			[l_FansView release];
//			l_FansView=nil;
//			m_checkFollowWebService=0;
		}
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	}
	
	[tempString release];
	tempString=nil;
	
	
} 
-(void)DeleteFriendCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_FriendsIndicatorView stopLoadingView];
	NSLog(@"data downloaded");
	NSInteger ResponseCode=[responseCode integerValue];
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	if (ResponseCode==200) 
	{
		UIAlertView *tmp_alertview=[[UIAlertView alloc] initWithTitle:@"Goodbye Friend" message:nil 
															 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_alertview show];
		[tmp_alertview release];
		m_unblockBtn.hidden=YES;
		m_blockBtn.hidden=YES;
		m_addFriendButton.hidden=NO;
	}
	else  {
        NSLog(@"ViewAFriend response: %d", [responseCode intValue]);
        [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
    }
	[tempString release];
	tempString=nil;
	
}
-(void)AddFriendCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_FriendsIndicatorView stopLoadingView];
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	if ([responseCode intValue]==200) 
	{
		UIAlertView *tmp_alertview=[[UIAlertView alloc] initWithTitle:@"Friend request sent" message:@"" 
															 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_alertview show];
		[tmp_alertview release];
		
		m_addFriendButton.hidden=YES;
		m_FriendRequestPendingBtn.hidden=NO;
	}
	else {
        NSLog(@"ViewAFriend response: %d", [responseCode intValue]);
        [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
    }
}
-(void)blockORunblockPingsCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_FriendsIndicatorView stopLoadingView];
	if ([responseCode intValue]==200)
	{
		if (UnblockBtnclicked==NO) 
		{
			UIAlertView *tmp_alertview=[[UIAlertView alloc] initWithTitle:@"Ping is turned off" message:nil
																 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[tmp_alertview show];
			[tmp_alertview setTag:10];
			[tmp_alertview release];	
			m_blockBtn.hidden=YES;
			m_unblockBtn.hidden=NO;
			
		}
		else 
		{
			m_blockBtn.hidden=NO;
			m_unblockBtn.hidden=YES;
			UnblockBtnclicked=NO;
			
		}
	}
	else  {
        NSLog(@"ViewAFriend response: %d", [responseCode intValue]);
        [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
    }
}
-(void)FollowAndUnfollowCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	[l_FriendsIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) 
	{
		NSString *message = @"";
		if (FollowBtnClicked==YES) 
		{
			m_lblFollowStatusLine.text=@"You are following this user";
			m_followBtn.hidden=YES;
			m_unfollowBtn.hidden=NO;
			message = @"You are now following successfully";
		}
		else 
		{
			m_lblFollowStatusLine.text=@"You are not following this user";
			m_followBtn.hidden=NO;
			m_unfollowBtn.hidden=YES;
			message = @"You are no longer following";
		}
		FollowBtnClicked=NO;
		
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:message message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;

	}
	else  {
        NSLog(@"ViewAFriend response: %d", [responseCode intValue]);
        [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
    }
}
-(void)requestCallBackMethodForAcceptOrReject:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	[l_FriendsIndicatorView stopLoadingView];
	if ([responseCode integerValue]==200) 
	{
		m_addFriendButton.hidden=NO;
		m_FriendRequestPendingBtn.hidden=YES;
	}
	else  {
        NSLog(@"ViewAFriend response: %d", [responseCode intValue]);
        [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
    }
}
#pragma mark alertview methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
		if (alertView.tag==10)
		{
			if (buttonIndex==0) 
			{	
			
				m_blockBtn.hidden=YES;
				m_unblockBtn.hidden=NO;
			}
		}
		else if(alertView.tag==11)
		{
			if (buttonIndex==0) {
				
			}
			
			 else if (buttonIndex==1) {
				 [self deleteAFriendAction];	
			}
			
		}
	
	}


#pragma mark -
-(void)viewDidAppear:(BOOL)animated
{
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
    [self getparticularBtuuonDetails:nil];

    NSString *messageStr=[NSString stringWithFormat:@"Tiny News only allows friends to read and leave messages. Add %@ as a friend today.",m_strLbl1];
    
    UILabel *m_messageLBL=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 100)];
    [m_messageLBL setBackgroundColor:[UIColor clearColor]];
    [m_messageLBL setText:messageStr];
    [m_messageLBL setFont:[UIFont fontWithName:@"Helvetica" size:17.0f]];
    [m_messageLBL setNumberOfLines:0];
    [m_messageLBL setTag:11];
    [self.m_commentsView addSubview:m_messageLBL];
    [m_messageLBL release];
    
    
	l_FriendsIndicatorView = [LoadingIndicatorView SharedInstance];
	m_unblockBtn.hidden=YES;
	//NSString *tmp_personName;
	m_unfollowBtn.hidden=YES;
	l_requestObj=[[ShopbeeAPIs alloc] init];
	//NSLog(@"%@",m_strLbl1);
	
	if (m_strLbl1.length==0)
	{
		m_label1.text = @"No Name";
		m_label2.text = @"No Name";	
		m_label3.text = @"No Name";
		
	}
	else {
		m_label1.text = m_strLbl1;
		m_label2.text=m_strLbl1;
		m_label3.text=[NSString stringWithFormat:@"   %@'s Personal info",m_strLbl1];
		
	}
	m_PicView.image=m_personImage;
	m_theProfileArray=[[NSArray alloc] initWithObjects:@"About me:",@"Interests:",@"Favorite brand:",@"Favorite quotation:",nil];
	m_ScrollView.contentSize = CGSizeMake(m_ScrollView.frame.size.width,[UIScreen mainScreen].bounds.size.height );
	m_addFriendButton.hidden=YES;
    [self.m_meView setContentOffset:CGPointMake(0, 0)];
    self.m_meView.contentSize = CGSizeMake(320, 420);
    self.m_meView.clipsToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated {

    selectedIndex=-1;
    
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_string=[prefs valueForKey:@"userName"];
	[l_FriendsIndicatorView startLoadingView:self];
	[l_requestObj getUserInfo:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:tmp_string loginId:m_Email];
	
	if (LevelTrackFriendsView==1) 
	{
		m_blockBtn.hidden=YES;
		m_unblockBtn.hidden=YES;
	}
    
    

}

- (BOOL) messageLayout
{
    return messageLayout;
}

- (void) setMessageLayout:(BOOL) value
{
    messageLayout = value;
    if (messageLayout) {
        [[MessageBoardManager sharedManager] getMessageBoardId:self custID:m_Email];
    } else {
        [self.m_commentsButton setHidden:YES];
       // [self.m_commentsView setHidden:NO];
        [self.m_addCommentButton setHidden:YES];
        [self.m_chatView setHidden:YES];
    }
	
}

- (void) setMessageId:(NSString *) value
{
    if (!value) {
        self.messageLayout = NO;
    }
    self.m_friendMessageID = value;
    
    [[MessageBoardManager sharedManager] getComments:self custID:m_Email messageID:m_friendMessageID];
}

- (void)viewWillDisappear:(BOOL)animated
{
	
}

#pragma mark Comments

#if 0
- (void) presentComments
{
    [self.m_commentsView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];   
    
	self.m_commentsView.scrollsToTop = NO;
	self.m_commentsView.contentOffset = CGPointMake(0,0);
	CGFloat imgViewY=0;
	
	int counter;
	
	NSInteger Count = [self.m_commentsArray count];
	counter=0;
	for(int a=0;a<Count;a++)
	{
		NSString	*temp_string=[[self.m_commentsArray objectAtIndex:a] valueForKey:@"responseMessageBody"];
		if (![temp_string isEqualToString:@""]) 
		{
			
            UIView *temp_imgView2=[[UIView alloc]initWithFrame:CGRectMake(0, imgViewY, 274, 59)];
			temp_imgView2.tag = 32;
			
			UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(40, 0, 210, 40)];
			textView.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
			//textView.font = [UIFont fontWithName:kFontName size:13];
			[textView setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_SIZE_KEY]]];
			textView.backgroundColor = [UIColor clearColor];
			textView.delegate = nil;
			textView.tag = 33;
			textView.text=temp_string;
			textView.returnKeyType = UIReturnKeyDefault;
			textView.keyboardType = UIKeyboardTypeDefault;
			textView.scrollEnabled = YES;
			textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
			textView.editable=YES;
			textView.userInteractionEnabled=NO;
			
			
			if (counter%2==0)
			{
				temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.frame.size.height);
				//temp_imgView2.image=[[UIImage imageNamed:@"white_message_box.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
			}
			else
			{
				imgViewY=imgViewY-25;
				temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.frame.size.height);
				//temp_imgView2.image=[[UIImage imageNamed:@"white_message_box_rotated.png"] stretchableImageWithLeftCapWidth:50 topCapHeight:30];
			}
			
            
			[temp_imgView2 addSubview:textView];
			
			[self.m_commentsView addSubview:temp_imgView2];
			
			temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.contentSize.height);
			
			
			textView.frame =CGRectMake(40, 0, 210,textView.contentSize.height);
			
			if (counter%2!=0)
			{
				textView.frame =CGRectMake(40, 12, 210,textView.contentSize.height);
			}
			
			
			UILabel *tmp_elapsedTime=[[UILabel alloc] initWithFrame:CGRectMake(40, textView.contentSize.height -5, 190, 12)];
			
			if (counter%2!=0)
			{
				tmp_elapsedTime.frame=CGRectMake(40, textView.contentSize.height +8, 190, 12);
				
			}
			
			//Bharat: DE116: Commented out for now (so that user does not see the timeline nad reject app for old updates)
			//tmp_elapsedTime.text=[NSString stringWithFormat:@"Added %@ by %@ ",[[messageResponseArray objectAtIndex:a]valueForKey:@"elapsedTime"],[[messageResponseArray objectAtIndex:a]valueForKey:@"responseCustomerName"]];
			tmp_elapsedTime.text=[NSString stringWithFormat:@"%@ %@ ",[[self.m_commentsArray objectAtIndex:a]valueForKey:@"responseCustomerName"],[[self.m_commentsArray objectAtIndex:a]valueForKey:@"elapsedTime"]];
			tmp_elapsedTime.backgroundColor=[UIColor clearColor];
			tmp_elapsedTime.textColor=[UIColor lightGrayColor];
			tmp_elapsedTime.font=[UIFont fontWithName:kFontName size:10];
			
			[temp_imgView2 addSubview:tmp_elapsedTime];
			//[tmp_elapsedTime setFrame:CGRectMake(30, textView.contentSize.height -5, 190, 12)];
			
            
			
			imgViewY=imgViewY+textView.contentSize.height+40;
			
            AsyncImageView* SenderImage = [[[AsyncImageView alloc]
                                            initWithFrame:CGRectMake(5,5,35,35)] autorelease];
			
			if (counter%2!=0)
			{
				SenderImage.frame=CGRectMake(5, 20, 35, 35);
			}
			
			[temp_imgView2 addSubview:SenderImage];
			NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[self.m_commentsArray objectAtIndex:a] valueForKey:@"thumbCustomerUrl"]];
			NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,60,32]];
			[SenderImage loadImageFromURL:tempLoadingUrl ];
			
			
            UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(0, textView.contentSize.height+tmp_elapsedTime.frame.size.height+2, 320 , 1)];
			
			if (counter%2!=0)
			{
				lbl.frame=CGRectMake(0, textView.contentSize.height+tmp_elapsedTime.frame.size.height+8, 320, 1);
				
			}
            [lbl setBackgroundColor:[UIColor lightGrayColor]];
            [temp_imgView2 addSubview:lbl];
            [lbl release];
            
			NSLog(@"imgViewY>>>>>>>>>>>>>>>>%f",imgViewY);
			//textViewY=textViewY+4;
			[temp_imgView2 release];
			temp_imgView2=nil;
			[textView release];
			textView=nil;
			[tmp_elapsedTime release];
			tmp_elapsedTime=nil;
            
            
			
			counter++;
		}
	}
	//Bharat: 11/26/11: Set absolute value, do not set increments
	self.m_commentsView.contentSize = CGSizeMake(m_commentsView.contentSize.width,imgViewY+10);
}
#else
- (void) presentComments
{
    [self.m_commentsView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
	self.m_commentsView.scrollsToTop = NO;
	self.m_commentsView.contentOffset = CGPointMake(0,0);
	CGFloat imgViewY=0;
	
	int counter;
	
	NSInteger Count = [self.m_commentsArray count];
    
    // NSLog(@"comment array %@",self.m_commentsArray);
    
    
	counter=0;
	for(int a=0;a<Count;a++)
	{
		
		
		NSString	*temp_string=[[self.m_commentsArray objectAtIndex:a] valueForKey:@"responseMessageBody"];
		if (![temp_string isEqualToString:@""])
		{
			UIView *temp_imgView2=[[UIView alloc]initWithFrame:CGRectMake(0, imgViewY, 310, 59)];
			temp_imgView2.tag = 32;
			
            // ================= text view with comment
			UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(40, 5, 210, 40)];
			textView.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
			//textView.font = [UIFont fontWithName:kFontName size:13];
			[textView setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_SIZE_KEY]]];
			textView.backgroundColor = [UIColor clearColor];
			textView.delegate = nil;
			textView.tag = 33;
			textView.text=temp_string;
			textView.returnKeyType = UIReturnKeyDefault;
			textView.keyboardType = UIKeyboardTypeDefault;
			textView.scrollEnabled = YES;
			textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
			textView.editable=YES;
			textView.userInteractionEnabled=NO;
			
			[temp_imgView2 addSubview:textView];
			
			
			temp_imgView2.frame=CGRectMake(0, imgViewY, 310, 25+textView.contentSize.height);
			
			textView.frame =CGRectMake(40, 5, 210,textView.contentSize.height);
			[temp_imgView2 addSubview:textView];
			
			UILabel *tmp_elapsedTime=[[UILabel alloc] initWithFrame:CGRectMake(47, textView.contentSize.height, 190, 12)];
			
			if (counter%2!=0)
			{
				tmp_elapsedTime.frame=CGRectMake(47, textView.contentSize.height, 190, 12);
				
			}
			

			tmp_elapsedTime.text=[NSString stringWithFormat:@"%@ %@ ",[[self.m_commentsArray objectAtIndex:a]valueForKey:@"responseCustomerName"],[[self.m_commentsArray objectAtIndex:a]valueForKey:@"elapsedTime"]];
			tmp_elapsedTime.backgroundColor=[UIColor clearColor];
			tmp_elapsedTime.textColor=[UIColor lightGrayColor];
			tmp_elapsedTime.font=[UIFont fontWithName:kFontName size:10];
			
			[temp_imgView2 addSubview:tmp_elapsedTime];
            
            CGRect newframe=[temp_imgView2  frame];
            
            AsyncButtonView* SenderImage = [[[AsyncButtonView alloc]
                                             initWithFrame:CGRectMake(5,(newframe.size.height-35)/2,35,35)] autorelease];
			
			[temp_imgView2 addSubview:SenderImage];
			NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[self.m_commentsArray objectAtIndex:a] valueForKey:@"thumbCustomerUrl"]];
			NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,60,32]];
            SenderImage.cropImage = YES;
            SenderImage.maskImage = YES;
            SenderImage.messageTag = a;
			[SenderImage loadImageFromURL:tempLoadingUrl target:self action:@selector(commenterImagePressed:) btnText:@""];
			
			
            UIView *lbl=[[UIView alloc] initWithFrame:CGRectMake(0, temp_imgView2.frame.size.height, 320 , 1)];
			

            [lbl setBackgroundColor:[UIColor lightGrayColor]];
            [temp_imgView2 addSubview:lbl];
            imgViewY = temp_imgView2.frame.origin.y + temp_imgView2.frame.size.height;
            [lbl release];
			[self.m_commentsView addSubview:temp_imgView2];
			
			[self.m_commentsView addSubview:temp_imgView2];

            imgViewY = temp_imgView2.frame.origin.y + temp_imgView2.frame.size.height;
			//textViewY=textViewY+4;
			[temp_imgView2 release];
			temp_imgView2=nil;
			[textView release];
			textView=nil;
			[tmp_elapsedTime release];
			tmp_elapsedTime=nil;
            
			counter++;
		}
	}
	//Bharat: 11/26/11: Set absolute value, do not set increments
	self.m_commentsView.contentSize = CGSizeMake(m_commentsView.contentSize.width,imgViewY+10);
}
#endif

- (void)commenterImagePressed:(id)sender
{
    ViewAFriendViewController *tempViewAFriend=[[ViewAFriendViewController alloc]init];
    NSDictionary *responseDict = [self.m_commentsArray objectAtIndex:((MoreButton *)sender).tag];
    
    tempViewAFriend.m_Email=[responseDict valueForKey:@"responseCustomerId"];
    
    // If it's my post - don't let click on it
    if ([tempViewAFriend.m_Email isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"]]) {
        return;
    }
    tempViewAFriend.m_strLbl1=[responseDict valueForKey:@"responseCustomerName"];
    tempViewAFriend.messageLayout = NO;
    [self.navigationController pushViewController:tempViewAFriend animated:YES];
    [tempViewAFriend release];
    tempViewAFriend=nil;
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"MyPage"
                                                    withAction:@"commenterImagePressed"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (void) setCommentsArray:(NSArray *)anArray
{
    if (!anArray) {
        return;
    }
    
    [self.m_commentsButton setHidden:NO];
    [self.m_addCommentButton setHidden:NO];
//    [self.m_chatView setHidden:NO];
//    [self.m_commentsView setHidden:NO];

    UILabel *m_message=(UILabel *)[self.m_commentsView viewWithTag:11];
    if (m_message!=nil)
    {
        [m_message removeFromSuperview];
    }

    self.m_dataArray = anArray;
    self.m_commentsArray = [[[self.m_dataArray objectAtIndex:0] valueForKey:@"messageResponseList"] reversedArray];
    [self presentComments];
}

- (IBAction)addComment:(id)sender
{
	BuyItCommentViewController *commentController = [[BuyItCommentViewController alloc] init];
    
    // GDL: I was releasing one of the components of m_MainArray.
	commentController.m_MainArray = self.m_dataArray;
	commentController.m_FavourString = @"";
	commentController.m_CallBackViewController = self;//**
    commentController.m_personImage = m_personImage;
	[self.navigationController pushViewController:commentController animated:YES];
	[commentController release];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

// GDL: Modified everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_label1 = nil;
    self.m_label2 = nil;
    self.m_label3 = nil;
    self.m_PicView = nil;
    self.m_blockBtn = nil;
    self.m_unblockBtn = nil;
    self.m_TableView = nil;
    self.m_ScrollView = nil;
    self.m_imageView = nil;
    self.m_Email = nil;
    self.m_photosLabel = nil;
    self.m_friendsLabel = nil;
    self.m_wishListLabel = nil;
    self.m_followBtn = nil;
    self.m_unfollowBtn = nil;
    self.m_followingBtn = nil;
    self.m_followerBtn = nil;
    self.m_addFriendButton = nil;
    self.m_lblFollowStatusLine = nil;
    self.m_FriendRequestPendingBtn = nil;
    self.m_RequestTypeString = nil;
    self.m_followBtnImageView = nil;
    self.m_cityLBL=nil;
    self.m_meBtn  = nil;
    self.m_messageBtn=nil;
    self.m_StoriesBtn=nil;
    self.m_FavoritesBtn=nil;
    self.m_favoriteLBL=nil;
    self.m_aboutme=nil;
    self.m_chatView=nil;
    self.m_meView=nil;
    self.m_storyView=nil;
    self.m_storyHeaderLBL=nil;
    self.m_storyMore=nil;
    self.m_FavoritesView=nil;
    self.m_FavoriteHeaderLBL=nil;
    self.m_FavoritesyMore=nil;
    self.m_mystoryheader=nil;
    self.m_myfavoriteheader=nil;
    self.interestsLabel = nil;
    self.favBrandLabel = nil;

}

- (void)dealloc {
    [m_label1 release];
    [m_label2 release];
    [m_label3 release];
    [m_PicView release];
	[m_blockBtn release];
	[m_unblockBtn release];
    [m_strLbl1 release];
    [m_personImage release];
    [m_TableView release];
    [m_theProfileArray release];
    [m_profileSettings release];
    [m_ScrollView release];
    [m_imageView release];
    [m_Email release];
    [m_photosLabel release];
    [m_friendsLabel release];
    [m_wishListLabel release];
    [m_followBtn release];
    [m_unfollowBtn release];
    [m_followingBtn release];
    [m_followerBtn release];
    [m_addFriendButton release];
    [m_friendNameString release];
    //[m_PingStatus release];
    [m_lblFollowStatusLine release];
    [m_FriendRequestPendingBtn release];
    [m_RequestTypeString release];
    [m_followBtnImageView release];
    [m_YourFriendNameString release];
    [m_friendMessageID release];
    [m_commentsView release];
    [m_commentsArray release];
    [m_profileInfoBtn release];
    [m_commentsButton release];
    [m_addCommentButton release];
    [m_cityLBL release];
    [m_meBtn release];
    [m_messageBtn release];
    [m_FavoritesBtn release];
    [m_StoriesBtn release];
    [m_aboutme release];
    [m_favoriteLBL release];
    [m_chatView release];
    [m_meView release];
    [m_storyView release];
    [m_storyHeaderLBL release];
    [m_storyMore release];
    [m_FavoritesView release];
    [m_FavoriteHeaderLBL release];
    [m_FavoritesyMore release];
    [m_mystoryheader release];
    [m_myfavoriteheader release];
    [userPortrait release];
    [favBrandLabel release];
    [interestsLabel release];
	[super dealloc];
}


@end
