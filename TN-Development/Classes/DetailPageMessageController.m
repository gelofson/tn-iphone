//
//  DetailPageViewController.m
//  TinyNews
//
//  Created by Nava Carmon on 20/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DetailPageMessageController.h"
#import "Constants.h"
#import "NewsDataManager.h"
#import "QNavigatorAppDelegate.h"
#import "UIView+Origin.h"
#import "CommentViewController.h"
#import "LoadingIndicatorView.h"
#import "ViewAFriendViewController.h"
#import "AsyncButtonView.h"
#import "MoreButton.h"
#import "NewsMoreViewController.h"
#import "Context.h"
#import "FontsDef.h"

@interface DetailPageMessageController ()
- (NSString *) formatWhoWhatText:(NSDictionary *)dataDict;
- (void) setFollowButtonStatus:(NSDictionary *)dict;
- (void) setLikeButtonsStatus:(NSDictionary *)dict;
- (void) FindCurrentIndexInTheArray; 
- (void) configureView;
-(void) showHideArrowButton; 
- (void) setViewData;
-(void)addToWishListPublic:(NSDictionary *)aData;
-(void)addToWishListPrivate:(NSDictionary *)aData;


@end

@implementation DetailPageMessageController

@synthesize m_textViewSubject,m_whoWhatTextView;
@synthesize m_headingLabel, m_likeLabel,m_originatorNameLabel;
@synthesize m_storyImageView, m_userImageView;
@synthesize m_scrollView, m_customTabbar, commentsGroup, m_dislikeLabel;
@synthesize m_commentsLabel, allCommentsView, commentsView;
@synthesize followButton, likeButton, dislikeButton, commentButton;
@synthesize viewAllCommentButton, reply, m_messageId, m_headerLabel, m_userId, m_Type, statusesArray;
@synthesize firstPictureID, m_data, filter, m_messageIDsArray, m_currentMessageID, curPage;
@synthesize m_leftArrow, m_rightArrow, m_ThePhotoView, m_ImageView, commentGroupMine;
@synthesize mineTrack, m_mineUserImageView, m_mineOriginatorNameLabel, m_mineTextViewSubject, fetchingData;
@synthesize m_CallBackViewController, clipButton, videoWebView, theWebView;
@synthesize viaLabel, linkButton, storyUrl;
#define SPICTURE_SIZE    300

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.trackedViewName = @"Detail Page From News Page";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.m_headerLabel setFont:[UIFont fontWithName:kMyriadProRegularFont size:18]];
	
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	self.m_userId=[prefs valueForKey:@"userName"];
    self.statusesArray = [[[NSMutableArray alloc] init] autorelease];
	UISwipeGestureRecognizer *recognizer; // To detect the swipe on the scroll view.
	
	self.m_scrollView.scrollsToTop = NO;
	self.m_scrollView.contentOffset = CGPointMake(0,0);
	self.m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width,m_scrollView.frame.size.height);//-50);
	
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	[self.m_scrollView addGestureRecognizer:recognizer];
	recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:UISwipeGestureRecognizerDirectionRight];
	[self.m_scrollView addGestureRecognizer:recognizer];
    
    GetPinnFlag = YES;
}

- (NSMutableArray *) m_messageIDsArray
{
    if (!m_messageIDsArray) {
        self.m_messageIDsArray = [[[NSMutableArray alloc] init] autorelease];
    }
    
    return m_messageIDsArray;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
	self.m_scrollView.contentOffset = CGPointMake(0,0);
    
    if (mineTrack) {
        [commentGroupMine setHidden:NO];
        [commentsGroup setHidden:YES];
        [commentsView setHidden:NO];
        [self.clipButton setHidden:YES];
    } else {
        [commentGroupMine setHidden:YES];
        [commentsGroup setHidden:NO];
        [commentsView setHidden:NO];
        [self.clipButton setHidden:NO];
    }
    
    numPages = 1;
    totalRecords = [self.m_messageIDsArray count];
    [self FindCurrentIndexInTheArray];
    m_headerLabel.text=self.m_Type;
    //[self setViewData];
    moreTrack = [self.m_CallBackViewController isKindOfClass:[NewsMoreViewController class]];
    [self configureView];
	[self showHideArrowButton];
    
    if (fetchingData) {
        [[LoadingIndicatorView SharedInstance] startLoadingView:self];
    }
}

- (void) setMessagesIdsFromPopularPhotos:(NSArray *)popularPhotosArray
{
    for (NSDictionary *dict in popularPhotosArray) {
        [self.m_messageIDsArray addObject:[dict valueForKey:@"id"]];
    }
}

- (void) setMessagesIdsFromWishListPhotos:(NSArray *)popularPhotosArray
{
    for (NSDictionary *dict in popularPhotosArray) {
        [self.m_messageIDsArray addObject:[dict valueForKey:@"messageid"]];
    }
}

- (void) setMessagesIdsFromWishListData:(NSDictionary *)dict
{
    [self.m_messageIDsArray addObject:[dict valueForKey:@"messageid"]];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) FindCurrentIndexInTheArray 
{
	self.m_currentMessageID=[self.m_messageIDsArray indexOfObject:[NSNumber numberWithInt:m_messageId] ];
}

- (void) configureView
{
	self.m_scrollView.contentOffset = CGPointMake(0,0);
    if (mineTrack) {
        [self setMineViewData];
    } else {
        [self setViewData];
    }
}

- (void) setMineViewData
{
    [[self.commentsView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
	NSDictionary *aData = [self.m_data messageData:0];
        
    //=================== Adding headline
    
    NSDictionary *temp_text_dict= [aData valueForKey:@"wsBuzz"];
    if ([temp_text_dict isKindOfClass:[NSDictionary class]]) {
        self.m_headingLabel.text = [temp_text_dict objectForKey:@"headline"];
    }
    
    //=================== Adding product picture
    
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[aData valueForKey:@"productImageUrl"]];
    
	//Bharat: 11/23/11: US44 - Check if product info exists, if yes, show the image2
	id wsProductDictionary = [aData valueForKey:@"wsProduct"];
    
	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
		id imageUrlsArr = [((NSDictionary *)wsProductDictionary) valueForKey:@"imageUrls"];
		if ((imageUrlsArr != nil) && ([imageUrlsArr isKindOfClass:[NSArray class]] == YES) && 
            ([((NSArray *)imageUrlsArr) count] > 1) && ([[imageUrlsArr objectAtIndex:1] length] > 1)) {
			productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[((NSArray *)imageUrlsArr) objectAtIndex:1]];
		}	
	}
    
	self.m_storyImageView.messageTag=1000;
    self.m_storyImageView.cropImage = NO;
    self.m_storyImageView.delegate = self;
    self.m_storyImageView.frame = CGRectMake(29, 44, 260, 260);
    [self.m_storyImageView resetButton];
    self.m_storyImageView.maskImage = NO;
	[self.m_storyImageView loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked:) btnText:@""];
    if (self.m_storyImageView.imagePresent) {
        CGSize imageSize = [self.m_storyImageView getImageSize];
        CGRect storyRect = self.m_storyImageView.frame;
        storyRect.size = imageSize;
        [self.m_storyImageView changeViewRect:storyRect];
    }
    
    //================  Adding story texts - headline & who, what...
    
    NSInteger yHeight = self.m_storyImageView.frame.origin.y + self.m_storyImageView.frame.size.height;
    [self.clipButton changeViewYOriginTo:yHeight - self.clipButton.frame.size.height + 5];
    if ([temp_text_dict isKindOfClass:[NSDictionary class]]) {
        self.m_whoWhatTextView.text = [self formatWhoWhatText:temp_text_dict];
    }
    
    if ([self.m_whoWhatTextView.text length] > 0) {
        [self.m_whoWhatTextView sizeToFit];
        NSLog(@"height for who what for cell %d = %f", m_currentMessageID, self.m_whoWhatTextView.contentSize.height);
		self.m_whoWhatTextView.frame = CGRectMake(self.m_whoWhatTextView.frame.origin.x, self.m_whoWhatTextView.frame.origin.y, self.m_whoWhatTextView.frame.size.width, self.m_whoWhatTextView.contentSize.height);
        [self.m_whoWhatTextView changeViewYOriginTo:yHeight];
        yHeight += self.m_whoWhatTextView.frame.size.height;
    }
    
    [self.commentGroupMine changeViewYOriginTo:yHeight];
    
    yHeight += self.commentGroupMine.frame.size.height;
    
    //=================  Adding originator picture
    NSString *Url=[NSString stringWithFormat:@"%@%@",kServerUrl,[aData valueForKey:@"originatorThumbImageUrl"]];	
	NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
    //Nava adopting new layout
	self.m_mineUserImageView.messageTag=10001;
    self.m_mineUserImageView.maskImage = YES;
	[self.m_mineUserImageView loadImageFromURL:temp_loadingUrl target:self action:@selector(ProductBtnClicked:) btnText:@"" ignoreCaching:YES];
    
    //set contentMode to scale aspect to fit
    self.m_mineUserImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    //=================  Adding originator name
    NSString *UserName =[aData valueForKey:@"originatorName"];	
	
	self.m_mineOriginatorNameLabel.text=[NSString stringWithFormat:@"%@",UserName];
	self.m_mineOriginatorNameLabel.font = [UIFont fontWithName:kMyriadProRegularFont size:13];
    
    //=================  Adding text subject
    NSDictionary *temp_dictionary=[aData valueForKey:@"wsMessage"];
	self.m_mineTextViewSubject.text=[temp_dictionary valueForKey:@"bodyMessage"];
	[self.m_mineTextViewSubject sizeToFit];
    
    //yHeight = self.commentGroupMine.frame.origin.y + self.commentGroupMine.frame.size.height;

    [self.commentsView changeViewYOriginTo:yHeight];
    
    int realCount = 0;
	NSArray *messageResponseArray=[aData valueForKey:@"messageResponseList"];
	for (int j=0; j<messageResponseArray.count; j++) 
	{
        NSString	*temp_string=[[messageResponseArray objectAtIndex:j] valueForKey:@"responseMessageBody"];
		if (temp_string && ![temp_string isEqualToString:@""]) 
		{
			realCount++;
		}
	}
    
    BOOL showComments = NO;
    if (realCount > 0) {
        int imgViewY = 0;
        CGRect rect = self.commentsView.frame;
        rect.size.height = 1;
        self.commentsView.frame = rect;
        int countForPresentation = 0;
        int counter = 0;
        
        for(int a=0;a < messageResponseArray.count; a++)
        {
            NSString	*temp_string=[[messageResponseArray objectAtIndex:a] valueForKey:@"responseMessageBody"];
            if (![temp_string isEqualToString:@""]) 
            {
                countForPresentation++;
                showComments = YES;
                UIImageView *temp_imgView2=[[UIImageView alloc]initWithFrame:CGRectMake(10, imgViewY, 274, 59)];
                temp_imgView2.tag = 32;	
                temp_imgView2.userInteractionEnabled = YES;
                UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(40, 0, 190, 40)];
                textView.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
                //textView.font = [UIFont fontWithName:kFontName size:13];
                [textView setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_SIZE_KEY]]];
                textView.backgroundColor = [UIColor clearColor];
                textView.delegate = self;
                textView.tag = 33;
                textView.text=temp_string;
                textView.returnKeyType = UIReturnKeyDefault;
                textView.keyboardType = UIKeyboardTypeDefault; 
                textView.scrollEnabled = YES;
                textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
                textView.editable=NO; 
                textView.userInteractionEnabled=YES;
                
                
                if (counter%2==0) 
                {
                    temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.frame.size.height);
                    temp_imgView2.image=[[UIImage imageNamed:@"white_message_box.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
                }
                else 
                {
                    imgViewY=imgViewY-25;
                    temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.frame.size.height);
                    temp_imgView2.image=[[UIImage imageNamed:@"white_message_box_rotated.png"] stretchableImageWithLeftCapWidth:50 topCapHeight:30];
                }
                
                
                [temp_imgView2 addSubview:textView];
                
                temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.contentSize.height);
                
                
                textView.frame =CGRectMake(40, 0, 190,textView.contentSize.height);
                
                if (counter%2!=0) 
                {
                    textView.frame =CGRectMake(40, 12, 190,textView.contentSize.height);
                }
                
                
                UILabel *tmp_elapsedTime=[[UILabel alloc] initWithFrame:CGRectMake(47, textView.contentSize.height -5, 190, 12)];
                
                if (counter%2!=0) 
                {
                    tmp_elapsedTime.frame=CGRectMake(47, textView.contentSize.height +8, 190, 12);
                    
                }
                
                tmp_elapsedTime.text=[NSString stringWithFormat:@"Added by %@ ",[[messageResponseArray objectAtIndex:a]valueForKey:@"responseCustomerName"]];
                tmp_elapsedTime.backgroundColor=[UIColor clearColor];	
                tmp_elapsedTime.textColor=[UIColor lightGrayColor];
                tmp_elapsedTime.font=[UIFont fontWithName:kFontName size:10];
                
                [temp_imgView2 addSubview:tmp_elapsedTime];
                
                [tmp_elapsedTime release];
                tmp_elapsedTime=nil;
                
                
                //imgViewY=imgViewY+textView.contentSize.height+40;
                
                AsyncButtonView* SenderImage = [[[AsyncButtonView alloc]
                                                 initWithFrame:CGRectMake(5,5,35,35)] autorelease];
                
                if (counter%2!=0) 
                {
                    SenderImage.frame=CGRectMake(5, 20, 35, 35);
                }
                SenderImage.messageTag = a;
                [temp_imgView2 addSubview:SenderImage];
                NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[messageResponseArray objectAtIndex:a] valueForKey:@"thumbCustomerUrl"]];
                NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,60,32]];
                SenderImage.maskImage = YES;
                [SenderImage loadImageFromURL:tempLoadingUrl target:self action:@selector(commenterImagePressed:) btnText:nil];
               
                [self.commentsView addSubview:temp_imgView2];
                
                rect = self.commentsView.frame;
                rect.size.height += temp_imgView2.frame.size.height + 10;
                self.commentsView.frame = rect;
                
                imgViewY = temp_imgView2.frame.origin.y + temp_imgView2.frame.size.height + 10;
                NSLog(@"imgViewY>>>>>>>>>>>>>>>>%d",imgViewY);
                [temp_imgView2 release];
                temp_imgView2=nil;
                [textView release];
                textView=nil;
                counter++;
            }
        }
    } 
    
    
    
    if (showComments) {
        [self.commentsView setHidden:NO];
        yHeight = self.commentsView.frame.origin.y + self.commentsView.frame.size.height;
    } else {
        [self.commentsView setHidden:YES];
        yHeight = self.commentsGroup.frame.origin.y + self.commentsGroup.frame.size.height;
    }
    
    [m_scrollView setContentSize:CGSizeMake(320, yHeight + 400)];
   
    self.reply.tag = m_currentMessageID;
}


- (void) setViewData
{
    [[self.commentsView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
	
	NSDictionary *aData = [self.m_data messageData:0];
    
    NSInteger messageId = [[aData valueForKey:@"messageId"] intValue];
    
    NSMutableDictionary *statusDict = [self findStatusByMessageId:messageId];
    //=================== Adding headline
    
    NSDictionary *temp_text_dict= [aData valueForKey:@"wsBuzz"];
    self.storyUrl = @"";
    if ([temp_text_dict isKindOfClass:[NSDictionary class]]) {
        self.m_headingLabel.text = [temp_text_dict objectForKey:@"headline"];
    }
    
    //=================== Adding product picture
    
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[aData valueForKey:@"productImageUrl"]];
    
	//Bharat: 11/23/11: US44 - Check if product info exists, if yes, show the image2
	id wsProductDictionary = [aData valueForKey:@"wsProduct"];
    
	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
		id imageUrlsArr = [((NSDictionary *)wsProductDictionary) valueForKey:@"imageUrls"];
		if ((imageUrlsArr != nil) && ([imageUrlsArr isKindOfClass:[NSArray class]] == YES) && 
            ([((NSArray *)imageUrlsArr) count] > 1) && ([[imageUrlsArr objectAtIndex:1] length] > 1)) {
			productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[((NSArray *)imageUrlsArr) objectAtIndex:1]];
		}	
	}
    
	self.m_storyImageView.messageTag=1000;
    self.m_storyImageView.cropImage = NO;
    self.m_storyImageView.delegate = self;
    self.m_storyImageView.frame = CGRectMake(29, 44, 260, 260);
    [self.m_storyImageView resetButton];
    self.m_storyImageView.maskImage = NO;
	[self.m_storyImageView loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked:) btnText:@""];
    if (self.m_storyImageView.imagePresent) {
        CGSize imageSize = [self.m_storyImageView getImageSize];
        CGRect storyRect = self.m_storyImageView.frame;
        storyRect.size = imageSize;
        [self.m_storyImageView changeViewRect:storyRect];
    }
    
    //================  Adding story texts - headline & who, what...
    
    NSInteger yHeight = self.m_storyImageView.frame.origin.y + self.m_storyImageView.frame.size.height;
    [self.clipButton changeViewYOriginTo:yHeight - self.clipButton.frame.size.height + 5];
    if ([temp_text_dict isKindOfClass:[NSDictionary class]]) {
        self.m_whoWhatTextView.text = [self formatWhoWhatText:temp_text_dict];
    } 
    
    if ([self.m_whoWhatTextView.text length] > 0) {
        [self.m_whoWhatTextView sizeToFit];
        NSLog(@"height for who what for cell %d = %f", m_currentMessageID, self.m_whoWhatTextView.contentSize.height);
		self.m_whoWhatTextView.frame = CGRectMake(self.m_whoWhatTextView.frame.origin.x, self.m_whoWhatTextView.frame.origin.y, self.m_whoWhatTextView.frame.size.width, self.m_whoWhatTextView.contentSize.height);
        [self.m_whoWhatTextView changeViewYOriginTo:yHeight];
        yHeight += self.m_whoWhatTextView.frame.size.height;
    }
    
    [self.commentsGroup changeViewYOriginTo:yHeight];
    
    yHeight += self.commentsGroup.frame.size.height;
    
    //=================  Adding originator picture
    NSString *Url=[NSString stringWithFormat:@"%@%@",kServerUrl,[aData valueForKey:@"originatorThumbImageUrl"]];	
	NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
    //Nava adopting new layout
	self.m_userImageView.messageTag=10001;
    self.m_userImageView.maskImage = YES;
	[self.m_userImageView loadImageFromURL:temp_loadingUrl target:self action:@selector(ProductBtnClicked:) btnText:@"" ignoreCaching:YES];
    
    //set contentMode to scale aspect to fit
    self.m_userImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    //=================  Adding originator name
    NSString *UserName =[aData valueForKey:@"originatorName"];	
	
	self.m_originatorNameLabel.text=[NSString stringWithFormat:@"%@",UserName];
	self.m_originatorNameLabel.font = [UIFont fontWithName:kMyriadProRegularFont size:13];
    
    //=================  Adding likes text
	if ([aData valueForKey:@"likes"]) 
	{
        int value = [[aData valueForKey:@"likes"] intValue];
        if (value > 1 || value == 0) {
            self.m_likeLabel.text=[NSString stringWithFormat:@"%d likes",value];
        } else {
            self.m_likeLabel.text=[NSString stringWithFormat:@"%d like",value];
        }
	}
	if ([aData valueForKey:@"notFavourCount"]) 
	{
        int value = [[aData valueForKey:@"notFavourCount"] intValue];
        if (value > 1 || value == 0) {
            self.m_dislikeLabel.text=[NSString stringWithFormat:@"%d dislikes",value];
        } else {
            self.m_dislikeLabel.text=[NSString stringWithFormat:@"%d dislike",value];
        }
	}
    
	if ([aData valueForKey:@"comments"]) 
	{
        int value = [[aData valueForKey:@"comments"] intValue];
        if (value > 1 || value == 0) {
            self.m_commentsLabel.text=[NSString stringWithFormat:@"%d comments",value];
        } else {
            self.m_commentsLabel.text=[NSString stringWithFormat:@"%d comment",value];
        }
	}
    
    //=================  Adding text subject
    NSDictionary *temp_dictionary=[aData valueForKey:@"wsMessage"];
    
    if ([temp_text_dict isKindOfClass:[NSDictionary class]]) {
        self.storyUrl = [temp_text_dict valueForKey:@"storyUrl"];
    }
    
    if ([self.storyUrl length] > 0) {
        [self.m_textViewSubject setHidden:YES];
        [self.viaLabel setHidden:NO];
        [self.linkButton setHidden:NO];
        [self.linkButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [self.linkButton setTitle:self.storyUrl forState:UIControlStateNormal];
    } else {
        [self.m_textViewSubject setHidden:NO];
        [self.viaLabel setHidden:YES];
        [self.linkButton setHidden:YES];
        self.m_textViewSubject.text=[temp_dictionary valueForKey:@"bodyMessage"];
        [self.m_textViewSubject sizeToFit];
    }
    
    yHeight = self.commentsGroup.frame.origin.y + self.commentsGroup.frame.size.height;
    
    [self.commentsView changeViewYOriginTo:yHeight];
    
    int realCount = 0;
	NSArray *messageResponseArray=[aData valueForKey:@"messageResponseList"];
	for (int j=0; j<messageResponseArray.count; j++) 
	{
        NSString	*temp_string=[[messageResponseArray objectAtIndex:j] valueForKey:@"responseMessageBody"];
		if (temp_string && ![temp_string isEqualToString:@""]) 
		{
			realCount++;
		}
	}
    
    BOOL showComments = NO;
    if (realCount > 0) {
        int imgViewY = 0;
        CGRect rect = self.commentsView.frame;
        rect.size.height = 1;
        self.commentsView.frame = rect;
        int countForPresentation = 0;
        int counter = 0;
        
        for(int a=0;a < messageResponseArray.count; a++)
        {
            NSString	*temp_string=[[messageResponseArray objectAtIndex:a] valueForKey:@"responseMessageBody"];
            if (![temp_string isEqualToString:@""]) 
            {
                countForPresentation++;
                showComments = YES;
                UIImageView *temp_imgView2=[[UIImageView alloc]initWithFrame:CGRectMake(10, imgViewY, 274, 59)];
                temp_imgView2.tag = 32;	
                temp_imgView2.userInteractionEnabled = YES;
                UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(40, 0, 190, 40)];
                textView.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
                //textView.font = [UIFont fontWithName:kFontName size:13];
                [textView setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_SIZE_KEY]]];
                textView.backgroundColor = [UIColor clearColor];
                textView.delegate = self;
                textView.tag = 33;
                textView.text=temp_string;
                textView.returnKeyType = UIReturnKeyDefault;
                textView.keyboardType = UIKeyboardTypeDefault; 
                textView.scrollEnabled = YES;
                textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
                textView.editable=NO; 
                textView.userInteractionEnabled=YES;
                
			
                if (counter%2==0) 
                {
                    temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.frame.size.height);
                    temp_imgView2.image=[[UIImage imageNamed:@"white_message_box.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
                }
                else 
                {
                    imgViewY=imgViewY-25;
                    temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.frame.size.height);
                    temp_imgView2.image=[[UIImage imageNamed:@"white_message_box_rotated.png"] stretchableImageWithLeftCapWidth:50 topCapHeight:30];
                }
			
            
                [temp_imgView2 addSubview:textView];
                
                temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.contentSize.height);
                
                
                textView.frame =CGRectMake(40, 0, 190,textView.contentSize.height);
                
                if (counter%2!=0) 
                {
                    textView.frame =CGRectMake(40, 12, 190,textView.contentSize.height);
                }
                
                
                UILabel *tmp_elapsedTime=[[UILabel alloc] initWithFrame:CGRectMake(47, textView.contentSize.height -5, 190, 12)];
                
                if (counter%2!=0) 
                {
                    tmp_elapsedTime.frame=CGRectMake(47, textView.contentSize.height +8, 190, 12);
                    
                }
                
                tmp_elapsedTime.text=[NSString stringWithFormat:@"Added by %@ ",[[messageResponseArray objectAtIndex:a]valueForKey:@"responseCustomerName"]];
                tmp_elapsedTime.backgroundColor=[UIColor clearColor];	
                tmp_elapsedTime.textColor=[UIColor lightGrayColor];
                tmp_elapsedTime.font=[UIFont fontWithName:kFontName size:10];
                
                [temp_imgView2 addSubview:tmp_elapsedTime];
                
                [tmp_elapsedTime release];
                tmp_elapsedTime=nil;
                
                
                //imgViewY=imgViewY+textView.contentSize.height+40;
                
                AsyncButtonView* SenderImage = [[[AsyncButtonView alloc]
                                                    initWithFrame:CGRectMake(5,5,35,35)] autorelease];
                
                if (counter%2!=0) 
                {
                    SenderImage.frame=CGRectMake(5, 20, 35, 35);
                }
                SenderImage.messageTag = a;
                [temp_imgView2 addSubview:SenderImage];
                NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[messageResponseArray objectAtIndex:a] valueForKey:@"thumbCustomerUrl"]];
                NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,60,32]];
                SenderImage.maskImage = YES;
                [SenderImage loadImageFromURL:tempLoadingUrl target:self action:@selector(commenterImagePressed:) btnText:nil];
               
                [self.commentsView addSubview:temp_imgView2];
                
                rect = self.commentsView.frame;
                rect.size.height += temp_imgView2.frame.size.height + 10;
                self.commentsView.frame = rect;
                
                imgViewY = temp_imgView2.frame.origin.y + temp_imgView2.frame.size.height + 10;
                NSLog(@"imgViewY>>>>>>>>>>>>>>>>%d",imgViewY);
                [temp_imgView2 release];
                temp_imgView2=nil;
                [textView release];
                textView=nil;
                counter++;
            }
        }
    } 
    
    
    
    if (realCount > 1 || realCount == 0) {
        self.m_commentsLabel.text=[NSString stringWithFormat:@"%d comments",realCount];
    } else {
        self.m_commentsLabel.text=[NSString stringWithFormat:@"%d comment",realCount];
    }
    if (showComments) {
        [self.commentsView setHidden:NO];
        yHeight = self.commentsView.frame.origin.y + self.commentsView.frame.size.height;
    } else {
        [self.commentsView setHidden:YES];
        yHeight = self.commentsGroup.frame.origin.y + self.commentsGroup.frame.size.height;
    }
    
    [m_scrollView setContentSize:CGSizeMake(320, yHeight + 300)];
    
    if (statusDict && [statusDict valueForKey:@"isFollowing"]) {
        [self setFollowButtonStatus:statusDict];
    } else
        [self setFollowButtonStatus:aData];
    
    self.followButton.tag = m_currentMessageID;
    
    if (statusDict && [statusDict valueForKey:@"like"]) {
        [self setLikeButtonsStatus:statusDict];
    } else {
        self.likeButton.highlighted = NO;
        self.dislikeButton.highlighted = NO;
    }
    
    self.likeButton.tag = m_currentMessageID;
    self.dislikeButton.tag = m_currentMessageID;
    self.commentButton.tag = m_currentMessageID;
    self.viewAllCommentButton.tag = m_currentMessageID;

}

- (IBAction)openUrl:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.storyUrl]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.storyUrl]];
    }
}

-(void)initialDelayEnded
{
    m_ThePhotoView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    m_ThePhotoView.alpha = 1.0;
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
	m_ThePhotoView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    [UIView commitAnimations];
}

- (void)bounce1AnimationStopped
{
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
	m_ThePhotoView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
    [UIView commitAnimations];
}

- (void)bounce2AnimationStopped 
{
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	m_ThePhotoView.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];

}

- (IBAction)CloseBtnAction:(id)sender 
{
	[m_ThePhotoView removeFromSuperview];	
}

- (IBAction)CloseWebBtnAction:(id)sender
{
	[theWebView removeFromSuperview];
}


- (void)videofullScreen:(NSNotification *)notif
{
    NSLog(@"%@, info - %@, object - %@", notif, [notif userInfo], [notif object]);
    if (theWebView) {
        [theWebView setHidden:YES];
    }
}

- (void)videoExit:(NSNotification *)notif
{
    NSLog(@"%@, info - %@, object - %@", notif, [notif userInfo], [notif object]);
    if (theWebView) {
        [theWebView removeFromSuperview];
        [[NSNotificationCenter defaultCenter] removeObserver:self  name:@"UIMoviePlayerControllerDidEnterFullscreenNotification" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self  name:@"UIMoviePlayerControllerDidExitFullscreenNotification" object:nil];

    }
}

-(void)ProductBtnClicked:(id)sender
{
	
	if ([sender tag]==1000)
	{
        NSDictionary *aData = [self.m_data messageData:0];
        NSDictionary *temp_text_dict= [aData valueForKey:@"wsBuzz"];
        // check for video story
        if ([[temp_text_dict objectForKey:@"nature"] intValue] == 2 && [[temp_text_dict objectForKey:@"videoUrl"] length] > 0) {
            [theWebView setFrame:CGRectMake(5, 25, 310, 450)];
            self.videoWebView.allowsInlineMediaPlayback  = YES;
            self.videoWebView.mediaPlaybackRequiresUserAction = NO;
            [self.videoWebView setBackgroundColor:[UIColor clearColor]];
            self.videoWebView.delegate = self;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videofullScreen:) name:@"UIMoviePlayerControllerDidEnterFullscreenNotification" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoExit:) name:@"UIMoviePlayerControllerDidExitFullscreenNotification" object:nil];
            [tnApplication.window addSubview:theWebView];
            
            NSString *urlStr = [temp_text_dict objectForKey:@"videoUrl"];
            NSString *embedHTML = @"\
            <html><head>\
            <style type=\"text/css\">\
            body {\
            background-color: transparent;\
            color: white;\
            }\
            </style>\
            </head><body style=\"margin:0\">\
            <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
            width=\"%0.0f\" height=\"%0.0f\"></embed>\
            </body></html>";
            NSString *html = [NSString stringWithFormat:embedHTML, urlStr, videoWebView.bounds.size.width, videoWebView.bounds.size.height];
            [videoWebView loadHTMLString:html baseURL:nil];

        } else {
            [m_ThePhotoView setFrame:CGRectMake(5, 25, 310, 450)];
            [tnApplication.window addSubview:m_ThePhotoView];
            UIImage *tempImage=[self.m_storyImageView.m_btnIcon imageForState:UIControlStateNormal];
            //m_ImageView.contentMode=UIViewContentModeScaleAspectFill;
            if (!tempImage) 
            {
                tempImage=[UIImage imageNamed:@"no-image"];
            }
		
            [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailMessagePage"
                                                            withAction:@"ProductBtnClicked"
                                                             withLabel:nil
                                                             withValue:nil];
        
            m_ImageView.image= tempImage;
            [self initialDelayEnded];
        }
	}
    if([sender tag]==10001)
	{
        ViewAFriendViewController *tempViewAFriend=[[ViewAFriendViewController alloc]init];
        NSDictionary *aData=[self.m_data messageData:0];
        tempViewAFriend.m_Email=[[aData valueForKey:@"wsMessage"] valueForKey:@"sentFromCustomer"];
        tempViewAFriend.m_strLbl1=[aData valueForKey:@"originatorName"];
        tempViewAFriend.messageLayout = NO;
        [self.navigationController pushViewController:tempViewAFriend animated:YES];
        [tempViewAFriend release];
        tempViewAFriend=nil;
        [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailMessagePage"
                                                        withAction:@"authorPictureClicked"
                                                         withLabel:nil
                                                         withValue:nil];
        
	}
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"shouldStartLoadWithRequest");
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidStartLoad");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidFinishLoad");
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"webViewDidFinishLoad %@", [error localizedDescription]);
    if (videoWebView) {
        [videoWebView removeFromSuperview];
        videoWebView = nil;
    }
}


- (void) setLikeButtonsStatus:(NSDictionary *)dict
{
    BOOL likeSelected = [[dict valueForKey:@"like"] isEqualToString:@"Y"];
    BOOL dislikeSelected = [[dict valueForKey:@"dislike"] isEqualToString:@"Y"];
    
    self.likeButton.highlighted = likeSelected;
    self.dislikeButton.highlighted = dislikeSelected;
}

- (IBAction)likeThis:(id)sender
{
    //NSInteger index = ((UIButton *)sender).tag;
    NSString *newState = @"N";
	NSDictionary *aData = [self.m_data messageData:0];
    NSInteger messageId = [[aData valueForKey:@"messageId"] intValue];
    NSMutableDictionary *object = [self findStatusByMessageId:messageId];
    if (object) {
        NSString *likeStr = [object objectForKey:@"like"];
        if (likeStr && [likeStr isEqualToString:@"Y"] ) {
            [object setObject:@"N" forKey:@"like"];
            newState = @"Z";
        } else {
            [object setObject:@"Y" forKey:@"like"];
            newState = @"Y";
        }
        [object setObject:@"N" forKey:@"dislike"];
    } else {
        object = [[[NSMutableDictionary alloc] init] autorelease];
        [object setObject:[NSNumber numberWithInt:messageId] forKey:@"messageId"];
        [object setObject:@"Y" forKey:@"like"];
        newState = @"Y";
        [object setObject:@"N" forKey:@"dislike"];
        [self.statusesArray addObject:object];
    }
    
    [self sendLikeRequest:newState messageID:messageId];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailMessagePage"
                                                    withAction:@"likeThis"
                                                     withLabel:nil
                                                     withValue:nil];
}

//- (IBAction)flagIt:(id)sender
//{
//	UIActionSheet *alertSheet=[[UIActionSheet alloc] initWithTitle:@"Please flag with care" delegate:self cancelButtonTitle:@"Cancel" 
//											destructiveButtonTitle:nil otherButtonTitles:@"Misorganised",@"Prohibited",@"Spam/Overpost",nil];
//	[alertSheet showInView:self.view];
//	alertSheet.tag=1;
//	[alertSheet release];
//	alertSheet=nil;
//}

- (IBAction)clipThis:(id)sender
{
    NSDictionary *aData = [self.m_data messageData:0];
    
    [self addToWishListPublic:aData];

    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailMessagePage"
                                                    withAction:@"clipThis"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (IBAction)dislikeThis:(id)sender
{
    //NSInteger index = ((UIButton *)sender).tag;
	NSDictionary *aData = [self.m_data messageData:0];
    NSInteger messageId = [[aData valueForKey:@"messageId"] intValue];
    NSString *newState = @"N";
    NSMutableDictionary *object = [self findStatusByMessageId:messageId];
    if (object) {
        NSString *likeStr = [object objectForKey:@"dislike"];
        if (likeStr && [likeStr isEqualToString:@"Y"] ) {
            [object setObject:@"N" forKey:@"dislike"];
            newState = @"V";
        } else {
            [object setObject:@"Y" forKey:@"dislike"];
            newState = @"U";
        }
        [object setObject:@"N" forKey:@"like"];
    } else {
        object = [[[NSMutableDictionary alloc] init] autorelease];
        [object setObject:[NSNumber numberWithInt:messageId] forKey:@"messageId"];
        [object setObject:@"N" forKey:@"like"];
        [object setObject:@"Y" forKey:@"dislike"];
        newState = @"U";
        [self.statusesArray addObject:object];
    }
    [self setLikeButtonsStatus:object];
    [self sendLikeRequest:newState messageID:messageId];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailMessagePage"
                                                    withAction:@"dislikeThis"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (void) sendLikeRequest:(NSString *)likeStatus messageID:(NSInteger)messageID
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
    NSMutableDictionary *responseDict = [[[NSMutableDictionary alloc] init] autorelease];
    
	[responseDict setValue:likeStatus forKey:@"isFavour"];
	[responseDict setObject:[NSNumber numberWithInt:messageID] forKey:@"messageId"];
	[responseDict setValue:self.m_Type forKey:@"type"];
	[responseDict setValue:@"" forKey:@"responseWords"];
    
	[l_requestObj addResponseForFittingRoomRequest:@selector(CallBackMethod:responseData:) tempTarget:self userid:self.m_userId msgresponse:responseDict];
	[l_requestObj release];
	l_requestObj=nil;
}



- (NSString *) formatWhoWhatText:(NSDictionary *)dataDict
{
    if ([[dataDict objectForKey:@"oped"] length] > 0)
        return [dataDict objectForKey:@"oped"];
    
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    NSString *data = [dataDict objectForKey:@"who"];
    if ([data length] > 0) {
        [string appendFormat:@"Who: %@\n", data];
    }
    data = [dataDict objectForKey:@"what"];
    if ([data length] > 0) {
        [string appendFormat:@"What: %@\n", data];
    }
    data = [dataDict objectForKey:@"where"];
    if ([data length] > 0) {
        [string appendFormat:@"Where: %@\n", data];
    }
    data = [dataDict objectForKey:@"how"];
    if ([data length] > 0) {
        [string appendFormat:@"How: %@\n", data];
    }
    data = [dataDict objectForKey:@"why"];
    if ([data length] > 0) {
        [string appendFormat:@"Why: %@\n", data];
    }
    data = [dataDict objectForKey:@"when"];
    if ([data length] > 0) {
        [string appendFormat:@"When: %@\n", data];
    }
    return string;
}

- (IBAction)follow:(id)sender
{
    //NSInteger index = ((UIButton *)sender).tag;
	NSDictionary *aData = [self.m_data messageData:0];
    NSInteger messageId = [[aData valueForKey:@"messageId"] intValue];
    
    NSMutableDictionary *object = [self findStatusByMessageId:messageId];
    if (object) {
        NSString *followStr = [object objectForKey:@"isFollowing"];
        if (followStr && [followStr isEqualToString:@"Y"] ) {
            [object setObject:@"N" forKey:@"isFollowing"];
        } else {
            [object setObject:@"Y" forKey:@"isFollowing"];
        }
    } else {
        object = [[[NSMutableDictionary alloc] init] autorelease];
        [object setObject:[NSNumber numberWithInt:messageId] forKey:@"messageId"];
        BOOL following = [[aData valueForKey:@"isFollowing"] isEqualToString:@"Y"];
        [object setObject:(following ? @"N" : @"Y") forKey:@"isFollowing"];
        [self.statusesArray addObject:object];
    }
    
    BOOL following = [[object valueForKey:@"isFollowing"] isEqualToString:@"Y"];
    if (following) {
        [self followAction:aData];
    } else {
        [self unfollowAction:aData];
    }
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailMessagePage"
                                                    withAction:@"follow"
                                                     withLabel:nil
                                                     withValue:[NSNumber numberWithBool:following]];
}

-(void) followAction:(NSDictionary *)aData
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	NSDictionary *tmp_Dict=[aData valueForKey:@"wsMessage"];
	NSString *tmp_originatorName=[tmp_Dict valueForKey:@"sentFromCustomer"];
	
	NSArray *tmp_array=[[NSArray alloc] initWithObjects:tmp_originatorName,nil];
	
	if ([self.m_userId caseInsensitiveCompare:tmp_originatorName] == NSOrderedSame) 
	{
		UIAlertView *Tmp_alertView=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"You cannot Follow yourself." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[Tmp_alertView show];
		[Tmp_alertView release];
		Tmp_alertView=nil;
        
	}
	else 
	{
		[l_requestObj addFollowing:@selector(CallBackMethodforfollow:responseData:) tempTarget:self customerid:self.m_userId followers:tmp_array];
		
	}
    
	[l_requestObj release];
	l_requestObj=nil;
	//[l_buyItIndicatorView ]
	[tmp_array release];
	tmp_array=nil;
    
}

- (void) setFollowButtonStatus:(NSDictionary *)dict 
{
    if ([[dict valueForKey:@"isFollowing"] isEqualToString:@"Y"]) {
        [self.followButton setImage:[UIImage imageNamed:@"unfollow-red"] forState:UIControlStateNormal];
    } else {
        [self.followButton setImage:[UIImage imageNamed:@"red-follow-button"] forState:UIControlStateNormal];
    }
}

- (NSMutableDictionary  *)findStatusByMessageId:(NSInteger)messageId
{
    NSInteger index = [self.statusesArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *dict = (NSDictionary *)obj;
        
        return [[dict objectForKey:@"messageId"] intValue] == messageId;
    }];
    
    if (index != NSNotFound)
        return [self.statusesArray objectAtIndex:index];
    return nil;
}


-(void)unfollowAction:(NSDictionary *)aData
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	NSDictionary *tmp_Dict=[aData valueForKey:@"wsMessage"];
	NSString *tmp_originatorName=[tmp_Dict valueForKey:@"sentFromCustomer"];
	[l_requestObj unfollowAFriend:@selector(CallBackMethodforUnfollow:responseData:) tempTarget:self userid:self.m_userId following:tmp_originatorName];
	[l_requestObj release];
	l_requestObj=nil;
    
}

-(IBAction)btnBackAction:(id)sender
{
    if (self.m_CallBackViewController && [self.m_CallBackViewController respondsToSelector:@selector(updateCurrentPage:)]) {
        [self.m_CallBackViewController performSelector:@selector(updateCurrentPage:) withObject:[NSNumber numberWithInt:curPage]];
    }
	[self.navigationController popViewControllerAnimated:YES];
	
}

- (IBAction)commentThis:(id)sender
{
    //NSInteger index = ((UIButton *)sender).tag;
	NSDictionary *aData = [self.m_data messageData:0];
	CommentViewController *commentController = [[CommentViewController alloc] init];
    
    // GDL: I was releasing one of the components of m_MainArray.
	commentController.m_data = aData;
	commentController.m_FavourString = @"";
	commentController.m_CallBackViewController = self;//**
    
	[self.navigationController pushViewController:commentController animated:YES];
	[commentController release];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailMessagePage"
                                                    withAction:@"commentThis"
                                                     withLabel:nil
                                                     withValue:nil];
}

#pragma mark Follow/Unfollow
-(void)CallBackMethodforfollow:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	if ([responseCode intValue]==200) {
        //[self.m_tableView reloadData];
        NSDictionary *aData = [self.m_data messageData:0];
        NSInteger messageId = [[aData valueForKey:@"messageId"] intValue];
        NSMutableDictionary *object = [self findStatusByMessageId:messageId];
        [self setFollowButtonStatus:object];
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	[tempString release];
	tempString=nil;
	
}

-(void)CallBackMethodforUnfollow:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	if ([responseCode intValue]==200) 
	{
        NSDictionary *aData = [self.m_data messageData:0];
        NSInteger messageId = [[aData valueForKey:@"messageId"] intValue];
        NSMutableDictionary *object = [self findStatusByMessageId:messageId];
        [self setFollowButtonStatus:object];
	}
	else 
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

	[tempString release];
	tempString=nil;
}

#pragma mark FlagIt
-(void)CallBackMethodforFlagIt:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	//[l_buyItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) 
	{
		[self showAlertView:nil alertMessage:@"Your message was sent successfully." tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	[tempString release];
	tempString=nil;
}

#pragma mark -
#pragma mark action sheet delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{
	
    NSDictionary *aData = [self.m_data messageData:0];
    NSInteger messageId = [[aData valueForKey:@"messageId"] intValue];
    
    l_requestObj=[[ShopbeeAPIs alloc] init];
	
	NSString *flagType;
    
	if (actionSheet.tag==1) 
	{
		
        
        if (buttonIndex==0) 
        {
            flagType=@"misorganized";
            [l_requestObj FlagAMessage:@selector(CallBackMethodforFlagIt:responseData:) tempTarget:self userid:self.m_userId messageId:messageId flagType:flagType];
            
        }
        else if(buttonIndex==1)
        {
            flagType=@"prohibited";
            [l_requestObj FlagAMessage:@selector(CallBackMethodforFlagIt:responseData:) tempTarget:self userid:self.m_userId messageId:messageId flagType:flagType];
            
        }
        else if(buttonIndex==2)
        {
            flagType=@"spamOrOverPost";
            [l_requestObj FlagAMessage:@selector(CallBackMethodforFlagIt:responseData:) tempTarget:self userid:self.m_userId messageId:messageId flagType:flagType];
            
        }
	}
	else if (actionSheet.tag==2) 
	{
		if (buttonIndex==0) 
		{
			[self addToWishListPublic:aData];
		}
		else if(buttonIndex==1)
		{
			[self addToWishListPrivate:aData];	
		}
	}
	[l_requestObj release];
	l_requestObj=nil;
	
}

-(void)addToWishListPublic:(NSDictionary *)aData
{
	
	NSString *tmp_visibility=@"public";
	NSDictionary *tmp_ResponseDict=[aData valueForKey:@"wsMessage"];
	NSString *body_Message=[tmp_ResponseDict valueForKey:@"bodyMessage"];
	//NSString *tmp_subject=[tmp_ResponseDict valueForKey:@"subject"];
	//NSString *tmp_comments=[NSString stringWithFormat:@"%@%@",tmp_subject,body_Message];
	//NSLog(@"%@",m_messageId);
	NSDictionary *tmp_dict=[NSDictionary dictionaryWithObjectsAndKeys:self.m_userId,@"custid",body_Message,@"comments",[aData valueForKey:@"messageId"],@"msgid",tmp_visibility,@"visibility",nil];
	
	
	NSLog(@"%@",tmp_dict);
	//	NSLog(@"%@",l_objCatModel.m_strId);
	
	// http://66.28.216.132/salebynow/json.htm?action=addToWishListWithFittingRoomData&wishlist={"custid":"test21@gmail.com","comments":"This Product is very good .. ","msgid":"22","visibility":"private"}
	l_requestObj=[[ShopbeeAPIs alloc] init];
	[l_requestObj addToWishListWithFittingRoomData:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	//[l_requestObj addWishListWithoutProduct:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	[l_requestObj release];
	l_requestObj=nil;
}

-(void)addToWishListPrivate:(NSDictionary *)aData
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	NSString *tmp_visibility=@"private";
	NSDictionary *tmp_ResponseDict=[aData valueForKey:@"wsMessage"];
	NSString *body_Message=[tmp_ResponseDict valueForKey:@"bodyMessage"];
	
	NSDictionary *tmp_dict=[NSDictionary dictionaryWithObjectsAndKeys:self.m_userId,@"custid",body_Message,@"comments",[aData valueForKey:@"messageId"],@"msgid",tmp_visibility,@"visibility",nil];
	
	[l_requestObj addToWishListWithFittingRoomData:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	[l_requestObj release];
	l_requestObj=nil;
	
}


#pragma mark Clipit
-(void)CallBackMethodForIboughtit:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	if ([responseCode intValue]==200) {
        NSLog(@"data downloaded");
        NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"response string: %@",tempString);

        [self showAlertView:nil alertMessage:@"Your message was sent successfully." tag:123 cancelButtonTitle:@"OK" otherButtonTitles:nil];

        [tempString release];
        tempString=nil;
	} else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

}

-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage tag:(NSInteger)Tagvalue cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles
{
	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles ,nil];
	tempAlert.tag=Tagvalue;
	[tempAlert show];
	[tempAlert release];
	tempAlert=nil;
}

#pragma mark -
#pragma mark call back methods

-(void)requestCallBackMethodforWishList:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	if ([responseCode intValue]==200) {
		
		[self showAlertView:nil alertMessage:@"The story was added to your favorites successfully." tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	[tempString release];
	tempString=nil;
	
}

#pragma mark Like/Dislike
-(void)CallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	if ([responseCode intValue]==200) 
	{
        [self showAlertView:nil alertMessage:@"Your message was sent successfully." tag:123 cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        NSDictionary *aData = [self.m_data messageData:0];
        NSInteger messageId = [[aData valueForKey:@"messageId"] intValue];
        NSMutableDictionary *object = [self findStatusByMessageId:messageId];
        [self setLikeButtonsStatus:object];

	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	[tempString release];
	tempString=nil;
	
}

#pragma mark Swipe gestures

/*In response to a swipe gesture, show the image view appropriately then move the image view in the direction of the swipe as it fades out.
*/

#pragma mark Handle swipe

- (IBAction)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer
{
	if(recognizer.direction==UISwipeGestureRecognizerDirectionRight)
	{
		[self moveLeft:nil];
        [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailMessagePage"
                                                        withAction:@"swipe-left"
                                                         withLabel:nil
                                                         withValue:nil];
	}
	else
	{
		[self moveRight:nil];
        [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailMessagePage"
                                                        withAction:@"swipe-right"
                                                         withLabel:nil
                                                         withValue:nil];
	}
    
}

#pragma mark Right swipe
-(IBAction)moveRight:(id)sender
{
    
    NSLog(@"right movement\n");
    if (self.m_currentMessageID < self.m_messageIDsArray.count-1)
        self.m_currentMessageID++;
    else {
        if (moreTrack) {
            if (fetchingData) 
                return;
            
            if (curPage + 1 <= numPages) {
                [[LoadingIndicatorView SharedInstance] startLoadingView:self];
                self.curPage++;
                [[NewsDataManager sharedManager] getOneCategoryData:self type:self.m_Type filter:self.filter number:24 numPage:self.curPage addToExisting:YES];
                fetchingData = YES;
                return;
            }    
        }
    }
    
    // Show-Hide arrow button
	[self showHideArrowButton];
    
    if (self.m_currentMessageID<=m_messageIDsArray.count-1)  
    {
        self.m_messageId=[[self.m_messageIDsArray objectAtIndex:self.m_currentMessageID] intValue];
        
        ShopbeeAPIs *l_request = [[ShopbeeAPIs alloc] init];
        
        NSString *tmp_String=[[NSUserDefaults standardUserDefaults] stringForKey:@"userName"];
        
        [l_request getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:self.m_messageId];
        
        [l_request release];
        
//        [self configureView];
    } 
}

-(IBAction)moveLeft:(id)sender
{
	NSLog(@"left movement\n");
	
    if (self.m_currentMessageID > 0) 
		self.m_currentMessageID--;
    
    // Hide/Show Arrow
	[self showHideArrowButton];
	if (self.m_currentMessageID>=0)  
	{
		self.m_messageId=[[m_messageIDsArray objectAtIndex:self.m_currentMessageID] intValue];
        ShopbeeAPIs *l_request = [[ShopbeeAPIs alloc] init];
        
        NSString *tmp_String=[[NSUserDefaults standardUserDefaults] stringForKey:@"userName"];
        
        [l_request getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:self.m_messageId];
        
        [l_request release];
	} 
    
}

-(void) showHideArrowButton 
{ 
    
    // Left Arrow Button
    if (GetPinnFlag)
    {
        
        if (m_currentMessageID == 0){
            m_leftArrow.hidden = YES;
        }
        else {
            m_leftArrow.hidden = NO;
        }
        
        // Right Arrow Button
        
        if (m_currentMessageID == [self.m_messageIDsArray count]-1) {
            if (moreTrack) {
                m_rightArrow.hidden = self.curPage == numPages;
            } else
                m_rightArrow.hidden = YES;
        } else
            m_rightArrow.hidden = NO;
    }
    else
    {
        m_rightArrow.hidden=YES;
        m_leftArrow.hidden=YES;
    }
}

- (void)commenterImagePressed:(id)sender
{
    ViewAFriendViewController *tempViewAFriend=[[ViewAFriendViewController alloc]init];
	NSDictionary *aData = [self.m_data messageData:0];
	NSArray *messageResponseArray=[aData valueForKey:@"messageResponseList"];
    
    NSDictionary *responseDict = [messageResponseArray objectAtIndex:((MoreButton *)sender).tag];
    
    tempViewAFriend.m_Email=[responseDict valueForKey:@"responseCustomerId"];
    tempViewAFriend.m_strLbl1=[responseDict valueForKey:@"responseCustomerName"];
    tempViewAFriend.messageLayout = NO;
    [self.navigationController pushViewController:tempViewAFriend animated:YES];
    [tempViewAFriend release];
    tempViewAFriend=nil;
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailMessagePage"
                                                    withAction:@"commenterImagePressed"
                                                     withLabel:nil
                                                     withValue:nil];
}




-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSLog(@"data downloaded");
	if ([responseCode intValue]==200) 
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
		
        CategoryData *catData = [CategoryData convertMessageData:[tmp_array objectAtIndex:0]];
        
        self.m_data = catData;
        self.m_Type = catData.type;
        
        self.m_headerLabel.text = self.m_Type;

        [self configureView];
        
        if (tempString!=nil)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	
    
}

- (void) dealloc
{
    [m_textViewSubject release];
    [m_mineTextViewSubject release];
    [m_whoWhatTextView release];
    [m_headingLabel release];
    [m_likeLabel release];
    [m_originatorNameLabel release];
    [m_mineOriginatorNameLabel release];
    [m_storyImageView release];
    [m_userImageView release];
    [m_mineUserImageView release];
    [m_scrollView release];
    [m_customTabbar release];
    [commentsGroup release];
    [m_commentsLabel release];
    [m_dislikeLabel release];
    [allCommentsView release];
    [commentsView release];
    [followButton release];
    [likeButton release];
    [dislikeButton release];
    [clipButton release];
    [m_headerLabel release];
    [m_Type release];
    [m_messageIDsArray release];
    [statusesArray release];
    [m_data release];
    [m_userId release];
    [commentGroupMine release];
    [videoWebView release];
    [viaLabel release];
    [linkButton release];
    [storyUrl release];
    [super dealloc];
}



@end
