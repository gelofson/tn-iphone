//
//  GLImage.m
//  Category on UIImage with basic image filters and scaling.
//
//  Created by Greg Landweber on 5/14/11.
//  Copyright 2011 Gregory D. Landweber. All rights reserved.
//

#import "GLImage.h"

// Constrains pixel value to be between 0 and 255.
UInt8 constrain ( int x ) {
    if ( x < 0 )
        return 0;
    if ( x > 255 )
        return 255;
    return x;
}

@implementation UIImage (GLImage)

// Returns a new UIImage, applying all three filters simultaneously.
- (UIImage *)imageWithSaturation:(float)saturation gamma:(float)gamma smoothing:(int)smoothing {
    
    CGImageRef imageRef = [self CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Our buffer.
    UInt8 *rawData = malloc(height * width * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    
    // Create the context in which we will work.
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
												 bitsPerComponent, bytesPerRow, colorSpace,
												 kCGImageAlphaPremultipliedLast);// | kCGBitmapByteOrder32Big);
	
    // Render the image into our context.
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    
    // Adjust the gamma.
    if ( gamma != 1 ) {
        float oneOverGamma = 1.0 / gamma;
        
        // The gamma lookup table.
        UInt8 gamma[256];
        for ( int i = 0; i < 256; i++ )
            gamma[i] = ( 255.0 * powf ( i / 255.0, oneOverGamma ) + 0.5 );
        
        UInt8 *data = rawData;
        for (int ii = 0 ; ii < width * height ; ++ii) {
            data[0] = gamma[data[0]];
            data[1] = gamma[data[1]];
            data[2] = gamma[data[2]];
            data += 4;
        }
    }

    // Adjust the saturation.
    if ( saturation != 1 ) {
        UInt8 *data = rawData;

        for (int ii = 0 ; ii < width * height ; ++ii) {
            int red = data[0];
            int green = data[1];
            int blue = data[2];
            
            int gray = (red + green + blue) / 3;
            
            red = ( gray + saturation * ( red - gray ) );
            green = ( gray + saturation * ( green - gray ) );
            blue = ( gray + saturation * ( blue - gray ) );
            
            data[0] = constrain(red);
            data[1] = constrain(green);
            data[2] = constrain(blue);
            
            data += 4;
        }
    }
        
    // Smooth the image smoothing times.
    for ( int i = 0; i < smoothing; i++ ) {
        // Horizontal gaussian blur.
        UInt8 *data = rawData;
        
        for ( int row = 0; row < height; row++ ) {
            
            data[0] = ( (((int)data[0]) * 3 ) + (int)data[4] ) >> 2;
            data[1] = ( (((int)data[1]) * 3 ) + (int)data[5] ) >> 2;
            data[2] = ( (((int)data[2]) * 3 ) + (int)data[6] ) >> 2;
            data += 4;
            
            for ( int column = 1; column < width - 1; column++ ) {
                data[0] = ( (((int)data[0]) << 1 ) + (int)data[4] + (int)data[-4] ) >> 2;
                data[1] = ( (((int)data[1]) << 1 ) + (int)data[5] + (int)data[-3] ) >> 2;
                data[2] = ( (((int)data[2]) << 1 ) + (int)data[6] + (int)data[-2] ) >> 2;
                data += 4;           
            }
            
            data[0] = ( (((int)data[0]) * 3 ) + (int)data[-4] ) >> 2;
            data[1] = ( (((int)data[1]) * 3 ) + (int)data[-3] ) >> 2;
            data[2] = ( (((int)data[2]) * 3 ) + (int)data[-2] ) >> 2;
            data += 4;
        }
        
        // Vertical gaussian blur.
        data = rawData;
        
        for ( int column = 0; column < width; column++ ) {
            data[0] = ( (((int)data[0]) * 3) + (int)data[bytesPerRow] ) >> 2;
            data[1] = ( (((int)data[1]) * 3) + (int)data[1+bytesPerRow] ) >> 2;
            data[2] = ( (((int)data[2]) * 3) + (int)data[2+bytesPerRow] ) >> 2;
            data += 4;            
        }
        
        for ( int row = 1; row < height - 1; row++ )
            for ( int column = 0; column < width; column++ ) {
                data[0] = ( (((int)data[0]) << 1) + (int)data[bytesPerRow] + (int)data[-bytesPerRow] ) >> 2;
                data[1] = ( (((int)data[1]) << 1) + (int)data[1+bytesPerRow] + (int)data[1-bytesPerRow] ) >> 2;
                data[2] = ( (((int)data[2]) << 1) + (int)data[2+bytesPerRow] + (int)data[2-bytesPerRow] ) >> 2;
                data += 4;            
            }
        
        for ( int column = 0; column < width; column++ ) {
            data[0] = ( (((int)data[0]) * 3) + (int)data[-bytesPerRow] ) >> 2;
            data[1] = ( (((int)data[1]) * 3) + (int)data[1-bytesPerRow] ) >> 2;
            data[2] = ( (((int)data[2]) * 3) + (int)data[2-bytesPerRow] ) >> 2;
            data += 4;            
        }
    }
    
    // Sharpen the image -smoothing times.
    for ( int i = 0; i < -smoothing; i++ ) {
        
        // Horizontal sharpen filter.
        UInt8 *data = rawData;
        
        for ( int row = 0; row < height; row++ ) {
            
            data[0] = constrain ( ((((int)data[0]) * 5 ) - (int)data[4] ) >> 2 );
            data[1] = constrain( ((((int)data[1]) * 5 ) - (int)data[5] ) >> 2 );
            data[2] = constrain( ((((int)data[2]) * 5 ) - (int)data[6] ) >> 2 );
            data += 4;
            
            for ( int column = 1; column < width - 1; column++ ) {
                data[0] = constrain( ((((int)data[0]) * 6) - (int)data[4] - (int)data[-4] ) >> 2);
                data[1] = constrain( ((((int)data[1]) * 6 ) - (int)data[5] - (int)data[-3] ) >> 2);
                data[2] = constrain( ((((int)data[2]) * 6 ) - (int)data[6] - (int)data[-2] ) >> 2);
                data += 4;            
            }
            
            data[0] = constrain( ((((int)data[0]) * 5 ) - (int)data[-4] ) >> 2);
            data[1] = constrain( ((((int)data[1]) * 5 ) - (int)data[-3] ) >> 2);
            data[2] = constrain( ((((int)data[2]) * 5 ) - (int)data[-2] ) >> 2);
            data += 4;
        }
        
        // Vertical Sharpen filter.
        data = rawData;
        
        for ( int column = 0; column < width; column++ ) {
            data[0] = constrain( ((((int)data[0]) * 5 ) - (int)data[bytesPerRow] ) >> 2);
            data[1] = constrain( ((((int)data[1]) * 5 ) - (int)data[1+bytesPerRow] ) >> 2);
            data[2] = constrain( ((((int)data[2]) * 5 ) - (int)data[2+bytesPerRow] ) >> 2);
            data += 4;            
        }
        
        for ( int row = 1; row < height - 1; row++ ) {
            for ( int column = 0; column < width; column++ ) {
                data[0] = constrain( ((((int)data[0]) * 6) - (int)data[bytesPerRow] - (int)data[-bytesPerRow] ) >> 2 );
                data[1] = constrain( ((((int)data[1]) * 6) - (int)data[1+bytesPerRow] - (int)data[1-bytesPerRow] ) >> 2);
                data[2] = constrain( ((((int)data[2]) * 6) - (int)data[2+bytesPerRow] - (int)data[2-bytesPerRow] ) >> 2);
                data += 4;            
            }
        }
        
        for ( int column = 0; column < width; column++ ) {
            data[0] = constrain( ((((int)data[0]) * 5) - (int)data[-bytesPerRow] ) >> 2);
            data[1] = constrain( ((((int)data[1]) * 5) - (int)data[1-bytesPerRow] ) >> 2);
            data[2] = constrain( ((((int)data[2]) * 5) - (int)data[2-bytesPerRow] ) >> 2);
            data += 4;            
        }
    }
    
    // Extract the image from the bitmap context.
    imageRef = CGBitmapContextCreateImage(context);
	UIImage *image = [UIImage imageWithCGImage:imageRef
                                         scale:1
                                   orientation:self.imageOrientation];  
    
    // Release what we allocated.
    
    CGImageRelease(imageRef);
    CGContextRelease(context);
	CGColorSpaceRelease(colorSpace);
    free(rawData);
    
    return image;
}

// Returns a new UIImage, applying filters individually, calling through to method above.

- (UIImage *)imageWithSaturation:(float)saturation {
    return [self imageWithSaturation:saturation gamma:1 smoothing:0];
}

- (UIImage *)imageWithGamma:(float)gamma {
    return [self imageWithSaturation:1 gamma:gamma smoothing:0];
}

- (UIImage *)imageWithSmoothing:(int)smoothing {
    return [self imageWithSaturation:1 gamma:1 smoothing:smoothing];
}

// Returns a new UIImage, scaling this image to size.
- (UIImage *)imageWithSize:(CGSize)size
{
	UIGraphicsBeginImageContext(size);	
    [self drawInRect:CGRectMake(0.0f, 0.0f,size.width, size.height)];	
	UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();	
	UIGraphicsEndImageContext();
	
	return scaledImage;
}

// Returns a new UIImage, scaling down only, maintaining the aspect ratio.
- (UIImage *)scaledDownImageWithMaximumSize:(CGSize)size 
{
    
    // Is the image already less than the maximum size?
    if ( self.size.width <= size.width && self.size.height <= size.height )
        return self;

    // Compute width and height that preserve the aspect ratio.
    float newWidth = size.height * self.size.width / self.size.height;
    float newHeight = size.width * self.size.height / self.size.width;
    
    if ( newWidth < size.width )
        size.width = newWidth;
    if ( newHeight < size.height )
        size.height = newHeight;
    
    return [self imageWithSize:size];
}

@end
