//
//  MenuViewController.m
//  SlideOutNavigationSample
//
//  Created by Nick Harris on 2/3/12.
//  Copyright (c) 2012 Sepia Labs. All rights reserved.
//

#import "MenuViewController.h"
//#import "LogoExpandingViewController.h"
//#import "LogoFadeViewController.h"
#import "QNavigatorAppDelegate.h"
#import"Constants.h"
#import "NewsDataManager.h"
#import "JMC.h"

@implementation MenuViewController
@synthesize btn_crash;
@synthesize screenShotImageView, screenShotImage, tapGesture, panGesture;
@synthesize m_activityIndicator;
@synthesize m_mutResponseData;

NSURLConnection *l_theConnection;
int m_intResponseStatusCode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.trackedViewName = @"Slider Screen";
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // create a UITapGestureRecognizer to detect when the screenshot recieves a single tap 
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapScreenShot:)];
    [screenShotImageView addGestureRecognizer:tapGesture];
    
    // create a UIPanGestureRecognizer to detect when the screenshot is touched and dragged
    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureMoveAround:)];
    [panGesture setMaximumNumberOfTouches:2];
    [panGesture setDelegate:self];
    [screenShotImageView addGestureRecognizer:panGesture];
#if DEBUG
    [btn_crash setHidden:NO];
#endif
}

- (void)viewDidUnload
{
    [self setBtn_crash:nil];
    [super viewDidUnload];
    
    // remove the gesture recognizers
    [self.screenShotImageView removeGestureRecognizer:self.tapGesture];
    [self.screenShotImageView removeGestureRecognizer:self.panGesture];
    self.m_activityIndicator = nil;
    self.m_mutResponseData=nil;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // when the menu view appears, it will create the illusion that the other view has slide to the side
    // what its actually doing is sliding the screenShotImage passed in off to the side
    // to start this, we always want the image to be the entire screen, so set it there
    [screenShotImageView setImage:self.screenShotImage];
    [screenShotImageView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    // now we'll animate it across to the right over 0.2 seconds with an Ease In and Out curve
    // this uses blocks to do the animation. Inside the block the frame of the UIImageView has its
    // x value changed to where it will end up with the animation is complete.
    // this animation doesn't require any action when completed so the block is left empty
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [screenShotImageView setFrame:CGRectMake(265, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
                     completion:^(BOOL finished){  }];
}

-(void) slideThenHide
{
    // this animates the screenshot back to the left before telling the app delegate to swap out the MenuViewController
    // it tells the app delegate using the completion block of the animation
    [UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [screenShotImageView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
                     completion:^(BOOL finished){ [tnApplication hideSideMenu]; }];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)showNewsViewController
{
    // this sets the currentViewController on the tnApplication to the expanding view controller
    // then slides the screenshot back over
    [tnApplication goToPage:RUNWAY_TAB_INDEX];
    [self slideThenHide];
}

-(IBAction)showNewsMapViewController
{
    // this sets the currentViewController on the tnApplication to the expanding view controller
    // then slides the screenshot back over
    [tnApplication goToPage: GoogleMap_TAB_INDEX];
    [self slideThenHide];
}
-(IBAction)showMakeReportViewController
{
    // this sets the currentViewController on the tnApplication to the expanding view controller
    // then slides the screenshot back over
    [tnApplication goToPage: SHARE_TAB_INDEX];
    [self slideThenHide];
}


-(IBAction)showMyStoriesViewController
{
    // this sets the currentViewController on the tnApplication to the expanding view controller
    // then slides the screenshot back over
    [tnApplication goToPage: MY_PAGE_TAB_INDEX];
    [self slideThenHide];
}
-(IBAction)showSettingViewController
{
    [tnApplication goToPage: SETTINGS_TAB_INDEX];
    [self slideThenHide];
}
- (IBAction)showNotifications
{
//    [tnApplication showNotifications];
    [tnApplication goToPage:NOTIFICATIONS_TAB_INDEX];
    [self slideThenHide];
}

-(IBAction)showFindFriendsViewController
{
    // this sets the currentViewController on the tnApplication to the expanding view controller
    // then slides the screenshot back over
    [tnApplication goToPage: FIND_FRIENDS_TAB_INDEX];
    [self slideThenHide];
}

-(IBAction)showFeedbackViewController
{
    // this sets the currentViewController on the tnApplication to the expanding view controller
    // then slides the screenshot back over
    [tnApplication goToPage: MY_PAGE_TAB_INDEX];
    [[tnApplication contentViewController] presentModalViewController:[[JMC sharedInstance] feedbackViewController] animated:YES];
    [self slideThenHide];
    
    
}

- (IBAction)logout:(id)sender
{
    [self sendRequestForLogout];
}

-(IBAction)showCrashApp
{
    // this sets the currentViewController on the tnApplication to the expanding view controller
    // then slides the screenshot back over
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"About to close app" message:@"This action will crash the app" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Go Ahead", nil];
    [alert show];
    [alert release];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            [self performSelector:@selector(crashNow:)];            
            break;
        default:
            break;
    }
    
}

-(void)sendRequestForLogout
{
	[tnApplication CheckInternetConnection];
	if(tnApplication.m_internetWorking==0)//0: internet working
	{
		self.view.userInteractionEnabled=FALSE;
		[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
        
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=logout&custid=%@",kServerUrl,customerID];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"logout url=[%@]",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//NSLog(@"device used:%@\n",[[UIDevice currentDevice] model]);
		
		if([[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"])
		{
			NSLog(@"No need to send token message\n");
		}
		else 
		{
			NSDictionary *tmp_deviceDetails=[NSDictionary dictionaryWithObjectsAndKeys:tnApplication.m_strDeviceId,@"did",tnApplication.m_strDeviceToken,@"deviceToken",nil];
			NSString *temp_strJson=[NSString stringWithFormat:@"devicedetails=%@",[tmp_deviceDetails JSONFragment]];
			[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		}
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"MenuViewController:sendRequestForLogout: Request sent to get data");
		}
		
	}
	else
	{
		self.view.userInteractionEnabled=TRUE;
		[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
	
}

#pragma mark -
#pragma mark Connection response methods

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse*)response 
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	m_intResponseStatusCode = [httpResponse statusCode];
	
	NSArray * all = [NSHTTPCookie cookiesWithResponseHeaderFields:[httpResponse allHeaderFields] forURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/salebynow/", kServerUrl]]];
    NSLog(@"MenuViewController: didReceiveResponse: How many Cookies: %d", all.count);
	
	[[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookies:all forURL:[NSURL URLWithString:
																		  [NSString stringWithFormat:@"%@/salebynow/", kServerUrl]] mainDocumentURL:nil];
	
	NSArray * availableCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:
								  [NSURL URLWithString:[NSString stringWithFormat:@"%@/salebynow/", kServerUrl]]];
	
	NSLog(@"MenuViewController: didReceiveResponse: no. of cookies: %d",availableCookies.count);
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory =[paths objectAtIndex:0];
	NSString *writableStatePath=[documentsDirectory stringByAppendingPathComponent:@"cookieFile.plist"];
	NSDictionary *temp_dict;
	
	for (NSHTTPCookie *cookie in availableCookies)
	{
        temp_dict = [NSDictionary dictionaryWithObjectsAndKeys:
                     [NSString stringWithFormat:@"%@",kServerUrl], NSHTTPCookieOriginURL,
                     cookie.name, NSHTTPCookieName,
                     cookie.path, NSHTTPCookiePath,
                     cookie.value, NSHTTPCookieValue,
                     nil];
		[temp_dict writeToFile:writableStatePath atomically:YES];

		NSLog(@"MenuViewController: didReceiveResponse: [%@]",temp_dict);

		
	}
	
	NSLog(@"MenuViewController: didReceiveResponse: m_intResponseStatusCode=[%d]",m_intResponseStatusCode);
	
	[m_mutResponseData setLength:0];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[m_mutResponseData appendData:data];
	
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	self.view.userInteractionEnabled=TRUE;
	
	if(m_intResponseStatusCode==200)
	{
		NSLog(@"MenuViewController: connectionDidFinishLoading: response from server: %@",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);

        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        if ([prefs boolForKey:@"fbRegistration"]) {
            [prefs setBool:NO forKey:@"fbRegistration"];
            [tnApplication removeOAuthData];
            [tnApplication.m_session logout];
        }
        
        tnApplication.isUserLoggedInAfterLoggedOutState=FALSE;
        [tnApplication logout];
    } else if (m_intResponseStatusCode == 401){
        NSLog(@"Logged out, ignored the 401 session expired");
        //[[NewsDataManager sharedManager] showErrorByCode:m_intResponseStatusCode fromSource:NSStringFromClass([self class])];
    } else {
		NSLog(@"MenuViewController: connectionDidFinishLoading: response from server: %@",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:@"Logout failure!" message:@"Wrong username/password. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[networkDownAlert show];
		[networkDownAlert release];
		networkDownAlert=nil;
	}
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];		
	self.view.userInteractionEnabled=TRUE;
	
    [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
	
}


- (void)singleTapScreenShot:(UITapGestureRecognizer *)gestureRecognizer 
{
    // on a single tap of the screenshot, assume the user is done viewing the menu
    // and call the slideThenHide function
    [self slideThenHide];
}


/* The following is from http://blog.shoguniphicus.com/2011/06/15/working-with-uigesturerecognizers-uipangesturerecognizer-uipinchgesturerecognizer/ */

-(void)panGestureMoveAround:(UIPanGestureRecognizer *)gesture;
{
    UIView *piece = [gesture view];
    [self adjustAnchorPointForGestureRecognizer:gesture];
    
    if ([gesture state] == UIGestureRecognizerStateBegan || [gesture state] == UIGestureRecognizerStateChanged) {
        
        CGPoint translation = [gesture translationInView:[piece superview]];
        
        // I edited this line so that the image view cannont move vertically
        [piece setCenter:CGPointMake([piece center].x + translation.x, [piece center].y)];
        [gesture setTranslation:CGPointZero inView:[piece superview]];
    }
    else if ([gesture state] == UIGestureRecognizerStateEnded)
        [self slideThenHide];
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gestureRecognizer.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}

- (void)dealloc {
    [btn_crash release];
    [super dealloc];
}
@end
