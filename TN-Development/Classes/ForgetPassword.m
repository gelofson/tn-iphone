//
//  ForgetPassword.m
//  QNavigator
//
//  Created by softprodigy on 13/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ForgetPassword.h"
#import"Constants.h"
#import "ShopbeeAPIs.h"
#import "LoadingIndicatorView.h"

#import"QNavigatorAppDelegate.h"


@implementation ForgetPassword

@synthesize m_textFieldEmail;

ShopbeeAPIs *l_request;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	UILabel *temp_ResetPassword = [[UILabel alloc]initWithFrame:CGRectMake(10,80, 300, 50)];
	
	temp_ResetPassword.text = @"In order to reset your password, please enter the email you used to register your FashionGram.";
	temp_ResetPassword.numberOfLines=2;
	temp_ResetPassword.backgroundColor=[UIColor clearColor];
	[temp_ResetPassword setFont:[UIFont fontWithName:kFontName size:14]];	
	temp_ResetPassword.textColor=[UIColor whiteColor];
	[self.view addSubview:temp_ResetPassword];
	[temp_ResetPassword release];
	
}
-(IBAction)m_goToBackView// for navigating to the back view
{
	[self dismissModalViewControllerAnimated:YES];
}

-(IBAction)m_submitBtnAction{
	
	if (m_textFieldEmail.text.length>0)
	{
		[m_textFieldEmail resignFirstResponder];
		LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
		[tempLoadingIndicator startLoadingView:self];
	
	
		l_request=[[ShopbeeAPIs alloc] init];
		[l_request forgotPassword:@selector(requestCallBackMethod:responseData:) tempTarget:self email:m_textFieldEmail.text];
	
		[l_request release];
		l_request=nil;
	}
	else {
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Please enter email address used to register FashionGram." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];	
	}

	
//	http://66.28.216.132/salebynow/customer.htm?action=forgotPassword&email=
	//UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"This feature is currently not available." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//	[alert show];
//	[alert release];
	
}


	
-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	
	int  temp_responseCode=[responseCode intValue];
	NSLog(@"response data: %@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator stopLoadingView];
	
	if(temp_responseCode==200)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Your password has been sent to your Email Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];	
		

//       NSString *str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding]autorelease];
//		SBJSON *objJson=[SBJSON new];
//		NSMutableArray *response=[objJson objectWithString:str];
		
	}
	else if(temp_responseCode!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"This feature is currently not available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];	
	}
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex==0){
			[self dismissModalViewControllerAnimated:YES];
	}
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
   
    // Release retained IBOutlets.
    self.m_textFieldEmail = nil;
}

- (void)dealloc {
    [m_textFieldEmail release];
    
    [super dealloc];
}


@end
