//
//  AlertMessageSettingViewController.h
//  QNavigator
//
//  Created by softprodigy on 31/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AlertMessageSettingViewController : UIViewController {
	UISwitch *m_buyIt;
	UISwitch *m_boughtIt;
	BOOL m_BuyItStatus;
	BOOL m_BoughtItStatus;
	UIView *m_view;
	UIActivityIndicatorView *m_indicatorView;
	NSString *Buyit;
	NSString *BoughtIt;

}
-(IBAction) m_goToBackView;

@property(nonatomic,retain) IBOutlet UISwitch *m_buyIt; 

@property(nonatomic,retain) IBOutlet UISwitch *m_boughtIt;

-(IBAction) m_buyItAction;

-(IBAction) m_boughtItAction;

-(IBAction) m_btnDoneAction;
@end
