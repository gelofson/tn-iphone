//
//  FittingRoomLoadingView.h
//  QNavigator
//
//  Created by softprodigy on 22/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FittingRoomLoadingView : UIView 
{

}

-(void)startLoadingView:(id)target type:(int)type;
-(void)stopLoadingView;
+ (id) SharedInstance;

@end
