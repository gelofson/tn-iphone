//
//  ProfileSettingsViewController.h
//  QNavigator
//
//  Created by softprodigy on 30/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"ProfileSettingViewController.h"
#import"ProfileinfoSettingViewController.h"
#import"AlertMessageSettingViewController.h"
#import "JSON.h"
#import "ContentViewController.h"

@interface MyPageSettingViewController:ContentViewController<UITableViewDataSource,UITableViewDelegate> {
	NSArray * m_profileSetting;
	NSArray * m_profileSetting1;

	ProfileSettingViewController *m_ProfileSettingVC;
	ProfileinfoSettingViewController *m_ProfileinfoSettingVC;
	AlertMessageSettingViewController *m_AlertMessageSettingVC;
	
	// Bharat: 11/10/2011: Show busy indicator when logout web call in progress.
	UIActivityIndicatorView		*m_activityIndicator;
	NSMutableData 				*m_mutResponseData;
    UIView                      *signoutPopup;
	int m_intResponseStatusCode;
    
    //BOOL menuWasPressed;
}

// Bharat: 11/10/2011: Show busy indicator when logout web call in progress.
@property (nonatomic,retain) IBOutlet UIActivityIndicatorView		*m_activityIndicator;
@property (nonatomic,retain) NSMutableData							*m_mutResponseData;
@property (nonatomic,retain) IBOutlet UIView                      *signoutPopup;
@property	 int m_intResponseStatusCode;

//-(IBAction)slid;
-(void)sendRequestForLogout;
//- (IBAction)slideMenuButtonTouched;
- (IBAction)signOut:(id)sender;
- (IBAction)cancel:(id)sender;
@end
