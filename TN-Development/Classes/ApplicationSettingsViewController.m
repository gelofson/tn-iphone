//
//  ApplicationSettingsViewController.m
//  QNavigator
//
//  Created by Bharat Biswal on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ApplicationSettingsViewController.h"
#import"Constants.h"
#import"ShopbeeAPIs.h"
#import"QNavigatorAppDelegate.h"
#import "GeoSelectViewController.h"

@implementation ApplicationSettingsViewController


@synthesize locationPickerController;
@synthesize m_scrollView;
@synthesize m_label;

QNavigatorAppDelegate *l_appDelegate;
#pragma mark Custom methods-----
-(IBAction) m_goToBackView{      // to navigate back to the previous views
	[self.navigationController popViewControllerAnimated:YES ];
	
}
-(IBAction) editFontSizeAction:(id)sender
{
	UIActionSheet *alert=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" 
									   destructiveButtonTitle:nil otherButtonTitles:@"Small",@"Medium",@"Large",nil];
	[alert showInView:self.view];
	[alert setTag:[sender tag]];
	NSLog(@"%i",[sender tag]);
	[alert release];
}

-(IBAction)SaveBtnAction
{
	if ([m_label.text caseInsensitiveCompare:@"Small"] == NSOrderedSame) {
		[[Context getInstance] setValue:@"-2" forKey:USER_SELECTED_TEXT_FONT_SIZE_KEY];
	} else if ([m_label.text caseInsensitiveCompare:@"Medium"] == NSOrderedSame) {
		[[Context getInstance] setValue:@"0" forKey:USER_SELECTED_TEXT_FONT_SIZE_KEY];
	} else if ([m_label.text caseInsensitiveCompare:@"Large"] == NSOrderedSame) {
		[[Context getInstance] setValue:@"2" forKey:USER_SELECTED_TEXT_FONT_SIZE_KEY];
	} else  {
		[[Context getInstance] setValue:@"0" forKey:USER_SELECTED_TEXT_FONT_SIZE_KEY];
		m_label.text = @"Medium";
	}
}
-(IBAction)InitializeScrollView
{
	m_scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0,110,320,310)];
	
	m_scrollView.pagingEnabled = NO;
	m_scrollView.userInteractionEnabled = YES;
	m_scrollView.showsVerticalScrollIndicator = YES;
	m_scrollView.showsHorizontalScrollIndicator = NO;
	
	m_scrollView.scrollsToTop = NO;
	m_scrollView.contentOffset = CGPointMake(0,0);
	m_scrollView.backgroundColor = [UIColor clearColor];
	
	
	UIButton *temp_feeds = [UIButton buttonWithType:UIButtonTypeCustom];
	[temp_feeds setTag:1];
	UILabel *tmp_personalInfo=[[UILabel alloc] initWithFrame:CGRectMake(10, 5, 150, 20)];
	tmp_personalInfo.text=@"Font Size";
	//tmp_personalInfo.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
    tmp_personalInfo.textColor=[UIColor blackColor];
	//tmp_personalInfo.font=[UIFont fontWithName:kFontName size:16];
    tmp_personalInfo.font=[UIFont boldSystemFontOfSize:16.0f];
	tmp_personalInfo.backgroundColor=[UIColor clearColor];
	m_label=[[UILabel alloc] initWithFrame:CGRectMake(210, 5, 90,25)];
	m_label.backgroundColor=[UIColor clearColor];
	//m_label.textColor=[UIColor colorWithRed:0.46 green:0.80 blue:0.74 alpha:1.0];
    m_label.textColor=[UIColor lightGrayColor];
	m_label.font=[UIFont boldSystemFontOfSize:16.0f];
	m_label.textAlignment=UITextAlignmentRight;

	int fontVal = [[[Context getInstance] getValue:USER_SELECTED_TEXT_FONT_SIZE_KEY] intValue];
	if (fontVal == -2) {
		m_label.text=@"Small";
	} else if (fontVal == 0) {
		m_label.text=@"Medium";
	} else if (fontVal == 2) {
		m_label.text=@"Large";
	}

	
    temp_feeds.frame = CGRectMake(0, 0, 320, 35); // position in the parent view and set the size of the button
	//[temp_feeds setImage:[UIImage imageNamed:@"lightgrey_bar.png"] forState:UIControlStateNormal];
	// add targets and actions
    [temp_feeds addTarget:self action:@selector(editFontSizeAction:) forControlEvents:UIControlEventTouchUpInside];
    
	[temp_feeds addSubview:m_label];
	[temp_feeds addSubview:tmp_personalInfo];
    [m_scrollView addSubview:temp_feeds];
	[tmp_personalInfo release];
	tmp_personalInfo=nil;
    UIImageView *lineImageView=[[UIImageView alloc]init];
    [lineImageView setFrame:CGRectMake(0, 35, 320, 1)];
    [lineImageView setImage:[UIImage imageNamed:@"grey_line.png"]];
    [m_scrollView addSubview:lineImageView];
    [lineImageView release];
    

	
	
#if 0
	temp_feeds = [UIButton buttonWithType:UIButtonTypeCustom];
	[temp_feeds setTag:2];
	tmp_personalInfo=[[UILabel alloc] initWithFrame:CGRectMake(10, 5, 150, 20)];
	tmp_personalInfo.text=@"Home Location";
	//tmp_personalInfo.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
    tmp_personalInfo.textColor=[UIColor blackColor];
	 tmp_personalInfo.font=[UIFont boldSystemFontOfSize:16.0f];
	tmp_personalInfo.backgroundColor=[UIColor clearColor];
	UILabel * aLabel=[[UILabel alloc] initWithFrame:CGRectMake(210, 5, 90,25)];
	aLabel.backgroundColor=[UIColor clearColor];
//	aLabel.textColor=[UIColor colorWithRed:0.46 green:0.80 blue:0.74 alpha:1.0];
//	aLabel.font=[UIFont fontWithName:kFontName size:16];
    m_label.textColor=[UIColor lightGrayColor];
	m_label.font=[UIFont boldSystemFontOfSize:16.0f];
	aLabel.textAlignment=UITextAlignmentRight;
	aLabel.text=@"Bangalore";
	
    temp_feeds.frame = CGRectMake(0, 50, 320, 35); // position in the parent view and set the size of the button
	//[temp_feeds setImage:[UIImage imageNamed:@"lightgrey_bar.png"] forState:UIControlStateNormal];
	// add targets and actions
    [temp_feeds addTarget:self action:@selector(editHomeLocation:) forControlEvents:UIControlEventTouchUpInside];
    
	[temp_feeds addSubview:aLabel];
    [aLabel release];
	[temp_feeds addSubview:tmp_personalInfo];
    [m_scrollView addSubview:temp_feeds];
	[tmp_personalInfo release];
	tmp_personalInfo=nil;
    UIImageView *lineImageView=[[UIImageView alloc]init];
    [lineImageView setFrame:CGRectMake(0, 50, 320, 1)];
    [lineImageView setImage:[UIImage imageNamed:@"grey_line.png"]];
    [m_scrollView addSubview:lineImageView];
    [lineImageView release];
#endif

	m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width, 200);//-50);
	
	[self.view addSubview:m_scrollView];
	
	[m_scrollView release];
	m_scrollView=nil;
}

#pragma mark action sheet methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (actionSheet.tag==1) 
	{
		if (buttonIndex==0) 
		{
			m_label.text=@"Small";
		}
		else if(buttonIndex==1)
		{
			m_label.text=@"Medium";
		} 
		else if(buttonIndex==2)
		{
			m_label.text=@"Large";
		}
	}
}	

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	GeoSelectViewController * gs = [[GeoSelectViewController alloc] initWithNibName:@"GeoSelectViewController" bundle:nil];
	gs.title = @"Home Location";
	self.locationPickerController = gs;
	[gs release];

	[self InitializeScrollView];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_scrollView = nil;
    self.m_label = nil;
}

- (void)dealloc {
	self.locationPickerController = nil;
	[m_scrollView release];
	[m_label release];

    [super dealloc];
}

- (void) editHomeLocation: (id) sender {

	[self.navigationController pushViewController:locationPickerController animated:YES];

}

@end
