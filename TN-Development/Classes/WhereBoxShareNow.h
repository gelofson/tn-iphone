//
//  WhereBox ShareNow.h
//  QNavigator
//
//  Created by Bharat Biswal on 12/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"
//#import "TwitterProcessing.h"
#import "JSON.h"

#ifdef OLD_FB_SDK
#import "FBConnect/FBConnect.h"
#import "FBConnect/FBSession.h"
#endif
#import"SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"

#import "CategoryModelClass.h"


#define CONTACT_ENTRY_SELECT_TAG 20011
#define CONTACT_ENTRY_NAME_TAG 20012
#define CONTACT_ENTRY_EMAIL_TAG 20013

#define SHARENOW_REQUEST_TYPE_ADDCONTACT 1111
#define SHARENOW_REQUEST_TYPE_DELCONTACT 2222
#define SHARENOW_REQUEST_TYPE_GETCONTACT 3333
#define SHARENOW_REQUEST_TYPE_SHARENOW	 4444

#define INPUT_TYPE_SHOPBEE_ADS	 3131
#define INPUT_TYPE_GROUPON_ADS	 4242

@class BuzzButtonAddFromAddressBook;
@interface WhereBoxShareNow : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate,
		FBSessionDelegate, FBRequestDelegate, FBDialogDelegate, SA_OAuthTwitterControllerDelegate, SA_OAuthTwitterEngineDelegate> {
	
	//NSString *  m_strType;
	//NSString *  m_strMessage;
	CategoryModelClass *  m_objCatModel;

	IBOutlet UITableView *m_emailTableView;
	IBOutlet UIButton *m_facebookButton;
	IBOutlet UIButton *m_twitterButton;
	IBOutlet UIButton *m_selectAllButton;
	
	IBOutlet UIView *m_sectionHeaderView;
	IBOutlet UIImageView *m_photoView;

	UIImage * photoImage;
	
	NSMutableDictionary *m_DictOfContactDicts;
	NSMutableDictionary *m_newlyAddedContactsDict;
	NSMutableDictionary *m_deletedContactsDict;

	BuzzButtonAddFromAddressBook * addrBookVc;
	
#ifdef OLD_FB_SDK
    FBSession *localFBSession;
	//FBLoginDialog* fbLoginDialog;
#endif
	//TwitterProcessing * twitterProcessingVc;
	
	NSMutableData *m_mutResponseData;
	int m_intResponseCode;
	int m_intRequestType;
	int m_intInputType;
	NSString * m_photoUrl;
		
	SA_OAuthTwitterEngine *twitterEngine;  
}

@property (nonatomic,assign) SA_OAuthTwitterEngine * twitterEngine;
#ifdef OLD_FB_SDK
@property (nonatomic,assign) FBSession * localFBSession;
#endif
//@property (nonatomic,assign) FBLoginDialog * fbLoginDialog;

@property (nonatomic,assign) int m_intInputType;
@property (nonatomic,assign) int m_intRequestType;
@property (nonatomic,assign) int m_intResponseCode;
@property (nonatomic,retain) NSString *m_photoUrl;
@property (nonatomic,retain) NSMutableData *m_mutResponseData;
//@property (nonatomic,retain) NSString *m_strType;
//@property (nonatomic,retain) NSString *m_strMessage;
@property (nonatomic,retain) CategoryModelClass *m_catObjModel;
//@property (nonatomic,retain) TwitterProcessing *twitterProcessingVc;
@property (nonatomic,retain) IBOutlet UITableView *m_emailTableView;
@property (nonatomic,retain) IBOutlet UIButton *m_facebookButton;
@property (nonatomic,retain) IBOutlet UIButton *m_twitterButton;
@property (nonatomic,retain) IBOutlet UIButton *m_selectAllButton;
@property (nonatomic,retain) IBOutlet UIView *m_sectionHeaderView;
@property (nonatomic,retain) IBOutlet UIImageView *m_photoView;
@property (nonatomic,retain) UIImage *photoImage;
@property (nonatomic,retain) NSMutableDictionary *m_DictOfContactDicts;
@property (nonatomic,retain) NSMutableDictionary *m_newlyAddedContactsDict;
@property (nonatomic,retain) NSMutableDictionary *m_deletedContactsDict;
@property (nonatomic,retain) BuzzButtonAddFromAddressBook *addrBookVc;

-(IBAction)goToBackView;
-(IBAction) contactsSelectAllToggleAction:(id) sender;
-(IBAction) facebookToggleAction:(id) sender;
-(IBAction) twitterToggleAction:(id) sender;
-(IBAction) contactSelectToggleAction:(id) sender;

-(IBAction) doShareNow:(id) sender;
-(IBAction) addNewEmailAction:(id) sender;


@end


