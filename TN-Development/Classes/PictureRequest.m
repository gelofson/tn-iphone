//
//  PictureRequest.m
//  TinyNews
//
//  Created by Nava Carmon on 01/11/12.
//
//

#import "PictureRequest.h"
#import "ImageCacheManager.h"

@implementation PictureRequest

@synthesize picUrl, saveName, delegate;

- (id) initWithPictureURL:(NSURL *) url nameToSave:(NSString *) name
{
    return [self initWithPictureURL:url nameToSave:name ignoreCache:NO];
}

- (id) initWithPictureURL:(NSURL *) url nameToSave:(NSString *) name ignoreCache:(BOOL)ignore
{
    self = [super init];
    
    if (self) {
        self.picUrl = url;
        self.saveName = name;
        if (!ignore) {
            UIImage *image = [[ImageCacheManager sharedManager] getImage:self.saveName];
            if (image) {
                [self cancel];
                if (self.delegate && [self.delegate respondsToSelector:@selector(setNewImage:forName:)]) {
                    [self.delegate performSelector:@selector(setNewImage:forName:) withObject:image withObject:self.saveName];
                }
            }
        }
    }
    return self;
}

- (void) main
{
    NSAutoreleasePool *aPool = [[NSAutoreleasePool alloc] init];
    
    if (![self isCancelled]) {
        [self performMain];
    }
    
    [aPool drain];
}

- (void) performMain
{
     NSURLRequest *url = [NSURLRequest requestWithURL:self.picUrl];
    [NSURLConnection sendAsynchronousRequest:url queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error) {
            UIImage *image = [UIImage imageWithData:data];
            if (image!=nil)
            {
                [[ImageCacheManager sharedManager] savePicture:data forName:self.saveName];
                
            }
            if (self.delegate && [self.delegate respondsToSelector:@selector(setNewImage:forName:)]) {
                [self.delegate performSelector:@selector(setNewImage:forName:) withObject:image withObject:self.saveName];
            }
        }
    }];
    
}


- (void) dealloc
{
    self.saveName = nil;
    self.picUrl = nil;
    [super dealloc];
}


@end
