//
//  MainScreenViewController.m
//  QNavigator
//
//  Created by Soft Prodigy on 24/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MainScreenViewController.h"
#import "QNavigatorAppDelegate.h"
#import "BigSalesAdsViewController.h"
#import "IndyShopViewController.h"
#import "CategoriesViewController.h"

@implementation MainScreenViewController

CustomTopBarView *l_customView;
QNavigatorAppDelegate *l_appDelegate;

@synthesize m_lblTitle;

#pragma mark -
#pragma mark UIView Methods

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
	
}

-(void)viewWillAppear:(BOOL)animated
{
	//[self loadRequiredViewController:1];
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_lblTitle = nil;
}


- (void)dealloc {
    [m_lblTitle release];
    
    [super dealloc];
}


@end
