//working..................

//
//  GoogleFriendsViewController.m
//  QNavigator
//
//  Created by softprodigy on 11/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GoogleFriendsViewController.h"

#import "GData.h"

#import "GDataFeedContact.h"
#import "GoogleViewfriends.h"
#import "ShopbeeAPIs.h"
#import "SBJSON.h"
#import "LoadingIndicatorView.h"
#import"QNavigatorAppDelegate.h"
#import"Constants.h"
#import "NewsDataManager.h"
//#define kContactsSegment = 0;
//#define kGroupsSegment = 1;

// GDL: We have the implementation below.
@interface GDataEntryContact (ContactsSampleAdditions)
- (NSDictionary *)entryDisplayName;
@end

@implementation GoogleFriendsViewController

@synthesize m_imgView;

@synthesize m_rememberMeButton;
@synthesize m_strEmailIds;
@synthesize temp_Array;
@synthesize m_nonshopBeeArray;
@synthesize m_shopBeeArray;

// GDL: We declare these properties, so we need to synthesize them
@synthesize m_indicatorView;
@synthesize m_view;
@synthesize m_logo;
ShopbeeAPIs *l_request;

GoogleViewfriends *l_googleFrndsViewCtrl;

QNavigatorAppDelegate *l_appDelegate;


- (void)viewDidLoad {
    [super viewDidLoad];
	m_shopBeeArray =[[NSMutableArray alloc]init];
	m_nonshopBeeArray=[[NSMutableArray alloc]init];		
	temp_Array=[[NSMutableArray alloc]init];
	
	//...	m_rememberMeButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	//	
	//	[m_rememberMeButton setFrame:CGRectMake(12, 225, 50, 50)];
	//	
	//	if(mUsernameField.text==nil)
	//	{
	//		[m_rememberMeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"check_sel" ofType:@"png"]] 
	//						forState:UIControlStateNormal];
	//		isRememberMeActivated=YES;
	//	}
	//	else
	//	{
	//		[m_rememberMeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"check_btn" ofType:@"png"]] 
	//		 						forState:UIControlStateNormal];
	//		isRememberMeActivated=NO;
	//	}
	//	[m_rememberMeButton addTarget:self action:@selector(rememberMeAction:) forControlEvents:UIControlEventTouchUpInside];
	//	[self.view addSubview:m_rememberMeButton];
}

//......- (void) rememberMeAction :(id) sender{
//	
//	if (!isRememberMeActivated) {
//		
//		[(UIButton*)sender setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"check_sel" ofType:@"png"]] 
//						   forState:UIControlStateNormal];
//		
//		isRememberMeActivated = YES;
//	}
//	else {
//		
//		[(UIButton*)sender setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"check_btn" ofType:@"png"]] 
//						   forState:UIControlStateNormal];
//		isRememberMeActivated = NO;
//	}
//}
-(void)viewDidAppear:(BOOL)animated
{
}


#pragma mark -
#pragma mark Text field delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if(textField.text.length>=50 && range.length==0)
	{
		return NO;
	}
	return YES;
	
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	//NSUserDefaults *prefs1 = [NSUserDefaults standardUserDefaults];
	mUsernameField.text=[mUsernameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	mPasswordField.text=[mPasswordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if(mUsernameField.text.length==0 || mPasswordField.text.length==0)
	{
		UIAlertView *temp_alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input username/password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_alertView show];
		[temp_alertView release];
		temp_alertView=nil;
	}
	else
	{
		//[self performSelector:@selector(loginButtonAction:)];
		
		[textField resignFirstResponder];
		LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
		[tempLoadingIndicator startLoadingView:self];
		//[self addProgressView];
		[self fetchAllContacts];
		
		
		//   .....      if (isRememberMeActivated==YES) {
		//					
		//					[prefs1 setObject:mUsernameField.text forKey:@"userName1"];
		//					[prefs1 setObject:mPasswordField.text forKey:@"password1"];
		//					[prefs1 setObject:@"rememberMe" forKey:@"remember1"];
		//					mUsernameField.text=[prefs1 objectForKey:@"userName1"];
		//					mPasswordField.text=[prefs1 objectForKey:@"password1"];
		//				
		//				}
		//				else {
		//					[prefs1 setObject:@"" forKey:@"userName1"];
		//					[prefs1 setObject:@"" forKey:@"password1"];
		//					[prefs1 setObject:@"" forKey:@"remember1"];
		//					mUsernameField.text=[prefs1 objectForKey:@"userName1"];
		//					mPasswordField.text=[prefs1 objectForKey:@"password1"];
		//
		//					
		//				}
		//				
	}
	
	return YES;
}


#pragma mark -
#pragma mark google methods

- (IBAction)loginButtonAction:(id)sender
{
	//LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
//	[tempLoadingIndicator startLoadingView:self];
	
	//NSUserDefaults *prefs1 = [NSUserDefaults standardUserDefaults];
	
	mUsernameField.text=[mUsernameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	mPasswordField.text=[mPasswordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if(mUsernameField.text.length==0 || mPasswordField.text.length==0)
	{
		UIAlertView *temp_alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input username/password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_alertView show];
		[temp_alertView release];
		temp_alertView=nil;
	}
	else {
		
		//	......	if (isRememberMeActivated==YES) {
		//				
		//				[prefs1 setObject:mUsernameField.text forKey:@"userName1"];
		//				[prefs1 setObject:mPasswordField.text forKey:@"password1"];
		//				[prefs1 setObject:@"rememberMe" forKey:@"remember1"];
		//								
		//			}
		//			else {
		//				[prefs1 setObject:@"" forKey:@"userName1"];
		//				[prefs1 setObject:@"" forKey:@"password1"];
		//				[prefs1 setObject:@"" forKey:@"remember1"];
		//				
		//			}
		//[self addProgressView];
		LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
		[tempLoadingIndicator startLoadingView:self];
		
		[self	fetchAllContacts];
	}
}





#pragma mark -
#pragma mark imageFetcher methods

- (void)imageFetcher:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)data error:(NSError *)error
{
	
    if (error) {
        NSLog(@"imageFetcher:%@ failedWithError:%@", fetcher,  error);
        return;
    }
    
	NSURLRequest *tempRequest=[fetcher mutableRequest];
	NSLog(@"downloaded in GoogleFriendsViewController class with index: %@",[tempRequest valueForHTTPHeaderField:@"Index"]);
	
	
	
	NSMutableDictionary *tempDict=[[NSMutableDictionary alloc] initWithDictionary:[temp_Array objectAtIndex:[[tempRequest valueForHTTPHeaderField:@"Index"] intValue]]];  //.............set image//
	[tempDict setObject:[UIImage imageWithData:data] forKey:@"Image"];
	
	[temp_Array replaceObjectAtIndex:[[tempRequest valueForHTTPHeaderField:@"Index"] intValue]  withObject:tempDict];
	[tempDict release];
	
	l_googleFrndsViewCtrl.m_arrAllFriends=temp_Array;
	
	//[l_googleFrndsViewCtrl imageDownloaded:[UIImage imageWithData:data] withId:[tempRequest valueForHTTPHeaderField:@"Index"]];
	
	
	
}

- (void)imageFetcher:(GTMHTTPFetcher *)fetcher failedWithError:(NSError *)error
{
	NSLog(@"imageFetcher:%@ failedWithError:%@", fetcher,  error);       
}






#pragma mark Fetch all contacts

- (NSURL *)contactFeedURL {
	
    // GDL: mPropertyNameField is nil, so this doesn't do anything,
    // GDL: and UIPicker doesn't have a stringValue method.
	NSString *propName = nil; //[mPropertyNameField stringValue];
	
	NSURL *feedURL;
	if ([propName caseInsensitiveCompare:@"full"] == NSOrderedSame
		|| [propName length] == 0) {
		
		// full feed includes all clients' extended properties
		feedURL = [GDataServiceGoogleContact contactFeedURLForUserID:kGDataServiceDefaultUser];
		
	} else if ([propName caseInsensitiveCompare:@"thin"] == NSOrderedSame) {
		
		// thin feed excludes all extended properties
		feedURL = [GDataServiceGoogleContact contactFeedURLForUserID:kGDataServiceDefaultUser
														  projection:kGDataGoogleContactThinProjection];
	} else {
		
		feedURL = [GDataServiceGoogleContact contactFeedURLForPropertyName:propName];
	}
	return feedURL;
}

// begin retrieving the list of the user's contacts
- (void)fetchAllContacts {
	
	[self setContactFeed:nil];
	[self setContactFetchError:nil];
	[self setContactFetchTicket:nil];
	
	GDataServiceGoogleContact *service = [self contactService];
	GDataServiceTicket *ticket;
	
	BOOL shouldShowDeleted = TRUE; //([mShowDeletedCheckbox state] == NSOnState);
	//	BOOL shouldQueryMyContacts = TRUE; //([mMyContactsCheckbox state] == NSOnState);
	
	// request a whole buncha contacts; our service object is set to
	// follow next links as well in case there are more than 2000
	const int kBuncha = 2000;
	
	NSURL *feedURL = [self contactFeedURL];
	
	GDataQueryContact *query = [GDataQueryContact contactQueryWithFeedURL:feedURL];
	[query setShouldShowDeleted:shouldShowDeleted];
	[query setMaxResults:kBuncha];
	
	
	ticket = [service fetchFeedWithQuery:query
								delegate:self
					   didFinishSelector:@selector(contactsFetchTicket:finishedWithFeed:error:)];
	
	[self setContactFetchTicket:ticket];
	
	//[self updateUI];
}

// contacts fetched callback
- (void)contactsFetchTicket:(GDataServiceTicket *)ticket
           finishedWithFeed:(GDataFeedContact *)feed
                      error:(NSError *)error 
{
	
	if ([error code] == 403)
	{
		UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"Authentication Error!" message:@"Please check your username/password and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tempAlert show];
		[tempAlert release];
		tempAlert=nil;
		
		LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
		[tempLoadingIndicator stopLoadingView];
		
		//[m_indicatorView stopAnimating];
		//		[m_view removeFromSuperview];
		//		[m_view release];
		//		m_view=nil;
		//		[m_indicatorView release];
		//		m_indicatorView=nil;
		
		
	} else if ([error code] == 401 || [error code] == 500) {
        NSLog(@"GoogleFriendsViewController:contactsFetchTicket %d", [error code]);
        [[NewsDataManager sharedManager] showErrorByCode:[error code] fromSource:NSStringFromClass([self class])];
        return;
	} else {
		
		NSArray *tempArray=[feed entries];
		
		[temp_Array removeAllObjects];
		
		for (int i=0; i<tempArray.count; i++)
			[temp_Array addObject:[(GDataEntryContact *)[tempArray objectAtIndex:i] entryDisplayName]]; 
		
		NSMutableArray	*tempArray_Email=[[NSMutableArray alloc]init];
		
		///////////////
		//logic to get the images for friends
		for (int i=0;i<temp_Array.count;i++)
		{
			
			[tempArray_Email addObject:[NSString stringWithFormat:@"%@",[[temp_Array objectAtIndex:i]valueForKey:@"Email"]]];
			
			GDataServiceGoogleContact *service = [self contactService];
			NSMutableURLRequest *request = [service requestForURL:[NSURL URLWithString:[[temp_Array objectAtIndex:i] valueForKey:@"Photo"]]
															 ETag:nil
													   httpMethod:nil];
			
			
			[request setValue:@"image/*" forHTTPHeaderField:@"Accept"];
			[request setValue:[NSString stringWithFormat:@"%d",i] forHTTPHeaderField:@"Index"];
			GTMHTTPFetcher *fetcher = [GTMHTTPFetcher fetcherWithRequest:request];
			
			NSURLRequest *tempRequest=[fetcher mutableRequest];
			NSLog(@"%@",[tempRequest valueForHTTPHeaderField:@"Index"]);
			[fetcher beginFetchWithDelegate:self
						  didFinishSelector:@selector(imageFetcher:finishedWithData:error:)];
			
		}
		
		self.m_strEmailIds=[NSString stringWithFormat:@"listids=%@",[tempArray_Email JSONFragment]];
		[tempArray_Email release];
		tempArray_Email=nil;
		
		NSLog(@"%@",m_strEmailIds);
		
		
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
		
		l_request=[[ShopbeeAPIs alloc] init];
		[l_request getRegisteredUsers:@selector(requestCallBackMethodForListIds:responseData:) tempTarget:self customerid:customerID listids:m_strEmailIds];
		[l_request release];
		l_request=nil;
	} 	
}

- (void)setContactFeed:(GDataFeedContact *)feed {
	[mContactFeed autorelease];
	mContactFeed = [feed retain];
}

- (NSError *)contactFetchError {
	return mContactFetchError; 
}

- (void)setContactFetchError:(NSError *)error {
	[mContactFetchError release];
	mContactFetchError = [error retain];
}

- (GDataServiceTicket *)contactFetchTicket {
	return mContactFetchTicket; 
}

- (void)setContactFetchTicket:(GDataServiceTicket *)ticket {
	[mContactFetchTicket release];
	mContactFetchTicket = [ticket retain];
}

- (NSString *)contactImageETag {
	return mContactImageETag;
}

- (void)setContactImageETag:(NSString *)str {
	[mContactImageETag autorelease];
	mContactImageETag = [str copy];
}


- (NSURL *)groupFeedURL {
	NSURL *feedURL;
	
	feedURL = [GDataServiceGoogleContact groupFeedURLForUserID:kGDataServiceDefaultUser];
	
	return feedURL;
}



- (void)groupsFetchTicket:(GDataServiceTicket *)ticket
         finishedWithFeed:(GDataFeedContactGroup *)feed
                    error:(NSError *)error {
	NSLog(@"%@",[feed entries]);
}

-(GDataServiceGoogleContact *)contactService {
	
	static GDataServiceGoogleContact* service = nil;
	
	if (!service) {
		service = [[GDataServiceGoogleContact alloc] init];
		
		[service setShouldCacheResponseData:YES];
		[service setServiceShouldFollowNextLinks:YES];
	}
	
	// update the username/password each time the service is requested
	NSString *username = mUsernameField.text; 
	NSString *password = mPasswordField.text;
	
	[service setUserCredentialsWithUsername:username
								   password:password];
	
	return service;
}


#pragma mark -
#pragma mark Callback methods

-(void)requestCallBackMethodForListIds:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	int  temp_responseCode=[responseCode intValue];
	//NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator stopLoadingView];
	
	if(temp_responseCode==200)
	{
		NSLog(@"responseCode is 200");
		NSString *str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding]autorelease];
		
		
		SBJSON *objJson=[SBJSON new];
		
		id response=[objJson objectWithString:str];
		
		
		NSMutableArray	*tempArray_Email=[[NSMutableArray alloc]init];
		
		for(int i=0;i<[response count];i++)
		{
			[tempArray_Email addObject:[[response objectAtIndex:i]valueForKey:@"shopbee"]];
		}
		NSArray *arr = [tempArray_Email objectAtIndex:0];
		
		BOOL tempStatus;
		tempStatus=FALSE;
		
		[m_shopBeeArray removeAllObjects];
		[m_nonshopBeeArray removeAllObjects];
		
		for(int j=0;j<[temp_Array count];j++)
		{
			for(int i=0;i<[arr	count];i++)
			{	
				if([[[temp_Array objectAtIndex:j] objectForKey:@"Email"] isEqualToString:[arr objectAtIndex:i]])
				{
					tempStatus=TRUE;
					break;
					
				}
				else
				{
					tempStatus=FALSE;
					
				}
				
			}
			
			if (tempStatus==TRUE)
			{
				[m_shopBeeArray addObject:[temp_Array objectAtIndex:j]];
				tempStatus=FALSE;
			}
			else
			{
				[m_nonshopBeeArray addObject:[temp_Array objectAtIndex:j]];
			}
			
			
		}
		
		NSLog(@"FashionGram count: %d, nonFashionGram count: %d",m_shopBeeArray.count, m_nonshopBeeArray.count);
		l_googleFrndsViewCtrl=[[GoogleViewfriends alloc]init];
		l_googleFrndsViewCtrl.m_nonshopBeeFriends=[m_nonshopBeeArray autorelease];
		l_googleFrndsViewCtrl.m_ShopBeegoogleFriends=[m_shopBeeArray autorelease];
		[self.navigationController pushViewController:l_googleFrndsViewCtrl animated:YES];
		
	}
	else {
        NSLog(@"GoogleFriendsViewController: %d", [responseCode intValue]);
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }
	
}

#pragma mark -
#pragma mark custom methods

-(IBAction) m_goToBackView{
	[self.navigationController popViewControllerAnimated:YES];
}


/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

// GDL: Edited everything below this in GoogleFriendsViewController implementation.

#pragma mark -

- (void)viewDidUnload {
    [super viewDidUnload];

    // Release all retained IBOutlets.
    [mUsernameField release]; mUsernameField = nil;
    [mPasswordField release]; mPasswordField = nil;
    
    [mShowDeletedCheckbox release]; mShowDeletedCheckbox = nil;
    [mMyContactsCheckbox release]; mMyContactsCheckbox = nil;
    [mPropertyNameField release]; mPropertyNameField = nil;
    [mGetContactsButton release]; mGetContactsButton = nil;
    
    [mFeedSegmentedControl release]; mFeedSegmentedControl = nil;
    [mFeedTable release]; mFeedTable = nil;
    [mFeedProgressIndicator release]; mFeedProgressIndicator = nil;
    [mFeedResultTextField release]; mFeedResultTextField = nil;
    [mFeedCancelButton release]; mFeedCancelButton = nil;
    [mSortFeedCheckbox release]; mSortFeedCheckbox = nil;

    [mContactImageView release]; mContactImageView = nil;
    [mSetContactImageButton release]; mSetContactImageButton = nil;
    [mDeleteContactButton release]; mDeleteContactButton = nil;
    [mSetContactImageProgressIndicator release]; mSetContactImageProgressIndicator = nil;
    
	[mAddContactButton release]; mAddContactButton = nil;
    [mAddTitleField release]; mAddTitleField = nil;
    [mAddEmailField release]; mAddEmailField = nil;
    [mDeleteContactButton release]; mDeleteContactButton = nil;
    [mDeleteAllButton release]; mDeleteAllButton = nil;
    
    [mEntrySegmentedControl release]; mEntrySegmentedControl = nil;
    
    [mEntryTable release]; mEntryTable = nil;
    [mEntryResultTextField release]; mEntryResultTextField = nil;
    
    [mAddEntryButton release]; mAddEntryButton = nil;
    [mDeleteEntryButton release]; mDeleteEntryButton = nil;
    [mEditEntryButton release]; mEditEntryButton = nil;
    [mMakePrimaryEntryButton release]; mMakePrimaryEntryButton = nil;
    
    [mServiceURLField release]; mServiceURLField = nil;

    self.m_imgView = nil;
}


- (void)dealloc {
    [mUsernameField release];
    [mPasswordField release];
    
    [mShowDeletedCheckbox release];
    [mMyContactsCheckbox release];
    [mPropertyNameField release];
    [mGetContactsButton release];
    
    [mFeedSegmentedControl release];
    [mFeedTable release];
    [mFeedProgressIndicator release];
    [mFeedResultTextField release];
    [mFeedCancelButton release];
    [mSortFeedCheckbox release];
    
    [mContactImageView release];
    [mSetContactImageButton release];
    [mDeleteContactButton release];
    [mSetContactImageProgressIndicator release];
    
	[mAddContactButton release];
    [mAddTitleField release];
    [mAddEmailField release];
    [mDeleteContactButton release];
    [mDeleteAllButton release];
    
    [mEntrySegmentedControl release];
    
    [mEntryTable release];
    [mEntryResultTextField release];
    
    [mAddEntryButton release];
    [mDeleteEntryButton release];
    [mEditEntryButton release];
    [mMakePrimaryEntryButton release];
    
    [mServiceURLField release];
    
    [mContactFetchTicket release];
    [mContactFetchError release];
    [mContactFeed release];
    [mContactImageETag release];
    
    [mGroupFeed release];
    [mGroupFetchTicket release];
    [mGroupFetchError release];
    
    [m_imgView release];
    [m_rememberMeButton release];
    
    [m_strEmailIds release];
    [m_view release];
    [m_indicatorView release];
    [temp_Array release];
    [m_nonshopBeeArray release];
    [m_shopBeeArray release];
    [m_logo release];
    [super dealloc];
}

@end


@implementation GDataEntryContact (ContactsSampleAdditions)
NSDictionary *m_dicGetDetails;
NSMutableArray *m_arrGetDetails; 

- (NSDictionary *)entryDisplayName {
	
	NSString *getName;
	NSString *getEmail;
	NSString *getPhoto;
	
	getName = [[self title] stringValue];
	
	
	/*if ([title length] == 0 && [[self emailAddresses] count] > 0) {
	 
	 GDataEmail *email = [self primaryEmailAddress];
	 if (email == nil) {
	 email = [[self emailAddresses] objectAtIndex:0];
	 }
	 title = [email address]; 
	 }*/
	
	
	GDataEmail *email = [self primaryEmailAddress];
	if (email == nil) {
		email = [[self emailAddresses] objectAtIndex:0];
	}
	
	getEmail = [email address];
	
	
	GDataLink *tempLink=[self photoLink];
	
	NSURL *tempUrl=[tempLink URL];
	
	
	getPhoto=[NSString stringWithFormat:@"%@",tempUrl];
	
	
	if ([getPhoto length] == 0) {
		
		getPhoto = [self description]; 
	}
	
	
	
	
	
	//NSLog(@"->>>>>>>>>google_getEmail>>>>>>>>>>>>>%@",getEmail);
	//	NSLog(@"->>>>>>>>>google_getPhoto>>>>>>>>>>>>>%@",getPhoto);
	//	NSLog(@"->>>>>>>>>google_getName>>>>>>>>>>>>>%@",getName);
	
	if(!getName)
	{
		NSLog(@"array is empty..........:)");
		
	}
	
	m_dicGetDetails=[NSDictionary dictionaryWithObjectsAndKeys:getEmail,@"Email",getPhoto,@"Photo",getName,@"Name",nil];
	
	//NSLog(@" array of friendsssssssssss...........%@",m_arrGetDetails);
	
	
	
	
	
	return m_dicGetDetails;
}



@end
