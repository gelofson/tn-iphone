//
//  PhotosLoadingView.h
//  QNavigator
//
//  Created by softprodigy on 24/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosLoadingView : UIView {
	NSURLConnection* connection; //keep a reference to the connection so we can cancel download in dealloc
	NSMutableData* data; //keep reference to the data so we can collect it as it downloads
	//but where is the UIImage reference? We keep it in self.subviews - no need to re-code what we have in the parent class
	UIImageView* imageView;
}
@property (nonatomic,retain)UIImageView* imageView;

- (void)loadImageFromURL:(NSURL*)url tempImage:(id)tempImage;

- (UIImage*) image;

@end
