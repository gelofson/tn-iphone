//
//  FittingRoomMoreViewController.m
//  QNavigator
//
//  Created by softprodigy on 21/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FittingRoomMoreViewController.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import"FittingRoomUseCasesViewController.h"
#import"BuyItCommentViewController.h"
#import"ShopbeeAPIs.h"
#import"AsyncImageView.h"
#import"AsyncButtonView.h"
#import<QuartzCore/QuartzCore.h>
#import"MyPageViewController.h"
#import "ViewAFriendViewController.h"
#import"LoadingIndicatorView.h"
#import "UIView+Origin.h"
#import "NewsDataManager.h"
#import "CategoryData.h"

#define TRANSITION_DURATION 0.8
#define SUBJECT_MIN_HEIGHT 15
#define PICTURE_SIZE    232

@implementation FittingRoomMoreViewController

@synthesize m_scrollViewMoreInfo;

@synthesize m_BtnComment,m_BtnWishlist,m_BtnFollowOrUnfollow,m_likeBtn;

@synthesize m_BuyBtn;

@synthesize m_dontBuyBtn;

@synthesize m_IboughtItBtn;

//@synthesize m_ChangeInMindBtn;

@synthesize m_messageId;

@synthesize m_dataArray;

@synthesize m_Type;

@synthesize m_ResponseDictionary;

@synthesize m_CheckString;

@synthesize m_ThePhotoView;

@synthesize m_ImageView;

@synthesize MineTrack;

@synthesize m_headerLabel;

@synthesize m_CallBackViewController;

@synthesize m_messageIDsArray;

@synthesize m_FollowOrUnfollowLabel,m_LabelWishlist;

@synthesize m_GreenImageView;
@synthesize m_originatorNameLabel;
@synthesize m_textViewSubject;
@synthesize filter, curPage;
@synthesize whiteLine1, whiteLine2, m_likeLabel, m_currentMessageID;

@synthesize headerLabel, whoWhatTextView,Whowhattitletext;
@synthesize m_leftArrow, m_rightArrow;
@synthesize GetPinnFlag, asyncProductButton2;
QNavigatorAppDelegate *l_appDelegate;

FittingRoomUseCasesViewController *l_obj;
BuyItCommentViewController *l_Obj1;
ShopbeeAPIs *l_requestObj;
LoadingIndicatorView *l_buyItIndicatorView;

//LoadingIndicatorView *l_FittingRoomIndicatorView;

CategoryModelClass *l_objCatModel;

AsyncButtonView *asyncProductButton;

int i,n=4,textViewY=4,imgViewY=200,j=0,messageID1;

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.m_currentMessageID = -1;
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    
	[m_headerLabel setFont:[UIFont fontWithName:kMyriadProRegularFont size:18]];
    
	NSLog(@"fitting room more called\n");
	[self FindtheCurrentIndexInTheArray];
	l_buyItIndicatorView=[LoadingIndicatorView SharedInstance];
	[self performSelector:@selector(btnMoreAction:)];
	//m_unfollowBtn.hidden=YES;
	//[self.view setBackgroundColor:[UIColor colorWithRed:86.0f/255.0f green:167.0f/255.0f blue:187.0f/255.0f alpha:1.0f]];
    firstTime = YES;
	l_requestObj =[[ShopbeeAPIs alloc] init];
	l_buyItIndicatorView =[LoadingIndicatorView SharedInstance];
    [l_buyItIndicatorView startLoadingView:self];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	m_userId=[prefs valueForKey:@"userName"];
	m_ResponseDictionary=[[NSMutableDictionary alloc] init];
	[super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated
{
	[l_appDelegate.m_customView setHidden:YES];
    if (self.m_currentMessageID==-1) 
    {
        self.m_currentMessageID=0;
    }
    
    [self showHideArrowButton];
    
    [l_buyItIndicatorView stopLoadingView];
    fetchingData = NO;
    
}

- (void) updateData
{
    CategoryData *data = [[NewsDataManager sharedManager] getCategoryDataByType:self.m_Type];
    if (data) {
        self.m_messageIDsArray = data.messageIDs;
        numPages = data.numPages;
        totalRecords = data.totalRecords;
        if (fetchingData) {
            [l_buyItIndicatorView stopLoadingView];
            fetchingData = NO;
            [self moveRight];
        } else {
            [self showHideArrowButton];
        }
    }
}

- (void) getAllDataInRow
{
    
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    [self getAllDataInRow];
    
    if (GetPinnFlag==NO)
    {
        m_leftArrow.hidden=YES;
        m_rightArrow.hidden=YES;
    }
    else
    {
        m_leftArrow.hidden=NO;
        m_rightArrow.hidden=NO;
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
	[l_appDelegate.m_customView setHidden:YES]; // to hide the custom bar 
	NSDictionary *tmp_dictionary=[[self.m_dataArray objectAtIndex:0] valueForKey:@"wsMessage"];
    m_headerLabel.text = self.m_Type;//[tmp_dictionary valueForKey:@"subject"];
    //	if (([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"BuzzMessage-BuyItOrNot"] == NSOrderedSame) ||
    //			([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"FittingRoomMessage-BuyItOrNot"] == NSOrderedSame) ||
    //			([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"FittingRoomMessage-BuyItOrNot-reply"] == NSOrderedSame))	{
    //		m_headerLabel.text=[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"];		
    //	}
    //	else if (([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"BuzzMessage-HowDoesThisLook"] == NSOrderedSame) ||
    //			([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"FittingRoomMessage-HowDoesThisLook"] == NSOrderedSame) ||
    //			([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"FittingRoomMessage-HowDoesThisLook-reply"] == NSOrderedSame)) {
    //		m_headerLabel.text=[NSString stringWithFormat:[[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"]];
    //	}
    //    else if (([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"BuzzMessage-CheckThisOut"] == NSOrderedSame) ||
    //			([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"FittingRoomMessage-CheckThisOut"] == NSOrderedSame) ||
    //			([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"FittingRoomMessage-CheckThisOut-reply"] == NSOrderedSame)) {
    //		m_headerLabel.text=[[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."];
    //	}
    //    else if (([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"BuzzMessage-FoundASale"] == NSOrderedSame) ||
    //			([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"FittingRoomMessage-FoundASale"] == NSOrderedSame) ||
    //			([[tmp_dictionary valueForKey:@"subject"] caseInsensitiveCompare:@"FittingRoomMessage-FoundASale-reply"] == NSOrderedSame)) {
    //		m_headerLabel.text=[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"];
    //	}
    
	if ([[[m_dataArray objectAtIndex:0] valueForKey:@"isFollowing"] caseInsensitiveCompare:@"Y"] == NSOrderedSame) 
	{
        follow = YES;
        [m_BtnFollowOrUnfollow setImage:[UIImage imageNamed:@"unfollow_withtext"] forState:UIControlStateNormal];
	}
	else
	{
        follow = NO;
        [m_BtnFollowOrUnfollow setImage:[UIImage imageNamed:@"follow_icon"] forState:UIControlStateNormal];
	}
    
    CGRect newRect;
	if (MineTrack==YES) 
	{	
		NSLog(@"Mine view of \"How does this look?\"");
		
		if ([[tmp_dictionary valueForKey:@"isMessageAppendable"] caseInsensitiveCompare:@"OPEN"] == NSOrderedSame) 
		{
            //			m_BtnComment.frame = CGRectMake(40.0f, 356.0f, 52.0f, 45.0f);
            //			m_BtnWishlist.frame = CGRectMake(125.0f,352.0f, 63.0f,45.0f);
            
            CGRect aRect = m_BtnComment.frame;
            
            aRect.origin.x = whiteLine1.frame.origin.x;
            m_BtnComment.frame = aRect;
            
            aRect = m_BtnWishlist.frame;
            aRect.origin.x = whiteLine2.frame.origin.x;
            m_BtnWishlist.frame = aRect;
            
            whiteLine1.hidden = YES;
            whiteLine2.hidden = YES;
            
			m_LabelWishlist.frame = CGRectMake(120.0f, 384.0f, 61.0f, 24.0f);
			m_LabelWishlist.text = @"Save this";
            m_LabelWishlist.hidden = YES;
			m_IboughtItBtn.hidden=YES;
			m_BuyBtn.hidden=YES;
			m_dontBuyBtn.hidden=YES;
			m_likeBtn.hidden=YES;
			m_BtnComment.hidden=NO;
            newRect = CGRectMake(204, 371, m_BtnWishlist.frame.size.width, m_BtnWishlist.frame.size.height);
            [m_BtnWishlist setFrame:newRect];
			m_BtnWishlist.hidden=NO;
			m_BtnFollowOrUnfollow.hidden=YES;
			//m_GreenImageView.hidden=NO;
			//m_FollowOrUnfollowLabel.hidden=YES;
			
		}
		else 
		{
			m_IboughtItBtn.hidden=YES;
			m_BuyBtn.hidden=YES;
			m_dontBuyBtn.hidden=YES;
			m_likeBtn.hidden=YES;
			m_BtnComment.hidden=YES;
			m_BtnWishlist.hidden=YES;
			m_BtnFollowOrUnfollow.hidden=YES;
			//m_GreenImageView.hidden=YES;
			//m_FollowOrUnfollowLabel.hidden=YES;
		}
        
	}
	else 
	{
		m_IboughtItBtn.hidden=YES;
		m_BuyBtn.hidden=NO;
		m_dontBuyBtn.hidden=NO;
        newRect = CGRectMake(166, 371, m_BtnWishlist.frame.size.width, m_BtnWishlist.frame.size.height);
        [m_BtnWishlist setFrame:newRect];
        m_BtnWishlist.hidden=NO;
		//MineTrack=NO;
		m_LabelWishlist.text = @"I Want this!";
	}
    
	[l_appDelegate.m_customView setHidden:YES];
}

#pragma mark -
#pragma mark custom

-(IBAction) btnDontBuyAction:(id)sender
{
	[m_ResponseDictionary setValue:@"" forKey:@"isFavour"];
	[m_ResponseDictionary setObject:[NSNumber numberWithInt:m_messageId] forKey:@"messageId"];
	[self showAlertView:nil alertMessage:@"Thanks! Would you like to leave a review for your friend?" tag:100 cancelButtonTitle:@"NO" otherButtonTitles:@"OK"];
	//UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Thanks!Would you like to leave a review for your friend?" 
    //												 delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"OK",nil];
    //	[alert show];
    //	[alert setTag:9];
    //	[alert release];
    
}

-(IBAction)btnBuyAction:(id)sender{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	[m_ResponseDictionary setValue:@"Y" forKey:@"isFavour"];
	[m_ResponseDictionary setObject:[NSNumber numberWithInt:m_messageId] forKey:@"messageId"];
	[m_ResponseDictionary setValue:m_Type forKey:@"type"];
	[m_ResponseDictionary setValue:@"" forKey:@"responseWords"];
	// [m_ResponseDictionary setValue:m_Type forKey:@"type"];
	[l_buyItIndicatorView startLoadingView:self];
	[l_requestObj addResponseForFittingRoomRequest:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_userId msgresponse:m_ResponseDictionary];
	[l_requestObj release];
	l_requestObj=nil;
	//[self showAlertView:nil alertMessage:@"Thanks! Would you like to leave a review for your friend?" tag:10 cancelButtonTitle:@"NO" otherButtonTitles:@"OK"];
    
}
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

-(IBAction)btnBackAction:(id)sender
{
    if (self.m_CallBackViewController && [self.m_CallBackViewController respondsToSelector:@selector(updateCurrentPage:)]) {
        [self.m_CallBackViewController performSelector:@selector(updateCurrentPage:) withObject:[NSNumber numberWithInt:curPage]];
    }
	[self.navigationController popViewControllerAnimated:YES];
	
}
-(NSString *)formatWhoWhattitle:(NSDictionary*)datatitle
{
    //    if ([[datatitle objectForKey:@"oped"] length] > 0)
    //        return [datatitle objectForKey:@"oped"];
    
    
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    NSString *data = [datatitle objectForKey:@"who"];
    if ([data length] > 0) {
        
        [string appendFormat:@"•%@:\n",@"Who"];
    }
    data = [datatitle objectForKey:@"what"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"What"];
        
    }
    data = [datatitle objectForKey:@"when"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"When"];
    }
    data = [datatitle objectForKey:@"where"];
    if ([data length] > 0) {
        if ([data length]<=24)
        {
            [string appendFormat:@"•%@:\n",@"Where"];
        }
        else
        {
            [string appendFormat:@"•%@:\n\n",@"Where"];
        }        
    }
    data = [datatitle objectForKey:@"how"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"How"];
        
    }
    data = [datatitle objectForKey:@"why"];
    if ([data length] > 0) {
        [string appendFormat:@"•%@:\n",@"Why"];
    }
    return string;
    
}

- (NSString *) formatWhoWhatText:(NSDictionary *)dataDict
{
    
    if ([[dataDict objectForKey:@"oped"] length] > 0)
        return [dataDict objectForKey:@"oped"];
    
    
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    NSString *data = [dataDict objectForKey:@"who"];
    if ([data length] > 0) {
        
        [string appendFormat:@" %@\n",data];
    }
    data = [dataDict objectForKey:@"what"];
    if ([data length] > 0) {
        
        [string appendFormat:@" %@\n",data];
        
    }
    data = [dataDict objectForKey:@"when"];
    if ([data length] > 0) {
        
        [string appendFormat:@" %@\n",[self dateReFormated:data]];
    }
    data = [dataDict objectForKey:@"where"];
    if ([data length] > 0) {
        [string appendFormat:@" %@\n",data];
        
    }
    data = [dataDict objectForKey:@"how"];
    if ([data length] > 0) {
        [string appendFormat:@" %@\n",data];
        
    }
    data = [dataDict objectForKey:@"why"];
    if ([data length] > 0) {
        [string appendFormat:@" %@\n",data];
        
        
    }
    return string;
}

-(NSString*)dateReFormated:(NSString*)date
{
    NSMutableString *string = [[[NSMutableString alloc] init] autorelease];
    if ([date intValue])
    {
        NSArray *mainarray=[date componentsSeparatedByString:@" "];
        NSArray *timearray=[[mainarray objectAtIndex:1]componentsSeparatedByString:@":"];
        NSString *timeformate=[NSString stringWithFormat:@"%@ %@:%@",[mainarray objectAtIndex:0],[timearray objectAtIndex:0],[timearray objectAtIndex:1]];
        NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
        [dateFormate setDateFormat:@"YYYY:mm:dd HH:mm"];
        NSDate *dateValue=[dateFormate dateFromString:timeformate];
        [dateFormate setDateFormat:@"MMM dd, HH:mm a"];
        [string appendFormat:@"%@",[dateFormate stringFromDate:dateValue]];
        [dateFormate release]; 
        //return string;
    }
    else
    {
        [string appendFormat:@"%@",date];  
    }
    return string;
}
-(IBAction)btnMoreAction:(id)sender
{
	NSDictionary *tmp_dictionary=[self.m_dataArray objectAtIndex:0];
	NSArray *messageResponseArray=[tmp_dictionary valueForKey:@"messageResponseList"];
	int Count=[messageResponseArray count];
	
	
	self.m_scrollViewMoreInfo.scrollsToTop = NO;
	self.m_scrollViewMoreInfo.contentOffset = CGPointMake(0,0);
	self.m_scrollViewMoreInfo.contentSize = CGSizeMake(m_scrollViewMoreInfo.contentSize.width,m_scrollViewMoreInfo.frame.size.height);//-50);
	
	UISwipeGestureRecognizer *recognizer; // To detect the swipe on the scroll view.
	
	
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	[m_scrollViewMoreInfo addGestureRecognizer:recognizer];
	recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:UISwipeGestureRecognizerDirectionRight];
	[m_scrollViewMoreInfo addGestureRecognizer:recognizer];
    //=================== Adding product picture
    float imageYPos = 32;
	self.asyncProductButton2 = [[[AsyncButtonView alloc] initWithFrame:CGRectMake(39, imageYPos, PICTURE_SIZE, PICTURE_SIZE)] autorelease];
	self.asyncProductButton2.messageTag=1000;
    self.asyncProductButton2.layer.shadowColor = [UIColor grayColor].CGColor;
    self.asyncProductButton2.layer.shadowOffset = CGSizeMake(1, -1);
    self.asyncProductButton2.layer.shadowOpacity = 0.5;
	//asyncButtonTop.isCatView=YES;
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[tmp_dictionary valueForKey:@"productImageUrl"]];
    
	//Bharat: 11/23/11: US44 - Check if product info exists, if yes, show the image2
	id wsProductDictionary = [tmp_dictionary valueForKey:@"wsProduct"];
	NSString * prodDiscountInfoStr = nil;
	NSString * prodTagLineStr = nil;
	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
		id imageUrlsArr = [((NSDictionary *)wsProductDictionary) valueForKey:@"imageUrls"];
		if ((imageUrlsArr != nil) && ([imageUrlsArr isKindOfClass:[NSArray class]] == YES) && 
            ([((NSArray *)imageUrlsArr) count] > 1) && ([[imageUrlsArr objectAtIndex:1] length] > 1)) {
			productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[((NSArray *)imageUrlsArr) objectAtIndex:1]];
		}	
		id discInf = [((NSDictionary *)wsProductDictionary) valueForKey:@"discountInfo"];
		if ((discInf != nil) && ([discInf isKindOfClass:[NSString class]] == YES)) {
			prodDiscountInfoStr=[NSString stringWithFormat:@"%@",discInf];
		}	
		id tagLine = [((NSDictionary *)wsProductDictionary) valueForKey:@"productTagLine"];
		if ((tagLine != nil) && ([tagLine isKindOfClass:[NSString class]] == YES)) {
			prodTagLineStr=[NSString stringWithFormat:@"%@",tagLine];
		}	
	}
    
    
	[self.asyncProductButton2 loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked:) btnText:@""];
	[m_scrollViewMoreInfo addSubview:self.asyncProductButton2];
    
    //================  Adding story texts - headline & who, what...
    CGFloat yHeight = imageYPos + self.asyncProductButton2.frame.size.height + 10;
    NSDictionary *temp_text_dict=[tmp_dictionary valueForKey:@"wsBuzz"];
    if (temp_text_dict && [temp_text_dict isKindOfClass:[NSDictionary class]]) {
        headerLabel.text = [temp_text_dict objectForKey:@"headline"];
        
        whoWhatTextView.text = [self formatWhoWhatText:temp_text_dict];
        Whowhattitletext.text=[self formatWhoWhattitle:temp_text_dict];
        
        if ([whoWhatTextView.text length] > 0) {
            
            [Whowhattitletext sizeToFit];
            Whowhattitletext.frame=CGRectMake(Whowhattitletext.frame.origin.x, Whowhattitletext.frame.origin.y, Whowhattitletext.frame.size.width, Whowhattitletext.contentSize.height);
            
            [whoWhatTextView sizeToFit];
            whoWhatTextView.frame = CGRectMake(whoWhatTextView.frame.origin.x, whoWhatTextView.frame.origin.y, whoWhatTextView.frame.size.width, whoWhatTextView.contentSize.height);
            yHeight = whoWhatTextView.frame.origin.y + whoWhatTextView.frame.size.height + 10;
            
        }
    }
    
    //=================  Adding originator name
    NSString *UserName=[tmp_dictionary valueForKey:@"originatorName"];	
	
	self.m_originatorNameLabel.text=[NSString stringWithFormat:@"%@:",UserName];
	self.m_originatorNameLabel.font = [UIFont fontWithName:kMyriadProRegularFont size:13];
    [self.m_originatorNameLabel changeViewYOriginTo:yHeight];
    //=================  Adding originator picture
    
    NSString *Url=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_dataArray objectAtIndex:0]valueForKey:@"originatorThumbImageUrl"]];
	
	NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
	
    //Nava adopting new layout
	asyncButtonTop = [[[AsyncButtonView alloc]
                       initWithFrame:CGRectMake(32,yHeight,48,48)] autorelease];
	asyncButtonTop.messageTag=10001;
	asyncButtonTop.isCatView=YES;
    asyncButtonTop.layer.shadowColor = [UIColor grayColor].CGColor;
    asyncButtonTop.layer.shadowOffset = CGSizeMake(1, -1);
    asyncButtonTop.layer.shadowOpacity = 0.5;
	[asyncButtonTop loadImageFromURL:temp_loadingUrl target:self action:@selector(ProductBtnClicked:) btnText:@""];
	//asyncButton.backgroundColor=[UIColor whiteColor];
	[m_scrollViewMoreInfo addSubview:asyncButtonTop];
    
    //=================  Adding likes text
    
	if ([[tmp_dictionary valueForKey:@"favourCount"] intValue]>1) 
	{
		m_likeLabel.text=[NSString stringWithFormat:@"%d Likes",[[tmp_dictionary valueForKey:@"favourCount"] intValue] ];
	}
	else {
		m_likeLabel.text=[NSString stringWithFormat:@"%d Like",[[tmp_dictionary valueForKey:@"favourCount"] intValue] ];
		
	}
	self.m_likeLabel.textColor=[UIColor darkGrayColor];
	self.m_likeLabel.backgroundColor=[UIColor clearColor];
    [self.m_likeLabel changeViewYOriginTo:yHeight];
    
    //=================  Adding text subject
    NSDictionary *temp_dictionary=[tmp_dictionary valueForKey:@"wsMessage"];
	self.m_textViewSubject.text=[temp_dictionary valueForKey:@"bodyMessage"];
	self.m_textViewSubject.textColor=[UIColor darkGrayColor];
    
	[m_textViewSubject sizeToFit];
    [self.m_textViewSubject changeViewYOriginTo:self.m_originatorNameLabel.frame.origin.y +
     self.m_originatorNameLabel.frame.size.height + 2];
    
	float extra_height = 0.0f;
	if (m_textViewSubject.contentSize.height > SUBJECT_MIN_HEIGHT) {
		extra_height = m_textViewSubject.contentSize.height - SUBJECT_MIN_HEIGHT;
		m_textViewSubject.frame = CGRectMake(m_textViewSubject.frame.origin.x, m_textViewSubject.frame.origin.y, m_textViewSubject.frame.size.width, m_textViewSubject.contentSize.height);
	}
    
#if ADD_ELAPSED_TIME
    // Is not in use currently
	m_elapsedTimelabel=[[UILabel alloc] initWithFrame:CGRectMake(m_originatorNameLabel.frame.origin.x+m_originatorNameLabel.frame.size.width+10,5, 120, 20)];
#if 0
Bharat: DE116: Commented out for now (so that user does not see the timeline nad reject app for old updates)
	m_elapsedTimelabel.text=[temp_dictionary valueForKey:@"elapsedTime"];
#endif
	m_elapsedTimelabel.textColor=[UIColor colorWithRed:.69f green:.72f blue:.73f alpha:1.0f];
	m_elapsedTimelabel.font=[UIFont fontWithName:kHelveticaBoldFont size:12];
	m_elapsedTimelabel.backgroundColor=[UIColor clearColor];
	m_elapsedTimelabel.textAlignment=UITextAlignmentLeft;
	[m_scrollViewMoreInfo addSubview:m_elapsedTimelabel];
#endif
	//[tmp_elapsedTimelabel release];
    //	tmp_elapsedTimelabel=nil;
	//[tmp_dontBuyLabel release];
    //	tmp_dontBuyLabel=nil;
    //	[tmp_likeImageView release];
    //	tmp_likeImageView=nil;
    
    
	
    //    float imageYPos = 70.0;
    //	if ((m_textViewSubject.frame.origin.y+m_textViewSubject.frame.size.height) > imageYPos)
    //		imageYPos = m_textViewSubject.frame.origin.y+m_textViewSubject.frame.size.height;
    //
    // Nava - adopting to different layout
	
#if ADD_DISCOUNT_INFO
	//Bharat: 11/24/11: Adding discount info 
	if ((prodDiscountInfoStr != nil) && ([prodDiscountInfoStr length] > 0)) {
		UILabel *temp_lblDiscount=[[UILabel alloc]initWithFrame:CGRectMake(50,yHeight,215,20)]; //190,5,85,20)];
		temp_lblDiscount.numberOfLines = 1;
		[temp_lblDiscount setText:prodDiscountInfoStr]; //@"10% discount"];
		[temp_lblDiscount setTextAlignment:UITextAlignmentCenter];
		[temp_lblDiscount setTextColor:[UIColor darkGrayColor]];
		[temp_lblDiscount setBackgroundColor:[UIColor clearColor]];
		[temp_lblDiscount setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_DISCOUNTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_DISCOUNTLABEL_FONT_SIZE_KEY]]];
		//[m_scrollViewMoreInfo addSubview:temp_lblDiscount];
		[temp_lblDiscount release];
		yHeight += (20+5);
	}
	
	//Bharat: 11/24/11: Adding tagline info 
	if ((prodTagLineStr != nil) && ([prodTagLineStr length] > 0)) {
		UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(50,yHeight,215,20)]; //190,5,85,20)];
		temp_lblTitle.numberOfLines = 1;
		[temp_lblTitle setText:prodTagLineStr]; //@"10% discount"];
		[temp_lblTitle setTextAlignment:UITextAlignmentCenter];
		[temp_lblTitle setTextColor:[UIColor darkGrayColor]];
		[temp_lblTitle setBackgroundColor:[UIColor clearColor]];
		[temp_lblTitle setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];
		//[m_scrollViewMoreInfo addSubview:temp_lblTitle];
		[temp_lblTitle release];
		yHeight += (5+20);
	}
#endif    
	
    //    float imageYPos = 70.0;
    //	if ((m_textViewSubject.frame.origin.y+m_textViewSubject.frame.size.height) > imageYPos)
    //		imageYPos = m_textViewSubject.frame.origin.y+m_textViewSubject.frame.size.height;
    
    
	//Bharat: 11/24/11: Adjust height automatically
	//imgViewY=350 + extra_height;
    yHeight = m_textViewSubject.frame.origin.y + m_textViewSubject.frame.size.height + 20;
	imgViewY=yHeight;
	
	int counter;
	
	
	counter=0;
	for(int a=0;a<Count;a++)
	{
		
		
		NSString	*temp_string=[[messageResponseArray objectAtIndex:a] valueForKey:@"responseMessageBody"];
		if (![temp_string isEqualToString:@""]) 
		{
			UIImageView *temp_imgView2=[[UIImageView alloc]initWithFrame:CGRectMake(10, imgViewY, 274, 59)];
			temp_imgView2.tag = 32;	
			
			UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(40, 0, 190, 40)];
			textView.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
			//textView.font = [UIFont fontWithName:kFontName size:13];
			[textView setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_SIZE_KEY]]];
			textView.backgroundColor = [UIColor clearColor];
			textView.delegate = self;
			textView.tag = 33;
			textView.text=temp_string;
			textView.returnKeyType = UIReturnKeyDefault;
			textView.keyboardType = UIKeyboardTypeDefault; 
			textView.scrollEnabled = YES;
			textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
			textView.editable=YES; 
			textView.userInteractionEnabled=YES;
			
			
			if (counter%2==0) 
			{
				temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.frame.size.height);
				temp_imgView2.image=[[UIImage imageNamed:@"white_message_box.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
			}
			else 
			{
				imgViewY=imgViewY-25;
				temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.frame.size.height);
				temp_imgView2.image=[[UIImage imageNamed:@"white_message_box_rotated.png"] stretchableImageWithLeftCapWidth:50 topCapHeight:30];
			}
			
            
			[temp_imgView2 addSubview:textView];
			
			[m_scrollViewMoreInfo addSubview:temp_imgView2];
			
			temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.contentSize.height);
			
			
			textView.frame =CGRectMake(40, 0, 190,textView.contentSize.height);
			
			if (counter%2!=0) 
			{
				textView.frame =CGRectMake(40, 12, 190,textView.contentSize.height);
			}
			
			
			UILabel *tmp_elapsedTime=[[UILabel alloc] initWithFrame:CGRectMake(47, textView.contentSize.height -5, 190, 12)];
			
			if (counter%2!=0) 
			{
				tmp_elapsedTime.frame=CGRectMake(47, textView.contentSize.height +8, 190, 12);
				
			}
			
			tmp_elapsedTime.text=[NSString stringWithFormat:@"Added by %@ ",[[messageResponseArray objectAtIndex:a]valueForKey:@"responseCustomerName"]];
			tmp_elapsedTime.backgroundColor=[UIColor clearColor];	
			tmp_elapsedTime.textColor=[UIColor lightGrayColor];
			tmp_elapsedTime.font=[UIFont fontWithName:kFontName size:10];
			
			[temp_imgView2 addSubview:tmp_elapsedTime];
			
			[tmp_elapsedTime release];
			tmp_elapsedTime=nil;
			
			
			imgViewY=imgViewY+textView.contentSize.height+40;
			
			AsyncButtonView* SenderImage = [[[AsyncButtonView alloc]
                                            initWithFrame:CGRectMake(5,5,35,35)] autorelease];
			
			if (counter%2!=0) 
			{
				SenderImage.frame=CGRectMake(5, 20, 35, 35);
			}
			SenderImage.messageTag = a;
			[temp_imgView2 addSubview:SenderImage];
			NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[messageResponseArray objectAtIndex:a] valueForKey:@"thumbCustomerUrl"]];
			NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,60,32]];
			[SenderImage loadImageFromURL:tempLoadingUrl target:self action:@selector(commenterImagePressed:) btnText:nil];
			
			temp_imgView2.userInteractionEnabled = YES;
			NSLog(@"imgViewY>>>>>>>>>>>>>>>>%d",imgViewY);
			//textViewY=textViewY+4;
			NSLog(@"iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii>>>>>>>>%d",i);
			[temp_imgView2 release];
			temp_imgView2=nil;
			[textView release];
			textView=nil;
			
			
			
			counter++;
		}
	}
	//Bharat: 11/26/11: Set absolute value, do not set increments
	m_scrollViewMoreInfo.contentSize = CGSizeMake(m_scrollViewMoreInfo.contentSize.width,imgViewY+10);
    
    /*m_likeImageView=[[UIImageView alloc ]initWithFrame:CGRectMake(239, asyncProductButton2.frame.origin.y + 20, 78, 22)];
     [m_likeImageView setBackgroundColor:[UIColor clearColor]];
     m_likeImageView.image=[UIImage imageNamed:@"star_like_rate_blue_bg.png"];
     //[m_scrollViewMoreInfo addSubview:m_likeImageView];
     [self.m_scrollViewMoreInfo addSubview:m_likeImageView];*/
	
	
	//m_likeLabel=[[UILabel alloc] initWithFrame:CGRectMake(25, 3, 45, 10)];
    
    
    
	//[m_scrollViewMoreInfo addSubview:m_likeLabel];
    
	//Bharat: 11/23/11: US44 - Adding sunburst and discount info to pop-over view
    wsProductDictionary = nil;
	wsProductDictionary = [tmp_dictionary valueForKey:@"wsProduct"];
	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
		id discountInfo = [((NSDictionary *)wsProductDictionary) valueForKey:@"discountInfo"];
		if ((discountInfo != nil) && ([discountInfo isKindOfClass:[NSString class]] == YES) && ([discountInfo length] > 0)) {
			UILabel *temp_lblDiscount=[[UILabel alloc]initWithFrame:CGRectMake(111,25,85,40)]; //190,5,85,20)];
			temp_lblDiscount.numberOfLines = 2;
			[temp_lblDiscount setText:discountInfo]; //@"10% discount"];
			[temp_lblDiscount setTextAlignment:UITextAlignmentCenter];
			[temp_lblDiscount setTextColor:[UIColor darkGrayColor]];
			[temp_lblDiscount setBackgroundColor:[UIColor clearColor]];
			[temp_lblDiscount setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_DISCOUNTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_DISCOUNTLABEL_FONT_SIZE_KEY]]];
			
			UIImageView *temp_yellowImgView=[[UIImageView alloc]init] ;
			temp_yellowImgView.contentMode=UIViewContentModeScaleAspectFit;
			if([discountInfo length]>7)
			{
				temp_yellowImgView.frame=CGRectMake(105,3,99,80);
				[temp_yellowImgView setImage:[UIImage imageNamed:@"yellow_tag.png"]];
				
			}
			else
			{
				[temp_yellowImgView setImage:[UIImage imageNamed:@"yellow_tag_small.png"]];
				temp_yellowImgView.frame=CGRectMake(113,5,83,79);
				temp_lblDiscount.frame=CGRectMake(118, 25, 70,40);
			}
			//[m_ThePhotoView addSubview:temp_yellowImgView];
			//[m_ThePhotoView addSubview:temp_lblDiscount];
			[temp_yellowImgView release];
			[temp_lblDiscount release];
		}
		id productTagLine = [((NSDictionary *)wsProductDictionary) valueForKey:@"productTagLine"];
		if ((productTagLine != nil) && ([productTagLine isKindOfClass:[NSString class]] == YES) && ([productTagLine length] > 0)) {
			UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10,390,290,60)];
			temp_lblTitle.lineBreakMode = UILineBreakModeWordWrap;
			temp_lblTitle.numberOfLines = 0;
			[temp_lblTitle setText:productTagLine];
			temp_lblTitle.textAlignment = UITextAlignmentCenter;
			[temp_lblTitle setTextColor:[UIColor darkGrayColor]];
			[temp_lblTitle setBackgroundColor:[UIColor clearColor]];
			[temp_lblTitle setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];
            
			[m_ThePhotoView addSubview:temp_lblTitle];
			[temp_lblTitle release];
		}
	}
    
}


-(IBAction) btnFollowAction:(id)sender
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_UserName=[prefs valueForKey:@"userName"];
	NSDictionary *tmp_Dict=[[m_dataArray objectAtIndex:0] valueForKey:@"wsMessage"];
	NSString *tmp_originatorName=[tmp_Dict valueForKey:@"sentFromCustomer"];
	
	NSArray *tmp_array=[[NSArray alloc] initWithObjects:tmp_originatorName,nil];
	
	if ([tmp_UserName caseInsensitiveCompare:tmp_originatorName] == NSOrderedSame) 
	{
		UIAlertView *Tmp_alertView=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"You cannot Follow yourself." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[Tmp_alertView show];
		[Tmp_alertView release];
		Tmp_alertView=nil;
        
	}
	else 
	{
		[l_buyItIndicatorView startLoadingView:self];
		[l_requestObj addFollowing:@selector(CallBackMethodforfollow:responseData:) tempTarget:self customerid:tmp_UserName followers:tmp_array];
		
	}
    
	[l_requestObj release];
	l_requestObj=nil;
	//[l_buyItIndicatorView ]
	[tmp_array release];
	tmp_array=nil;
    follow = YES;
    [m_BtnFollowOrUnfollow setImage:[UIImage imageNamed:@"unfollow_withtext"] forState:UIControlStateNormal];
}
-(IBAction)btnUnfollowAction:(id)sender
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_UserName=[prefs valueForKey:@"userName"];
	NSDictionary *tmp_Dict=[[m_dataArray objectAtIndex:0] valueForKey:@"wsMessage"];
	NSString *tmp_originatorName=[tmp_Dict valueForKey:@"sentFromCustomer"];
	[l_buyItIndicatorView startLoadingView:self];
	[l_requestObj unfollowAFriend:@selector(CallBackMethodforUnfollow:responseData:) tempTarget:self userid:tmp_UserName following:tmp_originatorName];
	[l_requestObj release];
	l_requestObj=nil;
    follow = NO;
    [m_BtnFollowOrUnfollow setImage:[UIImage imageNamed:@"follow_icon"] forState:UIControlStateNormal];
	//m_followBtn.hidden=NO	;
    //	m_unfollowBtn.hidden=YES;
}
-(IBAction) btnFlagItAction
{
	UIActionSheet *alertSheet=[[UIActionSheet alloc] initWithTitle:@"Please flag with care" delegate:self cancelButtonTitle:@"Cancel" 
											destructiveButtonTitle:nil otherButtonTitles:@"Misorganised",@"Prohibited",@"Spam/Overpost",nil];
	[alertSheet showFromTabBar:self.tabBarController.tabBar];
	alertSheet.tag=1;
	[alertSheet release];
	alertSheet=nil;
	
	
	
}
-(IBAction)btnIboughtItAction
{
	NSLog(@"I bought it\n");
	l_requestObj=[[ShopbeeAPIs alloc] init];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_string=[prefs valueForKey:@"userName"];
	[l_buyItIndicatorView startLoadingView:self];
	[l_requestObj iboughtitforAparticularItem:@selector(CallBackMethodForIboughtit:responseData:) tempTarget:self userid:tmp_string messageId:m_messageId];
	
	IboughtItBtnClicked=YES;
	//UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Your message was sent successfully" 
    //												 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //	
    //	[alert show];
    //	[alert setTag:20];
    //	[alert release];
	[l_requestObj release];
	l_requestObj=nil;
	
}
-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage tag:(NSInteger)Tagvalue cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles
{
	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles ,nil];
	tempAlert.tag=Tagvalue;
	[tempAlert show];
	[tempAlert release];
	tempAlert=nil;
}

-(void)ProductBtnClicked:(id)sender
{
	
	if ([sender tag]==1000)
	{
		[m_ThePhotoView setFrame:CGRectMake(5, 25, 310, 450)];
		[l_appDelegate.window addSubview:m_ThePhotoView];
		UIImage *tempImage=[self.asyncProductButton2.m_btnIcon backgroundImageForState:UIControlStateNormal];
		//m_ImageView.contentMode=UIViewContentModeScaleAspectFill;
		if (!tempImage) 
		{
			tempImage=[UIImage imageNamed:@"no-image"];
		}
		
        
		m_ImageView.image= tempImage;
		[self initialDelayEnded];
	}
	else if([sender tag]==10001)
	{
		//if (MineTrack==YES) 
        //		{
        //			MyPageViewController *temp_mypage=[[MyPageViewController alloc] init];
        //			temp_mypage.BackButtonTrack=YES;
        //			[self.navigationController pushViewController:temp_mypage animated:YES];
        //			[temp_mypage release];
        //			temp_mypage=nil;
        //			NSLog(@"go to mypage");
        //				
        //		}
        //		else 
        //		{
        ViewAFriendViewController *tempViewAFriend=[[ViewAFriendViewController alloc]init];
        NSDictionary *tmp_dictionary=[m_dataArray objectAtIndex:0];
        tempViewAFriend.m_Email=[[tmp_dictionary valueForKey:@"wsMessage"] valueForKey:@"sentFromCustomer"];
        tempViewAFriend.m_strLbl1=[tmp_dictionary valueForKey:@"originatorName"];
        tempViewAFriend.messageLayout = NO;
        [self.navigationController pushViewController:tempViewAFriend animated:YES];
        [tempViewAFriend release];
        tempViewAFriend=nil;
        
        
		//}
        
	}
	
}

- (void) commenterImagePressed:(id)sender
{
    ViewAFriendViewController *tempViewAFriend=[[ViewAFriendViewController alloc]init];
    NSDictionary *tmp_dictionary=[m_dataArray objectAtIndex:0];
	NSArray *messageResponseArray=[tmp_dictionary valueForKey:@"messageResponseList"];
    
    NSDictionary *responseDict = [messageResponseArray objectAtIndex:((UIButton *)sender).tag];
    
    tempViewAFriend.m_Email=[responseDict valueForKey:@"responseCustomerId"];
    tempViewAFriend.m_strLbl1=[responseDict valueForKey:@"responseCustomerName"];
    tempViewAFriend.messageLayout = NO;
    [self.navigationController pushViewController:tempViewAFriend animated:YES];
    [tempViewAFriend release];
    tempViewAFriend=nil;
}


-(void)initialDelayEnded
{
    m_ThePhotoView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    m_ThePhotoView.alpha = 1.0;
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
	m_ThePhotoView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    [UIView commitAnimations];
}

- (void)bounce1AnimationStopped
{
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
	m_ThePhotoView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
    [UIView commitAnimations];
}

- (void)bounce2AnimationStopped {
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	m_ThePhotoView.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
}

- (IBAction)CloseBtnAction {
	[m_ThePhotoView removeFromSuperview];	
}

- (IBAction)ChangeInMindAction {
	l_requestObj=[[ShopbeeAPIs alloc] init];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_string=[prefs valueForKey:@"userName"];
	[l_buyItIndicatorView startLoadingView:self];
	[l_requestObj announceAChangeInMind:@selector(CallBackMethodForIboughtit:responseData:) tempTarget:self userid:tmp_string messageId:m_messageId];
	
	//ChangeInMindBtnClicked=YES;
	IboughtItBtnClicked=NO;
	[l_requestObj release];
	l_requestObj=nil;
	
}

- (IBAction)FindtheCurrentIndexInTheArray {
	self.m_currentMessageID=[m_messageIDsArray indexOfObject:[NSNumber numberWithInt:m_messageId] ];
	//m_currentMessageID=[ MessageID integerValue];
}

- (IBAction)FollowOrUnfollowAction { // If user is not following a particular person show follow, else show unfollow.
	if ( !follow ) 
		[self btnFollowAction:nil];
	else 
		[self btnUnfollowAction:nil];
}

// GDL: Performing this the third time caused a crash?
// The user has tapped the "Comment" button.
- (IBAction)CommentBtnAction {
	BuyItCommentViewController *commentController = [[BuyItCommentViewController alloc] init];
    
    // GDL: I was releasing one of the components of m_MainArray.
	commentController.m_MainArray = m_dataArray;
	commentController.m_FavourString = @"";
	commentController.m_CallBackViewController = self;//**
    
	[self.navigationController pushViewController:commentController animated:YES];
	[commentController release];
}

// GDL: The user has tapped the "Save this button". Why isn't it called "Add to Wishlist"?
- (IBAction)btnWishListAction {
	UIActionSheet *alert=[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil
											otherButtonTitles:@"Leave it Public",@"Keep it Private",nil];
	[alert setTag:2];
	[alert showFromTabBar:self.tabBarController.tabBar];
	[alert release];
}

-(void)addToWishListPrivate:(id)sender
{
	l_requestObj=[[ShopbeeAPIs alloc] init];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_userName=[prefs valueForKey:@"userName"];
	NSString *tmp_visibility=@"private";
	NSDictionary *tmp_ResponseDict=[[m_dataArray objectAtIndex:0] valueForKey:@"wsMessage"];
	NSString *body_Message=[tmp_ResponseDict valueForKey:@"bodyMessage"];
	NSNumber *MessageID1=[NSNumber numberWithInt:m_messageId];
	//NSString *tmp_subject=[tmp_ResponseDict valueForKey:@"subject"];
	//NSString *tmp_comments=[NSString stringWithFormat:@"%@%@",tmp_subject,body_Message];
	
	NSDictionary *tmp_dict=[NSDictionary dictionaryWithObjectsAndKeys:tmp_userName,@"custid",body_Message,@"comments",MessageID1,@"msgid",tmp_visibility,@"visibility",nil];
	
	//..NSDictionary *tmp_dict=[NSDictionary dictionaryWithObjectsAndKeys:tmp_userName,@"custid",body_Message,@"comments",tmp_visibility,@"visibility",nil];
	NSLog(@"%@",tmp_dict);
	NSLog(@"%@",l_objCatModel.m_strId);
	[l_buyItIndicatorView startLoadingView:self];
	[l_requestObj addToWishListWithFittingRoomData:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	//..[l_requestObj addWishListWithoutProduct:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	[l_objCatModel release];
	l_objCatModel=nil;
	[l_requestObj release];
	l_requestObj=nil;
	
}

-(void)addToWishListPublic:(id)sender
{
	
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_userName=[prefs valueForKey:@"userName"];
	NSString *tmp_visibility=@"public";
	NSDictionary *tmp_ResponseDict=[[m_dataArray objectAtIndex:0] valueForKey:@"wsMessage"];
	NSString *body_Message=[tmp_ResponseDict valueForKey:@"bodyMessage"];
	NSNumber *MessageID=[NSNumber numberWithInt:m_messageId];
	//NSString *tmp_subject=[tmp_ResponseDict valueForKey:@"subject"];
	//NSString *tmp_comments=[NSString stringWithFormat:@"%@%@",tmp_subject,body_Message];
	//NSLog(@"%@",m_messageId);
	NSDictionary *tmp_dict=[NSDictionary dictionaryWithObjectsAndKeys:tmp_userName,@"custid",body_Message,@"comments",MessageID,@"msgid",tmp_visibility,@"visibility",nil];
	
	
	NSLog(@"%@",tmp_dict);
	//	NSLog(@"%@",l_objCatModel.m_strId);
	[l_buyItIndicatorView startLoadingView:self];
	
	// http://66.28.216.132/salebynow/json.htm?action=addToWishListWithFittingRoomData&wishlist={"custid":"test21@gmail.com","comments":"This Product is very good .. ","msgid":"22","visibility":"private"}
	l_requestObj=[[ShopbeeAPIs alloc] init];
	[l_requestObj addToWishListWithFittingRoomData:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	//[l_requestObj addWishListWithoutProduct:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:tmp_dict];
	[l_objCatModel release];
	l_objCatModel=nil;
	[l_requestObj release];
	l_requestObj=nil;
}

/*
 In response to a swipe gesture, show the image view appropriately then move the image view in the direction of the swipe as it fades out.
 */

#pragma mark Handle swipe

- (IBAction)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer
{
	if(recognizer.direction==UISwipeGestureRecognizerDirectionRight)
	{
		[self RightMovement:nil];
	}
	else
	{
		[self LeftMovement:nil];
	}
    
}

#pragma mark Right swipe
-(void)moveRight
{
    if (fetchingData) {
        return;
    }
    
    NSLog(@"right movement\n");
    if (self.m_currentMessageID < m_messageIDsArray.count-1) 
        self.m_currentMessageID++;
    else {
        if (curPage + 1 < numPages) {
            [l_buyItIndicatorView startLoadingView:self];
            self.curPage++;
            [[NewsDataManager sharedManager] getOneCategoryData:self type:self.m_Type filter:self.filter number:24 numPage:self.curPage addToExisting:YES];
            fetchingData = YES;
            return;
        }    
    }
    
    // Show-Hide arrow button
	[self showHideArrowButton];
    
    if (self.m_currentMessageID<=m_messageIDsArray.count-1)  
    {
        messageID1=[[m_messageIDsArray objectAtIndex:self.m_currentMessageID] intValue];
        m_messageId=messageID1;
        NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
        NSString *tmp_String=[prefs2 stringForKey:@"userName"];
        l_requestObj=[[ShopbeeAPIs alloc] init];
        [l_buyItIndicatorView startLoadingView:self];
        [self.view setUserInteractionEnabled:FALSE];
        
        [l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:messageID1];
        [l_requestObj release];
        l_requestObj=nil;		
    } }

-(IBAction)RightMovement:(id)sender
{
    [self moveRight];	    
}

#pragma mark Left swipe

-(void)moveLeft
{
	NSLog(@"left movement\n");
	
    if (self.m_currentMessageID >= 0) 
		self.m_currentMessageID--;
    
    // Hide/Show Arrow
	[self showHideArrowButton];
	if (self.m_currentMessageID>=0)  
	{
		messageID1=[[m_messageIDsArray objectAtIndex:self.m_currentMessageID] intValue];
		m_messageId=messageID1;
		NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
		NSString *tmp_String=[prefs2 stringForKey:@"userName"];
		l_requestObj=[[ShopbeeAPIs alloc] init];
		[l_buyItIndicatorView startLoadingView:self];
		[self.view setUserInteractionEnabled:FALSE];
		[l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:messageID1];
		[l_requestObj release];
		l_requestObj=nil;
		
	} 
    
}

-(IBAction)LeftMovement:(id)sender
{
    [self moveLeft];
}


#pragma mark Right swipe


-(void) showHideArrowButton { 
    
    // Left Arrow Button
    if (GetPinnFlag)
    {
        
        if (m_currentMessageID == 0){
            m_leftArrow.hidden = YES;
        }
        else {
            m_leftArrow.hidden = NO;
        }
        
        // Right Arrow Button
        
        if (m_currentMessageID == m_messageIDsArray.count-1){
            if (self.curPage < numPages) {
                m_rightArrow.hidden = NO;
            } else
                m_rightArrow.hidden = YES;
        }
        else{
            m_rightArrow.hidden = NO;
        }
    }
    else
    {
        m_rightArrow.hidden=YES;
        m_leftArrow.hidden=YES;
    }
}


-(IBAction)UpdateScrollViewContent
{
	//m_scrollViewMoreInfo.contentSize = CGSizeMake(m_scrollViewMoreInfo.contentSize.width,m_scrollViewMoreInfo.frame.size.height);//-50);
	int Count=0;		
    
	for (UIView *temp in m_scrollViewMoreInfo.subviews)
	{
		//if ([temp isKindOfClass:[UIImageView class]]) {
		if ([[m_scrollViewMoreInfo viewWithTag:32] superview]) {
			[[m_scrollViewMoreInfo viewWithTag:32] removeFromSuperview];	
		}	
		
		if ([[m_scrollViewMoreInfo viewWithTag:33] superview]) {
			[[m_scrollViewMoreInfo viewWithTag:33] removeFromSuperview];	
		}	
		if ([[m_scrollViewMoreInfo viewWithTag:44] superview]) {
			[[m_scrollViewMoreInfo viewWithTag:44] removeFromSuperview];	
		}	
		
		if ([[m_scrollViewMoreInfo viewWithTag:45] superview]) {
			[[m_scrollViewMoreInfo viewWithTag:45] removeFromSuperview];	
		}	
		if ([[m_scrollViewMoreInfo viewWithTag:5555] superview]) {
			[[m_scrollViewMoreInfo viewWithTag:5555] removeFromSuperview];	
		}	
	}
	
	NSDictionary *tmp_dictionary1=[self.m_dataArray objectAtIndex:0];
	NSArray *messageResponseArray=[tmp_dictionary1 valueForKey:@"messageResponseList"];
	for (int j=0; j<messageResponseArray.count; j++) 
	{
		if ([[messageResponseArray objectAtIndex:j] valueForKey:@"responseMessageBody"]) 
		{
			Count++;
		}
	}
	if (Count==0) 
	{
		//m_scrollViewMoreInfo.frame=CGRectMake(6,38,308,310);
	}
	
    //=================== Adding product picture
	NSString *productImageUrl1=[NSString stringWithFormat:@"%@%@",kServerUrl,[tmp_dictionary1 valueForKey:@"productImageUrl"]];
	//Bharat: 11/23/11: US44 - Check if product info exists, if yes, show the image2
	NSString * prodDiscountInfoStr = nil;
	NSString * prodTagLineStr = nil;
	id wsProductDictionary = [tmp_dictionary1 valueForKey:@"wsProduct"];
	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
		id imageUrlsArr = [((NSDictionary *)wsProductDictionary) valueForKey:@"imageUrls"];
		if ((imageUrlsArr != nil) && ([imageUrlsArr isKindOfClass:[NSArray class]] == YES) && 
            ([((NSArray *)imageUrlsArr) count] > 1) && ([[imageUrlsArr objectAtIndex:1] length] > 1)) {
			productImageUrl1=[NSString stringWithFormat:@"%@%@",kServerUrl,[((NSArray *)imageUrlsArr) objectAtIndex:1]];
		}	
		id discInf = [((NSDictionary *)wsProductDictionary) valueForKey:@"discountInfo"];
		if ((discInf != nil) && ([discInf isKindOfClass:[NSString class]] == YES)) {
			prodDiscountInfoStr=[NSString stringWithFormat:@"%@",discInf];
		}	
		id tagLine = [((NSDictionary *)wsProductDictionary) valueForKey:@"productTagLine"];
		if ((tagLine != nil) && ([tagLine isKindOfClass:[NSString class]] == YES)) {
			prodTagLineStr=[NSString stringWithFormat:@"%@",tagLine];
		}	
	}
    
	[self.asyncProductButton2 loadImageFromURL:[NSURL URLWithString:productImageUrl1] target:self action:@selector(ProductBtnClicked:) btnText:@""];
    
    float imageYPos = 32;
	self.asyncProductButton2.frame = CGRectMake(self.asyncProductButton2.frame.origin.x, imageYPos, self.asyncProductButton2.frame.size.width, self.asyncProductButton2.frame.size.height);
    
    //================  Adding story texts - headline & who, what...
    CGFloat yHeight = imageYPos + self.asyncProductButton2.frame.size.height + 10;
    NSDictionary *temp_text_dict=[tmp_dictionary1 objectForKey:@"wsBuzz"];
    if (temp_text_dict && [temp_text_dict isKindOfClass:[NSDictionary class]]) {
        headerLabel.text = [temp_text_dict objectForKey:@"headline"];
        whoWhatTextView.text = [self formatWhoWhatText:temp_text_dict];
        
        Whowhattitletext.text=[self formatWhoWhattitle:temp_text_dict];
        
        if ([whoWhatTextView.text length] > 0) {
            [Whowhattitletext sizeToFit];
            Whowhattitletext.frame=CGRectMake(Whowhattitletext.frame.origin.x, Whowhattitletext.frame.origin.y, Whowhattitletext.frame.size.width, Whowhattitletext.contentSize.height);
            
            [whoWhatTextView sizeToFit];
            whoWhatTextView.frame = CGRectMake(whoWhatTextView.frame.origin.x, whoWhatTextView.frame.origin.y, whoWhatTextView.frame.size.width, whoWhatTextView.contentSize.height);
            yHeight = whoWhatTextView.frame.origin.y + whoWhatTextView.frame.size.height + 10;
            
        }
    } else {
        whoWhatTextView.text = @"";
        Whowhattitletext.text = @"";
        headerLabel.text = @"";
    }
    
    
    //=================  Adding originator name
	NSString *OriginatorName=[tmp_dictionary1 valueForKey:@"originatorName"];	
	
	m_originatorNameLabel.text=[NSString stringWithFormat:@"%@:",OriginatorName];
    [self.m_originatorNameLabel changeViewYOriginTo:yHeight];
	
    //=================  Adding originator picture
	NSString *Url=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_dataArray objectAtIndex:0]valueForKey:@"originatorThumbImageUrl"]];
	
	NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
	
	[asyncButtonTop loadImageFromURL:temp_loadingUrl target:self action:@selector(ProductBtnClicked:) btnText:@""];
    [asyncButtonTop changeViewYOriginTo:yHeight];
    
    //=================  Adding likes text
	if ([[tmp_dictionary1 valueForKey:@"favourCount"] intValue]>1) 
	{
		m_likeLabel.text=[NSString stringWithFormat:@"%d Likes",[[tmp_dictionary1 valueForKey:@"favourCount"] intValue] ];
	}
	else {
		m_likeLabel.text=[NSString stringWithFormat:@"%d Like",[[tmp_dictionary1 valueForKey:@"favourCount"] intValue] ];
		
	}
    [self.m_likeLabel changeViewYOriginTo:yHeight];
	
    //=================  Adding text subject
	NSDictionary *temp_dictionary1=[tmp_dictionary1 valueForKey:@"wsMessage"];
	
	m_textViewSubject.text=[temp_dictionary1 valueForKey:@"bodyMessage"];
	[m_textViewSubject sizeToFit];
	float extra_height = 0.0f;
	if (m_textViewSubject.contentSize.height > SUBJECT_MIN_HEIGHT) {
		extra_height = m_textViewSubject.contentSize.height - SUBJECT_MIN_HEIGHT;
	}
	m_textViewSubject.frame = CGRectMake(m_textViewSubject.frame.origin.x, m_textViewSubject.frame.origin.y, m_textViewSubject.frame.size.width, m_textViewSubject.contentSize.height);
	
#if ADD_ELAPSED_TIME
#if 0
Bharat: DE116: Commented out for now (so that user does not see the timeline nad reject app for old updates)
	m_elapsedTimelabel.text=[temp_dictionary1 valueForKey:@"elapsedTime"];
#endif
	//Bharat: 11/17/2011: elapsed time size fixed
	//m_elapsedTimelabel.frame = CGRectMake(m_elapsedTimelabel.frame.origin.x, 22+SUBJECT_MIN_HEIGHT-10+extra_height, m_elapsedTimelabel.frame.size.width, m_elapsedTimelabel.frame.size.height);
	
	m_dontBuyLabel.text=[NSString stringWithFormat:@"%d",[[tmp_dictionary1 valueForKey:@"notFavourCount"] intValue] ];
	//asyncButtonTop.isCatView=YES;
	
	yHeight = imageYPos + PICTURE_SIZE + 10;
	//Bharat: 11/24/11: Adding discount info 
	if ((prodDiscountInfoStr != nil) && ([prodDiscountInfoStr length] > 0)) {
		UILabel *temp_lblDiscount=[[UILabel alloc]initWithFrame:CGRectMake(50,yHeight,215,20)]; //190,5,85,20)];
		temp_lblDiscount.numberOfLines = 1;
		[temp_lblDiscount setText:prodDiscountInfoStr]; //@"10% discount"];
		[temp_lblDiscount setTextAlignment:UITextAlignmentCenter];
		[temp_lblDiscount setTextColor:[UIColor darkGrayColor]];
		[temp_lblDiscount setBackgroundColor:[UIColor clearColor]];
		[temp_lblDiscount setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_DISCOUNTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_DISCOUNTLABEL_FONT_SIZE_KEY]]];
		//[m_scrollViewMoreInfo addSubview:temp_lblDiscount];
		[temp_lblDiscount release];
		yHeight += (20+5);
	}
	
	//Bharat: 11/24/11: Adding tagline info 
	if ((prodTagLineStr != nil) && ([prodTagLineStr length] > 0)) {
		UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(50,yHeight,215,20)]; //190,5,85,20)];
		temp_lblTitle.numberOfLines = 1;
		[temp_lblTitle setText:prodTagLineStr]; //@"10% discount"];
		[temp_lblTitle setTextAlignment:UITextAlignmentCenter];
		[temp_lblTitle setTextColor:[UIColor darkGrayColor]];
		[temp_lblTitle setBackgroundColor:[UIColor clearColor]];
		[temp_lblTitle setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];
		//[m_scrollViewMoreInfo addSubview:temp_lblTitle];
		[temp_lblTitle release];
		yHeight += (5+20);
	}
#endif	
	
	//Bharat: 11/24/11: Adjust height automatically
	//imgViewY=350 + extra_height;
	
    // Nava adopting to a new layout
    yHeight = m_textViewSubject.frame.origin.y + m_textViewSubject.frame.size.height + 20;
	imgViewY=yHeight;
	
	int counter;
	
	counter=0;
	for(int a=0;a<Count;a++)
	{
		
		
		NSString	*temp_string=[[messageResponseArray objectAtIndex:a] valueForKey:@"responseMessageBody"];
		if (![temp_string isEqualToString:@""]) 
		{
			
			UIImageView *temp_imgView2=[[UIImageView alloc]initWithFrame:CGRectMake(10, imgViewY, 274, 59)];
			temp_imgView2.tag = 44;	
			UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(40, 0, 190, 40)];
			textView.tag = 45;
			textView.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
			textView.font = [UIFont fontWithName:kFontName size:13];
			textView.backgroundColor = [UIColor clearColor];
			textView.delegate = self;
			textView.text=temp_string;
			textView.returnKeyType = UIReturnKeyDefault;
			textView.keyboardType = UIKeyboardTypeDefault; 
			textView.scrollEnabled = YES;
			textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
			textView.editable=YES; 
			textView.userInteractionEnabled=YES;
			
			
			if (counter%2==0) 
			{
				temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.frame.size.height);
				temp_imgView2.image=[[UIImage imageNamed:@"white_message_box.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
			}
			else 
			{
				imgViewY=imgViewY-25;
				temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.frame.size.height);
				temp_imgView2.image=[[UIImage imageNamed:@"white_message_box_rotated.png"] stretchableImageWithLeftCapWidth:50 topCapHeight:30];
			}
			
			
			[temp_imgView2 addSubview:textView];
			
			[m_scrollViewMoreInfo addSubview:temp_imgView2];
			
			temp_imgView2.frame=CGRectMake(10, imgViewY, 274, 25+textView.contentSize.height);
			
			
			textView.frame =CGRectMake(40, 0, 190,textView.contentSize.height);
			textView.frame =CGRectMake(40, 0, 190,textView.contentSize.height);
			
			if (counter%2!=0) 
			{
				textView.frame =CGRectMake(40, 12, 190,textView.contentSize.height);
				textView.frame =CGRectMake(40, 12, 190,textView.contentSize.height);
			}
			
			
			UILabel *tmp_elapsedTime=[[UILabel alloc] initWithFrame:CGRectMake(47, textView.contentSize.height -5, 190, 12)];
			
			if (counter%2!=0) 
			{
				tmp_elapsedTime.frame=CGRectMake(47, textView.contentSize.height +8, 190, 12);
				
			}
			
			//Bharat: DE116: Commented out for now (so that user does not see the timeline nad reject app for old updates)
			//tmp_elapsedTime.text=[NSString stringWithFormat:@"Added %@ by %@ ",[[messageResponseArray objectAtIndex:a]valueForKey:@"elapsedTime"],[[messageResponseArray objectAtIndex:a]valueForKey:@"responseCustomerName"]];
			tmp_elapsedTime.text=[NSString stringWithFormat:@"Added by %@ ",[[messageResponseArray objectAtIndex:a]valueForKey:@"responseCustomerName"]];
			tmp_elapsedTime.backgroundColor=[UIColor clearColor];	
			tmp_elapsedTime.textColor=[UIColor lightGrayColor];
			tmp_elapsedTime.font=[UIFont fontWithName:kFontName size:10];
			
			[temp_imgView2 addSubview:tmp_elapsedTime];
			//[tmp_elapsedTime setFrame:CGRectMake(30, textView.contentSize.height -5, 190, 12)];
			
			[tmp_elapsedTime release];
			tmp_elapsedTime=nil;
			
			imgViewY=imgViewY+textView.contentSize.height+40;
			
			AsyncButtonView* SenderImage = [[[AsyncButtonView alloc]
                                             initWithFrame:CGRectMake(5,5,35,35)] autorelease];
			
			if (counter%2!=0) 
			{
				SenderImage.frame=CGRectMake(5, 20, 35, 35);
			}
			SenderImage.messageTag = a;
			[temp_imgView2 addSubview:SenderImage];
			NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[messageResponseArray objectAtIndex:a] valueForKey:@"thumbCustomerUrl"]];
			NSURL *tempLoadingUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@&width=%d&height=%d",temp_strUrl,60,32]];
			//((UIImageView *)[asyncImage viewWithTag:101]).image=[UIImage imageNamed:@"loading.png"];
			[SenderImage loadImageFromURL:tempLoadingUrl target:self action:@selector(commenterImagePressed:) btnText:nil];
			
			temp_imgView2.userInteractionEnabled = YES;
			
			
			NSLog(@"imgViewY>>>>>>>>>>>>>>>>%d",imgViewY);
			//textViewY=textViewY+4;
			NSLog(@"iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii>>>>>>>>%d",i);
			//[temp_imgView2 release];
            //			temp_imgView2=nil;
			//[textView release];
            //			textView=nil;
            //			
			
			counter++;
		}
		
	}
	//Bharat: 11/26/11: Set absolute value, do not set increments
	m_scrollViewMoreInfo.contentSize = CGSizeMake(m_scrollViewMoreInfo.contentSize.width,imgViewY+10);
	
	//[self.view addSubview:m_scrollViewMoreInfo];
	//[m_moreDetailView addSubview:m_scrollView];
	//[m_scrollViewMoreInfo release];
	//m_scrollViewMoreInfo=nil;
	
	[l_buyItIndicatorView stopLoadingView];
	
}

#pragma mark -
#pragma mark scroll view delegates 
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	
	
	
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	
	
}

#pragma mark -
#pragma mark call back methods

-(void)requestCallBackMethodforWishList:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_buyItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) {
		
		[self showAlertView:nil alertMessage:@"Your item has been added to Wishlist successfully." tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
	tempString=nil;
	
}

-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData
{		
	NSLog(@"data downloaded");
	[self.view setUserInteractionEnabled:YES];
	[l_buyItIndicatorView stopLoadingView];
	
	if ([responseCode intValue]==200) 
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		self.m_dataArray = [tempString JSONValue];
		[self UpdateScrollViewContent];
		
		NSString *Following=[[self.m_dataArray objectAtIndex:0]valueForKey:@"isFollowing"] ;
		
		if ([Following caseInsensitiveCompare:@"N"] == NSOrderedSame)  
		{
            follow = NO;
            [m_BtnFollowOrUnfollow setImage:[UIImage imageNamed:@"follow_icon"] forState:UIControlStateNormal];
		}
		else if([Following caseInsensitiveCompare:@"Y"] == NSOrderedSame)
		{
            follow = YES;
            [m_BtnFollowOrUnfollow setImage:[UIImage imageNamed:@"unfollow_withtext"] forState:UIControlStateNormal];
		}
        
        //m_messageIDsArray = m_messageIDsArrayBuyIt;
        
		//m_type=[tmp_dict valueForKey:@"messageType"];
        //		l_ViewRequestObj.m_Type=m_type;
		//if (MineCheck==1) 
        //		{
        //			l_ViewRequestObj.MineTrack=YES;
        //			l_boughtItRequest.MineTrack=YES;
        //			
        //		}
        //		else {
        //			l_ViewRequestObj.MineTrack=NO;
        //			l_boughtItRequest.MineTrack=NO;
        //		}
		
		//NSLog(@"response string: %@",tempString);
		//if ([m_type caseInsensitiveCompare:kBuyItOrNot] == NSOrderedSame) 
        //		{
        //			l_ViewRequestObj.m_dataArray=tmp_array;
        //			l_ViewRequestObj.m_messageIDsArray=m_messageIDsArrayBuyIt;
        //			[self btnPicturesAction:nil]	;
        //		}
        //		else if([m_type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) 
        //		{
        //			l_boughtItRequest.m_mainArray=tmp_array;
        //			[self btnBoughtItPicturesAction:nil];
        //		}
		if (tempString)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	
	
}

-(void)CallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_buyItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) 
	{
		[self showAlertView:nil alertMessage:@"Your message was sent successfully." tag:11 cancelButtonTitle:@"OK" otherButtonTitles:nil];
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
	tempString=nil;
	
}
-(void)CallBackMethodforfollow:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_buyItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) {
		//[self showAlertView:nil alertMessage:@"Thank you for message" tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
        follow = YES;
        
        [m_BtnFollowOrUnfollow setImage:[UIImage imageNamed:@"unfollow_withtext"] forState:UIControlStateNormal];
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
	tempString=nil;
	
}
-(void)CallBackMethodforUnfollow:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[l_buyItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) 
	{
		//[self showAlertView:nil alertMessage:@"Thank you for message" tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
        follow = NO;
        
        [m_BtnFollowOrUnfollow setImage:[UIImage imageNamed:@"follow_icon"] forState:UIControlStateNormal];
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}	
	[tempString release];
	tempString=nil;
}

-(void)CallBackMethodForIboughtit:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	if ([responseCode intValue]==200) 
	{
		if (IboughtItBtnClicked==YES) 
		{
			NSLog(@"data downloaded");
			NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
			NSLog(@"response string: %@",tempString);
			[l_buyItIndicatorView stopLoadingView];
			if ([responseCode intValue]==200) 
			{
				[self showAlertView:nil alertMessage:@"Your message was sent successfully." tag:123 cancelButtonTitle:@"OK" otherButtonTitles:nil];
			}
			else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
			{
				UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
				[alert show];
				[alert release];
				l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
				[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
			}
			else if([responseCode intValue]!=-1)
			{
				UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
				[alert show];
				[alert release];
				alert=nil;
			}	
			[tempString release];
			tempString=nil;
		}
		//else if(ChangeInMindBtnClicked==YES)
		//		{
		//			m_IboughtItBtn.hidden=NO;
		//			//m_ChangeInMindBtn.hidden=YES;
		//			IboughtItBtnClicked=NO;
		//			ChangeInMindBtnClicked=NO;
		//				[l_buyItIndicatorView stopLoadingView];
		//		}
	}
}
-(void)CallBackMethodforFlagIt:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_buyItIndicatorView stopLoadingView];
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	//[l_buyItIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) 
	{
		[self showAlertView:nil alertMessage:@"Your message was sent successfully." tag:-10 cancelButtonTitle:@"OK" otherButtonTitles:nil];
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
	tempString=nil;
}

#pragma mark -
#pragma mark action sheet delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	l_requestObj=[[ShopbeeAPIs alloc] init];
	
	NSString *flagType;
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	NSString *tmp_userName=[prefs valueForKey:@"userName"];
	if (actionSheet.tag==1) 
	{
		
        
        if (buttonIndex==0) 
        {
            flagType=@"misorganized";
            [l_buyItIndicatorView startLoadingView:self ];
            [l_requestObj FlagAMessage:@selector(CallBackMethodforFlagIt:responseData:) tempTarget:self userid:tmp_userName messageId:m_messageId flagType:flagType];
            
        }
        else if(buttonIndex==1)
        {
            flagType=@"prohibited";
            [l_buyItIndicatorView startLoadingView:self ];
            [l_requestObj FlagAMessage:@selector(CallBackMethodforFlagIt:responseData:) tempTarget:self userid:tmp_userName messageId:m_messageId flagType:flagType];
            
        }
        else if(buttonIndex==2)
        {
            flagType=@"spamOrOverPost";
            [l_buyItIndicatorView startLoadingView:self ];
            [l_requestObj FlagAMessage:@selector(CallBackMethodforFlagIt:responseData:) tempTarget:self userid:tmp_userName messageId:m_messageId flagType:flagType];
            
        }
	}
	else if (actionSheet.tag==2) 
	{
		if (buttonIndex==0) 
		{
			[self addToWishListPublic:nil];
		}
		else if(buttonIndex==1)
		{
			[self addToWishListPrivate:nil];	
		}
		//UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:@"Your item has been added to wishlist successfully" 
		//												 delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
		//		[alert setTag:3];
		//		[alert show];
		//		[alert release];
	}
	[l_requestObj release];
	l_requestObj=nil;
	
	
}

#pragma mark -
#pragma mark alert view delegates

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	l_requestObj= [[ShopbeeAPIs alloc] init];
    
	if (alertView.tag==9) 
	{
		if (buttonIndex==0) 
		{
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Your message has been sent successfully" 
                                                        delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
            [alert show];
            [alert setTag:18];
            [alert release];
		}
		else if(buttonIndex==1)
		{
			l_Obj1=[[BuyItCommentViewController alloc] init];
			l_Obj1.m_MainArray=m_dataArray;
			[self.navigationController pushViewController:l_Obj1 animated:YES];
			[l_Obj1 release];
			l_Obj1=nil;
		}
	}
	if (alertView.tag==10) 
	{ /////////
        if (buttonIndex==0) 
        {	
            [m_ResponseDictionary setValue:@"" forKey:@"responseWords"];
            // [m_ResponseDictionary setValue:m_Type forKey:@"type"];
            [l_buyItIndicatorView startLoadingView:self];
            [l_requestObj addResponseForFittingRoomRequest:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_userId msgresponse:m_ResponseDictionary];
            
            //[self showAlertView:nil alertMessage:@"Your message has been sent successfully"  tag:11 cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Your message has been sent successfully" 
            //											delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
            //				[alert show];
            //				[alert setTag:11];
            //				[alert release];
        }
        else if (buttonIndex==1) 
        {
            l_Obj1=[[BuyItCommentViewController alloc]init];
            l_Obj1.m_MainArray=m_dataArray;
            [m_ResponseDictionary setValue:m_Type forKey:@"type"];
            l_Obj1.m_Dictionary=m_ResponseDictionary;
            l_Obj1.m_FavourString=@"Y";
            l_Obj1.m_CallBackViewController=m_CallBackViewController;
            [self.navigationController pushViewController:l_Obj1 animated:YES];
            [l_Obj1 release];
            l_Obj1=nil;
        }
	}
	if (alertView.tag==11) 
	{
		if (buttonIndex==0) 
		{
			//l_obj=[[FittingRoomUseCasesViewController alloc] init];
			//[self.navigationController popViewControllerAnimated:YES];
			//[l_obj release];
            //			l_obj=nil;
			
		}
	}
	if (alertView.tag==18) {
		if (buttonIndex==0) {
			//l_obj=[[FittingRoomUseCasesViewController alloc] init];
            //			[self.navigationController pushViewController:l_obj animated:YES];
            //			[l_obj release];
            //			l_obj=nil;
			[self.navigationController popViewControllerAnimated:YES];
			//[self.navigationController popToRootViewControllerAnimated:YES];
		}
	}
	if (alertView.tag==20) 
	{
		if (buttonIndex==0)
		{
			m_IboughtItBtn.hidden=NO;
			//m_ChangeInMindBtn.hidden=NO;
		}
	}
	if (alertView.tag==100) 
	{ /////////
		if (buttonIndex==0) {	
			[m_ResponseDictionary setValue:@"" forKey:@"responseWords"];
			// [m_ResponseDictionary setValue:m_Type forKey:@"type"];
			[l_buyItIndicatorView startLoadingView:self];
			[l_requestObj addResponseForFittingRoomRequest:@selector(CallBackMethod:responseData:) tempTarget:self userid:m_userId msgresponse:m_ResponseDictionary];
			
			//[self showAlertView:nil alertMessage:@"Your message has been sent successfully"  tag:11 cancelButtonTitle:@"Ok" otherButtonTitles:nil];
			//UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Your message has been sent successfully" 
			//											delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
			//				[alert show];
			//				[alert setTag:11];
			//				[alert release];
		}
		else if (buttonIndex==1) 
		{
			l_Obj1=[[BuyItCommentViewController alloc]init];
			l_Obj1.m_MainArray=m_dataArray;
			[m_ResponseDictionary setValue:m_Type forKey:@"type"];
			l_Obj1.m_Dictionary=m_ResponseDictionary;
			l_Obj1.m_FavourString=@"N";
			l_Obj1.m_CallBackViewController=m_CallBackViewController;
			[self.navigationController pushViewController:l_Obj1 animated:YES];
			[l_Obj1 release];
			l_Obj1=nil;
		}
	}
	if (alertView.tag==123) 
	{
        {
            m_IboughtItBtn.hidden=NO;
			// m_ChangeInMindBtn.hidden=NO;
            
		}
	}
	[l_requestObj release];
	l_requestObj=nil;
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    
    self.m_dontBuyBtn = nil;
    self.m_IboughtItBtn = nil;
    //self.m_ChangeInMidBtn = nil;
    self.m_BuyBtn = nil;
    self.m_ThePhotoView = nil;
    self.m_ImageView = nil;
    self.m_headerLabel = nil;
	
	self.m_messageIDsArray = nil;
	self.m_BtnWishlist = nil;
	self.m_BtnComment = nil;
	self.m_BtnFollowOrUnfollow = nil;
	self.m_likeBtn = nil;
    self.m_leftArrow = nil;
	self.m_rightArrow = nil;
    self.Whowhattitletext=nil;
}

- (void)dealloc {
    [m_scrollViewMoreInfo release];
    [asyncProductButton2 release];
    [asyncButtonTop release];
	[m_BtnComment release];
	[m_BtnFollowOrUnfollow release];
	[m_likeBtn release];
	[m_leftArrow release];
    [m_rightArrow release];
    [m_dontBuyBtn release];
    [m_BuyItBtn release];
    [m_IboughtItBtn release];
    [m_BuyBtn release];
    [m_ResponseDictionary release];
    //[m_userId release];
    //[m_Type release];
	if (m_dataArray) 
	{
		[m_dataArray release];	
	}
    
	if(m_messageIDsArray)
	{
		[m_messageIDsArray release];
	}
	
    [m_CheckString release];
    [m_ThePhotoView release];
    //   [m_ImageView release];
    //[m_headerLabel release];
    [m_CallBackViewController release];
    [headerLabel release];
    [whoWhatTextView release];
    [Whowhattitletext release];
	[super dealloc];
}

@end
