//
//  BuzzSalesShoutsStep1.h
//  QNavigator
//
//  Created by softprodigy on 03/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BuzzSalesShoutsStep1 : UIViewController<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UITextFieldDelegate> {

	UIImagePickerController *m_imgPicker;
	UIView *m_btnStepsView;
	UIButton *btnStep1;
	UIButton *btnStep2;
	UIButton *btnStep3;
	UITextView *m_textView;
	UILabel *lbl_CountText;
	UISearchBar *m_searchBar;
	UITableView *m_tableView;
	NSArray  *tempArray;
	UIView *m_enterNewStoreView;
	UIButton *m_BtnAddNewStore;
	UITextField *m_storeNameTextField;
	UITextField *m_AddressTextfield;
	UITextField *m_CityTextfield;
	UITextField *m_StateTextfield;
	UITextField *m_ZipcodeTextfield;
	UITextField *m_PhoneTextfield;
}
@property(nonatomic,retain)IBOutlet UITextField *m_storeNameTextField;
@property(nonatomic,retain)IBOutlet UITextField *m_AddressTextfield;
@property(nonatomic,retain)IBOutlet UITextField *m_CityTextfield;
@property(nonatomic,retain)IBOutlet UITextField *m_StateTextfield;
@property(nonatomic,retain)IBOutlet UITextField *m_ZipcodeTextfield;
@property(nonatomic,retain)IBOutlet UITextField *m_PhoneTextfield;

@property(nonatomic,retain)IBOutlet UIButton *m_BtnAddNewStore;
@property(nonatomic,retain)IBOutlet UIView *m_enterNewStoreView;
@property(nonatomic,retain)NSArray *tempArray;
@property(nonatomic,retain)IBOutlet UISearchBar *m_searchBar;
@property(nonatomic,retain)IBOutlet UITableView *m_tableView;
@property(nonatomic,retain)IBOutlet UILabel *lbl_CountText;
@property(nonatomic,retain)IBOutlet UITextView *m_textView;
@property(nonatomic,retain)IBOutlet UIButton *btnStep1;
@property(nonatomic,retain)IBOutlet UIButton *btnStep2;
@property(nonatomic,retain)IBOutlet UIButton *btnStep3;
@property(nonatomic,retain)IBOutlet UIView *m_btnStepsView;

-(IBAction)btnSalesShoutStep1Action:(id)sender;
-(IBAction)btnStep1Action:(id)sender;
-(void)animateViewUpward;
-(void)animateViewDownward;
-(IBAction)BtnAddNewStoreAction:(id)sender;

@end
