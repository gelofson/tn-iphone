//
//  NotificationCell.m
//  QNavigator
//
//  Created by Nicolas Jakubowski on 10/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "NotificationCell.h"
#import "AsyncButtonView.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"

@implementation NotificationCell

@synthesize notification = m_notification;
@synthesize delegate = m_delegate;
@synthesize m_arrowView;
@synthesize m_clockView;
@synthesize m_clockLabel;
@synthesize m_nameLabel;
@synthesize m_messageLabel;

static NSDictionary* BaseAttributes = nil;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    if (!m_notification) {
        return;
    }
    
    if (!m_asyncButtonView) {
        AsyncButtonView* asyncButton = [[[AsyncButtonView alloc]
                                         initWithFrame:CGRectMake(3.5f,3.5f,48.0f,48.0f)] autorelease];
        asyncButton.maskImage = YES;

        [asyncButton loadImageFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kServerUrl,m_notification.thumbImageUrl]]
                               target:self 
                               action:@selector(actionProfileButton:) 
                              btnText:m_notification.firstName];
        
        asyncButton.tag = 1000;
        [self.contentView  addSubview:asyncButton];
        
        m_asyncButtonView = asyncButton;
        
    }else{
        [m_asyncButtonView resetButton];
        
        m_asyncButtonView.maskImage = YES;
        [m_asyncButtonView cancelRequest];
        [m_asyncButtonView loadImageFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kServerUrl,m_notification.thumbImageUrl]]
                               target:self 
                               action:@selector(actionProfileButton:) 
                              btnText:m_notification.firstName];
    }
    
	//Bharat: 11/19/11: show elapsed time separately
    //NSString* notificationText = [NSString stringWithFormat:@"%@ %@", m_notification.notificationMessage,m_notification.elapsedTime];
   
    UIColor* messageTextColor = [UIColor colorWithRed:0.3647f green:0.3647f blue:0.3647f alpha:1.0f];
    //Bharat: changing text colot to match with clock icon
	//UIColor* timeTextColor = [UIColor colorWithRed:0.6824f green:0.6824f blue:0.6824f alpha:1.0f];
    UIColor* timeTextColor = [UIColor colorWithRed:0.28 green:0.28 blue:0.28 alpha:1.0f];

    float cellHeight = [NotificationCell heightForNotification:m_notification];
    
    if (!m_arrowView) {
        m_arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_arrow.png"]];
        //[self.contentView addSubview:m_arrowView];
    }
    
    CGRect r = m_arrowView.frame;
    r.origin.y = (cellHeight - r.size.height) / 2;
    r.origin.x = 320.0f - r.size.width - 5.0f;
    m_arrowView.frame = r;
    
    if (!m_clockView) {
        m_clockView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"clock_icon.png"]];
        m_clockView.frame = CGRectMake(320.0f-50.0f, (cellHeight - 17)/2 , 17, 17);
		[self.contentView addSubview:m_clockView];
    }
    if (!m_clockLabel) {
        m_clockLabel = [[UILabel alloc] initWithFrame:CGRectMake(m_clockView.frame.origin.x+m_clockView.frame.size.width+2,m_clockView.frame.origin.y - 2,300,20)];
		m_clockLabel.backgroundColor = [UIColor clearColor];
		m_clockLabel.font = [UIFont boldSystemFontOfSize:13.0];
		m_clockLabel.textColor = timeTextColor;
		m_clockLabel.textAlignment = UITextAlignmentLeft;
		[self.contentView addSubview:m_clockLabel];
    }
	NSString * str = [NSString stringWithString:m_notification.elapsedTime];
	if ([[str componentsSeparatedByString:@"our"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"our"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else if ([[str componentsSeparatedByString:@"in"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"in"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else if ([[str componentsSeparatedByString:@"ay"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"ay"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else if ([[str componentsSeparatedByString:@"moments"] count] > 1) {
		m_clockLabel.text = @"1m";
	} else if ([[str componentsSeparatedByString:@"eek"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"eek"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else if ([[str componentsSeparatedByString:@"onth"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"onth"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else if ([[str componentsSeparatedByString:@"ear"] count] > 1) {
		m_clockLabel.text = [[[str componentsSeparatedByString:@"ear"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
	} else {
		m_clockLabel.text = str;
	}
    
	if (!m_nameLabel) {
        m_nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(55.0f, 0.0f, 120.0f, 18.0f)];
        m_nameLabel.font = [UIFont boldSystemFontOfSize:13.0f];
        //Bharat: 11/18/11: Set text color for name on notification page
		//m_nameLabel.textColor = [UIColor colorWithRed:0.3843f green:0.5608f blue:0.6000 alpha:1.0f];
        m_nameLabel.textColor = [UIColor colorWithRed:0.28f green:0.28f blue:0.28f alpha:1.0];
        m_nameLabel.backgroundColor = [UIColor clearColor];
		m_nameLabel.textAlignment = UITextAlignmentLeft;
        m_nameLabel.lineBreakMode = UILineBreakModeClip;
		m_nameLabel.numberOfLines = 1;
        [self.contentView addSubview:m_nameLabel];
    }
    //m_nameLabel.text = m_notification.firstName;
    m_nameLabel.text = [m_notification.firstName capitalizedString];
	//[m_nameLabel sizeToFit];
    
    
	if (!m_messageLabel) {
        m_messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(55,m_nameLabel.frame.origin.y+m_nameLabel.frame.size.height,200,cellHeight-26)];
		m_messageLabel.backgroundColor = [UIColor clearColor];
		m_messageLabel.font = [UIFont systemFontOfSize:12.0];
		m_messageLabel.textColor = messageTextColor;
		m_messageLabel.textAlignment = UITextAlignmentLeft;
		m_messageLabel.lineBreakMode = UILineBreakModeWordWrap;
		m_messageLabel.numberOfLines = 2;
		[self.contentView addSubview:m_messageLabel];
    }
	m_messageLabel.text = m_notification.notificationMessage;
	//m_messageLabel.text = @"Bharat test ,es for two line  jkvr  vrw vwl vwelv lw vnlknbrwln lenlwn lewnl";
    
}

- (void)actionProfileButton:(id)sender{
    if (m_delegate) {
        [m_delegate selectedProfileWithEmail:m_notification.email withName:m_notification.firstName];
    }
}

- (void)setNotification:(Notification*)notification{
    if (!notification) {
        return;
    }
    
    @synchronized(self){
        
        [m_notification release];
        m_notification = [notification retain];
        [self setNeedsLayout];
        
    }
}

+ (CGFloat)heightForNotification:(Notification*)n{
    const float min_height = 55.0f;
    
    if (!n) {
        return min_height;
    }
    
	//Bharat:11/19/11: Elapsed time moved top right
    //NSString* notificationText = [NSString stringWithFormat:@"%@ %@ %@", n.firstName, n.notificationMessage, n.elapsedTime];
    NSString* notificationText = [NSString stringWithFormat:@"%@ %@", n.firstName, n.notificationMessage];
    
    CGSize textSize = [notificationText sizeWithFont:[UIFont systemFontOfSize:12.0f] constrainedToSize:CGSizeMake(239.0f, 999999.0f) lineBreakMode:UILineBreakModeWordWrap];
    
    float neededHeight = textSize.height + 26.0f;
    
    return neededHeight < min_height ? min_height : neededHeight;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)dealloc{
    
    if (m_notification) {
        [m_notification release];
        m_notification = nil;
    }
    
    if (m_asyncButtonView) {
        [m_asyncButtonView release];
        m_asyncButtonView = nil;
    }
    
    if (m_messageLayer) {
        [m_messageLayer release];
        m_messageLayer = nil;
    }
    
    if (m_nameLabel) {
        [m_nameLabel release];
        m_nameLabel = nil;
    }
    
    if (m_clockLabel) {
        [m_clockLabel release];
        m_clockLabel = nil;
    }
    
    if (m_messageLabel) {
        [m_messageLabel release];
        m_messageLabel = nil;
    }
    
    if (m_clockView) {
        [m_clockView release];
        m_clockView = nil;
    }
    
    if (m_arrowView) {
        [m_arrowView release];
        m_arrowView = nil;
    }
    
    if (m_delegate) {
        [m_delegate release];
        m_delegate = nil;
    }
    
    [super dealloc];
}

+(NSDictionary*)getBaseAttributes{
    
    if (BaseAttributes) {
        return BaseAttributes;
    }
    
    
    NSDictionary *fontAttributes = [NSDictionary dictionaryWithObjectsAndKeys: 
                                    @"Arial", (NSString *)kCTFontFamilyNameAttribute,
                                    @"Bold", (NSString *)kCTFontStyleNameAttribute,
                                    [NSNumber numberWithFloat:11.0f], (NSString *)kCTFontSizeAttribute,
                                    nil];
    
    CTFontDescriptorRef descriptor = CTFontDescriptorCreateWithAttributes((CFDictionaryRef)fontAttributes);
    CTFontRef font2 = CTFontCreateWithFontDescriptor(descriptor, 0, NULL);
    
    CFRelease(descriptor);
    
    NSDictionary *baseAttributes = [NSDictionary dictionaryWithObject:(id)font2 
                                                               forKey:(NSString *)kCTFontAttributeName];
    
    CFRelease(font2);
    
    BaseAttributes = [baseAttributes retain];
    
    return BaseAttributes;
}

@end
