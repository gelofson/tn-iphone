 //
//  WishListViewController.m
//  QNavigator
//
//  Created by Soft Prodigy on 24/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "WishListViewController.h"
#import "DatabaseManager.h"
#import "tblWishList.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "CategoryModelClass.h"
#import "AuditingClass.h"
#import "QNavigatorAppDelegate.h"
#import "ShopbeeAPIs.h"
#import "SBJSON.h"
#import "QNavigatorAppDelegate.h"
#import "AsyncImageView.h"
#import "WEPopoverContentViewController.h"
#import "UIBarButtonItem+WEPopover.h"
#import "testViewCtrl.h"
#import "LoadingIndicatorView.h"
#import<QuartzCore/QuartzCore.h>
#import "BuzzCaseWishListViewController.h"
#import "AsyncButtonView.h"
#import "NewsDataManager.h"
#import "DetailPageMessageController.h"
#import "CategoryData.h"
#import "Cell.h"
#import "NewCell.h"
@implementation WishListViewController
@synthesize m_tblWishList;
@synthesize context = _context;
@synthesize m_moreDetailView;
@synthesize m_scrollView;

@synthesize l_ArrayWishlist;
@synthesize m_popUpView;
@synthesize m_lblBrandName;
@synthesize m_lblDiscount;
@synthesize m_lblCategory;
@synthesize m_lblAddress;
@synthesize m_lblPhoneNo;
@synthesize m_lblStoreHours;
@synthesize l_closeBtn;
@synthesize m_imageView;
@synthesize m_txtViewComments;
@synthesize popoverController;
@synthesize frame;
@synthesize isFromFriendsView;
@synthesize m_friendsMailString;
@synthesize m_imgViewPhoneIcon;
@synthesize m_imgViewAddressIcon;
@synthesize m_imgViewTimeIcon;
@synthesize m_greenCommentBox;
@synthesize footerView;
@synthesize table;

CategoryModelClass *l_objCatModel;
CLLocation *l_cllocation;
NSMutableArray *l_arrWishList;
BOOL l_isEditingTrue;
UITextView *l_txtInput;
QNavigatorAppDelegate *l_appDelegate;
DatabaseManager *l_db;
UILabel *l_lblRow;
ShopbeeAPIs *l_request;
int cellIndexpath=0;
QNavigatorAppDelegate *l_appDelegate;
NSArray *arr;
NSString *l_isProductExists;
NSString *l_isBuzzExists;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

-(void)viewDidAppear:(BOOL)animated
{
	
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	[super viewDidLoad];
    
    
    
    
    
	//m_lblAddress=[[UILabel alloc]init];
	m_lblAddress.numberOfLines=2;
    
	[m_greenCommentBox.layer  setCornerRadius:6.0f];
	
    
	[m_imgViewAddressIcon setHidden:YES];
	[m_imgViewPhoneIcon setHidden:YES];
	[m_imgViewTimeIcon setHidden:YES];
	
	
	[m_popUpView setHidden:YES];
	[self.view bringSubviewToFront:l_closeBtn];
	[l_closeBtn setHidden:YES];
	l_ArrayWishlist=[[NSMutableArray alloc] init];
	l_db = [[DatabaseManager alloc] init];
	l_isEditingTrue=NO;
	//[m_tblWishList setEditing:YES animated:YES];
	
	//[m_tblWishList setAllowsSelectionDuringEditing:YES];
	
	l_lblRow=[[UILabel alloc]init];
	
	l_lblRow.font=[UIFont fontWithName:kFontName size:14];
	l_lblRow.backgroundColor=[UIColor clearColor];
	l_lblRow.textColor=[UIColor blackColor];
	
	

    
	[l_closeBtn addTarget: self action:@selector(btnCloseAction:) forControlEvents:UIControlEventTouchUpInside];
	l_closeBtn.frame=CGRectMake(270,134,40,40);
	
	//m_lblBrandName.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size: 17.0];
	//kArialBoldFont
	
	m_lblBrandName.font = [UIFont fontWithName:kArialBoldFont size: 18.0];
	m_lblCategory.font = [UIFont fontWithName:kFontName size: 15.0];
	m_lblAddress.font = [UIFont fontWithName:kFontName size: 12.0];
	m_lblPhoneNo.font = [UIFont fontWithName:kFontName size: 12.0];
	m_lblStoreHours.font = [UIFont fontWithName:kFontName size: 12.0];
	m_txtViewComments.font=[UIFont fontWithName:kFontName size:15.0];
	
	m_imageView.frame=CGRectMake(5,0,70,70);
	m_lblBrandName.frame=CGRectMake(100,10,175,20);
	m_lblDiscount.frame=CGRectMake(100,35,200,20);
	m_lblCategory.frame=CGRectMake(10,86,200,20);
	m_lblAddress.frame=CGRectMake(30,105,200,40);
	m_imgViewAddressIcon.frame=CGRectMake(10,112,11,11);
	m_lblPhoneNo.frame=CGRectMake(30,144,200,20);
	m_imgViewPhoneIcon.frame=CGRectMake(10,147,9,13);
	m_lblStoreHours.frame=CGRectMake(30,164,300,20);
	m_imgViewTimeIcon.frame=CGRectMake(10,167,12,11);
	
	m_txtViewComments.frame=CGRectMake(25,60,252,115);
	m_greenCommentBox.frame=CGRectMake(25,20,250,155);
	
    
	
	
	[m_popUpView addSubview:m_imageView];
    
	[m_popUpView addSubview:m_lblBrandName];
	[m_popUpView addSubview:m_lblCategory];
	[m_popUpView addSubview:m_lblDiscount];
	[m_popUpView addSubview:m_lblAddress];
	[m_popUpView addSubview:m_lblPhoneNo];
	[m_popUpView addSubview:m_lblStoreHours];
	[m_popUpView addSubview:m_imgViewTimeIcon];
	[m_popUpView addSubview:m_imgViewPhoneIcon];
	[m_popUpView addSubview:m_imgViewAddressIcon];
	[m_popUpView addSubview:m_greenCommentBox];
	[m_popUpView addSubview:m_txtViewComments];
    
	[m_txtViewComments setHidden:YES];
	[m_greenCommentBox setHidden:YES];
	
	[self.view addSubview:m_popUpView];
	[self.view addSubview:l_closeBtn];
    
    //   [m_tblWishList setAllowsSelectionDuringEditing:YES];
    //	UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] init]; //]WithTarget:self action:@selector(tapped:)];
    //	[tapRecognizer setNumberOfTapsRequired:1];
    //	[tapRecognizer setDelegate:self];
    //	[m_tblWishList addGestureRecognizer:tapRecognizer];
	
    
	currentPopoverCellIndex = -1;
	
}

-(void)viewWillAppear:(BOOL)animated
{
    m_tblWishList.allowsSelectionDuringEditing = YES;
    
	//	l_appDelegate=[QNavigatorAppDelegate SharedInstance];
	NSLog(@"%d",[l_ArrayWishlist count]);
	[self requestFetching];
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	[[LoadingIndicatorView SharedInstance] startLoadingView:self];
	
	l_request=[[ShopbeeAPIs alloc] init];
	
	if (isFromFriendsView==YES) 
	{
		l_isEditingTrue=NO;
		
		[l_request getItemsOnFriendWishList:@selector(requestCallBackMethodforGetItemsOfFriends:responseData:) tempTarget:self customerid:customerID friendId:m_friendsMailString];
		
	}
	else 
	{
		l_isEditingTrue=YES;
		[l_request getItemsOnWishList:@selector(requestCallBackMethodforGetItems:responseData:) tempTarget:self customerid:customerID type:@"mine"];
	}
	
	//[l_request getItemsOnWishList:@selector(requestCallBackMethodforGetItems:responseData:) tempTarget:self customerid:customerID type:@"mine"];
	
	[l_request release];
	l_request=nil;
	
}
- (WEPopoverContainerViewProperties *)improvedContainerViewProperties {
	
	WEPopoverContainerViewProperties *props = [[WEPopoverContainerViewProperties alloc] autorelease];
	NSString *bgImageName = nil;
	CGFloat bgMargin = 0.0;
	CGFloat bgCapSize = 0.0;
	CGFloat contentMargin = 4.0;
	
	bgImageName = @"popoverBg.png";
	//bgImageName = @"";
	// These constants are determined by the popoverBg.png image file and are image dependent
	bgMargin = 50; // margin width of 13 pixels on all sides popoverBg.png (62 pixels wide - 36 pixel background) / 2 == 26 / 2 == 13 
	bgCapSize = 200; // ImageSize/2  == 62 / 2 == 31 pixels
	
	props.leftBgMargin = bgMargin;
	props.rightBgMargin = bgMargin;
	props.topBgMargin = bgMargin;
	props.bottomBgMargin = bgMargin;
	props.leftBgCapSize = bgCapSize;
	props.topBgCapSize = bgCapSize;
	props.bgImageName = bgImageName;
	props.leftContentMargin = contentMargin;
	props.rightContentMargin = contentMargin - 1; // Need to shift one pixel for border to look correct
	props.topContentMargin = contentMargin; 
	props.bottomContentMargin = contentMargin;
	
	props.arrowMargin = 4.0;
	
	props.upArrowImageName = @"popoverArrowUp.png";
	props.downArrowImageName = @"popoverArrowDown.png";
	props.leftArrowImageName = @"popoverArrowLeft.png";
	props.rightArrowImageName = @"popoverArrowRight.png";
	return props;	
}



/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

// GDL: Edited the following two methods.

#pragma mark - memory management

- (void)viewDidUnload {
	[super viewDidUnload];
    
	[self.popoverController dismissPopoverAnimated:NO];
	self.popoverController = nil;
    
    [m_tblWishList release];
    m_tblWishList = nil;
    
    [m_editButton release];
    m_editButton = nil;
    
    self.m_greenCommentBox = nil;
    self.m_imgViewAddressIcon = nil;
    self.m_imgViewPhoneIcon = nil;
    self.m_imgViewTimeIcon = nil;
    self.m_imageView = nil;
    self.l_closeBtn = nil;
    self.m_lblBrandName = nil;
    self.m_lblDiscount = nil;
    self.m_lblCategory = nil;
    self.m_lblAddress = nil;
    self.m_lblPhoneNo = nil;
    self.m_lblStoreHours = nil;
    self.m_txtViewComments = nil;
    self.m_popUpView = nil;
}

- (void)dealloc {
    // Don't release global variables!
    //[l_db release];
    //[l_arrWishList release];
    [m_tblWishList release];
    [m_customEntryView release];
    [_context release];
    [m_editButton release];
    [m_moreDetailView release];
    [m_scrollView release];
    [l_ArrayWishlist release];
	[m_lblBrandName release];
	[m_lblDiscount release];
	[m_lblCategory release];
	[m_lblAddress release];
	[m_lblPhoneNo release];
	[m_lblStoreHours release];
	[l_closeBtn release];
	[m_imageView release];
	[m_txtViewComments release];
    [m_popUpView release];
    [popoverController release];
    [m_friendsMailString release];
	[m_imgViewAddressIcon release];
	[m_imgViewPhoneIcon release];
	[m_imgViewTimeIcon release];
	[m_greenCommentBox release];
    [footerView release];
    [table release];
    
	[super dealloc];
}

#pragma mark -
#pragma mark Custom Methods

-(IBAction)btnCloseAction:(id)sender
{
	[m_imgViewAddressIcon setHidden:YES];
	[m_imgViewPhoneIcon setHidden:YES];
	[m_imgViewTimeIcon setHidden:YES];
	m_tblWishList.userInteractionEnabled=TRUE;
	[l_closeBtn setHidden:YES];
	[m_popUpView setHidden:YES];
}


-(IBAction) m_goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)requestFetching
{
	_context=[l_db managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription 
								   entityForName:@"tblWishList" inManagedObjectContext:_context];
    [fetchRequest setEntity:entity];
    NSError *error;
	if(l_arrWishList!=nil)
	{
		[l_arrWishList release];
		l_arrWishList=nil;
	}
    l_arrWishList = [[NSMutableArray alloc] initWithArray:[_context executeFetchRequest:fetchRequest error:&error]];
	NSLog(@"%@",l_arrWishList);
    [fetchRequest release];
}

- (NSString *)getTextForIndexPath:(NSIndexPath *)indexPath
{
	if (l_isEditingTrue == YES) {
		if(indexPath.row<=l_arrWishList.count)
		{
			return [[l_arrWishList objectAtIndex:indexPath.row-1]Wish_Text];
		}
	} else {
		if(indexPath.row<l_arrWishList.count)
		{
			return [[l_arrWishList objectAtIndex:indexPath.row]Wish_Text];
		}
	}
	return @"";
}

- (CGSize)GetSizeOfText:(NSString *)text withFont:(UIFont *)font
{
	return [text sizeWithFont: font constrainedToSize:CGSizeMake(300,700)];
}

-(void)drawInputView 
{
	if (!m_customEntryView) 
	{
		m_customEntryView=[[UIView alloc]initWithFrame:CGRectMake(0,54,320,471)];
		m_customEntryView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7f];
		[self.view addSubview:m_customEntryView];
		
		l_txtInput=[[UITextView alloc]initWithFrame:CGRectMake(10,10,300,131)];
		[l_txtInput setTextColor:[UIColor blackColor]];
		[l_txtInput setFont:[UIFont fontWithName:kFontName size:22]];
		[m_customEntryView addSubview:l_txtInput];
		[l_txtInput becomeFirstResponder];
		[l_txtInput setDelegate:self];
        
		
	}
	else if(![m_customEntryView superview])
	{
		m_customEntryView.frame=CGRectMake(0,54,320,471);
		[self.view addSubview:m_customEntryView];
		
		l_txtInput.frame=CGRectMake(10,10,300,131);
		l_txtInput.text=@"";
		[l_txtInput becomeFirstResponder];
		[l_txtInput setDelegate:self];
		
		
	}
	
    
    
	UIToolbar *temp_toolbar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,148,320,44)];
	[temp_toolbar setTintColor:[UIColor blackColor]];
	[m_customEntryView addSubview:temp_toolbar];
	
	UIBarButtonItem *btn_Save = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(btnSaveAction:)];
	UIBarButtonItem *btn_Cancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(btnCancelAction:)];
	UIBarButtonItem *btn_space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	
	btn_space.width=60;
	
	NSArray *temp_arrbBn=[[NSArray alloc]initWithObjects:btn_space,btn_Save,btn_Cancel,nil];
	[temp_toolbar setItems:temp_arrbBn]; 
	
	[btn_Save release];
	[btn_Cancel release];
	[btn_space release];
	[temp_arrbBn release];
	[temp_toolbar release];
	
	
}


-(void)btnCancelAction:(id)sender
{
	[m_editButton setSelected:NO];
	[m_customEntryView removeFromSuperview];
}

-(void)btnSaveAction:(id)sender
{
	l_txtInput.text=[l_txtInput.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if(l_txtInput.text.length>0)
	{
		
		UIActionSheet *alertSheet=[[UIActionSheet alloc] initWithTitle:@"public or private" delegate:self cancelButtonTitle:@"Cancel" 
												destructiveButtonTitle:nil otherButtonTitles:@"public",@"private",nil];
        [alertSheet showInView:self.view];
		[alertSheet release];
	}
	else 
	{
		UIAlertView *temp_alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input item description." 
														 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_alert show];
		[temp_alert  release];
		
	}
}


-(void)saveBtnActionforSendingRequest:(NSString *)str
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	[[LoadingIndicatorView SharedInstance] startLoadingView:self];
	
	l_request=[[ShopbeeAPIs alloc] init];
	NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",l_txtInput.text,@"comments",str,@"visibility",nil];
	NSLog(@"%@",dict);
	
	[l_request addWishListWithoutProduct:@selector(requestCallBackMethod:responseData:) tempTarget:self tmpDict:dict];
	
	[l_request release];
	l_request=nil;
}



-(void)btnAddNewAction:(id)sender
{
	[self drawInputView];	
}

-(void)btnEditAction:(id)sender
{
	// Deleting rows from tblwishlist	

	NSManagedObject *managedObject = nil;
	
	if (l_isEditingTrue == YES) {
		managedObject = [l_arrWishList objectAtIndex:[sender tag]-1]; //indexPath.row-1];
	} else {
		managedObject = [l_arrWishList objectAtIndex:[sender tag]]; //indexPath.row];
	}
	[_context deleteObject:managedObject];
	
	// Commit the change.
	NSError *error;
	if (![_context save:&error]) 
	{
		// Handle the error.
	}
	[m_tblWishList reloadData];
	
}

-(IBAction)addBtnPressed:(id)sender
{
	
	NSLog(@"addBtnPressed action");
	
}
-(void)buzzCaseMoreInfo{}

-(void)btnMoreInfoAction:(id)sender
{
	m_tblWishList.userInteractionEnabled=FALSE;
	cellIndexpath=[sender tag];
	[[LoadingIndicatorView SharedInstance] startLoadingView:self];
	
	l_request=[[ShopbeeAPIs alloc] init];
	NSLog(@"l_ArrayWishlist:%@",l_ArrayWishlist);
	
	
	
	if (l_isEditingTrue==TRUE)
	{
		l_isProductExists=[[l_ArrayWishlist objectAtIndex:cellIndexpath]valueForKey:@"isProductExists"];
		//l_isBuzzExists
		l_isBuzzExists=[[l_ArrayWishlist objectAtIndex:cellIndexpath]valueForKey:@"isBuzzExists"];
		
        // GDL: We want to send an integer as wishlistid, not an object.
		[l_request getWishListDetails:@selector(requestCallBackMethodforMoreinfo:responseData:) tempTarget:self wishlistid:[[[l_ArrayWishlist objectAtIndex:cellIndexpath]valueForKey:@"wishListId"] intValue]];
	}
	else 
	{
		l_isProductExists=[[l_ArrayWishlist objectAtIndex:cellIndexpath]valueForKey:@"isProductExists"];
		l_isBuzzExists=[[l_ArrayWishlist objectAtIndex:cellIndexpath]valueForKey:@"isBuzzExists"];
        
        // GDL: We want to send an integer as wishlistid, not an object.
		[l_request getWishListDetails:@selector(requestCallBackMethodforMoreinfo:responseData:) tempTarget:self wishlistid:[[[l_ArrayWishlist objectAtIndex:cellIndexpath]valueForKey:@"wishListId"] intValue]];
	}
    
	
	[l_request release];
	l_request=nil;
	
	
}


#pragma mark -
#pragma mark delegate Methods for textView


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	if(textView.text.length>=150 && range.length==0)
	{
		return NO;
	}
	return YES;
	
}


#pragma mark -
#pragma mark delegate Methods for actionSheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex==0) 
	{
		[self performSelector:@selector(saveBtnActionforSendingRequest:)withObject:@"public"];		
	}
	else if(buttonIndex==1)
	{
		
		[self performSelector:@selector(saveBtnActionforSendingRequest:)withObject:@"private"];
	}
	
}
#pragma mark -
#pragma mark alertview delegates
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex==0) 
	{
		[self.navigationController popViewControllerAnimated:YES];
		
	}
}


#pragma mark -
#pragma mark CallBack Methods
-(void)requestCallBackMethodforGetItemsOfFriends:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSString *str;
	int  temp_responseCode=[responseCode intValue];
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
	NSLog(@"str :%@",str);
	
	
	if(temp_responseCode==200)
	{
		NSLog(@"responseCode:200");
		
        [l_ArrayWishlist removeAllObjects];
		
		[l_ArrayWishlist addObjectsFromArray:[str JSONValue]];
		[table reloadData];
		
	}
	else if([responseCode intValue]==401 || [responseCode intValue]!=-1)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	}
	else if ([responseCode intValue]==kLockedInfoErrorCode) 
	{
		UIAlertView *tmp_AlertView=[[UIAlertView alloc] initWithTitle:kCannotViewFriendInfoTitle message:kCannotViewFriendInfoMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmp_AlertView show];
		[tmp_AlertView release];
		tmp_AlertView=nil;
	}
	
	
}



-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	int  temp_responseCode=[responseCode intValue];
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	if(temp_responseCode==200)
	{
		
		[m_editButton setSelected:NO];
		[m_customEntryView removeFromSuperview];
		NSLog(@"%@",l_ArrayWishlist);
		
		
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *customerID= [prefs objectForKey:@"userName"];
		
		[[LoadingIndicatorView SharedInstance] startLoadingView:self];
		
		l_request=[[ShopbeeAPIs alloc] init];
		[l_ArrayWishlist removeAllObjects];
		[l_request getItemsOnWishList:@selector(requestCallBackMethodforGetItems:responseData:) tempTarget:self customerid:customerID type:@"mine"];
		
		[l_request release];
		l_request=nil;
	}
	else {
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

        [m_editButton setSelected:NO];
		[m_customEntryView removeFromSuperview];
	}
	
}

-(void)requestCallBackMethodfordeletion:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	int  temp_responseCode=[responseCode intValue];
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	
	
	
	if(temp_responseCode==200)
	{
		if([l_ArrayWishlist count]!=1)
		{
			if (l_isEditingTrue == YES) {
				[l_ArrayWishlist removeObjectAtIndex:cellIndexpath-1];
			} else {
				[l_ArrayWishlist removeObjectAtIndex:cellIndexpath];
			}
			NSLog(@"cellIndexpath: %d",cellIndexpath);
			NSLog(@"l_ArrayWishlist:%@",l_ArrayWishlist);
		}
		else if([l_ArrayWishlist count]==1)
		{
			[l_ArrayWishlist removeAllObjects];
		}	
		
		[m_tblWishList reloadData];
	}
	else {
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

        [m_editButton setSelected:NO];
		[m_customEntryView removeFromSuperview];
	}
	
}

-(void)requestCallBackMethodforGetItems:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSString *str;
	int  temp_responseCode=[responseCode intValue];
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
	NSLog(@"str :%@",str);
	
	
	if(temp_responseCode==200)
	{
		NSLog(@"responseCode:200");
		
        [l_ArrayWishlist removeAllObjects];
		
		[l_ArrayWishlist addObjectsFromArray:[str JSONValue]];
		[table reloadData];
		
	}
	else {
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

        [m_editButton setSelected:NO];
		[m_customEntryView removeFromSuperview];
	}
	
	
}


-(void)requestCallBackMethodforUpdation:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	int  temp_responseCode=[responseCode intValue];
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	
	[[LoadingIndicatorView SharedInstance] stopLoadingView];	
	if(temp_responseCode==200)
	{
		
		[m_editButton setSelected:NO];
		[m_customEntryView removeFromSuperview];
		NSLog(@"%@",l_ArrayWishlist);
		
	}
	else {
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

		[m_editButton setSelected:NO];
		[m_customEntryView removeFromSuperview];
	}
	
}
-(IBAction)buzzCaseMoreInfoCloseButtonAction:(id)sender{
    m_tblWishList.userInteractionEnabled=YES;
    [[sender superview] setHidden:YES];
}

-(void)requestCallBackMethodforMoreinfo:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	int  temp_responseCode=[responseCode intValue];
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	if(temp_responseCode==200)
	{
		
		
		NSString *str;
		str=[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
		arr=[[str JSONValue] retain];
		NSLog(@"arr: %@",arr);
		NSLog(@"l_isBuzzExists: %@",l_isBuzzExists);

		int refIdx = 0;
		if (l_isEditingTrue == YES) {
			refIdx = cellIndexpath;//-1;
		} else {
			refIdx = cellIndexpath;
		}


        if ( [l_isBuzzExists isEqualToString:@"YES"])
        {
            m_tblWishList.userInteractionEnabled=TRUE;
                        NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
            NSString *tmp_String=[prefs2 stringForKey:@"userName"];
            
            l_request=[[[ShopbeeAPIs alloc] init] autorelease];
            
            [l_request getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:[[[l_ArrayWishlist objectAtIndex:selectedIndex]valueForKey:@"messageid"]intValue]];

            
            
        }
		
		else if ([l_isProductExists isEqualToString:@"YES"] )		{
			
			[m_imageView setHidden:NO];
			[m_imgViewAddressIcon setHidden:NO];
			[m_imgViewPhoneIcon setHidden:NO];
			[m_imgViewTimeIcon setHidden:NO];
			
			if([l_isProductExists isEqualToString:@"YES"])
			{
				m_lblBrandName.text=[[[arr objectAtIndex:0]valueForKey:@"product"] valueForKey:@"category"];
				m_lblCategory.text=[[[arr objectAtIndex:0]valueForKey:@"product"] valueForKey:@"brandName"];
				m_lblDiscount.text=[[[arr objectAtIndex:0]valueForKey:@"product"] valueForKey:@"discountInfo"];
				
				NSMutableString *add1= [[[arr objectAtIndex:0]valueForKey:@"location"] valueForKey:@"address1"];
				[add1 appendString:@", "];
				[add1 appendString:[[[arr objectAtIndex:0]valueForKey:@"location"] valueForKey:@"city"]];
				[add1 appendString:@", "];
				[add1 appendString:[[[arr objectAtIndex:0]valueForKey:@"location"] valueForKey:@"state"]];
				[add1 appendString:@", "];
				[add1 appendString:[[[arr objectAtIndex:0]valueForKey:@"location"] valueForKey:@"zip"]];
				
				m_lblAddress.text=(NSString *)add1;
				m_imgViewAddressIcon.frame=CGRectMake(10,112,11,11);
                
				if([[[[arr objectAtIndex:0]valueForKey:@"location"] valueForKey:@"phoneNumber"] length]==0 )
				{
					m_lblPhoneNo.text=@"N/A";
					
				}
				else
				{
					m_lblPhoneNo.text=[[[arr objectAtIndex:0]valueForKey:@"location"] valueForKey:@"phoneNumber"];	
				}
                
				if([[[[arr objectAtIndex:0]valueForKey:@"location"] valueForKey:@"storeHours"] length]==0 )
				{
					m_lblStoreHours.text=@"N/A";
                    
				}
				else
				{
					m_lblStoreHours.text=[[[arr objectAtIndex:0]valueForKey:@"location"] valueForKey:@"storeHours"];
				}
				
                
				//m_lblPhoneNo.text=[[[arr objectAtIndex:0]valueForKey:@"location"] valueForKey:@"phoneNumber"];
				//m_lblStoreHours.text=[[[arr objectAtIndex:0]valueForKey:@"location"] valueForKey:@"storeHours"];
				m_txtViewComments.text=@"";
				m_txtViewComments.hidden=YES;
				m_greenCommentBox.hidden=YES;
				
				
				CGRect frame2;
				frame2.size.width=70; frame2.size.height=70;
				frame2.origin.x=10; frame2.origin.y=10;
				
				AsyncImageView* asyncImage = [[[AsyncImageView alloc] initWithFrame:frame2] autorelease];
				NSLog(@"%@",[[[[arr objectAtIndex:0]valueForKey:@"product"] valueForKey:@"imageUrls"]objectAtIndex:0]);
				
				NSMutableString *temp_strUrl;
				
				temp_strUrl=[NSMutableString stringWithFormat:@"%@%@",kServerUrl,[[[[arr objectAtIndex:0]valueForKey:@"product"] valueForKey:@"imageUrls"]objectAtIndex:0]];
				
				NSLog(@"%@",temp_strUrl);
				NSURL *tempLoadingUrl = [NSURL URLWithString:temp_strUrl];
				
				[asyncImage loadImageFromURL:tempLoadingUrl];
				[m_imageView addSubview:asyncImage];
			}
			else
			{
				m_lblBrandName.text=[[[arr objectAtIndex:0]valueForKey:@"wsBuzz"] valueForKey:@"comments"];
				//m_lblBrandName.text=@"";
				m_lblCategory.text=@"";
				m_lblDiscount.text=@"";
				m_lblAddress.text=@"N/A";
				m_imgViewAddressIcon.frame=CGRectMake(10,117,11,11);
				m_lblPhoneNo.text=@"N/A";
				m_lblStoreHours.text=@"N/A";
				
				m_txtViewComments.text=@"";
				m_txtViewComments.hidden=YES;
				m_greenCommentBox.hidden=YES;
				
				CGRect frame2;
				frame2.size.width=70; frame2.size.height=70;
				frame2.origin.x=10; frame2.origin.y=10;
				
				AsyncImageView* asyncImage = [[[AsyncImageView alloc] initWithFrame:frame2] autorelease];
				NSLog(@"%@",[[[arr objectAtIndex:0]valueForKey:@"wsBuzz"] valueForKey:@"thumbImageUrl"]);
				
				NSMutableString *temp_strUrl;
				
				temp_strUrl=[NSMutableString stringWithFormat:@"%@%@",kServerUrl,[[[arr objectAtIndex:0]valueForKey:@"wsBuzz"] valueForKey:@"thumbImageUrl"]];
				
				NSLog(@"%@",temp_strUrl);
				NSURL *tempLoadingUrl = [NSURL URLWithString:temp_strUrl];
				
				[asyncImage loadImageFromURL:tempLoadingUrl];
				[m_imageView addSubview:asyncImage];
				
			}
            
            
            
            [m_popUpView setHidden:NO];
            [l_closeBtn setHidden:NO];
            
		}
		else 
		{
			
			[m_imageView setHidden:YES];
			[m_imgViewAddressIcon setHidden:YES];
			[m_imgViewPhoneIcon setHidden:YES];
			[m_imgViewTimeIcon setHidden:YES];
			
			m_lblBrandName.text=@"";
			m_lblCategory.text=@"";
			m_lblDiscount.text=@"";
			m_lblAddress.text=@"";
			m_imgViewAddressIcon.frame=CGRectMake(10,117,11,11);
			m_lblPhoneNo.text=@"";
			m_lblStoreHours.text=@"";
			[m_greenCommentBox setHidden:NO];
			[m_txtViewComments setHidden:NO];
			NSLog(@"%@",[[[arr objectAtIndex:0]valueForKey:@"wishlist"] valueForKey:@"comments"]);
			//NSMutableString *temp_str = [[NSMutableString alloc] initWithString:@"WishList comments:"];
			//[temp_str appendString:@"\n"];
			NSMutableString *temp_str;
			temp_str=[[[arr objectAtIndex:0]valueForKey:@"wishlist"] valueForKey:@"comments"];
			//[temp_str appendString:[[[arr objectAtIndex:0]valueForKey:@"wishlist"] valueForKey:@"comments"]];
			m_txtViewComments.text=(NSString *)temp_str;	
            
            
            [m_popUpView setHidden:NO];
            [l_closeBtn setHidden:NO];
		}
		
		
		
	}
	else {
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
		[m_editButton setSelected:NO];
		[m_customEntryView removeFromSuperview];
	}
	
}

-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData {		
	NSLog(@"data downloaded");
	if ([responseCode intValue]==200) 
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
        NSDictionary *tmp_dict = [[tmp_array objectAtIndex:0] valueForKey:@"wsMessage"];
		
        CategoryData *catData = [CategoryData convertMessageData:[tmp_array objectAtIndex:0]];
        
        DetailPageMessageController *detailController = [[[DetailPageMessageController alloc] init] autorelease];
        detailController.m_data = catData;
        [detailController setMessagesIdsFromWishListData:[l_ArrayWishlist objectAtIndex:selectedIndex]];
        detailController.m_Type = [[l_ArrayWishlist objectAtIndex:selectedIndex] objectForKey:@"buzzType"];
        detailController.m_messageId = [[tmp_dict objectForKey:@"id"] intValue];
        detailController.filter = POPULAR;
        detailController.mineTrack = NO;
        detailController.curPage = 1;
        //@@@@ To be changed later
        //    self.detailController.fetchingData = YES;
        detailController.m_CallBackViewController = self;
        [self.navigationController pushViewController:detailController animated:YES];
		
        if (tempString!=nil)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
        [[LoadingIndicatorView SharedInstance]stopLoadingView];
}



#pragma mark- UIGridViewDelegate

- (UIView *) gridFooterView
{
    if (!self.footerView) {
        self.footerView = [[[UIView alloc] initWithFrame:CGRectMake(0., 0., 320, 30)] autorelease];
        
    }
    return  self.footerView;
}



- (CGFloat) gridView:(UIGridView *)grid widthForColumnAt:(int)columnIndex
{
	return 80;
}

- (CGFloat) gridView:(UIGridView *)grid heightForRowAt:(int)rowIndex
{
	return 115;
}

- (NSInteger) numberOfColumnsOfGridView:(UIGridView *) grid
{
	return 4;
}


- (NSInteger) numberOfCellsOfGridView:(UIGridView *) grid
{
	return [l_ArrayWishlist count];
}

- (UIGridViewCell *) gridView:(UIGridView *)grid cellForRowAt:(int)rowIndex AndColumnAt:(int)columnIndex
{
	NewCell *cell = (NewCell *)[grid dequeueReusableCell];
	
	if (cell == nil) {
		cell = [[[NewCell alloc] init] autorelease];
	}
	
    NSInteger indexInArray = rowIndex * 4 + columnIndex;
    
    NSDictionary *tempDict =[l_ArrayWishlist objectAtIndex:indexInArray];
    
    
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[tempDict valueForKey:@"buzzImageThumbUrl"]];
    
    
    
    cell.headline.text=[NSString stringWithFormat:@"%@",[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"comments"]];
    
    
   /* if ([[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"isBuzzExists"]isEqualToString:@"YES" ]) {

        if ([[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"buzzType"] isEqualToString:kIBoughtIt]) {
            [cell.headline setText:[NSString stringWithFormat:@"%@: %@",[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"originatorFirstName"],
                                      [[Context getInstance] getDisplayTextFromMessageType:@"I bought it!"]]];
            
        }
        else if ([[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"buzzType"] isEqualToString:kBuyItOrNot]) {
            [cell.headline setText:[NSString stringWithFormat:@"%@: %@",[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"originatorFirstName"],[[Context getInstance] getDisplayTextFromMessageType:@"Buy it or not?"]]];
            
            
        }
        else if ([[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"buzzType"] isEqualToString:kCheckThisOut]) {
            [cell.headline setText:[NSString stringWithFormat:@"%@: %@",[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"originatorFirstName"],[[Context getInstance] getDisplayTextFromMessageType:@"Check this out..."]]];
            
            
        }
        else if ([[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"buzzType"] isEqualToString:kFoundASale]) {
            [cell.headline setText:[NSString stringWithFormat:@"%@: %@",[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"originatorFirstName"],[[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"]]];
            
                   }
        else if ([[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"buzzType"] isEqualToString:kHowDoesThisLook]) {
            [cell.headline setText:[NSString stringWithFormat:@"%@: %@",[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"originatorFirstName"],[[Context getInstance] getDisplayTextFromMessageType:@"How do i look?"]]];
            
                   }
        else if ([[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"buzzType"] isEqualToString:@"igotit"]) {
            [cell.headline setText:[NSString stringWithFormat:@"%@: %@",[[l_ArrayWishlist objectAtIndex:indexInArray] objectForKey:@"originatorFirstName"],@"I got it!"]];
            
            }
        
    }*/
    if ([cell.headline.text length]==0)
    {
        cell.headline.text=@"No comments";
        
        
    }

    
    
    
    cell.thumbnail.messageTag = indexInArray+1;
    cell.thumbnail.cropImage = YES;
    [cell.thumbnail loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked:) btnText:nil];
    
	return cell;
}

- (void) ProductBtnClicked:(id)sender
{
    int tag=((UIButton *)sender).tag-1;
    selectedIndex=tag;

	[[LoadingIndicatorView SharedInstance] startLoadingView:self];
	
	l_request=[[ShopbeeAPIs alloc] init];
    if (l_isEditingTrue==TRUE)
	{
		l_isProductExists=[[l_ArrayWishlist objectAtIndex:selectedIndex]valueForKey:@"isProductExists"];
		//l_isBuzzExists
		l_isBuzzExists=[[l_ArrayWishlist objectAtIndex:selectedIndex]valueForKey:@"isBuzzExists"];
		
        // GDL: We want to send an integer as wishlistid, not an object.
		[l_request getWishListDetails:@selector(requestCallBackMethodforMoreinfo:responseData:) tempTarget:self wishlistid:[[[l_ArrayWishlist objectAtIndex:selectedIndex]valueForKey:@"wishListId"] intValue]];
	}
	else
	{
		l_isProductExists=[[l_ArrayWishlist objectAtIndex:selectedIndex]valueForKey:@"isProductExists"];
		l_isBuzzExists=[[l_ArrayWishlist objectAtIndex:selectedIndex]valueForKey:@"isBuzzExists"];
        
        // GDL: We want to send an integer as wishlistid, not an object.
		[l_request getWishListDetails:@selector(requestCallBackMethodforMoreinfo:responseData:) tempTarget:self wishlistid:[[[l_ArrayWishlist objectAtIndex:selectedIndex]valueForKey:@"wishListId"] intValue]];
	}
    
	
	[l_request release];
	l_request=nil;
	

    
    
    
}





- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(l_isEditingTrue)
		return YES;
	else
		return NO;
}

// GDL: These are required methods for the popover controller delegate protocol.
// GDL: We say we implement the protocol, so we should do it.

#pragma mark -
#pragma mark WEPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    return YES;
}

@end
