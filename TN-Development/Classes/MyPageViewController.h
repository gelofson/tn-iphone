//
//  MyPageViewController.h
//  QNavigator
//
//  Created by softprodigy on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"FriendsListDisplay.h"
#import"MyPageSettingViewController.h"
#import"ChangePictureViewController.h"
#import"DBManager.h"
#import"ProfileinfoSettingViewController.h"
#import "tblWishList.h"
#import"MyPagePhotoViewController.h"
#import"ShopbeeAPIs.h"
#import "ContentViewController.h"


@interface MyPageViewController : ContentViewController <UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate, UITableViewDataSource, UITableViewDelegate> {
	UIView *m_changePhotoView;
	UIButton *m_addPhotoButton;
	int count;
	int tmp_count;
	UIImageView *m_changePic;
	UIView *m_changePicView;
	NSArray *m_theProfileArray;
	UITableView *m_theMyPageTable;
	UIScrollView *m_commentsView;
	NSArray *m_profileSettings;
	MyPagePhotoViewController *m_photocontroller;
	UILabel *m_nameLabel;
	UILabel *m_photoLabel;
	UILabel *m_wishlistLabel;
	UILabel *m_friendsLabel;
	BOOL m_friendsbuttonclicked; // for checking if the friends button was clicked 
	UIScrollView *m_scrollView;
	int m_checkFollowWebService;// to differentiate between follower and following callback methods
	UIButton *m_followingBtn;
	UIButton *m_followerBtn;
	UIButton *m_profileInfoBtn;
	UIButton *m_commentsButton;
	UIImage *m_personImage;
	NSData  *m_personData;
	BOOL isPictureSelected;
	UILabel *m_requestsCount;
	UIImageView *m_RequestImageView;	
	ShopbeeAPIs *l_request;
	UIButton *m_backButton;
    UIButton *m_SettingButton;
    UIButton *m_addFriedButton;
	BOOL BackButtonTrack;
	//BOOL shouldForcefullySessionExpire;
    NSArray *m_commentsArray;
    NSArray *m_dataArray;
    BOOL flag;
    NSString *userID;
    UIButton *addCommentButton;
    
    BOOL isDragging;
    BOOL isLoading;
    UIImageView *refreshArrow;
    UIActivityIndicatorView *refreshSpinner;
    UIView *refreshHeaderView;
    UILabel *refreshLabel;
    
    
    UIButton *m_meBtn;
    UIButton *m_messageBtn;
    UIButton *m_StoriesBtn;
    UIButton *m_FavoritesBtn;
    UILabel *m_cityLBL;
    UIScrollView *m_meView;
    UILabel *m_aboutme;
    UILabel *m_favoriteLBL;
    UIView *m_chatView;
    
    UIView *m_storyView;
    UIButton *m_storyMore;
    
    NSMutableArray *m_storyArray;
    UILabel *m_storyHeaderLBL;
    UIImageView *m_mystoryheader;
    int selectedIndex;
    int selectedFavIndex;
    UIView *m_FavoritesView;
    UIButton *m_FavoritesyMore;
    NSMutableArray *m_favoriteArray;
    UILabel *m_FavoriteHeaderLBL;
    UIImageView *m_myfavoriteheader;
    UILabel *favBrandLabel;
    UILabel *interestsLabel;

}
@property(nonatomic,retain)IBOutlet UIImageView *m_myfavoriteheader;
@property(nonatomic,retain)IBOutlet UIView *m_FavoritesView;
@property(nonatomic,retain)IBOutlet UIButton *m_FavoritesyMore;
@property(nonatomic,retain)IBOutlet UILabel *m_FavoriteHeaderLBL;

@property(nonatomic,retain)IBOutlet UIButton *m_storyMore;
@property(nonatomic,retain)IBOutlet UILabel *m_storyHeaderLBL;
@property(nonatomic,retain)IBOutlet UIImageView *m_mystoryheader;
@property(nonatomic,retain)IBOutlet UIView *m_storyView;
@property(nonatomic,retain)IBOutlet UIView *m_chatView;
@property(nonatomic,retain)IBOutlet UILabel *m_favoriteLBL;
@property(nonatomic,retain)IBOutlet UILabel *m_aboutme;
@property(nonatomic,retain)IBOutlet UIScrollView *m_meView;
@property(nonatomic,retain)IBOutlet UILabel *m_cityLBL;
@property(nonatomic,retain)IBOutlet UIButton *m_meBtn;
@property(nonatomic,retain)IBOutlet UIButton *m_messageBtn;
@property(nonatomic,retain)IBOutlet UIButton *m_StoriesBtn;
@property(nonatomic,retain)IBOutlet UIButton *m_FavoritesBtn;

@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property(nonatomic,retain) IBOutlet	UIButton *m_addPhotoButton;
@property(nonatomic,retain) IBOutlet	UIImageView *m_changePic;
@property(nonatomic,retain) IBOutlet	UIView *m_changePhotoView;
@property(nonatomic,retain) IBOutlet	UITableView *m_theMyPageTable;
@property(nonatomic,retain) IBOutlet	UIScrollView *m_commentsView;
@property (nonatomic,retain) IBOutlet	UIView *m_changePicView;
@property(nonatomic,retain) IBOutlet	UILabel *m_nameLabel;
@property(nonatomic,retain) IBOutlet	UILabel *m_wishlistLabel;
@property(nonatomic,retain) IBOutlet	UILabel *m_friendsLabel;
@property(nonatomic,retain) IBOutlet	UILabel *m_photoLabel;
@property(nonatomic,retain) IBOutlet	UIScrollView *m_scrollView;
@property(nonatomic,retain) IBOutlet	UIButton *m_followingBtn;
@property(nonatomic,retain) IBOutlet	UIButton *m_followerBtn;
@property(nonatomic,retain) IBOutlet	UILabel *m_requestsCount;
@property(nonatomic,retain) IBOutlet	UIImageView *m_RequestImageView;
@property(nonatomic,retain) IBOutlet    UIButton *m_backButton;
@property(nonatomic,retain) IBOutlet    UIButton *m_SettingButton;
@property(nonatomic,retain) IBOutlet    UIButton *m_addFriedButton;
@property(nonatomic,retain) IBOutlet    UIButton *m_profileInfoBtn;
@property(nonatomic,retain) IBOutlet    UIButton *m_commentsButton;
@property(nonatomic, copy) NSArray *m_commentsArray;
@property(nonatomic, copy) NSArray *m_dataArray;
@property (nonatomic, retain) IBOutlet UIButton *addCommentButton;
@property (nonatomic, retain) UIImage *m_personImage;
@property (readonly) NSInteger favSelectedIndex;
@property BOOL BackButtonTrack;
@property(nonatomic,retain) IBOutlet    UILabel *favBrandLabel;
@property(nonatomic,retain) IBOutlet    UILabel *interestsLabel;

@property(nonatomic,retain) NSString *userID;


-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData;

-(IBAction) clickOnCameraButton:(id)sender;
-(IBAction) clickedOnFriendsButton;
-(IBAction) showProfileSettingsAction;
-(IBAction) goToMyPageAction;
-(IBAction) clickonChangeBtn ;
-(IBAction) btn_wishList;
-(IBAction) viewPhotosAction;
-(IBAction) FollowersBtnAction;
-(IBAction) FollowingBtnAction;
-(IBAction)sendPictureToWebservice;
-(IBAction)BackBtnAction;
-(IBAction) addAFriendAction;
- (IBAction)showMessages:(id)sender;
- (IBAction)showProfile:(id)sender;
- (IBAction)addComment:(id)sender;
- (void) presentComments;
@property (retain, nonatomic) IBOutlet UIImageView *m_imageHeader;

- (void) setCommentsArray:(NSArray *)anArray;
- (void) setMessageId:(NSString *)messageID;
-(void)getResponse;
-(void)addPullTorefreshView;
-(void)startLoading;
-(void)refresh;
-(IBAction)getparticularBtuuonDetails:(id)sender;
-(IBAction)gotoStoryDetailsPage:(id)sender;
-(IBAction)gotoFavoriteDetailsPage:(id)sender;
- (void)commenterImagePressed:(id)sender;
@end
