//
//  Cell.m
//  naivegrid
//
//  Created by Apirom Na Nakorn on 3/6/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Cell.h"
#import <QuartzCore/QuartzCore.h> 

@implementation Cell


@synthesize thumbnail;


- (id)init {
	
    if (self = [super init]) {
		
        self.frame = CGRectMake(0, 0, 106, 106);
		
		//[[NSBundle mainBundle] loadNibNamed:@"Cell" owner:self options:nil];
        CGRect tRect = CGRectInset(self.bounds, 1, 1);
        self.thumbnail = [[AsyncButtonView alloc] initWithFrame:tRect];
        [self addSubview:self.thumbnail];
        
        UIImageView *imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(1, 62, 104, 43)] autorelease];
        [imageView setImage:[UIImage imageNamed:@"story-title-bgrd"]];
        imageView.alpha = 0.5;
        [self addSubview:imageView];

        self.headline = [[[UILabel alloc] initWithFrame:CGRectMake(1, 62, 104, 43)] autorelease];
        self.headline.font = [UIFont boldSystemFontOfSize:13.];
        self.headline.numberOfLines = 0;
        self.headline.lineBreakMode = UILineBreakModeWordWrap;
        self.headline.backgroundColor = [UIColor clearColor];
        self.headline.textColor = [UIColor whiteColor];
        self.headline.textAlignment  = UITextAlignmentCenter;
        
        [self addSubview:self.headline];
	}
	
    return self;
	
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/

- (void)dealloc {
    self.thumbnail = nil;
    self.headline = nil;
    [super dealloc];
}


@end
