//
//  NSMutableArray+Reverse.h
//  TinyNews
//
//  Created by Nava Carmon on 6/13/13.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Reverse)
- (void)reverse;

@end
