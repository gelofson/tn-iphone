//
//  GeoSelectViewController.h
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "BSForwardGeocoder.h"
#import "BSKmlResult.h"

@class GeoSelectAnnotation;

@interface GeoSelectViewController : UIViewController <MKMapViewDelegate, UITextFieldDelegate, BSForwardGeocoderDelegate>
{	
	IBOutlet MKMapView *mapView;	
	IBOutlet UILabel * subjectLabel;
    
    GeoSelectAnnotation * draggablePinAnnotation;
	BSForwardGeocoder * forwardGeocoder;
    
}

@property (nonatomic, retain) BSForwardGeocoder *forwardGeocoder;
@property (nonatomic, retain) IBOutlet MKMapView * mapView;
@property (nonatomic, retain) IBOutlet UILabel * subjectLabel;

@property (nonatomic, retain) GeoSelectAnnotation * draggablePinAnnotation;

-(IBAction) backAction:(id) sender;

@end
