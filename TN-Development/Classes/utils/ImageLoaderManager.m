//
//  ImageCacheManager.m
//  ooVoo
//
//  Created by Nava Carmon on 12/21/10.
//  Copyright 2010 ooVoo. All rights reserved.
//

#import "ImageLoaderManager.h"
#import "QNavigatorAppDelegate.h"

@interface ImageLoaderManager ()

@end

@implementation ImageLoaderManager

static ImageLoaderManager* sharedLoaderManager = nil ;

+ (ImageLoaderManager*)sharedManager
{
    if (sharedLoaderManager == nil) {
        sharedLoaderManager = [[super allocWithZone:NULL] init];
        
    }
    return sharedLoaderManager;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [[self sharedManager] retain];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)retain
{
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (oneway void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;
}

- (void) addLoaderRequest:(PictureRequest *)request
{
    if (!loaderQueue) {
        loaderQueue = [NSOperationQueue new];
        [loaderQueue setMaxConcurrentOperationCount:10];
    }
    [loaderQueue addOperation:request];
}

@end
