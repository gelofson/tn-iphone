//
//  ImageCacheManager.m
//  ooVoo
//
//  Created by Nava Carmon on 12/21/10.
//  Copyright 2010 ooVoo. All rights reserved.
//

#import "ImageCacheManager.h"
#import "QNavigatorAppDelegate.h"

#define CACHE_SIZE  50

@interface ImageCacheManager ()

- (void) addImageToCache:(UIImage *) anImage forKey:(NSString *) keyString;

@end

@implementation ImageCacheManager

static ImageCacheManager* sharedImagesManager = nil ;

+ (ImageCacheManager*)sharedManager
{
    if (sharedImagesManager == nil) {
        sharedImagesManager = [[super allocWithZone:NULL] init];
        
    }
    return sharedImagesManager;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [[self sharedManager] retain];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)retain
{
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (oneway void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;
}

- (void) addImageToCache:(UIImage *) anImage forKey:(NSString *) keyString
{
    [imagesCache setObject:anImage forKey:keyString];
}

- (UIImage *) getImage:(NSString *)name
{
    @synchronized (self) {
    NSString *fileName = [[tnApplication applicationImagesDirectory] stringByAppendingPathComponent:name];
    
    if (!imagesCache) {
        imagesCache = [NSMutableDictionary new];
    }
    UIImage *image = nil;
    if ([imagesCache count] > 0) {
        image = [imagesCache objectForKey:[fileName lastPathComponent]];
    }
    if (!image) {
        image = [UIImage imageWithContentsOfFile:fileName];      
        if (image) {
            [self addImageToCache:image forKey:[fileName lastPathComponent]];
        }
    }
    
    return image;
}
}

- (void) removeFromCache:(NSString *) keyName
{
    if (imagesCache) {
        [imagesCache removeObjectForKey:[keyName lastPathComponent]];
    }
}

- (void) savePicture:(NSData *) imageData forName:(NSString *)name
{
    @synchronized (self) {
    NSString *imageFilePath = [[tnApplication applicationImagesDirectory] stringByAppendingPathComponent:name];
    
    NSError *errorMsg = nil;//[NSError alloc];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:imageFilePath]) {
        [imageData  writeToFile:imageFilePath options: NSDataWritingAtomic error:&errorMsg];
    }
    
    if(errorMsg)
        NSLog(@"%@", errorMsg);
    }
}

- (void)cleanCache
{
    [imagesCache enumerateKeysAndObjectsUsingBlock:
     ^(id key, id obj, BOOL *stop) {
        NSString *imageFilePath = [[tnApplication applicationImagesDirectory] stringByAppendingPathComponent: (NSString *) key];
        if ([[NSFileManager defaultManager] fileExistsAtPath:imageFilePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:imageFilePath error:nil];
        }
    }];
    if (imagesCache) 
        [imagesCache removeAllObjects];
}


@end
