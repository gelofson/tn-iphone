//
//  UIView+Origin.h
//  TinyNews
//
//  Created by Nava Carmon on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Origin)
- (void) changeViewYOriginTo:(CGFloat)newY;

@end
