//
//  GeoSelectViewController.m
//

#import "GeoSelectViewController.h"
#import "GeoSelectAnnotation.h"
#import "QNavigatorAppDelegate.h"

@implementation GeoSelectViewController


@synthesize forwardGeocoder;
@synthesize mapView;
@synthesize draggablePinAnnotation;
@synthesize  subjectLabel;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		draggablePinAnnotation = nil;
		forwardGeocoder = nil;
    }
    return self;
}

-(void) resetLocation {
		CLLocationCoordinate2D thisLocation;
		// set default value of location to Infinity Circle
		thisLocation.latitude=LATITUDE_IN_SIMULATOR;
		thisLocation.longitude=LONGITUDE_IN_SIMULATOR;

		MKCoordinateSpan span;
	span.latitudeDelta = 0.02; span.longitudeDelta = 0.02;
		MKCoordinateRegion region = MKCoordinateRegionMake(thisLocation, span);

		if (draggablePinAnnotation == nil) {
        	GeoSelectAnnotation * gann = [[GeoSelectAnnotation alloc] init];
			self.draggablePinAnnotation = gann;
			[gann release];
        	[mapView addAnnotation:draggablePinAnnotation];
		} 
		[draggablePinAnnotation setCoordinate:thisLocation];
		[mapView setRegion:region animated:TRUE];

}

- (void)viewWillAppear:(BOOL)animated 
{

	[super viewWillAppear:animated];

	if (draggablePinAnnotation == nil) {
		[self resetLocation];
	}
	[mapView setCenterCoordinate:draggablePinAnnotation.coordinate animated:TRUE];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	UIBarButtonItem * barItem = [[UIBarButtonItem alloc] initWithTitle:@"Locate" style:UIBarButtonItemStyleBordered target:self action:@selector(locateAction:)];
	self.navigationItem.rightBarButtonItem = barItem;
	[barItem release];
	
	self.subjectLabel.text = self.title;

	mapView.showsUserLocation=NO;

}

- (void)didReceiveMemoryWarning {
 	NSLog(@"GeoSelectViewController::didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {
	[super viewDidUnload];

	if (forwardGeocoder)
		[forwardGeocoder release];
	forwardGeocoder = nil;
}


- (void)dealloc {
    self.subjectLabel = nil;
	mapView.delegate = nil;
	if(mapView != nil)
		[mapView release];
	if (forwardGeocoder)
		[forwardGeocoder release];
	forwardGeocoder = nil;
    [super dealloc];
}
    

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
    NSLog(@"GeoSelectViewController: didChangeDragState");
    if ( (newState == MKAnnotationViewDragStateStarting) ||
         (newState == MKAnnotationViewDragStateDragging) ) {
        // Do settings ??
    }
}
- (void)mapView:(MKMapView *)mapView1 didDeselectAnnotationView:(MKAnnotationView *)view 
{
}
- (void)mapView:(MKMapView *)mapView1 didSelectAnnotationView:(MKAnnotationView *)annView
{

    
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView1 viewForAnnotation:(id <MKAnnotation>) annotation{

	NSLog(@"GeoSelectViewController:viewForAnnotation called %d ", annotation);
	MKPinAnnotationView * annView = nil;
	if ((draggablePinAnnotation  != nil) && (annotation == draggablePinAnnotation)) {
	    annView=[[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"DRAGGABLE_PIN"] autorelease];
	    annView.draggable=YES;
	    annView.pinColor = MKPinAnnotationColorPurple;
	    annView.animatesDrop=TRUE;
	    annView.canShowCallout = YES;
	    annView.calloutOffset = CGPointMake(0, 0);
	} /*else {
	    annView=[[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"NON_DRAGGABLE_PIN"] autorelease];
	    annView.draggable = NO;
	    annView.pinColor = MKPinAnnotationColorRed;
	    annView.animatesDrop=TRUE;
	    annView.canShowCallout = YES;
	    annView.calloutOffset = CGPointMake(0, 0);
	}*/
	return annView;
}

- (void)mapView:(MKMapView *)mapView1 didAddAnnotationViews:(NSArray *)views {
	NSLog(@"GeoSelectViewController:didAddAnnotationViews called viewcount=%d",[views count]);
	
}


- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error {
    //QNavigatorAppDelegate * mainDg = (QNavigatorAppDelegate *) [[UIApplication sharedApplication] delegate];
    //[mainDg stopActivityIndicator:self.navigationController];
    if (error != nil) {
        NSString * descr = @""; 
        NSString * reason = @"";
        NSString * recovery = @"";
        if ([error localizedDescription] != nil)
            descr = [error localizedDescription];
        if ([error localizedFailureReason] != nil)
            reason = [error localizedFailureReason];
        if ([error localizedRecoverySuggestion] != nil)
            recovery = [error localizedRecoverySuggestion];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Loading map failed" 
            message:[NSString stringWithFormat:@"Error Code=%d\n.%@",[error code],[error localizedDescription]] 
            delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        NSLog(@"GeoSelectViewController:mapViewDidFailLoadingMap:ERROR code=%d.Description=%@.Reason=%@.Recovery=%@",[error  code], descr, reason, recovery);
		[alert show]; [alert release];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Loading map failed" 
            message:@""
            delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        NSLog(@"GeoSelectViewController:mapViewDidFailLoadingMap:ERROR");
		[alert show]; [alert release];
    }
}

- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error
{

    //QNavigatorAppDelegate * mainDg = (QNavigatorAppDelegate *) [[UIApplication sharedApplication] delegate];
    //[mainDg stopActivityIndicator:self.navigationController];

    if (error != nil) {
        NSString * descr = @""; 
        NSString * reason = @"";
        NSString * recovery = @"";
        if ([error localizedDescription] != nil)
            descr = [error localizedDescription];
        if ([error localizedFailureReason] != nil)
            reason = [error localizedFailureReason];
        if ([error localizedRecoverySuggestion] != nil)
            recovery = [error localizedRecoverySuggestion];

		
        if ([error domain] == kCLErrorDomain) {
			if ([error code] == kCLErrorDenied) {
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Services Unavailable" 
												message:@"Please allow \"OptumizeMe℠\" to auto-detect your location. To do so, go to Settings > General > Location Services and get \"OptumizeMe℠\" to ON and re-launch application.\n\nYou can continue to use the application without allowing auto-detection access."
												delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				NSLog(@"GeoSelectViewController:didFailToLocateUserWithError:ERROR code=%d.Description=%@.Reason=%@.Recovery=%@",[error  code], descr, reason, recovery);
				[alert show]; [alert release];
				
                return;
			}
		}
		 
		
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed to locate user" 
            message:[NSString stringWithFormat:@"Error Code=%d\n.%@\n%@\n%@",[error code],descr, reason, recovery]
            delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			
        NSLog(@"GeoSelectViewController:didFailToLocateUserWithError:ERROR code=%d.Description=%@.Reason=%@.Recovery=%@",[error  code], descr, reason, recovery);
		/*[alert show]; */[alert release];
    } else {
		
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed to locate user" 
            message:@""
            delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			
        NSLog(@"GeoSelectViewController:didFailToLocateUserWithError:ERROR");
		/*[alert show]; */[alert release];
    }
}

-(void)forwardGeocoderError:(NSString *)errorMessage
{
	//QNavigatorAppDelegate * mainDg = (QNavigatorAppDelegate *)[[UIApplication sharedApplication] delegate];
    //[mainDg stopActivityIndicator:self.navigationController];
}

-(void)forwardGeocoderFoundLocation {
	if(forwardGeocoder.status == G_GEO_SUCCESS) {
		int searchResults = [forwardGeocoder.results count];
		if (searchResults > 0 ) {
			BSKmlResult *place = [forwardGeocoder.results objectAtIndex:0];
			// Add a placemark on the map
			CLLocationCoordinate2D coordinate;
			coordinate.latitude = place.latitude;
			coordinate.longitude = place.longitude;
			MKCoordinateSpan span;
			span.latitudeDelta = 0.02; span.longitudeDelta = 0.02;
			MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, span);
			if (draggablePinAnnotation == nil) {
				GeoSelectAnnotation * gann = [[GeoSelectAnnotation alloc] init];
				self.draggablePinAnnotation = gann;
				[gann release];
				[mapView addAnnotation:draggablePinAnnotation];
			} 
			[draggablePinAnnotation setCoordinate:coordinate];
			[mapView setRegion:region animated:TRUE];
		}
	}
	else {
		NSString *message = @"";
		switch (forwardGeocoder.status) {
			case G_GEO_BAD_KEY:
				message = @"The API key is invalid.";
				break;
				
			case G_GEO_UNKNOWN_ADDRESS:
				message = [NSString stringWithFormat:@"Could not find %@", forwardGeocoder.searchQuery];
				break;
				
			case G_GEO_TOO_MANY_QUERIES:
				message = @"Too many queries has been made for this API key.";
				break;
				
			case G_GEO_SERVER_ERROR:
				message = @"Server error, please try again.";
				break;
				
				
			default:
				break;
		}
        NSLog (@"message=[%@]",message);
	}
	//QNavigatorAppDelegate * mainDg = (QNavigatorAppDelegate *)[[UIApplication sharedApplication] delegate];
    //[mainDg stopActivityIndicator:self.navigationController];
}

-(IBAction) backAction:(id) sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
