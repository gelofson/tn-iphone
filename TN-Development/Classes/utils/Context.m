//
//  Context.m
//  QNavigator
//
//  Created by Bharat Biswal on 11/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/NSDate.h>
#import "Context.h"
#import "Constants.h"
#import "FontsDef.h"
static Context * context_instance = nil;
static NSString *appVersionString = nil;

@interface Context ()
-(void) loadExistingConfig;
@end

@implementation Context

@synthesize m_settings, categories;

-(void) loadExistingConfig {
    
	// create a pointer to a dictionary
	NSDictionary *dictionary;
	// read "foo.plist" from application bundle
	NSString *localFileFullName = [[Context GetDocumentDirectoryPath] stringByAppendingPathComponent:APP_CONFIG_FILENAME];	
	if ([Context FileExistsAtPath:localFileFullName] == true) {
		NSLog(@"Context::loadExistingConfig: loading configuration from %@", localFileFullName);
		dictionary = [NSDictionary dictionaryWithContentsOfFile:localFileFullName];
		self.m_settings = [dictionary mutableCopy];
        
		// dump the contents of the dictionary to the console
		for (id key in m_settings) {
			NSLog(@"Context::loadExistingConfig: key=%@, value=%@", key, [m_settings objectForKey:key]);
		}
	} else {
		NSMutableDictionary * dict = [[NSMutableDictionary alloc] initWithCapacity:3];
		self.m_settings = dict;
		[dict release];
	}
    
    self.categories = [[[NSMutableArray alloc] init] autorelease];
    
    [self.categories addObject:kHowDoesThisLook];
    [self.categories addObject:kIBoughtIt];
	[self.categories addObject:kCheckThisOut];
    [self.categories addObject:kFoundASale];
    [self.categories addObject:kBuyItOrNot];
}


- (id) init 
{
	self = [super init];
	return self;
}


+ (Context *) getInstance 
{
    @synchronized(self) 
	{
        if (context_instance == nil) 
		{
            context_instance = [[self alloc] init]; // assignment not done here
			[context_instance loadExistingConfig];
		}
    }
    return context_instance;
}

+ (NSString *) GetDocumentDirectoryPath
{
	NSArray  *paths	= NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [paths objectAtIndex:0];
}

+ (NSString *) GetResourceDirectoryPath
{
	return [[NSBundle mainBundle] resourcePath];
}

+(bool) FileExistsAtPath:(NSString*) path
{
	NSFileHandle* handle = [NSFileHandle fileHandleForReadingAtPath:path];
	const bool present = handle != nil;
	if(handle) [handle closeFile];
	return present;
}

-(NSString *) getValue:(NSString *) keyId {
	NSString * ret = [m_settings objectForKey:keyId];
	if (ret != nil) 
		return ret;
	NSString * defVal = nil;
	if ([keyId compare:USER_SELECTED_TEXT_FONT_SIZE_KEY] == NSOrderedSame) {
		defVal = @"0";
	} else if ([keyId compare:APP_PREFIX_KEY] == NSOrderedSame) {
		defVal = @"fashiongram12";
	}
    
	if (defVal != nil) {
		NSLog(@"Context:getValue:Added default value '%@' for key '%@'",defVal, keyId);
		[m_settings setObject:defVal forKey:keyId];
		NSString *localFileFullName = [[Context GetDocumentDirectoryPath] stringByAppendingPathComponent:APP_CONFIG_FILENAME];	
		if ([m_settings writeToFile:localFileFullName atomically:YES] != YES) {
			NSLog(@"Context::getValue:ERROR Failed to save configuration.");
		}
	}
    
	ret = [m_settings objectForKey:keyId];
	if (ret != nil) 
		return ret;
	else  {
		NSLog(@"Context:getValue:ERROR: Could not handle key=%@",keyId);
		return nil;
	}
}

-(void) setValue:(NSString *) val forKey:(NSString *) keyId {
	if ((keyId != nil) && (val != nil)) {
		//NSLog(@"Context:setValue:value '%@' for key '%@'",val, keyId);
		[m_settings setObject:val forKey:keyId];
		NSString *localFileFullName = [[Context GetDocumentDirectoryPath] stringByAppendingPathComponent:APP_CONFIG_FILENAME];	
		if ([m_settings writeToFile:localFileFullName atomically:YES] != YES) {
			NSLog(@"Context::setValue:ERROR Failed to save configuration.");
		}
	}
}


- (NSString *) getAppVersion {
	if (appVersionString == nil) {
		NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
		NSString *name = [infoDictionary objectForKey:@"CFBundleDisplayName"];
		NSString *version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
		NSString *build = [infoDictionary objectForKey:@"CFBundleVersion"];
		NSString *label = [NSString stringWithFormat:@"%@ v%@ (%@)", name, version, build];
		appVersionString = [[NSString alloc] initWithString:label];
	}
	return appVersionString;
}

- (NSString *) getFontTypeForKey:(NSUInteger) fontTypeKey {
    
	// Handle special page considerations
	if (fontTypeKey == WHEREBOX_TITLEBAR_FONT_TYPE_KEY)
		return @"GillSans";
	else if (fontTypeKey == WHEREBOX_DISCOUNTLABEL_FONT_TYPE_KEY)
		return @"GillSans-Bold";
	else if (fontTypeKey == WHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY)
		return @"Arial-BoldMT";
	else if (fontTypeKey == WHEREBOX_DESCRIPTIONLABEL_FONT_TYPE_KEY)
		return @"GillSans";
    
	if (fontTypeKey == GROUPONWHEREBOX_TITLEBAR_FONT_TYPE_KEY)
		return @"GillSans";
	else if (fontTypeKey == GROUPONWHEREBOX_DISCOUNTLABEL_FONT_TYPE_KEY)
		return @"GillSans-Bold";
	else if (fontTypeKey == GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY)
		return @"GillSans";
	else if (fontTypeKey == GROUPONWHEREBOX_DESCRIPTIONLABEL_FONT_TYPE_KEY)
		return @"GillSans";
    
	if (fontTypeKey == BUZZBUTTONCAMERAFUNCTIONALITY_TITLEBAR_FONT_TYPE_KEY)
		return @"Futura";
	else if (fontTypeKey == BUZZBUTTONCAMERAFUNCTIONALITY_MESSAGETYPELABEL_FONT_TYPE_KEY)
		return @"GillSans";
	else if (fontTypeKey == BUZZBUTTONCAMERAFUNCTIONALITY_COMMENTVIEW_FONT_TYPE_KEY)
		return @"HelveticaNeue";
    
	if (fontTypeKey == FITTINGROOMMOREVIEWCONTROLLER_TITLEBAR_FONT_TYPE_KEY)
		return @"GillSans";
	else if (fontTypeKey == FITTINGROOMMOREVIEWCONTROLLER_USERNAMELABEL_FONT_TYPE_KEY)
		return @"GillSans";
	else if (fontTypeKey == FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_TYPE_KEY)
		return @"GillSans";
    
	if (fontTypeKey == FITTINGROOMUSECASESVIEWCONTROLLER_HEADERBAR_FONT_TYPE_KEY)
		return @"GillSans";
	
	if (fontTypeKey == MYPAGEVIEWCONTROLLER_USERNAMELABEL_FONT_TYPE_KEY)
		return @"GillSans";
	
	
	// No handle defaults
	if ((fontTypeKey > 101000000) && (fontTypeKey < 101999999)) {
		// All title bar text fonts
		return @"GillSans-Bold";
	} else if ((fontTypeKey > 102000000) && (fontTypeKey < 102999999)) {
		// All header text fonts
		return @"GillSans-Bold";
	} else if ((fontTypeKey > 103000000) && (fontTypeKey < 103999999)) {
		// All sub-header text fonts
		return @"GillSans-Bold";
	} else if ((fontTypeKey > 104000000) && (fontTypeKey < 104999999)) {
		// All highlighted text fonts
		return @"GillSans";
	} else if ((fontTypeKey > 105000000) && (fontTypeKey < 105999999)) {
		// All normal text fonts
		return @"GillSans";
	} else if ((fontTypeKey > 106000000) && (fontTypeKey < 106999999)) {
		// All light text fonts
		return @"GillSans";
	} 
    
    return @"GillSans";
    
    
}
- (CGFloat) getFontSizeForKey:(NSUInteger) fontSizeKey {
	
    
	// Calculate user configured delta for positionId
	int delta = 0;
	// User font settings applicable to positions { highlighted, normal and light } text only
	if ((fontSizeKey > 104000000) && ( fontSizeKey < 106999999))
		delta = [[[Context getInstance] getValue:USER_SELECTED_TEXT_FONT_SIZE_KEY] intValue];
    
    
	// Handle special page considerations
	if (fontSizeKey == WHEREBOX_TITLEBAR_FONT_SIZE_KEY)
		return 20.0+delta;
	else if (fontSizeKey == WHEREBOX_DISCOUNTLABEL_FONT_SIZE_KEY)
		return 16.0+delta;
	else if (fontSizeKey == WHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY)
		return 14.0+delta;
	else if (fontSizeKey == WHEREBOX_DESCRIPTIONLABEL_FONT_SIZE_KEY)
		return 14.0+delta;
    
	if (fontSizeKey == GROUPONWHEREBOX_TITLEBAR_FONT_SIZE_KEY)
		return 20.0+delta;
	else if (fontSizeKey == GROUPONWHEREBOX_DISCOUNTLABEL_FONT_SIZE_KEY)
		return 18.0+delta;
	else if (fontSizeKey == GROUPONWHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY)
		return 16.0+delta;
	else if (fontSizeKey == GROUPONWHEREBOX_DESCRIPTIONLABEL_FONT_SIZE_KEY)
		return 14.0+delta;
    
	if (fontSizeKey == BUZZBUTTONCAMERAFUNCTIONALITY_TITLEBAR_FONT_SIZE_KEY)
		return 18.0+delta;
	else if (fontSizeKey == BUZZBUTTONCAMERAFUNCTIONALITY_MESSAGETYPELABEL_FONT_SIZE_KEY)
		return 18.0+delta;
	else if (fontSizeKey == BUZZBUTTONCAMERAFUNCTIONALITY_COMMENTVIEW_FONT_SIZE_KEY)
		return 19.0+delta;
    
	if (fontSizeKey == FITTINGROOMMOREVIEWCONTROLLER_TITLEBAR_FONT_SIZE_KEY)
		return 20.0+delta;
	else if (fontSizeKey == FITTINGROOMMOREVIEWCONTROLLER_USERNAMELABEL_FONT_SIZE_KEY)
		return 14.0+delta;
	else if (fontSizeKey == FITTINGROOMMOREVIEWCONTROLLER_USERCOMMENTLABEL_FONT_SIZE_KEY)
		return 14.0+delta;
	
	if (fontSizeKey == FITTINGROOMUSECASESVIEWCONTROLLER_HEADERBAR_FONT_SIZE_KEY)
		return 20.0+delta;
	
	if (fontSizeKey == MYPAGEVIEWCONTROLLER_USERNAMELABEL_FONT_SIZE_KEY)
		return 17.0+delta;
    
	// No handle defaults
	if ((fontSizeKey > 101000000) && (fontSizeKey < 101999999)) {
		// All title bar text fonts
		return 22.0+delta;
	} else if ((fontSizeKey > 102000000) && (fontSizeKey < 102999999)) {
		// All header text fonts
		return 20.0+delta;
	} else if ((fontSizeKey > 103000000) && (fontSizeKey < 103999999)) {
		// All sub-header text fonts
		return 18.0+delta;
	} else if ((fontSizeKey > 104000000) && (fontSizeKey < 104999999)) {
		// All highlighted text fonts
		return 16.0+delta;
	} else if ((fontSizeKey > 105000000) && (fontSizeKey < 105999999)) {
		// All normal text fonts
		return 14.0+delta;
	} else if ((fontSizeKey > 106000000) && (fontSizeKey < 106999999)) {
		// All light text fonts
		return 12.0+delta;
	}
    
    return 12.0+delta;
}


- (BOOL)isSameDay:(NSDate*)date1 comparedTo:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

-(NSString *) getHHMMStringFromRdeemString:(NSString *) redeemStr {
	if (redeemStr == nil)
		return nil;
    
	NSString * hhmmssStr = [[[[redeemStr componentsSeparatedByString:@"T"] objectAtIndex:1] componentsSeparatedByString:@"Z"] objectAtIndex:0];
    
	NSString * dayStr = @"am";
	NSString * hrsString = [[hhmmssStr componentsSeparatedByString:@":"] objectAtIndex:0];
	NSString * minsString = [[hhmmssStr componentsSeparatedByString:@":"] objectAtIndex:1];
	if ((hrsString) && ([hrsString intValue] >= 12)) {
		NSString * newHrs = [NSString stringWithFormat:@"%d",([hrsString intValue] -12)];
		hrsString = newHrs;
		dayStr = @"pm";
	}
	return [NSString stringWithFormat:@"%@:%@%@",hrsString, minsString,dayStr];
    
}

- (BOOL) getFormatedGrouponAdRdeemTimingMessageWithStart:(NSString *) redeemStart withEnd:(NSString *) redeemEnd saveTo:(NSMutableArray *) msgArray {
	
	if ((redeemStart == nil) || (redeemEnd == nil) || (msgArray == nil))
		return NO;
    
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss ZZZZ"];
	NSDate * startDate = [dateFormatter dateFromString:[redeemStart stringByReplacingOccurrencesOfString:@"Z" withString:@" +0000"]];
	NSDate * endDate = [dateFormatter dateFromString:[redeemEnd stringByReplacingOccurrencesOfString:@"Z" withString:@" +0000"]];
    
	if ((startDate != nil) && (endDate != nil)) {
        
		NSString * msg1 = @"";
		NSString * msg2 = @"";
		
        
		if ([self isSameDay:startDate comparedTo:endDate]) {
			if ([self isSameDay:[NSDate date] comparedTo:startDate]) {
				// Begins and ends today
				msg1 = @"Redeem Today between";
			} else if ([self isSameDay:[NSDate dateWithTimeIntervalSinceNow:24*60*60] comparedTo:startDate]) {
				// Begins and ends tomorrow
				msg1 = @"Redeem Tomorrow between";
			} else {
				[dateFormatter setDateFormat:@"MMM/dd"];
				msg1 = [NSString stringWithFormat:@"Redeem on %@ between",[dateFormatter stringFromDate:startDate]];
			}
			
			[dateFormatter setDateFormat:@"h:mma"];
			msg2 = [NSString stringWithFormat:@"%@ - %@",
                    [dateFormatter stringFromDate:startDate],
                    [dateFormatter stringFromDate:endDate]];
            
			//msg2 = [NSString stringWithFormat:@"%@ - %@",
			//	 [self getHHMMStringFromRdeemString:redeemStart],
			//	 [self getHHMMStringFromRdeemString:redeemEnd]];
		} else {
			msg1 = @"Redeem between";
			//[dateFormatter setDateFormat:@"MMM/dd, h:mma"];
			
			[dateFormatter setDateFormat:@"h:mma"];
			msg2 = [NSString stringWithFormat:@"%@ - %@",
                    [dateFormatter stringFromDate:startDate],
                    [dateFormatter stringFromDate:endDate]];
			
			//msg2 = [NSString stringWithFormat:@"%@ - %@",
			//	 [self getHHMMStringFromRdeemString:redeemStart],
			//	 [self getHHMMStringFromRdeemString:redeemEnd]];
		}
        
		[msgArray addObject:msg1];
		[msgArray addObject:msg2];
        
		[dateFormatter release];
		return YES;
	} else {
		[msgArray addObject:@"invalid"];
		[msgArray addObject:@"invalid"];
	}
    
	[dateFormatter release];
	return NO;
}

- (NSString *) getDisplayTextFromMessageType:(NSString *) strType {
    
    NSString *str = [NSString stringWithFormat:@"Check what I posted in %@ news category", strType];
    return  str;
    
    if (!strType) {
        return nil;
    }
    
    if (
			([strType caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) ||
			([strType caseInsensitiveCompare:@"checkthisout..."] == NSOrderedSame) ||
			([strType caseInsensitiveCompare:@"checkthisout ..."] == NSOrderedSame) ||
			([strType caseInsensitiveCompare:@"check this out"] == NSOrderedSame) ||
			([strType caseInsensitiveCompare:@"check this out..."] == NSOrderedSame) ||
        ([strType caseInsensitiveCompare:@"buzzworthy"] == NSOrderedSame) ||
        
        ([strType caseInsensitiveCompare:@"check this out ..."] == NSOrderedSame) 
        )
	{
		// US 285
		//return @"Check this out";
		return @"Cats Dogs & Heroes";//@"To Buy or not To Buy";//@"Buzzworthy";
	}
    else if ( 
			([strType caseInsensitiveCompare:kBuyItOrNot] == NSOrderedSame) ||
			([strType caseInsensitiveCompare:@"buyitornot?"] == NSOrderedSame) ||
			([strType caseInsensitiveCompare:@"buy it or not"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"tobuyornottobuy"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"tobuyornottobuy?"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"to buy or not to buy"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"to buy or not to buy?"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"buy it or not?"] == NSOrderedSame) 
             )
	{
		// US 285
		//return @"Buy it or not?";
		return @"Food, Art, Fashion";
	}
    else if (
             ([strType caseInsensitiveCompare:kFoundASale] == NSOrderedSame) || 
             ([strType caseInsensitiveCompare:@"foundasale!"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"found a sale"] == NSOrderedSame) ||
             
             ([strType caseInsensitiveCompare:@"found a sale!"] == NSOrderedSame) 
             
             )
        
	{
		// US 330
		//return @"Found a sale!";
		return @"Cops and Robbers";//@"Found a Deal!";
	}
    else if (([strType caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) || 
             ([strType caseInsensitiveCompare:@"howdoilook"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"how do i look?"] == NSOrderedSame) || 
             ([strType caseInsensitiveCompare:@"how do i look"] == NSOrderedSame) || 
             ([strType caseInsensitiveCompare:@"how does this look?"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"how does this look"] == NSOrderedSame))
	{
		//return @"How do I look?";
        // US285
        return @"Politics and insurrection";//@"Haute or not?";
        
	}
    else if (
        ([strType caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) ||
        ([strType caseInsensitiveCompare:@"i bought it"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"i bought it!"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"iboughtit!"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"iboughtthis"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"iboughtthis!"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"i bought this!"] == NSOrderedSame) ||
             ([strType caseInsensitiveCompare:@"i bought this"] == NSOrderedSame) 
             )
	{
		//return @"I bought this !";
		// US 324 fixes
		return @"The Green Earth";//@"The Haul";
	}
    else if ([strType caseInsensitiveCompare:kMessageBoard] == NSOrderedSame) 
    {
        return @"Message Board";
    }
	else 
	{
		return @"I Got it!";
	}
    
}

@end
