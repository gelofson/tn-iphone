#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface ParkPlaceMark : NSObject<MKAnnotation> {
	CLLocationCoordinate2D coordinate;
	
	NSString * titleStr;
	
	NSString * subTitleStr;
	
	MKPinAnnotationView * customView;
}
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic,retain) NSString * titleStr;
@property (nonatomic,retain) NSString * subTitleStr;
@property (nonatomic,retain) MKPinAnnotationView * customView;
-(id)initWithCoordinate:(CLLocationCoordinate2D) coordinate;
- (NSString *)subtitle;
- (NSString *)title;

@end
