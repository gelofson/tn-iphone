//
//  BuzzButtonEnterOpenFunctionality.h
//  TinyNews
//
//  Created by jay kumar on 4/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuzzButtonEnterOpenFunctionality : UIViewController<UIActionSheetDelegate>
{
    UITextView *m_textView;
    NSString *m_headerMessage;
    NSString *m_strType;
    NSString *m_strMessage;
    NSString *m_whoMessage;
    NSString *m_whatMessage;
    NSString *m_whenMessage;
    NSString *m_whereMessage;
    NSString *m_howMessage;
    NSString *m_whyMessage;
    UIImage * photoImage;
}
@property(nonatomic,retain)NSString *m_headerMessage;
@property(nonatomic,retain) NSString *  m_strMessage;
@property(nonatomic,retain) NSString *m_whoMessage;
@property(nonatomic,retain) NSString *m_whatMessage;
@property(nonatomic,retain) NSString *m_whenMessage;
@property(nonatomic,retain) NSString *m_whereMessage;
@property(nonatomic,retain) NSString *m_howMessage;
@property(nonatomic,retain) NSString *m_whyMessage;
@property(nonatomic,retain) UIImage * photoImage;
@property(nonatomic ,retain)NSString *m_strType;
@property(nonatomic,retain)IBOutlet UITextView *m_textView;

//-(IBAction)clicktoBack:(id)sender;
-(IBAction)clickToNext:(id)sender;
-(IBAction)clickToCancel:(id)sender;

-(void) finallyPerformRealShare;
@end
