//
//  LoadingIndicatorView.h
//  QNavigator
//
//  Created by softprodigy on 28/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoadingIndicatorView : UIView 
{
	
}

-(void)startLoadingView:(id)target;
-(void)stopLoadingView;
+ (id) SharedInstance;
@end
