//
//  MessageBoardManager.h
//  TinyNews
//
//  Created by Nava Carmon on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageBoardManager : NSObject

+ (MessageBoardManager *) sharedManager;
- (void) sendBuzzRequest;
- (void) onGetBuzzResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *)error;
- (void) getMessageBoardId:(id) delegate;
- (BOOL) showError:(NSError *)error;
- (NSString *) onGetMessageResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *)error;
- (void) getMessageBoardId:(id) delegate custID:(NSString *) custId;

- (void)getComments:(id) delegate;
- (void) getComments:(id) delegate custID:(NSString *)customerId messageID:(NSString *)messageId;
@end
