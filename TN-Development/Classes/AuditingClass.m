//
//  AuditingClass.m
//  QNavigator
//
//  Created by Soft Prodigy on 22/09/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AuditingClass.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"
#import "SBJSON.h"

@implementation AuditingClass

@synthesize m_arrAuditData;
@synthesize m_responseData;
@synthesize m_intResponseCode;

QNavigatorAppDelegate *l_appDelegate;

//initialize the member variables
- (void)initializeMembers
{
	l_appDelegate=(QNavigatorAppDelegate  *)[[UIApplication sharedApplication]delegate];
	
	if(!m_arrAuditData)
	{
		m_arrAuditData=[[NSMutableArray alloc]init];
	}
	
	if(!m_responseData)
	{
		m_responseData=[[NSMutableData alloc]init];
	}
	
}

#pragma mark -

//method to send request to webservice to submit audit data
-(void)sendRequestToSubmitAuditData
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		//self.view.userInteractionEnabled=FALSE;
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		//NSString *temp_url; //salebynow/json.htm?action=getCateroriesByHandsetLongitudeAndLatitude&did=1234&lng=-84.8626412&lat=33.9199217&dist=10
		//NSString *temp_url=[NSString stringWithFormat:@"%@/salebynow/json.htm?action=auditUserActions&did=%@&lng=%f&lat=%f",kServerUrl,l_appDelegate.m_strDeviceId,l_appDelegate.m_geoLongitude,l_appDelegate.m_geoLatitude];
		NSString *temp_url=[NSString stringWithFormat:@"%@/salebynow/json.htm?action=auditUserActions&did=%@",kServerUrl,l_appDelegate.m_strDeviceId];//,l_appDelegate.m_geoLongitude,l_appDelegate.m_geoLatitude];
		
		//[temp_url replaceOccurrencesOfString:@"(null)" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [temp_url length])];
		
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		//NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"%@&usagedata=%@",temp_url,[temp_objSBJson stringWithObject:m_arrAuditData]];
		NSMutableString *temp_strJson =[[[NSMutableString alloc]initWithFormat:@"usagedata=%@",[temp_objSBJson stringWithObject:m_arrAuditData]] autorelease];
											
		//NSLog(@"strJson: %@",temp_strJson);
		
		[temp_objSBJson release];
		temp_objSBJson=nil;
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		NSURLConnection  *theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		[theConnection start];
		//isConnectionActive=TRUE;
		
		if(theConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
}


#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	m_intResponseCode = [httpResponse statusCode];
	NSLog(@"%d",m_intResponseCode);
	
	[m_responseData setLength:0];	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	//NSLog(@"%@",[[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]autorelease]);
	
	[m_responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	if(m_responseData!=nil)
	{
		//NSLog(@"response from server: %@",[[[NSString alloc]initWithData:m_responseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string=[[[NSString alloc]initWithData:m_responseData encoding:NSUTF8StringEncoding] autorelease];
		if([temp_string isEqualToString:@"[true]"])
		{
			[m_arrAuditData removeAllObjects];
		}
		
	}	
}

//called if connection failed due to network failure
- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
	UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[networkDownAlert show];
	[networkDownAlert release];
	networkDownAlert=nil;
}

#pragma mark -
#pragma mark Singleton Instance method
+ (id) SharedInstance {
	static id sharedManager = nil;
	
    if (sharedManager == nil) {
        sharedManager = [[self alloc] init];
	}
	return sharedManager;
}

// GDL: Modified everything below.

#pragma mark -
#pragma mark Memory Management Methods

-(void)dealloc {
	[m_arrAuditData release];
    [m_responseData release];
    
	[super dealloc];
}

@end
