//
//  BuzzButtonCameraFunctionality.m
//  QNavigator
//
//  Created by softprodigy on 04/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PhotoUseCaseViewController.h"
#import "QNavigatorAppDelegate.h"
#import<QuartzCore/QuartzCore.h>
#import <Math.h>
#import "GLImage.h"
#import "UploadImageAPI.h"
#import "LoadingIndicatorView.h"
#import "MyPageViewController.h"

@implementation PhotoUseCaseViewController

QNavigatorAppDelegate *l_appDelegate;
UploadImageAPI *l_buzzRequest;

@synthesize m_sendBtn;
@synthesize m_lblselectAnImg;
@synthesize m_userImage;
@synthesize l_textView;
@synthesize m_LabCountChar;
@synthesize m_buttomTextView;
@synthesize m_imageView;
@synthesize m_sliderZooming;
@synthesize smoothnessSlider;
@synthesize brightnessSlider;
@synthesize saturationSlider;
@synthesize m_buzzImage;
@synthesize m_imageViewCrop;
@synthesize m_bottomBar;
@synthesize m_indicatorView;
@synthesize m_beautyView;
@synthesize btnEdit;
@synthesize btnPreview;
@synthesize btnUndo;
@synthesize m_ViewTopTextView;
@synthesize m_arrWithUndoDict;
@synthesize m_ZoomDict;
@synthesize m_BeautyDict;
@synthesize m_CommentDict;
@synthesize m_strType;
@synthesize m_beautyViewBorder;
@synthesize	m_zoomSliderView;
@synthesize m_lblBasicText;
@synthesize m_requiredView;
@synthesize m_MypageViewController;
@synthesize m_newRegistration;
@synthesize m_scrollView;
@synthesize shouldUseEditedImage;


//BOOL isCaptureImage;
UIView *temp_View;
UIView *preview_view;
UIView *m_view;

UILabel *Preview_ViewLabel;

UIImageView *l_commentCheckMark;

int checkUndoOrZoom;
int check;
int i;


BOOL isRetakeImage;


float l_zoomValue, l_brightnessValue, l_smoothnessValue, l_saturationValue;

NSString *str_textView;
int count_text2=140;

BOOL isEditViewVisible;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	//m_MypageViewController=[[MyPageViewController alloc] init];
    i=0;
	//[m_lblselectAnImg setUserInteractionEnabled:TRUE];
//	[m_sendBtn setUserInteractionEnabled:TRUE];
	m_lblselectAnImg.hidden=YES;
	NSLog(@"m_strType: %@",m_strType);
	m_arrWithUndoDict=[[NSMutableArray alloc] init];
	m_ViewTopTextView.hidden=YES;
	[m_ViewTopTextView.layer setCornerRadius:6];

	btnPreview.hidden= NO;
	btnUndo.hidden= NO;

	scaleValue = 1.0f;
	
	isEditViewVisible=FALSE;
	m_LabCountChar.hidden=YES;
	
	[m_zoomSliderView.layer setCornerRadius:6.0];
	
	m_zoomSliderView.hidden=NO;
	
	previousScaleValue=1.0f;
	
	//////////////top view//////////////////
	//if(![preview_view superview])
//	{
		//preview_view=[[UIView alloc] initWithFrame:CGRectMake(0,395, 320, 25)];
	//	preview_view.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
		
		Preview_ViewLabel = [[UILabel alloc ] initWithFrame:CGRectMake(0,5,320,20)];
		Preview_ViewLabel.textAlignment =  UITextAlignmentCenter;
		Preview_ViewLabel.textColor = [UIColor whiteColor];
		Preview_ViewLabel.backgroundColor = [UIColor clearColor];
		Preview_ViewLabel.font = [UIFont fontWithName:kMyriadProRegularFont size:(16.0)];
		Preview_ViewLabel.text=@"Preview";
		[self.view addSubview:Preview_ViewLabel];
		
	//	[tmp_topViewLabel setTag:5];
		
		//[preview_view addSubview:Preview_ViewLabel];
		
		//[self.view addSubview:preview_view];
	//	preview_view.hidden=YES;
	
	//new comment
				
	//}
	m_imageViewCrop.frame=CGRectMake(-170, -250, 640, 920);
	m_imageViewCrop.image = [UIImage imageNamed:@"crop_back.png"];
	[m_imageViewCrop setHidden:YES];

	smoothnessSlider.hidden=YES;
	brightnessSlider.hidden=YES;
	saturationSlider.hidden=YES;
	

	m_zoomSliderView.hidden=YES;
	m_buttomTextView.hidden=YES;
	l_textView.hidden=YES;
	//[l_textView.layer setCornerRadius:6];
	//[m_buttomTextView.layer setCornerRadius:6];

	
	self.view.frame=[[UIScreen mainScreen] bounds];
	
	temp_View = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
	[l_textView.layer setCornerRadius:6];

	//temp_View.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"overlay_back.png"]];
//	UIImageView *temp_bottomImage=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,320,480)]; 	[temp_bottomImage setImage:[UIImage imageNamed:@"overlay_camera_back.png"]]; 	[temp_View addSubview:temp_bottomImage];
//	[temp_bottomImage release];
    UIImageView *pic1View = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,320,32)];
    [pic1View setImage:[UIImage imageNamed:@"navbar"]];
    [temp_View addSubview:pic1View];
    [pic1View release];
    
    pic1View = [[UIImageView alloc]initWithFrame:CGRectMake(0,429,320,51)];
    [pic1View setImage:[UIImage imageNamed:@"footer-bgrd"]];
    [temp_View addSubview:pic1View];
    [pic1View release];
	
    pic1View = [[UIImageView alloc]initWithFrame:CGRectMake(0,352,320,78)];
    [pic1View setImage:[UIImage imageNamed:@"grey-bgrd"]];
    [temp_View addSubview:pic1View];
    [pic1View release];
	
	UILabel *Preview_ViewLabel2 = [[UILabel alloc ] initWithFrame:CGRectMake(0,5,320,20)];
	Preview_ViewLabel2.textAlignment =  UITextAlignmentCenter;
	Preview_ViewLabel2.textColor = [UIColor whiteColor];
	Preview_ViewLabel2.backgroundColor = [UIColor clearColor];
	Preview_ViewLabel2.font = [UIFont fontWithName:kHelveticaBoldBFont size:(16.0)];
	Preview_ViewLabel2.text=@"Take picture";
	[temp_View addSubview:Preview_ViewLabel2];
	[Preview_ViewLabel2 release];
	
    
	UILabel *Preview_ViewLabel3 = [[UILabel alloc ] initWithFrame:CGRectMake(0,369,320,43)];
	Preview_ViewLabel3.textAlignment =  UITextAlignmentCenter;
	Preview_ViewLabel3.textColor = [UIColor blackColor];
	Preview_ViewLabel3.backgroundColor = [UIColor clearColor];
	Preview_ViewLabel3.font = [UIFont fontWithName:kHelveticaBoldBFont size:(19.0)];
	Preview_ViewLabel3.text=@"User Profile Photo";
	[temp_View addSubview:Preview_ViewLabel3];
	[Preview_ViewLabel3 release];
	

	UIButton *takePicButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[takePicButton addTarget:self action:@selector(dismissCameraView) forControlEvents:UIControlEventTouchUpInside];
	takePicButton.frame = CGRectMake(5,443,67, 29);
	[takePicButton setImage:[UIImage imageNamed:@"New_cancel_btn.png"] forState:UIControlStateNormal];
	[temp_View addSubview:takePicButton];

	
	UIButton *btnCancel  = [UIButton buttonWithType:UIButtonTypeCustom];
	[btnCancel addTarget:self action:@selector(takePicture:) forControlEvents:UIControlEventTouchUpInside];//cancel_btn.png
	btnCancel.frame = CGRectMake(116, 438, 89, 38);
	[btnCancel setImage:[UIImage imageNamed:@"take-pic-btn.png"] forState:UIControlStateNormal];
	[temp_View addSubview:btnCancel];
	
	
	UIButton *existingPic = [UIButton buttonWithType:UIButtonTypeCustom];
	[existingPic addTarget:self action:@selector(btnPhotoLibrary:) forControlEvents:UIControlEventTouchUpInside];
	existingPic.frame = CGRectMake(248, 442,66,30);
	[existingPic setImage:[UIImage imageNamed:@"choose-existing-icon.png"] forState:UIControlStateNormal];
	[temp_View addSubview:existingPic];
	
	[smoothnessSlider setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateNormal];
	[brightnessSlider setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateNormal];
	[saturationSlider setThumbImage:[UIImage imageNamed:@"slider_btn.png"] forState:UIControlStateNormal];
	[smoothnessSlider setMinimumTrackImage:[UIImage imageNamed:@"on_bar.png"] forState:UIControlStateNormal];
	[smoothnessSlider setMaximumTrackImage:[UIImage imageNamed:@"Off_bar.png"] forState:UIControlStateNormal];
	
	[brightnessSlider setMinimumTrackImage:[UIImage imageNamed:@"on_bar.png"] forState:UIControlStateNormal];
	[brightnessSlider setMaximumTrackImage:[UIImage imageNamed:@"Off_bar.png"] forState:UIControlStateNormal];
	
	[saturationSlider setMinimumTrackImage:[UIImage imageNamed:@"on_bar.png"] forState:UIControlStateNormal];
	[saturationSlider setMaximumTrackImage:[UIImage imageNamed:@"Off_bar.png"] forState:UIControlStateNormal];
	
	[m_sliderZooming setMinimumTrackImage:[UIImage imageNamed:@"on_bar.png"] forState:UIControlStateNormal];
	
	//[m_beautyView addSubview:smoothnessSlider];
	//[m_beautyView addSubview:brightnessSlider];
	//[m_beautyView addSubview:saturationSlider];
	//[self.view addSubview:m_beautyView];
	
	//m_beautyView.hidden=YES;
	m_beautyViewBorder.hidden=YES;
	
	m_imgPicker=[[UIImagePickerController alloc]init];
	
	
	//add the three buttons
	m_view= [[UIView alloc] initWithFrame:CGRectMake(0, 355, 320, 80)];
	m_view.backgroundColor=[UIColor clearColor]; //colorWithRed:0 green:0 blue:0 alpha:0.5];
	m_view.tag=101;
	
    
    
	UIButton *BtnZoom = [UIButton buttonWithType:UIButtonTypeCustom];
	[BtnZoom addTarget:self action:@selector(btnZoomAction:) forControlEvents:UIControlEventTouchUpInside];
	BtnZoom.frame = CGRectMake(260,25,60,60);
	[BtnZoom setImage:[UIImage imageNamed:@"zoom.png"] forState:UIControlStateNormal];
	[m_view addSubview:BtnZoom];
	 
	/*
	UIButton *BtnBeauty  = [UIButton buttonWithType:UIButtonTypeCustom];
	[BtnBeauty addTarget:self action:@selector(btnBeautyAction:) forControlEvents:UIControlEventTouchUpInside];//cancel_btn.png
	BtnBeauty.frame = CGRectMake(180,13,60,60);
	[BtnBeauty setImage:[UIImage imageNamed:@"beauty.png"] forState:UIControlStateNormal];
	[m_view addSubview:BtnBeauty];
     */
	
	[self.view addSubview:m_view];
	[m_view setHidden:YES];
	
    /*
	l_commentCheckMark=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"buzz_check_icon.png"]];
	l_commentCheckMark.frame=CGRectMake(265, 3, 23, 22);
	[m_view addSubview:l_commentCheckMark];
	l_commentCheckMark.hidden=YES;
     */
     
	
	m_indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150,200,20,20)];
	m_indicatorView.hidesWhenStopped=YES;
	[temp_View addSubview:m_indicatorView];
	
	
	/////////////////////
	[self performSelector:@selector(btnUndoAction:)];
	
}

//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
//{
//	
//	return m_imageView;
//}

-(void)startIndicator
{
	[temp_View setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.7]];
	[m_indicatorView startAnimating];
	
}

-(void)viewWillAppear:(BOOL)animated
{
//	[m_beautyView.layer setCornerRadius:8.0f];
//	[m_beautyView.layer setMasksToBounds:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];

    // Anurag
    m_buttomTextView.hidden=YES;
	m_zoomSliderView.hidden=YES;
	m_beautyViewBorder.hidden=YES;
	m_lblBasicText.hidden=YES;
    isEditViewVisible=TRUE;
    [m_view setHidden:YES];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

-(void)viewDidAppear:(BOOL)animated
{
	if(i==0)
	{
		[self performSelector:@selector(btnRetakeAction:)];
		i=1;
	}
}



#pragma mark -
#pragma mark Custom methods


-(IBAction)btnRetakeAction:(id)sender
{
	btnEdit.enabled=YES;
	m_sendBtn.enabled=YES;
	
    
	m_LabCountChar.hidden=YES;
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
        [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"PhotoUseCaseViewController"
                                                        withAction:@"btnRetakeAction"
                                                         withLabel:nil
                                                         withValue:nil];

		if (!m_imgPicker)
		{
			m_imgPicker=[[UIImagePickerController alloc]init];
		}
		m_imgPicker.delegate=self;
		m_imgPicker.sourceType=UIImagePickerControllerSourceTypeCamera;
		m_imgPicker.showsCameraControls=NO;
		m_imgPicker.cameraOverlayView = temp_View;
		//m_imgPicker.navigationBar.barStyle=UIBarStyleBlackOpaque;
		m_imgPicker.allowsEditing = YES;
		[self presentModalViewController:m_imgPicker animated:NO];
	}
	else 
	{
		m_imgPicker.allowsEditing = YES;
		[self performSelector:@selector(chooseImage:)];
	}
	
}


-(IBAction)btnPreviewAction:(id)sender
{
	Preview_ViewLabel.text=@"Preview";
	m_lblBasicText.hidden=NO;
	//NSLog(@"m_sliderZooming value: %d",m_sliderZooming.value);
//	m_dictUndo=[NSDictionary dictionaryWithObjectsAndKeys:scaleValue,@"",nil];
	NSDictionary *tmpDictionary;
	
	if([m_sliderZooming value]!=1.00 && check==1)
	{
		check=-1;
		
		//tmpDictionary=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:[m_sliderZooming value]],@"zoomValue",@"zoom",@"type",nil];
		tmpDictionary=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:l_zoomValue],@"zoomValue",@"zoom",@"type",nil];
		[m_arrWithUndoDict addObject:tmpDictionary];
		
		l_zoomValue=[m_sliderZooming value];
		
	}
	
	if(([brightnessSlider value]!=0.00||[smoothnessSlider value]!=0.00||[saturationSlider value]!=1.00) && check==2)
	{
		check=-1;
		//tmpDictionary=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:[brightnessSlider value]],@"brightnessValue",[NSNumber numberWithFloat:[smoothnessSlider value]],@"smoothnessValue",[NSNumber numberWithFloat:[saturationSlider value]],@"saturationValue",@"beauty",@"type",nil];
		tmpDictionary=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:l_brightnessValue],@"brightnessValue",[NSNumber numberWithFloat:l_smoothnessValue],@"smoothnessValue",[NSNumber numberWithFloat:l_saturationValue],@"saturationValue",@"beauty",@"type",nil];
		[m_arrWithUndoDict addObject:tmpDictionary];
		
		l_brightnessValue=[brightnessSlider value];
		l_smoothnessValue=[smoothnessSlider value];
		l_saturationValue=[saturationSlider value];
		
	}
	
	
	//1 for zooming ,2 for beauty , 3 for comments
	m_imageViewCrop.hidden=YES;
	m_zoomSliderView.hidden=YES;
	//m_beautyView.hidden=YES;
	m_beautyViewBorder.hidden=YES;

	preview_view.hidden=NO;
	//btnEdit.hidden=NO;
	//btnPreview.hidden=YES;
	//btnUndo.hidden=YES;
	m_view.hidden=NO;
    
    
	NSLog(@"preview btn");
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"PhotoUseCaseViewController"
                                                    withAction:@"Filters"
                                                     withLabel:nil
                                                     withValue:nil];
	
}


-(IBAction)btnUndoAction:(id)sender
{
	//m_sliderZooming.hidden=YES;
	//m_beautyView.hidden=YES;
	
	checkUndoOrZoom=1;
//	NSLog(@"%@",[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]);
	if([m_arrWithUndoDict count]!=0)
	{
		if([[NSString stringWithString:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"type"]] isEqualToString:@"zoom"])
		{
			//NSLog(@"zooming previous value = %f",[[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"zoomValue"] floatValue]);
			NSLog(@"zoom block");
			[self zoomValueChanged:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"zoomValue"]];
			[m_sliderZooming setValue:[[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"zoomValue"] floatValue]]; 
			
			[m_arrWithUndoDict removeLastObject];
		}
		else if([[NSString stringWithString:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"type"]] isEqualToString:@"beauty"])
		{
			NSLog(@"beauty block");
			[self brightnessforUndo:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"brightnessValue"]];
			[self smoothnessforUndo:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"smoothnessValue"]];
			[self saturationforUndo:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"saturationValue"]];
			//NSLog(@"beauty previous value = %f",[[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"brightnessValue"] floatValue]);
			//NSLog(@"beauty previous value = %f",[[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"smoothnessValue"] floatValue]);
			//NSLog(@"beauty previous value = %f",[[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"saturationValue"] floatValue]);
			[m_arrWithUndoDict removeLastObject];
		}
		else if([[NSString stringWithString:[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"type"]] isEqualToString:@"comments"])
		{
			NSLog(@"comments block");
			//NSLog(@"comments previous value = %@",[[m_arrWithUndoDict objectAtIndex:[m_arrWithUndoDict count]-1]valueForKey:@"zoomValue"]);
			[m_arrWithUndoDict removeLastObject];
		}
	}
	else if([m_arrWithUndoDict count]==0)
	{
		[self reset];
	}
}

-(void)reset {
	saturationSlider.value = 1.00;
    brightnessSlider.value = 0.00;
    smoothnessSlider.value = 0.00;
	m_sliderZooming.value = 1.00;
	
	l_saturationValue=1.0;
	l_brightnessValue=0.0;
	l_smoothnessValue=0.0;
	l_zoomValue=1.0;
	
	[self zoomValueChanged:[NSNumber numberWithInt:1.00]];
	[self saturationforUndo:[NSNumber numberWithInt:1.00]];
	[self smoothnessforUndo:[NSNumber numberWithInt:0.00]];
	[self brightnessforUndo:[NSNumber numberWithInt:0.00]];
}

-(void)zoomValueChanged:(NSNumber *)zoomvalue
{
	scaleValue = [zoomvalue floatValue];
	//NSLog(@"scaleValue :%f", scaleValue);
	

	scale = scaleValue/previousScaleValue;
	previousScaleValue = scaleValue;
	
	CGAffineTransform transform = m_imageView.transform;
	transform = CGAffineTransformScale(transform, scale, scale);
	m_imageView.transform = transform;
	
	m_imageView.frame=CGRectMake(0, 0, m_imageView.frame.size.width, m_imageView.frame.size.height);
	[m_scrollView setContentSize:CGSizeMake(m_imageView.frame.size.width, m_imageView.frame.size.height)];
	
	
	//NSLog(@"frame: x: %f  y:%f height:%f width:%f",m_imageView.frame.origin.x,m_imageView.frame.origin.y,m_imageView.frame.size.height,m_imageView.frame.size.width);
}

-(IBAction)btnEditAction:(id)sender
{
	m_buttomTextView.hidden=YES;
	m_zoomSliderView.hidden=YES;
	m_beautyViewBorder.hidden=YES;
	btnPreview.hidden=YES;
	btnUndo.hidden=YES;
	Preview_ViewLabel.text=@"Edit";
	m_lblBasicText.hidden=YES;
	
//	if (![[self.view viewWithTag:101] superview] && isEditViewVisible==FALSE)
//	{
		//preview_view.hidden=YES;
		
		btnEdit.hidden=YES;
		btnPreview.hidden=NO;
		btnUndo.hidden=NO;
		isEditViewVisible=TRUE;
		
		[m_view setHidden:NO];

}

-(void)cropImage
{
	
//	((UILabel *)[preview_view viewWithTag:5]).text=@"Crop image";
	//m_buttomTextView.hidden=NO;	
//	m_beautyView.hidden=YES;	
//	l_textView.hidden=YES;
//	m_buttomTextView.hidden=YES;
//	m_view.hidden=YES;
//	m_LabCountChar.hidden=YES;
//	m_sliderZooming.hidden=YES;
//	smoothnessSlider.hidden=YES;
//	brightnessSlider.hidden=YES;
//	saturationSlider.hidden=YES;
//	m_imageViewCrop.hidden=NO;
}


-(void)btnCommentAction:(id)sender
{
	[l_textView becomeFirstResponder];
	Preview_ViewLabel.text=@"Comment";
	m_lblBasicText.hidden=YES;
	check=3;
	m_ViewTopTextView.hidden=NO;
	m_LabCountChar.hidden=NO;
	m_imageViewCrop.hidden=YES;
	//count=1;
	//((UILabel *)[preview_view viewWithTag:5]).text=@"Add Comment";
	//preview_view.hidden=NO;
	m_beautyViewBorder.hidden=YES;
	m_buttomTextView.hidden=YES;
	l_textView.hidden=NO;
	l_textView.delegate=self;
	[l_textView.layer setCornerRadius:6];
	l_textView.backgroundColor=[UIColor whiteColor];
	[self.view addSubview:m_ViewTopTextView];
	//[self.view addSubview:l_textView];
}

-(void)btnBeautyAction:(id)sender
{
	check=2;
	m_imageViewCrop.hidden=YES;
	Preview_ViewLabel.text=@"Beauty";
	m_lblBasicText.hidden=YES;
	//count=3;
	//((UILabel *)[preview_view viewWithTag:5]).text=@"Beauty mode";
	m_beautyViewBorder.hidden=NO;	
	l_textView.hidden=YES;
	m_buttomTextView.hidden=YES;
	m_view.hidden=YES;
	//preview_view.hidden=NO;
	m_LabCountChar.hidden=YES;
	m_zoomSliderView.hidden=YES;
	smoothnessSlider.hidden=NO;
	brightnessSlider.hidden=NO;
	saturationSlider.hidden=NO;

}

-(void)btnZoomAction:(id)sender
{
	checkUndoOrZoom=2;
	check=1;
	Preview_ViewLabel.text=@"Zoom";
	m_imageViewCrop.hidden=YES;
	//count=2;
	//((UILabel *)[preview_view viewWithTag:5]).text=@"Zoom image";
	m_beautyViewBorder.hidden=YES;
	//preview_view.hidden=NO;	
	l_textView.hidden=YES;
	m_buttomTextView.hidden=YES;
	m_view.hidden=YES;
	m_zoomSliderView.hidden=NO;
    m_lblBasicText.hidden=YES;
	m_LabCountChar.hidden=YES;
	
		
	//previousScaleValue = 1.0f;
		
	[m_sliderZooming addTarget:self action:@selector(scaleChanged:) forControlEvents:UIControlEventValueChanged];
	[m_sliderZooming addTarget:self action:@selector(beginTracking:) forControlEvents:UIControlEventTouchDragInside];
	[m_sliderZooming addTarget:self action:@selector(endTracking:) forControlEvents:UIControlEventTouchUpInside |UIControlEventTouchUpOutside];
	
}


-(void)beginTracking:(id)sender
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:0.1];
	
	[UIView commitAnimations];
}

-(void)endTracking:(id)sender
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:0.1];
	
	[UIView commitAnimations];
}

-(void)scaleChanged:(id)sender
{
	scaleValue = [m_sliderZooming value];
	//NSLog(@"scaleValue :%f", scaleValue);
	
	scale = scaleValue/previousScaleValue;
	previousScaleValue = scaleValue;
	
	CGAffineTransform transform = m_imageView.transform;
	transform = CGAffineTransformScale(transform, scale, scale);
	m_imageView.transform = transform;
	//NSLog(@"frame: x: %f  y:%f height:%f width:%f",m_imageView.frame.origin.x,m_imageView.frame.origin.y,m_imageView.frame.size.height,m_imageView.frame.size.width);
	//NSLog(@"scroll view content: %f %f", m_scrollView.contentSize.height, m_scrollView.contentSize.width);
	
	m_imageView.frame=CGRectMake(0, 0, m_imageView.frame.size.width, m_imageView.frame.size.height);
	//m_scrollView.delegate=self;
	
	[m_scrollView setContentSize:CGSizeMake(m_imageView.frame.size.width, m_imageView.frame.size.height)];
	
	if(m_scrollView.contentSize.width>=320)
		[m_scrollView scrollRectToVisible:CGRectMake((m_scrollView.contentSize.width-320)/2, (m_scrollView.contentSize.height -427)/2, 320, 427	) animated:NO];
	else
		m_imageView.center=CGPointMake(m_scrollView.frame.size.width/2, m_scrollView.frame.size.height/2);
	
	
}


//-(void)btnSaveAction:(id)sender
//{
//	if(count==1)
//	{
//		preview_view.hidden=YES;	
//		[l_textView resignFirstResponder];
//		l_textView.hidden=YES;
//		if(l_textView.text==nil)
//		{
//			m_buttomTextView.hidden=YES;
//		}
//		else
//		{
//			m_buttomTextView.hidden=NO;
//			m_buttomTextView.text=str_textView;
//			m_buttomTextView.userInteractionEnabled=NO;
//			
//		}
//
//			}
//	else if(count==2)
//	{
//		m_sliderZooming.hidden=YES;
//		preview_view.hidden=YES;
//		if(m_buzzImage!=nil)
//		{
//			UIImageWriteToSavedPhotosAlbum(m_buzzImage, nil, nil, nil);
//		}
//		else {
//			NSLog(@"image is nil");
//		}
//
//	}
//	else if(count==3)
//	{
//		m_beautyView.hidden=YES;
//		preview_view.hidden=YES;
//		if(m_buzzImage!=nil)
//		{
//			UIImageWriteToSavedPhotosAlbum(m_buzzImage, nil, nil, nil);
//		}
//		else 
//		{
//			NSLog(@"image is nil");
//		}
//	}
//
//}
//



-(IBAction)btnPhotoLibrary:(id)sender
{
	if(!(m_imageView.image))
	{
		m_lblselectAnImg.hidden=NO;
		btnEdit.enabled=FALSE;
		m_sendBtn.enabled=FALSE;
	}
	else 
	{
		m_lblselectAnImg.hidden=YES;
		btnEdit.enabled=TRUE;
		m_sendBtn.enabled=TRUE;
		
	}	
	
	shouldUseEditedImage=TRUE;
	m_imgPicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"PhotoUseCaseViewController"
                                                    withAction:@"btnPhotoLibrary"
                                                     withLabel:nil
                                                     withValue:nil];

}

-(IBAction)btnCancelAction:(id)sender
{
	if (m_imgPicker)
	{
		[m_imgPicker release];
		m_imgPicker=nil;
	}
	
	if([l_appDelegate.m_checkForRegAndMypage isEqualToString:@"1"])
	{
		
		
		[self performSelector:@selector(dismissCurrentView) withObject:nil afterDelay:0.1];
		
		//[self.view removeFromSuperview];
		
	}
	else
	{
		[self dismissModalViewControllerAnimated:NO];
		[self.navigationController popViewControllerAnimated:YES];
	}

	if(!(m_imageView.image))
	{
		m_lblselectAnImg.hidden=NO;
		btnEdit.enabled=FALSE;
		m_sendBtn.enabled=FALSE;
	}
	else 
	{
		m_lblselectAnImg.hidden=YES;
		btnEdit.enabled=TRUE;
		m_sendBtn.enabled=TRUE;
		
	}	
	
//	[btnEdit setUserInteractionEnabled:FALSE];
//	[m_sendBtn setUserInteractionEnabled:FALSE];
	
}

-(void)navigateToBackView
{
	if (m_imgPicker)
	{
		[m_imgPicker release];
		m_imgPicker=nil;
	}
	
	if([l_appDelegate.m_checkForRegAndMypage isEqualToString:@"1"])
	{
		[self dismissModalViewControllerAnimated:NO];
				
		[self performSelector:@selector(dismissCurrentView) withObject:nil afterDelay:0.1];
		
		//[self.view removeFromSuperview];
		
	}
	else
	{
		[self dismissModalViewControllerAnimated:NO];
		[self.navigationController popViewControllerAnimated:YES];
	}
		
}

-(void)dismissCurrentView
{
	[self dismissModalViewControllerAnimated:NO];
}

-(void)dismissCameraView
{
	if (!m_imageView.image)
	{
		[self performSelector:@selector(navigateToBackView)];
	}
	else {
			

		if (m_imgPicker)
		{
			[m_imgPicker release];
			m_imgPicker=nil;
		}
		
		[self dismissModalViewControllerAnimated:NO];
		if(!(m_imageView.image))
		{
			m_lblselectAnImg.hidden=NO;
			btnEdit.enabled=FALSE;
			m_sendBtn.enabled=FALSE;
		}
		else {
			m_lblselectAnImg.hidden=YES;
			btnEdit.enabled=TRUE;
			m_sendBtn.enabled=TRUE;
			
		}	
	}
}

- (IBAction) takePicture:(id)sender
{
	i=2;
	m_lblselectAnImg.hidden=YES;
	shouldUseEditedImage=FALSE;
	
	//call in background to start immediately
	[self performSelectorInBackground:@selector(startIndicator) withObject:nil];
	
	m_imgPicker.allowsEditing = YES;
	
	[m_imgPicker takePicture];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"PhotoUseCaseViewController"
                                                    withAction:@"takePicture"
                                                     withLabel:nil
                                                     withValue:nil];
}


- (IBAction) smoothnessforUndo: (NSNumber *)smoothnessSliderValue
{
	
	//smoothnessSlider.hidden=YES;
//	brightnessSlider.hidden=YES;
//	saturationSlider.hidden=YES;
	
    // Just rounding to the nearest integer, making sure it works for negative integers.
    int newSmoothness = (int) ( [smoothnessSliderValue floatValue] + 10.5 ) - 10;
    if ( newSmoothness != smoothness ) {
        smoothness = newSmoothness;
        m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
	
}

- (IBAction) brightnessforUndo: (NSNumber *)brightnessSliderValue {
    
	
	//smoothnessSlider.hidden=YES;
//	brightnessSlider.hidden=YES;
//	saturationSlider.hidden=YES;
	// Gamma is on a logarithmic scale, here ranging from 0.25 (2^-2) to 4 (2^2).
    float newGamma = powf(2,[brightnessSliderValue floatValue]);
    if ( newGamma != gamma ) {
        gamma = newGamma;
        m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
}

- (IBAction) saturationforUndo: (NSNumber *)saturationSliderValue {
    //smoothnessSlider.hidden=YES;
//	brightnessSlider.hidden=YES;
//	saturationSlider.hidden=YES;
	
	float newSaturation = [saturationSliderValue floatValue];
    if ( newSaturation != saturation ) {
        saturation = newSaturation;
        m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
}






- (IBAction) chooseImage:(id) sender {
	if(!(m_imageView.image))
	{
		m_lblselectAnImg.hidden=NO;
		btnEdit.enabled=FALSE;
		m_sendBtn.enabled=FALSE;
	}
	else {
		m_lblselectAnImg.hidden=YES;
		btnEdit.enabled=TRUE;
		m_sendBtn.enabled=TRUE;
		
	}	
	
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	
	[self presentModalViewController:picker animated:YES];
	[picker release];
			
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"PhotoUseCaseViewController"
                                                    withAction:@"chooseImage"
                                                     withLabel:nil
                                                     withValue:nil];
}


- (IBAction) smoothness: (UISlider *)slider {
	
	smoothnessSlider.hidden=NO;
	brightnessSlider.hidden=NO;
	saturationSlider.hidden=NO;
	
    // Just rounding to the nearest integer, making sure it works for negative integers.
    int newSmoothness = (int) ( slider.value + 10.5 ) - 10;
    if ( newSmoothness != smoothness ) {
        smoothness = newSmoothness;
        m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
	
}

- (IBAction) brightness: (UISlider *)slider {
    
	smoothnessSlider.hidden=NO;
	brightnessSlider.hidden=NO;
	saturationSlider.hidden=NO;
	// Gamma is on a logarithmic scale, here ranging from 0.25 (2^-2) to 4 (2^2).
    float newGamma = powf(2,slider.value);
    if ( newGamma != gamma ) {
        gamma = newGamma;
        m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
}

- (IBAction) saturation: (UISlider *)slider {
    smoothnessSlider.hidden=NO;
	brightnessSlider.hidden=NO;
	saturationSlider.hidden=NO;
	
	float newSaturation = slider.value;
    if ( newSaturation != saturation ) {
        saturation = newSaturation;
        m_imageView.image = [self.m_buzzImage imageWithSaturation:saturation gamma:gamma smoothing:smoothness];
    }
}


-(IBAction)btnSendAction:(id)sender
{
	CGSize temp_size=CGSizeMake(320,320);
	
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"PhotoUseCaseViewController"
                                                    withAction:@"btnSendAction"
                                                     withLabel:nil
                                                     withValue:nil];
	UIGraphicsBeginImageContext(temp_size);
	[self.m_requiredView.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	
	if([l_appDelegate.m_checkForRegAndMypage isEqualToString:@"1"])
	{
		[m_newRegistration.m_TakePictureBtn setImage:newImage forState:UIControlStateNormal];
		m_newRegistration.m_personData=UIImageJPEGRepresentation(newImage, 0.5);
		
		//[self.view removeFromSuperview];
		[self performSelector:@selector(dismissCurrentView) withObject:nil afterDelay:0.1];
		//[self dismissModalViewControllerAnimated:YES];
		
	}
	else
	{
		//set the image
		
		[self performSelectorInBackground:@selector(startLoadingIndicator) withObject:nil];
		
		
		[self sendPictureToWebservice:newImage];
		
		[m_MypageViewController.m_changePic setImage:newImage];
		[m_MypageViewController.m_addPhotoButton setImage:newImage forState:UIControlStateNormal];
		[self.navigationController popViewControllerAnimated:YES];
	}

	
}

-(void)startLoadingIndicator
{
	NSAutoreleasePool *tempPool=[[NSAutoreleasePool alloc]init];
	
	[[LoadingIndicatorView SharedInstance] startLoadingView:self];
	
	[tempPool release];
}

-(void)saveUserImage:(NSData *)userPicData
{
	NSAutoreleasePool *tempPool=[[NSAutoreleasePool alloc]init];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fileName=[NSString stringWithFormat:@"%@.jpg",[[NSUserDefaults standardUserDefaults] valueForKey:@"userName"]];
	
	NSMutableString *pathDoc=[NSString stringWithFormat:@"%@/%@",documentsDirectory,fileName];
	
	[userPicData writeToFile:pathDoc atomically:YES];	
		
	[tempPool release];
    
}

-(void)sendPictureToWebservice:(UIImage *)newImage
{
	NSData *tempData=UIImageJPEGRepresentation(newImage, 0.5);
	
	[self performSelectorInBackground:@selector(saveUserImage:) withObject:tempData];
	
	UploadImageAPI *tempUploadObj=[UploadImageAPI SharedInstance];
	
	NSMutableDictionary *temp_dictRegData;
	NSUInteger len = [tempData length];
	const char *raw=[tempData bytes];
	//creates the byte array from image
	NSMutableArray *tempByteArray=[[NSMutableArray alloc]init];
	for (long int i = 0; i < len; i++)
	{
		[tempByteArray addObject:[NSNumber numberWithInt:raw[i]]];
	} 
	
	NSString *tmp_usrId=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	
	//[l_indicatorView stopLoadingView];
	
	temp_dictRegData=[NSMutableDictionary dictionaryWithObjectsAndKeys:[tempByteArray JSONFragment],@"photo",tmp_usrId,@"userId",nil];
	NSDictionary *tmp_dictionary=(NSMutableDictionary *)temp_dictRegData;
	
	//[tempUploadObj performSelectorInBackground:@selector(updateUserPic:) withObject:tmp_dictionary];
		
	[[LoadingIndicatorView SharedInstance] stopLoadingView];
	
	[tempUploadObj updateUserPic:tmp_dictionary];
	
	//release the temporary byte array
	[tempByteArray release];
	tempByteArray=nil;
	
	
	
}	


#pragma mark -
#pragma mark textView methods
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	if ([text isEqualToString:@"\n"]) 
	{
		//preview_view.hidden=YES;
		
		
		[l_textView resignFirstResponder];
		
		
		Preview_ViewLabel.text=@"Preview";
		if(l_textView.text.length==0)
		{
			m_buttomTextView.hidden=YES;
			m_lblBasicText.hidden=YES;
			l_commentCheckMark.hidden=YES;
		}
		else
		{	
			l_commentCheckMark.hidden=NO;
			m_lblBasicText.hidden=YES;
			btnPreview.hidden=YES;
			btnUndo.hidden=YES;
			btnEdit.hidden=NO;
			m_view.hidden=YES;
			m_ViewTopTextView.hidden=YES;
			//preview_view.hidden=NO;	
			m_buttomTextView.hidden=NO;
			m_buttomTextView.text=str_textView;
			m_buttomTextView.userInteractionEnabled=NO;
		}
		
		//l_textView.hidden=YES;
		
		m_ViewTopTextView.hidden=YES;
		
	}
	
	if(l_textView.text.length>=140 && range.length==0)
	{
		return NO;
	}
	else
	{
		return YES;
	}
	return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
	m_LabCountChar.hidden=NO;
	int max_count;
	if (count_text2 == 0) 
	{
		NSLog(@"not editing\n");
		[l_textView setUserInteractionEnabled:FALSE];
	}
	str_textView = l_textView.text;
	[str_textView retain];
	max_count = count_text2-[str_textView length];
	m_LabCountChar.text=[NSString stringWithFormat:@"%d",max_count];
	NSLog(@"%d",count_text2);
	NSLog(@"%@",m_LabCountChar.text);
	
}


#pragma mark -
#pragma mark textView methods

- (CGFloat)distancebetweentwopoints:(CGPoint)frompoint topoint:(CGPoint)topoint 
{
	
	float x = topoint.x - frompoint.x;
	float y = topoint.y - frompoint.y;
	
	return sqrt(x * x + y * y);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event 
{
	[super touchesBegan:touches withEvent:event];
	
	NSSet *all = [event allTouches];
	
	NSArray *allTouches = [all allObjects];
	
	int count = [allTouches count];
	if (count > 1) {
		touch1 = [[allTouches objectAtIndex:0] locationInView:self.view]; 
		touch2 = [[allTouches objectAtIndex:1] locationInView:self.view];
		
		initialDistance=[self distancebetweentwopoints:touch1 topoint:touch2];
		if (initialDistance==0)
		{
			initialDistance=1;
		}
		
		
		scaleValue = 1.0;
	}
	
}


-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event 
{
	CGPoint currentTouch1;
	CGPoint currentTouch2;

//	UITouch *touch = [touches anyObject];
	NSSet *all = [event allTouches];
	NSArray *allTouches = [all allObjects];
	int count = [allTouches count]; 
	if(count ==1)
	{
		CGPoint newTouch = [[touches anyObject] locationInView:self.view];
		CGPoint lastTouch = [[touches anyObject] previousLocationInView: self.view];
		//NSLog(@"new touch:    %i %i last touch  :%i  %i",newTouch.x,newTouch.y,lastTouch.x,lastTouch.y);
		float xDif = newTouch.x - lastTouch.x;
		float yDif = newTouch.y - lastTouch.y;
		//NSLog(@"%f",xDif);
		//NSLog(@"%f",yDif);
		
		CGAffineTransform translate = CGAffineTransformMakeTranslation(xDif, yDif);
		[m_imageViewCrop setTransform: CGAffineTransformConcat([m_imageViewCrop transform], translate)];
		
	}
	else if(count>1)
	{	
			
			
			if ((CGRectContainsPoint([m_imageViewCrop frame], [[allTouches objectAtIndex:0] locationInView:self.view])) ||
			(CGRectContainsPoint([m_imageViewCrop frame], [[allTouches objectAtIndex:1] locationInView:self.view]))) {
			
			currentTouch1 = [[allTouches objectAtIndex:0] locationInView:self.view]; 
			currentTouch2 = [[allTouches objectAtIndex:1] locationInView:self.view];
			CGFloat finalDistance = [self distancebetweentwopoints:currentTouch1 topoint:currentTouch2];

			
			//CGFloat finalDistance = [self distanceBetweenTwoPoints:currentTouch1 toPoint:currentTouch2];
			
			//Check if zoom in or zoom out.
			
			if (initialDistance!=0&&finalDistance!=0)
			{
				if(m_imageViewCrop.frame.size.width>=640 && m_imageViewCrop.frame.size.height>=920)
				{
					if(initialDistance > finalDistance) 
					{
						//NSLog(@"Zoom Out");
						scaleValue-=0.02;
					}
				}	
				if (initialDistance < finalDistance){
					//NSLog(@"Zoom In");
					scaleValue+=0.02;
				}
				//NSLog(@"initial: %0.2f",initialDistance);
				//NSLog(@"final: %0.2f",finalDistance);
				//CGAffineTransform scaleTransform = CGAffineTransformMakeScale( scaleValue, scaleValue);
				m_imageViewCrop.transform= CGAffineTransformMakeScale(scaleValue, scaleValue);
				//touch1 = currentTouch1;
//				touch2 = currentTouch2;
//				scaleValue=1.0;
//				
				initialDistance = finalDistance;
			}			
		}
	}
	
	
		
}


-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSLog(@"touches ended");
	[super touchesEnded:touches withEvent:event];
	
}
#pragma mark -
#pragma mark alertView methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex==0)
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
	
}

#pragma mark -
#pragma mark imagePickerController methods


//-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
//{
//	m_personImage=[[[UIImage alloc] init]autorelease];
//	m_personImage=image;
//	
//	if (m_personData) {
//		[m_personData release];
//		m_personData=nil;
//	}
//	m_personData=[NSData dataWithData:UIImagePNGRepresentation(m_personImage)];
//	
//	[m_personData retain];
//	[self dismissModalViewControllerAnimated:YES];
//}


- (UIImage*)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect
{
	//create a context to do our clipping in
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef currentContext = UIGraphicsGetCurrentContext();
	
	//create a rect with the size we want to crop the image to
	//the X and Y here are zero so we start at the beginning of our
	//newly created context
	CGRect clippedRect = CGRectMake(0, 0, rect.size.width, rect.size.height);
	CGContextClipToRect(currentContext, clippedRect);
	
	//create a rect equivalent to the full size of the image
	//offset the rect by the X and Y we want to start the crop
	//from in order to cut off anything before them
	CGRect drawRect = CGRectMake(rect.origin.x * -1,
								 rect.origin.y * -1,
								 imageToCrop.size.width,
								 imageToCrop.size.height);
	
	//draw the image to our clipped context using our offset rect
	CGContextDrawImage(currentContext, drawRect, imageToCrop.CGImage);
	
	//pull the image from our cropped context
	UIImage *cropped = UIGraphicsGetImageFromCurrentImageContext();
	
	
	//pop the context to get back to the default
	UIGraphicsEndImageContext();
	
	//Note: this is autoreleased
	return cropped;
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	[m_indicatorView stopAnimating];
	[temp_View setBackgroundColor:[UIColor clearColor]];
	
	saturationSlider.value = 1.00;
    brightnessSlider.value = 0.00;
    smoothnessSlider.value = 0.00;
	saturation=1;
	gamma=1;
	smoothness=0;
	
	m_sliderZooming.value = 1.00;
	
	l_saturationValue=1.0;
	l_brightnessValue=0.0;
	l_smoothnessValue=0.0;
	l_zoomValue=1.0;
	
	previousScaleValue=1.0f;
	
	m_imageView.transform = CGAffineTransformIdentity;
	m_imageView.frame=CGRectMake(0, 0, m_imageView.frame.size.width, m_imageView.frame.size.height);
	[m_scrollView setContentSize:CGSizeMake(m_imageView.frame.size.width, m_imageView.frame.size.height)];
	
		
	//NSLog(@"%f%f",((UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage]).size.height,((UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage]).size.width);
	
	//self.m_buzzImage = [[info objectForKey:UIImagePickerControllerOriginalImage] scaledDownImageWithMaximumSize:CGSizeMake(320,480)];
	
	if (shouldUseEditedImage==TRUE)
	{
		self.m_buzzImage=[[info objectForKey:UIImagePickerControllerEditedImage] scaledDownImageWithMaximumSize:CGSizeMake(320,320)]; //imageWithSize:CGSizeMake(320, 480)];
		[m_scrollView setFrame:CGRectMake(0, 0, m_buzzImage.size.width, m_buzzImage.size.height)];
		[m_imageView setFrame:CGRectMake(0, 0, m_buzzImage.size.width, m_buzzImage.size.height)];
		m_scrollView.contentSize=CGSizeMake(m_buzzImage.size.width, m_buzzImage.size.height);
			
	}
	else
	{
		self.m_buzzImage=[[info objectForKey:UIImagePickerControllerOriginalImage] scaledDownImageWithMaximumSize:CGSizeMake(320,320)]; //imageWithSize:CGSizeMake(320, 480)];
		
		[m_scrollView setFrame:CGRectMake(0,0,320,320)];//m_imageView.frame.size.width, m_imageView.frame.size.height)];
		m_imageView.frame=CGRectMake(0, 0, 320,320);//m_imageView.frame.size.width, m_imageView.frame.size.height);
				
			
	}

	NSLog(@"width is %f",self.m_buzzImage.size.width);
	NSLog(@"height is %f",self.m_buzzImage.size.height);
	
	[m_imageView setImage:self.m_buzzImage];
	
	[picker dismissModalViewControllerAnimated:NO];
	
	if(!(m_imageView.image))
	{
		m_lblselectAnImg.hidden=NO;
		btnEdit.enabled=FALSE;
		m_sendBtn.enabled=FALSE;
		
	}
	else 
	{
		m_lblselectAnImg.hidden=YES;
		btnEdit.enabled=TRUE;
		m_sendBtn.enabled=TRUE;
		
	}	
	
	[self performSelector:@selector(btnPreviewAction:)];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[m_indicatorView stopAnimating];
	[temp_View setBackgroundColor:[UIColor clearColor]];
	
  	m_view.hidden=YES;
	[picker dismissModalViewControllerAnimated:NO];
	
	if(!(m_imageView.image))
	{
		m_lblselectAnImg.hidden=NO;
		btnEdit.enabled=FALSE;
		m_sendBtn.enabled=FALSE;
		
		[self performSelector:@selector(navigateToBackView)];
		
	}
	else 
	{
		m_lblselectAnImg.hidden=YES;
		btnEdit.enabled=TRUE;
		m_sendBtn.enabled=TRUE;
		
	}
	
	[self performSelector:@selector(btnPreviewAction:)];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


//
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {    
//	m_buzzImage = [[info objectForKey:UIImagePickerControllerOriginalImage] scaledDownImageWithMaximumSize:CGSizeMake(320,480)];
//	[m_imageView setImage:self.m_buzzImage];
//	[picker dismissModalViewControllerAnimated:YES];
//    
//    [self reset:nil];
//}
//


// GDL: edited everything below to avoid memory leaks.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
	
	// Release any retained IBOutlets
    self.m_scrollView = nil;
    self.m_requiredView = nil;
    self.m_lblBasicText = nil;
    self.m_zoomSliderView = nil;
    self.m_beautyViewBorder = nil;
    self.m_sendBtn = nil;
    self.m_lblselectAnImg = nil;
    self.m_ViewTopTextView = nil;
    self.btnEdit = nil;
    self.btnPreview = nil;
    self.btnUndo = nil;
    self.m_bottomBar = nil;
    self.m_imageViewCrop = nil;
    self.m_beautyView = nil;
    self.m_sliderZooming = nil;
    self.m_imageView = nil;
    self.m_buttomTextView = nil;
    self.m_userImage = nil;
    self.l_textView = nil;
    self.m_LabCountChar = nil;
    self.smoothnessSlider = nil;
    self.brightnessSlider = nil;
    self.saturationSlider = nil;
}

- (void)dealloc  {
    [m_imgPicker release];
    [m_userImage release];
    [l_textView release];
    [m_buttomTextView release];
    [m_LabCountChar release];
    [m_imageView release];
    [m_imageViewCrop release];
    [m_sliderZooming release];
    [m_buzzImage release];
    [smoothnessSlider release];
    [brightnessSlider release];
    [saturationSlider release];
    [m_beautyView release];
    [m_beautyViewBorder release];
    
    [m_bottomBar release];
    [m_indicatorView release];
    [btnEdit release];
    [btnPreview release];
    [btnUndo release];
    [m_ViewTopTextView release];
    [m_arrWithUndoDict release];
    [m_ZoomDict release];
    [m_BeautyDict release];
    [m_CommentDict release];
    [m_strType release];
    [m_personData release];
    [m_personImage release];
    [m_lblselectAnImg release];
    [m_sendBtn release];
    [m_zoomSliderView release];
    [m_lblBasicText release];
    [m_requiredView release];
    [m_MypageViewController release];
    [m_newRegistration release];
    [m_scrollView release];
       
	[super dealloc];
}

@end
