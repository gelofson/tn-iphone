//
//  NotificationResponse.m
//  QNavigator
//
//  Created by Nicolas Jakubowski on 10/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "NotificationResponse.h"
#import "JSON.h"
#import "Notification.h"

@implementation NotificationResponse

@synthesize serverResponse = m_serverResponse;
@synthesize totalRecords = m_totalRecords;
@synthesize numberOfPages = m_numberOfPages;
@synthesize notifications = m_notifications;

- (id)init{
    self = [super init];
    if (self) {
        m_notifications = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)parse{
    if (!m_serverResponse || [m_serverResponse length] == 0) {
        return;
    }
    
    NSArray* a = [m_serverResponse JSONValue];
    NSDictionary* dataDictionary = nil;
    if (a && [a count] >= 1) {
        dataDictionary = [a objectAtIndex:0];
    }
    
    if (dataDictionary) {
        m_totalRecords = [[dataDictionary objectForKey:@"totalRecords"] intValue];
        m_numberOfPages = [[dataDictionary objectForKey:@"noOfPages"] intValue];
        
        NSArray* notificationsArray = [dataDictionary objectForKey:@"notificationTrayList"];
        
        for (NSDictionary* notification in notificationsArray) {
            Notification *n = [[Notification alloc] init];
            [n parseFromDictionary:notification];
            [m_notifications addObject:n];
            [n release];
        }
    }

}

@end
