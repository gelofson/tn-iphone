//
//  CategoryData.m
//  TinyNews
//
//  Created by Nava Carmon on 29/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CategoryData.h"

@interface CategoryData ()
- (void) initMessages:(BOOL)fromFittingList;
- (id) initWithData:(NSArray *)aData fromList:(BOOL) fromFittingList;
@end 

@implementation CategoryData

@synthesize data, messageIDs, categoryData;

- (id) initWithData:(NSArray *)aData fromList:(BOOL) fromFittingList
{
    self = [super init];
    if (self) {
        self.data = aData;
        [self initMessages:fromFittingList];
    }
    return self;
}

- (id) initWithData:(NSArray *) aData
{
    self = [super init];
    if (self) {
        self.data = aData;
        [self initMessages:YES];
    }
    return self;
}

- (NSMutableArray *)messageIDs
{
    if (!messageIDs) {
        self.messageIDs = [[[NSMutableArray alloc] init] autorelease];
    }
    return messageIDs;
}

- (void) initMessages:(BOOL)fromFittingList
{    
    if (fromFittingList) {
        self.categoryData = [[self.data objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
    } else
        self.categoryData = self.data; 
    
    for (int i = 0; i < [self.categoryData count]; i++) {
        NSInteger MessageID = [[[self.categoryData objectAtIndex:i] valueForKey:@"messageId"] intValue];
        [self.messageIDs addObject:[NSNumber numberWithInt:MessageID]];
    }
}

- (void) addDataFromArray:(NSArray *)array
{
    NSMutableArray *anArray = [[[NSMutableArray alloc] init] autorelease];
    NSArray *summary = [[array objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
    [anArray addObjectsFromArray:self.categoryData];
    [anArray addObjectsFromArray:summary];
    
    self.categoryData = anArray;
    
    
    for (int i = 0; i < [summary count]; i++) {
        NSInteger MessageID = [[[summary objectAtIndex:i] valueForKey:@"messageId"] intValue];
        [self.messageIDs addObject:[NSNumber numberWithInt:MessageID]];
    }
}

- (NSInteger) indexOfMessage:(NSInteger)messageId
{
    NSUInteger index = [self.messageIDs indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [obj intValue] == messageId;
    }];
    
    return index;
}


- (NSInteger) numPages
{
    NSDictionary *dict = [self.data objectAtIndex:0];
    if ([dict count] == 0) {
        return 0;
    }
    if (![dict valueForKey:@"noOfPages"]) {
        return 0;
    }
    return [[dict valueForKey:@"noOfPages"] intValue];
}

- (NSInteger) totalRecords
{
    NSDictionary *dict = [self.data objectAtIndex:0];
    if ([dict count] == 0) {
        return 0;
    }
    
    NSString *str = [dict valueForKey:@"totalRecords"];
    if (!str) {
        return 0;
    }
    return [str intValue];
}

- (NSString *) type
{
    return [[self.categoryData objectAtIndex:0] objectForKey:@"type"];
}

- (NSDictionary *) messageData:(NSInteger) index
{
    return [categoryData objectAtIndex:index];
}

- (NSDictionary *) wsMessage:(NSInteger) index
{
    return [[categoryData objectAtIndex:index] objectForKey:@"wsMessage"];
}

- (NSDictionary *) wsBuzz:(NSInteger) index
{
    return [[categoryData objectAtIndex:index] objectForKey:@"wsBuzz"];
}


// This function adapts the message data to a gars format, which is used in detail view
+ (CategoryData *) convertMessageData:(NSDictionary *)messageDataDict
{
    NSMutableArray * arrayOfMessages = [[[NSMutableArray alloc] init] autorelease];
    NSMutableDictionary *tmp_dict_mut = [[[NSMutableDictionary alloc] init] autorelease];
    [tmp_dict_mut addEntriesFromDictionary:messageDataDict];
    
    [tmp_dict_mut setObject:[[messageDataDict valueForKey:@"wsMessage"] objectForKey:@"id"] forKey:@"messageId"];
    [tmp_dict_mut setObject:[[messageDataDict valueForKey:@"wsMessage"] objectForKey:@"messageType"] forKey:@"type"];
    [tmp_dict_mut setObject:[messageDataDict valueForKey:@"favourCount"] forKey:@"likes"];
    
    NSArray *commentsArray = [messageDataDict valueForKey:@"messageResponseList"];
    [tmp_dict_mut setObject:[NSNumber numberWithInt:[commentsArray count]] forKey:@"comments"];
    
    NSDictionary *buzz = [messageDataDict valueForKey:@"wsBuzz"];
    
    [tmp_dict_mut setObject:[buzz valueForKey:@"headline"] forKey:@"headline"];
    
    [arrayOfMessages addObject:tmp_dict_mut];
    
    CategoryData *newData = [[[CategoryData alloc] initWithData:arrayOfMessages fromList:NO] autorelease];
    
    return newData;
}


- (void) dealloc
{
    [messageIDs release];
    [data release];
    [categoryData release];
    [super dealloc];
}

@end
