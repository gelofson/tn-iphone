//
//  FittingRoomLoadingView.m
//  QNavigator
//
//  Created by softprodigy on 22/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FittingRoomLoadingView.h"
#import<QuartzCore/QuartzCore.h>


@implementation FittingRoomLoadingView


#pragma mark -
#pragma mark Custom methods
-(void)startLoadingView:(id)target type:(int)type
{
	self.frame=CGRectMake(0, 0, 320, 480);
	
	if ([self viewWithTag:10])
	{
		[[self viewWithTag:10] removeFromSuperview];
	}
	
	//if (![self viewWithTag:10])
//	{
	if (type==1)
	{
		[self setBackgroundColor:[UIColor clearColor]];
		
		/*UIImageView *temp_loadingImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"fitting_room_loading_icon.png"]];
		[temp_loadingImage setFrame:CGRectMake(100, 200, 103, 49)];
		[temp_loadingImage setTag:10];*/
        
        UIView *temp_loadingImage = [[UIView alloc] initWithFrame:CGRectMake(100, 200, 103, 49)];
		[temp_loadingImage setTag:10];
        
        [temp_loadingImage setBackgroundColor:[UIColor colorWithWhite:0.10 alpha:0.75]];
        temp_loadingImage.layer.cornerRadius = 5.;
        
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [indicator startAnimating];
        indicator.frame = CGRectMake((temp_loadingImage.bounds.size.width - indicator.bounds.size.width)/2,
                                     (temp_loadingImage.bounds.size.height - indicator.bounds.size.height)/2,
                                     indicator.bounds.size.width, indicator.bounds.size.height);
        [temp_loadingImage addSubview:indicator];
        
        [self addSubview:temp_loadingImage];
		[temp_loadingImage release];
		temp_loadingImage=nil;		
		
		[[target view] addSubview:self];
		
	}
	else if	(type==2)
	{
		[self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
		
		UIImageView *temp_loadingImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"search_box.png"]];
		[temp_loadingImage setFrame:CGRectMake(100, 200, 123, 53)];
		[temp_loadingImage setTag:10];
		[self addSubview:temp_loadingImage];
		[temp_loadingImage release];
		temp_loadingImage=nil;	
		 
		[[target view] addSubview:self];
	}

	//}
//	else {
//		NSLog(@"Loading view already exists");
//	}
	
}


-(void)stopLoadingView
{
	//[(UIActivityIndicatorView *)[self viewWithTag:10] stopAnimating];
	//[[self viewWithTag:10] removeFromSuperview];
	[self removeFromSuperview];
	
}


#pragma mark Singleton Instance funtion

+ (id) SharedInstance 
{
	static id sharedManager = nil;
	
    if (sharedManager == nil) {
        sharedManager = [[self alloc] init];
    }
	
    return sharedManager;
}

- (void)dealloc {
    [super dealloc];
}

@end
