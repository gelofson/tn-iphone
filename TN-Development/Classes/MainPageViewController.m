//
//  MainPageViewController.m
//  TinyNews
//
//  Created by Nava Carmon on 5/10/13.
//
//

#import "MainPageViewController.h"
#import "QNavigatorAppDelegate.h"
#import "NewsDataManager.h"
#import "UIViewController+NibCells.h"
#import "NewsMoreViewController.h"
#import "CategoryData.h"
#import "Constants.h"

@interface MainPageViewController ()

@end

@implementation MainPageViewController

@synthesize categoriesTable, tableCells, moreController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.trackedViewName = @"Main Page screen";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    categoriesTable.rowHeight = 60.;
}

- (void) viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData) name:@"UpdateData" object:nil];

}

- (void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UpdateData" object:nil];

}

- (void) updateData
{
    [self.tableCells removeAllObjects];
    [self.categoriesTable reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)makeStory:(id)sender
{
    [tnApplication goToPage: SHARE_TAB_INDEX];
}

- (IBAction)findFriends:(id)sender
{
    [tnApplication goToPage: FIND_FRIENDS_TAB_INDEX];
}

- (IBAction)openNotifications:(id)sender
{
    [tnApplication goToPage:NOTIFICATIONS_TAB_INDEX];
}

- (IBAction)openNewsMap:(id)sender
{
    [tnApplication goToPage: GoogleMap_TAB_INDEX];
}

- (IBAction)openMyPage:(id)sender
{
    [tnApplication goToPage: MY_PAGE_TAB_INDEX];
}

- (IBAction)feedback:(id)sender
{
    [self presentModalViewController:[[JMC sharedInstance] feedbackViewController] animated:YES];
}

-(IBAction)showSettingViewController
{
    [tnApplication goToPage: SETTINGS_TAB_INDEX];
}

#pragma mark NewsDataManager delegate functions
- (void) updateCategories
{
    [self.tableCells removeAllObjects];
    [self.categoriesTable reloadData];
    [[NewsDataManager sharedManager] getNotificationsCount:self];
}


#pragma mark Table delegate functions
- (NSMutableArray *) tableCells
{
    if (!tableCells) {
        self.tableCells = [[[NSMutableArray alloc] init] autorelease];
    }
    return tableCells;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[NewsDataManager sharedManager] countActiveCategories];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"CategoryCell";
    
    UITableViewCell *cell = nil;
    NSInteger ind = [self.tableCells indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary   *dict = obj;
        
        return [[dict objectForKey:@"indexPath"] intValue] == indexPath.row;
    }];
    
    if (ind != NSNotFound) {
        NSDictionary *dict = [self.tableCells objectAtIndex:ind];
        cell = [dict objectForKey:@"cell"];
    } else {
        //    cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        //    if (nil == cell) {
        cell = [self loadReusableTableViewCellFromNibNamed:cellId];
        [self.tableCells addObject:[NSDictionary dictionaryWithObjectsAndKeys:cell, @"cell", [NSNumber numberWithInt:indexPath.row], @"indexPath", nil]];
    }

    UIImageView *categoryImage = (UIImageView *)[cell.contentView viewWithTag:2];
    UILabel *aLabel = (UILabel *)[cell.contentView viewWithTag:3];
    
    NSString *titleString = [[NewsDataManager sharedManager] getCategoryIdAtIndex:indexPath.row];
    aLabel.text = titleString;
    
    NSString *convertedName = [[NewsDataManager sharedManager] getCategoryNewName:titleString];
    UIImage *catImage = nil;
    
    if (!convertedName)
        catImage = [UIImage imageNamed:@"No-image"];
    else {
        catImage = [UIImage imageNamed:convertedName];
        aLabel.text = convertedName;
    }
       
    [categoryImage setImage:catImage];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryData *catData = [[NewsDataManager sharedManager] getCategoryDataByIndex:indexPath.row];
    self.moreController = [[[NewsMoreViewController alloc] initWithNibName:@"NewsMoreViewController" bundle:[NSBundle mainBundle]] autorelease];
    
    self.moreController.m_Type = catData.type;
    self.moreController.m_data = catData;
    self.moreController.curPage = 1;
    self.moreController.filter = POPULAR;
    self.moreController.m_CallBackViewController=self;
    self.moreController.fetchingData = YES;
    
	[self.navigationController pushViewController:self.moreController animated:YES];
    [[NewsDataManager sharedManager] getOneCategoryData:self.moreController type:self.moreController.m_Type filter:POPULAR number:24 numPage:1 addToExisting:NO];
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"NewsPage"
                                                    withAction:@"moreButtonClicked"
                                                     withLabel:nil
                                                     withValue:nil];
}

- (void) dealloc
{
    [tableCells release];
    [categoriesTable release];
    [super dealloc];
}

@end
