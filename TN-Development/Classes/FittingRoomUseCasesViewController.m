//
//  FittingRoomUseCasesViewController.m
//  QNavigator
//
//  Created by softprodigy on 21/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FittingRoomUseCasesViewController.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"
#import "FittingRoomMoreViewController.h"
#import "FittingRoomForMoreButtonViewController.h"
#import"FittingRoomBoughtItViewController.h"
#import<QuartzCore/QuartzCore.h>
#import "BuzzButtonUseCase.h"
#import "AsyncImageView.h"
#import"FittingRoomLoadingView.h"
#import "AsyncButtonView.h"
#import"FittingRoomMoreViewController.h"
#import"FittingRoomBoughtItViewController.h"
#import"Constants.h"
#import "PushNotificationViewController.h"
#import "UIView+Badge.h"
#import "NewsDataManager.h"

@implementation FittingRoomUseCasesViewController

#pragma mark - Properties
#pragma mark

@synthesize m_scrollView;
//@synthesize m_lableUseCase;
@synthesize m_mineView;
@synthesize m_MineBtn;
@synthesize m_FriendsBtn;
@synthesize m_FollowingBtn;
@synthesize m_NearbyBtn;
@synthesize m_PopularBtn;
@synthesize tagValue;
@synthesize m_exceptionPage;//**
@synthesize m_exceptionButton;//**
@synthesize notificationButton;//**
@synthesize totalRecordCountForActiveTag;
@synthesize sectionNumberToDraw;
@synthesize y;
int xCoordinate, yCoordinate, tmp_width, tmp_height;

QNavigatorAppDelegate * l_appDelegate;
ShopbeeAPIs * l_requestObj;
FittingRoomLoadingView * l_FittingRoomIndicatorView;
FittingRoomForMoreButtonViewController * l_MoreView;
FittingRoomMoreViewController * l_ViewRequestObj;
FittingRoomBoughtItViewController * l_boughtItRequest;
int x,height,width;
int Check; 
int MineCheck;// for checking that from which tab the user navigates to the more view controller

#define ROW_HEIGHT 173

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

#pragma mark -
#pragma mark

//view notification tray;
-(IBAction)viewNotification {//**
    PushNotificationViewController *notificationTray=[[PushNotificationViewController alloc] init];
    [self.navigationController   pushViewController:notificationTray animated:YES];
    [notificationButton removeBadge];
    [[[[[self tabBarController] tabBar] items] 
      objectAtIndex:0] setBadgeValue:nil];
    [notificationTray release];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewDidLoad {
	NSLog(@"fitting room view did load called\n");
			
	totalRecordCountForActiveTag = 0;
	m_messageIDsArrayBuyIt = [[NSMutableArray alloc] init];
	m_messageIDsArrayBoughtIt = [[NSMutableArray alloc] init];
    m_messageIDsArrayCheckThisOut = [[NSMutableArray alloc] init];
    m_messageIDsArrayHowDoesThisLook = [[NSMutableArray alloc] init];
    m_messageIDsArrayFoundASale = [[NSMutableArray alloc] init];
    [super viewDidLoad];
    
    // Bharat: 11/28/11: US121, set edge insets for title of exception button
    [m_exceptionButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 8, 5)];
    m_exceptionButton.hidden = YES;
    // Bharat : 11/26/11: Reducing scroll size by 50 pixels
    
    m_mineView.frame=CGRectMake(0, 127, 320, 345);
    //m_mineView.contentSize = CGSizeMake(self.m_scrollView.frame.size.width,750);
    m_mineView.contentSize = CGSizeMake(self.m_scrollView.frame.size.width,5*ROW_HEIGHT + 30);
    m_mineView.canCancelContentTouches = YES;
    // Bharat : 11/25/11: Adjusting upper height of scrollview
	//m_mineView.frame=CGRectMake(0, 154, 320, 364);
    
	m_requestObj = [[ShopbeeAPIs alloc] init];
	NSUserDefaults * prefs = [NSUserDefaults standardUserDefaults];
	m_UserId = [prefs valueForKey:@"userName"];
	
	l_FittingRoomIndicatorView=[FittingRoomLoadingView SharedInstance];
	[self sendRequestforAParticularBtn:nil] ;
	m_mainDictionary=[[NSMutableDictionary alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePopular:) name:@"UpdatePopular" object:nil];
    
	//Bharat: 11/11/11: Hiding the notifications button for now
	//Bharat: 11/14/11: No hiding neede. We are enabling the functionality now.
	//self.notificationButton.hidden = YES;
}

- (void) updateCategories
{
    [[NewsDataManager sharedManager] getAllCategoriesData:self filter:POPULAR number:3 numPage:1];
}

- (void) updatePopular:(NSNotification *)nf
{
	[self sendRequestforAParticularBtn:nil];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
	m_UserId=[prefs valueForKey:@"userName"];
	[[UIApplication sharedApplication]setStatusBarHidden:NO];
	[l_appDelegate.m_customView setHidden:YES];
	//[self sendRequestforAParticularBtn:nil];
	if (l_appDelegate.isFittingRoomShown==YES) // to update the info in case we logout and login again
	{
		l_appDelegate.isFittingRoomShown=NO;
	}
//    [m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
    [[NewsDataManager sharedManager] getActiveCategories:self];
}

-(void)viewDidAppear:(BOOL)animated {
	[l_appDelegate.m_customView setHidden:YES];
	
}

#pragma mark - Custom
#pragma mark

-(IBAction)btnPicturesAction:(id)sender {
	l_ViewRequestObj.m_CallBackViewController=self;		
	[self.navigationController pushViewController:l_ViewRequestObj animated:YES];
	[l_ViewRequestObj release];
	l_ViewRequestObj=nil;
	
}

-(IBAction)btnBoughtItPicturesAction:(id)sender {
	l_boughtItRequest.m_CallbackViewController=self;
	[self.navigationController pushViewController:l_boughtItRequest animated:YES];
}

-(IBAction)BtnMoreClicked:(id)sender {
	
	//tempFittingRoomMoreButton.m_Check=Check;
 	l_MoreView=[[FittingRoomForMoreButtonViewController alloc] init];
	l_MoreView.tmp_width=tmp_width;
	l_MoreView.tmp_height=tmp_height;
	l_MoreView.m_MoreBtnTagValue=[sender tag];
	l_MoreView.stringBuyitOrNot=[[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"];
	l_MoreView.m_Check=tagValue;
	NSLog(@"\n More Button has been Clicked m_MoreBtnTagValue=%d, m_Check=%d\n",l_MoreView.m_MoreBtnTagValue, l_MoreView.m_Check);
	[self.navigationController pushViewController:l_MoreView animated:YES];
	[l_MoreView release];
	l_MoreView=nil;
}

-(IBAction)IconClickedAction:(id)sender {
	int messageId=[sender tag];
	
	if (l_ViewRequestObj) 
	{
		[l_ViewRequestObj release];
		l_ViewRequestObj=nil;
	}
	
	if (l_boughtItRequest) 
	{
		[l_boughtItRequest release];
		l_boughtItRequest=nil;
	}
	
	l_ViewRequestObj=[[FittingRoomMoreViewController alloc] init];
	l_boughtItRequest=[[FittingRoomBoughtItViewController alloc] init];
	l_ViewRequestObj.m_messageId=messageId;
	l_boughtItRequest.m_currentMessageID=messageId;
    l_ViewRequestObj.GetPinnFlag=YES;
	
	//NSLog(@"Message Id is%@",[sender tag]);
	NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
	NSString *tmp_String=[prefs2 stringForKey:@"userName"];
	l_requestObj=[[ShopbeeAPIs alloc] init];

	//[l_FittingRoomIndicatorView startLoadingView:self type:1];

	[l_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:messageId];
    
}

- (void) createHeadlineLabel:(CGRect)picFrame inView:(UIView *)aView data:(NSArray *) theArray index:(NSInteger)ind
{
    picFrame.origin.y = picFrame.origin.y + picFrame.size.height + 3;
    picFrame.size.height = 32;
    UILabel *headlineLabel = [[UILabel alloc] initWithFrame:picFrame];
    headlineLabel.font =  [UIFont fontWithName:@"Helvetica" size:11];
    headlineLabel.lineBreakMode = UILineBreakModeCharacterWrap | UILineBreakModeTailTruncation;
    headlineLabel.textAlignment = UITextAlignmentCenter;
    headlineLabel.adjustsFontSizeToFitWidth = NO;
    headlineLabel.numberOfLines = 0;
    headlineLabel.baselineAdjustment = UIBaselineAdjustmentNone ;
    headlineLabel.textColor = [UIColor darkGrayColor];
    headlineLabel.text = [[theArray objectAtIndex:ind]valueForKey:@"headline"];
    [aView addSubview:headlineLabel];
//    headlineLabel.layer.borderColor = [UIColor greenColor].CGColor;
//    headlineLabel.layer.borderWidth = 1;

}


-(IBAction)addButtonsOnTheScreen:(UIView*)View NumberOfButtons:(NSInteger)NumberOfButtons type:(NSString*)type {
    
	NSLog(@"FittingRoomUseCasesViewController:addButtonsOnTheScreen: NumberOfButtons=%d, type=[%@]", NumberOfButtons, type);

    NSArray * theArray = nil;
    int moreButtonTag = 0;
    int whiteLineTag = 0;
    int sectionTitleTag = 0;
    int sectionTitleTextTag = 0;
    
    if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
        sectionTitleTag = 101;
        moreButtonTag = 102;
        whiteLineTag = 103;
        sectionTitleTextTag = 104;
    }
    else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
        sectionTitleTag = 201;
        moreButtonTag = 202;
        whiteLineTag = 203;
        sectionTitleTextTag = 204;
    }
    else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
        sectionTitleTag = 301;
        moreButtonTag = 302;
        whiteLineTag = 303;
        sectionTitleTextTag = 304;
    }
    else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
        sectionTitleTag = 401;
        moreButtonTag = 402;
        whiteLineTag = 403;
        sectionTitleTextTag = 404;
    }
    else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
        sectionTitleTag = 501;
        moreButtonTag = 502;
        whiteLineTag = 503;
        sectionTitleTextTag = 504;
    }
    
    switch (tagValue) {
            
        case 1:
            
            if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"MineBuyItArray"];
            }
            else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"MineBoughtItArray"];
            }
            else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"MineCheckThisOutArray"];
            }
            else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"MineHowDoesThisLookArray"];
            }
            else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"MineFoundASaleArray"];
            }
            
            MineCheck = 1;
            break;
            
        case 2:
            
            if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FriendsBuyItArray"];
            }
            else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FriendsBoughtItArray"];
            }
            else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FriendsCheckThisOutArray"];
            }
            else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FriendsFoundASaleArray"];
            }
            else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FriendsHowDoesThisLookArray"];
            }
            
            MineCheck = 0;
            break;
            
        case 3:
            
            if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FollowingBuyItArray"];
            }
            else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FollowingBoughtItArray"];
            }
            else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FollowingCheckThisOutArray"];
            }
            else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FollowingFoundASaleArray"];
            }
            else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FollowingHowDoesThisLookArray"];
            }
            
            MineCheck = 0;
            break;
            
        case 4:
            
            if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"NearByBuyItArray"];
            }
            else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"NearByBoughtItArray"];
            }
            else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"NearByCheckThisOutArray"];
            }
            else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"NearByFoundASaleArray"];
            }
            else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"NearByHowDoesThisLookArray"];
            }
            
            MineCheck = 0;
            break;
            
        case 5:
            
            if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularBuyItArray"];
            }
            else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularBoughtItArray"];
            }
            else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularCheckThisOutArray"];
            }
            else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularFoundASaleArray"];
            }
            else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularHowDoesThisLookArray"];
            }
            
            MineCheck = 0;
            break;
            
    }
    if (NumberOfButtons > 0) {
        
        //id sectionTitle;
        //CGPoint centerPoint;
        
        switch (sectionNumberToDraw) {
                
            case 1:
				// Bharat: 11/21/11: Pushing each view down by 12 pixels
                y = 43;
				/*
				Bharat: 11/26/11: Useless code. compute position and then apply. DO not mix everything.
                sectionTitle = [(id)m_mineView viewWithTag:401];
                [(id)m_mineView viewWithTag:403].hidden = NO;
                centerPoint.x = 281;
                centerPoint.y = 24.5;
                */
				break;
            case 2:                
				// Bharat: 11/21/11: Pushing each view down by 12 pixels
                y = 43 + 1*ROW_HEIGHT;
				/*
				Bharat: 11/26/11: Useless code. compute position and then apply. DO not mix everything.
                sectionTitle = [(id)m_mineView viewWithTag:201];
                [(id)m_mineView viewWithTag:203].hidden = NO;
                centerPoint.x = 281;
                centerPoint.y = 146.5;
                */
				break;
            case 3:
				// Bharat: 11/21/11: Pushing each view down by 12 pixels
                y = 43 + 2*ROW_HEIGHT;
				/*
				Bharat: 11/26/11: Useless code. compute position and then apply. DO not mix everything.
                sectionTitle = [(id)m_mineView viewWithTag:501];
                [(id)m_mineView viewWithTag:503].hidden = NO;
                centerPoint.x = 281;
                centerPoint.y = 269.5;
                */
				break;
            case 4:                
				// Bharat: 11/21/11: Pushing each view down by 12 pixels
                y = 43 + 3*ROW_HEIGHT;
				/*
				Bharat: 11/26/11: Useless code. compute position and then apply. DO not mix everything.
                sectionTitle = [(id)m_mineView viewWithTag:301];
                [(id)m_mineView viewWithTag:303].hidden = NO;
                centerPoint.x = 281;
                centerPoint.y = 392.5;
                */
				break;
            case 5:                
				// Bharat: 11/21/11: Pushing each view down by 12 pixels
                y = 43 + 4*ROW_HEIGHT;
				/*
				Bharat: 11/26/11: Useless code. compute position and then apply. DO not mix everything.
                sectionTitle = [(id)m_mineView viewWithTag:101];
                [(id)m_mineView viewWithTag:103].hidden = NO;
                centerPoint.x = 281;
                centerPoint.y = 515.5;
                */
				break;
                
        }
        
		UIImageView * sectionTitleView =(UIImageView *) [m_mineView viewWithTag:sectionTitleTag];
		sectionTitleView.hidden = NO;
        CGRect rct = sectionTitleView.frame;
		rct.origin.y = y - sectionTitleView.frame.size.height - 12;
		sectionTitleView.frame = rct;

               
		UILabel * tempText =(UILabel *) [m_mineView viewWithTag:sectionTitleTextTag];
		tempText.hidden = NO;
        rct = tempText.frame;
		rct.origin.y = y - tempText.frame.size.height - 6;
		tempText.frame = rct;
        tempText.font = [UIFont fontWithName:kMyriadProRegularFont size:18];
        CGSize  size = [tempText.text sizeWithFont:tempText.font minFontSize:18 actualFontSize:nil forWidth:rct.size.width lineBreakMode:UILineBreakModeTailTruncation];

		UIButton * moreButton =(UIButton *) [m_mineView viewWithTag:moreButtonTag];
		//moreButton.hidden = NO;
        rct = moreButton.frame;
		rct.origin.y = y - moreButton.frame.size.height - 12;
		moreButton.frame = rct;
        
		UIImageView * whiteLineView =(UIImageView *) [m_mineView viewWithTag:whiteLineTag];
		whiteLineView.hidden = NO;
        rct = whiteLineView.frame;
        rct.origin.x = tempText.frame.origin.x + size.width + 2;
        rct.size.width = moreButton.frame.origin.x + moreButton.frame.size.width - rct.origin.x - 10;
		rct.origin.y = y - whiteLineView.frame.size.height - 8;
		whiteLineView.frame = rct;
        
        sectionNumberToDraw++;

		int numberOfItemsInARow = DEFAULT_NUMBEROF_ITEMS_IN_ROW;
		int interItemGapInPixels = DEFAULT_INTER_ITEM_GAP;

		CGFloat iconWidth = ((320.0f - DEFAULT_MARGIN_WIDTH_FROM_SIDES*2.0f) - (interItemGapInPixels*(numberOfItemsInARow -1)))/(numberOfItemsInARow*1.0f);
		CGFloat iconHeight = iconWidth;
        
        if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
            //Bharat: 11/21/11: section title no more text
            //[sectionTitle setText:@"Buy it or not?"];
            xCoordinate = DEFAULT_MARGIN_WIDTH_FROM_SIDES;//, yCoordinate = 50;	
            tmp_width = iconWidth, tmp_height = iconHeight;
            
            for (int i = 0; i < NumberOfButtons; i++) {
                
                if (i % numberOfItemsInARow == 0 && i != 0) {
                    xCoordinate = DEFAULT_MARGIN_WIDTH_FROM_SIDES;
                    y = y + tmp_height + DEFAULT_MARGIN_WIDTH_FROM_TOP;
                }
                else if (i != 0) {
                    xCoordinate = xCoordinate + tmp_width + interItemGapInPixels;
                }
                
                NSString * Url = [NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[[theArray objectAtIndex:i]valueForKey:@"senderThumbImageUrl"],iconWidth,iconHeight];
                
                NSURL * temp_loadingUrl = [NSURL URLWithString:Url];
                CGRect picFrame = CGRectMake(xCoordinate,y,tmp_width,tmp_height);
                AsyncButtonView * asyncButton = [[[AsyncButtonView alloc]
                                                  initWithFrame:picFrame] autorelease];
                asyncButton.tag = [[[theArray objectAtIndex:i] valueForKey:@"messageId"] intValue];
//                asyncButton.layer.shadowColor = [UIColor grayColor].CGColor;
//                asyncButton.layer.shadowOffset = CGSizeMake(1, -1);
//                asyncButton.layer.shadowOpacity = 0.5;
                [asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[[theArray objectAtIndex:i]valueForKey:@"senderName"]];
                
                [View addSubview:asyncButton];
                
                [self createHeadlineLabel:picFrame inView:View data:theArray index:i];
            }
            
        }
        else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
            //Bharat: 11/21/11: section title no more text
            //[sectionTitle setText:@"I bought it!"];
            x = DEFAULT_MARGIN_WIDTH_FROM_SIDES;//, y = 180;
            tmp_width = iconWidth, tmp_height = iconHeight;
            
            for (int i=0; i<NumberOfButtons; i++) {
                
                if (i%numberOfItemsInARow==0 && i!=0) {
                    x=DEFAULT_MARGIN_WIDTH_FROM_SIDES;
                    y=y+tmp_height+DEFAULT_MARGIN_WIDTH_FROM_TOP;
                    
                }
                else if(i!=0)
                {
                    x=x+tmp_width+interItemGapInPixels;
                }
                NSString *Url=[NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[[theArray objectAtIndex:i]valueForKey:@"senderThumbImageUrl"],iconWidth,iconHeight];
                
                NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
                CGRect picFrame = CGRectMake(x,y,tmp_width,tmp_height);
                AsyncButtonView* asyncButton = [[[AsyncButtonView alloc]
                                                 initWithFrame:picFrame] autorelease];
                asyncButton.tag=[[[theArray objectAtIndex:i] valueForKey:@"messageId"] intValue];
                //NSLog(@"%@",[asyncButton tag]);
                
//                asyncButton.layer.shadowColor = [UIColor grayColor].CGColor;
//                asyncButton.layer.shadowOffset = CGSizeMake(1, -1);
//                asyncButton.layer.shadowOpacity = 0.5;
                [asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[[theArray objectAtIndex:i]valueForKey:@"senderName"]];
                
                [View  addSubview:asyncButton];
                

                [self createHeadlineLabel:picFrame inView:View data:theArray index:i];
            }
        }
        else {
            if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                //Bharat: 11/21/11: section title no more text
                //[sectionTitle setText:@"Check this out..."];
                x = DEFAULT_MARGIN_WIDTH_FROM_SIDES;//, y = 310;
                tmp_width = iconWidth, tmp_height = iconHeight;
                
                for (int i = 0; i < NumberOfButtons; i++) {
                    if ((i % numberOfItemsInARow == 0) && (i != 0)) {
                        x = DEFAULT_MARGIN_WIDTH_FROM_SIDES;
                        y = y + tmp_height + DEFAULT_MARGIN_WIDTH_FROM_TOP;
                    }
                    else if (i != 0) {
                        x = x + tmp_width + interItemGapInPixels;
                    }
                    NSString * Url = [NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[[theArray objectAtIndex:i]valueForKey:@"senderThumbImageUrl"],iconWidth,iconHeight];
                    
                    NSURL * temp_loadingUrl = [NSURL URLWithString:Url];
                    
                    CGRect picFrame = CGRectMake(x,y,tmp_width,tmp_height);
                    AsyncButtonView * asyncButton = [[[AsyncButtonView alloc]
                                                      initWithFrame:picFrame] autorelease];
                    asyncButton.tag=[[[theArray objectAtIndex:i] valueForKey:@"messageId"] intValue];            
                    
//                    asyncButton.layer.shadowColor = [UIColor grayColor].CGColor;
//                    asyncButton.layer.shadowOffset = CGSizeMake(1, -1);
//                    asyncButton.layer.shadowOpacity = 0.5;
                    [asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[[theArray objectAtIndex:i]valueForKey:@"senderName"]];
                   
                    

                    [View  addSubview:asyncButton];


                    [self createHeadlineLabel:picFrame inView:View data:theArray index:i];
                }
            }
            else {
                if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                    //Bharat: 11/21/11: section title no more text
                    //[sectionTitle setText:@"How do I look?"];
                    x = DEFAULT_MARGIN_WIDTH_FROM_SIDES;//, y = 440;
                    tmp_width = iconWidth, tmp_height = iconHeight;
                    
                    for (int i = 0; i < NumberOfButtons; i++) {
                        if (((i % numberOfItemsInARow) == 0) && (i != 0)) {
                            x = DEFAULT_MARGIN_WIDTH_FROM_SIDES;
                            y = y + tmp_height + DEFAULT_MARGIN_WIDTH_FROM_TOP;
                        }
                        else if (i != 0) {
                            x = x + tmp_width + interItemGapInPixels;
                        }
                        NSString * Url = [NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[[theArray objectAtIndex:i]valueForKey:@"senderThumbImageUrl"],tmp_width,tmp_height];
                        
                        NSURL * temp_loadingUrl = [NSURL URLWithString:Url];
                        
                        CGRect picFrame = CGRectMake(x,y,tmp_width,tmp_height);
                        AsyncButtonView * asyncButton = [[[AsyncButtonView alloc]
                                                          initWithFrame:picFrame] autorelease];
                        asyncButton.tag=[[[theArray objectAtIndex:i] valueForKey:@"messageId"] intValue];            
                        
//                        asyncButton.layer.shadowColor = [UIColor grayColor].CGColor;
//                        asyncButton.layer.shadowOffset = CGSizeMake(1, -1);
//                        asyncButton.layer.shadowOpacity = 0.5;
                        [asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[[theArray objectAtIndex:i]valueForKey:@"senderName"]];
                        
                        [View  addSubview:asyncButton];
                        

                        [self createHeadlineLabel:picFrame inView:View data:theArray index:i];
                    }
                }
                else {
                    if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                        //Bharat: 11/21/11: section title no more text
                        //[sectionTitle setText:@"Found a sale!"];
                        x = DEFAULT_MARGIN_WIDTH_FROM_SIDES;//, y = 570;
                        tmp_width = iconWidth, tmp_height = iconHeight;
                        
                        for (int i = 0; i < NumberOfButtons; i++) {
                            if ((i % numberOfItemsInARow == 0) && (i != 0)) {
                                x = DEFAULT_MARGIN_WIDTH_FROM_SIDES;
                                y = y + tmp_height + DEFAULT_MARGIN_WIDTH_FROM_TOP;
                            }
                            else if (i != 0) {
                                x = x + tmp_width + interItemGapInPixels;
                            }
                            NSString * Url = [NSString stringWithFormat:@"%@%@&width=%d&height=%d",kServerUrl,[[theArray objectAtIndex:i]valueForKey:@"senderThumbImageUrl"],iconWidth,iconHeight];
                            
                            NSURL * temp_loadingUrl = [NSURL URLWithString:Url];
                            
                            CGRect picFrame = CGRectMake(x,y,tmp_width,tmp_height);
                            AsyncButtonView * asyncButton = [[[AsyncButtonView alloc]
                                                              initWithFrame:picFrame] autorelease];
                            asyncButton.tag=[[[theArray objectAtIndex:i] valueForKey:@"messageId"] intValue];            
                            
//                            asyncButton.layer.shadowColor = [UIColor grayColor].CGColor;
//                            asyncButton.layer.shadowOffset = CGSizeMake(1, -1);
//                            asyncButton.layer.shadowOpacity = 0.5;
                           [asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(IconClickedAction:) btnText:[[theArray objectAtIndex:i]valueForKey:@"senderName"]];
                           
                            
 
                            [View  addSubview:asyncButton];

                            
                            [self createHeadlineLabel:picFrame inView:View data:theArray index:i];
                        }
                    }
                }
            }
        }
        
        //[sectionTitle setHidden:NO];
        
    }
    
    return;
}




-(void)hideLabelsAndButtons {
    
    int tag = 101;
    for (tag = 101; tag <= 504; tag++) {
		if ([m_mineView viewWithTag:tag] != nil)
        	[(id)m_mineView viewWithTag:tag].hidden = YES;
    }
    
}

-(void)showLabelsAndButtons {
    
    int tag = 101;
    for (tag = 101; tag <= 504; tag++) {
		if ([m_mineView viewWithTag:tag] != nil)
        	[(id)m_mineView viewWithTag:tag].hidden = NO;
    }
    
}

-(IBAction)sendRequestforAParticularBtn:(id)sender {//send request for a particular button  
	
	totalRecordCountForActiveTag = 0;
	[m_exceptionPage setImage:[UIImage imageNamed:@"background_runway"]];
    m_exceptionButton.hidden = YES;

	[self hideLabelsAndButtons];
    // Reset Scroll Position
    [m_mineView setContentOffset:CGPointZero];
    
	if (sender == nil) {
		tagValue=5;//for the first time when the view is loaded
	}
	else {
		tagValue=[sender tag];	
	}
    
    int recordsCount;
    
    sectionNumberToDraw = 1;
    y = 50;
	
	switch (tagValue) {
            
		case 1:
            //Mine button
			for (UIView *tempView in m_mineView.subviews )
			{
				if ([tempView isKindOfClass:[AsyncButtonView class]])//[tempView tag]!=101&&[tempView tag]!=102&&[tempView tag]!=103&&[tempView tag]!=201
					//				&&[tempView tag]!=202&&[tempView tag]!=203) 
				{
					[tempView removeFromSuperview];
				}
				
			}
			[m_mineView removeFromSuperview];
			
			
			Check=1;
			
			//m_lableUseCase.text=@"Mine";

			// Bharat: 11/26/11: No setting of bg color
			//m_mineView.backgroundColor=[UIColor colorWithRed:.39f green:.69f blue:.75f alpha:1.0];
			
			[self.view addSubview:m_mineView];
			m_MineBtn.selected=YES;
			m_FriendsBtn.selected=NO;
			m_FollowingBtn.selected=NO;
			m_NearbyBtn.selected=NO;
			m_PopularBtn.selected=NO;
			
			l_ViewRequestObj.MineTrack=YES;
			l_boughtItRequest.MineTrack=YES;
           // [l_FittingRoomIndicatorView startLoadingView:self type:1];

			//Bharat : 11/25/11: Chinging the order of icon download
            //[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:4 pageno:1];
            //recordsCount = [[m_mainDictionary valueForKey:@"MineBuyItArray_TotalRecords"] intValue];
			[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
            
            recordsCount = [[m_mainDictionary valueForKey:@"MineHowDoesThisLookArray_TotalRecords"] intValue];
            
            
			break;
			
		case 2:
			for (UIView *tempView in m_mineView.subviews )
			{
				if ([tempView isKindOfClass:[AsyncButtonView class]])//[tempView tag]!=101&&[tempView tag]!=102&&[tempView tag]!=103&&[tempView tag]!=201
					//				&&[tempView tag]!=202&&[tempView tag]!=203) 
				{
					[tempView removeFromSuperview];
				}
				
			}
			[m_mineView removeFromSuperview];
			
			
			//m_lableUseCase.text=@"Friends";
			
			Check=1;

			// Bharat: 11/25/11: Xib is setting clear color
			//m_mineView.backgroundColor=[UIColor colorWithRed:.40f green:.71f blue:.78f alpha:1.0];;
			[self.view addSubview:m_mineView];
			m_MineBtn.selected=NO;
			m_FriendsBtn.selected=YES;
			m_FollowingBtn.selected=NO;
			m_NearbyBtn.selected=NO;
			m_PopularBtn.selected=NO;
			
			
			l_ViewRequestObj.MineTrack=NO;
			l_boughtItRequest.MineTrack=NO;
			//if ([m_mainDictionary valueForKey:@"FriendsBuyItArray"]==nil||[m_mainDictionary valueForKey:@"FriendsBoughtItArray"]==nil) {
			//Updated by Gauri
            //[l_FittingRoomIndicatorView startLoadingView:self type:1];

			//Bharat : 11/25/11: Chinging the order of icon download
			//[m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:4 pageno:1];
            //recordsCount = [[m_mainDictionary valueForKey:@"FriendsBuyItArray_TotalRecords"] intValue];
			[m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
			
            recordsCount = [[m_mainDictionary valueForKey:@"FriendsHowDoesThisLookArray_TotalRecords"] intValue];
           
			//[self addButtonsOnTheScreen:m_mineView NumberOfButtons:4 lngth:60 hght:60 xCoordinate:10 yCoordinate:50];	
			//}
			break;
		case 3:

            for (UIView *tempView in m_mineView.subviews )
			{
				if ([tempView isKindOfClass:[AsyncButtonView class]])
				{
					[tempView removeFromSuperview];
				}
			}
            
			[m_mineView removeFromSuperview];
			
			//m_lableUseCase.text=@"Following";
			Check=2;

			// Bharat: 11/25/11: Xib is setting clear color
			//m_mineView.backgroundColor=[UIColor colorWithRed:.40f green:.71f blue:.78f alpha:1.0];;
			[self.view addSubview:m_mineView];
			
			m_MineBtn.selected=NO;
			m_FriendsBtn.selected=NO;
			m_FollowingBtn.selected=YES;
			m_NearbyBtn.selected=NO;
			m_PopularBtn.selected=NO;
			
			l_ViewRequestObj.MineTrack=NO;
			l_boughtItRequest.MineTrack=NO;
            //[l_FittingRoomIndicatorView startLoadingView:self type:1];

			//Bharat : 11/25/11: Chinging the order of icon download
			//[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:4 pageno:1];
            //recordsCount = [[m_mainDictionary valueForKey:@"FollowingBuyItArray_TotalRecords"] intValue];
			[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
            
            recordsCount = [[m_mainDictionary valueForKey:@"FollowingHowDoesThisLookArray_TotalRecords"] intValue];
            
			break;
		case 4:
            /*
			for (UIView *tempView in [m_mineView viewWithTag:200].subviews)
			{
				if ([tempView isKindOfClass:[AsyncButtonView class]])//[tempView tag]!=101&&[tempView tag]!=102&&[tempView tag]!=103&&[tempView tag]!=201
					//				&&[tempView tag]!=202&&[tempView tag]!=203&&[tempView tag]!=401) 
				{
					[tempView removeFromSuperview];
				}
				
			}*/
            for (UIView *tempView in m_mineView.subviews )
			{
				if ([tempView isKindOfClass:[AsyncButtonView class]])
				{
					[tempView removeFromSuperview];
				}
			}
			
			[m_mineView removeFromSuperview];
			
			//m_lableUseCase.text=@"Nearby";
			Check=2;

			// Bharat: 11/25/11: Xib is setting clear color
			//m_mineView.backgroundColor=[UIColor colorWithRed:.46f green:.82f blue:.89f alpha:1.0f];
			self.m_scrollView.contentSize = CGSizeMake(self.m_scrollView.contentSize.width,410);
			self.m_scrollView.backgroundColor = [UIColor clearColor];
			[self.view addSubview:m_mineView];
			
			m_MineBtn.selected=NO;
			m_FriendsBtn.selected=NO;
			m_FollowingBtn.selected=NO;
			m_NearbyBtn.selected=YES;
			m_PopularBtn.selected=NO;
			
			
			l_ViewRequestObj.MineTrack=NO;
			l_boughtItRequest.MineTrack=NO;
			//if ([m_mainDictionary valueForKey:@"NearByBuyItArray"]==nil||[m_mainDictionary valueForKey:@"NearByBoughtItArray"]==nil) {
			//Updated by Gauri12/08	
            //[l_FittingRoomIndicatorView startLoadingView:self type:1];

			//Bharat : 11/25/11: Chinging the order of icon download
			//[m_requestObj getUnreadRequestsForNearby:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:4 pageno:1];
            //recordsCount = [[m_mainDictionary valueForKey:@"NearByBuyItArray_TotalRecords"] intValue];
			[m_requestObj getUnreadRequestsForNearby:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
			
            recordsCount = [[m_mainDictionary valueForKey:@"NearByHowDoesThisLookArray_TotalRecords"] intValue];
            
			break;
			
		case 5:
			for (UIView *tempView in m_mineView.subviews )
			{
				if ([tempView isKindOfClass:[AsyncButtonView class]])//[tempView tag]!=101&&[tempView tag]!=102&&[tempView tag]!=103&&[tempView tag]!=201
					//				&&[tempView tag]!=202&&[tempView tag]!=203) 
				{
					[tempView removeFromSuperview];
				}
				
			}
			[m_mineView removeFromSuperview];
			
			//m_lableUseCase.text=@"Popular";
			Check=3;
			// Bharat: 11/25/11: Xib is setting clear color
			//m_mineView.backgroundColor=[UIColor colorWithRed:.58f green:.84f blue:.90f alpha:1.0];
            
			[self.view addSubview:m_mineView];
			
			m_MineBtn.selected=NO;
			m_FriendsBtn.selected=NO;
			m_FollowingBtn.selected=NO;
			m_NearbyBtn.selected=NO;
			m_PopularBtn.selected=YES;
			
			l_ViewRequestObj.MineTrack=NO;
			l_boughtItRequest.MineTrack=NO;
			//if ([m_mainDictionary valueForKey:@"PopularBuyItArray"]==nil||[m_mainDictionary valueForKey:@"PopularBoughtItArray"]==nil) {
			//Updated by Gauri12/08
           // [l_FittingRoomIndicatorView startLoadingView:self type:1];

			//Bharat : 11/25/11: Chinging the order of icon download
			//[m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:4 pageno:1];
            //recordsCount = [[m_mainDictionary valueForKey:@"PopularBuyItArray_TotalRecords"] intValue];
			[m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:@"Food/Drink" number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];

            recordsCount = [[m_mainDictionary valueForKey:@"PopularHowDoesThisLookArray_TotalRecords"] intValue];
            NSLog(@"thje count value is %d",recordsCount);
            NSLog(@"the response data is %@",m_mainDictionary);
			break;
			
		default:
            
			break;
	}


}

#pragma mark - Callback Methods
#pragma mark

-(void)requestCallBackMethodForBuyItOrNot:(NSNumber *)responseCode responseData:(NSData *)responseData {
	//[l_FittingRoomIndicatorView stopLoadingView ];
	NSString *tempString = nil;
	if ([responseCode intValue]==200) 
	{
		
		switch (tagValue) 
		{
				
			case 1:// to send request for mine usecase ie get data for mine buy it use case
				NSLog(@"data downloaded");
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
				m_buyItArray=[tempString JSONValue];
				NSArray *tmp_fittingRoomSummary=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				if ([m_messageIDsArrayBuyIt count]) {
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummary count]; i++) {
					NSInteger MessageID=[[[tmp_fittingRoomSummary objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBuyIt addObject:[NSNumber numberWithInt:MessageID]];
				}
				
				
				int Count=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += Count;

				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"]forKey:@"MineBuyItArray"];// put value in the the dictionary so next time we can check 
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"MineBuyItArray_TotalRecords"];
					
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"MineBuyItArray"];
					
				}
				
				NSArray *tmp_array=[m_mainDictionary valueForKey:@"MineBuyItArray"];
				// for adding buttons for mine "buy it" use case 
				int tmp_count;
				if ([tmp_array count]<4) 
				{							// if the count returned by the webservice is less than four
					tmp_count=[tmp_array count];
					
				}
				else 
				{
					tmp_count=4;
					
				}
				if (Count > DEFAULT_NUMBEROF_ITEMS_IN_ROW) {
					[(UIButton*)m_mineView viewWithTag:102].hidden=NO;	
				}
				else {
					[(UIButton*)m_mineView viewWithTag:102].hidden=YES;	
				}


				/*
				Bharat: 11/25/11: Handled in requestCallBackMethodForHowDoesThisLook method 
				if (tmp_count==0) {
                    [m_exceptionPage setImage:[UIImage imageNamed:@"mine-page.png"]];
                    [m_mineView setHidden:YES];
                }
                if (tmp_count>0) {
                    [self.view addSubview:m_mineView];
                    [m_mineView setHidden:NO];
                }
                */
                
				[self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_count type:@"BuyIt"];
                

				//Bharat : 11/25/11: Chinging the order of icon download
                //[l_FittingRoomIndicatorView startLoadingView:self type:1];
				//[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:4 pageno:1];
				
				break;
				
			case 2:
				// to send request for friends buy it usecase
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
				m_buyItArray=[tempString JSONValue];
				NSArray *tmp_fittingRoomSummary1=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				if ([m_messageIDsArrayBuyIt count]) {
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummary1 count]; i++) {
					NSInteger MessageID1=[[[tmp_fittingRoomSummary1 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBuyIt addObject:[NSNumber numberWithInt:MessageID1]];
				}
				int Tmp_Count=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += Tmp_Count;

				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"]forKey:@"FriendsBuyItArray"];// put value in the the dictionary so next time we can check 
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FriendsBuyItArray_TotalRecords"];
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"FriendsBuyItArray"];
					
				}
				NSArray *tmp_arrayfrnds=[m_mainDictionary valueForKey:@"FriendsBuyItArray"];
				
				int tmp_frndscount;
				if ([tmp_arrayfrnds count]<4) 
				{							// if the count returned by the webservice is less than four
					tmp_frndscount=[tmp_arrayfrnds count];
					
				}
				else 
				{
					tmp_frndscount=4;
					
				}
				if (Tmp_Count > DEFAULT_NUMBEROF_ITEMS_IN_ROW) {
					[(UIButton*)m_mineView viewWithTag:102].hidden=NO;
				}
				else {
					[(UIButton*)m_mineView viewWithTag:102].hidden=YES;	
				}
				/*
				Bharat: 11/25/11: Handled in requestCallBackMethodForHowDoesThisLook method 
				if (tmp_frndscount==0) {
                    [m_exceptionPage setImage:[UIImage imageNamed:@"friends-page.png"]];
                    [m_mineView setHidden:YES];
                }
				
                if (tmp_frndscount>0) {
                    [self.view addSubview:m_mineView];
                    [m_mineView setHidden:NO];
                }
                */
                
				[self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_frndscount type:@"BuyIt"];
				
				//Bharat : 11/25/11: Chinging the order of icon download
                //[l_FittingRoomIndicatorView startLoadingView:self type:1];
				//[m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:4 pageno:1];
				
				break;
			case 3: // to send request for following buy it usecase
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
				m_buyItArray=[tempString JSONValue];
				NSArray *tmp_fittingRoomSummary3=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if ([m_messageIDsArrayBuyIt count]) {
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				
				for (int i=0; i<[tmp_fittingRoomSummary3 count]; i++) {
					NSInteger MessageID3=[[[tmp_fittingRoomSummary3 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBuyIt addObject:[NSNumber numberWithInt:MessageID3]];
				}
				int TotalRecordsFollowingBuyIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsFollowingBuyIt;

				
				
				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"]forKey:@"FollowingBuyItArray"];// put value in the the dictionary so next time we can check 
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FollowingBuyItArray_TotalRecords"];
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"FollowingBuyItArray"]	;
				}
				
				NSArray *tmp_arrayfollowing=[m_mainDictionary valueForKey:@"FollowingBuyItArray"];
				
				int tmp_followingcount;
				if ([tmp_arrayfollowing count]<4) 
				{							// if the count returned by the webservice is less than four
					tmp_followingcount=[tmp_arrayfollowing count];
					
				}
				else 
				{
					tmp_followingcount=4;
					
				}
				if (TotalRecordsFollowingBuyIt > DEFAULT_NUMBEROF_ITEMS_IN_ROW) 
				{
					[(UIButton*)m_mineView viewWithTag:102].hidden=NO;	
				}
				else
				{
					[(UIButton*)m_mineView viewWithTag:102].hidden=YES;
				}
				/*
				Bharat: 11/25/11: Handled in requestCallBackMethodForHowDoesThisLook method 
                if (tmp_followingcount==0) {
                    [m_exceptionPage setImage:[UIImage imageNamed:@"no-following.png"]];
                    [m_mineView setHidden:YES];
                }
                if (tmp_followingcount>0) {
                    [m_mineView setHidden:NO];
                    [self.view addSubview:m_mineView];
                }
				*/
				[self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_followingcount type:@"BuyIt"];
				
				//Bharat : 11/25/11: Chinging the order of icon download
                //[l_FittingRoomIndicatorView startLoadingView:self type:1];
				//[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:4 pageno:1];
				
				break;
			case 4: // for sending nearby request for buy it
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                
				m_buyItArray=[tempString JSONValue];
				
				NSArray *tmp_fittingRoomSummary4=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				if ([m_messageIDsArrayBuyIt count]) {
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				
				for (int i=0; i<[tmp_fittingRoomSummary4 count]; i++) {
					
					NSInteger MessageID4=[[[tmp_fittingRoomSummary4 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBuyIt addObject:[NSNumber numberWithInt:MessageID4]];
					
				}
				int TotalRecordsNearbyBuyIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsNearbyBuyIt;

				
				
				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"]forKey:@"NearByBuyItArray"];// put value in the the dictionary so next time we can check 
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"NearByBuyItArray_TotalRecords"];
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"NearByBuyItArray"];
				}
				
				NSArray *tmp_arrayPopular=[m_mainDictionary valueForKey:@"NearByBuyItArray"];
				
				int tmp_Popularcount;
				if ([tmp_arrayPopular count]<4) 
				{							// if the count returned by the webservice is less than four
					tmp_Popularcount=[tmp_arrayPopular count];
					
				}
				else 
				{
					tmp_Popularcount=4;
					
				}
				if (TotalRecordsNearbyBuyIt > DEFAULT_NUMBEROF_ITEMS_IN_ROW) {
					[(UIButton*)m_mineView viewWithTag:102].hidden=NO;
				}
				else {
					[(UIButton*)m_mineView viewWithTag:102].hidden=YES;
				}
				/*
				Bharat: 11/25/11: Handled in requestCallBackMethodForHowDoesThisLook method 
				if (tmp_Popularcount==0) {
                    [m_exceptionPage setImage:[UIImage imageNamed:@"no-nearby.png"]];
                    [m_mineView removeFromSuperview];
                }
                if (tmp_Popularcount>0) {
                    [m_mineView setHidden:NO];
                    [self.view addSubview:m_mineView];                    
                }
                */
                
				[self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_Popularcount type:@"BuyIt"];
				
				//Bharat : 11/25/11: Chinging the order of icon download
                //[l_FittingRoomIndicatorView startLoadingView:self type:1];
				//[m_requestObj getUnreadRequestsForNearby:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:4 pageno:1];
				
				
				break;
				
			case 5: // to send request for popular buy it usecase
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
				m_buyItArray=[tempString JSONValue];
				
				NSArray *tmp_fittingRoomSummary5=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				if ([m_messageIDsArrayBuyIt count]) {
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummary5 count]; i++) {
					NSInteger MessageID5=[[[tmp_fittingRoomSummary5 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBuyIt addObject:[NSNumber numberWithInt:MessageID5]];
				}
				int PopularRecordsNearbyBuyIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += PopularRecordsNearbyBuyIt;

				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"]forKey:@"PopularBuyItArray"];// put value in the the dictionary so next time we can check 
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularBuyItArray_TotalRecords"];
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"PopularBuyItArray"];
				}
				NSArray *tmp_popularArray=[m_mainDictionary valueForKey:@"PopularBuyItArray"];
				
				int tmp_popularCount;
				if ([tmp_popularArray count]<10) 
				{							// if the count returned by the webservice is less than four
					tmp_popularCount=[tmp_popularArray count];
					
				}
				else 
				{
					tmp_popularCount=10;
					
				}
				if (PopularRecordsNearbyBuyIt > DEFAULT_NUMBEROF_ITEMS_IN_ROW) {
					[(UIButton*)m_mineView viewWithTag:102].hidden=NO;	
				}
				else 
				{
					[(UIButton*)m_mineView viewWithTag:102].hidden=YES;	
				}
                
                [self.view addSubview:m_mineView];
                [m_mineView setHidden:NO];
				
				[self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_popularCount type:@"BuyIt"];
				
				//Bharat : 11/25/11: Chinging the order of icon download
                //[l_FittingRoomIndicatorView startLoadingView:self type:1];
				//[m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:4 pageno:1];
				
				break;
			default:
				break;
		}
        
		if (totalRecordCountForActiveTag <= 0) {
			if (tagValue == 1) {
				// Bharat: 11/27/11: DE22 fixes
				//[m_exceptionPage setImage:[UIImage imageNamed:@"mine-page.png"]];
				[m_exceptionPage setImage:[UIImage imageNamed:@"exception_paparazzi"]];
				[m_exceptionButton setTitle:@"Take pictures" forState:UIControlStateNormal];
				[m_exceptionButton setTitle:@"Take pictures" forState:UIControlStateHighlighted];
                
    			m_exceptionButton.hidden = NO;
			} else if (tagValue == 2) {
				// Bharat: 11/27/11: DE22 fixes
				//[m_exceptionPage setImage:[UIImage imageNamed:@"friends-page.png"]];
				[m_exceptionPage setImage:[UIImage imageNamed:@"exception_ADD FRIEND IN MESSAGE"]];
				[m_exceptionButton setTitle:@"Add friends" forState:UIControlStateNormal];
				[m_exceptionButton setTitle:@"Add friends" forState:UIControlStateHighlighted];
    			m_exceptionButton.hidden = NO;
			} else if (tagValue == 3) {
				// Bharat: 11/27/11: DE22 fixes
				//[m_exceptionPage setImage:[UIImage imageNamed:@"no-following.png"]];
				[m_exceptionPage setImage:[UIImage imageNamed:@"exception_following"]];
    			m_exceptionButton.hidden = YES;
			} else if (tagValue == 4) {
				[m_exceptionPage setImage:[UIImage imageNamed:@"no-nearby.png"]];
    			m_exceptionButton.hidden = YES;
			}
			[m_mineView setHidden:YES];
		} else {
    		m_exceptionButton.hidden = YES;
			[self.view addSubview:m_mineView];
			[m_mineView setHidden:NO];
		}
			
		// Update Scroll Size //
        
        int scrollHeight = 5*ROW_HEIGHT + 30;
        for (int index = 6; index > sectionNumberToDraw; index--) {
            scrollHeight -= ROW_HEIGHT;
        }
        
        CGSize scrollSize = CGSizeMake(m_mineView.contentSize.width, scrollHeight);
        
        [m_mineView setContentSize:scrollSize];
        
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		//[l_FittingRoomIndicatorView stopLoadingView];
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		//[l_FittingRoomIndicatorView stopLoadingView];
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else 
	{
		//[l_FittingRoomIndicatorView stopLoadingView];
	}
	
	if (tempString) 
	{
		[tempString release];
		tempString=nil;
	}		
	
    //[l_FittingRoomIndicatorView stopLoadingView];
	
}

-(void)requestCallBackMethodForIBoughtIt:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSString *tempString;
	tempString=nil;
	//[l_FittingRoomIndicatorView stopLoadingView];
	if ([responseCode intValue] == 200) {
        NSUserDefaults * prefs = [NSUserDefaults standardUserDefaults];
        NSString * custID = [prefs objectForKey:@"userName"];
		switch (tagValue) 
		{
			case 1:
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];			
				m_buyItArray=[tempString JSONValue];
				int TotalRecordsMineBoughtIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsMineBoughtIt;

				
				NSArray *tmp_fittingRoomSummaryBoughtIt=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if (m_messageIDsArrayBoughtIt) { // remove the previous objects in the aray.
					[m_messageIDsArrayBoughtIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummaryBoughtIt count]; i++) // Fill the message ids for all the records in an array.
				{
					NSInteger MessageID=[[[tmp_fittingRoomSummaryBoughtIt objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBoughtIt addObject:[NSNumber numberWithInt:MessageID]];
				}				
				
				
				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"] forKey:@"MineBoughtItArray"];
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecord"] forKey:@"MineBoughtItArray_TotalRecords"];
				}
				else 
				{   
                    
                    
					[m_mainDictionary setValue:nil forKey:@"MineBoughtItArray"];
					
				}
				
				NSArray *tmp_MineBoughItarray=[m_mainDictionary valueForKey:@"MineBoughtItArray"];
				int tmp_MineBoughtItcount;
				if ([tmp_MineBoughItarray count]<4) 
				{
					tmp_MineBoughtItcount=[tmp_MineBoughItarray count];
				}
				else
				{
					tmp_MineBoughtItcount=4;
				}
				if (TotalRecordsMineBoughtIt > DEFAULT_NUMBEROF_ITEMS_IN_ROW) 
				{
					[(UIButton*)m_mineView viewWithTag:202].hidden=NO;
				}
				else 
				{
					[(UIButton*)m_mineView viewWithTag:202].hidden=YES;
				}
				if (tmp_MineBoughItarray>0) {//**
                    [m_mineView setHidden:NO];
                }
				[self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_MineBoughtItcount type:kIBoughtIt];
                
                if ([m_UserId length] <= 0) {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                else {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
				
				break;
				
			case 2:
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];			
				m_buyItArray=[tempString JSONValue];
				int TotalRecordsFriendsBoughtIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsFriendsBoughtIt;

				
				NSArray *tmp_fittingRoomSummaryBoughtIt1=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if (m_messageIDsArrayBoughtIt) { // remove the previous objects in the aray.
					[m_messageIDsArrayBoughtIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummaryBoughtIt1 count]; i++) // Fill the message ids for all the records in an array.
				{
					NSInteger MessageID=[[[tmp_fittingRoomSummaryBoughtIt1 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBoughtIt addObject:[NSNumber numberWithInt:MessageID]];
				}				
				
				
				if ([m_buyItArray count]>0) 
				{
					
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"] forKey:@"FriendsBoughtItArray"];
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FriendsBoughtItArray_TotalRecords"];	
				}
				else 
				{
                    
					[m_mainDictionary setValue:nil forKey:@"FriendsBoughtItArray"];
					
				}
				NSArray *tmp_FriendsBoughtItarray=[m_mainDictionary valueForKey:@"FriendsBoughtItArray"];
				int tmp_FriendsBoughtItCount;
				if ([tmp_FriendsBoughtItarray count]<4) {
					tmp_FriendsBoughtItCount=[tmp_FriendsBoughtItarray count];
					
				}
				else{
					tmp_FriendsBoughtItCount=4;
				}
				if (TotalRecordsFriendsBoughtIt > DEFAULT_NUMBEROF_ITEMS_IN_ROW) {
					[(UIButton*)m_mineView viewWithTag:202].hidden=NO;
					
				}
				else{
					[(UIButton*)m_mineView viewWithTag:202].hidden=YES;
				}
                if (tmp_FriendsBoughtItCount>0) {//**
                    [m_mineView setHidden:NO];
                }
				[self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_FriendsBoughtItCount type:kIBoughtIt];
				
				//[l_FittingRoomIndicatorView stopLoadingView];
                
                if ([m_UserId length] <= 0) {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                else {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                
				break;
				
				
			case 3:
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];			
				m_buyItArray=[tempString JSONValue];
				int TotalRecordsFollowingBoughtIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsFollowingBoughtIt;

				
				NSArray *tmp_fittingRoomSummaryBoughtIt2=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if (m_messageIDsArrayBoughtIt) { // remove the previous objects in the aray.
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummaryBoughtIt2 count]; i++) // Fill the message ids for all the records in an array.
				{
					NSInteger MessageID=[[[tmp_fittingRoomSummaryBoughtIt2 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBoughtIt addObject:[NSNumber numberWithInt:MessageID]];
				}			
				
				if ([m_buyItArray count]>0)
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"] forKey:@"FollowingBoughtItArray"];
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FollowingBoughtItArray_TotalRecords"];
				}
				else 
				{   
                    
                    
					[m_mainDictionary setValue:nil forKey:@"FollowingBoughtItArray"];
					
				}
				
				x=10,y=179,height=70,width=70;
				NSArray *tmp_FollowingBoughtItArray=[m_mainDictionary valueForKey:@"FollowingBoughtItArray"];
				int tmp_FollowingBoughtItCount;
				if ([tmp_FollowingBoughtItArray count]<4) {
					tmp_FollowingBoughtItCount=[tmp_FollowingBoughtItArray count];
					
				}
				else
				{
					tmp_FollowingBoughtItCount=4;
					
				}
				if (TotalRecordsFollowingBoughtIt > DEFAULT_NUMBEROF_ITEMS_IN_ROW) {
					[(UIButton*)m_mineView viewWithTag:202].hidden=NO;
				}
				else {
					[(UIButton*)m_mineView viewWithTag:202].hidden=YES;
				}
				
				[self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_FollowingBoughtItCount type:kIBoughtIt];
				//[l_FittingRoomIndicatorView stopLoadingView];
                
                if ([m_UserId length] <= 0) {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                else {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
				
				break;
			case 4:
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];			
				m_buyItArray=[tempString JSONValue];
				int TotalRecordsNearbyBoughtIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsNearbyBoughtIt;

				
				NSArray *tmp_fittingRoomSummaryBoughtIt3=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if (m_messageIDsArrayBoughtIt) { // remove the previous objects in the aray.
					[m_messageIDsArrayBoughtIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummaryBoughtIt3 count]; i++) // Fill the message ids for all the records in an array.
				{
					NSInteger MessageID=[[[tmp_fittingRoomSummaryBoughtIt3 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBoughtIt addObject:[NSNumber numberWithInt:MessageID]];
				}
				
				if ([m_buyItArray count]>0) 
				{
					
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"] forKey:@"NearByBoughtItArray"];
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"NearByBoughtItArray_TotalRecords"];
				}
				else
				{   
                    
					[m_mainDictionary setValue:nil forKey:@"NearByBoughtItArray"];
				}
				
				x=10,y=179,height=70,width=70;
				NSArray *tmp_NearByBoughtItArray=[m_mainDictionary valueForKey:@"NearByBoughtItArray"];
				int tmp_NearByBoughtItCount;
				if ([tmp_NearByBoughtItArray count]<4) 
				{
					tmp_NearByBoughtItCount=[tmp_NearByBoughtItArray count];
					
				}
				else
				{
					tmp_NearByBoughtItCount=4;
					
				}
				
				if (TotalRecordsNearbyBoughtIt > DEFAULT_NUMBEROF_ITEMS_IN_ROW) 
				{
					[(UIButton*)m_mineView viewWithTag:202].hidden=NO;
				}
				else
				{
					[(UIButton*)m_mineView viewWithTag:202].hidden=YES;
				}
				[self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_NearByBoughtItCount type:kIBoughtIt];
				//[l_FittingRoomIndicatorView stopLoadingView];
                
                if ([m_UserId length] <= 0) {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                else {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                
				break;
				
			case 5:
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];			
				m_buyItArray=[tempString JSONValue];
				int TotalRecordsPopularBoughtIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsPopularBoughtIt;

				
				NSArray *tmp_fittingRoomSummaryBoughtIt4=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if (m_messageIDsArrayBoughtIt) { // remove the previous objects in the aray.
					[m_messageIDsArrayBoughtIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummaryBoughtIt4 count]; i++) // Fill the message ids for all the records in an array.
				{
					NSInteger MessageID=[[[tmp_fittingRoomSummaryBoughtIt4 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBoughtIt addObject:[NSNumber numberWithInt:MessageID]];
				}			
				
				
				
				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"] forKey:@"PopularBoughtItArray"];
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularBoughtItArray_TotalRecords"];
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"PopularBoughtItArray"];
				}
				
				NSArray *tmp_PopularBoughtItArray=[m_mainDictionary valueForKey:@"PopularBoughtItArray"];
				int tmp_PopularBoughtItCount;
				if ([tmp_PopularBoughtItArray count]<10) 
				{
					tmp_PopularBoughtItCount=[tmp_PopularBoughtItArray count];
					
				}
				else
				{
					tmp_PopularBoughtItCount=10;
					
				}
				if (TotalRecordsPopularBoughtIt>10) 
				{
					[(UIButton*)m_mineView viewWithTag:202].hidden=NO;
				}
				else
				{
					[(UIButton*)m_mineView viewWithTag:202].hidden=YES;
				}
				[self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_PopularBoughtItCount type:kIBoughtIt];
				
				//[l_FittingRoomIndicatorView stopLoadingView];
                
                if ([m_UserId length] <= 0) {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                else {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
				
				
				break;
			default:
				break;
		}
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	
	if (tempString) 
	{
		[tempString release];
		tempString=nil;
	}
    
}

-(void)requestCallBackMethodForBadgetNumber:(NSNumber *)responseCode responseData:(NSData *)responseData {
    
	// NSLog(@"data downloaded");
	// NSLog(@"response code is%d",[responseCode intValue]);
	NSString * tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	// NSLog(@"response string: %@",tempString);
	
	if ([responseCode intValue] == 200) {
        NSArray * tmpArray = [[tempString JSONValue]retain];
		NSLog(@"the array is %@",tmpArray);
        [tempString release];
		l_appDelegate.userInfoDic = [tmpArray objectAtIndex:0];
        
		NSString * notificationsCount = [NSString stringWithFormat:@"%@", [tmpArray objectAtIndex:0]];
        NSLog(@"the array is %@",[tmpArray objectAtIndex:0]);
		
        if (tmpArray) {
            [notificationButton setBadge:notificationsCount];
            //self.tabBarController.view.hidden=YES;
        }
        
        
        
        
        switch (tagValue) {
                
            case 1:
                
                [m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
                break;
                
            case 2:
                
                [m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
                break;
                
            case 3:
                
                [m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
                break;
                
            case 4:
                
                [m_requestObj getUnreadRequestsForNearby:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
                break;
                
            case 5:
                
                [m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
                break;
                
        }
        
        
	}
	else if ([responseCode intValue] == 401) {  // In case the session expires we have to make the user login again.
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState = FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if ([responseCode intValue] != -1) {
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
	}
	
}

-(void)requestCallBackMethodForCheckThisOut:(NSNumber *)responseCode responseData:(NSData *)responseData {
    
	// NSLog(@"Data Downloaded");
	// NSLog(@"Response code is: %d",[responseCode intValue]);
	// NSLog(@"Response String: %@",tempString);
	
    // Response Code is OK //
    
	if ([responseCode intValue] == 200) {
        
        NSString *tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];	
        
        if (m_messageIDsArrayCheckThisOut) {
            [m_messageIDsArrayCheckThisOut removeAllObjects];
        }

        m_CheckThisOutArray = [tempString JSONValue];
        [tempString release];
        int TotalRecordsCheckThisOut = [[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
		// Bharat: 11/28/11: US121: Save record count in instance variable
		totalRecordCountForActiveTag += TotalRecordsCheckThisOut;

        
        NSArray * tmp_fittingRoomSummaryList = [[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
        
        for (int i = 0; i < [tmp_fittingRoomSummaryList count]; i++) {
            NSInteger MessageID = [[[tmp_fittingRoomSummaryList objectAtIndex:i] valueForKey:@"messageId"] intValue];
            [m_messageIDsArrayCheckThisOut addObject:[NSNumber numberWithInt:MessageID]];
        }
        
        int tmp_CheckThisOutCount;
        
        switch (tagValue) {
    
            case 1:				
				
				if ([m_CheckThisOutArray count] > 0) {
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"MineCheckThisOutArray"];
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"MineCheckThisOutArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"MineCheckThisOutArray"];
				}
				
				NSArray * tmp_MineCheckThisOutArray = [m_mainDictionary valueForKey:@"MineCheckThisOutArray"];
				
				if ([tmp_MineCheckThisOutArray count] < 4) {
					tmp_CheckThisOutCount = [tmp_MineCheckThisOutArray count];
				}
				else {
					tmp_CheckThisOutCount = 4;
				}
                
                // Request to fill How Does This Look? section //
                //[l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
				//[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:4 pageno:1];
            	[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 2:				
				
				if ([m_CheckThisOutArray count] > 0) {
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FriendsCheckThisOutArray"];
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FriendsCheckThisOutArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FriendsCheckThisOutArray"];
				}
				
				NSArray * tmp_FriendsCheckThisOutArray = [m_mainDictionary valueForKey:@"FriendsCheckThisOutArray"];
				
				if ([tmp_FriendsCheckThisOutArray count] < 4) {
					tmp_CheckThisOutCount = [tmp_FriendsCheckThisOutArray count];
				}
				else {
					tmp_CheckThisOutCount = 4;
				}
                
                // Request to fill How Does This Look? section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
                //[m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:4 pageno:1];
				[m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 3:				
				
				if ([m_CheckThisOutArray count] > 0) {
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FollowingCheckThisOutArray"];
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FollowingCheckThisOutArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FollowingCheckThisOutArray"];
				}
				
				NSArray * tmp_FollowingCheckThisOutArray = [m_mainDictionary valueForKey:@"FollowingCheckThisOutArray"];
				
				if ([tmp_FollowingCheckThisOutArray count] < 4) {
					tmp_CheckThisOutCount = [tmp_FollowingCheckThisOutArray count];
				}
				else {
					tmp_CheckThisOutCount = 4;
				}
                
                // Request to fill How Does This Look? section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
                //[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:4 pageno:1];
				[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 4:				
				
				if ([m_CheckThisOutArray count] > 0) {
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"NearByCheckThisOutArray"];
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"NearByCheckThisOutArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"NearByCheckThisOutArray"];
				}
				
				NSArray * tmp_NearByCheckThisOutArray = [m_mainDictionary valueForKey:@"NearByCheckThisOutArray"];
				
				if ([tmp_NearByCheckThisOutArray count] < 4) {
					tmp_CheckThisOutCount = [tmp_NearByCheckThisOutArray count];
				}
				else {
					tmp_CheckThisOutCount = 4;
				}
                
                // Request to fill How Does This Look? section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
                //[m_requestObj getUnreadRequestsForNearby:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:4 pageno:1];
				[m_requestObj getUnreadRequestsForNearby:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 5:
                
                if ([m_CheckThisOutArray count] > 0) {
                    [m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"]forKey:@"PopularCheckThisOutArray"];
                    [m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularCheckThisOutArray_TotalRecords"];
                }
                else {
                    [m_mainDictionary setValue:nil forKey:@"PopularCheckThisOutArray"];
                }
                
                NSArray * tmp_PopularCheckThisOutArray = [m_mainDictionary valueForKey:@"PopularCheckThisOutArray"];
                
                if ([tmp_PopularCheckThisOutArray count] < 10) {
                    tmp_CheckThisOutCount = [tmp_PopularCheckThisOutArray count];
                }
                else {
                    tmp_CheckThisOutCount = 10;
                }
                
                // Request to fill How Does This Look? section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
                //[m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:4 pageno:1];
				[m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
                
                break;
        }
        
        if (tmp_CheckThisOutCount > 0) {
            [m_mineView setHidden:NO];
        }
        
        [self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_CheckThisOutCount type:kCheckThisOut];
        
        if (TotalRecordsCheckThisOut > DEFAULT_NUMBEROF_ITEMS_IN_ROW) {
            [(UIButton*)m_mineView viewWithTag:302].hidden = NO;
        }
        else {
            [(UIButton*)m_mineView viewWithTag:302].hidden = YES;
        }
        
	}
    
    /* The session expires so the user have to login again */
    
	else if ([responseCode intValue] == 401) {
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState = FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
    
    /* Network Connection error */
     
	else if ([responseCode intValue] != -1) {
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
	}
	
}

-(void)requestCallBackMethodForHowDoesThisLook:(NSNumber *)responseCode responseData:(NSData *)responseData {
    
    // NSLog(@"Data Downloaded");
	// NSLog(@"Response code is: %d",[responseCode intValue]);
	// NSLog(@"Response String: %@",tempString);
	
    // Response Code is OK //
    
	if ([responseCode intValue] == 200) {
        
        NSString *tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];	
        
        if (m_messageIDsArrayHowDoesThisLook) {
            [m_messageIDsArrayHowDoesThisLook removeAllObjects];
        }
        
        m_HowDoesThisLookArray = [tempString JSONValue];
        
        [tempString release];
        
        int TotalRecordsHowDoesThisLook = [[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
		// Bharat: 11/28/11: US121: Save record count in instance variable
		totalRecordCountForActiveTag += TotalRecordsHowDoesThisLook;

        
        NSArray * tmp_fittingRoomSummaryList = [[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
        
        for (int i = 0; i < [tmp_fittingRoomSummaryList count]; i++) {
            NSInteger MessageID = [[[tmp_fittingRoomSummaryList objectAtIndex:i] valueForKey:@"messageId"] intValue];
            [m_messageIDsArrayHowDoesThisLook addObject:[NSNumber numberWithInt:MessageID]];
        }
        
        int tmp_HowDoesThisLookCount;
        
        switch (tagValue) {
                
            case 1:				
				
				if ([m_HowDoesThisLookArray count] > 0) {
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"MineHowDoesThisLookArray"];
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"MineHowDoesThisLookArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"MineHowDoesThisLookArray"];
				}
				
				NSArray * tmp_MineHowDoesThisLookArray = [m_mainDictionary valueForKey:@"MineHowDoesThisLookArray"];
				
				if ([tmp_MineHowDoesThisLookArray count] < 4) {
					tmp_HowDoesThisLookCount = [tmp_MineHowDoesThisLookArray count];
				}
				else {
					tmp_HowDoesThisLookCount = 4;
				}
                
                // Request to fill Found a Sale section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
                //[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:4 pageno:1];
				[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 2:				
				
				if ([m_HowDoesThisLookArray count] > 0) {
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FriendsHowDoesThisLookArray"];
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FriendsHowDoesThisLookArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FriendsHowDoesThisLookArray"];
				}
				
				NSArray * tmp_FriendsHowDoesThisLookArray = [m_mainDictionary valueForKey:@"FriendsHowDoesThisLookArray"];
				
				if ([tmp_FriendsHowDoesThisLookArray count] < 4) {
					tmp_HowDoesThisLookCount = [tmp_FriendsHowDoesThisLookArray count];
				}
				else {
					tmp_HowDoesThisLookCount = 4;
				}
                
                // Request to fill Found a Sale section //
                //[l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
                //[m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:4 pageno:1];
				[m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 3:				
				
				if ([m_HowDoesThisLookArray count] > 0) {
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FollowingHowDoesThisLookArray"];
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FollowingHowDoesThisLookArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FollowingHowDoesThisLookArray"];
				}
				
				NSArray * tmp_FollowingHowDoesThisLookArray = [m_mainDictionary valueForKey:@"FollowingHowDoesThisLookArray"];
				
				if ([tmp_FollowingHowDoesThisLookArray count] < 4) {
					tmp_HowDoesThisLookCount = [tmp_FollowingHowDoesThisLookArray count];
				}
				else {
					tmp_HowDoesThisLookCount = 4;
				}
                
                // Request to fill Found a Sale section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
                //[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:4 pageno:1];
				[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 4:				
				
				if ([m_HowDoesThisLookArray count] > 0) {
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"NearByHowDoesThisLookArray"];
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"NearByHowDoesThisLookArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"NearByHowDoesThisLookArray"];
				}
				
				NSArray * tmp_NearByHowDoesThisLookArray = [m_mainDictionary valueForKey:@"NearByHowDoesThisLookArray"];
				
				if ([tmp_NearByHowDoesThisLookArray count] < 4) {
					tmp_HowDoesThisLookCount = [tmp_NearByHowDoesThisLookArray count];
				}
				else {
					tmp_HowDoesThisLookCount = 4;
				}
                
                // Request to fill Found a Sale section //
                //[l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
                //[m_requestObj getUnreadRequestsForNearby:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:4 pageno:1];
				[m_requestObj getUnreadRequestsForNearby:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 5:
                
                if ([m_HowDoesThisLookArray count] > 0) {
                    [m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"]forKey:@"PopularHowDoesThisLookArray"];
                    [m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularHowDoesThisLookArray_TotalRecords"];
                }
                else {
                    [m_mainDictionary setValue:nil forKey:@"PopularHowDoesThisLookArray"];
                }
                
                NSArray * tmp_PopularHowDoesThisLookArray = [m_mainDictionary valueForKey:@"PopularHowDoesThisLookArray"];
                
                if ([tmp_PopularHowDoesThisLookArray count] < 10) {
                    tmp_HowDoesThisLookCount = [tmp_PopularHowDoesThisLookArray count];
                }
                else {
                    tmp_HowDoesThisLookCount = 10;
                }
                
                // Request to fill Found a Sale section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
                //[m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:4 pageno:1];
				[m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
                
                break;
        }
        
        if (tmp_HowDoesThisLookCount > 0) {
            [m_mineView setHidden:NO];
        }
		
        [self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_HowDoesThisLookCount type:kHowDoesThisLook];
        
        if (TotalRecordsHowDoesThisLook > DEFAULT_NUMBEROF_ITEMS_IN_ROW) {
            [(UIButton*)m_mineView viewWithTag:402].hidden = NO;
        }
        else {
            [(UIButton*)m_mineView viewWithTag:402].hidden = YES;
        }
        
	}
    
    /* The session expires so the user have to login again */
    
	else if ([responseCode intValue] == 401) {
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState = FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
    
    /* Network Connection error */
    
	else if ([responseCode intValue] != -1) {
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
	}
    
}

-(void)requestCallBackMethodForFoundASale:(NSNumber *)responseCode responseData:(NSData *)responseData {
    
    // NSLog(@"Data Downloaded");
	// NSLog(@"Response code is: %d",[responseCode intValue]);
	// NSLog(@"Response String: %@",tempString);
	
    // Response Code is OK //
    
	if ([responseCode intValue] == 200) {
        NSString * tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
       
        if (m_messageIDsArrayFoundASale) {
            [m_messageIDsArrayFoundASale removeAllObjects];
        }
        
        NSLog(@"requestCallBackMethodForFoundASale - %@", tempString);
        
        if (tempString)
        {
            [tempString release];
            tempString=nil;
        }

        m_FoundASaleArray = [tempString JSONValue];
        
        int TotalRecordsFoundASale = [[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
		// Bharat: 11/28/11: US121: Save record count in instance variable
		totalRecordCountForActiveTag += TotalRecordsFoundASale;

        
        NSArray * tmp_fittingRoomSummaryList = [[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
        
        for (int i = 0; i < [tmp_fittingRoomSummaryList count]; i++) {
            NSInteger MessageID = [[[tmp_fittingRoomSummaryList objectAtIndex:i] valueForKey:@"messageId"] intValue];
            [m_messageIDsArrayFoundASale addObject:[NSNumber numberWithInt:MessageID]];
        }
        
        int tmp_FoundASaleCount;
        
        switch (tagValue) {
                
            case 1:				
				
				if ([m_FoundASaleArray count] > 0) {
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"MineFoundASaleArray"];
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"MineFoundASaleArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"MineFoundASaleArray"];
				}
				
				NSArray * tmp_MineFoundASaleArray = [m_mainDictionary valueForKey:@"MineFoundASaleArray"];
				
				if ([tmp_MineFoundASaleArray count] < 4) {
					tmp_FoundASaleCount = [tmp_MineFoundASaleArray count];
				}
				else {
					tmp_FoundASaleCount = 4;
				}
				
                // Request to fill Found a Sale section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
				[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForCheckThisOut:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 2:				
				
				if ([m_FoundASaleArray count] > 0) {
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FriendsFoundASaleArray"];
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FriendsFoundASaleArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FriendsFoundASaleArray"];
				}
				
				NSArray * tmp_FriendsFoundASaleArray = [m_mainDictionary valueForKey:@"FriendsFoundASaleArray"];
				
				if ([tmp_FriendsFoundASaleArray count] < 4) {
					tmp_FoundASaleCount = [tmp_FriendsFoundASaleArray count];
				}
				else {
					tmp_FoundASaleCount = 4;
				}
                
                // Request to fill Found a Sale section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
				[m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForCheckThisOut:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 3:				
				
				if ([m_FoundASaleArray count] > 0) {
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FollowingFoundASaleArray"];
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FollowingFoundASaleArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FollowingFoundASaleArray"];
				}
				
				NSArray * tmp_FollowingFoundASaleArray = [m_mainDictionary valueForKey:@"FollowingFoundASaleArray"];
				
				if ([tmp_FollowingFoundASaleArray count] < 4) {
					tmp_FoundASaleCount = [tmp_FollowingFoundASaleArray count];
				}
				else {
					tmp_FoundASaleCount = 4;
				}
				
                // Request to fill Found a Sale section //
                //[l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
				[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForCheckThisOut:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 4:				
				
				if ([m_FoundASaleArray count] > 0) {
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"NearByFoundASaleArray"];
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"NearByFoundASaleArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"NearByFoundASaleArray"];
				}
				
				NSArray * tmp_NearByFoundASaleArray = [m_mainDictionary valueForKey:@"NearByFoundASaleArray"];
				
				if ([tmp_NearByFoundASaleArray count] < 4) {
					tmp_FoundASaleCount = [tmp_NearByFoundASaleArray count];
				}
				else {
					tmp_FoundASaleCount = 4;
				}
				
                // Request to fill Found a Sale section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
				[m_requestObj getUnreadRequestsForNearby:@selector(requestCallBackMethodForCheckThisOut:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
				break;
                
            case 5:
                
                if ([m_FoundASaleArray count] > 0) {
                    [m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"]forKey:@"PopularFoundASaleArray"];
                    [m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularFoundASaleArray_TotalRecords"];
                }
                else {
                    [m_mainDictionary setValue:nil forKey:@"PopularFoundASaleArray"];
                }
                
                NSArray * tmp_PopularFoundASaleArray = [m_mainDictionary valueForKey:@"PopularFoundASaleArray"];
                
                if ([tmp_PopularFoundASaleArray count] < 10) {
                    tmp_FoundASaleCount = [tmp_PopularFoundASaleArray count];
                }
                else {
                    tmp_FoundASaleCount = 10;
                }
                
                // Request to fill Found a Sale section //
               // [l_FittingRoomIndicatorView startLoadingView:self type:1];

				//Bharat : 11/25/11: Chinging the order of icon download
				[m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForCheckThisOut:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:DEFAULT_NUMBEROF_ITEMS_IN_ROW pageno:1];
				
                break;
        }
        
        if (tmp_FoundASaleCount > 0) {
            [m_mineView setHidden:NO];
        }
        
        [self addButtonsOnTheScreen:m_mineView NumberOfButtons:tmp_FoundASaleCount type:kFoundASale];
        
        if (TotalRecordsFoundASale > DEFAULT_NUMBEROF_ITEMS_IN_ROW) {
            [(UIButton*)m_mineView viewWithTag:502].hidden = NO;
        }
        else {
            [(UIButton*)m_mineView viewWithTag:502].hidden = YES;
        }
        
	}
    
    /* The session expires so the user have to login again */
    
	else if ([responseCode intValue] == 401) {
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState = FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
    
    /* Network Connection error */
    
	else if ([responseCode intValue] != -1) {
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
	}
    
}

-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData {		
	NSLog(@"data downloaded");
	[l_FittingRoomIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) 
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
		        
		NSDictionary *tmp_dict=[[tmp_array objectAtIndex:0] valueForKey:@"wsMessage"] ;
		m_type=[tmp_dict valueForKey:@"messageType"];
		l_ViewRequestObj.m_Type=m_type;							// In case of mine, show the i bought it only.
		if (MineCheck==1) 
		{
			l_ViewRequestObj.MineTrack=YES;
			l_boughtItRequest.MineTrack=YES;
			
		}
		else {
			l_ViewRequestObj.MineTrack=NO;
			l_boughtItRequest.MineTrack=NO;
		}
		
		NSLog(@"response string: %@",tempString);
		if ([m_type caseInsensitiveCompare:kBuyItOrNot] == NSOrderedSame) {
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArrayBuyIt;
			[self btnPicturesAction:nil];
		}
		else if([m_type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
			l_boughtItRequest.m_mainArray = tmp_array;
			l_boughtItRequest.m_messageIDsBoughtItArray = m_messageIDsArrayBoughtIt;
			[self btnBoughtItPicturesAction:nil];
			
		}
        else if([m_type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArrayCheckThisOut;
			[self btnPicturesAction:nil];
			
		}
        else if([m_type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArrayHowDoesThisLook;
			[self btnPicturesAction:nil];
			
		}
        else if([m_type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArrayFoundASale;
			[self btnPicturesAction:nil];
			
		}
		if (tempString)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kSessionTimeOutTitle message:kSessionTimeOutMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	
	
}


//JMC & Hockey App Integration

- (IBAction)triggerFeedback {
    [self presentModalViewController:[[JMC sharedInstance] viewController]
                            animated:YES];
}

// GDL: Changed everything below.

#pragma mark - Memory management
#pragma mark

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlsts.
    self.m_scrollView = nil;
    //self.m_lableUseCase = nil;
    self.m_mineView = nil;
    self.m_MineBtn = nil;
    self.m_FriendsBtn = nil;
    self.m_FollowingBtn = nil;
    self.m_NearbyBtn = nil;
    self.m_PopularBtn = nil;
}

- (void)dealloc {
    [m_scrollView release];
    [m_scrollView2 release];
    //[m_lableUseCase release];
    [m_mineView release];
    [m_View release];
    [m_indicatorView release];
    [m_buyItArray release];
    [m_mainDictionary release];
    [m_UserId release];
    [m_requestObj release];
    [m_MineBtn release];
    [m_FriendsBtn release];
    [m_FollowingBtn release];
    [m_NearbyBtn release];
    [m_PopularBtn release];
    [m_type release];
    [notificationButton release];//**
    [m_exceptionPage release];//**
    [m_exceptionButton release];//**

    [super dealloc];
}

-(IBAction)exceptionButtonAction {
	QNavigatorAppDelegate * appDg = (QNavigatorAppDelegate *) [[UIApplication sharedApplication] delegate];
	if ((appDg != nil) && (appDg.tabBarController != nil)) {
		if (tagValue == 1) {
			appDg.tabBarController.selectedIndex = SHARE_TAB_INDEX;
		} else if (tagValue == 2) {
			appDg.tabBarController.selectedIndex = FIND_FRIENDS_TAB_INDEX;
		}
	}
}

@end
