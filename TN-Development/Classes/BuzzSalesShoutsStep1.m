//
//  BuzzSalesShoutsStep1.m
//  QNavigator
//
//  Created by softprodigy on 03/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BuzzSalesShoutsStep1.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import"QNavigatorAppDelegate.h"
# define SectionHeaderHeight 25


@implementation BuzzSalesShoutsStep1

@synthesize m_btnStepsView;
@synthesize btnStep1;
@synthesize btnStep2;
@synthesize btnStep3;
@synthesize lbl_CountText;
@synthesize m_textView;
@synthesize m_searchBar;
@synthesize m_tableView;
@synthesize tempArray;
@synthesize m_enterNewStoreView;
@synthesize m_BtnAddNewStore;
@synthesize m_storeNameTextField;
@synthesize m_AddressTextfield;
@synthesize m_CityTextfield;
@synthesize m_StateTextfield;
@synthesize m_ZipcodeTextfield;
@synthesize m_PhoneTextfield;



int countTextVal=140;
NSString *str_textView;
QNavigatorAppDelegate *l_appDelegate;
//NSArray  *tempArray;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	[m_enterNewStoreView.layer setCornerRadius:6.0f];	
	m_enterNewStoreView.hidden=YES;
	m_BtnAddNewStore.hidden=YES;
	[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_patterns.png"]]];
	tempArray = [[NSArray arrayWithObjects:@"one",@"two",@"three",@"four",@"five",nil]retain];
	
	NSLog(@"%@",tempArray);
	lbl_CountText.hidden=YES;
	m_textView.hidden=YES;
	m_tableView.hidden=YES;
	m_searchBar.hidden=YES;
	lbl_CountText.text=@"140";
	//m_imgPicker=[[UIImagePickerController alloc]init];
	[m_textView.layer setCornerRadius:6.0f];
	[m_btnStepsView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header_bg.jpg"]]];
	[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_patterns.png"]]];
	[self performSelector:@selector(btnSalesShoutStep1Action:)];

}

-(void)viewWillAppear:(BOOL)animated {
    // GDL: Added call to super.
	[super viewWillAppear:animated];
	[l_appDelegate.m_customView setHidden:YES];
}	

#pragma mark -
#pragma mark Custom methods
-(IBAction)btnStep1Action:(id)sender
{
	lbl_CountText.hidden=YES;
	m_textView.hidden=YES;
	[self performSelector:@selector(btnSalesShoutStep1Action:)];
}

- (void)textViewDidChange:(UITextView *)textView
{
	
	int max_count;
	if (countTextVal == 0) {
		NSLog(@"not editing\n");
		[m_textView setUserInteractionEnabled:FALSE];
	}
	str_textView = m_textView.text;
	[str_textView retain];
	max_count = countTextVal-[str_textView length];
	lbl_CountText.text=[NSString stringWithFormat:@"%d",max_count];
	NSLog(@"%d",countTextVal);
	NSLog(@"%@",lbl_CountText.text);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
		if (textField==m_storeNameTextField) 
		{
			//[m_storeNameTextField resignFirstResponder];
			[m_AddressTextfield becomeFirstResponder];
		}
		else if (textField==m_AddressTextfield)
		{
			[m_CityTextfield becomeFirstResponder];
		}
		else if (textField==m_CityTextfield)
		{
			[m_StateTextfield becomeFirstResponder];
			[self animateViewUpward];
		}
		else if(textField==m_StateTextfield)
		{
			[m_ZipcodeTextfield becomeFirstResponder];
		}
		else if(textField==m_ZipcodeTextfield)
		{
			[m_PhoneTextfield becomeFirstResponder];
		}
		else if(textField==m_PhoneTextfield)
		{
			[m_PhoneTextfield resignFirstResponder];
		}
		else
		{
			[textField resignFirstResponder];
			[self animateViewDownward];
		}
		return YES;
				
}



//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//	if ([string isEqualToString:@"\n"]) 
//	{	
//		if (textField==m_storeNameTextField) 
//		{
//			[m_AddressTextfield becomeFirstResponder];
//		}
//		else if (textField==m_AddressTextfield)
//		{
//			//[m_AddressTextfield resignFirstResponder];
//			[m_CityTextfield becomeFirstResponder];
//			
//		}
//		else if (textField==m_CityTextfield)
//		{
//			[m_StateTextfield becomeFirstResponder];
//			[self animateViewUpward];
//		}
//		else if(textField==m_StateTextfield)
//		{
//			//[self animateViewDownward];
//			[m_ZipcodeTextfield becomeFirstResponder];
//		}
//		else if(textField==m_ZipcodeTextfield){
//			[m_PhoneTextfield becomeFirstResponder];
//		}
//		else if(textField==m_PhoneTextfield)
//		{
//			[m_PhoneTextfield resignFirstResponder];
//		}
//		return TRUE;
//}
//return FALSE;
//}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	if (textField==m_CityTextfield) 
	{
		[self animateViewUpward];
	}
	else if(textField==m_StateTextfield)
	{
		[self animateViewUpward];
	}
	else if(textField==m_ZipcodeTextfield)
	{
		[self animateViewUpward];
	}
	else if(textField==m_PhoneTextfield)
	{
		[self animateViewUpward];
	}
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{	// return NO to not change text
	
	//[textView resignFirstResponder];
//	return YES;
	
	
	if([text isEqualToString:@"\n"]) {
			
			[m_textView resignFirstResponder];
			
			
			[btnStep1 setImage:[UIImage imageNamed:@"steppp1.png"] forState:UIControlStateNormal];
			[btnStep2 setImage:[UIImage imageNamed:@"steppp2.png"] forState:UIControlStateNormal];
			[btnStep3 setImage:[UIImage imageNamed:@"steppp3.png"] forState:UIControlStateNormal];
			
			m_tableView.hidden=NO;
			m_searchBar.hidden=NO;
			
			
		}
		return YES	;
		//if(m_textView.text.length>=140 && range.length==0)
//		{
//			return NO;
//		}
//		else
//		{
//			return YES;
//		}
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/



-(IBAction)btnSalesShoutStep1Action:(id)sender
{
	UIActionSheet *alertSheet=[[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" 
											destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose Existing Photo",@"Skip Photo",nil];
	[alertSheet showFromTabBar:self.tabBarController.tabBar];
	[alertSheet release];
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex==0)
	{
		if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
		{
			if (!m_imgPicker)
			{
				m_imgPicker=[[UIImagePickerController alloc]init];
			}
			m_imgPicker.delegate=self;
			m_imgPicker.sourceType=UIImagePickerControllerSourceTypeCamera;
			m_imgPicker.showsCameraControls=NO;
			//m_imgPicker.cameraOverlayView = temp_View;
			m_imgPicker.navigationBar.barStyle=UIBarStyleBlackOpaque;
			m_imgPicker.allowsEditing =NO;
			[self presentModalViewController:m_imgPicker animated:YES];
		}
		else 
		{
			//m_imgPicker.allowsEditing = NO;
			UIAlertView *AlertForCamera=[[UIAlertView alloc]initWithTitle:@"" message:@"Camera not available!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[AlertForCamera show];
			[AlertForCamera release];
		}
	}
	else if(buttonIndex==1)
	{
		UIImagePickerController *picker = [[UIImagePickerController alloc] init];
		picker.delegate = self;
		picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
		[self presentModalViewController:picker animated:YES];
		[picker release];
	}
	else if(buttonIndex==2) 
	{
		m_textView.hidden=NO;
		lbl_CountText.hidden=NO;

		[m_textView becomeFirstResponder];
		
		[btnStep1 setImage:[UIImage imageNamed:@"stepp1.png"] forState:UIControlStateNormal];
		[btnStep2 setImage:[UIImage imageNamed:@"stepp2.png"] forState:UIControlStateNormal];
		[btnStep3 setImage:[UIImage imageNamed:@"stepp3.png"] forState:UIControlStateNormal];
	}

}



- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{

	[self.navigationController popViewControllerAnimated:YES];

}
		
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section                              
{
	if(section==0)
	{
		return 5;    
	}
	else if(section==1) 
	{
		return 1;
	}
	else {
		return 0;
	}


}


	
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	
	//NSLog(@"%@",tempArray);
	NSString *SimpleTableIdentifier = [NSString stringWithFormat:@"Cell %@",indexPath];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: SimpleTableIdentifier];
	
	if (cell == nil) 
	{ 
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleTableIdentifier] autorelease];
		cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bluebox.png"]];	
		cell.selectionStyle=UITableViewCellSelectionStyleNone;
	}
	
	cell.textLabel.text=[tempArray objectAtIndex:indexPath.row];
	return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	if(indexPath.section==0)
	{
		UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"Is store name correct?" message:@"" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"Go Back",nil];
		[tempAlert show];
		[tempAlert release];
		tempAlert=nil;
	}
	else
	{
		m_searchBar.hidden=YES;
		m_tableView.hidden=YES;
		m_BtnAddNewStore.hidden=NO;
		m_enterNewStoreView.hidden=NO;
	//	m_textView.hidden
		//[m_textView becomeFirstResponder];
	}

}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
	//[self animateViewDownward];
	//[m_textView resignFirstResponder];
	return YES;
}

-(IBAction)BtnAddNewStoreAction:(id)sender
{

	UIAlertView *tmpAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"your message is successfully" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
	[tmpAlert show];
	[tmpAlert release];
	tmpAlert=nil;
	[self.navigationController popViewControllerAnimated:YES];

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  
{
	return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	if (section==0) {
		
		UIView *tmp_view;
		tmp_view = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, SectionHeaderHeight)];
		tmp_view.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		[tmp_view autorelease];
		
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,2, 350, 25)]  ;
		tmp_headerLabel.font=[UIFont fontWithName:kFontName size:16];
		tmp_headerLabel.text=@"a";
		tmp_headerLabel.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		tmp_headerLabel.textColor=[UIColor whiteColor];
		[tmp_view addSubview:tmp_headerLabel];
		[tmp_headerLabel release];
		tmp_headerLabel=nil;
		
		return tmp_view;
	}
	else 
	{
		
		UIView *tmp_headerForSecondSection;
		tmp_headerForSecondSection = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, SectionHeaderHeight)];
		tmp_headerForSecondSection.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		[tmp_headerForSecondSection autorelease];
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,2, 350, 25)] ;
		[tmp_headerLabel autorelease];
		tmp_headerLabel.font=[UIFont  fontWithName:kFontName size:16 ];
		tmp_headerLabel.text=@"Still not in the list!";
		tmp_headerLabel.textColor=[UIColor whiteColor];
		tmp_headerLabel.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		[tmp_headerForSecondSection addSubview:tmp_headerLabel];
		
		return tmp_headerForSecondSection;
		
		
	}
}



	
#pragma mark animaion methods

-(void)animateViewUpward
{
	[UIView beginAnimations: @"upView" context: nil];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDuration: 0.4];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	self.view.frame = CGRectMake(0,-100, 320,460);
	[UIView commitAnimations];
}

-(void)animateViewDownward
{
	[UIView beginAnimations: @"downView" context: nil];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDuration: 0.4];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	self.view.frame = CGRectMake(0,0, 320,440);
	[UIView commitAnimations];
}


#pragma mark -
#pragma mark alertView delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if(buttonIndex==0)
	{
		UIAlertView *tmpAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Your message sent successfully." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tmpAlert show];
		[tmpAlert release];
		tmpAlert=nil;
		[self.navigationController popViewControllerAnimated:YES];
	}
	
}


// GDL: Modified everything below

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release retained IBOutlets.
    self.m_storeNameTextField = nil;
    self.m_AddressTextfield = nil;
    self.m_CityTextfield = nil;
    self.m_StateTextfield = nil;
    self.m_ZipcodeTextfield = nil;
    self.m_PhoneTextfield = nil;
    
    self.m_BtnAddNewStore = nil;
    self.m_enterNewStoreView = nil;
    self.m_searchBar = nil;
    self.m_tableView = nil;
    self.lbl_CountText = nil;
    self.m_textView = nil;
    self.btnStep1 = nil;
    self.btnStep2 = nil;
    self.btnStep3 = nil;
    self.m_btnStepsView = nil;
}

- (void)dealloc {
    [m_imgPicker release];
    [m_btnStepsView release];
    [btnStep1 release];
    [btnStep2 release];
    [btnStep3 release];
    [m_textView release];
    [lbl_CountText release];
    [m_searchBar release];
    [m_tableView release];
    [tempArray release];
    [m_enterNewStoreView release];
    [m_BtnAddNewStore release];
    [m_storeNameTextField release];
    [m_AddressTextfield release];
    [m_CityTextfield release];
    [m_StateTextfield release];
    [m_ZipcodeTextfield release];
    [m_PhoneTextfield release];
    
    [super dealloc];
}

@end
