//
//  AlertMessageSettingViewController.m
//  QNavigator
//
//  Created by softprodigy  on 31/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AlertMessageSettingViewController.h"
#import"QNavigatorAppDelegate.h"
#import"ShopbeeAPIs.h"
#import"LoadingIndicatorView.h"
#import"Constants.h"
#import "NewsDataManager.h"

@implementation AlertMessageSettingViewController

QNavigatorAppDelegate *l_appDelegate;
ShopbeeAPIs *l_requestObj;
LoadingIndicatorView *l_alertMessageActivityIndicator;
@synthesize m_buyIt;

@synthesize m_boughtIt;


#pragma mark Custom methods
-(IBAction) m_goToBackView{
	
	[self.navigationController popViewControllerAnimated:YES];
}
-(IBAction) m_buyItAction{
	if (m_buyIt.on) {
		m_BuyItStatus=YES ;	
	}
	else {
		m_BuyItStatus=NO;	
	}
}
-(IBAction) m_boughtItAction{
	if (m_boughtIt.on) {
		m_BoughtItStatus=YES;
	}
	else {
		m_BoughtItStatus=NO;
	}

}
-(IBAction) m_btnDoneAction
{
	if (m_BuyItStatus==YES) {
		Buyit=@"Y";
	}
	else {
		Buyit=@"N";
	}
	if (m_BoughtItStatus==YES) {
		BoughtIt=@"Y";
	}
	else {
		BoughtIt=@"N";
	}
	[l_appDelegate.userInfoDic setValue:Buyit forKey:@"buyItStatus"];
	[l_appDelegate.userInfoDic setValue:BoughtIt forKey:@"boughtItStatus"];
	NSString *tmp_name=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	NSDictionary *tmp_dictionary=[[NSDictionary alloc]initWithObjectsAndKeys:Buyit,@"buyItStatus",BoughtIt,@"boughtItStatus",tmp_name,@"userId",nil];
	l_requestObj=[[ShopbeeAPIs alloc] init];
	[l_alertMessageActivityIndicator startLoadingView:self];
	[l_requestObj updateUserInfo:@selector(requestCallBackMethod:responseData:) tempTarget:self tmp_dictionary:tmp_dictionary];
	[l_requestObj release];
	l_requestObj=nil;
	
}

#pragma mark -
#pragma mark callBack methods
-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_alertMessageActivityIndicator stopLoadingView];
	NSLog(@"data downloaded");
	
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	if ([responseCode intValue]==200) 
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
		
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	[tempString release];
	tempString=nil;
	
}	
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
#pragma mark -
-(void)viewDidAppear:(BOOL)animated
{
}
-(void)viewDidLoad{
	
//	[m_buyIt setOn:YES];
//	[m_boughtIt setOn:YES];
//	m_BoughtItStatus=YES;
//	m_BuyItStatus=YES;
	l_alertMessageActivityIndicator=[LoadingIndicatorView SharedInstance];
	if ([[l_appDelegate.userInfoDic valueForKey:@"boughtItStatus"] isEqualToString:@""]) {
		[m_boughtIt setOn:YES];
	}
	if ([[l_appDelegate.userInfoDic valueForKey:@"buyItStatus"]isEqualToString:@""]) {
		[m_buyIt setOn:YES];
	}
	
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}
-(void)viewWillAppear:(BOOL)animated{
	
	if ([[l_appDelegate.userInfoDic valueForKey:@"buyItStatus"]isEqualToString:@"Y"]) {
		[m_buyIt setOn:YES];
		m_BuyItStatus=TRUE;
	}
	else if([[l_appDelegate.userInfoDic valueForKey:@"buyItStatus"]isEqualToString:@"N"]) {
		[m_buyIt setOn:NO];
		m_BuyItStatus=FALSE;
	}
	if ([[l_appDelegate.userInfoDic valueForKey:@"boughtItStatus"] isEqualToString:@"Y"]) {
		[m_boughtIt setOn:YES];
		m_BoughtItStatus=TRUE;
		
	}
	else if([[l_appDelegate.userInfoDic valueForKey:@"boughtItStatus"] isEqualToString:@"N"]) {
		[m_boughtIt setOn:NO];
		m_BoughtItStatus=FALSE;
	}
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];

    // Release retained IBOutlets.
    self.m_buyIt = nil;
    self.m_boughtIt = nil;
}

- (void)dealloc  {
	[m_buyIt release];
	[m_boughtIt release];
    [m_view release];
    [m_indicatorView release];
    [Buyit release];
    [BoughtIt release];

    [super dealloc];
}


@end
