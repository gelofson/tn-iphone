//
//  CategoriesViewController.h
//  QNavigator
//
//  Created by Soft Prodigy on 30/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTopBarView.h"
#include <CoreLocation/CoreLocation.h>


@interface CategoriesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate>
{
	UITableView					*m_tableView;
	UIView						*m_moreDetailView;
	UILabel						*m_lblCaption;
	NSString					*m_strCaption;
	UIActivityIndicatorView		*m_activityIndicator;
	NSMutableData				*m_mutCatResponseData;
	NSURLConnection				*m_theConnection;
	NSURLConnection				*m_mallMapConnection;
	BOOL						m_isConnectionActive;
	BOOL						m_isMallMapConnectionActive;
	int							m_pageNumber;
	int							m_intPrevCatIndex;
	
	CLLocationManager			*m_locationManager;
	UIScrollView				*m_scrollView;
	UINavigationController		*nav;
	int							m_intResponseCode;
	UIScrollView				*m_logosScrollView;
	UISearchBar					*m_searchBar;
	BOOL						isCurrentListingView;
	UIImageView					*m_exceptionPage;
    BOOL                        isEditing;
    // GDL. For reloading the category ads every 10 minutes.
    NSTimer *reloadTimer;
    NSArray *exceptionImageNamesArray;
}
@property (nonatomic,retain) NSArray	*exceptionImageNamesArray;
@property (nonatomic,retain) IBOutlet		UIImageView	*m_exceptionPage;
@property BOOL								isCurrentListingView;
@property (nonatomic,retain) IBOutlet		UISearchBar	 *m_searchBar;
@property (nonatomic,retain) IBOutlet		UIScrollView *m_logosScrollView;
@property (nonatomic,retain) IBOutlet		UINavigationController *nav;
@property (nonatomic,retain) IBOutlet		UITableView *m_tableView;
@property (nonatomic,retain) IBOutlet		UILabel *m_lblCaption;
@property (nonatomic,retain) IBOutlet		UIActivityIndicatorView *m_activityIndicator;

@property (nonatomic,retain) NSMutableData	*m_mutCatResponseData;
@property (nonatomic,retain) UIView			*m_moreDetailView;
@property (nonatomic,retain) NSString		*m_strCaption;

@property int m_intResponseCode;

@property (nonatomic,retain) NSURLConnection *m_theConnection;
@property (nonatomic,retain) NSURLConnection *m_mallMapConnection;
@property					 BOOL  			 m_isConnectionActive;
@property					 BOOL			m_isMallMapConnectionActive;
@property					 int			m_pageNumber;
@property					 int			m_intPrevCatIndex;
@property (nonatomic,retain) CLLocationManager *m_locationManager;

@property (nonatomic,retain) UIScrollView *m_scrollView;

-(void)sendRequestForCategoryData:(int)temp_page;
-(void)checkIfDataAvailable;
-(void)updateDBonOtherViewNavigation;
-(void)sendRequestToCheckMallMap:(NSString *)imageUrl;
-(void)addLogosButtonViews;
- (void)enableCancelButton:(UISearchBar *)aSearchBar;
@end
