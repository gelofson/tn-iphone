//
//  WhereBox.m
//  QNavigator
//
//  Created by softprodigy on 08/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "MGTwitterStatusesParser.h"
#import "MGTwitterXMLParser.h"
#import "WhereBox.h"
#import "QNavigatorAppDelegate.h"
#import "CustomTopBarView.h"
#import "CategoriesViewController.h"
#import "CategoryModelClass.h"
#import "AuditingClass.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"

#import "SBJSON.h"
#import "ATIconDownloader.h"
#import "DBManager.h"
#import "StreetMallMapViewController.h"
#import "TellAFriendViewController.h"
#import "SA_OAuthTwitterEngine.h" 
#import"SA_OAuthTwitterController.h"
#import "ShopbeeFriendsViewController.h"
#import "MGTwitterEngine.h"
#import "TwitterFriendsViewController.h"
#import "AddFriendsMypageViewController.h"
#import "TwitterProcessing.h"
#import "FittingRoomViewController.h"
#import "ShopbeeAPIs.h"
#import"LoadingIndicatorView.h"
#import "NewsDataManager.h"
#import "WhereBoxShareNow.h"

#define POP_UP_OFFSET_X 1
#define POP_UP_OFFSET_Y 45

@implementation WhereBox

@synthesize m_lblBrandName;
@synthesize popUpforShareInfo;
@synthesize tagg;
@synthesize btnBackObj;
@synthesize m_moreDetailView;
@synthesize m_tableView;
@synthesize m_theConnection;
@synthesize m_mallMapConnection;
@synthesize m_isConnectionActive;
@synthesize	m_isMallMapConnectionActive;
@synthesize m_activityIndicator;
@synthesize m_intResponseCode;

@synthesize m_shareView;
@synthesize m_scrollView;
@synthesize m_mutCatResponseData;
@synthesize twitterEngine, tw; 
@synthesize m_FittingRoomView;
@synthesize m_emailFrnds,m_facebookFrnds,m_twitterFrnds,m_shopBeeFrnds;
@synthesize m_btnSendBuyOrNot;
@synthesize m_btnSendIBoughtIt;
@synthesize m_sharing_lable;
@synthesize m_imgShareView;
@synthesize m_publicWishlist;
@synthesize m_privateWishlist;
@synthesize m_btnCancelWhereBox;
@synthesize m_privateOrPublic;
@synthesize m_BoughtItOrNot;
@synthesize m_productImage;
@synthesize m_DarkGreenBoxView;
@synthesize isFromIndyShopView;

CategoryModelClass *l_objCatModel;
QNavigatorAppDelegate *l_appDelegate;
CLLocation *l_cllocation;
TwitterFriendsViewController *Twitter_obj;
FittingRoomViewController *tempFittingRoom;
LoadingIndicatorView *l_whereboxIndicatorView;

NSString *temp_strCity;
NSString *temp_strState;
NSString *temp_strZip;
NSString *temp_strAdd2;



ShopbeeAPIs *l_request;
int counter;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewWillAppear:(BOOL)animated{
	//counter=0;
	[m_shareView setHidden:YES];
}

- (void)viewDidLoad 
{

	// Set font for objects defined in XIB here
	[m_lblBrandName setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_TITLEBAR_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_TITLEBAR_FONT_SIZE_KEY]]];

	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	
	//delete the category sales images saved in temproray directory on applcation termination
	//NSError **tempErr;
	NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
	//directoryContentsAtPath:tempPath];
	NSArray *onlyJPGs = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.jpg'"]];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	if (onlyJPGs) 
	{	
		for (int i = 0; i < [onlyJPGs count]; i++) {
			//NSLog(@"Directory Count: %i", [onlyJPGs count]);
			NSString *contentsOnly = [NSString stringWithFormat:@"%@/%@", documentsDirectory, [onlyJPGs objectAtIndex:i]];
			[fileManager removeItemAtPath:contentsOnly error:nil];
		}
	}
	//--------------
	
	counter=0;
	[super viewDidLoad];
	l_whereboxIndicatorView=[LoadingIndicatorView SharedInstance];
	[popUpforShareInfo.layer setCornerRadius:8.0f ];
	//popUpforShareInfo.hidden=YES;
	//m_DarkGreenBoxView=[[UIView alloc]init];
	[m_DarkGreenBoxView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"darkgreenbox.png"]]];
	//m_DarkGreenBoxView=[[UIView alloc]init];
	//m_DarkGreenBoxView.frame=CGRectMake(0, 0, 310, 277);
	m_mutCatResponseData=[[NSMutableData alloc] init];
	[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bgNewWhereBox.png"]]];
	tempFittingRoom=[[FittingRoomViewController alloc]init];
	[m_emailFrnds setHidden:YES];
	[m_facebookFrnds setHidden:YES];
	[m_twitterFrnds setHidden:YES];
	[m_shopBeeFrnds setHidden:YES];
	[m_btnSendBuyOrNot setHidden:YES];
	[m_btnSendIBoughtIt setHidden:YES];
	[m_sharing_lable setHidden:YES];
	
	QNavigatorAppDelegate *obj=(QNavigatorAppDelegate*)[[UIApplication sharedApplication]delegate];
	
	NSLog(@"getFollowers   :  %@",[_engine getFollowersIncludingCurrentStatus:YES]);
	
	 // to download the category images and save them in the documents directory
	if (isFromIndyShopView==YES) // in case the user is from indyshop view we start downloading the indyshop images 
	{
		if ([l_appDelegate.m_arrIndyShopData count]>0) 
		{
			NSString *tmp_urlString=[[l_appDelegate.m_arrIndyShopData objectAtIndex:counter] valueForKey:@"m_strImageUrl"];
			[self sendRequestToLoadImages:tmp_urlString withContinueToNext:YES];
			
		}
		
	}
	else // else download images for the categories  
	{
		if ([l_appDelegate.m_arrCategorySalesData count]>0) 
		{
			NSString *tmp_urlString=[[l_appDelegate.m_arrCategorySalesData objectAtIndex:counter] valueForKey:@"m_strImageUrl2"];
            //Bharat: 11/10/2011: Added check to make sure image2 URL is not empty string.
			if ((tmp_urlString == nil) || ([tmp_urlString length] < 1)) {
				NSLog(@"WhereBox:m_strImageUrl2 is nil (Line 184)");
				tmp_urlString = [[l_appDelegate.m_arrCategorySalesData objectAtIndex:counter] valueForKey:@"m_strImageUrl"];
			} else {
				NSLog(@"WhereBox:m_strImageUrl2=[%@] (Line 184)",tmp_urlString);
			}
			[self sendRequestToLoadImages:tmp_urlString withContinueToNext:YES];
			
		}
		
	}
	
				
	[self whereBtnPressed:tagg];
	
}

-(void)viewDidAppear:(BOOL)animated
{
	
}
-(void)viewWillDisappear:(BOOL)animated
{
	if (m_isConnectionActive)
	{
		[m_theConnection cancel];
		m_isConnectionActive=FALSE;
	}
	
	//remove the saved images to avoid inconsistency
		
	
}

#pragma mark -
#pragma mark Callback methods
-(void)requestCallBackMethodforWishList:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	//NSLog(@"Friend added. response code: %d, response string:%@",[responseCode intValue],[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
	
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	[l_whereboxIndicatorView stopLoadingView];
	
	
	if([responseCode intValue]==200)
	{
		
		UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Your message sent successfully." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]autorelease];
		[m_shareView setHidden:YES];
		[dataReloadAlert show];
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
	}
	else if([responseCode intValue]==500)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Advertisement Expired!" message:@"Advertisement doesn't exists." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else 
	{
		[l_whereboxIndicatorView stopLoadingView];
	}
	
	
}


-(void)requestCallBackMethodForSendBuyItOrNot:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	
	NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	[l_whereboxIndicatorView stopLoadingView];
	
	
	if([responseCode intValue]==200)
	{
		
		UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Your message sent successfully." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]autorelease];
		[dataReloadAlert show];
		[m_shareView setHidden:YES];
		
	}
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else 
	{
		[l_whereboxIndicatorView stopLoadingView];
	}
	
	
}



-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	NSLog(@"data downloaded");
	[l_whereboxIndicatorView stopLoadingView];
	
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	
	NSArray *tempArray=[[tempString JSONValue] retain];
	
	if ([responseCode intValue]==200) 
	{
		if ([tempArray count]>0) 
		{
			ShareSalesWithFriends *tmp_obj=[[ShareSalesWithFriends alloc]init];
			tmp_obj.m_catObjModel=l_objCatModel;
			tmp_obj.m_bigSalesModel=nil;
            NSMutableArray * arr = [[NSMutableArray alloc] initWithCapacity:3];
            [arr addObjectsFromArray:tempArray];
			tmp_obj.m_friendsArray=arr;
            [arr release];
			[self.navigationController pushViewController:tmp_obj animated:YES];	
			[tmp_obj release];
			tmp_obj=nil;
		}
		else 
		{
			UIAlertView *Tmp_alert=[[ UIAlertView alloc] initWithTitle:@"Oops!" message:@"No FashionGram friends found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[Tmp_alert show];
			[Tmp_alert release];
			Tmp_alert=nil;			
		}

	}
	
	else if([responseCode intValue]==401)  // In case the session expires we have to make the user login again.
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
		alert=nil;
	}
	else 
	{
		[l_whereboxIndicatorView stopLoadingView];
	}
	
	[tempString release];
	tempString=nil;
	
}



-(IBAction)btnCloseOnThreeButtonViewAction
{
	[m_shareView setHidden:YES];
}


-(IBAction)btnTellAFriendAction
{
	[self performSelector:@selector(showPicker)];
}


#pragma mark -
#pragma mark mail composer methods



-(void)showPicker
{
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self performSelector:@selector(displayComposerSheet)];	
		}
		else
		{
			[self performSelector:@selector(launchMailAppOnDevice)];
		}
	}
	else
	{
		[self performSelector:@selector(launchMailAppOnDevice)];
	}
}



// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	[picker setSubject:@"Share \"FashionGram\""];
	NSMutableArray *toRecipients=[[NSMutableArray alloc]init];

	
	NSMutableString *emailBody;//=[[NSMutableString alloc] init];
	emailBody=[NSMutableString stringWithFormat:@"%@",l_objCatModel.m_strBrandName];
	[emailBody appendFormat:@"\n"];
	[emailBody appendFormat:@"%@",l_objCatModel.m_strTitle];
	[emailBody appendFormat:@"\n"];
	[emailBody appendFormat:@"%@",l_objCatModel.m_strDiscount];
	[emailBody appendFormat:@"%@",l_objCatModel.m_strAddress];
	
	
	//	working
	//
	UIImage *logoPic = [UIImage imageNamed:@"logo_for_share.png"];
	NSData *imageD = UIImageJPEGRepresentation(logoPic, 1);
	[picker addAttachmentData:imageD mimeType:@"image/jpg" fileName:@"logo_for_share.png"];
	
	[picker setToRecipients:toRecipients];
	[toRecipients release];
	toRecipients=nil;
	
	[picker setMessageBody:emailBody isHTML:NO];
	
	[self presentModalViewController:picker animated:NO];
	
    [picker release];
	picker=nil;
	
}	


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	AuditingClass *temp_objAudit;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			[m_shareView setHidden:YES];
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			
			//------------------------------------------------------------
			
			temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Tell a Friend about FashionGram",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-2",@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
				
			}
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"your message sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert setTag:2];
			
			[alert show];
			[alert release];
			
			[m_shareView setHidden:YES];

			//------------------------------------------------------------
					
			
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	
	self.navigationItem.rightBarButtonItem.enabled=NO;
	[self dismissModalViewControllerAnimated:YES];
	
}

// Launches the Mail application on the device.
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=";
	NSString *body = @"&body=";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}


#pragma mark -
#pragma mark custom methods

-(IBAction)btnShareInfoAction:(id)sender
{
	if(![popUpforShareInfo superview])
	{
		[self.view	addSubview:popUpforShareInfo];
		[popUpforShareInfo setTransform:CGAffineTransformMakeTranslation(30,70)];
	}
	else
	{
		[popUpforShareInfo setHidden:NO];
		[self.view bringSubviewToFront:popUpforShareInfo];
	}
}

-(void)whereBtnPressed:(int)tag
{
	
	for(UIView *tempView in m_scrollView.subviews)
	{
		for (int i=1000; i<1015; i++) 
		{
			if ([[m_scrollView viewWithTag:i] superview]) {
				[[m_scrollView viewWithTag:i] removeFromSuperview];
				[[m_scrollView viewWithTag:i] release];
				//[m_scrollView viewWithTag:i=nil;
			}	
		}
		
	}
	for(UIView *tempView in popUpforShareInfo.subviews)
	{
		for (int i=1008; i<1015; i++) 
		{
			if ([[popUpforShareInfo viewWithTag:i] superview]) {
				[[popUpforShareInfo viewWithTag:i] removeFromSuperview];
				//[[popUpforShareInfo viewWithTag:i] release];
				//[m_scrollView viewWithTag:i=nil;
			}	
		}
		
	}
	if(l_appDelegate.m_intCurrentView==1)
	{	
		if ((tag >= 0) && (tag < [l_appDelegate.m_arrIndyShopData count])) {
			l_objCatModel= [l_appDelegate.m_arrIndyShopData objectAtIndex:tag];
			l_appDelegate.m_intIndyShopIndex=tag;
		} else {
			return;
		}
	}
	else
	{
		if ((tag >= 0) && (tag < [l_appDelegate.m_arrCategorySalesData count])) {
			l_objCatModel= [l_appDelegate.m_arrCategorySalesData objectAtIndex:tag];
			l_appDelegate.m_intCategorySalesIndex=tag;
		} else {
			return;
		}
	}
	//------------------------------------------------------------
	AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
	[temp_objAudit initializeMembers];
	NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
	[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
	
	if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
	{
		NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
								 @"Where button pressed",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",l_objCatModel.m_strId,@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
		[temp_objAudit.m_arrAuditData addObject:temp_dict];
	}
	
	if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
	{
		[temp_objAudit sendRequestToSubmitAuditData];
	}
	//------------------------------------------------------------
	
	[m_tableView setUserInteractionEnabled:FALSE];
	
	//UIImageView *tempDarkGreenBox=[[UIImageView alloc]initWithFrame:CGRectMake(5,5,312,277)];
//	[tempDarkGreenBox setImage:[UIImage imageNamed:@"darkgreenbox.png"]];
//	tempDarkGreenBox.contentMode=UIViewContentModeScaleAspectFit;
	
	
	//---------------------------scrollview------------------------------------
	m_scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(5,50,310,270)];
		
	m_scrollView.pagingEnabled = NO;
	m_scrollView.userInteractionEnabled = YES;
	m_scrollView.showsVerticalScrollIndicator = YES;
	m_scrollView.showsHorizontalScrollIndicator = NO;
	

	m_scrollView.scrollsToTop = NO;
	m_scrollView.contentOffset = CGPointMake(0,0);
	m_scrollView.backgroundColor = [UIColor clearColor];
	
	UISwipeGestureRecognizer *recogniser=[[UISwipeGestureRecognizer alloc] init]; // add a swipe gesture recogniser
	recogniser.direction=UISwipeGestureRecognizerDirectionLeft;
	[recogniser addTarget:self action:@selector(handleSwipeFrom:)];
	[m_scrollView addGestureRecognizer:recogniser];
	[recogniser release];
	recogniser=nil;
	
	UISwipeGestureRecognizer *recogniser1=[[UISwipeGestureRecognizer alloc] init];
	recogniser1.direction=UISwipeGestureRecognizerDirectionRight;
	[recogniser1 addTarget:self action:@selector(handleSwipeFrom:)];
	[m_scrollView addGestureRecognizer:recogniser1];
	[recogniser1 release];
	recogniser1=nil;
	
		
	//UIImageView *temp_imgView=[[UIImageView alloc]initWithFrame:CGRectMake(140,7,157,157)];
	m_ProductImageView=[[PhotosLoadingView alloc]initWithFrame:CGRectMake(140,7,157,157) ];
	
	
	m_ProductImageView.tag=1000;
	m_ProductImageView.contentMode=UIViewContentModeScaleAspectFit;
	[m_scrollView addSubview:m_ProductImageView];
	//[temp_imgView setImage:[UIImage imageNamed:@"loading.png"]];
	if (isFromIndyShopView==YES) 
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",documentsDirectory,tag];
		
		NSFileManager *temp_fileManger=[NSFileManager defaultManager];
		
		NSString *tmp_urlString=[[l_appDelegate.m_arrIndyShopData objectAtIndex:tag] valueForKey:@"m_strImageUrl"];	
		NSURL *tmp_imageUrl=[[NSURL alloc] initWithString:tmp_urlString];
		
		
		NSLog(@"%@",tmpPath);
		
		if ([temp_fileManger fileExistsAtPath:tmpPath])
		{
			[m_ProductImageView loadImageFromURL:tmp_imageUrl tempImage:[UIImage imageWithContentsOfFile:tmpPath]];
		}
		else
		{
			[m_ProductImageView loadImageFromURL:tmp_imageUrl tempImage:nil];
		}
		
		
		
	}
	else 
	{
		
	

	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",documentsDirectory,tag];
		
	NSFileManager *temp_fileManger=[NSFileManager defaultManager];
		
	NSString *tmp_urlString=[[l_appDelegate.m_arrCategorySalesData objectAtIndex:tag] valueForKey:@"m_strImageUrl2"];
        //Bharat: 11/10/2011: Added check to make sure image2 URL is not empty string.
		if ((tmp_urlString == nil) || ([tmp_urlString length] < 1)) {
			NSLog(@"WhereBox:m_strImageUrl2 is nil (line 672)");
			tmp_urlString=[[l_appDelegate.m_arrCategorySalesData objectAtIndex:tag] valueForKey:@"m_strImageUrl"];
		} else {
			NSLog(@"WhereBox:m_strImageUrl2=[%@] (line 672)",tmp_urlString);
		}
	NSURL *tmp_imageUrl=[[NSURL alloc] initWithString:tmp_urlString];

	// Bharat: poor coding. Better to update same variable with backup values
	//NSString *backup_urlString=[[l_appDelegate.m_arrCategorySalesData objectAtIndex:tag] valueForKey:@"m_strImageUrl"];
	//NSURL *backup_imageUrl=[[NSURL alloc] initWithString:backup_urlString];


	NSLog(@"%@",tmpPath);
	
	// Check if temp image file exists and has content.
	if ([temp_fileManger fileExistsAtPath:tmpPath] && [UIImage imageWithContentsOfFile:tmpPath])
	{
		[m_ProductImageView loadImageFromURL:tmp_imageUrl tempImage:[UIImage imageWithContentsOfFile:tmpPath]];
	}
	else
	{
		[m_ProductImageView loadImageFromURL:tmp_imageUrl tempImage:nil];
	}
	
	//[m_scrollView addSubview:m_ProductImageView];
	}
//	[self.view addSubview:temp_imgView];


//	UILabel *temp_lblBrandName=[[UILabel alloc]initWithFrame:CGRectMake(107,15,195,20)];
//	
	if(l_objCatModel.m_strBrandName.length>0)
		[m_lblBrandName setText:l_objCatModel.m_strBrandName];
	else
		[m_lblBrandName setText:@"Brand: N/A"];
	
	
	[m_lblBrandName setTextColor:[UIColor whiteColor]];
//	[m_lblBrandName setBackgroundColor:[UIColor clearColor]];
//	[m_lblBrandName setFont:[UIFont fontWithName:kArialBoldFont size:16]];

//	[m_scrollView addSubview:temp_lblBrandName];
//..	[m_DarkGreenBoxView addSubview:temp_lblBrandName];

//	[temp_lblBrandName release];
//	temp_lblBrandName=nil;
	
	UILabel *temp_lblDiscount=[[UILabel alloc]initWithFrame:CGRectMake(30,25,75,40)]; //190,5,85,20)];
	temp_lblDiscount.tag=1001;
	//temp_lblDiscount.lineBreakMode = UILineBreakModeWordWrap;
	temp_lblDiscount.numberOfLines = 2;
	[temp_lblDiscount setFont:[UIFont fontWithName:@"Futura-CondensedExtraBold" size:14]];
	[temp_lblDiscount setText:l_objCatModel.m_strDiscount]; //@"10% discount"];
	[temp_lblDiscount setTextAlignment:UITextAlignmentCenter];
	[temp_lblDiscount setTextColor:[UIColor darkGrayColor]];
	[temp_lblDiscount setBackgroundColor:[UIColor clearColor]];
	//Bharat : 11/21:11
	//[temp_lblDiscount setFont:[UIFont fontWithName:kFontName size:14]];
	[temp_lblDiscount setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_DISCOUNTLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_DISCOUNTLABEL_FONT_SIZE_KEY]]];
	
	UIImageView *temp_yellowImgView=[[UIImageView alloc]init] ;
	temp_yellowImgView.tag=1002;
	
	temp_yellowImgView.contentMode=UIViewContentModeScaleAspectFit;
	
	
	if([l_objCatModel.m_strDiscount length]>7)
	{
		temp_yellowImgView.frame=CGRectMake(20,3,99,80);
		[temp_yellowImgView setImage:[UIImage imageNamed:@"yellow_tag.png"]];
		
	}
	else
	{
		[temp_yellowImgView setImage:[UIImage imageNamed:@"yellow_tag_small.png"]];
		temp_yellowImgView.frame=CGRectMake(5,5,83,79);
		temp_lblDiscount.frame=CGRectMake(13, 25, 60,40);
	}
	[m_scrollView addSubview:temp_yellowImgView];
	[m_scrollView addSubview:temp_lblDiscount];
	//[temp_lblDiscount release];
//	temp_lblDiscount=nil;

	
	//-------calculate distance-------
	l_cllocation=[[CLLocation alloc]initWithLatitude:[l_objCatModel.m_strLatitude floatValue] longitude:[l_objCatModel.m_strLongitude floatValue]];
	CLLocation *tempCurrentCL=[[CLLocation alloc] initWithLatitude:[l_appDelegate.m_objGetCurrentLocation m_latitude] longitude:[l_appDelegate.m_objGetCurrentLocation m_longitude]];
	
	double tempDistance=[l_cllocation distanceFromLocation:tempCurrentCL];
	
	NSString *temp_strDistance;
	
	if (tempDistance > 400) 
	{
		temp_strDistance=[NSString stringWithFormat:@"%.2f miles away",tempDistance * 0.0006214f];
	}
	else {
		temp_strDistance=[NSString stringWithFormat:@"%.0f steps away",tempDistance];
	}
	
	[l_cllocation release];
	[tempCurrentCL release];
	
	//-----------------------------------------------------------------
	
	UIButton *temp_btnMapIt=[UIButton buttonWithType:UIButtonTypeCustom];
	temp_btnMapIt.tag=1003;
	[temp_btnMapIt setImage:[UIImage imageNamed:@"btn_mapit.png"] forState:UIControlStateNormal];
	[temp_btnMapIt addTarget:self action:@selector(btnMapItAction:) forControlEvents:UIControlEventTouchUpInside];
	//Bharat: 11/22/11: Moving down by 20 pixels
	//[temp_btnMapIt setFrame:CGRectMake(10,122,56,46)];
	[temp_btnMapIt setFrame:CGRectMake(10,142,56,46)];
	[m_scrollView addSubview:temp_btnMapIt];
	
	
	UIButton *temp_btn_storeinfo=[UIButton buttonWithType:UIButtonTypeCustom];
	temp_btn_storeinfo.tag=1004;
	[temp_btn_storeinfo setImage:[UIImage imageNamed:@"btn_storeinfo.png"] forState:UIControlStateNormal];
	[temp_btn_storeinfo addTarget:self action:@selector(btnShareInfoAction:) forControlEvents:UIControlEventTouchUpInside];
	//Bharat: 11/22/11: Moving down by 20 pixels
	//[temp_btn_storeinfo setFrame:CGRectMake(77,122,56,46)];
	[temp_btn_storeinfo setFrame:CGRectMake(77,142,56,46)];
//	[m_moreDetailView addSubview:temp_btnMapIt];
//	[m_scrollView addSubview:temp_btnMapIt];
	[m_scrollView addSubview:temp_btn_storeinfo];
	//[self.view addSubview:temp_btnMapIt];

	
	//Bharat: 11/22/11: Moving to right , up by 10 pixels, below product image
	//UILabel *temp_lblDistance=[[UILabel alloc]initWithFrame:CGRectMake(10,176,140,20)];
	UILabel *temp_lblDistance=[[UILabel alloc]initWithFrame:CGRectMake(155,166,140,20)];
	temp_lblDistance.tag=1005;
	[temp_lblDistance setText:temp_strDistance];//[m_locationManager distanceToLocation:l_cllocation]];
	//[m_locationManager distanceAndDirectionTo:l_cllocation]];
	//[NSString stringWithFormat:@"(%.2f meters away)",([l_objCatModel.m_strDistance floatValue] * 1609.344f)]];
	[temp_lblDistance setTextColor:[UIColor darkGrayColor]];
	[temp_lblDistance setBackgroundColor:[UIColor clearColor]];
	[temp_lblDistance setFont:[UIFont fontWithName:kFontName size:12]];
	//[m_scrollView addSubview:];
	[m_scrollView addSubview:temp_lblDistance];


	//	[m_moreDetailView addSubview:temp_lblDistance];
	//[temp_lblDistance release];
//	temp_lblDistance=nil;
	//--------------------------------------------------------------------
	
	//Bharat: 11/22/11: Moving down and expanding the box
	//UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10,82,120,40)];
	UILabel *temp_lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10,82,120,60)];
	temp_lblTitle.tag=1006;
	temp_lblTitle.lineBreakMode = UILineBreakModeWordWrap;
	temp_lblTitle.numberOfLines = 0;
	[temp_lblTitle setText:l_objCatModel.m_strTitle];
	//[temp_lblTitle setTextColor:[UIColor whiteColor]];
	[temp_lblTitle setTextColor:[UIColor colorWithRed:53.0f/255.0f green:135.0f/255.0f blue:151.0f/255.0f alpha:1.0]];
	[temp_lblTitle setBackgroundColor:[UIColor clearColor]];
	//[temp_lblTitle setFont:[UIFont fontWithName:kArialBoldFont size:12]];
	[temp_lblTitle setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_BRIEFDESCLABEL_FONT_SIZE_KEY]]];

	//[m_moreDetailView addSubview:temp_lblTitle];
	[m_scrollView addSubview:temp_lblTitle];
	//[temp_lblTitle release];
//	temp_lblTitle=nil;
	
	UITextView *textViewDescription = [[UITextView alloc] initWithFrame:CGRectMake(10, 195, 290, 60)];
	textViewDescription.tag=1007;
	textViewDescription.scrollEnabled = NO;
	textViewDescription.backgroundColor=[UIColor clearColor];
	textViewDescription.userInteractionEnabled = NO;
	//textViewDescription.textColor=[UIColor whiteColor];
	textViewDescription.textColor=[UIColor blackColor];
	//[textViewDescription setFont:[UIFont fontWithName:kFontName size:12]];
	[textViewDescription setFont:[UIFont fontWithName:[[Context getInstance] getFontTypeForKey:WHEREBOX_DESCRIPTIONLABEL_FONT_TYPE_KEY] size:[[Context getInstance] getFontSizeForKey:WHEREBOX_DESCRIPTIONLABEL_FONT_SIZE_KEY]]];
	textViewDescription.text = l_objCatModel.m_strOverview;
	[m_scrollView addSubview:textViewDescription];
	
		
	[textViewDescription setFrame:CGRectMake(10, 195, 290, textViewDescription.contentSize.height)];
	[textViewDescription setFrame:CGRectMake(10, 195, 290, textViewDescription.contentSize.height)];
	
//	UILabel *temp_lblDescription=[[UILabel alloc]initWithFrame:CGRectMake(10,200,290,120)];
//	temp_lblDescription.lineBreakMode = UILineBreakModeWordWrap;
//	temp_lblDescription.numberOfLines = 0;
//	[temp_lblDescription setText:l_objCatModel.m_strDescription];
//	[temp_lblDescription setTextColor:[UIColor whiteColor]];
//	[temp_lblDescription setBackgroundColor:[UIColor grayColor]];
//	[temp_lblDescription setFont:[UIFont fontWithName:kFontName size:12]];
//	[m_scrollView addSubview:temp_lblDescription];
	
	
	
	UIImageView *temp_imgViewAddress=[[UIImageView alloc]initWithFrame:CGRectMake(5, 27, 11, 10)];
	temp_imgViewAddress.tag=1008;
	temp_imgViewAddress.image=[UIImage imageNamed:@"address_icon.png"];
	//..[m_DarkGreenBoxView addSubview:temp_imgViewAddress];
	[popUpforShareInfo addSubview:temp_imgViewAddress];
	//[temp_imgViewAddress release];
//	temp_imgViewAddress=nil;
	
	UILabel *temp_lblAddress=[[UILabel alloc]initWithFrame:CGRectMake(20,22,240,40)];
	temp_lblAddress.tag=1009;
	[temp_lblAddress setText:l_objCatModel.m_strAddress];
	[temp_lblAddress setTextColor:[UIColor whiteColor]];
	[temp_lblAddress setNumberOfLines:2];
	[temp_lblAddress setBackgroundColor:[UIColor clearColor]];
	[temp_lblAddress setFont:[UIFont fontWithName:kFontName size:13]];
	[popUpforShareInfo addSubview:temp_lblAddress];
//..	[m_DarkGreenBoxView addSubview:temp_lblAddress];

	//[temp_lblAddress release];
//	temp_lblAddress=nil;
	
	UIImageView *temp_imgViewPhone=[[UIImageView alloc]initWithFrame:CGRectMake(5, 66, 8, 12)];
	temp_imgViewPhone.image=[UIImage imageNamed:@"phone_icon.png"];
	temp_imgViewPhone.tag=1010;
//..	[m_DarkGreenBoxView addSubview:temp_imgViewPhone];

	[popUpforShareInfo addSubview:temp_imgViewPhone];
	//[temp_imgViewPhone release];
//	temp_imgViewPhone=nil;
	
	UILabel *temp_lblPhone=[[UILabel alloc]initWithFrame:CGRectMake(20,64,240,20)];
	temp_lblPhone.tag=1011;
	if(l_objCatModel.m_strPhoneNo.length>0)
	{
		[temp_lblPhone setText:l_objCatModel.m_strPhoneNo];
	}
	else 
	{
		[temp_lblPhone setText:@"Phone: N/A"];
	}
	
	[temp_lblPhone setTextColor:[UIColor whiteColor]];
	[temp_lblPhone setBackgroundColor:[UIColor clearColor]];
	[temp_lblPhone setFont:[UIFont fontWithName:kFontName size:13]];
//...	[m_DarkGreenBoxView addSubview:temp_lblPhone];
	[popUpforShareInfo addSubview:temp_lblPhone];
	[temp_lblPhone release];
	temp_lblPhone=nil;
	
	UIImageView *temp_imgViewHours=[[UIImageView alloc]initWithFrame:CGRectMake(5, 91, 10, 10)];
	temp_imgViewHours.tag=1012;
	temp_imgViewHours.image=[UIImage imageNamed:@"hours_icon.png"];
//..	[m_DarkGreenBoxView addSubview:temp_imgViewHours];
	[popUpforShareInfo addSubview:temp_imgViewHours];
	[temp_imgViewHours release];
	temp_imgViewHours=nil;
	
	UILabel *temp_lblHours=[[UILabel alloc]initWithFrame:CGRectMake(20,87,240,20)];
	temp_lblHours.tag=1013;
	if (l_objCatModel.m_strStoreHours.length>0)
	{
		[temp_lblHours setText:l_objCatModel.m_strStoreHours];
	}
	else 
	{
		[temp_lblHours setText:@"Store hours: N/A"];
	}
	
	[temp_lblHours setTextColor:[UIColor whiteColor]];
	[temp_lblHours setBackgroundColor:[UIColor clearColor]];
	[temp_lblHours setFont:[UIFont fontWithName:kFontName size:13]];
//..	[m_DarkGreenBoxView addSubview:temp_lblHours];
	
	[popUpforShareInfo addSubview:temp_lblHours];
	[temp_lblHours release];
	temp_lblHours=nil;
	
	UIButton *temp_closeButton=[UIButton buttonWithType:UIButtonTypeCustom];
	temp_closeButton.tag=1014;
	[temp_closeButton setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
	[temp_closeButton addTarget:self action:@selector(btnClosePopUpforShareInfo:) forControlEvents:UIControlEventTouchUpInside];
	[temp_closeButton setFrame:CGRectMake(221,-11,50,50)];
	[popUpforShareInfo addSubview:temp_closeButton];
	
	//UITextView *temp_txtViewOverview=[[UITextView alloc]init]; 
	//	[temp_txtViewOverview setTextColor:[UIColor whiteColor]];
	//	[temp_txtViewOverview setFont:[UIFont fontWithName:kFontName size:13]];
	//	[temp_txtViewOverview setBackgroundColor:[UIColor clearColor]];
	//	[temp_txtViewOverview setEditable:FALSE];
	//	[temp_txtViewOverview setScrollEnabled:FALSE];
	//	[m_scrollView addSubview:temp_txtViewOverview];
	//...	[m_DarkGreenBoxView addSubview:temp_txtViewOverview];
	//[temp_txtViewOverview setFrame:CGRectMake(10,180,270,temp_txtViewOverview.contentSize.height)];
	//	[temp_txtViewOverview setFrame:CGRectMake(10,180,270,temp_txtViewOverview.contentSize.height)];
	//	
	//m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width, m_scrollView.frame.size.height+ temp_txtViewOverview.contentSize.height);//-50);
	//	[temp_txtViewOverview release];
	//	temp_txtViewOverview=nil;
	//[m_DarkGreenBoxView addSubview:m_scrollView];
	//[m_scrollView addSubview:m_DarkGreenBoxView];

	[self.view addSubview:m_scrollView];
	
	/*// To add the arrow image on the scroll view 
	UIImageView *tmp_arrowImageView=[[UIImageView alloc] initWithFrame:CGRectMake(290,298, 17,17)];
	tmp_arrowImageView.image=[UIImage imageNamed:@"Arrow.png"];
	[self.view addSubview:tmp_arrowImageView];
	[tmp_arrowImageView release];
	tmp_arrowImageView = nil;*/
    
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLeft setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"green arrow_left" ofType:@"png"]] forState:UIControlStateNormal];
    [btnLeft setFrame:CGRectMake(265.0,220.0, 9.0,11.0)];
    [btnLeft addTarget:self action:@selector(leftClick) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:btnLeft];

    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"green arrow_right" ofType:@"png"]] forState:UIControlStateNormal];
    [btnRight setFrame:CGRectMake(285.0,220.0, 9.0,11.0)];
    [btnRight addTarget:self action:@selector(rightClick) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:btnRight];
	
	[m_scrollView setContentSize:CGSizeMake(310, 195 + textViewDescription.frame.size.height)];
	[m_scrollView setContentSize:CGSizeMake(310, 195 + textViewDescription.frame.size.height)];
	
	[textViewDescription release];
	textViewDescription=nil;
	
	//[m_scrollView release];
	//	m_scrollView=nil;

}

-(void)leftClick
{
    [self moveLeft];    
}

-(void)rightClick
{
    [self moveRight];
}

-(IBAction)btnClosePopUpforShareInfo:(id)sender
{
	[popUpforShareInfo setHidden:YES];
}
-(IBAction) btnFindTwitterFrnd
{
	TwitterProcessing *temp_twitterPross=[[TwitterProcessing alloc]init];
	temp_twitterPross.m_addFrndMypageVC=self;
	[temp_twitterPross btnTellFriendByTwitterAction:5];	
}	


-(void)btnMapItAction:(id)sender
{
	[self performSelector:@selector(checkForMallMapExistence)];
	ForPictureDownloading=NO;// in case the person clicks on the map it button while the images are being downloaded ,so set the value of this bool to NO.
}
-(void)checkForMallMapExistence
{
	if(m_isMallMapConnectionActive)
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		m_isMallMapConnectionActive=FALSE;
		[m_mallMapConnection cancel];
	}
	
	 //represents other category sales view i.e. shoes, bags etc.
			
		if(l_appDelegate.m_intCurrentView==1)
		{
		
			if(l_appDelegate.m_arrIndyShopData.count > 0 && l_appDelegate.m_intIndyShopIndex!=-1) //l_appDelegate.m_intCategorySalesIndex!=-1)
			{
				CategoryModelClass *temp_objCatSales=[l_appDelegate.m_arrIndyShopData objectAtIndex:l_appDelegate.m_intIndyShopIndex];		
				[self sendRequestToCheckMallMap:temp_objCatSales.m_strMallImageUrl];	
			}
		}
		else
		{
			if(l_appDelegate.m_arrCategorySalesData.count > 0 && l_appDelegate.m_intCategorySalesIndex!=-1)
			{
				CategoryModelClass *temp_objCatSales=[l_appDelegate.m_arrCategorySalesData objectAtIndex:l_appDelegate.m_intCategorySalesIndex];
				[self sendRequestToCheckMallMap:temp_objCatSales.m_strMallImageUrl];	
			}
		}

	
		
}

-(void)sendRequestToCheckMallMap:(NSString *)imageUrl
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		[[LoadingIndicatorView SharedInstance]startLoadingView:self];
		//[m_activityIndicator startAnimating];
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/text; charset=utf-8", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if (m_isMallMapConnectionActive==TRUE)
		{
			[m_mallMapConnection cancel];
		}
		
		m_mallMapConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
		
		[m_mallMapConnection start];
		m_isMallMapConnectionActive=TRUE;
		
		if(m_mallMapConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		m_isMallMapConnectionActive=FALSE;
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		
		//Bharat: 11/10/2011: On image download failure, the message for runway is being automatically generated
		// due to the delegate being set to 'self'.
		// Setting it 
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		
	}
}



-(IBAction)back
{
	CategoriesViewController *categories_obj=[[CategoriesViewController alloc] init];
	[self.navigationController popViewControllerAnimated:YES];
	[categories_obj release];
	categories_obj=nil;
}

-(IBAction)shareBtnAction
{
	WhereBoxShareNow * vc = [[WhereBoxShareNow alloc] init];
	vc.photoImage = m_ProductImageView.imageView.image;
	vc.m_intInputType = INPUT_TYPE_SHOPBEE_ADS;
	vc.m_catObjModel = l_objCatModel;

	[self.navigationController pushViewController:vc animated:YES];
	[vc release];

#if 0
	// OLD CODE, BEFORE US236 FIXES
	[m_emailFrnds setHidden:NO];
	[m_facebookFrnds setHidden:NO];
	[m_twitterFrnds setHidden:NO];
	[m_shopBeeFrnds setHidden:NO];
	
	[m_btnSendBuyOrNot setHidden:YES];
	[m_btnSendIBoughtIt setHidden:YES];
	
	[m_publicWishlist setHidden:YES];
	[m_privateWishlist setHidden:YES];
	[m_btnCancelWhereBox setHidden:YES];
	
	
	[m_imgShareView setImage:[UIImage imageNamed:@"pop-up_background.png"]];
	
	if(![m_shareView superview])
	{
		[self.view	addSubview:m_shareView];
		[m_shareView setTransform:CGAffineTransformMakeTranslation(POP_UP_OFFSET_X, POP_UP_OFFSET_Y)];
		[m_shareView setHidden:NO];
	}
	else {
		[m_shareView setHidden:NO];
		[self.view bringSubviewToFront:m_shareView];
	}
#endif // OLD CODE, BEFORE US236 FIXES

}

-(IBAction)btnTellFriendByEmailAction:(id)sender
{
	[self btnTellAFriendAction];
}

-(IBAction)btnTellShopBeeFriendsAction:(id)sender
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	l_request=[[ShopbeeAPIs alloc] init];
	[l_whereboxIndicatorView startLoadingView:self];
	[l_request getFriendsList:@selector(requestCallBackMethod:responseData:) tempTarget:self customerid:customerID loginId:customerID];
	[l_request release];
	l_request=nil;
	
}
-(IBAction)btnFittingRoomViewAction
{
	
	[m_emailFrnds setHidden:YES];
	[m_facebookFrnds setHidden:YES];
	[m_twitterFrnds setHidden:YES];
	[m_shopBeeFrnds setHidden:YES];
	
	[m_btnSendBuyOrNot setHidden:NO];
	[m_btnSendIBoughtIt setHidden:NO];
	
	[m_publicWishlist setHidden:YES];
	[m_privateWishlist setHidden:YES];
	[m_btnCancelWhereBox setHidden:NO];
	
	
	[m_imgShareView setImage:[UIImage imageNamed:@"pop-up_fittingroom-backgound.png"]];
	
	if(![m_shareView superview])
	{
		[self.view	addSubview:m_shareView];
		[m_shareView setTransform:CGAffineTransformMakeTranslation(POP_UP_OFFSET_X, POP_UP_OFFSET_Y)];
		[m_shareView setHidden:NO];
	}
	else 
	{
		[m_shareView setHidden:NO];
		[self.view bringSubviewToFront:m_shareView];
	}
	
}

-(IBAction)btnWishListViewAction:(id)sender
{
	[m_emailFrnds setHidden:YES];
	[m_facebookFrnds setHidden:YES];
	[m_twitterFrnds setHidden:YES];
	[m_shopBeeFrnds setHidden:YES];
	
	[m_btnSendBuyOrNot setHidden:YES];
	[m_btnSendIBoughtIt setHidden:YES];
	[m_publicWishlist setHidden:NO];
	[m_privateWishlist setHidden:NO];
	[m_btnCancelWhereBox setHidden:NO];
	
	
	[m_imgShareView setImage:[UIImage imageNamed:@"pop-up_wishlist-background.png"]];
	
	
	if(![m_shareView superview])
	{
		[self.view	addSubview:m_shareView];
		[m_shareView setTransform:CGAffineTransformMakeTranslation(POP_UP_OFFSET_X, POP_UP_OFFSET_Y)];
		[m_shareView setHidden:NO];
	}
	else {
		[m_shareView setHidden:NO];
		[self.view bringSubviewToFront:m_shareView];
	}
}

-(IBAction)btnSendIBoughtItAction
{
	m_BoughtItOrNot=@"   I bought it";
	//if(tempFittingRoom)
//	{
//		[tempFittingRoom release];
//		tempFittingRoom = nil;
//	}
	tempFittingRoom=[[FittingRoomViewController alloc]init];
	
	tempFittingRoom.m_sendBuy=@"   I bought it";
	tempFittingRoom.m_typeLabel=@"I bought it";
	
	
	UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Would you like to leave a comment?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil]autorelease];
	[dataReloadAlert show];
	
}
-(IBAction)btnSendBuyOrNotAction
{
	m_BoughtItOrNot=@"   Buy or not";
	//if(tempFittingRoom)
//	{
//		[tempFittingRoom release];
//		tempFittingRoom = nil;
//	}
	tempFittingRoom=[[FittingRoomViewController alloc]init];
	tempFittingRoom.m_sendBuy=@"   Buy or not";
	tempFittingRoom.m_typeLabel=@"Buy or not";
	
	UIAlertView *dataReloadAlert=[[[UIAlertView alloc]initWithTitle:@"" message:@"Would you like to leave a comment?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil]autorelease];
	[dataReloadAlert show];
}



-(IBAction)btnPublicAction
{
	m_privateOrPublic=@"public";
	[self performSelector:@selector(addToWishList:)];
	
}	
-(IBAction)btnPrivateAction
{
	m_privateOrPublic=@"private";
	[self performSelector:@selector(addToWishList:)];
	
}


-(IBAction)addToWishList:(id)sender
{
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	l_request=[[ShopbeeAPIs alloc] init];
	NSDictionary *dic= 	[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",l_objCatModel.m_strId,@"advtlocid",l_objCatModel.m_strProductName,@"comments",m_privateOrPublic,@"visibility",nil];
	NSLog(@"%@",dic);
	[l_whereboxIndicatorView startLoadingView:self];
	[l_request addToWishList:@selector(requestCallBackMethodforWishList:responseData:) tempTarget:self tmpDict:dic];
	l_request=nil;
}
#pragma mark swipe methods
- (IBAction)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer
{
	[popUpforShareInfo setHidden:YES];
	[m_shareView setHidden:YES];
	if(recognizer.direction==UISwipeGestureRecognizerDirectionRight)
	{
		[self LeftMovement];
	}
	else
	{
		[self RightMovement];
	}
	
}

#pragma mark left swipe action

-(void)moveLeft
{
    if (tagg>0) 
	{
		tagg--;
		
	}
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",documentsDirectory,tagg];
	NSLog(@"WhereBox:moveLeft: imagePath=[%@]",tmpPath);
	
	//NSFileManager *tempFileManager=[NSFileManager defaultManager];
	m_productImage=[UIImage imageWithContentsOfFile:tmpPath];
	[self whereBtnPressed:tagg];
}

-(IBAction)LeftMovement
{
    [self moveLeft];	
}
#pragma mark right swipe action 

-(void)moveRight
{
    if (isFromIndyShopView==YES) 
	{
		if (tagg<[l_appDelegate.m_arrIndyShopData count]-1) 
		{
			tagg++;
		}
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",documentsDirectory,tagg];
		NSLog(@"WhereBox:moveRight: imagePath=[%@]",tmpPath);
		//NSFileManager *tempFileManager=[NSFileManager defaultManager];
		m_productImage=[UIImage imageWithContentsOfFile:tmpPath];
		[self whereBtnPressed:tagg];
		
	}
	else 
	{
		if (tagg<([l_appDelegate.m_arrCategorySalesData count]-1)) 
		{
			tagg++;
			
			NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
			NSString *documentsDirectory = [paths objectAtIndex:0];
			NSMutableString *tmpPath=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",documentsDirectory,tagg];
			NSLog(@"WhereBox:moveRight: imagePath=[%@]",tmpPath);
			//NSFileManager *tempFileManager=[NSFileManager defaultManager];
			m_productImage=[UIImage imageWithContentsOfFile:tmpPath];
			[self whereBtnPressed:tagg];
			
		}
	}

}

-(IBAction)RightMovement
{
    [self moveRight];	
}

-(void)sendRequestToLoadImages:(NSString *)imageUrl withContinueToNext:(BOOL) continueToNextFlag
{
    //NSLog(@"WhereBox:sendRequestToLoadImages:[%@]",imageUrl);
	if (continueToNextFlag == YES)
		ForPictureDownloading=YES;
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:25.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"text/xml; charset=utf-8", @"Content-Type", nil];
		
		
		//[theRequest setHTTPBody:[imageUrl dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if(m_isConnectionActive)
			[m_theConnection cancel];
		
		m_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[m_theConnection start];
		
		m_isConnectionActive=TRUE;
		
		if(m_theConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		//self.view.userInteractionEnabled=TRUE;
		m_isConnectionActive=FALSE;
		//[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		//Bharat: 11/10/2011: On image download failure, the message for runway is being automatically generated
		// due to the delegate being set to 'self'.
		// Setting it to nil will avoid false triggering.
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
}

#pragma mark -
#pragma mark WebService Response Received methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	//NSLog(@"WhereBox:didReceiveResponse:for tagg=[%d] when counter=[%d]",self.tagg, counter);
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	
	if (connection == m_mallMapConnection&&ForPictureDownloading==NO) 
	{
		[[LoadingIndicatorView SharedInstance]stopLoadingView];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		
		long long temp_length=[response expectedContentLength];
		
		[m_mallMapConnection cancel];
		m_isMallMapConnectionActive=FALSE;
		
		StreetMallMapViewController *temp_streetMapViewCtrl=[[StreetMallMapViewController alloc]init];
		
		if (temp_length ==0)
		{
			temp_streetMapViewCtrl.m_isShowMallMapTab=FALSE;	
		}
		else {
			temp_streetMapViewCtrl.m_isShowMallMapTab=TRUE;	
		}
		
		[self.navigationController pushViewController:temp_streetMapViewCtrl animated:YES];
		[temp_streetMapViewCtrl release];
		temp_streetMapViewCtrl=nil;
		
		[CATransaction begin];
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionFromLeft;
		animation.duration = 0.6;
		//animation.delegate=self;
		[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
		[CATransaction commit];
		
	}
	else 
	{
		m_intResponseCode = [httpResponse statusCode];
		NSLog(@"%d",m_intResponseCode);
		[m_mutCatResponseData setLength:0];
	}
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	//NSLog(@"WhereBox:didReceiveData:for tagg=[%d] when counter=[%d]",self.tagg, counter);
	[m_mutCatResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"WhereBox:connectionDidFinishLoading:for tagg=[%d] when counter=[%d]",self.tagg, counter);
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	[self.view setUserInteractionEnabled:TRUE];
	m_isConnectionActive=FALSE;
	
	if (m_intResponseCode==401)
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
	}
	else if(m_mutCatResponseData!=nil && m_intResponseCode==200&&ForPictureDownloading==NO)
	{	
		
				
		NSLog(@"%@",[[[NSString alloc]initWithData:m_mutCatResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string=[[NSString alloc]initWithData:m_mutCatResponseData encoding:NSUTF8StringEncoding];
		
		SBJSON *temp_objSBJson=[[SBJSON alloc]init];
		NSArray *temp_arrResponse =[[NSArray alloc]initWithArray:[temp_objSBJson objectWithString:temp_string]];
		
		CategoryModelClass *temp_objCatSales;
		
		NSDictionary *temp_dictCat;
		
		for (int i=0;i<[temp_arrResponse count]; i++)
		{
			temp_dictCat=[temp_arrResponse objectAtIndex:i];
			//NSLog(@"%@",temp_dictCat);
			temp_objCatSales=[[CategoryModelClass alloc]init];
			temp_objCatSales.m_strLatitude=[[temp_dictCat objectForKey:@"location"] objectForKey:@"latitude"];
			temp_objCatSales.m_strLongitude=[[temp_dictCat objectForKey:@"location"] objectForKey:@"longitude"];
			temp_objCatSales.m_strMallImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[temp_dictCat objectForKey:@"location"] objectForKey:@"mallMapImageUrl"]];
			
			//reuse distance for street name to be shown on category rows : address1 will be used
			//temp_objCatSales.m_strDistance=[[temp_dictCat objectForKey:@"location"] objectForKey:@"distance"];
			temp_objCatSales.m_strDistance=[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"];
			
			temp_objCatSales.m_strStoreHours=[[temp_dictCat objectForKey:@"location"] objectForKey:@"storeHours"];
			temp_objCatSales.m_strPhoneNo=[[temp_dictCat objectForKey:@"location"] objectForKey:@"phoneNumber"];
			
			temp_strAdd2=[[temp_dictCat objectForKey:@"location"] objectForKey:@"address2"];
			
			if(temp_strAdd2.length>0)
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"],temp_strAdd2];
			else
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@",[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"]];
			
			
			
			temp_strCity=[[temp_dictCat objectForKey:@"location"] objectForKey:@"city"];
			temp_strState=[[temp_dictCat objectForKey:@"location"] objectForKey:@"state"];
			temp_strZip=[[temp_dictCat objectForKey:@"location"] objectForKey:@"zip"];
			
			if(temp_strCity.length>0)
			{
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strCity];
			}
			
			if(temp_strState.length>0)
			{
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strState];
			}
			
			if(temp_strZip.length>0)
			{
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strZip];
			}
			
			temp_objCatSales.m_strTitle=[[temp_dictCat objectForKey:@"product"] objectForKey:@"productTagLine"];
			temp_objCatSales.m_strDiscount=[[temp_dictCat objectForKey:@"product"] objectForKey:@"discountInfo"];
			temp_objCatSales.m_strOverview=[[temp_dictCat objectForKey:@"product"] objectForKey:@"termsAndConditions"];
			temp_objCatSales.m_strProductName=[[temp_dictCat objectForKey:@"product"] objectForKey:@"productName"];
			temp_objCatSales.m_strBrandName=[[temp_dictCat objectForKey:@"product"] objectForKey:@"brandName"];
			
			temp_objCatSales.m_strId=[NSString stringWithFormat:@"%@",[[temp_dictCat objectForKey:@"product"] objectForKey:@"id"]];
			
			NSLog(@"%@",temp_objCatSales.m_strId);
			
			NSArray *temp_arrImgAdUrls=[[temp_dictCat objectForKey:@"product"] objectForKey:@"imageUrls"];
			NSString *temp_str=@"";
			if (temp_arrImgAdUrls.count>0)
			{
				temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:0]];
				temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				NSLog(@"%@",temp_str);
				temp_objCatSales.m_strImageUrl=temp_str;
				
				if(temp_arrImgAdUrls.count>=2)
				{
					NSString * tStrTest = [temp_arrImgAdUrls objectAtIndex:1];
					if ((tStrTest != nil) && ([tStrTest length] > 1)) {
						temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:1]];
						temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
						NSLog(@"Cat sale image url2: %@",temp_str);
						temp_objCatSales.m_strImageUrl2=temp_str;
					} else {
						temp_objCatSales.m_strImageUrl2=nil;
					}
				}

				if(temp_arrImgAdUrls.count>=3)
				{
					NSString * tStrTest = [temp_arrImgAdUrls objectAtIndex:2];
					if ((tStrTest != nil) && ([tStrTest length] > 1)) {
						temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:2]];
						temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
						//NSLog(@"Cat sale image url3: %@",temp_str);
						temp_objCatSales.m_strImageUrl3=temp_str;
					} else {
						temp_objCatSales.m_strImageUrl3=nil;
					}
				}
				
			}
			
	//..	
		
			if(l_appDelegate.m_intCurrentView==1)
			{
				[l_appDelegate.m_arrIndyShopData addObject:temp_objCatSales];
			}
			else {
				[l_appDelegate.m_arrCategorySalesData addObject:temp_objCatSales];
			}
			//[m_arrCategoryData addObject:temp_objCatSales];
			
			[temp_objCatSales release];
			temp_objCatSales=nil;
		}
		
		//NSLog(@"cat Index: %d, count: %d",l_appDelegate.m_intCategoryIndex,l_appDelegate.m_arrCategorySalesData.count);
		
		[temp_objSBJson release];
		temp_objSBJson=nil;
		
		[temp_string release];
		temp_string=nil;
		
		[temp_arrResponse release];
		temp_arrResponse=nil;
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		[self.view setUserInteractionEnabled:TRUE];
		m_isConnectionActive=FALSE;
		
		[m_tableView reloadData];
		
		if(l_appDelegate.m_intCurrentView==1)
		{
			if (l_appDelegate.m_arrIndyShopData.count>0) 
			{ 
					[m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
			}
		}
		else
		{
				if (l_appDelegate.m_arrCategorySalesData.count>0) 
				{
					[m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
				
				}
		}
		
		//[self performSelector:@selector(loadImagesForOnscreenRows) withObject:nil afterDelay:0.5];
		
	}
	else if (ForPictureDownloading==YES) 
	{
		ForPictureDownloading=NO;
		//NSString *temp_string=[[NSString alloc]initWithData:m_mutCatResponseData encoding:NSUTF8StringEncoding];
		
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSMutableString *pathDoc=[NSString stringWithFormat:@"%@",documentsDirectory];
		NSLog(@"Doc Directory______Path_____%@",pathDoc);
		NSMutableString *tempEmail=[NSMutableString stringWithFormat:@"%@/CategorySalesImage@_%d.jpg",pathDoc,counter];
		NSLog(@"tempEmail %@",tempEmail);
		//[l_indicatorView startLoadingView:self];
		[m_mutCatResponseData writeToFile:tempEmail atomically:YES];
		
		
		if (isFromIndyShopView==YES) 
		{
			if (counter<[l_appDelegate.m_arrIndyShopData count]-1)
			{
				counter++;
				NSString *_URLImageString=[[l_appDelegate.m_arrIndyShopData objectAtIndex:counter] valueForKey:@"m_strImageUrl"];
				[self sendRequestToLoadImages:_URLImageString withContinueToNext:YES];
				
			}
		}
		else 
		{
			if (counter<[l_appDelegate.m_arrCategorySalesData count]-1) 
		{
			counter++;
			NSString *URLImageString=[[l_appDelegate.m_arrCategorySalesData objectAtIndex:counter] valueForKey:@"m_strImageUrl2"];
        	//Bharat: 11/10/2011: Added check to make sure image2 URL is not empty string.
            if ((URLImageString == nil) || ([URLImageString length] < 1)) {

				NSLog(@"WhereBox:m_strImageUrl2 is nil (URLImageString)");
				URLImageString = [[l_appDelegate.m_arrCategorySalesData objectAtIndex:counter] valueForKey:@"m_strImageUrl"];
			} else {
				NSLog(@"WhereBox:m_strImageUrl2=[%@] (URLImageString)",URLImageString);
			}
			[self sendRequestToLoadImages:URLImageString withContinueToNext:YES];
			
		}
				
		}
	}
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	NSLog(@"WhereBox:didFailWithError:for tagg=[%d] when counter=[%d]",self.tagg, counter);
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	m_isConnectionActive=FALSE;
	m_isMallMapConnectionActive=FALSE;
	
	self.view.userInteractionEnabled=TRUE;
	[m_tableView reloadData];
	
	//Bharat: 11/10/2011: On image download failure, the message for runway is being automatically generated
	// due to the delegate being set to 'self'.
	// Setting it to nil will avoid false triggering.
	UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[networkDownAlert show];
	[networkDownAlert release];
	networkDownAlert=nil;
	
	//inform the user
    //NSLog(@"Connection failed! Error - %@ %@",[error localizedDescription],[[error userInfo] objectForKey:NSErrorFailingURLStringKey]);
	//NSLog(@"Exit :didFailWithError");
	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark Facebookmethods

-(IBAction)ShopbeeFriends
{

	ShopbeeFriendsViewController *shopbee_obj=[[ShopbeeFriendsViewController alloc]init];
	[self.navigationController pushViewController:shopbee_obj animated:YES];
	[shopbee_obj release];
	shopbee_obj=nil;
}

- (IBAction)clickButtonTellFriendsByFacebook:(id)sender 
{
#ifdef OLD_FB_SDK
	if (l_appDelegate.m_session == nil) {
		l_appDelegate.m_session = [FBSession sessionForApplication:_APP_KEY 
											  secret:_SECRET_KEY delegate:self];
	}
	
	localFBSession = [FBSession session];
	
	if (fbLoginDialog == nil) {
		fbLoginDialog = [[[FBLoginDialog alloc] initWithSession:localFBSession] autorelease];
	}
    [fbLoginDialog show];
#endif
}

#ifdef OLD_FB_SDK
- (void)session:(FBSession*)session didLogin:(FBUID)uid {
	localFBSession =session;
    NSLog(@"User with id %lld logged in.", uid);
	[self getFacebookName];
}

- (void)getFacebookName {
	NSString* fql = [NSString stringWithFormat:
					 @"select uid,name from user where uid == %lld", localFBSession.uid];
	NSDictionary* params = [NSDictionary dictionaryWithObject:fql forKey:@"query"];
	[[FBRequest requestWithDelegate:self] call:@"facebook.fql.query" params:params];
//	self.post=YES;
}

- (void)request:(FBRequest*)request didLoad:(id)result {
	if ([request.method isEqualToString:@"facebook.fql.query"]) {
//		NSArray* users = result;
//		NSDictionary* user = [users objectAtIndex:0];
//		NSString* name = [user objectForKey:@"name"];
//		self.username = name;		
		
//		if (self.post) {
			[self postToFacebookWall];
//			self.post = NO;
//		}
	}
}

- (void)postToFacebookWall {
	
	NSLog(@"%@, %@, %@, %@, %@, %@", l_objCatModel.m_strBrandName, l_objCatModel.m_strTitle, l_objCatModel.m_strDiscount, l_objCatModel.m_strAddress, l_objCatModel.m_strImageUrl, l_objCatModel.m_strMallImageUrl);
	NSString *message = [NSString stringWithFormat:@"%@, %@, %@", l_objCatModel.m_strTitle, l_objCatModel.m_strDiscount, l_objCatModel.m_strAddress];
	//NSString *message2 = [NSString stringWithFormat:@"%@\r%@\r%@", l_objCatModel.m_strTitle, l_objCatModel.m_strDiscount, l_objCatModel.m_strAddress];

	FBStreamDialog *dialog = [[[FBStreamDialog alloc] init] autorelease];
	dialog.userMessagePrompt = @"Enter additional comment";
	dialog.delegate = self;
	
	// build attachment with JSONFragment
	NSString *customMessage = message; 
	NSString *postName = l_objCatModel.m_strBrandName; 
	NSString *serverLink = [NSString stringWithFormat:@"http://itunes.apple.com/app/fashiongram/id498116453?mt=8"];
	NSString *imageSrc = l_objCatModel.m_strImageUrl;
	
	NSMutableDictionary *dictionary = [[[NSMutableDictionary alloc] init]autorelease];
	[dictionary setObject:postName forKey:@"name"];
	[dictionary setObject:serverLink forKey:@"href"];
	[dictionary setObject:customMessage forKey:@"description"];
	
	NSMutableDictionary *media = [[[NSMutableDictionary alloc] init]autorelease];
	[media setObject:@"image" forKey:@"type"];
	[media setObject:serverLink forKey:@"href"];
	[media setObject:imageSrc forKey:@"src"];               
	[dictionary setObject:[NSArray arrayWithObject:media] forKey:@"media"];  
	
	NSLog(@"WhereBox:postToFacebookWall:attachment=[%@]",[dictionary JSONFragment]);
	dialog.attachment = [dictionary JSONFragment];
	[dialog show];
	
}

/**
 * Called when the dialog succeeds and is about to be dismissed.
 */
- (void)dialogDidSucceed:(FBDialog*)dialog {
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message sent" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alert show];
	[alert release];	
}

/**
 * Called when the dialog is cancelled and is about to be dismissed.
 */
- (void)dialogDidCancel:(FBDialog*)dialog {
	// do nothing
}

- (void)fbDidLogin
{	
	 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Successful" message:@"login Successfull" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	 [alert show];
	 [alert release];	
	 NSLog(@"logged in.....................");
	
}
#pragma mark 
#pragma mark FBRequestDelegate 
/** 
 * Called when an error prevents the request from completing successfully. 
 */ 
- (void)request:(FBRequest*)request didFailWithError:(NSError*)error{ 
	
} 

/** 
 * Called when a request returns and its response has been parsed into an object. 
 * The resulting object may be a dictionary, an array, a string, or a number, depending 
 * on thee format of the API response. 
 */ 
//- (void)request:(FBRequest*)request didLoad:(id)result { 
//   
//	NSLog(@"resultresult:-%@",result);
//			
//}

- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"received response");
}

#endif
#pragma mark -
#pragma mark alertView  

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if(alertView.tag!=2)
	{
		
		if(buttonIndex==0)
		{
			if([m_BoughtItOrNot isEqualToString:@"   I bought it"])
			{	
				NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
				NSString *customerID= [prefs objectForKey:@"userName"];
				l_request=[[ShopbeeAPIs alloc] init];
				NSDictionary *dic= 	[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",l_objCatModel.m_strId,@"advtlocid",@"",@"comments",nil];
				NSLog(@"%@",dic);
				[l_whereboxIndicatorView startLoadingView:self];
				[l_request sendIBoughtItRequest:@selector(requestCallBackMethodForSendBuyItOrNot:responseData:) tempTarget:self tmpDict:dic];
				[l_request release];
				l_request=nil;
			}
			else 
			{
				NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
				NSString *customerID= [prefs objectForKey:@"userName"];
				l_request=[[ShopbeeAPIs alloc] init];
				NSDictionary *dic= 	[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid",l_objCatModel.m_strId,@"advtlocid",@"",@"comments",nil];
				NSLog(@"%@",dic);
				[l_whereboxIndicatorView startLoadingView:self];
				[l_request sendBuyItOrNotRequest:@selector(requestCallBackMethodForSendBuyItOrNot:responseData:) tempTarget:self tmpDict:dic];
				
				[l_request release];
				l_request=nil;
				
				
			}
			[m_shareView setHidden:YES];
		}
		else
		{
			tempFittingRoom.m_Image=m_productImage;
			NSLog(@"this is one");//yes
			tempFittingRoom.m_catObjModel = l_objCatModel;
			tempFittingRoom.m_bigSalesModel = nil;
			[self.navigationController pushViewController:tempFittingRoom animated:YES];
			
			
		}
		
	}
	
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_lblBrandName = nil;
    self.popUpforShareInfo = nil;
    self.m_DarkGreenBoxView = nil;
    self.m_btnCancelWhereBox = nil;
    self.m_imgShareView = nil;
    self.m_sharing_lable = nil;
    self.m_emailFrnds = nil;
    self.m_facebookFrnds = nil;
    self.m_twitterFrnds = nil;
    self.m_shopBeeFrnds = nil;
    self.m_btnSendBuyOrNot = nil;
    self.m_btnSendIBoughtIt = nil;
    self.m_publicWishlist = nil;
    self.m_privateWishlist = nil;
    self.m_tableView = nil;
    self.m_shareView = nil;
    self.btnBackObj = nil;
    self.m_activityIndicator = nil;
    self.m_FittingRoomView = nil;
    
    [m_cancel release]; m_cancel = nil;
    [popupView release]; popupView = nil;
}

- (void)dealloc {
    
    [btnBackObj release];
    [m_moreDetailView release];
    [m_shareView release];
    [m_FittingRoomView release];
    [m_tableView release];
    [m_scrollView release];
    
   // This was already released.
   [m_mallMapConnection release];
    
    [m_activityIndicator release];
    [m_mutCatResponseData release];
    [_engine release];
    [m_cancel release];
    [popupView release];
    [twitterEngine release];
    [tw release];
    [m_emailFrnds release];
    [m_facebookFrnds release];
    [m_twitterFrnds release];
    [m_shopBeeFrnds release];
    [m_btnSendBuyOrNot release];
    [m_btnSendIBoughtIt release];
    [m_publicWishlist release];
    [m_privateWishlist release];
    [m_sharing_lable release];
    [m_imgShareView release];
    [m_btnCancelWhereBox release];
    [m_view release];
    [m_indicatorView release];
    [m_privateOrPublic release];
    [m_BoughtItOrNot release];
   // [m_productImage release];
    [m_DarkGreenBoxView release];
    [popUpforShareInfo release];
    [m_lblBrandName release];
    
    [super dealloc];
}

@end
