//
//  GoogleViewfriends.m
//  QNavigator
//
//  Created by softprodigy on 13/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GoogleViewfriends.h"
#import "Constants.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AuditingClass.h"
#import "QNavigatorAppDelegate.h"
#import "GTMOAuth2Authentication.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "ShopbeeAPIs.h"
#import "LoadingIndicatorView.h"
#import "NewsDataManager.h"

# define SectionHeaderHeight 25

@implementation GoogleViewfriends
@synthesize m_nonshopBeeFriends;
@synthesize m_googleFriendsEmail;
@synthesize m_googleAllFriendsEmail;
@synthesize m_ShopBeegoogleFriends;
@synthesize m_string_email;
@synthesize table;
@synthesize m_arrAllFriends;
@synthesize currentIndex;
@synthesize m_intRequestType, m_intResponseCode, m_mutResponseData;

// GDL: We declare these, so we should synthesize them.
@synthesize m_view;
@synthesize m_indicatorView;

QNavigatorAppDelegate *l_appDelegate;
int count;
int AddButtonTag;
ShopbeeAPIs *l_request;

LoadingIndicatorView *temp_indicatorView;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	
	NSLog(@"----------->>>>>>>>>>>>>>>> %@",m_nonshopBeeFriends);
	[super viewDidLoad];
	m_googleFriendsEmail=[[NSMutableArray alloc]init];
	m_googleAllFriendsEmail=[[NSMutableArray alloc]init];
	temp_indicatorView=[LoadingIndicatorView SharedInstance];
	
}

#pragma mark -
#pragma mark custom methods

-(IBAction)btnLogoutAction:(id)sender
{
	
	GTMOAuth2ViewControllerTouch *newAuth=[[GTMOAuth2ViewControllerTouch alloc]init];
	[newAuth cancelSigningIn];
	[newAuth release];
	newAuth=nil;
}




/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


-(IBAction)btnBackAction:(id)sender
{
	
	[self.navigationController popViewControllerAnimated:YES];
	
}


-(void)btnInviteAllAction:(id)sender
{
	count=1;
	[self performSelector:@selector(showPicker:)];
	
	
}

-(void)btnAddAllAction:(id)sender
{
	NSString *tmp_mail;
	NSMutableArray *tmp_array;
	NSString *customerId;
	tmp_array=[[NSMutableArray alloc] init];
	for (int i=0; i<[m_ShopBeegoogleFriends count];i++)
	{
		tmp_mail=[[m_ShopBeegoogleFriends objectAtIndex:i]valueForKey:@"Email"];
		[tmp_array addObject:tmp_mail];
	}
	
	NSString *strJson=[NSString stringWithFormat:@"%@",[tmp_array JSONRepresentation]];
	
	customerId=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	l_request=[[ShopbeeAPIs alloc] init];
	[temp_indicatorView startLoadingView:self];
	[l_request sendRequestToAddFriend:@selector(requestCallBackMethodForAddFriend:responseData:) tempTarget:self custId:customerId tmpUids:strJson];
	[l_request release];
	l_request=nil;
	[tmp_array release];
	tmp_array=nil;
	AddButtonTag=-1;
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"GoogleViewFriends"
                                                    withAction:@"btnAddAllAction"
                                                     withLabel:nil
                                                     withValue:nil];
}

-(void)btnAddAction:(id)sender
{
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	
	l_request=[[ShopbeeAPIs alloc] init];
	
	NSString *tempString= [NSString stringWithFormat:@"[\"%@\"]",[[m_ShopBeegoogleFriends objectAtIndex:[sender tag] - 13000] valueForKey:@"Email"]];
	NSLog(@"%@",tempString);
	
	AddButtonTag=[sender tag] - 13000;
	[temp_indicatorView startLoadingView:self];
	[l_request sendRequestToAddFriend:@selector(requestCallBackMethodForAddFriend:responseData:) tempTarget:self custId:customerID tmpUids:tempString];//[m_shopBeeArray objectAtIndex:[sender tag]]
	[l_request release];
	l_request=nil;
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"GoogleViewFriends"
                                                    withAction:@"btnAddAction"
                                                     withLabel:nil
                                                     withValue:nil];
	
}

-(void)showLoadingView
{
	NSAutoreleasePool *tempPool=[[NSAutoreleasePool alloc]init];
	
	[[LoadingIndicatorView SharedInstance]startLoadingView:self];
	
	[tempPool release];
}

#define SHARENOW_REQUEST_TYPE_SHARENOW	 4444

-(void) performInviteFriends:(NSInteger)tag 
{
    
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	NSMutableArray * emailArr = [[NSMutableArray alloc] init];
    
	if(count==0)
	{
		NSString *emailStr=[[m_nonshopBeeFriends objectAtIndex:tag] objectForKey:@"Email"];
		NSLog(@"%@",emailStr);
        [emailArr addObject:emailStr];
	}
	else if(count==1)
	{
		for(int i=0;i<[m_nonshopBeeFriends count];i++)
		{
			[emailArr addObject:[[m_nonshopBeeFriends objectAtIndex:i] objectForKey:@"Email"]];
		}
		NSLog(@"emails.........>>%@",emailArr);
	}
    
    if ([emailArr count] <= 0) {
		[emailArr addObject:customerID];
	}
	
	[l_appDelegate CheckInternetConnection];
	
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		// Call FashionGram API to invoke share
		[self performSelectorInBackground:@selector(showLoadingView) withObject:nil];
        
		m_mutResponseData=[[NSMutableData alloc] init];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=addShopBeeFriends",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		NSString *temp_strJson=[NSString stringWithFormat:@"custid=%@&friendids=%@", customerID, [emailArr JSONRepresentation]];
		//NSLog(@"%@",temp_strJson);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		NSLog(@"temp_strJson=[%@]",temp_strJson);
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
        
		NSURLConnection * conn = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
		self.m_intRequestType = SHARENOW_REQUEST_TYPE_SHARENOW;
		[conn start];
	}
    
    [emailArr release]; emailArr = nil;
    
}


-(void)showPicker:(id)sender
{
    [self performInviteFriends:[sender tag] - 13000];
    return;
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	if(count==0)
	{
		m_string_email=[[[m_nonshopBeeFriends objectAtIndex:[sender tag]] objectForKey:@"Email"]retain];
		NSLog(@"%@",m_string_email);
	}
	else if(count==1)
	{
		for(int i=0;i<[m_nonshopBeeFriends count];i++)
		{
			[m_googleAllFriendsEmail addObject:[[m_nonshopBeeFriends objectAtIndex:i] objectForKey:@"Email"]];
		}
		NSLog(@"emails.........>>%@",m_googleAllFriendsEmail);
	}
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self performSelector:@selector(displayComposerSheet)];	
		}
		else
		{
			[self performSelector:@selector(launchMailAppOnDevice)];
		}
	}
	else
	{
		[self performSelector:@selector(launchMailAppOnDevice)];
	}
}


#pragma mark -
#pragma mark Image download finished
-(void)imageDownloaded:(UIImage *)image withId:(NSString *)withId
{
	NSLog(@"Image downloaded in GoogleViewfriends class");
}

-(UIImage *)checkForImageAvailability:(NSString *)email
{
	NSArray *arrTempFriends=[m_arrAllFriends copy];
	
	NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"Email CONTAINS[cd] %@",email];
	NSArray *filteredImageArray = [arrTempFriends filteredArrayUsingPredicate:bPredicate];
	[arrTempFriends release];
	
	if (filteredImageArray.count>0)
	{
		return [[filteredImageArray objectAtIndex:0] valueForKey:@"Image"];
	}
	
	return nil;
}

#pragma mark -
#pragma mark tableview delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
	return 2;
	
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	if(section==0)
	{
		return [m_ShopBeegoogleFriends count];
		
	}
	else 
	{
		return [m_nonshopBeeFriends count];
	}
	
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	
	NSString *SimpleTableIdentifier = [NSString stringWithFormat:@"Cell %@",indexPath];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: SimpleTableIdentifier];
	
	if (cell == nil) { 
		
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleTableIdentifier] autorelease];
		
		//making Main Title for TableViewCell
		UILabel *temp_lblFriendName=[[UILabel alloc]init];
		temp_lblFriendName.frame=CGRectMake(45,6,185,25);
		temp_lblFriendName.font=[UIFont fontWithName:kFontName size:16];
		temp_lblFriendName.tag=10;
		temp_lblFriendName.backgroundColor=[UIColor clearColor];
		temp_lblFriendName.textColor=[UIColor blackColor];
		[cell.contentView addSubview:temp_lblFriendName];
		[temp_lblFriendName release];
		temp_lblFriendName=nil;
		
		UIImageView *temp_imageView=[[UIImageView alloc]init];
		temp_imageView.frame=CGRectMake(5,6.5,35,35);
		temp_imageView.tag=11;
		temp_imageView.contentMode=UIViewContentModeScaleAspectFit;
		[cell.contentView addSubview:temp_imageView];
		[temp_imageView release];
		temp_imageView=nil;
	}
	
	
	if (indexPath.section==0) 
	{
		
		((UILabel *)[cell.contentView viewWithTag:10]).text =[NSString stringWithFormat:@"%@",[[m_ShopBeegoogleFriends objectAtIndex:indexPath.row] objectForKey:@"Name"]];	
		
		if (((UILabel *)[cell.contentView viewWithTag:10]).text.length==0)
		{
			((UILabel *)[cell.contentView viewWithTag:10]).text=[NSString stringWithFormat:@"%@",[[m_ShopBeegoogleFriends objectAtIndex:indexPath.row] objectForKey:@"Email"]];
		}
		
		
		((UIImageView *)[cell.contentView viewWithTag:11]).image=[self checkForImageAvailability:[[m_ShopBeegoogleFriends objectAtIndex:indexPath.row] objectForKey:@"Email"]];
		
		if(!(((UIImageView *)[cell.contentView viewWithTag:11]).image))
			((UIImageView *)[cell.contentView viewWithTag:11]).image=[UIImage imageNamed:@"no-image"];
		
        UIButton *but = (UIButton *)[cell.contentView viewWithTag:13000 + indexPath.row];
		if(but) 
            [but removeFromSuperview];
		// making custom UIButton
        UIButton *temp_addButton=[UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *img = [UIImage imageNamed:@"add_btn.png"];
        temp_addButton.frame=CGRectMake(250,(cell.contentView.frame.size.height - img.size.height)/2,img.size.width,img.size.height);
        //temp_addButton.frame=CGRectMake(250,13,43,24);
        [temp_addButton setImage:img forState:UIControlStateNormal];
        [temp_addButton addTarget: self action:@selector(btnAddAction:) forControlEvents:UIControlEventTouchUpInside];
        temp_addButton.tag = 13000 + indexPath.row;
        [cell.contentView addSubview:temp_addButton];
	}
	else if (indexPath.section==1) 
	{
		
		((UILabel *)[cell.contentView viewWithTag:10]).text=[NSString stringWithFormat:@"%@",[[m_nonshopBeeFriends objectAtIndex:indexPath.row] objectForKey:@"Name"]];
		
		if (((UILabel *)[cell.contentView viewWithTag:10]).text.length==0)
		{
			((UILabel *)[cell.contentView viewWithTag:10]).text=[NSString stringWithFormat:@"%@",[[m_nonshopBeeFriends objectAtIndex:indexPath.row] objectForKey:@"Email"]];
		}
		
		((UIImageView *)[cell.contentView viewWithTag:11]).image=[self checkForImageAvailability:[[m_nonshopBeeFriends objectAtIndex:indexPath.row] objectForKey:@"Email"]];
		
		if(!(((UIImageView *)[cell.contentView viewWithTag:11]).image))
			((UIImageView *)[cell.contentView viewWithTag:11]).image=[UIImage imageNamed:@"no-image"];
		
		
		// making custom UIButton
        UIButton *but = (UIButton *)[cell.contentView viewWithTag:indexPath.row + 13000];
		if(but) 
            [but removeFromSuperview];
        UIButton *temp_addButton=[UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *img = [UIImage imageNamed:@"blueinvite_btn.png"];
        temp_addButton.frame=CGRectMake(250,(cell.contentView.frame.size.height - img.size.height)/2,img.size.width,img.size.height);
        [temp_addButton setImage:img forState:UIControlStateNormal];
        temp_addButton.tag=indexPath.row + 13000;
        [temp_addButton addTarget: self action:@selector(showPicker:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:temp_addButton];

	}
	
    
	cell.backgroundView =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_without_arrow_in_add_friend"]];	
    
	cell.selectionStyle=UITableViewCellSelectionStyleNone;
	return cell;
	
	
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (section==0 && m_ShopBeegoogleFriends.count>0)
		return 30;
	else if(section==0 && m_ShopBeegoogleFriends.count==0)
		return 0;
	if (section==1 && m_nonshopBeeFriends.count>0)
		return 30;
	else if(section==1 && m_nonshopBeeFriends.count==0)
		return 0;
	return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	if (section==0) 
	{
		
		UIView *tmp_view;
		tmp_view = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, SectionHeaderHeight)];
        
        UIColor *bgColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"lightgrey_bar.png"]];
        tmp_view.backgroundColor=bgColor;
        
		//tmp_view.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		[tmp_view autorelease];
		
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,2, 320, 25)]  ;
		tmp_headerLabel.font=[UIFont fontWithName:kFontName size:16];
        
        // GDL: Removed unnecessary quotes.
		tmp_headerLabel.text=[NSString stringWithFormat:@"%i Friends are on Tiny News",[m_ShopBeegoogleFriends count]];
        
        
       tmp_headerLabel.backgroundColor=[UIColor clearColor];
        
	//	tmp_headerLabel.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		tmp_headerLabel.textColor=[UIColor blackColor];
		[tmp_view addSubview:tmp_headerLabel];
		[tmp_headerLabel release];
		tmp_headerLabel=nil;
		
		UIButton *temp_addAll=[[UIButton alloc] initWithFrame:tmp_view.frame];
        UIImage *img = [UIImage imageNamed:@"addall_btn.png"];
		[temp_addAll setImage:img forState:UIControlStateNormal];
		[temp_addAll addTarget: self action:@selector(btnAddAllAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_addAll.frame=CGRectMake(250,1,img.size.width,img.size.height);
		[tmp_view addSubview:temp_addAll];
		[temp_addAll release];//18aug
		temp_addAll = nil;
		
		return tmp_view;
		
	}
	else {
		
		UIView *tmp_headerForSecondSection;
		tmp_headerForSecondSection = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, SectionHeaderHeight)];
        UIColor *bgColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"lightgrey_bar.png"]];
        tmp_headerForSecondSection.backgroundColor=bgColor;
        
		//tmp_headerForSecondSection.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		[tmp_headerForSecondSection autorelease];
		
		UILabel *tmp_headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,2, 320, 25)] ;
		[tmp_headerLabel autorelease];
		tmp_headerLabel.font=[UIFont  fontWithName:kFontName size:16 ];
		tmp_headerLabel.text= [NSString stringWithFormat:@"%i"@" ""Friends not on Tiny News",[m_nonshopBeeFriends count]];
		tmp_headerLabel.textColor=[UIColor blackColor];
        
        tmp_headerLabel.backgroundColor=[UIColor clearColor];
        
		//tmp_headerLabel.backgroundColor=[UIColor colorWithRed:.76f green:.76f blue:.76f alpha:1.0];
		[tmp_headerForSecondSection addSubview:tmp_headerLabel];
		
		
		UIButton *temp_addAll=[[UIButton alloc] initWithFrame:tmp_headerForSecondSection.frame];
        UIImage *img = [UIImage imageNamed:@"blueinviteall_btn.png"];
		[temp_addAll setImage:img forState:UIControlStateNormal];
		[temp_addAll addTarget: self action:@selector(btnInviteAllAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_addAll.frame=CGRectMake(247,1,img.size.width,img.size.height);
		//temp_addAll.tag=indexPath.row;
		[tmp_headerForSecondSection addSubview:temp_addAll];
		[temp_addAll release];//18aug
		temp_addAll=nil;
		
		//tmp_headerForSecondSection=nil;
		
		return tmp_headerForSecondSection;
		
		
	}
}
#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	m_intResponseCode = [httpResponse statusCode];
	NSLog(@"InviteFriendsViaEmail:didReceiveResponse:statusCode=%d",m_intResponseCode);
	
	[m_mutResponseData setLength:0];	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	
	NSLog(@"InviteFriendsViaEmail:didReceiveData");
	[m_mutResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"InviteFriendsViaEmail:connectionDidFinishLoading");
	
	if ((m_intResponseCode==200) && (self.m_intRequestType == SHARENOW_REQUEST_TYPE_SHARENOW))//request from mypage user pic change
	{
        
		NSLog(@"InviteFriendsViaEmail:sharePhotoInfoViaEmail:[%@]",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string=[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding];
		if ((temp_string != nil) && ([temp_string rangeOfString:@"true"].location != NSNotFound)) {
			[[LoadingIndicatorView SharedInstance]stopLoadingView];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
			//[self.navigationController popViewControllerAnimated:YES];
            
			UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"" message:@"Invitation sent successfully" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
			[av show];
			[av release];
            
			[temp_string release];
			return;
		} 
		
		[[LoadingIndicatorView SharedInstance]stopLoadingView];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[temp_string release];
		return;
		
	} else {
        NSLog(@"GoogleViewFriends: %d", m_intResponseCode);
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }

	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
}


- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	NSLog(@"InviteFriendsViaEmail:didFailWithError");
	[[LoadingIndicatorView SharedInstance]stopLoadingView];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
    [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
	
	if (m_mutResponseData)
	{
		[m_mutResponseData release];
        m_mutResponseData=nil;
	}
}


#pragma mark -
#pragma mark Callback methods


-(void)requestCallBackMethodForAddFriend:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	//NSLog(@"%@",[[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[tempString release];
	tempString=nil;
	
	[temp_indicatorView stopLoadingView];
	NSInteger Code=[responseCode integerValue];

	if(AddButtonTag==-1)
	{
		if (Code==200)
		{
			[self showAlertView:@"Friend request has been sent to all friends." alertMessage:nil];	
			
			[m_ShopBeegoogleFriends removeAllObjects];
			[table reloadData];
			
		}
		else  {
            NSLog(@"GoogleViewFriends:requestCallBackMethodForAddFriend:AddButtonTag==-1 %d", Code);
            [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
		}
		
	}
	else
	{
		if (Code==200) 
		{
			[self showAlertView:@"Friend Request sent" alertMessage:@"Friend request has been sent." ];
			[m_ShopBeegoogleFriends removeObjectAtIndex:AddButtonTag];
			
			[table reloadData];
		}
		else {
            NSLog(@"GoogleViewFriends:requestCallBackMethodForAddFriend %d", Code);
            [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
		}
	}
	
}	

#pragma mark -
#pragma mark alertMessage methods

-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage
{
	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[tempAlert show];
	[tempAlert release];
	tempAlert=nil;
}



#pragma mark -
#pragma mark MailComposer delegates
// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet
{
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	NSMutableArray *toRecipients=[[NSMutableArray alloc]init];
	
	if(count==0)
	{
		[picker setToRecipients:[NSArray arrayWithObject:m_string_email]];
		[m_string_email release];
		m_string_email=nil;
	}
	else if(count==1)
	{
		[picker setBccRecipients:m_googleAllFriendsEmail];
		count=0;
	}
	[picker setSubject:kTellAFriendMailSubject];
	
	NSString *emailBody;
	emailBody=kTellAFriendMailBody;
	
	[toRecipients release];
	toRecipients=nil;
	
	[picker setMessageBody:emailBody isHTML:NO];
	[self presentModalViewController:picker animated:YES];
	
    [picker release];
	picker=nil;
	
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	AuditingClass *temp_objAudit;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			
			
			temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{	
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Tell a Friend about FashionGram",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-2",@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
			}
			
			
			
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	
	self.navigationItem.rightBarButtonItem.enabled=NO;
	//m_lblRecipient.text=@"";
	//m_lblRecipient.hidden=TRUE;
	[self dismissModalViewControllerAnimated:YES];
	
}
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=";
	NSString *body = @"&body=";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release all retained IBOutlets.
    self.table = nil;
}


- (void)dealloc  {
    [m_nonshopBeeFriends release];
    [m_googleFriendsEmail release];
    [m_googleAllFriendsEmail release];
    [m_ShopBeegoogleFriends release];
    [m_arrAllFriends release];
    [m_string_email release];
    [m_view release];
    [m_indicatorView release];
    [table release];
	
    [super dealloc];
}


@end
