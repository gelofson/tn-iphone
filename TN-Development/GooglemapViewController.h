//
//  GooglemapViewController.h
//  TinyNews
//
//  Created by jay kumar on 7/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MyAnnotation.h"
#import"ShopbeeAPIs.h"
#import "JMC.h"
#import "FittingRoomLoadingView.h"
#import "ContentViewController.h"



//#define DEFAULT_NUMBEROF_ITEMS_IN_ROW 3

@interface GooglemapViewController : ContentViewController <CLLocationManagerDelegate, MKMapViewDelegate>
{
    UIButton *m_MineBtn;
	UIButton *m_FriendsBtn;
	UIButton *m_FollowingBtn;
	UIButton *m_PopularBtn;
    NSInteger tagValue;
    int totalRecordCountForActiveTag;
    IBOutlet MKMapView* m_mapView; 
   NSMutableArray* annotations;
    int annonationCount;
     NSArray * m_HowDoesThisLookArray;
    NSMutableDictionary *m_mainDictionary; 
    ShopbeeAPIs *m_requestObj;
    NSString *m_UserId;
    NSMutableArray *m_setallNewCategryPins;
    NSMutableArray * m_messageIDsArrayHowDoesThisLook;
    NSMutableArray *m_arrayforpindata;
    NSArray * m_CheckThisOutArray;
     NSMutableArray * m_messageIDsArrayCheckThisOut;
    NSArray * m_FoundASaleArray;
     NSMutableArray * m_messageIDsArrayFoundASale;
    NSMutableArray *m_messageIDsArrayBoughtIt;
    NSMutableArray *m_messageIDsArrayBuyIt;
    NSArray *m_buyItArray;
    NSMutableArray *messageId;
    NSString *m_type;
    NSString *selectedType;
    
    IBOutlet UIButton *AddToCatagBtn;
    
    IBOutlet UILabel *m_headerLBL;
    IBOutlet UIImageView *m_headerIMG;
    int clickCount;
    
    

}
@property(nonatomic,retain)IBOutlet UILabel *m_headerLBL;
@property(nonatomic,retain)IBOutlet UIImageView *m_headerIMG;

@property (nonatomic, retain) CLLocationManager *locationManager;

@property(nonatomic,retain)IBOutlet UIButton *AddToCatagBtn;
@property(nonatomic,copy) NSString *selectedType;

@property(nonatomic,retain) NSMutableArray* annotations;
@property(nonatomic,retain) IBOutlet UIButton *m_MineBtn;
@property(nonatomic,retain) IBOutlet UIButton *m_FriendsBtn;
@property(nonatomic,retain) IBOutlet UIButton *m_FollowingBtn;
@property(nonatomic,retain) IBOutlet UIButton *m_PopularBtn;
@property(nonatomic, assign) int totalRecordCountForActiveTag;
@property(nonatomic,readwrite) IBOutlet NSInteger tagValue;
@property(nonatomic,retain)IBOutlet MKMapView* m_mapView;
-(IBAction)sendRequestforAParticularBtn:(id)sender;
-(IBAction)AddToCatagBtn:(id)sender;
-(void)mapviewdrow:(NSArray*)maparray;
-(void)drowPinsonView:(NSString*)type;
-(void)btnPicturesAction:(id)sender;


@property (nonatomic, retain) CLLocation *currLocation;
-(void) findLocation;
-(void) foundLocation:(CLLocation *)loc;
-(void) removeMoreClicked;
-(void) addMoreClicked;

@end
