//
//  MyCLController.h
// 
//
//  Created by Soft Prodigy on 01/10/2010.
//  Copyright __MyCompanyName__. All rights reserved.
//

#import "MyCLController.h"
#import "Constants.h"

// GDL: We need to import this here, since we're no longer doing in the header file.
#import "QNavigatorAppDelegate.h"

// GDL: BEGIN
// This switches off core location on the simuator and returns a fixed location in Porland, OR.
// We need this because Mac OS X 10.7 Lion freezes if you attempt core location in the simulator

#if TARGET_IPHONE_SIMULATOR == 1

@interface CLLocationManager (Simulator)
@end
//
@implementation CLLocationManager (Simulator)

//-(void)startUpdatingLocation {
//    CLLocation *powellsTech = [[[CLLocation alloc] initWithLatitude:LATITUDE_IN_SIMULATOR longitude:LONGITUDE_IN_SIMULATOR] autorelease];
//    [self.delegate locationManager:self
//               didUpdateToLocation:powellsTech
//                      fromLocation:powellsTech];    
//}

@end

#endif // TARGET_IPHONE_SIMULATOR


// GDL: END

// Shorthand for getting localized strings, used in formats below for readability
#define LocStr(key) [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:nil]

//#define locationUpdateMax  2
// This is a singleton class, see below
static MyCLController *sharedCLDelegate = nil;

@implementation MyCLController
//int locationUpdateCounter = 0;


@synthesize delegate,locationManager;
@synthesize appDelegate;

- (id) init
{
	self = [super init];
	if (self != nil) 
	{
		appDelegate = (QNavigatorAppDelegate *)[[UIApplication sharedApplication] delegate];
		self.locationManager = [[CLLocationManager alloc] init];
		
		//self.locationManager.distanceFilter = 10;
		//self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
		
		self.locationManager.delegate = self; // Tells the location manager to send updates to this object
	}
	return self;
}


// Called when the location is updated
- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation
{
	
	NSMutableString *update = [[[NSMutableString alloc] init] autorelease];
	
	[update appendFormat:@"%f,%f", newLocation.coordinate.latitude,newLocation.coordinate.longitude]; 
	
	//NSLog(@"lat and long %f , %f",oldLocation.coordinate.latitude,oldLocation.coordinate.longitude);
	//NSLog(@"lat and long %f , %f",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
	
	// Send the update to our delegate	
	[self.delegate newLocationUpdate:update];	
	
}


// Called when there is an error getting the location
- (void)locationManager:(CLLocationManager *)manager
	   didFailWithError:(NSError *)error
{
	NSMutableString *errorString = [[[NSMutableString alloc] init] autorelease];
	
	[locationManager stopUpdatingLocation];
	if ([error domain] == kCLErrorDomain) {
		
		// We handle CoreLocation-related errors here
		
		switch ([error code]) {
				// This error code is usually returned whenever user taps "Don't Allow" in response to
				// being told your app wants to access the current location. Once this happens, you cannot
				// attempt to get the location again until the app has quit and relaunched.
				//
				// "Don't Allow" on two successive app launches is the same as saying "never allow". The user
				// can reset this for all apps by going to Settings > General > Reset > Reset Location Warnings.
				//
			case kCLErrorDenied:
				//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationDenied", nil)];
				//NSLog(@"%@",errorString);
				//[self.delegate DisplayMessageForUserToAllowLocationFinder];
				[appDelegate failToUpdateLocation];
				break;
				
				// This error code is usually returned whenever the device has no data or WiFi connectivity,
				// or when the location cannot be determined for some other reason.
				//
				// CoreLocation will keep trying, so you can keep waiting, or prompt the user.
			case kCLErrorNetwork:
				//[errorString appendFormat:@"%@\n", NSLocalizedString(@"kCLErrorNetwork", nil)];
				[appDelegate networkErrorInLocationUpdate];
				
				break;			
		
				
			case kCLErrorLocationUnknown:
				//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
				[appDelegate unknownLocationInLocationUpdate];
				break;
				
				// We shouldn't ever get an unknown error code, but just in case...
				//
			
			default:
				//[appDelegate failToUpdateLocation];
				[appDelegate unknownLocationInLocationUpdate];
				//[errorString appendFormat:@"%@ %d\n", NSLocalizedString(@"GenericLocationError", nil), [error code]];
				break;
		}
	} 
	else {
		// We handle all non-CoreLocation errors here
		// (we depend on localizedDescription for localization)
		[errorString appendFormat:@"Error domain: \"%@\"  Error code: %d\n", [error domain], [error code]];
		[errorString appendFormat:@"Description: \"%@\"\n", [error localizedDescription]];
		
	}
	
	// Send the update to our delegate
}

#pragma mark ---- singleton object methods ----

// See "Creating a Singleton Instance" in the Cocoa Fundamentals Guide for more info

+ (MyCLController *)sharedInstance {
    @synchronized(self) {
        if (sharedCLDelegate == nil)
		{
            sharedCLDelegate=[[self alloc] init]; // assignment not done here
        }
    }
	
    return sharedCLDelegate;
}

+ (id)allocWithZone:(NSZone *)zone {
	NSLog(@"allocWithZone");
    @synchronized(self) {
        if (sharedCLDelegate == nil) {
            sharedCLDelegate = [super allocWithZone:zone];
            return sharedCLDelegate;  // assignment and return on first allocation
        }
    }
    return nil; // on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone
{
	NSLog(@"copyWithZone");
    return self;
}

- (id)retain 
{
	NSLog(@"retain");
    return self;
}

-(void)setCustomDelegate
{
	locationManager.delegate=self;
}

- (unsigned)retainCount {
	NSLog(@"retainCount");
    return UINT_MAX;  // denotes an object that cannot be released
}

- (void)release 
{
if(locationManager)
{
	[locationManager release];
	
	
}
	NSLog(@"release");
    //do nothing
}

-(void)releaseTheObjects
{
	if(locationManager)
	{
		sharedCLDelegate=nil;
		[locationManager release];
		
		
	}
	
}

- (id)autorelease {
    return self;
}

@end