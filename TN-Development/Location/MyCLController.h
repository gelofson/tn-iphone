//
//  MyCLController.h
// 
//
//  Created by Soft Prodigy on 01/10/2010.
//  Copyright __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

// GDL: Reference the class, but don't load the header.
@class QNavigatorAppDelegate;

// This protocol is used to send the text for location updates back to another view controller
@protocol MyCLControllerDelegate <NSObject>
@required
-(void)newLocationUpdate:(NSString *)text;

// GDL: As far as I can tell, this method is never called.
// GDL: I'm making it optional so we don't get compiler warnings for not implementing it.
@optional
-(void)InternetNotAvailable;

@end


// Class definition

@interface MyCLController : NSObject <CLLocationManagerDelegate> {
	CLLocationManager *locationManager;
	id delegate;	
	QNavigatorAppDelegate *appDelegate;
	
}

@property (nonatomic, retain) CLLocationManager *locationManager;
@property(nonatomic,retain)	QNavigatorAppDelegate *appDelegate;
@property (nonatomic,assign) id <MyCLControllerDelegate> delegate;

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation;

- (void)locationManager:(CLLocationManager *)manager
	   didFailWithError:(NSError *)error;
-(void)setCustomDelegate;

-(void)releaseTheObjects;

+ (MyCLController *)sharedInstance;

@end

