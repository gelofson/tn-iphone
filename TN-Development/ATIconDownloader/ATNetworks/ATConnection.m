
//  Copyright _ All rights reserved.
//

#import "ATConnection.h"


@implementation ATConnection

@synthesize tag;

- (id)initWithRequest:(NSURLRequest *)request delegate:(id)delegate startImmediately:(BOOL)startImmediately withTag:(NSString*)tag_{
	
	if(self == [super initWithRequest:request delegate:delegate startImmediately:YES])
		tag = [tag_ retain];
	
	return self;
	
}

- (void) dealloc{

	[tag release];
	[super dealloc];
}
@end
