
//  Copyright  __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"


@interface ATNetwork : NSObject {
    Reachability* hostReach;
    Reachability* internetReach;
    Reachability* wifiReach;
    NSInteger connectionTag;
	
}

- (void) openConnection : (NSString *)urlString withCallBackTarget:(id)target_ withCallBackSelector:(SEL)selector_;


@end
