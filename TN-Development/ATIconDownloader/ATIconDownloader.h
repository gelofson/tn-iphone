//
//  ATIconDownloader.h
//  
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ATIconDownloaderDelegate 

- (void)imageDownloadFinish:(UIImage*)image_ withId:(NSIndexPath*)id_;

@end


@class Reachability;

@interface ATIconDownloader : NSObject {
    Reachability* hostReach;
    Reachability* internetReach;
    Reachability* wifiReach;
    NSInteger connectionTag;

	NSIndexPath* id_;
	NSString	*imageUrl;
}

@property(nonatomic, assign) NSIndexPath* id_;
@property(nonatomic, retain) NSString	*imageUrl;

//- (void) openConnection : (NSString *)urlString withCallBackTarget:(NSIndexPath*)id_;
- (void) openConnection : (NSString *)urlString withOtherUrlString:(NSString*)urlString2 withCallBackTarget:(NSIndexPath*)target_ withDelegate:(id)delegate_;
- (BOOL) closeAllActiveConnections;
@end
