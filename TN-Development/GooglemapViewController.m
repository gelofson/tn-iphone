//
//  GooglemapViewController.m
//  TinyNews
//
//  Created by jay kumar on 7/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GooglemapViewController.h"
#import "QNavigatorAppDelegate.h"
#import"Constants.h"
#import"FittingRoomLoadingView.h"
//#import"FittingRoomMoreViewController.h"
#import "ChooseCategories.h"
#import "NewsDataManager.h"

@implementation GooglemapViewController

@synthesize m_MineBtn;
@synthesize m_FriendsBtn;
@synthesize m_FollowingBtn;
@synthesize m_PopularBtn;
@synthesize totalRecordCountForActiveTag;
@synthesize tagValue;
@synthesize m_mapView;
@synthesize annotations;
@synthesize selectedType;
@synthesize AddToCatagBtn;
@synthesize m_headerIMG;
@synthesize m_headerLBL;

@synthesize locationManager;
@synthesize currLocation;

QNavigatorAppDelegate *l_appDelegate;
FittingRoomLoadingView * l_FittingRoomIndicatorView;
FittingRoomMoreViewController * l_ViewRequestObj;
int MineCheck;

#define MIN_COUNT 24
#define MAX_COUNT 1000

int count = 24;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    totalRecordCountForActiveTag = 0;
    m_headerLBL.hidden=YES;
    m_headerIMG.hidden=NO;
   
    
    m_requestObj = [[ShopbeeAPIs alloc] init];
	NSUserDefaults * prefs = [NSUserDefaults standardUserDefaults];
	m_UserId = [prefs valueForKey:@"userName"];
    m_setallNewCategryPins=[[NSMutableArray alloc]init];
     m_messageIDsArrayHowDoesThisLook = [[NSMutableArray alloc] init];
    m_mainDictionary=[[NSMutableDictionary alloc]init];
     m_messageIDsArrayCheckThisOut = [[NSMutableArray alloc] init];
    m_messageIDsArrayFoundASale = [[NSMutableArray alloc] init];
    m_messageIDsArrayBoughtIt = [[NSMutableArray alloc] init];
    l_FittingRoomIndicatorView=[FittingRoomLoadingView SharedInstance];
    //TODO remove comment in line below.
//    [self sendRequestforAParticularBtn:nil];
    clickCount=0;
    // Do any additional setup after loading the view from its nib.

    
    UIButton *btnAddMore = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnAddMore setFrame:CGRectMake(20, 400, 36, 36)];
    [btnAddMore setImage:[UIImage imageNamed:@"add-pins"] forState:UIControlStateNormal];
    [btnAddMore addTarget:self action:@selector(addMoreClicked) forControlEvents:UIControlEventTouchUpInside];
    [btnAddMore setUserInteractionEnabled:YES];
    [self.view addSubview:btnAddMore];

    UIButton *btnRemoveMore = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRemoveMore setFrame:CGRectMake(320 - 36 - 20, 400, 36, 36)];
    [btnRemoveMore setImage:[UIImage imageNamed:@"remove-pins"] forState:UIControlStateNormal];
    [btnRemoveMore addTarget:self action:@selector(removeMoreClicked) forControlEvents:UIControlEventTouchUpInside];
    [btnRemoveMore setUserInteractionEnabled:YES];
    [self.view addSubview:btnRemoveMore];
    
    
}

-(void) removeMoreClicked
{
    NSLog(@"Remove more button has been clicked");
    
    count = count - 24;
    if ( count  <= MIN_COUNT)
    {
        count = MIN_COUNT;

    }
    [self sendRequestforAParticularBtn:nil];
}

-(void) addMoreClicked
{
    NSLog(@"Add more button has been clicked");
    count = count + 24;
    if ( count  >= MAX_COUNT)
    {
        count = MAX_COUNT;
    }
    [self sendRequestforAParticularBtn:nil];
}

#pragma mark MarkView custom method

-(void)btnPicturesAction:(id)sender {
	//l_ViewRequestObj.m_CallBackViewController=self;		
	[self.navigationController pushViewController:l_ViewRequestObj animated:YES];
    if (l_ViewRequestObj!=nil)
    {
        [l_ViewRequestObj release];
        l_ViewRequestObj=nil;
    }
    
	
	
}


-(void)drowPinsonView:(NSString*)type
{
    
    NSArray * theArray = nil;
    
    switch (tagValue) {
            
        case 1:
            
            if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"MineBuyItArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"MineBoughtItArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"MineCheckThisOutArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"MineHowDoesThisLookArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"MineFoundASaleArray"];
                [self mapviewdrow:theArray];
            }
            MineCheck = 1;
            break;
            
        case 2:
            
            if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FriendsBuyItArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FriendsBoughtItArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FriendsCheckThisOutArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FriendsFoundASaleArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FriendsHowDoesThisLookArray"];
                [self mapviewdrow:theArray];
            }
            MineCheck = 0;
            break;
            
        case 3:
            
            if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FollowingBuyItArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FollowingBoughtItArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FollowingCheckThisOutArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FollowingFoundASaleArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"FollowingHowDoesThisLookArray"];
                [self mapviewdrow:theArray];
            }
            MineCheck = 0;
            break;
            
        case 4:
            
            if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularBuyItArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularBoughtItArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularCheckThisOutArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularFoundASaleArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularHowDoesThisLookArray"];
                [self mapviewdrow:theArray];
            }
            MineCheck = 0;
            break;
        case 5:
        {
            
            
            theArray = [m_mainDictionary objectForKey:@"PopularHowDoesThisLookArray"];
            [self mapviewdrow:theArray];

/*
            if ([type caseInsensitiveCompare:@"BuyIt"] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularBuyItArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularBoughtItArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularCheckThisOutArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularFoundASaleArray"];
                [self mapviewdrow:theArray];
            }
            else if ([type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
                theArray = [m_mainDictionary objectForKey:@"PopularHowDoesThisLookArray"];
                [self mapviewdrow:theArray];
            }
             
            */
            
            MineCheck = 0;
            break;
        }
        
    }

    
}


-(void)mapviewdrow:(NSArray*)maparray;
{
    
    NSLog(@"the map array is %@",maparray);
    for (int i=0; i<[maparray count]; i++)
    {
        CLLocationCoordinate2D theLocation;
        MyAnnotation *myannotation=[[MyAnnotation alloc]init];
        
        NSDictionary *dict=[maparray objectAtIndex:i];
        
        
        
        theLocation.latitude=[[dict valueForKey:@"latitude"]doubleValue];
        theLocation.longitude=[[dict valueForKey:@"longitude"]doubleValue];
        
//        NSLog(@"Showing : %d, Lattitude :%f Longitude :%f", i, theLocation.latitude, theLocation.longitude);
        
        myannotation.title=[dict valueForKey:@"headline"];
        myannotation.coordinate=theLocation;
        [m_mapView addAnnotation:myannotation];
        [annotations addObject:myannotation];
        [messageId addObject:[dict valueForKey:@"messageId"]];
        [myannotation release];

    }
    
       
    MKMapRect flyTo=MKMapRectNull;
    
    for (id<MKAnnotation> annotation in annotations)
    {
    
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        }
        else 
        {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
    m_mapView.visibleMapRect = flyTo;
    UITapGestureRecognizer *m_mapViewtap = [[[UITapGestureRecognizer alloc] 
                                    initWithTarget:self action:@selector(handleGestureonMapViewDoubleTap:)] autorelease];
    m_mapViewtap.numberOfTapsRequired = 2;
    [m_mapView addGestureRecognizer:m_mapViewtap];


   
}

#pragma mark MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
	
	// if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
	// try to dequeue an existing pin view first
	static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
	MKPinAnnotationView* pinView = [[[MKPinAnnotationView alloc]
									 initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier] autorelease];
	pinView.animatesDrop=NO;
	pinView.canShowCallout=YES;
    pinView.tag=[[messageId objectAtIndex:annonationCount] intValue];
	pinView.pinColor=MKPinAnnotationColorRed;
    UITapGestureRecognizer *m_pinViewtap = [[[UITapGestureRecognizer alloc] 
                                   initWithTarget:self action:@selector(handleGestureforpinDoubletap:)] autorelease];
    m_pinViewtap.numberOfTapsRequired = 2;
    [pinView addGestureRecognizer:m_pinViewtap];

    annonationCount++;
    
	return pinView;
}

-(void)handleGestureforpinDoubletap:(UITapGestureRecognizer*)pinTap
{
    MKPinAnnotationView *pinView=(MKPinAnnotationView*)pinTap.view;
        
    int messageIdforpinclick=[pinView tag];
	
	if (l_ViewRequestObj) 
	{
		[l_ViewRequestObj release];
		l_ViewRequestObj=nil;
	}
	

	
	l_ViewRequestObj=[[FittingRoomMoreViewController alloc] init];
	
	l_ViewRequestObj.m_messageId=messageIdforpinclick;
	
	l_ViewRequestObj.GetPinnFlag=NO;
	//NSLog(@"Message Id is%@",[sender tag]);
	NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
	NSString *tmp_String=[prefs2 stringForKey:@"userName"];
	//m_requestObj=[[ShopbeeAPIs alloc] init];
    
	[l_FittingRoomIndicatorView startLoadingView:self type:1];
    
	[m_requestObj getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:tmp_String messageId:messageIdforpinclick];
    
    //NSLog(@"the pin count ia %d",pinTap.);
    
}

-(void)handleGestureonMapViewDoubleTap:(UITapGestureRecognizer*)mapViewTap
{
    int count=0;
    
    for (id<MKAnnotation> annotation in annotations)
    {
        if ([annotation.title length]>=1)
        {
        if (clickCount%2==0)
        {
            MKPinAnnotationView *pinView=(MKPinAnnotationView *)[self.view viewWithTag:[[messageId objectAtIndex:count] intValue]] ;
                UIImageView *bubbleimage=[[[UIImageView alloc]initWithFrame:CGRectMake(-15, -45,100 , 45)] autorelease];
                [bubbleimage setImage:[UIImage imageNamed:@"popup.png"]];
                bubbleimage.tag=count+100;
                
                UILabel *headerLBL=[[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 35)]autorelease];
                [headerLBL setBackgroundColor:[UIColor clearColor]];
                [headerLBL setFont:[UIFont boldSystemFontOfSize:12.0f]];
            [headerLBL setText:annotation.title];
                [headerLBL setNumberOfLines:0];
                [headerLBL setTextAlignment:UITextAlignmentCenter];
                [headerLBL setTextColor:[UIColor whiteColor]];
            pinView.canShowCallout=YES;
            
                //headerLBL.tag=count+100;
                [bubbleimage addSubview:headerLBL];
                [pinView addSubview:bubbleimage];
            
        }
        else
        {
                UIImageView *bubbleimage=(UIImageView *)[self.view viewWithTag:count+100];
                [bubbleimage removeFromSuperview];
            
        }
            
        }
        count++;
    }
    clickCount++;

}


#pragma mark - Callback Methods
#pragma mark


-(void)getAllRequestSummaries:(NSNumber *)responseCode responseData:(NSData *)responseData 
{
    NSArray * theArray = nil;
    NSString * tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    if ([responseCode intValue] == 200) {
        
        
        
        if (m_setallNewCategryPins) {
            [m_setallNewCategryPins removeAllObjects];
        }
        m_HowDoesThisLookArray = [tempString JSONValue];
        
        int TotalRecordsHowDoesThisLook = [[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
        totalRecordCountForActiveTag += TotalRecordsHowDoesThisLook;
        NSArray * tmp_fittingRoomSummaryList = [[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
        
        for (int i = 0; i < [tmp_fittingRoomSummaryList count]; i++) {
            NSInteger MessageID = [[[tmp_fittingRoomSummaryList objectAtIndex:i] valueForKey:@"messageId"] intValue];
            [m_setallNewCategryPins addObject:[NSNumber numberWithInt:MessageID]];
        }
       
        if ([m_HowDoesThisLookArray count] > 0) 
        {
            [m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"GetNewCategryPins"];
            [m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"GetNewCategryPins_TotalRecords"];
        }
        else {
            [m_mainDictionary setValue:nil forKey:@"GetNewCategryPins"];
        }
         theArray = [m_mainDictionary objectForKey:@"GetNewCategryPins"];
        
        [self mapviewdrow:theArray];

    }
    else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
    if (tempString!=nil)
    {
        [tempString release];
        tempString=nil;
    }
[l_FittingRoomIndicatorView stopLoadingView];
}


-(void)CallBackMethodForGetRequest:(NSNumber *)responseCode responseData:(NSData *)responseData {		
	NSLog(@"data downloaded");
	[l_FittingRoomIndicatorView stopLoadingView];
	if ([responseCode intValue]==200) 
	{
		NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray *tmp_array=[tempString JSONValue];
		
		
		NSDictionary *tmp_dict=[[tmp_array objectAtIndex:0] valueForKey:@"wsMessage"] ;
		m_type=[tmp_dict valueForKey:@"messageType"];
		l_ViewRequestObj.m_Type=m_type;							// In case of mine, show the i bought it only.
		if (MineCheck==1) 
		{
			l_ViewRequestObj.MineTrack=YES;
			
			
		}
		else {
			l_ViewRequestObj.MineTrack=NO;
			
		}
		
		NSLog(@"response string: %@",tempString);
		if ([m_type caseInsensitiveCompare:kBuyItOrNot] == NSOrderedSame) {
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArrayBuyIt;
			[self btnPicturesAction:nil];
		}
		else if([m_type caseInsensitiveCompare:kIBoughtIt] == NSOrderedSame) {
           [self btnPicturesAction:nil];
			
		}
        else if([m_type caseInsensitiveCompare:kCheckThisOut] == NSOrderedSame) {
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArrayCheckThisOut;
			[self btnPicturesAction:nil];
			
		}
        else if([m_type caseInsensitiveCompare:kHowDoesThisLook] == NSOrderedSame) {
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArrayHowDoesThisLook;
			[self btnPicturesAction:nil];
			
		}
        else if([m_type caseInsensitiveCompare:kFoundASale] == NSOrderedSame) {
			l_ViewRequestObj.m_dataArray = tmp_array;
			l_ViewRequestObj.m_messageIDsArray = m_messageIDsArrayFoundASale;
			[self btnPicturesAction:nil];
			
		}
		if (tempString!=nil)
		{
			[tempString release];
			tempString=nil;
		}
		
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

	
}





-(void)requestCallBackMethodForBuyItOrNot:(NSNumber *)responseCode responseData:(NSData *)responseData {
	
	NSString *tempString;
	tempString=nil;
	if ([responseCode intValue]==200) 
	{
		
		switch (tagValue) 
		{
				
			case 1:// to send request for mine usecase ie get data for mine buy it use case
				NSLog(@"data downloaded");
                if (tempString!=nil)
                {
                    [tempString release];
                    tempString=nil;
                }
				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
				m_buyItArray=[tempString JSONValue];
				NSArray *tmp_fittingRoomSummary=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				if ([m_messageIDsArrayBuyIt count]) {
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummary count]; i++) {
					NSInteger MessageID=[[[tmp_fittingRoomSummary objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBuyIt addObject:[NSNumber numberWithInt:MessageID]];
				}
				
				
				int Count=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += Count;
                
				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"]forKey:@"MineBuyItArray"];// put value in the the dictionary so next time we can check 
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"MineBuyItArray_TotalRecords"];
					
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"MineBuyItArray"];
					
				}
				
					
					
				[self drowPinsonView:@"BuyIt"];
                
				
				break;
				
			case 2:
				// to send request for friends buy it usecase
                if (tempString!=nil)
                {
                    [tempString release];
                    tempString=nil;
                }

				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
				m_buyItArray=[tempString JSONValue];
				NSArray *tmp_fittingRoomSummary1=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				if ([m_messageIDsArrayBuyIt count]) {
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummary1 count]; i++) {
					NSInteger MessageID1=[[[tmp_fittingRoomSummary1 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBuyIt addObject:[NSNumber numberWithInt:MessageID1]];
				}
				int Tmp_Count=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += Tmp_Count;
                
				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"]forKey:@"FriendsBuyItArray"];// put value in the the dictionary so next time we can check 
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FriendsBuyItArray_TotalRecords"];
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"FriendsBuyItArray"];
					
				}
				
                [self drowPinsonView:@"BuyIt"];
				
				
				
				break;
			case 3: // to send request for following buy it usecase
                
                if (tempString!=nil)
                {
                    [tempString release];
                    tempString=nil;
                }

				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
				m_buyItArray=[tempString JSONValue];
				NSArray *tmp_fittingRoomSummary3=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if ([m_messageIDsArrayBuyIt count]) {
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				
				for (int i=0; i<[tmp_fittingRoomSummary3 count]; i++) {
					NSInteger MessageID3=[[[tmp_fittingRoomSummary3 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBuyIt addObject:[NSNumber numberWithInt:MessageID3]];
				}
				int TotalRecordsFollowingBuyIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsFollowingBuyIt;
                
				
				
				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"]forKey:@"FollowingBuyItArray"];// put value in the the dictionary so next time we can check 
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FollowingBuyItArray_TotalRecords"];
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"FollowingBuyItArray"]	;
				}
				
				
					
                [self drowPinsonView:@"BuyIt"];
				
				
				
				break;
			case 4: // for sending nearby request for buy it
                
                if (tempString!=nil)
                {
                    [tempString release];
                    tempString=nil;
                }

                tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
				m_buyItArray=[tempString JSONValue];
				
				NSArray *tmp_fittingRoomSummary5=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				if ([m_messageIDsArrayBuyIt count]) {
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummary5 count]; i++) {
					NSInteger MessageID5=[[[tmp_fittingRoomSummary5 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBuyIt addObject:[NSNumber numberWithInt:MessageID5]];
				}
				int PopularRecordsNearbyBuyIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += PopularRecordsNearbyBuyIt;
                
				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"]forKey:@"PopularBuyItArray"];// put value in the the dictionary so next time we can check 
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularBuyItArray_TotalRecords"];
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"PopularBuyItArray"];
				}
				
					
				[self drowPinsonView:@"BuyIt"];
                break;

				
			
                default:
				break;
		}
	 [l_FittingRoomIndicatorView stopLoadingView];
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

	if (tempString!=nil) 
	{
		[tempString release];
		tempString=nil;
	}		
	
    [l_FittingRoomIndicatorView stopLoadingView];
	
}



-(void)requestCallBackMethodForHowDoesThisLook:(NSNumber *)responseCode responseData:(NSData *)responseData 
{
       
    NSString * tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    if ([responseCode intValue] == 200) {
        
        
        
        if (m_messageIDsArrayHowDoesThisLook) {
            [m_messageIDsArrayHowDoesThisLook removeAllObjects];
        }
        m_HowDoesThisLookArray = [tempString JSONValue];
        
        int TotalRecordsHowDoesThisLook = [[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
        totalRecordCountForActiveTag += TotalRecordsHowDoesThisLook;
        NSArray * tmp_fittingRoomSummaryList = [[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
        
        for (int i = 0; i < [tmp_fittingRoomSummaryList count]; i++) {
            NSInteger MessageID = [[[tmp_fittingRoomSummaryList objectAtIndex:i] valueForKey:@"messageId"] intValue];
            [m_messageIDsArrayHowDoesThisLook addObject:[NSNumber numberWithInt:MessageID]];
        }
        
        
        switch (tagValue) {
                
            case 1:				
               
				if ([m_HowDoesThisLookArray count] > 0) {
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"MineHowDoesThisLookArray"];
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"MineHowDoesThisLookArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"MineHowDoesThisLookArray"];
				}
				
                
               [l_FittingRoomIndicatorView startLoadingView:self type:1];
                
                [m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:1];
                break;
                
            case 2:	
                				
				if ([m_HowDoesThisLookArray count] > 0) {
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FriendsHowDoesThisLookArray"];
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FriendsHowDoesThisLookArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FriendsHowDoesThisLookArray"];
				}
				
				
                 [l_FittingRoomIndicatorView startLoadingView:self type:1];
                [m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:1];
                break;
                
            case 3:				
               
				if ([m_HowDoesThisLookArray count] > 0) {
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FollowingHowDoesThisLookArray"];
					[m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FollowingHowDoesThisLookArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FollowingHowDoesThisLookArray"];
				}
				
				
                 [l_FittingRoomIndicatorView startLoadingView:self type:1];
                [m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:1];
				break;
                
            case 4:
                
                if ([m_HowDoesThisLookArray count] > 0) {
                    [m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"]forKey:@"PopularHowDoesThisLookArray"];
                    [m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularHowDoesThisLookArray_TotalRecords"];
                }
                else {
                    [m_mainDictionary setValue:nil forKey:@"PopularHowDoesThisLookArray"];
                }
                
                 [l_FittingRoomIndicatorView startLoadingView:self type:1];
                [m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForIBoughtIt:responseData:) tempTarget:self userid:m_UserId type:kIBoughtIt number:24 pageno:1];
                break;
            case 5:
                if ([m_HowDoesThisLookArray count] > 0) {
                    [m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"]forKey:@"PopularHowDoesThisLookArray"];
                    [m_mainDictionary setValue:[[m_HowDoesThisLookArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularHowDoesThisLookArray_TotalRecords"];
                }
                else {
                    [m_mainDictionary setValue:nil forKey:@"fittingRoomSummaryList"];
                }
                [l_FittingRoomIndicatorView stopLoadingView];

                break;
        }
        
        [self drowPinsonView:kHowDoesThisLook];
	}
    
    /* The session expires so the user have to login again */
    
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
    if (tempString!=nil) 
	{
		[tempString release];
		tempString=nil;
	}		
   
}


-(void)requestCallBackMethodForCheckThisOut:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSString * tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    if ([responseCode intValue] == 200) {
        
       // tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];	
        
        if (m_messageIDsArrayCheckThisOut) {
            [m_messageIDsArrayCheckThisOut removeAllObjects];
        }
        
        m_CheckThisOutArray = [tempString JSONValue];
        
        int TotalRecordsCheckThisOut = [[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
		
		totalRecordCountForActiveTag += TotalRecordsCheckThisOut;
        
        
        NSArray * tmp_fittingRoomSummaryList = [[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
        
        for (int i = 0; i < [tmp_fittingRoomSummaryList count]; i++) {
            NSInteger MessageID = [[[tmp_fittingRoomSummaryList objectAtIndex:i] valueForKey:@"messageId"] intValue];
            [m_messageIDsArrayCheckThisOut addObject:[NSNumber numberWithInt:MessageID]];
        }
        
        
        
        switch (tagValue) {
                
            case 1:				
				if ([m_CheckThisOutArray count] > 0) {
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"MineCheckThisOutArray"];
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"MineCheckThisOutArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"MineCheckThisOutArray"];
				}
				
               
                
                 [l_FittingRoomIndicatorView startLoadingView:self type:1];
                
				[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:1];
				
				break;
                
            case 2:				

				if ([m_CheckThisOutArray count] > 0) {
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FriendsCheckThisOutArray"];
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FriendsCheckThisOutArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FriendsCheckThisOutArray"];
				}
				
                
                [l_FittingRoomIndicatorView startLoadingView:self type:1];
                
				[m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:1];
				
				break;
                
            case 3:				
				if ([m_CheckThisOutArray count] > 0) {
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FollowingCheckThisOutArray"];
					[m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FollowingCheckThisOutArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FollowingCheckThisOutArray"];
				}
				
                
                // Request to fill How Does This Look? section //
                 [l_FittingRoomIndicatorView startLoadingView:self type:1];
                
				[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:1];
				
				break;
                
            case 4:		
                if ([m_CheckThisOutArray count] > 0) {
                    [m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"]forKey:@"PopularCheckThisOutArray"];
                    [m_mainDictionary setValue:[[m_CheckThisOutArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularCheckThisOutArray_TotalRecords"];
                }
                else {
                    [m_mainDictionary setValue:nil forKey:@"PopularCheckThisOutArray"];
                }
                // Request to fill How Does This Look? section //
               [l_FittingRoomIndicatorView startLoadingView:self type:1];
                [m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForBuyItOrNot:responseData:) tempTarget:self userid:m_UserId type:kBuyItOrNot number:24 pageno:1];
                
                break;
        }
        [self drowPinsonView:kCheckThisOut];
          NSLog(@"the dict value is %@",m_mainDictionary ); 
	}
   
    /* The session expires so the user have to login again */
    
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

	
    if (tempString!=nil) 
	{
		[tempString release];
		tempString=nil;
}

	
}

-(void)requestCallBackMethodForFoundASale:(NSNumber *)responseCode responseData:(NSData *)responseData {
    
    // NSLog(@"Data Downloaded");
	// NSLog(@"Response code is: %d",[responseCode intValue]);
	NSString * tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	// NSLog(@"Response String: %@",tempString);
	
    // Response Code is OK //
    
	if ([responseCode intValue] == 200) {
        
       // tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];	
        
        if (m_messageIDsArrayFoundASale) {
            [m_messageIDsArrayFoundASale removeAllObjects];
        }
        
        m_FoundASaleArray = [tempString JSONValue];
        
        int TotalRecordsFoundASale = [[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
		// Bharat: 11/28/11: US121: Save record count in instance variable
		totalRecordCountForActiveTag += TotalRecordsFoundASale;
        
        
        NSArray * tmp_fittingRoomSummaryList = [[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
        
        for (int i = 0; i < [tmp_fittingRoomSummaryList count]; i++) {
            NSInteger MessageID = [[[tmp_fittingRoomSummaryList objectAtIndex:i] valueForKey:@"messageId"] intValue];
            [m_messageIDsArrayFoundASale addObject:[NSNumber numberWithInt:MessageID]];
        }
        
        
        
        switch (tagValue) {
                
            case 1:				
				
				if ([m_FoundASaleArray count] > 0) {
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"MineFoundASaleArray"];
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"MineFoundASaleArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"MineFoundASaleArray"];
				}
				
				
				
                // Request to fill Found a Sale section //
                [l_FittingRoomIndicatorView startLoadingView:self type:1];
                
				//Bharat : 11/25/11: Chinging the order of icon download
				[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForCheckThisOut:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:1];
				
				break;
                
            case 2:				
				
				if ([m_FoundASaleArray count] > 0) {
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FriendsFoundASaleArray"];
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FriendsFoundASaleArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FriendsFoundASaleArray"];
				}
				
				
                // Request to fill Found a Sale section //
                [l_FittingRoomIndicatorView startLoadingView:self type:1];
                
				//Bharat : 11/25/11: Chinging the order of icon download
				[m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForCheckThisOut:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:1];
				
				break;
                
            case 3:				
				
				if ([m_FoundASaleArray count] > 0) {
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"] forKey:@"FollowingFoundASaleArray"];
					[m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FollowingFoundASaleArray_TotalRecords"];
				}
				else {
					[m_mainDictionary setValue:nil forKey:@"FollowingFoundASaleArray"];
				}
				
				
                // Request to fill Found a Sale section //
                [l_FittingRoomIndicatorView startLoadingView:self type:1];
                
				//Bharat : 11/25/11: Chinging the order of icon download
				[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForCheckThisOut:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:1];
				
				break;
                
            case 4:				
				
				
                if ([m_FoundASaleArray count] > 0) {
                    [m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"]forKey:@"PopularFoundASaleArray"];
                    [m_mainDictionary setValue:[[m_FoundASaleArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularFoundASaleArray_TotalRecords"];
                }
                else {
                    [m_mainDictionary setValue:nil forKey:@"PopularFoundASaleArray"];
                }
                
                
                // Request to fill Found a Sale section //
                [l_FittingRoomIndicatorView startLoadingView:self type:1];
                
				//Bharat : 11/25/11: Chinging the order of icon download
				[m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForCheckThisOut:responseData:) tempTarget:self userid:m_UserId type:kCheckThisOut number:24 pageno:1];
				
                break;
        }
        [self drowPinsonView:kFoundASale];
	}
    
    /* The session expires so the user have to login again */
    
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];

    if (tempString!=nil) 
	{
		[tempString release];
		tempString=nil;
	}		
    
}

-(void)requestCallBackMethodForBadgetNumber:(NSNumber *)responseCode responseData:(NSData *)responseData {
    
	// NSLog(@"data downloaded");
	// NSLog(@"response code is%d",[responseCode intValue]);
	NSString * tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	// NSLog(@"response string: %@",tempString);
	
	if ([responseCode intValue] == 200) {
        NSArray * tmpArray = [[[tempString JSONValue]retain] autorelease];
		NSLog(@"the array is %@",tmpArray);
		l_appDelegate.userInfoDic = [tmpArray objectAtIndex:0];
        
		//NSString * notificationsCount = [NSString stringWithFormat:@"%@", [tmpArray objectAtIndex:0]];
        NSLog(@"the array is %@",[tmpArray objectAtIndex:0]);
		
//        if (tmpArray) {
//            //[notificationButton setBadge:notificationsCount];
//            //self.tabBarController.view.hidden=YES;
//        }
        
        
        switch (tagValue) {
                
            case 1:
                
                [m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:1];
                break;
                
            case 2:
                
                [m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:1];
                break;
                
            case 3:
                
                [m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:1];
                break;
                
            case 4:
                
                [m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForFoundASale:responseData:) tempTarget:self userid:m_UserId type:kFoundASale number:24 pageno:1];
                break;
        }
        
	}
	else 
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
    
    if (tempString!=nil) 
	{
		[tempString release];
		tempString=nil;
	}		
	
	
}
-(void)requestCallBackMethodForIBoughtIt:(NSNumber *)responseCode responseData:(NSData *)responseData {
	NSString *tempString;
	tempString=nil;
	
	if ([responseCode intValue] == 200) {
        NSUserDefaults * prefs = [NSUserDefaults standardUserDefaults];
        NSString * custID = [prefs objectForKey:@"userName"];
		switch (tagValue) 
		{
			case 1:
                if (tempString!=nil)
                {
                    [tempString release];
                    tempString=nil;
                }

				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];			
				m_buyItArray=[tempString JSONValue];
				int TotalRecordsMineBoughtIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsMineBoughtIt;
                
				
				NSArray *tmp_fittingRoomSummaryBoughtIt=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if (m_messageIDsArrayBoughtIt) { // remove the previous objects in the aray.
					[m_messageIDsArrayBoughtIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummaryBoughtIt count]; i++) // Fill the message ids for all the records in an array.
				{
					NSInteger MessageID=[[[tmp_fittingRoomSummaryBoughtIt objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBoughtIt addObject:[NSNumber numberWithInt:MessageID]];
				}				
				
				
				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"] forKey:@"MineBoughtItArray"];
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecord"] forKey:@"MineBoughtItArray_TotalRecords"];
				}
				else 
				{   
                    
                    
					[m_mainDictionary setValue:nil forKey:@"MineBoughtItArray"];
					
				}
				
                if ([m_UserId length] <= 0) {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                else {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
				
				break;
				
			case 2:
                if (tempString!=nil)
                {
                    [tempString release];
                    tempString=nil;
                }

				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];			
				m_buyItArray=[tempString JSONValue];
				int TotalRecordsFriendsBoughtIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsFriendsBoughtIt;
                
				
				NSArray *tmp_fittingRoomSummaryBoughtIt1=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if (m_messageIDsArrayBoughtIt) { // remove the previous objects in the aray.
					[m_messageIDsArrayBoughtIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummaryBoughtIt1 count]; i++) // Fill the message ids for all the records in an array.
				{
					NSInteger MessageID=[[[tmp_fittingRoomSummaryBoughtIt1 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBoughtIt addObject:[NSNumber numberWithInt:MessageID]];
				}				
				
				
				if ([m_buyItArray count]>0) 
				{
					
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"] forKey:@"FriendsBoughtItArray"];
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FriendsBoughtItArray_TotalRecords"];	
				}
				else 
				{
                    
					[m_mainDictionary setValue:nil forKey:@"FriendsBoughtItArray"];
					
				}
		
				//[l_FittingRoomIndicatorView stopLoadingView];
                
                if ([m_UserId length] <= 0) {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                else {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                
				break;
				
				
			case 3:
                if (tempString!=nil)
                {
                    [tempString release];
                    tempString=nil;
                }

				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];			
				m_buyItArray=[tempString JSONValue];
				int TotalRecordsFollowingBoughtIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsFollowingBoughtIt;
                
				
				NSArray *tmp_fittingRoomSummaryBoughtIt2=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if (m_messageIDsArrayBoughtIt) { // remove the previous objects in the aray.
					[m_messageIDsArrayBuyIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummaryBoughtIt2 count]; i++) // Fill the message ids for all the records in an array.
				{
					NSInteger MessageID=[[[tmp_fittingRoomSummaryBoughtIt2 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBoughtIt addObject:[NSNumber numberWithInt:MessageID]];
				}			
				
				if ([m_buyItArray count]>0)
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"] forKey:@"FollowingBoughtItArray"];
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"FollowingBoughtItArray_TotalRecords"];
				}
				else 
				{   
                    
                    
					[m_mainDictionary setValue:nil forKey:@"FollowingBoughtItArray"];
					
				}
				
				                
                if ([m_UserId length] <= 0) {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                else {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
				
				break;
			case 4:
                
                if (tempString!=nil)
                {
                    [tempString release];
                    tempString=nil;
                }

				tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];			
				m_buyItArray=[tempString JSONValue];
				int TotalRecordsPopularBoughtIt=[[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] intValue];
				// Bharat: 11/28/11: US121: Save record count in instance variable
				totalRecordCountForActiveTag += TotalRecordsPopularBoughtIt;
                
				
				NSArray *tmp_fittingRoomSummaryBoughtIt4=[[m_buyItArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
				
				if (m_messageIDsArrayBoughtIt) { // remove the previous objects in the aray.
					[m_messageIDsArrayBoughtIt removeAllObjects];
				}
				for (int i=0; i<[tmp_fittingRoomSummaryBoughtIt4 count]; i++) // Fill the message ids for all the records in an array.
				{
					NSInteger MessageID=[[[tmp_fittingRoomSummaryBoughtIt4 objectAtIndex:i] valueForKey:@"messageId"] intValue];
					[m_messageIDsArrayBoughtIt addObject:[NSNumber numberWithInt:MessageID]];
				}			
				
				
				
				if ([m_buyItArray count]>0) 
				{
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0]valueForKey:@"fittingRoomSummaryList"] forKey:@"PopularBoughtItArray"];
					[m_mainDictionary setValue:[[m_buyItArray objectAtIndex:0] valueForKey:@"totalRecords"] forKey:@"PopularBoughtItArray_TotalRecords"];
				}
				else 
				{
					[m_mainDictionary setValue:nil forKey:@"PopularBoughtItArray"];
				}
				
				//[l_FittingRoomIndicatorView stopLoadingView];
                
                if ([m_UserId length] <= 0) {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
                else {
                    [m_requestObj getNotificationsCount:@selector(requestCallBackMethodForBadgetNumber:responseData:) tempTarget:self customerid:custID];            
                }
				
				
				break;

				
			
            default:
				break;
		}
        [self drowPinsonView:kIBoughtIt];
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	
	if (tempString!=nil) 
	{
		[tempString release];
		tempString=nil;
	}
    
}


#pragma mark Custom Button Method
-(IBAction)sendRequestforAParticularBtn:(id)sender
{
    if (sender == nil) {
		tagValue=5;//for the first time when the view is loaded
	}
	else {
		tagValue=[sender tag];	
	}
    if (messageId!=nil)
    {
        [messageId  release];
        messageId=nil;
    }
    m_headerLBL.hidden=YES;
    m_headerIMG.hidden=NO;
    m_headerLBL.text=@"";
    messageId=[[NSMutableArray alloc]init];
    
    if (annotations!=nil)
    {
        [annotations release];
        annotations=nil;
    }
    annotations=[[NSMutableArray alloc] init];
    if (m_mapView.annotations >0)
    {
        NSArray *lastAnnonation=m_mapView.annotations;
        [self.m_mapView removeAnnotations:lastAnnonation];
    }
    annonationCount=0;
    int recordsCount;
	switch (tagValue) {
            
		case 1:
            //Mine button
						
			
			m_MineBtn.selected=YES;
			m_FriendsBtn.selected=NO;
			m_FollowingBtn.selected=NO;
			m_PopularBtn.selected=NO;
			
           [l_FittingRoomIndicatorView startLoadingView:self type:1];
          
			[m_requestObj getUnreadRequestsForMine:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:1];
            
            recordsCount = [[m_mainDictionary valueForKey:@"MineHowDoesThisLookArray_TotalRecords"] intValue];
            NSLog(@"thje count value is %d",recordsCount);
            NSLog(@"the response data is %@",m_mainDictionary);
            
			break;
			
		case 2:
            m_MineBtn.selected=NO;
			m_FriendsBtn.selected=YES;
			m_FollowingBtn.selected=NO;
			m_PopularBtn.selected=NO;
           // [self mapviewdrow];
           
			[l_FittingRoomIndicatorView startLoadingView:self type:1];
			
            [m_requestObj getUnreadRequestsForFriends:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:1];
			
            recordsCount = [[m_mainDictionary valueForKey:@"FriendsHowDoesThisLookArray_TotalRecords"] intValue];
            NSLog(@"thje count value is %d",recordsCount);
            NSLog(@"the response data is %@",m_mainDictionary);
            break;
		case 3:
            
            m_MineBtn.selected=NO;
			m_FriendsBtn.selected=NO;
			m_FollowingBtn.selected=YES;
			m_PopularBtn.selected=NO;
           // [self mapviewdrow];
         [l_FittingRoomIndicatorView startLoadingView:self type:1];
			[m_requestObj getUnreadRequestsForFollowing:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:1];
            
            recordsCount = [[m_mainDictionary valueForKey:@"FollowingHowDoesThisLookArray_TotalRecords"] intValue];
            NSLog(@"thje count value is %d",recordsCount);
            NSLog(@"the response data is %@",m_mainDictionary);
            
			break;
		case 4:
           		
            m_MineBtn.selected=NO;
			m_FriendsBtn.selected=NO;
			m_FollowingBtn.selected=NO;
			m_PopularBtn.selected=YES;
          //  [self mapviewdrow];
        [l_FittingRoomIndicatorView startLoadingView:self type:1];
			
			[m_requestObj getUnreadRequestsForPopular:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self userid:m_UserId type:kHowDoesThisLook number:24 pageno:1];
            
            recordsCount = [[m_mainDictionary valueForKey:@"PopularHowDoesThisLookArray_TotalRecords"] intValue];
            NSLog(@"thje count value is %d",recordsCount);
            NSLog(@"the response data is %@",m_mainDictionary);
            
			break;
        case 5:
        {
//            -(void)getNearStories:(SEL)tempSelector tempTarget:(id)tempTarget Lattitude:(double) lattitude Longitude:(double) longitude Count:(int) count

            m_MineBtn.selected=NO;
			m_FriendsBtn.selected=NO;
			m_FollowingBtn.selected=NO;
			m_PopularBtn.selected=YES;
            //  [self mapviewdrow];
            [l_FittingRoomIndicatorView startLoadingView:self type:1];
			
			[m_requestObj getNearStories:@selector(requestCallBackMethodForHowDoesThisLook:responseData:) tempTarget:self Lattitude: [[self currLocation] coordinate].latitude  Longitude:[[self currLocation] coordinate].longitude  Count:count];
            
            recordsCount = [[m_mainDictionary valueForKey:@"PopularHowDoesThisLookArray_TotalRecords"] intValue];
            NSLog(@"the count value is %d",recordsCount);
            NSLog(@"the response data is %@",m_mainDictionary);
        }
            break;
		default:
            
			break;
	}
    

    
}
-(IBAction)AddToCatagBtn:(id)sender
{
    ChooseCategories *aController = [[ChooseCategories alloc] initWithNibName:@"ChooseCategory" bundle:[NSBundle mainBundle]];
    
    if (self.selectedType) {
        aController.currentRow = [[Context getInstance].categories indexOfObject:self.selectedType];
    } else {
        aController.currentRow = -1;
    }
    aController.controller = self;
    [self.navigationController pushViewController:aController animated:YES];
    
    [aController release];

}
- (void) saveSelectedCategory:(NSString *)type
{
    self.selectedType = type;
    
    if (type) {
//        [self.AddToCatagBtn setTitle:[[Context getInstance] getDisplayTextFromMessageType:type] forState:UIControlStateNormal];
        if (messageId!=nil)
        {
            [messageId  release];
            messageId=nil;
        }
        
        messageId=[[NSMutableArray alloc]init];
        annonationCount=0;
        if (annotations!=nil)
        {
            [annotations release];
            annotations=nil;
        }
        annotations=[[NSMutableArray alloc] init];
        if (m_mapView.annotations >0)
        {
            NSArray *lastAnnonation=m_mapView.annotations;
            [self.m_mapView removeAnnotations:lastAnnonation];
        }
        
        m_MineBtn.selected=NO;
        m_FriendsBtn.selected=NO;
        m_FollowingBtn.selected=NO;
        m_PopularBtn.selected=NO;
        
        
        m_headerLBL.hidden=NO;
        m_headerIMG.hidden=YES;
        m_headerLBL.text=type;
        
        [l_FittingRoomIndicatorView startLoadingView:self type:1];
        [m_requestObj getUnreadRequestsForPopular:@selector(getAllRequestSummaries:responseData:) tempTarget:self userid:m_UserId type:type number:24 pageno:1];
        
        
    } else {
        
        m_headerLBL.hidden=YES;
        m_headerIMG.hidden=NO;

//        [self.AddToCatagBtn setTitle:@"Select Category" forState:UIControlStateNormal] ;
       
    }
   
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.m_FollowingBtn setHidden:YES];
    [self.m_FriendsBtn setHidden:YES];
    [self.m_MineBtn setHidden:YES];
    [self.m_PopularBtn setHidden:YES];
    [self.AddToCatagBtn setHidden:YES];

    [self.m_mapView setFrame:CGRectMake(0, 50, 320, 430)];

    if ([CLLocationManager locationServicesEnabled]) {
        
        locationManager = [[CLLocationManager alloc] init];
        [locationManager setDelegate:self];

        [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        [locationManager startUpdatingLocation];
    }
    
    [m_mapView setShowsUserLocation:YES];
    [self findLocation];
//    [self sendRequestforAParticularBtn:nil];
	
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.m_MineBtn = nil;
    self.m_FriendsBtn = nil;
    self.m_FollowingBtn = nil;
    self.m_PopularBtn = nil;
    self.m_mapView=nil;
    self.AddToCatagBtn=nil;
    self.m_headerIMG=nil;
    self.m_headerLBL=nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)dealloc
{
    [m_MineBtn release];
    [m_FriendsBtn release];
    [m_FollowingBtn release];
    [m_PopularBtn release];
    [m_mapView release];
    if (annotations!=nil)
    {
        [annotations release];
        annotations=nil;
    }
    [m_requestObj release];
    [m_messageIDsArrayHowDoesThisLook release];
    [m_mainDictionary release];
    [m_messageIDsArrayFoundASale release];
    [m_messageIDsArrayCheckThisOut release];
    [m_messageIDsArrayBoughtIt release];
    if (messageId!=nil)
    {
        [messageId  release];
        messageId=nil;
    }
    [AddToCatagBtn release];
    [m_setallNewCategryPins release];
     self.selectedType = nil;
    [m_headerLBL release];
    [m_headerIMG release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{

    CLLocationCoordinate2D loc2D = [userLocation coordinate];
    
    CLLocation *loc = [userLocation location];
    
    [self setCurrLocation:loc];
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc2D, 16000, 16000);
    [m_mapView setRegion:region animated:YES];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Could not find location: %@", error);
}

-(void) findLocation
{
    [locationManager startUpdatingLocation];
}
-(void) foundLocation:(CLLocation *)loc
{
    [self setCurrLocation:loc];
    CLLocationCoordinate2D coord = [loc coordinate];
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord, 25000, 25000);
//    [m_mapView setRegion:region animated:YES];
    [locationManager stopUpdatingLocation];
    NSLog(@"changed location to : %10.6f, %10.2f", coord.latitude, coord.longitude);
    

}
-(void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSTimeInterval t = [[newLocation timestamp] timeIntervalSinceNow];
    if (t < -180)
    {
        [self setCurrLocation:newLocation];
        return;
    }
    [self foundLocation:newLocation];
}

@end
