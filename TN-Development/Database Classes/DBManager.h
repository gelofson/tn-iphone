//
//  DBManager.h
//
//  Created by Soft Prodigy
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QNavigatorAppDelegate.h"
//#import "CategoryModelClass.h"
//#import"RegistrationViewController.h"
#import"ProfileinfoSettingViewController.h"
#include <sqlite3.h>

@interface DBManager : NSObject 
{
	sqlite3 *database;
	NSMutableArray *arrSearchHistory;
	NSMutableArray *arrFavorites;
	//CategoryModelClass *m_objCategory;
	
	//Array to contain list of Insee data
	NSMutableArray *arrInsee;
}

//@property(nonatomic,retain)CategoryModelClass *m_objCategory;
@property(nonatomic,retain)NSMutableArray *arrSearchHistory;
@property(nonatomic,retain)NSMutableArray *arrFavorites;
@property(nonatomic,retain)NSMutableArray *arrInsee;

//Database connection and initialization methods
-(BOOL) CloseDatabase;
-(BOOL) InitializeDB;
-(void)createEditableCopyOfDatabaseIfNeeded;
-(void)deleteCategoryData:(int)temp_catIndex;
-(NSMutableArray *)GetCategoriesData:(int)temp_catIndex;
-(NSInteger) insertCategoryRecord:(int)categoryNumber;
-(BOOL)checkForAlreadyInsertedData:(int)temp_catIndex;
-(void)flushDB;
-(void)insertRegistrationData:(NSDictionary *) m_Databasedict  ;
-(NSDictionary *) getRegistrationData:(NSDictionary *) m_getdatabaseData; 
-(void) insertProfileData:(NSDictionary *)m_ProfileInfoDict;
-(NSDictionary *) getProfileInfo;


+ (id) SharedInstance ;

@end
