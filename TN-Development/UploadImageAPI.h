//
//  UploadImageAPI.h
//  QNavigator
//
//  Created by softprodigy on 23/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UploadImageAPI : NSObject 
{
	NSURLConnection *m_theConnection;
	NSMutableData *m_mutResponseData;
	
	int m_intResponseCode;
	int requestType;

}

@property (nonatomic,retain) NSURLConnection *m_theConnection;
@property int requestType;

+ (id) SharedInstance ;
-(void) updateUserPic:(NSDictionary *)updateDictionary;

-(NSString *)sendBuzzRequest:(NSDictionary*)m_dictionary;



@end
