//
//  NewMapViewController.m
//  TinyNews
//
//  Created by jay kumar on 11/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NewMapViewController.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "DetailPageViewController.h"
#import "LoadingIndicatorView.h"
#import "CategoryTypeSeverRequest.h"
#import "ChooseCategories.h"
#import "NewsDataManager.h"
#import "UIImage+ViewImage.h"
#import <QuartzCore/QuartzCore.h>
#import "MoreButton.h"
#import "CategoryData.h"
#import "ShopbeeAPIs.h"
#import <QuartzCore/QuartzCore.h>
#import "CategoryData.h"



#define METERS_PER_MILE 1609.344

@implementation NewMapViewController
@synthesize m_data;
@synthesize m_mapView;
@synthesize selectedType;
@synthesize searchText;
@synthesize categoryBTN;
@synthesize moreButton;
@synthesize helpView;
@synthesize  closeByn;
@synthesize headerLBL;
@synthesize singleTapOnPin;
@synthesize singleTapOnheadline;
@synthesize firstDoubletap;
@synthesize secondDoubletap;
@synthesize headerImage;
@synthesize headertextLBL;
CategoryTypeSeverRequest *m_request;

int count;
int MapClickCount;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [m_mapView setMapType:MKMapTypeStandard];
    //m_mapView.userLocation
    
    zoomFlag=NO;
    headertextLBL.hidden=NO;
    headerImage.hidden=YES;
    serachTaG=NO;
    annonationCount=0;
    clickCount=0;
    moreButton.hidden=NO;
    [self show];
    if (messageId!=nil)
    {
        [messageId  release];
        messageId=nil;
    }
    messageId=[[NSMutableArray alloc]init];
    if (annotations!=nil)
    {
        [annotations release];
        annotations=nil;
    }
    annotations=[[NSMutableArray alloc] init];
    self.selectedType=@"Breaking News";
    headertextLBL.text=@"Trending news";
    count=1;
    currentRow=-1;
    m_request=[[CategoryTypeSeverRequest alloc]init];
    m_listArray=[[NSMutableArray alloc]init];
    [[LoadingIndicatorView SharedInstance] startLoadingView:self];
    [[NewsDataManager sharedManager] getOneCategoryData:self type:self.selectedType filter:POPULAR number:24 numPage:count addToExisting:NO];
    UILongPressGestureRecognizer *m_mapViewtap=[[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleGestureonMapViewDoubleTap:)]autorelease];
    m_mapViewtap.delegate=self;
    m_mapViewtap.minimumPressDuration=1.0f;
    [self.m_mapView.subviews[0] addGestureRecognizer:m_mapViewtap];
    
    
    // Do any additional setup after loading the view from its nib.
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    // 1
//    CLLocationCoordinate2D zoomLocation;
//    zoomLocation.latitude = 39.281516;
//    zoomLocation.longitude= -76.580806;
//    
//    // 2
//    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
//    // 3
//    MKCoordinateRegion adjustedRegion = [m_mapView regionThatFits:viewRegion];
//    // 4
//    [m_mapView setRegion:adjustedRegion animated:YES];
    
}



#pragma mark- Cumtom Button Method
-(IBAction)getCategoryList:(id)sender
{
    UITableView *tableView=(UITableView*)[self.view viewWithTag:11];
    
    if (tableView)
    {
        [UIView animateWithDuration:.25 animations:^{
            tableView.alpha=0.0;
        }];
        [UIView commitAnimations];
        
        [tableView removeFromSuperview];
        return;
        
    }
    
    CGRect frame=[self.categoryBTN frame];
    UITableView *categoryTable=[[UITableView alloc]initWithFrame:CGRectMake(120, frame.origin.y+frame.size.height+5, 162, 360) style:UITableViewStylePlain];
    categoryTable.alpha = 0.0;
    
    [UIView animateWithDuration:.25 animations:^{
        categoryTable.alpha=1.0;
    }];
    [UIView commitAnimations];
    [categoryTable setTag:11];
    [categoryTable setDelegate:self];
    [categoryTable setDataSource:self];
    [categoryTable setBackgroundView:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cat-menu-bgrd.png"]] autorelease]];
    categoryTable.layer.cornerRadius=5.0f;
    [self.view addSubview:categoryTable];
    [categoryTable release];
    
}



-(IBAction)HelpClicked:(id)sender
{
    [UIView animateWithDuration:.25 animations:^{
        self.helpView.alpha=1.0;
        self.closeByn.alpha=1.0;
        self.headerLBL.alpha=1.0;
        self.singleTapOnPin.alpha=1.0;
        self.singleTapOnheadline.alpha=1.0;
        self.firstDoubletap.alpha=1.0;
        self.secondDoubletap.alpha=1.0;
    }];
        [self show];

    [UIView commitAnimations];
    
    
}
-(IBAction)closeHelp:(id)sender
{
    [UIView animateWithDuration:.25 animations:^{
        self.helpView.alpha=0.0;
        self.closeByn.alpha=0.0;
        self.headerLBL.alpha=0.0;
        self.singleTapOnPin.alpha=0.0;
        self.singleTapOnheadline.alpha=0.0;
        self.firstDoubletap.alpha=0.0;
        self.secondDoubletap.alpha=0.0;
    }];
    [self hide];
    [UIView commitAnimations];
    
}
-(void)show
{
    self.helpView.hidden=NO;
    self.closeByn.hidden=NO;
    self.headerLBL.hidden=NO;
    self.singleTapOnPin.hidden=NO;
    self.singleTapOnheadline.hidden=NO;
    self.firstDoubletap.hidden=NO;
    self.secondDoubletap.hidden=NO;
}
-(void)hide
{
    self.helpView.hidden=YES;
    self.closeByn.hidden=YES;
    self.headerLBL.hidden=YES;
    self.singleTapOnPin.hidden=YES;
    self.singleTapOnheadline.hidden=YES;
    self.firstDoubletap.hidden=YES;
    self.secondDoubletap.hidden=YES;

}


-(IBAction)addMoreClicked
{
    if (MapClickCount>0)
    {
        clickCount=0;
        for (int i=0; i<MapClickCount; i++)
        {
            UIImageView *bubbleimage=(UIImageView *)[self.view viewWithTag:i+100];
            [bubbleimage removeFromSuperview];
        }
        
        
    }
    if (count + 1 <= pageCount)
    {
        count++;
//        for (id<MKAnnotation> annotation in annotations)
//        {
//            
//            MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
//            MKMapRect zoomRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
//            [m_mapView setVisibleMapRect:zoomRect animated:YES];
//        }
        
        annonationCount=0;
        if (annotations!=nil)
        {
            [annotations release];
            annotations=nil;
        }
        annotations=[[NSMutableArray alloc] init];
//        if (m_mapView.annotations >0)
//        {
//            NSArray *lastAnnonation=m_mapView.annotations;
//            [self.m_mapView removeAnnotations:lastAnnonation];
//        }
        
        for (id<MKAnnotation> annotation in self.m_mapView.annotations) {
            [self.m_mapView removeAnnotation:annotation];
        }

        
        if (messageId!=nil)
        {
            [messageId  release];
            messageId=nil;
        }
        messageId=[[NSMutableArray alloc]init];

        [[LoadingIndicatorView SharedInstance] startLoadingView:self];
        [[NewsDataManager sharedManager] getOneCategoryData:self type:self.selectedType filter:POPULAR number:24 numPage:count addToExisting:YES];
        return;
    }
    
}
-(void)CallBAckMethodRange:(NSNumber*)responseCode rasponseData:(NSData*)responseData
{
    NSString *str=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    [[LoadingIndicatorView SharedInstance] stopLoadingView];
    
    if ([responseCode intValue]==200)
    {
        
        m_listArray =[str JSONValue];
        
        if ([m_listArray count]>0)
        {
            NSLog(@"list array count %d",[m_listArray count]);
            [self drowPinsforcategorysearch:m_listArray];
        }
        else
        {
            UIAlertView *alartView=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Sorry, no match" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alartView show];
            [alartView release];
        }
        
    }
    else
    {
        UIAlertView *alartView=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Sorry, no match" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alartView show];
        [alartView release];
    }
    
    [str release];
    str =nil;
    
    
}
-(void)drowPinsforcategorysearch:(NSArray*)serchArray;
{
    [[LoadingIndicatorView SharedInstance] stopLoadingView];
    for (int i=0; i<[serchArray count]; i++)
    {
        
        NSDictionary *dict=[serchArray objectAtIndex:i];
//        if ([[dict valueForKey:@"latitude"]doubleValue]==0 ||[[dict valueForKey:@"longitude"]doubleValue]==0)
//        {
//            
//        }
        //else{
            CLLocationCoordinate2D theLocation;
            MyAnnotation *myannotation=[[MyAnnotation alloc]init];
        
        theLocation.latitude=[[dict valueForKey:@"latitude"]doubleValue];
        theLocation.longitude=[[dict valueForKey:@"longitude"]doubleValue];
        
        myannotation.title=[dict valueForKey:@"headline"];
        myannotation.coordinate=theLocation;
        [annotations addObject:myannotation];
        [messageId addObject:[dict valueForKey:@"messageId"]];
        [m_mapView addAnnotation:myannotation];
        [myannotation release];
            
        //}
                    
        
    }
    if ([annotations count]==0)
    {
        UIAlertView *alartView=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Sorry, no match" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alartView show];
        [alartView release];
    }
    
    
    MKMapRect flyTo=MKMapRectNull;
    
    for (id<MKAnnotation> annotation in annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.5, 0.5);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        }
        else
        {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
    // m_mapView.visibleMapRect = flyTo;
    [m_mapView setVisibleMapRect:flyTo  animated:YES];

    
}
- (void) updateData

{
    [[LoadingIndicatorView SharedInstance] stopLoadingView];
    CategoryData *data = [[NewsDataManager sharedManager] getCategoryDataByType:self.selectedType];
    if (data) {
        self.m_data=data;
        pageCount=data.numPages;
        NSLog(@"fitting room category data %d %d" ,[data.categoryData count],pageCount);
        for (int i=0; i<[data.categoryData count]; i++)
        {
            NSDictionary *aData = [data messageData:i];
            NSLog(@"record date is %@" ,aData);
            
            if ([[aData valueForKey:@"latitude"]doubleValue]==0 ||[[aData valueForKey:@"longitude"]doubleValue]==0)
            {
                
            }
            else{
            CLLocationCoordinate2D theLocation;
            MyAnnotation *myannotation=[[MyAnnotation alloc]init];
            double latitude=[[aData valueForKey:@"latitude"]doubleValue];
            double longitude=[[aData valueForKey:@"longitude"]doubleValue];
            theLocation.latitude=latitude;
            theLocation.longitude=longitude;
            myannotation.title=[aData valueForKey:@"headline"];
            myannotation.coordinate=theLocation;
            [annotations addObject:myannotation];
            [messageId addObject:[aData valueForKey:@"messageId"]];
            [self.m_mapView addAnnotation:myannotation];
            [myannotation release];
                
        }
            
        }
        
        if ([annotations count]==0)
        {
            UIAlertView *alartView=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Sorry, no match" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alartView show];
            [alartView release];
        }
    }
    
    
    else
    {
        UIAlertView *alartView=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Sorry, no match" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alartView show];
        [alartView release];
        
    }
    
    MKMapRect flyTo=MKMapRectNull;
    
    for (id<MKAnnotation> annotation in annotations)
    {
        
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.5, 0.5);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        }
        else
        {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
    [m_mapView setVisibleMapRect:flyTo animated:YES];
    
    
    [self enableMore];
}


- (void) enableMore
{
    BOOL enableMore = count < m_data.numPages;
    self.moreButton.hidden = !enableMore;
}

#pragma mark MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
	
	// if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
	// try to dequeue an existing pin view first
	static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
	MKPinAnnotationView* pinView = [[[MKPinAnnotationView alloc]
									 initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier] autorelease];
	pinView.animatesDrop=NO;
	pinView.canShowCallout=YES;
    pinView.image=[UIImage imageNamed:@"marker-icon.png"];
    
    
    //NSLog(@"pin id is %d",[[messageId objectAtIndex:annonationCount] intValue]);

    pinView.tag=[[messageId objectAtIndex:annonationCount] intValue];
    
	pinView.pinColor=MKPinAnnotationColorRed;
    
    UIButton *rightAcceryBtn=[UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [rightAcceryBtn setTag:annonationCount+1];
    [rightAcceryBtn addTarget:self action:@selector(moveToDetailPage:) forControlEvents:UIControlEventTouchDown];
    pinView.rightCalloutAccessoryView   =rightAcceryBtn;
    annonationCount++;
    
	return pinView;
}
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{


}

- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView
{
    
}
- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{

}
- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error
{

}



-(void)moveToDetailPage:(id)sender
{
    
    int BTNTag=[sender tag];
    int pinId=[[messageId objectAtIndex:BTNTag-1] intValue];
    NSLog(@"the pin value is %d",pinId);
    
    if (serachTaG)
    {
        
        NSUserDefaults *userdeft=[NSUserDefaults standardUserDefaults];
        NSString *userName=[userdeft objectForKey:@"userName"];
        [[LoadingIndicatorView SharedInstance] startLoadingView:self];
        ShopbeeAPIs * api = [[ShopbeeAPIs alloc] init];
        [api getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:userName messageId:pinId];
        [api release];
        
    }
    else{
        DetailPageViewController *detailController = [[[DetailPageViewController alloc] init] autorelease];
        detailController.m_data = self.m_data;
        detailController.m_Type = self.m_data.type;
        detailController.m_messageId = pinId;
        
        detailController.filter = POPULAR;
        
        detailController.mineTrack = NO;
        detailController.curPage = 1;
        detailController.m_CallBackViewController = self;
        [self.navigationController pushViewController:detailController animated:YES];
    }
    
    
    
}
- (void)CallBackMethodForGetRequest:(NSNumber*)responseCode responseData:(NSData *)responseData {
    [[LoadingIndicatorView SharedInstance] stopLoadingView];
	NSLog(@"PushNotificationViewController:CallBackMethodForGetRequest:responseCode=[%d], data=[%@]",
          [responseCode intValue], [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
    
	if ([responseCode intValue] == 200) {
        
		NSString * tempString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		NSArray * tmp_array = [tempString JSONValue];
        
        NSDictionary *tmp_dict = [[tmp_array objectAtIndex:0] valueForKey:@"wsMessage"];
        
        NSLog(@"tempString %@", tempString);
		// Bharat: 11/22/11: original message id check
		NSString * originalMessageId = [tmp_dict valueForKey:@"originalMessageId"];
		if ((originalMessageId != nil) && ([originalMessageId length] > 0)) {
            NSUserDefaults * userPreferences = [NSUserDefaults standardUserDefaults];
            NSString * userName = [userPreferences stringForKey:@"userName"];
            ShopbeeAPIs * api = [[ShopbeeAPIs alloc] init];
            [api getDataForParticularRequest:@selector(CallBackMethodForGetRequest:responseData:) tempTarget:self userid:userName messageId:[originalMessageId intValue]];
            [api release];
			return;
		}
        //No data Available by this message Id
        if ([tmp_array count]>0)
        {
            CategoryData *catData = [CategoryData convertMessageData:[tmp_array objectAtIndex:0]];
            
            DetailPageViewController *detailController = [[[DetailPageViewController alloc] init] autorelease];
            detailController.m_data = catData;
            detailController.m_Type = catData.type;
            detailController.m_messageId = [[tmp_dict objectForKey:@"id"] intValue];
            detailController.filter = POPULAR;
            detailController.mineTrack = NO;
            detailController.curPage = 1;
            detailController.m_CallBackViewController = self;
            [self.navigationController pushViewController:detailController animated:YES];
        }
        else
        {
            UIAlertView *alart=[[UIAlertView alloc]initWithTitle:@"Error Message" message:@"No data Available For this pin" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alart show];
            [alart release];
            
        }
        [tempString release];
        tempString = nil;
        
		
	}
	else
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	
	
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
-(void)handleGestureonMapViewDoubleTap:(UIGestureRecognizer*)mapViewTap
{
    
    
    if (mapViewTap.state == UIGestureRecognizerStateEnded)
        return;
    
    MapClickCount=0;
    for (id<MKAnnotation> annotation in annotations)
    {
        if ([annotation.title length]>=1)
        {
            if (clickCount%2==0)
            {
                
                MKPinAnnotationView *pinView=(MKPinAnnotationView *)[self.view viewWithTag:[[messageId objectAtIndex:MapClickCount]intValue]];
                UIImageView *bubbleimage=[[[UIImageView alloc]initWithFrame:CGRectMake(-15, -45,100 , 45)] autorelease];
                [bubbleimage setImage:[UIImage imageNamed:@"popup.png"]];
                bubbleimage.tag=MapClickCount+100;
                
                UILabel *headertexLBL=[[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 35)]autorelease];
                [headertexLBL setBackgroundColor:[UIColor clearColor]];
                [headertexLBL setFont:[UIFont boldSystemFontOfSize:12.0f]];
                [headertexLBL setText:annotation.title];
                [headertexLBL setNumberOfLines:0];
                [headertexLBL setTextAlignment:UITextAlignmentCenter];
                [headertexLBL setTextColor:[UIColor whiteColor]];
                
                [bubbleimage addSubview:headertexLBL];
                [pinView addSubview:bubbleimage];
            }
            else
            {
                UIImageView *bubbleimage=(UIImageView *)[self.view viewWithTag:MapClickCount+100];
                [bubbleimage removeFromSuperview];
            }
            
        }
        MapClickCount++;
    }
    
    clickCount++;
}

#pragma mark- tableView Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return [[NewsDataManager sharedManager] countAllCategories];
    return [[NewsDataManager sharedManager] countActiveCategories];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CategoryCellIdentifier = @"CategoryCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CategoryCellIdentifier];
    if (!cell)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CategoryCellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    //    cell.textLabel.text = [[NewsDataManager sharedManager] getAllCategoryIdAtIndex:indexPath.row];
    NSString *titleString = [[NewsDataManager sharedManager] getCategoryIdAtIndex:indexPath.row];
    
    cell.textLabel.text = titleString;
    
    NSString *convertedName = [[NewsDataManager sharedManager] getCategoryNewName:titleString];
    
    if (convertedName)
        cell.textLabel.text = convertedName;
    
    cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:15.0f];
    
    cell.selectedBackgroundView=[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu-selected.png"]]autorelease];
    if (currentRow == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)atableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    for (id<MKAnnotation> annotation in annotations)
//    {
//        
//        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
//        MKMapRect zoomRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
//        [m_mapView setVisibleMapRect:zoomRect animated:YES];
//    }

    serachTaG=NO;
    //self.searchText.text=@"";
    clickCount=0;
    moreButton.hidden=NO;
    count=1;
    currentRow = indexPath.row;
    [atableView reloadData];
    
    
    headertextLBL.hidden=NO;
    headerImage.hidden=YES;
    
    
    NSString *titleString = [[NewsDataManager sharedManager] getCategoryIdAtIndex:indexPath.row];
    self.headertextLBL.text = titleString;
    
    NSString *convertedName = [[NewsDataManager sharedManager] getCategoryNewName:titleString];
    
    if (convertedName)
    self.headertextLBL.text = convertedName;
    
    
    //[headertextLBL setText:[[NewsDataManager sharedManager] getAllCategoryIdAtIndex:currentRow]];
    annonationCount=0;
    if (annotations!=nil)
    {
        [annotations release];
        annotations=nil;
    }
    annotations=[[NSMutableArray alloc] init];
//    if (m_mapView.annotations >0)
//    {
//        NSArray *lastAnnonation=m_mapView.annotations;
//        [self.m_mapView removeAnnotations:lastAnnonation];
//    }
    
    
    for (id<MKAnnotation> annotation in self.m_mapView.annotations) {
        [self.m_mapView removeAnnotation:annotation];
    }
    
    if (messageId!=nil)
    {
        [messageId  release];
        messageId=nil;
    }
    messageId=[[NSMutableArray alloc]init];
    
    [[LoadingIndicatorView SharedInstance] startLoadingView:self];
    self.moreButton.hidden=NO;
    self.selectedType=[[NewsDataManager sharedManager] getAllCategoryIdAtIndex:currentRow];
    [[NewsDataManager sharedManager] getOneCategoryData:self type:self.selectedType filter:POPULAR number:24 numPage:count addToExisting:NO];
    
    UITableView *categoryTable=(UITableView*)[self.view viewWithTag:11];
    
    if (categoryTable)
    {
        
        [UIView animateWithDuration:.25 animations:^{
            categoryTable.alpha=0.0;
        }];
        [UIView commitAnimations];
        [categoryTable removeFromSuperview];
    }
}
#pragma mark- textField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
//    for (id<MKAnnotation> annotation in annotations)
//    {
//        
//        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
//        MKMapRect zoomRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
//        [m_mapView setVisibleMapRect:zoomRect animated:YES];
//    }

    
    headertextLBL.hidden=YES;
    headerImage.hidden=NO;
    
    serachTaG=YES;
    // count=500;
    self.moreButton.hidden=YES;
    [textField resignFirstResponder];
    
    annonationCount=0;
    if (annotations!=nil)
    {
        [annotations release];
        annotations=nil;
    }
    annotations=[[NSMutableArray alloc] init];
//    if (m_mapView.annotations >0)
//    {
//        NSArray *lastAnnonation=m_mapView.annotations;
//        [self.m_mapView removeAnnotations:lastAnnonation];
//    }
    
    for (id<MKAnnotation> annotation in self.m_mapView.annotations) {
        [self.m_mapView removeAnnotation:annotation];
    }
    
    
    if (messageId!=nil)
    {
        [messageId  release];
        messageId=nil;
    }
    messageId=[[NSMutableArray alloc]init];
    
    [[LoadingIndicatorView SharedInstance] startLoadingView:self];
    [m_request searchStories:@selector(CallBAckMethodRange:rasponseData:) tempTarget:self seratchText:textField.text startIndex:0 endIndex:24];
    
    
    
    return YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITableView *tableView=(UITableView*)[self.view viewWithTag:11];
    
    if (tableView)
    {
        [UIView animateWithDuration:.25 animations:^{
            tableView.alpha=0.0;
        }];
        [UIView commitAnimations];
        
        [tableView removeFromSuperview];
        
        
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
   // self.m_mapView=nil;
    self.selectedType=nil;
    self.searchText=nil;
    self.categoryBTN=nil;
    self.moreButton=nil;
    self.helpView=nil;
    self.closeByn=nil;
    self.headerLBL=nil;
    self.singleTapOnPin=nil;
    self.singleTapOnheadline =nil;
    self.firstDoubletap=nil;
    self.secondDoubletap=nil;
    self.headertextLBL=nil;
    self.headerImage=nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)dealloc
{
    [closeByn release];
    [headerLBL release];
    [singleTapOnPin release];
    [singleTapOnheadline release];
    [firstDoubletap release];
    [secondDoubletap release];
    [m_data release];
    [helpView release];
    [searchText release];
    [categoryBTN release];
    [m_mapView release];
    [m_request release];
    [selectedType release];
    [moreButton release];
    if (m_listArray!=nil)
    {
        [m_listArray release];
        m_listArray=nil;
    }
    if (annotations!=nil)
    {
        [annotations release];
        annotations=nil;
    }
    if (messageId!=nil)
    {
        [messageId release];
        messageId=nil;
    }
    [headerImage release];
    [headertextLBL release];
    [super dealloc];
}
@end
